﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AlertPerson;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.Kenh14.Entity;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/alertperson")]
    public class AlertPersonController : ApiController
    {
        #region AlertPerson

        [HttpPost, Route("search")]
        public HttpResponseMessage SearchAlertPerson(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRow = 0;
                var data = new AlertPersonServices().Search(keyword, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_by_id")]
        public HttpResponseMessage GetAlertPersonById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var alertperson = new AlertPersonServices().GetAlertPersonById(id);
                if (alertperson == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy person"));
                }

                return this.ToJson(WcfActionResponse<AlertPersonEntity>.CreateSuccessResponse(alertperson, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save")]
        public HttpResponseMessage SaveAlertPerson(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                
                var id = Utility.ConvertToInt(body.Get("Id"), 0);                
                var personName = body.Get("PersonName") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), 0);

                var tagNew = new AlertPersonEntity
                {
                    Id = id,
                    PersonName = personName,
                    Status = status
                };


                if (id > 0)
                {
                    var result = new AlertPersonServices().Update(tagNew);
                    return this.ToJson(result);
                }
                else {
                    var newsAlertPersonId = 0;
                    var result = new AlertPersonServices().InsertTag(tagNew);
                    result.Data = newsAlertPersonId.ToString();
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete")]
        public HttpResponseMessage AlertPersonDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var alertpersonId = Utility.ConvertToInt(body.Get("Id"));

                var result = new AlertPersonServices().DeleteById(alertpersonId);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        #endregion
    }
}
