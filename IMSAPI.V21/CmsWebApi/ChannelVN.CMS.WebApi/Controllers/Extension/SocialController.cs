﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.SocialNetwork.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/social")]
    public class SocialController : ApiController
    {
        [HttpPost, Route("search_log_action")]
        public HttpResponseMessage SearchActivity(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);
                var applicationId = Utility.ConvertToLong(body.Get("AppId"));
                var actionType = Utility.ConvertToInt(body.Get("Type"), -1);
                var objecttype = Utility.ConvertToInt(body.Get("ObjectType"), -1);
                var keyword = body.Get("Keyword")??string.Empty;
                var sourceId = body.Get("SourceId") ?? string.Empty;

                NewsActionType filterActionType;
                if (!Enum.TryParse(actionType.ToString(), true, out filterActionType))
                {
                    filterActionType = NewsActionType.AllAction;
                }
                EnumActivityType filterType;
                if (!Enum.TryParse(objecttype.ToString(), true, out filterType))
                {
                    filterType = EnumActivityType.News;
                }
                var dateFrom = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("From"),"dd/MM/yyyy hh:mm");
                var dateTo = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("To"), "dd/MM/yyyy hh:mm");
                
                var totalRows = 0;
                var data = new SocialServices().SearchActivity(keyword, pageIndex, pageSize, applicationId, filterActionType, filterType, sourceId, dateFrom, dateTo, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total= totalRows, Logs=data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #region NewsSocialEmbed

        #region action

        [HttpPost, Route("crawler_newssocialembed")]
        public HttpResponseMessage CrawlerNewsSocialEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                
                var url = body.Get("Url");
                if(string.IsNullOrEmpty(url) || (!string.IsNullOrEmpty(url) && !url.StartsWith("http"))){
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }

                var id = 0L;
                var urlRegex = string.Empty;
                var originalId = GetOriginalId(url, out urlRegex);

                if (originalId == 7777)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Sai định dạng link twitter, link mẫu: https://twitter.com/BMW/status/1041718429865193472"));

                if (originalId==9999)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Sai định dạng link intagram, link mẫu: https://www.instagram.com/p/BnRs5c0gD2u/"));

                if (originalId == 8888 && !string.IsNullOrEmpty(urlRegex))
                {
                    originalId= (int)EnumNewsSocialEmbedOriginalId.Intagram;
                    url = urlRegex;
                }

                var rawId = Crypton.Md5Encrypt(string.Format("{0}_{1}",originalId,url));

                var socialService = new SocialServices();

                //Table NewsSocialContent
                if (originalId == (int)EnumNewsSocialEmbedOriginalId.News)
                {                    
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không thể lấy được dữ liệu từ link bài viết."));
                }

                //check url
                if (socialService.CheckUrlNewsEmbeb(rawId))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.SocialNewsUrlDulicate]));
                }

                var status = (int)EnumNewsSocialEmbedStatus.NoActive;

                var embed = BuildEmbed(originalId, url);

                var newsSocial = new NewsSocialEmbedEntity
                {
                    Id = id,
                    RawId = rawId,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    Priority = 0,
                    Status = status,
                    OriginalId = originalId,                
                    Url = url,
                    Embed = embed
                };

                var data = socialService.InsertNewsSocialEmbed(newsSocial, ref id);
                data.Data = id.ToString();

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private string BuildEmbed(int originalId, string url)
        {
            var embed = string.Empty;                       
            switch (originalId)
            {
                case 1:
                    var json= GetRequest(string.Format("https://publish.twitter.com/oembed?url={0}", HttpContext.Current.Server.UrlEncode(url)));
                    if (!string.IsNullOrEmpty(json))
                    {
                        var paserJson = NewtonJson.Deserialize<TwitterJson>(json);
                        embed = paserJson!=null? paserJson.html:string.Empty;
                    }                        
                    break;
                case 2:
                    embed = string.Format(@"<iframe style='display: block; max-height:100%; max-width: 100%; min-height: 100%; min-width: 100%; margin: auto; opacity: 1;' scrolling='no' frameborder='0' allowtransparency='true' title='Instagram: null' src='{0}' class='i-amphtml-fill-content'></iframe>", url);
                    break;
                case 3:
                    embed = string.Format(@"<iframe src='https://www.facebook.com/plugins/post.php?href={0}&width=100%25&show_text=true&appId=1846714538873972&height=467' width='100%' height='100%' style='border:none;overflow:hidden' scrolling='no' frameborder='0' allowTransparency='true' allow='encrypted-media'></iframe>", HttpContext.Current.Server.UrlEncode(url));
                    break;
                case 4:
                    embed = string.Format(@"<iframe src='https://www.facebook.com/plugins/video.php?href={0}&width=500&show_text=true&appId=1846714538873972&height=459' width='100%' height='100%' style='border:none;overflow:hidden' scrolling='no' frameborder='0' allowTransparency='true' allow='encrypted-media' allowFullScreen='true'></iframe>", HttpContext.Current.Server.UrlEncode(url));
                    break;                
            }
            return embed;            
        }

        private string GetRequest(string url)
        {                        
            try
            {
                var req = (HttpWebRequest)WebRequest.Create(url);
                req.ContentType = "application/json; charset=utf-8";
                req.Accept = "text/html,application/xhtml+xml,application/xml,application/json;q=0.9,*/*;q=0.8";
                req.Method = "GET";

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                          
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return string.Empty;
            }
        }

        private int GetOriginalId(string url, out string urlRegex)
        {
            urlRegex = string.Empty;
            Uri NewUri;

            if (Uri.TryCreate(url, UriKind.Absolute, out NewUri))
            {
                if (NewUri.Authority.ToLower().Contains("twitter.com"))
                {
                    if (NewUri.AbsolutePath.ToLower().Contains("status"))
                        return (int)EnumNewsSocialEmbedOriginalId.Tweeter;
                    return 7777;
                }
                else if (NewUri.Authority.ToLower().Contains("instagram.com"))
                {
                    var reg = Regex.Match(NewUri.AbsolutePath, "^/p/[a-zA-Z-_0-9]+/+embed");
                    var reg2 = Regex.Match(NewUri.AbsolutePath, "^/p/[a-zA-Z-_0-9]+/");
                    if (reg.Success)
                        return (int)EnumNewsSocialEmbedOriginalId.Intagram;
                    else if (reg2.Success) {
                        urlRegex = url.Replace(NewUri.AbsolutePath, NewUri.AbsolutePath+"embed");
                        return 8888;
                    }
                    else
                        return 9999;
                }
                else if(NewUri.Authority.ToLower().Contains("facebook.com"))
                {
                    if(NewUri.AbsolutePath.ToLower().Contains("videos"))
                        return (int)EnumNewsSocialEmbedOriginalId.FacebookVideo;
                    return (int)EnumNewsSocialEmbedOriginalId.Facebook;
                }
                else
                {
                    return (int)EnumNewsSocialEmbedOriginalId.News;
                }
            }

            return (int)EnumNewsSocialEmbedOriginalId.News;
        }
        
        [HttpPost, Route("delete_newssocialembed")]
        public HttpResponseMessage DeleteNewsSocialEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var data = new SocialServices().DeleteNewsSocialEmbedById(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("toggle_status_newssocialembed")]
        public HttpResponseMessage OnToggleStatusNewsSocialEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var data = new SocialServices().OnToggleStatusNewsSocialEmbed(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Search
        [HttpPost, Route("search_newssocialembed")]
        public HttpResponseMessage SearchNewsSocialEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;                
                var status = Utility.ConvertToInt(body.Get("Status"), -1);                

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"));

                var totalRow = 0;
                var data = new SocialServices().SearchNewsSocialEmbed(status, pageIndex, pageSize, ref totalRow);
                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { TotalRow = totalRow, NewsSocialEmbeds = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region get

        [HttpPost, Route("get_newssocialembed_by_id")]
        public HttpResponseMessage GetNewsSocialEmbedById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));

                var data = new SocialServices().GetNewsSocialEmbedById(id);

                return this.ToJson(WcfActionResponse<NewsSocialEmbedEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!" + ex.Message));
            }
        }

        #endregion

        #endregion

        #region NewsSocialContent

        #region action
        [HttpPost, Route("crawler_newssocialcontent")]
        public HttpResponseMessage CrawlerNewsSocialContent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var url = body.Get("Url");
                if (string.IsNullOrEmpty(url) || (!string.IsNullOrEmpty(url) && !url.StartsWith("http")))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }

                var urlRegex = string.Empty;
                var originalId = GetOriginalId(url, out urlRegex);
                var rawId = Crypton.Md5Encrypt(string.Format("{0}_{1}", originalId, url));

                var socialService = new SocialServices();

                //Table NewsSocialContent
                if (originalId == (int)EnumNewsSocialEmbedOriginalId.News)
                {
                    var urlApiV2 = CmsChannelConfiguration.GetAppSetting("CrawlerV2ApiUrl");
                    var dataNewJson = GetRequest(string.Format(urlApiV2 + "?url_down={0}", url));                    
                    if (!string.IsNullOrEmpty(dataNewJson))
                    {
                        var paserJson = NewtonJson.Deserialize<DataNewsJson>(dataNewJson);
                        if (paserJson != null)
                            return this.ToJson(new NewsSocialContentEntity
                            {
                                Title = paserJson.title,
                                Body = paserJson.content,
                                Avatar = paserJson.avatar,
                                Url = paserJson.url,
                                OriginalUrl = paserJson.url,
                                OriginalId = originalId,
                                OriginalName = paserJson.domain,
                                CreatedBy = accountName,
                                CreatedDate = DateTime.Now,
                                Sapo = paserJson.excerpt,
                                WordCount = paserJson.length.Value,
                                Status = (int)EnumNewsSocialContentStatus.NoActive,
                                RawId = rawId
                            });

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Không thể passer json data."));
                    }
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không thể lấy được dữ liệu từ link bài viết."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Không thể lấy được dữ liệu từ link bài viết."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_newssocialcontent")]
        public HttpResponseMessage SaveNewsSocialContent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var title = body.Get("Title");
                var sapo = body.Get("Sapo");
                var content = body.Get("Body");
                var avatar = body.Get("Avatar");
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"),5);
                var originalName = body.Get("OriginalName");
                var url = body.Get("Url");
                var originalUrl = body.Get("OriginalUrl");
                var avatarCustom = body.Get("AvatarCustom");
                var avatar1 = body.Get("Avatar1");
                var avatar2 = body.Get("Avatar2");
                var avatar3 = body.Get("Avatar3");                
                var priority = Utility.ConvertToInt(body.Get("Priority"),0);
                var wordCount = Utility.ConvertToInt(body.Get("WordCount"),0);
                var note = body.Get("Note");
                var rawId = body.Get("RawId");

                var socialService = new SocialServices();                

                var newsSocialContent = new NewsSocialContentEntity
                {
                    Id = id,                    
                    Title = title,
                    Sapo = sapo,
                    Body = content,
                    Avatar = avatar,
                    OriginalId = originalId,
                    OriginalName=originalName,
                    Url = url,
                    OriginalUrl = originalUrl,
                    AvatarCustom = avatarCustom,
                    Avatar1 = avatar1,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Status = (int)EnumNewsSocialContentStatus.NoActive,
                    Priority = priority,
                    WordCount = wordCount,
                    Note = note,
                    RawId = rawId,
                    CreatedBy=accountName,
                    CreatedDate=DateTime.Now
                };

                var dataContent = new WcfActionResponse();

                if (id <= 0)
                {
                    if (socialService.CheckUrlNewsContent(rawId))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.SocialNewsUrlDulicate]));
                    }

                    dataContent = socialService.InsertNewsSocialContent(newsSocialContent, ref id);
                    dataContent.Data = id.ToString();
                }
                else
                {
                    dataContent = socialService.UpdateNewsSocialContent(newsSocialContent);
                }

                return this.ToJson(dataContent);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }        

        [HttpPost, Route("delete_newssocialcontent")]
        public HttpResponseMessage DeleteNewsSocialContent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var data = new SocialServices().DeleteNewsSocialContentById(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("toggle_status_newssocialcontent")]
        public HttpResponseMessage OnToggleStatusNewsSocialContent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var data = new SocialServices().OnToggleStatusNewsSocialContent(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Search
        [HttpPost, Route("search_newssocialcontent")]
        public HttpResponseMessage SearchNewsSocialContent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("Keyword");
                var status = Utility.ConvertToInt(body.Get("Status"), -1);                

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"));

                var totalRow = 0;
                var data = new SocialServices().SearchNewsSocialContent(keyword, status, pageIndex, pageSize, ref totalRow);
                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { TotalRow = totalRow, NewsSocialContents = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region get

        [HttpPost, Route("get_newssocialcontent_by_id")]
        public HttpResponseMessage GetNewsSocialContentById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));

                var data = new SocialServices().GetNewsSocialContentById(id);

                return this.ToJson(WcfActionResponse<NewsSocialContentEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!" + ex.Message));
            }
        }

        #endregion

        #endregion

        #region BoxNewsSocialEmbed
                                
        [HttpPost, Route("get_boxnewssocialembed")]
        public HttpResponseMessage GetListBoxNewsSocialEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var type = Utility.ConvertToInt(body.Get("Type"));

                var data = new SocialServices().GetListBoxNewsSocialEmbed(type,zoneId);

                return this.ToJson(WcfActionResponse<List<BoxNewsSocialEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_boxnewssocialembed")]
        public HttpResponseMessage UpdateListBoxNewsSocialEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                
                var type = Utility.ConvertToInt(body.Get("Type"));
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var listNewsId = body.Get("ListNewsId") ?? string.Empty;

                var result = new SocialServices().UpdateListBoxNewsSocialEmbed(listNewsId, type, zoneId);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}
