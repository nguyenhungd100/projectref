﻿using ChannelVN.Advertisment.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/advertisment")]
    public class AdvertismentController : ApiController
    {
        #region Advertisment

        [HttpPost, Route("search")]
        public HttpResponseMessage Search(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword")??string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var typeId = Utility.ConvertToInt(body.Get("TypeId"), -1);
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"), -1);
                var positionId = Utility.ConvertToInt(body.Get("PositionId"), -1);

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 50);
                
                var totalRow = 0;
                var data = new AdvertismentServices().Search(keyword, (EnumAdvertismentType)typeId, zoneId, displayStyle, (EnumAdvertismentStatus)status, positionId, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("list_all")]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var data = new AdvertismentServices().GetAllAdvertisment();

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_by_position")]
        public HttpResponseMessage GetAdvertismentByPosition(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var typeId = Utility.ConvertToInt(body.Get("TypeId"), -1);
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"), -1);
                var positionId = Utility.ConvertToInt(body.Get("PositionId"), -1);                
                
                var data = new AdvertismentServices().GetAdvertismentByPosition((EnumAdvertismentType)typeId, zoneId, displayStyle, (EnumAdvertismentStatus)status, positionId);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_by_id")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new AdvertismentServices().GetById(id);
                if (data == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not found data"));
                }

                return this.ToJson(WcfActionResponse<AdvertismentDetailEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save")]
        public HttpResponseMessage Save(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var title = body.Get("Title") ?? string.Empty;
                if (string.IsNullOrEmpty(title))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not empty 'Title'"));
                }
                var adCode = body.Get("AdCode") ?? string.Empty;
                var targetUrl = body.Get("TargetUrl") ?? string.Empty;
                var typeId = Utility.ConvertToInt(body.Get("TypeId"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 1);//Active
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"), 0);
                var positionId = Utility.ConvertToInt(body.Get("PositionId"), 0);
                var note = body.Get("Note") ?? string.Empty;
                var priority = Utility.ConvertToInt(body.Get("Priority"), 0);
                var startDate = Utility.ParseDateTime(body.Get("StartDate"),new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });
                var endDate = Utility.ParseDateTime(body.Get("EndDate"), new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });
                var width = Utility.ConvertToInt(body.Get("Width"), 0);
                var height = Utility.ConvertToInt(body.Get("Height"), 0);

                var listZones = body.Get("ZoneIds") ?? string.Empty;

                var obj = new AdvertismentEntity
                {
                    Id = id,
                    Title = title,
                    ZoneId = zoneId,
                    TypeId = typeId,
                    AdCode = adCode,
                    DisplayStyle= displayStyle,
                    TargetUrl = targetUrl,
                    PositionId = positionId,
                    Note = note,                    
                    Priority= priority,
                    StartDate= startDate,
                    EndDate = endDate,                                                           
                    Width= width,
                    Height = height,                    
                    Status = status                   
                }; 
                               
                if (id > 0)
                {                    
                    var result = new AdvertismentServices().Update(obj, listZones);
                    return this.ToJson(result);
                }
                else {                    
                    var result = new AdvertismentServices().Create(obj, listZones);                    
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"),0);

                var result = new AdvertismentServices().Delete(id);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_status")]
        public HttpResponseMessage UpdateStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                if (status <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Status not support"));
                }
                
                var result = new AdvertismentServices().UpdateStatus(id, (EnumAdvertismentStatus)status);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion        
    }
}
