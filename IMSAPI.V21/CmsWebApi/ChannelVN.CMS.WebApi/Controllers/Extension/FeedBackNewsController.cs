﻿using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/feedbacknews")]
    public class FeedBackNewsController : ApiController
    {
        [HttpPost, Route("search")]
        public HttpResponseMessage SearchActivity(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var keyword = body.Get("Keyword")??string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRow = 0;
                var data = new FeedBackNewsServices().SearchNews(keyword, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total= totalRow, News=data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("receive")]
        public HttpResponseMessage RecieveFeedbackNewsIntoCms(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var newsId = Utility.ConvertToInt(body.Get("Id"), 0);
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var feedbacknews = new FeedBackNewsServices().GetNewsById(newsId);
                var status = (int)NewsStatus.Temporary;
                
                NewsEntity newsEntity = new NewsEntity()
                {
                    Title = feedbacknews.News_Title,
                    SubTitle = feedbacknews.News_Subtitle,
                    Sapo = string.Empty,
                    Body = feedbacknews.News_Content,
                    Avatar = feedbacknews.News_Image,
                    AvatarDesc = feedbacknews.News_ImageNote,
                    Source = feedbacknews.News_Source,
                    Author = "A.D",
                    WordCount = feedbacknews.WordCount,
                    IsPr = false,
                    PrPosition = 0,
                    AdStore = false,
                    AdStoreUrl = "",
                    Url = "",
                    LocationType = 0,
                    Status = status,
                    CreatedBy = accountName,
                    ZoneId = feedbacknews.Cat_ID,
                    NewsRelation = feedbacknews.News_Relation,
                    TagItem = string.Empty,
                    NewsCategory = 0,
                    Type = 0,
                    Price = 0,
                    IsFocus = feedbacknews.News_isFocus,
                    DistributionDate = feedbacknews.News_PublishDate,
                    Id = feedbacknews.ImsNewsId,
                    ParentNewsId = feedbacknews.News_ID,
                };
                var returnNewsId = 0L;
                var responseData = new NewsServices().RecieveFeedbackNewsIntoCms(newsEntity, ref returnNewsId);
                if (responseData.Success)
                {
                    new FeedBackNewsServices().UpdateStatusIms(returnNewsId, 2, feedbacknews.FeedBack_ID);
                };
               
                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
    }
}
