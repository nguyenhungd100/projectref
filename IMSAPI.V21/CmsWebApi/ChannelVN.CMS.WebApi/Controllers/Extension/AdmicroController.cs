﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.ForExternalApplication.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    //[Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/adstore")]
    public class AdmicroController : ApiController
    {
        [HttpPost, Route("get_news_by_id")]
        public HttpResponseMessage GetNewsById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var adPageForChannel = (ChannelMappingForExternalApplication)Utility.ConvertToInt(body.Get("adPageForChannel"));
                var secretKey = body.Get("secretKey") ?? string.Empty;
                var newsId = Utility.ConvertToLong(body.Get("newsId"));

                var secretKeyCheck = Crypton.Md5Encrypt(ServiceChannelConfiguration.GetSecretKey(adPageForChannel.ToString()));
                if (string.IsNullOrEmpty(secretKey) || (secretKey != secretKeyCheck))
                    return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Invalid secretKey"));

                var data = new AdmicroServices().GetNewsById(adPageForChannel, newsId);

                return this.ToJson(WcfActionResponse<NewsForAdStoreEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("search_adpage")]
        public HttpResponseMessage SearchAdpage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var adPageForChannel = (ChannelMappingForExternalApplication)Utility.ConvertToInt(body.Get("adPageForChannel"));
                var secretKey = body.Get("secretKey") ?? string.Empty;

                var secretKeyCheck = Crypton.Md5Encrypt(ServiceChannelConfiguration.GetSecretKey(adPageForChannel.ToString()));
                if (string.IsNullOrEmpty(secretKey) || (secretKey != secretKeyCheck))
                    return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Invalid secretKey"));

                var keyword = body.Get("keyword") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"),0);

                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("distributionDateFrom"), "dd/MM/yyyy");
                var to_date = body.Get("distributionDateTo");
                if (!string.IsNullOrEmpty(to_date))
                    to_date = to_date + " 23:59:59";

                var toDate = Utility.ConvertToDateTimeByFormat(to_date, "dd/MM/yyyy HH:mm:ss");

                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 20);

                var totalRows = 0;
                var data = new AdmicroServices().SearchNewsForAdStore(adPageForChannel, keyword, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, News = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_adpage_mode")]
        public HttpResponseMessage UpdateAdPageMode(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var adPageForChannel = (ChannelMappingForExternalApplication)Utility.ConvertToInt(body.Get("adPageForChannel"));
                var secretKey = body.Get("secretKey") ?? string.Empty;
                var newsId = Utility.ConvertToLong(body.Get("newsId"));
                var adPageMode = (EnumAdPageMode)Utility.ConvertToInt(body.Get("adPageMode"));
                var adStoreUrl = body.Get("adStoreUrl") ?? string.Empty;

                var secretKeyCheck = Crypton.Md5Encrypt(ServiceChannelConfiguration.GetSecretKey(adPageForChannel.ToString()));
                if (string.IsNullOrEmpty(secretKey) || (secretKey != secretKeyCheck))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Invalid secretKey"));

                var data = new AdmicroServices().UpdateAdPageMode(adPageForChannel, newsId, adPageMode, adStoreUrl);

                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
    }
}
