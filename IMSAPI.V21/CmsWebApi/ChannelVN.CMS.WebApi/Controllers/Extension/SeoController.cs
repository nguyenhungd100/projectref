﻿using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.SEO.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/seo")]
    public class SeoController : ApiController
    {
        #region SEOMetaNews
        [HttpPost, Route("get_seometa_by_news_id")]
        public HttpResponseMessage GetSeoMetaNewsById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                long newsId = 0;
                long.TryParse(body.Get("NewsId"), out newsId);
                if (newsId == 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy NewsId này."));
                }
                                
                var data = new SeoServices().GetSeoMetaNewsById(newsId);

                return this.ToJson(WcfActionResponse<SEOMetaNewsEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        #endregion

        #region SEOMetaVideo
        [HttpPost, Route("get_seometa_by_video_id")]
        public HttpResponseMessage GetSeoMetaVideoById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                int videoId = Utility.ConvertToInt(body.Get("VideoId"));
                if (videoId == 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy Video này."));
                }

                var data = new SeoServices().GetSeoMetaVideoById(videoId);

                return this.ToJson(WcfActionResponse<SEOMetaVideoEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region For tool seo

        #region JOB api dung cho bot auto quest seo.
        [HttpPost, Route("job_update_seo_review")]
        public HttpResponseMessage JobUpdateSeoReview(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
               
                var Queue = ExpertSuggestionService.Queue_SeoerReview_GetList();

                //test data
                //Queue = new List<WcfMapping.ServiceExpertSuggestion.QueueSeoerReviewEntity> {
                //    new WcfMapping.ServiceExpertSuggestion.QueueSeoerReviewEntity
                //    {
                //        CreateBy="acctest",
                //        Body="test body",
                //        MetaDescription="test MetaDescription",
                //        KeywordFocus="test KeywordFocus",
                //        MetaTitle="test MetaTitle",
                //        NewsId=20130813100030569,
                //        Tags=""
                //    }
                //};

                if (Queue != null && Queue.Count>0)
                {
                    var newsIds = string.Empty;
                    //update seo vao DB
                    foreach (var item in Queue)
                    {
                        if (newsIds != string.Empty) newsIds += ";";
                        var News = new SeoerNewsEntity
                        {
                            ReviewerName = item.CreateBy,
                            Body = item.Body,
                            MetaDescription = item.MetaDescription,
                            MetaKeyword = item.KeywordFocus,
                            MetaTitle = item.MetaTitle,
                            NewsId = item.NewsId,
                            Tag = item.Tags,
                            ReviewStatus=(int)SEOMetaNewsReviewStatusEnum.ReviewWaiting
                        };
                        var res = new NewsServices().Seoer_Update(News);

                        //Logger.WriteLog(Logger.LogType.Debug, "JobUpdateSeoReview: NewsServices.Seoer_Update => " + res);
                        if (res.Success)
                        {
                            newsIds += News.NewsId;
                        }
                    }
                    //Logger.WriteLog(Logger.LogType.Debug, "JobUpdateSeoReview: News update => " + newsIds);
                    if (!string.IsNullOrEmpty(newsIds))
                    {
                        var up = ExpertSuggestionService.Queue_SeoerReview_UpdateStatus(newsIds);
                        if (up)
                            Logger.WriteLog(Logger.LogType.Debug, "JobUpdateSeoReview: News Ok => " + newsIds);
                    }                    
                }

                return this.ToJson(WcfActionResponse<bool>.CreateSuccessResponse(true,"Success."));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Error: " + ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("job_update_seo_thread")]
        public HttpResponseMessage JobUpdateSeoThread(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var Queue = ExpertSuggestionService.ThreadQueue_GetByNamespace();
                if (Queue != null && Queue.Count > 0)
                {
                    var idThreads = string.Empty;
                    //update seo vao DB
                    var ThreadServices = new ThreadServices();
                    foreach (var item in Queue)
                    {
                        try
                        {
                            var imsThread = ThreadServices.GetThreadByThreadId(item.ThreadChannelId);
                            if (imsThread != null)
                            {
                                var Thread = new ThreadEntity();
                                Thread.Avatar = item.Avatar;
                                Thread.Description = item.Description;
                                Thread.EditedBy = item.EditedBy;
                                Thread.IsHot = item.IsHot;
                                Thread.MetaContent = item.MetaContent;
                                Thread.MetaKeyword = item.MetaKeyword;
                                Thread.ModifiedDate = item.ModifiedDate;
                                Thread.Title = item.Title;
                                Thread.UnsignName = item.UnsignName;
                                Thread.Url = item.Url;
                                Thread.Id = (int)item.ThreadChannelId;
                                Thread.Name = item.Name;
                                Thread.CreatedBy = imsThread.CreatedBy;
                                Thread.CreatedDate = imsThread.CreatedDate;
                                Thread.HomeAvatar = imsThread.HomeAvatar;
                                Thread.Invisibled = imsThread.Invisibled;
                                Thread.IsOnHome = imsThread.IsOnHome;
                                Thread.NewsCount = imsThread.NewsCount;
                                Thread.NewsCoverId = imsThread.NewsCoverId;
                                Thread.SpecialAvatar = imsThread.SpecialAvatar;
                                Thread.TemplateId = imsThread.TemplateId;

                                string ThreadRelations = "";
                                foreach (var _ThreadRelation in imsThread.ThreadRelation)
                                {
                                    ThreadRelations += _ThreadRelation.Id;
                                }
                                if (ThreadRelations != "") ThreadRelations = ThreadRelations.Substring(1);

                                if (idThreads != "") idThreads += ";";
                                int zoneId = 0;
                                string zoneIdList = "";
                                if (item.ThreadInZone.Count() > 0)
                                {
                                    var PrimaryZone = item.ThreadInZone.Where(k => k.IsPrimary == true).FirstOrDefault();
                                    if (PrimaryZone != null)
                                        zoneId = PrimaryZone.ZoneId;
                                    var ListZone = item.ThreadInZone.Where(k => k.IsPrimary == false).ToList();
                                    foreach (var zondId in ListZone)
                                    {
                                        if (zoneIdList != "") zoneIdList += ";";
                                        zoneIdList += zondId.ZoneId;
                                    }
                                }
                                //Logger.WriteLog(Logger.LogType.Debug, "ThreadRelations==>" + ThreadRelations);
                                var result = ThreadServices.Update(Thread, zoneId, zoneIdList, ThreadRelations);
                                if (result.Success) idThreads += item.Id;
                            }
                        }
                        catch (Exception ex)
                        {                            
                            Logger.WriteLog(Logger.LogType.Error, "Thread error==>"+ ex.Message + " item: " + NewtonJson.Serialize(item));
                        }
                    }
                    //Logger.WriteLog(Logger.LogType.Debug, "JobUpdateSeoThread: Thread update => " + idThreads);
                    if (!string.IsNullOrEmpty(idThreads))
                    {
                        var up = ExpertSuggestionService.ThreadQueue_UpdateStatus(idThreads);
                        if (up)
                            Logger.WriteLog(Logger.LogType.Debug, "JobUpdateSeoThread: Thread Ok => " + idThreads);
                    }
                }

                return this.ToJson(WcfActionResponse<bool>.CreateSuccessResponse(true, "Success."));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Error: " + ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("job_update_seo_tag")]
        public HttpResponseMessage JobUpdateSeoTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var Queue = ExpertSuggestionService.Tag_GetByQueue();
                if (Queue != null && Queue.Count > 0)
                {
                    var idTags = string.Empty;
                    //update seo vao DB
                    var TagServices = new TagServices();
                    foreach (var item in Queue)
                    {                        
                        if (idTags != "") idTags += ";";                        
                        int zoneId = 0;
                        string zoneIdList = "";
                        if (item.TagZone.Count() > 0)
                        {
                            var PrimaryZone = item.TagZone.Where(k => k.IsPrimary == true).FirstOrDefault();
                            if (PrimaryZone != null)
                                zoneId = PrimaryZone.ZoneChannelId;
                            var ListZone = item.TagZone.Where(k => k.IsPrimary == false).ToList();
                            foreach (var zondId in ListZone)
                            {
                                if (zoneIdList != "") zoneIdList += ";";
                                zoneIdList += zondId.ZoneChannelId;
                            }
                        }

                        var zoneName = string.Empty;
                        var zone = ZoneBo.GetZoneById(zoneId);
                        if (zone != null)
                        {
                            zoneName = zone.Name;
                        }

                        var tagNew = new TagEntity
                        {
                            Name = item.Name,
                            Url = item.Url,
                            IsHotTag = item.IsHotTag,
                            Type = item.Type,
                            Id = item.TagChannelId,
                            EditedBy = item.CreatedBy,
                            Avatar = item.Avatar,
                            UnsignName = item.UnsignName,
                            TagContent = item.TagContent,
                            TagTitle = item.TagTitle,
                            TagInit = item.TagInit,
                            TagMetaKeyword = item.TagMetaKeyword,
                            TagMetaContent = item.TagMetaContent,
                            CreatedDate = item.CreatedDate,
                            ZoneId=zoneId,
                            ZoneName=zoneName
                        };

                        var imsTag = TagServices.GetTagByTagName(item.Name);
                        if (imsTag != null)
                        {                            
                            tagNew.CreatedDate = imsTag.CreatedDate;
                            tagNew.Description = imsTag.Description;
                            tagNew.Invisibled = imsTag.Invisibled;
                            tagNew.IsThread = imsTag.IsThread;
                            tagNew.NewsCoverId = imsTag.NewsCoverId;
                            tagNew.ParentId = imsTag.ParentId;
                            tagNew.Priority = imsTag.Priority;
                            tagNew.SubTitle = imsTag.SubTitle;
                            tagNew.TemplateId = imsTag.TemplateId;
                            tagNew.UnsignName = imsTag.UnsignName;
                            tagNew.Url = imsTag.Url;
                            tagNew.Id = imsTag.Id;
                            tagNew.ZoneId = zoneId;
                            tagNew.ZoneName = zoneName;

                            var result = TagServices.Update(tagNew, zoneId, zoneIdList);
                            if (result.Success)
                            {
                                idTags += item.TagId;                                
                            }
                        }
                        else
                        {
                            var result = TagServices.InsertTag(tagNew, zoneId, zoneIdList);
                            if (result.Success)
                            {
                                idTags += item.TagId;                                
                            }
                        }
                    }
                    //Logger.WriteLog(Logger.LogType.Debug, "JobUpdateSeoThread: Thread update => " + idTags);
                    if (!string.IsNullOrEmpty(idTags))
                    {
                        var up = ExpertSuggestionService.TagQueue_UpdateStatus(idTags);
                        if (up)
                            Logger.WriteLog(Logger.LogType.Debug, "JobUpdateSeoTag: Tag Ok => " + idTags);
                    }
                }

                return this.ToJson(WcfActionResponse<bool>.CreateSuccessResponse(true, "Success."));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Error: " + ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("job_update_seo_box_thread")]
        public HttpResponseMessage JobUpdateSeoBoxThreadEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var Queue = ExpertSuggestionService.BoxThreadEmbedGetListQueue();
                if (Queue != null && Queue.Count > 0)
                {
                    WcfActionResponse result = new WcfActionResponse();
                    var ThreadServices = new ThreadServices();

                    foreach (var item in Queue)
                    {
                        switch (item.Action)
                        {
                            case 1:
                                var threadEmbebBox = new BoxThreadEmbedEntity
                                {
                                    ZoneId = item.ZoneId,
                                    ThreadId = int.Parse(item.ListThreadId),
                                    SortOrder = item.SortOrder,
                                    Type = item.Type
                                };
                                result = ThreadServices.InsertThreadEmbed(threadEmbebBox);
                                break;
                            case 2:
                                result = ThreadServices.UpdateThreadEmbed(item.ListThreadId, item.ZoneId, item.Type);
                                break;
                            case 3:
                                result = ThreadServices.DeleteThreadEmbed(int.Parse(item.ListThreadId), item.ZoneId, item.Type);
                                break;
                            default:
                                break;
                        }
                        if (result.Success)
                        {
                            var up = ExpertSuggestionService.BoxThreadEmbedUpdateStatus(item.Id, 1);
                            if (up)
                                Logger.WriteLog(Logger.LogType.Debug, "JobUpdateSeoBoxThreadEmbed: BoxThread Ok => " + item.Id);
                        }
                    }
                }

                return this.ToJson(WcfActionResponse<bool>.CreateSuccessResponse(true, "Success."));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Error: " + ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        [HttpPost, Route("list_seo_change")]
        public HttpResponseMessage ListSeoChange(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var Queue = ExpertSuggestionService.Queue_SeoerReview_GetList();                

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(Queue, "Success."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_seo_status")]
        public HttpResponseMessage UpdateSeoStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var newsIds = body.Get("NewsIds");
                var res = ExpertSuggestionService.Queue_SeoerReview_UpdateStatus(newsIds);
                if(res)
                    return this.ToJson(WcfActionResponse<bool>.CreateSuccessResponse(res, "Success."));

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: Update status seo."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("push_seo")]
        public HttpResponseMessage PushSeo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var domain = WcfExtensions.WcfMessageHeader.Current.Namespace;
                long newsId = 0;
                long.TryParse(body.Get("NewsId"), out newsId);
                if (newsId == 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy NewsId này."));
                }
                var priority = Utility.ConvertToInt(body.Get("Priority"), 4);
                var note = GetQueryString.GetPost("Note", "");

                var newsDetail = new NewsServices().GetDetail(newsId, "");
                var news = newsDetail.NewsInfo;
                var entity = new WcfMapping.ServiceExpertSuggestion.NewsReviewEntity();
                entity.Title = news.Title;
                entity.SubTitle = news.SubTitle;
                entity.Sapo = news.Sapo;
                entity.Avatar = news.Avatar;
                entity.Body = news.Body;
                entity.CreateBy = username;
                entity.Domain = domain;
                entity.PriorityLevelId = priority;
                entity.Note = note;
                entity.NewsId = newsId;
                entity.Type = 1;
                entity.Tags = news.TagItem;
                entity.Url = CmsChannelConfiguration.GetAppSetting("FRONT_END_URL") + news.Url;
                var Zones = new NewsServices().GetNewsInZoneByNewsId(newsId).Where(k => k.IsPrimary).ToList();
                if (Zones.Count > 0)
                    entity.ZoneId = Zones[0].ZoneId;
                var seo = new SeoServices().GetSeoMetaNewsById(newsId);
                if (seo != null)
                {
                    entity.MetaKeywords = seo.KeywordFocus;
                    entity.MetaDescription = seo.MetaDescription;
                    entity.MetaTitle = seo.MetaTitle;
                }
                
                var res = ExpertSuggestionService.SendReview(entity, "");

                return this.ToJson(res);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_seo_review")]
        public HttpResponseMessage ListSeoReview(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var status = WcfMapping.ServiceExpertSuggestion.NewsReviewStatus.EditorialWait;//(WcfMapping.ServiceExpertSuggestion.NewsReviewStatus)Utility.ConvertToInt(body.Get("status"),3);
                var domain = WcfExtensions.WcfMessageHeader.Current.Namespace;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy");
                var to_date = body.Get("to");
                if (!string.IsNullOrEmpty(to_date))
                    to_date = to_date + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to_date, "dd/MM/yyyy HH:mm:ss");
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));

                var totalRows = 0;
                var Queue = ExpertSuggestionService.NewsReview_GetListByStatus(status, zoneIds, domain, fromDate, toDate,  pageSize, pageIndex, ref totalRows);

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { TotalRow = totalRows, News = Queue }, "Success."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news_for_seo")]
        public HttpResponseMessage SearchNewsForSeo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var username = body.Get("username") ?? "";//WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy");
                var to_date = body.Get("to");
                if (!string.IsNullOrEmpty(to_date))
                    to_date = to_date + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to_date, "dd/MM/yyyy HH:mm:ss");                               
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                
                var totalRows = 0;
                var data = new NewsServices().SearchNewsForSeo(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<SearchNewsForSeoResponse>.CreateSuccessResponse(new SearchNewsForSeoResponse { TotalRow = totalRows, News = data?.Select(s=>new ExpertNewsInListEntity {
                    Id=s.Id,
                    Title=s.Title,
                    Avatar=s.Avatar,
                    CreatedBy=s.CreatedBy,
                    CreatedDate=s.CreatedDate,
                    DistributionDate=s.DistributionDate,
                    EncryptId=s.EncryptId,
                    ZoneId=Utility.ConvertToInt(s.ZoneIds?.FirstOrDefault(),0),
                    ZoneName=s.ZoneName,
                    Note=s.Note,
                    IsOnHome=s.IsOnHome,
                    Url=s.Url                    
                }).ToList() }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_news_for_seo_by_id")]
        public HttpResponseMessage GetNewsDetailForSeoById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                                
                var news = new NewsServices().GetNewsByNewsId(newsId);
                if (news != null)
                {
                    news.ZoneId = Utility.ConvertToInt((news.ListZoneId??"0").Split(',').FirstOrDefault(),0);
                }
                var seo= new SeoServices().GetSeoMetaNewsById(newsId);
                var data = new NewsDetailEntity
                {
                    News=news,
                    Seo=seo
                };

                return this.ToJson(WcfActionResponse<NewsDetailEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #region Tag
        [HttpPost, Route("search_tag")]
        public HttpResponseMessage SearchTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var key = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var type = Utility.ConvertToInt(body.Get("type"));
                var isThread = Utility.ConvertToInt(body.Get("isThread"));
                var isHotTag = -1;
                int.TryParse(body.Get("isHotTag"), out isHotTag);
                var parentTagId = Utility.ConvertToInt(body.Get("parentTagId"));
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 20);
                var orderBy = Utility.ConvertToInt(body.Get("orderBy"));
                EnumSearchTagOrder order;
                if (!Enum.TryParse(orderBy.ToString(), out order))
                {
                    order = EnumSearchTagOrder.CreatedDateDesc;
                }
                var totalRow = 0;
                var data = new TagServices().SearchTag(key, parentTagId, isThread, zoneId, type, order, isHotTag, pageIndex, pageSize, ref totalRow, false);
                if (data != null && data.Count > 0)
                {
                    var linkFormatTag = CmsChannelConfiguration.GetAppSetting("UrlFormatTag");
                    data.Select(s => { s.Url = string.Format(linkFormatTag, s.Url); return s; }).ToList();
                }
                return this.ToJson(WcfActionResponse<SearchTagResponse>.CreateSuccessResponse(new SearchTagResponse { TotalRow = totalRow, Tags = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_tag_by_id")]
        public HttpResponseMessage GetTagById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagId = Utility.ConvertToLong(body.Get("id"));

                var data = new TagServices().GetTagByTagId(tagId);

                return this.ToJson(WcfActionResponse<TagEntityDetail>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("get_tag_by_name")]
        public HttpResponseMessage GetTagByName(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagName = body.Get("name")??string.Empty;
                
                var data = new TagServices().GetTagByTagName(tagName);

                return this.ToJson(WcfActionResponse<TagEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region thread
        [HttpPost, Route("search_thread")]
        public HttpResponseMessage SearchThread(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var key = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var isHotThread = -1;
                int.TryParse(body.Get("isHotTag"), out isHotThread);
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var orderBy = Utility.ConvertToInt(body.Get("orderBy"));
                EnumSearchTagOrder order;
                if (!Enum.TryParse(orderBy.ToString(), out order))
                {
                    order = EnumSearchTagOrder.CreatedDateDesc;
                }
                var totalRow = 0;
                var data = new ThreadServices().SearchThread(key, zoneId, (int)order, isHotThread, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var linkFormatThread = CmsChannelConfiguration.GetAppSetting("UrlFormatThread");
                    data.Select(s => { s.Url = string.Format(linkFormatThread, s.Url, s.Id); return s; }).ToList();
                }
                return this.ToJson(WcfActionResponse<SearchThreadResponse>.CreateSuccessResponse(new SearchThreadResponse { TotalRow = totalRow, Threads = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_thread_by_id")]
        public HttpResponseMessage GetThreadEventById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var news = new ThreadServices().GetThreadByThreadId(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(Entity.ErrorCode.ErrorMapping.Current[Entity.ErrorCode.ErrorMapping.ErrorCodes.NotThreaObject]));
                }
                return this.ToJson(WcfActionResponse<ThreadDetailEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_thread_embed")]
        public HttpResponseMessage GetListThreadEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"),0);
                var type = Utility.ConvertToInt(body.Get("type"),1);

                var data = new ThreadServices().GetListThreadEmbed(zoneId, type);
                return this.ToJson(WcfActionResponse<List<BoxThreadEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        #endregion

        #endregion

        #region SEOMetaTag
        [HttpPost, Route("seo_metatag_getlist")]
        public HttpResponseMessage GetListSEOMetaTags(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword")?? string.Empty;
                var zoneid = Utility.ConvertToInt(body.Get("ZoneId"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                var totalRow = 0;
                var data = new SeoServices().GetListSeoMetaTags(keyword, zoneid, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total= totalRow,Data= data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("seo_metatag_save")]
        public HttpResponseMessage SEOMetaTagsSave(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                                                
                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var name = body.Get("Name") ?? string.Empty;
                var metaKeyword = body.Get("MetaKeyword") ?? string.Empty;
                var metaDescription = body.Get("MetaDescription") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);

                var entity = new SEOMetaTagsEntity
                {
                    Id = id,
                    Name = name,
                    MetaKeyword = metaKeyword,
                    MetaDescription = metaDescription,
                    ZoneId = zoneId
                };

                if (id > 0)
                {
                    var result = new SeoServices().SEOMetaTagsUpdate(entity);
                    return this.ToJson(result);
                }
                else {                    
                    var result = new SeoServices().SEOMetaTagsInsert(entity);                    
                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("seo_metatag_getdetail")]
        public HttpResponseMessage GetSEOMetaTagsDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var data = new SeoServices().GetSeoMetaTagsById(id);

                return this.ToJson(WcfActionResponse<SEOMetaTagsEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("seo_metatag_delete")]
        public HttpResponseMessage SEOMetaTagsDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new SeoServices().SEOMetaTagsDelete(id);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region SEOMetaTopic
        [HttpPost, Route("seo_metatopic_getlist")]
        public HttpResponseMessage GetListSEOMetaTopic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword") ?? string.Empty;
                var zoneid = Utility.ConvertToInt(body.Get("ZoneId"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                var totalRow = 0;
                var data = new SeoServices().GetListSeoMetaTags(keyword, zoneid, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("seo_metatopic_save")]
        public HttpResponseMessage SEOMetaTopicSave(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var name = body.Get("Name") ?? string.Empty;
                var metaKeyword = body.Get("MetaKeyword") ?? string.Empty;
                var metaDescription = body.Get("MetaDescription") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);

                var entity = new SEOMetaTagsEntity
                {
                    Id = id,
                    Name = name,
                    MetaKeyword = metaKeyword,
                    MetaDescription = metaDescription,
                    ZoneId = zoneId
                };

                if (id > 0)
                {
                    var result = new SeoServices().SEOMetaTagsUpdate(entity);
                    return this.ToJson(result);
                }
                else {
                    var result = new SeoServices().SEOMetaTagsInsert(entity);
                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("seo_metatopic_getdetail")]
        public HttpResponseMessage GetSEOMetaTopicDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var data = new SeoServices().GetSeoMetaTagsById(id);

                return this.ToJson(WcfActionResponse<SEOMetaTagsEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("seo_metatopic_delete")]
        public HttpResponseMessage SEOMetaTopicDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new SeoServices().SEOMetaTagsDelete(id);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion
    }
}
