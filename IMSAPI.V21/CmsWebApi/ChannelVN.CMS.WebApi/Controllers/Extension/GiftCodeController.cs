﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ExcelUtility = ExcelUtilities.Library.Excel;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/giftcode")]
    public class GiftCodeController : ApiController
    {
        [HttpPost, Route("export_gift_code")]
        public HttpResponseMessage ExportGiftCode(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                long newsId = 0;
                long.TryParse(body.Get("NewsId"), out newsId);
                if (newsId == 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy NewsId này."));
                }

                var userName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var tempExcel = HttpContext.Current.Server.MapPath(CmsChannelConfiguration.GetAppSetting("GiftCodeFile"));                
                var rowConfig = HttpContext.Current.Server.MapPath(CmsChannelConfiguration.GetAppSetting("GiftCodeConfig"));
                var downloadDir = CmsChannelConfiguration.GetAppSetting("DownloadDir");

                var fileName = string.Format("{0}_{1}.xlsx", userName, string.Format("{0:yyyymmddHHmmssFFF}", DateTime.Now));
                var filePath = HttpContext.Current.Server.MapPath(string.Format("{0}/{1}", downloadDir, fileName));
                var downloadPath = string.Format("{0}/{1}", downloadDir, fileName);
                
                var newsList = new GiftCodeServices().GetByNewsId(newsId);                
                var count = newsList.Count;
                var excel = new ExcelUtility(tempExcel, filePath, rowConfig);
                var ws = excel.Worksheet(1);                
                excel.SetHeader(ws, excel.RowConfig.Header);
                if (newsList != null && newsList.Count > 0)
                {
                    excel.DataBind(ws, newsList);
                }
                excel.Save();
                //upload storage
                downloadPath = UploadFileStorage(userName, filePath);

                File.Delete(filePath);

                return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD") + "Temp/" + downloadPath, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        private string UploadFileStorage(string username, string filePath)
        {
            var fileNameUpload = Path.GetFileName(filePath);
            var directoryName = Path.GetDirectoryName(filePath);
            var path = directoryName.Replace(HttpContext.Current.Server.MapPath("~/Data/"), "").Replace("\\", "/") + "/";            
            var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(username, path, fileNameUpload, false);
            var policy = policyData[1];
            var signature = policyData[2];            
            try
            {
                var result = Services.ExternalServices.FileStorageServices.UploadFile(policy, signature, fileNameUpload, "data:application/x-rar-compressed;base64," + Serialize(filePath));
                if (result)
                {
                    return fileNameUpload;
                }
                return fileNameUpload;
            }
            catch (Exception)
            {
                return fileNameUpload;
            }
        }
        private static string Serialize(string fileName)
        {
            using (FileStream reader = new FileStream(fileName, FileMode.Open))
            {
                byte[] buffer = new byte[reader.Length];
                reader.Read(buffer, 0, (int)reader.Length);
                return Convert.ToBase64String(buffer);
            }
        }

        [HttpPost, Route("insert_gamegiftcode")]
        public HttpResponseMessage InsertGameGiftCode()
        {
            var httpRequest = HttpContext.Current.Request;
            string path = string.Empty;
            try
            {                              
                if (httpRequest.Files.Count > 0)
                {                                        
                    long newsId = 0;
                    long.TryParse(httpRequest.Params["NewsId"], out newsId);
                    if (newsId == 0)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy NewsId này."));
                    }                    
                    var tags = httpRequest.Params["Tags"]??string.Empty;
                    HttpPostedFile file = httpRequest.Files[0];
                    path = HttpContext.Current.Server.MapPath("~/Data/Temp/") + file.FileName;                    
                    file.SaveAs(path);

                    if (File.Exists(path))
                    {
                        using (var fileStream = File.Open(path, FileMode.Open, FileAccess.Read))
                        {
                            IExcelDataReader dataReader = null;
                            if (file.FileName.EndsWith(".xls"))
                            {
                                dataReader = ExcelReaderFactory.CreateBinaryReader(fileStream);
                            }
                            else if (file.FileName.EndsWith(".xlsx"))
                            {
                                dataReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);
                            }

                            dataReader.IsFirstRowAsColumnNames = true;
                            var data = dataReader.AsDataSet().Tables[0];

                            string giftCode = "";
                            for (var i = 0; i < data.Rows.Count; i++)
                            {
                                var row = data.Rows[i];
                                giftCode += row[0] + ";";
                            }
                            var responseData = new GenkServices().InsertGiftCode(newsId, giftCode, tags);                                
                                           
                            File.Delete(path);

                            return this.ToJson(responseData);
                        }
                    }

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy đường đẫn file."));
                }
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy file."));
            }
            catch (Exception ex)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_giftcode")]
        public HttpResponseMessage GetListNewsGiftCode(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var keyWord = body.Get("Keyword")?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("Zone"), 0);
                //var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;                                              
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 0);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 0);
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("From"), "dd/MM/yyyy HH:mm");
                var todate = Utility.ConvertToDateTimeByFormat(body.Get("To"), "dd/MM/yyyy HH:mm");

                var totalRows = 0;
                var listNews = new List<NewsInListEntity>();
                if (!string.IsNullOrEmpty(keyWord) && keyWord.StartsWith("http://"))
                {
                    var arrLink = keyWord.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                    var lastNewsId = arrLink[arrLink.Length - 1].Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                    var newsId = long.Parse(lastNewsId[0]);
                    var news = new NewsServices().GetDetail(newsId, string.Empty);
                    if (news.NewsInfo != null)
                    {
                        listNews = new List<NewsInListEntity>() { new NewsInListEntity() { EncryptId = news.NewsInfo.EncryptId, Status = news.NewsInfo.Status, Type = news.NewsInfo.Type, IsOnHome = news.NewsInfo.IsOnHome, DistributionDate = news.NewsInfo.DistributionDate, Title = news.NewsInfo.Title, Note = news.NewsInfo.Note, ZoneId = news.NewsInfo.ZoneId, Avatar = news.NewsInfo.Avatar, Source = news.NewsInfo.Source, CreatedBy = news.NewsInfo.CreatedBy, PublishedBy = news.NewsInfo.PublishedBy } };
                        totalRows = 1;
                    }
                    else
                    {
                        totalRows = 0;
                    }
                }
                else
                {
                    listNews = new NewsServices().SearchNewsForNewsPosition(zoneId, keyWord, -1, -1, fromDate, todate, pageIndex, pageSize, "", (int)NewsType2.GiftCode, ref totalRows);
                    //listNews = new NewsServices().SearchNews(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
                }
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, News = listNews }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("stat_gift_code")]
        public HttpResponseMessage StatsGameGiftCode(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                long newsId = 0;
                long.TryParse(body.Get("NewsId"), out newsId);
                if (newsId == 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy NewsId này."));
                }
                                
                var stats = new GenkServices().GetStartGiftCodeByNewsId(newsId);
                
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(stats, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
    }
}
