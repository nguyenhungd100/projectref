﻿using ChannelVN.BrandContent.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/brandcontent")]
    public class BrandContentController : ApiController
    {
        #region BrandContent

        [HttpPost, Route("search_brandcontent")]
        public HttpResponseMessage SearchBrandContent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRow = 0;
                var data = new BrandContentServices().BrandContent_Search(keyword, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_brandcontent_by_id")]
        public HttpResponseMessage GetBrandContentById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var brandcontent = new BrandContentServices().BrandContent_GetById(id);
                if (brandcontent == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy brandcontent"));
                }

                return this.ToJson(WcfActionResponse<BrandContentEntity>.CreateSuccessResponse(brandcontent, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save_brandcontent")]
        public HttpResponseMessage SaveBrandContent(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);                
                var brandName = body.Get("BrandName") ?? string.Empty;
                var brandUrl = body.Get("BrandUrl") ?? string.Empty;               
                var icon = body.Get("Icon") ?? string.Empty;
                var logo = body.Get("Logo") ?? string.Empty;
                var logoStream = body.Get("LogoStream") ?? string.Empty;

                var brandcontentEntity = new BrandContentEntity
                {
                    BrandId = id,
                    BrandName = brandName,
                    BrandUrl = brandUrl,
                    Icon = icon,                    
                    Logo = logo,
                    LogoStream = logoStream
                };


                if (id > 0)
                {                    
                    var result = new BrandContentServices().BrandContent_Update(brandcontentEntity);
                    return this.ToJson(result);
                }
                else {
                    var newsBrandContentId = 0;
                    var result = new BrandContentServices().BrandContent_Insert(brandcontentEntity);
                    result.Data = newsBrandContentId.ToString();
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("brandcontent_delete")]
        public HttpResponseMessage BrandContentDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var brandcontentId = Utility.ConvertToInt(body.Get("Id"));

                var result = new BrandContentServices().BrandContent_Delete(brandcontentId);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        #endregion
    }
}
