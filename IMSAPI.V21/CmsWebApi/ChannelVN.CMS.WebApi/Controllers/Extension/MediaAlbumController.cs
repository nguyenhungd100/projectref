﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Net.Http;
using System.Web.Http;
using ChannelVN.CMS.Entity.Base.MediaAlbum;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/mediaalbum")]
    public class MediaAlbumController : ApiController
    {
        #region MediaAlbum

        [HttpPost, Route("search_album")]
        public HttpResponseMessage SearchMediaAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword")??string.Empty;
                var zoneId= Utility.ConvertToInt(body.Get("ZoneId"),-1);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 50);
                
                var totalRow = 0;
                var data = new MediaAlbumServices().SearchMediaAlbum(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_album_by_id")]
        public HttpResponseMessage GetMediaAlbumForEditById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new MediaAlbumServices().GetMediaAlbumByAlbumId(id);
                if (data == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not found data"));
                }

                return this.ToJson(WcfActionResponse<MediaAlbumEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save_album")]
        public HttpResponseMessage SaveMediaAlbum(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"),0);
                var name = body.Get("Name") ?? string.Empty;
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not empty Name."));
                }         
                var avatar = body.Get("Avatar") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"), false);

                var objMedia = new MediaAlbumEntity
                {
                    Id = id,
                    ZoneId = zoneId,
                    Name = name,
                    Avatar = avatar,
                    Status = status,
                    IsOnHome = isOnHome,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now,
                    //StartDate = DateTime.Now,
                    //ExpireDate = DateTime.Now.AddDays(1)
                }; 
                               
                if (id > 0)
                {                    
                    var result = new MediaAlbumServices().UpdateMediaAlbum(objMedia);
                    return this.ToJson(result);
                }
                else {                    
                    var result = new MediaAlbumServices().CreateMediaAlbum(objMedia);                    
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete_album")]
        public HttpResponseMessage MediaAlbumDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new MediaAlbumServices().DeleteMediaAlbum(id);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_status_album")]
        public HttpResponseMessage UpdateStatusMediaAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var action = body.Get("Action");

                var result = new MediaAlbumServices().UpdateStatusMediaAlbum(id, action);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region MediaAlbumDetail

        [HttpPost, Route("list_album_detail_by_album_id")]
        public HttpResponseMessage ListMediaAlbumDetailByAlbumId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("AlbumId"), 0);

                var data = new MediaAlbumServices().GetMediaAlbumForEditByMediaAlbumId(id,0);

                return this.ToJson(WcfActionResponse<MediaAlbumForEditEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_album_detail")]
        public HttpResponseMessage SaveMediaAlbumDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var albumId = Utility.ConvertToInt(body.Get("AlbumId"), 0);
                var title = body.Get("Title") ?? string.Empty;
                if (string.IsNullOrEmpty(title))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not empty Title."));
                }
                var unsignName = Utility.UnicodeToKoDau(title);
                var description = body.Get("Description") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var mediaType = Utility.ConvertToInt(body.Get("MediaType"), 0);
                var mediaContent = body.Get("MediaContent") ?? string.Empty;
                var author = body.Get("Author") ?? string.Empty;
                var tags = body.Get("Tags") ?? string.Empty;
                var distributionDate = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm");
                var isHot = Utility.ConvertToBoolean(body.Get("IsHot"), false);
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var avatar = body.Get("Avatar") ?? string.Empty;
                var url = body.Get("Url") ?? string.Empty;
                if (string.IsNullOrEmpty(url))
                {
                    url = Utility.UnicodeToKoDauAndGach(title);
                }

                var objMedia = new MediaAlbumDetailEntity
                {
                    Id = id,
                    AlbumId = albumId,
                    Title = title,
                    UnSignName = unsignName,
                    Description = description,
                    Status = status,
                    ZoneId = zoneId,
                    MediaType = mediaType,
                    MediaContent = mediaContent,
                    Author = author,
                    Tags = tags,
                    DistributionDate = distributionDate,
                    DistributionBy = accountName,
                    IsHot = isHot,
                    NewsId = newsId,
                    Avatar = avatar,
                    Url = url,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = accountName,
                    ModifiedDate = DateTime.Now
                };

                if (id > 0)
                {
                    var result = new MediaAlbumServices().UpdateMediaAlbumDetail(objMedia);
                    return this.ToJson(result);
                }
                else {                    
                    var result = new MediaAlbumServices().CreateMediaAlbumDetail(objMedia);                    
                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_album_detail_by_id")]
        public HttpResponseMessage GetMediaAlbumDetailById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new MediaAlbumServices().GetMediaAlbumDetailForEditById(id);
                
                return this.ToJson(WcfActionResponse<MediaAlbumDetailEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_album_detail")]
        public HttpResponseMessage MediaAlbumDetailDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));

                var result = new MediaAlbumServices().DeleteMediaAlbumDetail(id);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        //[HttpPost, Route("update_status_album_detail")]
        //public HttpResponseMessage UpdateStatusMediaAlbumDetail(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        var body = request.Content.ReadAsFormDataAsync().Result;

        //        var id = Utility.ConvertToInt(body.Get("Id"));
        //        var action = body.Get("Action");

        //        var result = new MediaAlbumServices().UpdateStatusMediaAlbumDetail(id, action);
        //        return this.ToJson(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
        //    }
        //}
        #endregion

        [HttpPost, Route("save_muil_album_detail")]
        public HttpResponseMessage SaveMuilMediaAlbumDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;                

                var eAlbum = body.Get("MediaAlbum") ?? "";
                if (string.IsNullOrEmpty(eAlbum))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not empty MediaAlbum."));
                }
                var listMediaAlbumDetail = body.Get("ListMediaAlbumDetail") ?? "";
                
                var oAlbum = NewtonJson.Deserialize<MediaAlbumEntity>(eAlbum);
                if (oAlbum != null)
                {
                    oAlbum.CreatedBy = accountName;
                    oAlbum.CreatedDate = DateTime.Now;
                    oAlbum.LastModifiedBy = accountName;
                    oAlbum.LastModifiedDate = DateTime.Now;
                }

                var listAlbumDetail = NewtonJson.Deserialize<List<MediaAlbumDetailEntity>>(listMediaAlbumDetail);
                if (listAlbumDetail==null)
                {
                    listAlbumDetail=new List<MediaAlbumDetailEntity>();
                }

                var objMedia = new MediaAlbumForEditEntity
                {
                    MediaAlbum = oAlbum,
                    ListMediaAlbumDetail = listAlbumDetail
                };

                var listDeleteItem = body.Get("ListDeleteItem") ?? "";
                if (!string.IsNullOrEmpty(listDeleteItem))
                {
                    var listId = listDeleteItem.Split(',');
                    foreach (var item in listId)
                    {
                        var id = Utility.ConvertToLong(item);
                        new MediaAlbumServices().DeleteMediaAlbumDetail(id);
                    }
                }

                if (oAlbum != null)
                {
                    if (oAlbum.Id > 0)
                    {
                        var result = new MediaAlbumServices().UpdateMediaAlbum(oAlbum);
                        if (result.Success)
                        {                            
                            foreach (var MediaAlbumDetail in objMedia.ListMediaAlbumDetail)
                            {
                                MediaAlbumDetail.AlbumId = oAlbum.Id;
                                MediaAlbumDetail.UnSignName = Utility.UnicodeToKoDau(MediaAlbumDetail.Title);
                                MediaAlbumDetail.Url = Utility.UnicodeToKoDauAndGach(MediaAlbumDetail.Title);                                                                
                                MediaAlbumDetail.ModifiedBy = accountName;
                                MediaAlbumDetail.ModifiedDate = DateTime.Now;

                                if (MediaAlbumDetail.Id > 0)
                                {                                    
                                    new MediaAlbumServices().UpdateMediaAlbumDetail(MediaAlbumDetail);                                    
                                }
                                else
                                {
                                    MediaAlbumDetail.CreatedBy = accountName;
                                    MediaAlbumDetail.CreatedDate = DateTime.Now;
                                    new MediaAlbumServices().CreateMediaAlbumDetail(MediaAlbumDetail);                                    
                                }
                            }
                        }
                        return this.ToJson(result);
                    }
                    else {
                        var result = new MediaAlbumServices().CreateMediaAlbum(oAlbum);
                        if (result.Success)
                        {
                            var newMediaAlbumId = Utility.ConvertToInt(result.Data);
                            if (newMediaAlbumId > 0 && objMedia.ListMediaAlbumDetail != null && objMedia.ListMediaAlbumDetail.Count > 0)
                            {                                
                                foreach (var MediaAlbumDetail in objMedia.ListMediaAlbumDetail)
                                {
                                    MediaAlbumDetail.AlbumId = newMediaAlbumId;
                                    MediaAlbumDetail.UnSignName = Utility.UnicodeToKoDau(MediaAlbumDetail.Title);
                                    MediaAlbumDetail.Url = Utility.UnicodeToKoDauAndGach(MediaAlbumDetail.Title);
                                    MediaAlbumDetail.CreatedBy = accountName;
                                    MediaAlbumDetail.CreatedDate = DateTime.Now;
                                    MediaAlbumDetail.ModifiedBy = accountName;
                                    MediaAlbumDetail.ModifiedDate = DateTime.Now;
                                    
                                    new MediaAlbumServices().CreateMediaAlbumDetail(MediaAlbumDetail);
                                }
                            }
                        }
                        return this.ToJson(result);
                    }
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error parser MediaAlbum"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
    }
}
