﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Net.Http;
using System.Web.Http;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/quickanswer")]
    public class QuickAnswerController : ApiController
    {
        #region QuickAnswer EXT

        [HttpPost, Route("search_quickanswer_ext")]
        public HttpResponseMessage SearchQuickAnswerExt(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");                
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRow = 0;
                var data = new QuickAnswerServices().ExternalSearchQuickAnswer(keyword, status, pageIndex, pageSize, ref totalRow);
                
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, QuickAnswers = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_quickanswer_ext_by_id")]
        public HttpResponseMessage GetQuickAnswerExtById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var quickNews = new QuickAnswerServices().ExternalGetQuickAnswerById(id);

                return this.ToJson(WcfActionResponse<QuickAnswerEntity>.CreateSuccessResponse(quickNews, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }                

        [HttpPost, Route("receive_quickanswer")]
        public HttpResponseMessage ReceiveQuickAnswer(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var answerContent = body.Get("AnswerContent") ?? string.Empty;

                var cmsQuickAnswer = new QuickAnswerServices().GetQuickAnswerById(id);
                if (cmsQuickAnswer != null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Câu hỏi này đã được trả lời."));
                }
                var quickAnswer = new QuickAnswerServices().ExternalGetQuickAnswerById(id);
                if (quickAnswer == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy Quick Answer Id: "+id));

                quickAnswer.AnsweredBy = username;
                quickAnswer.AnsweredDate = DateTime.Now;
                quickAnswer.AnswerContent = answerContent;
                if(string.IsNullOrEmpty(answerContent) || string.IsNullOrWhiteSpace(answerContent))
                    quickAnswer.Status = (int)StatusEnum.None;
                quickAnswer.Status = (int)StatusEnum.Received;

                //Chèn vào các bảng QuickAnswer IMS
                var quickAnswerInsertData = new QuickAnswerServices().InsertQuickAnswer(quickAnswer);
                if (quickAnswerInsertData.Success)
                {
                    new QuickAnswerServices().ExternalReceiveQuickAnswer(id, username);                    
                }
                
                return this.ToJson(quickAnswerInsertData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }        

        [HttpPost, Route("delete_quickanswer_ext")]
        public HttpResponseMessage DeleteQuickAnswerExt(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;                                                                     
                
                var data = new QuickAnswerServices().ExternalDeleteQuickAnswer(id, username);
                
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_multi_quickanser_ext")]
        public HttpResponseMessage DeleteMultiQuickAnswerExt(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var idList = body.Get("Ids") ?? "";
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if (string.IsNullOrEmpty(idList))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param Ids không được để trống."));
                }
                var data = new QuickAnswerServices().ExternalDeleteMultiQuickAnswer(idList,username);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        //[HttpPost, Route("count_quickanswer")]
        //public HttpResponseMessage CountQuickAnswer(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        var body = request.Content.ReadAsFormDataAsync().Result;

        //        var quickNewsCount = new List<QuickAnswerCount>();
        //        quickNewsCount.Add(new QuickAnswerCount
        //        {
        //            Count = new QuickAnswerServices().ExternalCountQuickAnswer(1, 0),
        //            Type = 1,
        //            Status = 0
        //        });
        //        quickNewsCount.Add(new QuickAnswerCount
        //        {
        //            Count = new QuickAnswerServices().ExternalCountQuickAnswer(0, 0),
        //            Type = 0,
        //            Status = 0
        //        });

        //        return this.ToJson(WcfActionResponse<List<QuickAnswerCount>>.CreateSuccessResponse(quickNewsCount, "Success"));
        //    }
        //    catch (Exception ex)
        //    {
        //        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
        //    }
        //}

        #endregion

        #region QuickAnswer

        [HttpPost, Route("search_quickanswer")]
        public HttpResponseMessage SearchQuickAnswer(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);

                var totalRow = 0;
                var data = new QuickAnswerServices().SearchQuickAnswer(keyword, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, QuickAnswers = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_quickanswer_by_id")]
        public HttpResponseMessage GetQuickAnswerById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var quickNews = new QuickAnswerServices().GetQuickAnswerById(id);

                return this.ToJson(WcfActionResponse<QuickAnswerEntity>.CreateSuccessResponse(quickNews, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_quickanswer")]
        public HttpResponseMessage UpdateQuickAnswer(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var title = body.Get("Title") ?? string.Empty;
                var questContent = body.Get("QuestContent") ?? string.Empty;
                var answerContent = body.Get("AnswerContent") ?? string.Empty;

                var cmsQuickAnswer = new QuickAnswerServices().GetQuickAnswerById(id);
                if (cmsQuickAnswer != null)
                {
                    cmsQuickAnswer.Title = title;
                    cmsQuickAnswer.QuestContent = questContent;

                    cmsQuickAnswer.ModifiedBy = username;
                    cmsQuickAnswer.ModifiedDate = DateTime.Now;
                    cmsQuickAnswer.AnswerContent = answerContent;                    
                }
                
                //Update vào các bảng QuickAnswer IMS
                var quickAnswerUpdateData = new QuickAnswerServices().UpdateQuickAnswer(cmsQuickAnswer);
                                
                return this.ToJson(quickAnswerUpdateData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_quickanswer")]
        public HttpResponseMessage DeleteQuickAnswer(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new QuickAnswerServices().DeleteQuickAnswer(id, username);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_multi_quickanser")]
        public HttpResponseMessage DeleteMultiQuickAnswer(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var idList = body.Get("Ids") ?? "";
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if (string.IsNullOrEmpty(idList))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param Ids không được để trống."));
                }
                var data = new QuickAnswerServices().DeleteMultiQuickAnswer(idList, username);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }        

        #endregion
    }
}
