﻿using ChannelVN.MagazineTemplate.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/magazinetemplate")]
    public class MagazineTemplateController : ApiController
    {
        #region MagazineTemplate

        [HttpPost, Route("search_magazinetemplate")]
        public HttpResponseMessage SearchMagazineTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var type = Utility.ConvertToInt(body.Get("Type"),0);
                var status = EnumMagazineTemplateStatus.AllStatus;
                var statusInput = Utility.ConvertToInt(body.Get("Status"),-1);
                status = (EnumMagazineTemplateStatus)statusInput;

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRow = 0;
                var data = new MagazineTemplateServices().MagazineTemplateSearch(keyword, type, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_magazinetemplate_by_id")]
        public HttpResponseMessage GetMagazineTemplateById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var magazine = new MagazineTemplateServices().GetMagazineTemplateById(id);
                if (magazine == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("MagazineTemplate is empty."));
                }

                return this.ToJson(WcfActionResponse<MagazineTemplateEntity>.CreateSuccessResponse(magazine, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save_magazinetemplate")]
        public HttpResponseMessage SaveMagazineTemplate(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var title = body.Get("Title") ?? string.Empty;
                var avatar = body.Get("Avatar") ?? string.Empty;
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var cover = body.Get("Cover") ?? string.Empty;
                var mobileCover = body.Get("MobileCover") ?? string.Empty;
                var videoCover = body.Get("VideoCover") ?? string.Empty;
                var textLayer = body.Get("TextLayer") ?? string.Empty;
                var backgroundColor = body.Get("BackgroundColor") ?? string.Empty;
                var textColor = body.Get("TextColor") ?? string.Empty;
                var desktopZipUrl = body.Get("DesktopZipUrl") ?? string.Empty;
                var mobileZipUrl = body.Get("MobileZipUrl") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var sapo = body.Get("Sapo") ?? string.Empty;                
                var content = body.Get("Content") ?? string.Empty;

                var magazineEntity = new MagazineTemplateEntity
                {
                    Id = id,
                    Title = title,
                    Avatar = avatar,
                    Type = type,
                    Cover = cover,
                    MobileCover = mobileCover,
                    VideoCover = videoCover,
                    TextLayer= textLayer,
                    BackgroundColor= backgroundColor,
                    TextColor= textColor,
                    DesktopZipUrl= desktopZipUrl,
                    MobileZipUrl= mobileZipUrl,
                    Status = status,
                    Sapo= sapo,
                    Content= content,
                    CreatedBy =accountName,
                    CreatedDate=DateTime.Now,
                    ModifiedBy= accountName,
                    ModifiedDate=DateTime.Now                    
                };


                if (id > 0)
                {                    
                    var result = new MagazineTemplateServices().UpdateMagazineTemplate(magazineEntity);
                    return this.ToJson(result);
                }
                else {
                    var newsMagazineTemplateId = 0;
                    var result = new MagazineTemplateServices().InsertMagazineTemplate(magazineEntity, ref newsMagazineTemplateId);
                    result.Data = newsMagazineTemplateId.ToString();
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("magazinetemplate_delete")]
        public HttpResponseMessage MagazineTemplateDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var magazineId = Utility.ConvertToInt(body.Get("Id"));

                var result = new MagazineTemplateServices().DeleteMagazineTemplate(magazineId);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        #endregion
    }
}
