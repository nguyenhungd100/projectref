﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.AlertPerson;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/crawleralert")]
    public class CrawlerAlertController : ApiController
    {
        #region CrawlerAlert

        [HttpPost, Route("list")]
        public HttpResponseMessage Search(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var lstAlertNews = CrawlerAlertServices.GetAlert();

                var lstLastNews = new List<NewsInListEntity>();
                if (lstAlertNews!=null && lstAlertNews.Count <= 0)
                {
                    return this.ToJson(WcfActionResponse<List<NewsInListEntity>>.CreateSuccessResponse(lstLastNews, "Success"));
                }
                string lstNewsId = string.Join(";", lstAlertNews.Select(x => x.NewsID).Distinct());
                var array = lstNewsId.Split(';');
                if (array.Length>0) {
                    foreach (var item in array)
                    {
                        var news = new NewsServices().GetDetail(Utility.ConvertToLong(item), string.Empty);
                        if (news != null && news.NewsInfo != null)
                        {
                            var newsAlert = lstAlertNews.FirstOrDefault(x => x.NewsID == news.NewsInfo.Id);
                            lstLastNews.Add(new NewsInListEntity()
                            {
                                EncryptId = news.NewsInfo.Id.ToString(),
                                Avatar = news.NewsInfo.Avatar,
                                Title = news.NewsInfo.Title,
                                DistributionDate = news.NewsInfo.DistributionDate,
                                Id = news.NewsInfo.Id,
                                OriginalId = news.NewsInfo.OriginalId,
                                Source = news.NewsInfo.Source,
                                Status = news.NewsInfo.Status,
                                CreatedBy = news.NewsInfo.CreatedBy,
                                PublishedBy = news.NewsInfo.PublishedBy,
                                Note = newsAlert.NewsNote,
                                LastModifiedBy = news.NewsInfo.LastModifiedBy,
                                Url = newsAlert != null ? newsAlert.newsUrl : "#"
                            });
                        }
                    }
                }
                
                return this.ToJson(WcfActionResponse<List<NewsInListEntity>>.CreateSuccessResponse(lstLastNews, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
       
        [HttpPost, Route("add_news_crawl_monitor")]
        public HttpResponseMessage AddNewsCrawlMonitor(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;

                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowCrawlerAlert") == 1)
                {
                    var title = body.Get("Title")?? "";
                    var id = Utility.ConvertToLong(body.Get("Id"));
                    var url = body.Get("Url")?? "";
                    title = HttpUtility.UrlEncode(title);
                    
                    var task = Task.Run(() => CrawlerAlertServices.AddMonitor(id, title, url));
                    try
                    {
                        if (task.Wait(TimeSpan.FromSeconds(5)))
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                    return this.ToJson(WcfActionResponse.CreateSuccessResponse(id.ToString(),""));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: AllowCrawlerAlert = 0  not add."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("unpublish")]
        public HttpResponseMessage Unpublish(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var listNewsId = body.Get("Newsid")?? "";
                
                var listNewsIdUpdated = "";
                var responseData = WcfActionResponse.CreateErrorResponse("Action này không được hỗ trợ.");
                try
                {
                    var newsIds = listNewsId.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                    {
                        responseData = new NewsServices().Unpublish(id, username);
                        if (responseData.Success)
                        {
                            CrawlerAlertServices.RemoveAlert(Utility.ConvertToLong(id));
                            listNewsIdUpdated += ";" + id;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                }
                //Logger.WriteLog(Logger.LogType.Trace, "Unpublished ListNewsId=" + listNewsIdUpdated);
                if (!string.IsNullOrEmpty(listNewsIdUpdated))
                {
                    responseData= WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                }
                
                return this.ToJson(responseData);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        #endregion
    }
}
