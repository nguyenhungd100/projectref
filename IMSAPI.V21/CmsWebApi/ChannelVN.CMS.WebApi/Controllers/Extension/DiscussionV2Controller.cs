﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/discussionv2")]
    public class DiscussionV2Controller : ApiController
    {
        [HttpPost, Route("get_list_discussion_topic")]
        public HttpResponseMessage GetListDiscussionTopic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = 1;
                int.TryParse(body.Get("pageIndex"),out pageIndex);
                var pageSize = 50;
                int.TryParse(body.Get("pageSize"), out pageSize);
                var type = -1;
                int.TryParse(body.Get("type"), out type);
                var status = 1;
                int.TryParse(body.Get("status"), out status);
                var bstatus = status == 1;                              
                var me = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var totalRow = 0;
                var data = new DiscussionV2Services().GetListDiscussionTopic(keyword, type, bstatus, me, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        } 
    }
}
