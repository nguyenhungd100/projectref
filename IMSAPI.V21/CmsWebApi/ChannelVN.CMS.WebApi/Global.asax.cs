﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Caching;
using System;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
        {
            try {
                HttpContext.Current?.Response?.Headers?.Remove("X-Powered-By");
                HttpContext.Current?.Response?.Headers?.Remove("X-AspNet-Version");
                HttpContext.Current?.Response?.Headers?.Remove("X-AspNetMvc-Version");
                HttpContext.Current?.Response?.Headers?.Remove("X-Powered-By-Plesk");
                HttpContext.Current?.Response?.Headers?.Remove("Server");                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Application_PreSendRequestHeaders =>" + ex.Message);
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            //WebBackgrounderSetup.Shutdown();
            ////QueueJobHost.Stop();
        }
    }
}
