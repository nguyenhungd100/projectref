﻿using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using Thinktecture.IdentityModel.Tokens;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.WebApi.Identity
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string _audience = "Any";

        private readonly byte[] _secret = new byte[]{ };
        private readonly string _issuer;
        private readonly TokenValidationParameters _validationParameters;

        public CustomJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public CustomJwtFormat(string issuer, byte[] secret)
            : this(issuer)
        {
            _secret = secret;
        }

        public CustomJwtFormat(string issuer, TokenValidationParameters validationParameters)
            : this(issuer)
        {
            _validationParameters = validationParameters;
        }

        public CustomJwtFormat(string issuer, byte[] secret, TokenValidationParameters validationParameters)
            : this(issuer)
        {
            _secret = secret;
            _validationParameters = validationParameters;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var signingKey = new HmacSigningCredentials(_secret);
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            return new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(
                _issuer,
                _audience,
                data.Identity.Claims,
                issued.Value.UtcDateTime,
                expires.Value.UtcDateTime,
                signingKey
            ));
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            ClaimsPrincipal principal = null;
            SecurityToken validToken = null;
            try
            {
                principal = new JwtSecurityTokenHandler().ValidateToken(protectedText, _validationParameters, out validToken);

                var validJwt = validToken as JwtSecurityToken;
                if (validJwt == null)
                {
                    throw new ArgumentException("ValidJwt Invalid JWT => protectedText: " + protectedText);
                }

                //chinhnb tam comment check ip
                //var clientIp = System.Web.HttpContext.Current.Request.UserHostAddress;
                //var isValid = false;
                //var sub = "";
                //if (!string.IsNullOrEmpty(clientIp))
                //{
                //    var claim = validJwt.Claims.FirstOrDefault(c => c.Type == "sub");                    

                //    if (claim != null)
                //    {
                //        sub = Crypton.Decrypt(claim.Value);
                //    }

                //    if (!string.IsNullOrEmpty(sub) && clientIp.Equals(sub))
                //    {
                //        isValid = true;
                //    }
                //}

                //if (!isValid)
                //{
                //    throw new ArgumentException("IsValid Invalid JWT => clientIp: "+ clientIp+ "; sub: "+sub);
                //}
            }
            catch (SecurityTokenValidationException ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, "ValidateToken: SecurityTokenValidationException =>" + ex.Message);
                //return null;
                throw new SecurityTokenValidationException(ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, "ValidateToken: ArgumentNullException =>" + ex.Message);
                //return null;
                throw new ArgumentNullException(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, "ValidateToken: ArgumentException =>" + ex.Message);
                //return null;
                throw new ArgumentException(ex.Message);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, "ValidateToken: Exception =>" + ex.Message);
                //return null;
                throw new Exception(ex.Message);
            }

            // Validation passed. Return a valid AuthenticationTicket.
            foreach (var identity in principal.Identities)
            {
                return new AuthenticationTicket(identity, new AuthenticationProperties());
            }

            return null;
        }
    }
}