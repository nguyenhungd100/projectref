﻿using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;

namespace ChannelVN.CMS.WebApi.Identity
{
    public class CustomOAuthBearerAuthenticationProvider : OAuthBearerAuthenticationProvider
    {
        private const string DEFAULT_ACCESS_TOKEN_KEY= "Authorization";

        private readonly string _name = DEFAULT_ACCESS_TOKEN_KEY;

        public CustomOAuthBearerAuthenticationProvider(string name = null)
        {
            if (!string.IsNullOrEmpty(name))
            {
                _name = name;
            }
        }

        public override Task RequestToken(OAuthRequestTokenContext context)
        {
            var value = context.Request.Headers.Get(_name);

            // [FromURI]
            if (string.IsNullOrEmpty(value))
            {
                value = context.Request.Query.Get(_name);
            }

            // To clean the "Bearer" schema
            if (!string.IsNullOrEmpty(value) && value.ToLower().Contains("bearer"))
            {
                value = value.Trim().Substring(6).Trim();
            }

            if (!string.IsNullOrEmpty(value))
            {
                context.Token = value;
            }
            else // Custom the authorization in base Web API.
            {
                value = context.Request.Headers[DEFAULT_ACCESS_TOKEN_KEY];

                if (!string.IsNullOrEmpty(value) && !value.ToLower().Contains("bearer"))
                {
                    context.Request.Headers[DEFAULT_ACCESS_TOKEN_KEY] = $"Bearer {value}";
                    context.Token = value;
                }
            }
            return Task.FromResult<object>(null);
        }
    }
}