﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using ChannelVN.CMS.Common;
using System.Linq;
using System.Configuration;

namespace ChannelVN.CMS.WebApi.Identity
{
    public class CustomOAuthAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var allowOrigin = new string[] { "*" };
            //var configAllowOrigin = ConfigurationManager.AppSettings["AllowedDomains"]?.ToString();
            //if (!string.IsNullOrEmpty(configAllowOrigin))
            //{
            //    allowOrigin = configAllowOrigin.Split(',');
            //}            
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", allowOrigin);            

            return Task.Factory.StartNew(() => {                
                var username = context.UserName;
                var password = context.Password;

                var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (string.IsNullOrEmpty(password) || !apiPasscodes.Contains(password)) // !password.Equals(WcfExtensions.AppConfigs.CmsApiPasscode))
                {
                    context.Rejected();
                    context.SetError("invalid_secret", "The secret key are invalid");
                }
                else
                {                                        
                    var nspParams = context.Request.ReadFormAsync().Result;

                    var nsp = WcfExtensions.AppConfigs.CmsApiNamespace;
                    if (!string.IsNullOrEmpty(nspParams.Get("namespace")))
                    {
                        nsp = nspParams.Get("namespace");
                    }

                    var lang = string.Empty;
                    if (!string.IsNullOrEmpty(nspParams.Get("lang")))
                    {
                        lang = nspParams.Get("lang");
                    }

                    //var user = new SecurityServices().GetUserByUsername(username);
                    //if (user == null)
                    //{
                    //    context.Rejected();
                    //    context.SetError("invalid_username", "Tài khoản này không tồn tại.");
                    //}

                    var claims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Name, Common.Crypton.Encrypt(username)),
                        //new Claim("uid", Common.Crypton.Encrypt(user.Id.ToString())),
                        //new Claim("uisrole", Common.Crypton.Encrypt(user.IsRole.ToString())),                            
                        new Claim("sub", Crypton.Encrypt(context.Request.RemoteIpAddress)),
                        new Claim("nsp", Crypton.Encrypt(nsp)),
                        new Claim("lang", Crypton.Encrypt(lang))
                    };
                    var identity = new ClaimsIdentity(claims, Startup.OAuthAuthorizationOptions.AuthenticationType);

                    var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                    context.Validated(ticket);
                    
                }
            });
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
            {
                context.Validated();
            }
            return Task.FromResult<object>(null);
        }
    }
}