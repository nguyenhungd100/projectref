﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Extensions
{
    public static class ControllerExtension
    {
        public static HttpResponseMessage ToJson<T>(this ApiController self, T data)
        {
            if(data == null)
            {
                return new HttpResponseMessage();
            }
            if(data is string)
            {
                return new HttpResponseMessage
                {
                    Content = new StringContent(data as string, Encoding.UTF8, "application/json")
                };
            }
            return new HttpResponseMessage
            {
                Content = new StringContent(
                    Newtonsoft.Json.JsonConvert.SerializeObject(data, new Newtonsoft.Json.JsonSerializerSettings()
                    {
                        DateFormatString = "yyyy-MM-ddTHH:mm:ssZ"                        
                    })
                    , Encoding.UTF8, "application/json")
            };
        }
        public static T DeJson<T>(this ApiController self, string data)
        {
            if(data == null)
            {
                return default(T);
            }
            if(data is string)
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);
            }
            return default(T);
        }

        public static TResult ToObject<TResult>(this ApiController self, HttpRequestMessage request) where TResult : class, new()
        {
            var message = request.Content.ReadAsFormDataAsync().Result;
            return ToObject<TResult>(self, message);
        }

        public static TResult ToObject<TResult>(this ApiController self, System.Collections.Specialized.NameValueCollection message) where TResult : class, new()
        {
            if(message != null)
            {
                return default(TResult);
            }

            return default(TResult);
        }

        public static TResult ToObject<TResult>(this ApiController self, string message) where TResult : class, new()
        {
            if (String.IsNullOrEmpty(message))
            {
                return default(TResult);
            }
            Newtonsoft.Json.Linq.JObject obj = null;
            try
            {
                obj = Newtonsoft.Json.Linq.JObject.Parse(message);
            }
            catch { }
            if (obj != null)
            {
                return obj.ToObject<TResult>();
            }
            return default(TResult);
        }

        public static TResult ToObject<TResult>(this Newtonsoft.Json.Linq.JObject obj) where TResult : class, new()
        {
            //create instance of T type object.
            var target = Activator.CreateInstance<TResult>();

            //loop through the properties of the object you want to covert.
            foreach (var pi in target.GetType().GetProperties())
            {
                try
                {
                    //get the value of property and try 
                    //to assign it to the property of TResult type object.
                    var hasValue = obj[pi.Name];

                    if (hasValue != null)
                    {
                        var propertyInfo = target.GetType().GetProperty(pi.Name);
                        var propertyType = Type.GetType(propertyInfo.PropertyType.FullName);

                        if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                        {
                            var value = obj.Value<DateTime>(pi.Name);
                            propertyInfo.SetValue(target, value, null);
                        }
                        else if (propertyType == typeof(string))
                        {
                            var value = obj.Value<string>(pi.Name);
                            propertyInfo.SetValue(target, value, null);
                        }
                        else if (propertyType == typeof(bool) || propertyType == typeof(bool?))
                        {
                            var value = obj.Value<bool>(pi.Name);
                            propertyInfo.SetValue(target, value, null);
                        }
                        else if (propertyType == typeof(int) || propertyType == typeof(int?))
                        {
                            var value = obj.Value<int>(pi.Name);
                            propertyInfo.SetValue(target, value, null);
                        }
                        else if (propertyType == typeof(long) || propertyType == typeof(long?))
                        {
                            var value = obj.Value<long>(pi.Name);
                            propertyInfo.SetValue(target, value, null);
                        }
                        else if (propertyType == typeof(decimal) || propertyType == typeof(decimal?))
                        {
                            var value = obj.Value<decimal>(pi.Name);
                            propertyInfo.SetValue(target, value, null);
                        }
                        else if (propertyType == typeof(double) || propertyType == typeof(double?))
                        {
                            var value = obj.Value<double>(pi.Name);
                            propertyInfo.SetValue(target, value, null);
                        }
                        else if (propertyType == typeof(float) || propertyType == typeof(float?))
                        {
                            var value = obj.Value<float>(pi.Name);
                            propertyInfo.SetValue(target, value, null);
                        }
                    }
                }
                catch { }
            }

            //return the TResult type object.
            return target;
        }
    }
}