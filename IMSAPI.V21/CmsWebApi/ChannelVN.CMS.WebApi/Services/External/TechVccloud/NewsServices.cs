﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.BigStory.Entity;
using System.Net;
using System.IO;
using ChannelVN.CMS.WebApi.Services.Extension;
using Newtonsoft.Json;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.Common.ChannelConfig;
using System.Text;
using System.Collections.Specialized;
using ChannelVN.CMS.BO.Nodejs;

namespace ChannelVN.CMS.WebApi.Services.External.TechVccloud
{
    public class NewsServices : Base.NewsServices
    {
        public override WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions, string action)
        {
            //var body = this.Body;//Request.Content.ReadAsFormDataAsync().Result;

            #region Xử lý News Extension

            //bài phỏng vấn            
            //if (news.InterviewId > 0)
            var interviewId = body.Get("InterviewId") ?? string.Empty;
            if (!string.IsNullOrEmpty(interviewId))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.InterviewId,
                    Value = interviewId
                });
            }

            //bài tường thuật sự kiện
            var rollingNewsId = body.Get("RollingNewsId") ?? string.Empty;
            if (!string.IsNullOrEmpty(rollingNewsId))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.RollingNewsId,
                    Value = rollingNewsId
                });
            }

            //interactive
            var interactiveId = Utility.ConvertToInt(body.Get("InteractiveId"));
            if (interactiveId > 0)
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.LandingPageId,
                    Value = interactiveId.ToString()
                });
            }

            var donateCode = body.Get("DonateCode");
            if (!string.IsNullOrEmpty(donateCode))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.SohaNews_DonateCode,
                    Value = donateCode
                });
            }

            if (!string.IsNullOrEmpty(body.Get("IsShowBlinkText")))
            {
                var isShowBlinkText = Utility.ConvertToInt(body.Get("IsShowBlinkText"));
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.SohaNews_ShowBlinkText,
                    Value = isShowBlinkText + ""
                });
            }

            //google news            
            if (!string.IsNullOrEmpty(body.Get("googlenews")))
            {
                var isGoogleNews = Utility.ConvertToInt(body.Get("googlenews"));
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.IsGoogleNews,
                    Value = isGoogleNews.ToString()
                });
            }

            //facebook            
            if (!string.IsNullOrEmpty(body.Get("FacebookArticle")))
            {
                var isFacebookArticle = Utility.ConvertToInt(body.Get("FacebookArticle"));
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.IsFacebookArticle,
                    Value = isFacebookArticle.ToString()
                });
            }

            #region k14
            // tin toàn cảnh
            //var overViewNewsId = body.Get("OverviewNewsIdList") ?? string.Empty;
            var overViewNewsId = body.Get("OverviewNewsList") ?? string.Empty;
            if (!string.IsNullOrEmpty(overViewNewsId))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.OverviewNewsList,
                    Value = overViewNewsId
                });
            }

            // tin chân bài
            //var footerNewsId = body.Get("FooterNewsIdList") ?? string.Empty;
            var footerNewsId = body.Get("FooterNewsList") ?? string.Empty;
            if (!string.IsNullOrEmpty(footerNewsId))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.FooterContentRelatedNews,
                    Value = footerNewsId
                });
            }

            //hien thi but danh
            var isShowPenname = Utility.ConvertToInt(body.Get("ShowPenname"), 0);
            if (isShowPenname == 1)
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.ShowPenName,
                    Value = isShowPenname.ToString()
                });
            }
            //giu nguyen chieu cao anh
            var isKeepPhotoHeight = Utility.ConvertToInt(body.Get("KeepPhotoHeight"), 0);
            if (isKeepPhotoHeight == 1)
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.KeepPhotoHeight,
                    Value = isKeepPhotoHeight.ToString()
                });
            }

            if (!string.IsNullOrEmpty(body.Get("sensitivenews")))
            {
                var isSensitiveNews = Utility.ConvertToInt(body.Get("sensitivenews"), 0);
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.IsSensitiveNews,
                    Value = isSensitiveNews.ToString()
                });
            }
            #endregion

            var isHot24 = body.Get("nb24");
            if (!string.IsNullOrEmpty(isHot24))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.SohaNews_NB24h,
                    Value = isHot24 == "on" ? "1" : "0"
                });
            }            

            //trực tiếp bóng đá
            var liveMatchId = body.Get("LiveMatchId") ?? string.Empty;
            if (!string.IsNullOrEmpty(liveMatchId))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.LiveMatchId,
                    Value = liveMatchId.ToString()
                });
            }

            if (news.Type == (int)NewsType.LandingPage && interactiveId > 0)
            {
                try
                {
                    var bodyContent = GetLandingPageContent(interactiveId);
                    news.Body = bodyContent;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            //is show avatar
            var showavatar = body.Get("is_show_avatar");
            if (!string.IsNullOrEmpty(showavatar))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {

                    Type = (int)EnumNewsExtensionType.ShowAvatar,
                    Value = showavatar
                });
            }
            //cho phep tu add multi extension
            var multiExtension = body.Get("ExtensionInfo") ?? string.Empty;
            if (!string.IsNullOrEmpty(multiExtension))
            {
                try
                {
                    var data = NewtonJson.Deserialize<List<NewsExtensionEntity>>(multiExtension);
                    if (data != null && data.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            if (!newsExtensions.Exists(s => s.Type == item.Type))
                            {
                                newsExtensions.Add(new NewsExtensionEntity
                                {
                                    Type = item.Type,
                                    Value = item.Value
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            #endregion

            #region Dạng bài đặc biệt

            #region Magazine
            if (news.Type == (int)NewsType.Magazine)
            {
                var magazineType = body.Get("magazine_type");
                if (string.IsNullOrEmpty(magazineType)) { magazineType = "1"; }
                var avatar6 = body.Get("avatar6");

                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.MagazineType,
                    Value = magazineType
                });

                if (magazineType == "1")
                {
                    if (!string.IsNullOrEmpty(avatar6))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {

                            Type = (int)EnumNewsExtensionType.MagazineCoverMobile,
                            Value = avatar6
                        });
                    }

                    //video cover
                    var videocover = body.Get("magazine_video_cover");
                    if (!string.IsNullOrEmpty(videocover))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {

                            Type = (int)EnumNewsExtensionType.MagazineVideoCover,
                            Value = videocover
                        });
                    }

                    //cover text
                    var videotextlayer = body.Get("magazine_video_text_layer");
                    if (!string.IsNullOrEmpty(videotextlayer))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {

                            Type = (int)EnumNewsExtensionType.MagazineVideoCoverText,
                            Value = videotextlayer
                        });
                    }
                }
                else if (magazineType == "2")
                {
                    var magazinWebZipUrl = body.Get("magazine_web_zip_url");
                    var magazinMobileZipUrl = body.Get("magazine_mobile_zip_url");

                    if (!string.IsNullOrEmpty(magazinWebZipUrl))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {
                            Type = (int)EnumNewsExtensionType.MagazineWebUrl,
                            Value = magazinWebZipUrl
                        });

                        news.Body = RequestHtml(magazinWebZipUrl);
                    }

                    if (!string.IsNullOrEmpty(magazinMobileZipUrl))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {
                            Type = (int)EnumNewsExtensionType.MagazineMobileUrl,
                            Value = magazinMobileZipUrl
                        });
                    }
                }
            }
            #endregion
            #region Bài BigStory
            else if (news.Type == (int)NewsType.BigStory)
            {
                var bigStoryId = body.Get("big_story_id");
                var rawItems = body.Get("big_story_items");
                var rawFocusItems = body.Get("big_story_focus_items");
                var bigStory = new BigStoryServices().BigStory_GetById(bigStoryId);

                if (bigStory == null || bigStoryId == "")
                {
                    //Thêm mới BigStory
                    var res = AddBigStory(new NodeJs_BigStoryEntity()
                    {
                        Avatar = "",
                        CreatedBy = news.CreatedBy,
                        CreatedDate = DateTime.Now,
                        Description = news.InitSapo,
                        Title = news.Title,
                        IsFocus = 0,
                        Status = 1,
                    });

                    //Lấy BigStoryId
                    try
                    {
                        if (res != null && res.Success == true)
                        {
                            bigStoryId = res.Data;
                        }
                        else
                        {
                            Logger.WriteLog(Logger.LogType.Error, "BigStory_Insert => Error");
                            throw new Exception();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    //cập nhật BigStory
                    bigStory.Title = news.Title;
                    bigStory.Description = news.InitSapo;
                    bigStory.ModifiedBy = news.LastModifiedBy;
                    bigStory.ModifiedDate = DateTime.Now;
                    UpdateBigStory(bigStory);
                }

                //Liên kết với bài viết qua NewsExtensions
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.BigstoryId,
                    Value = bigStoryId
                });
                //Cập nhật BigStoryItem
                if (!string.IsNullOrEmpty(rawItems))
                    UpdateBigStoryItems(bigStoryId, rawItems, rawFocusItems, news.CreatedBy);
            }
            #endregion
            #region FacebookArticle
            else if (news.Type == (int)NewsType.FacebookArticle)
            {
                var facebookArticleType = GetQueryString.GetPost("FacebookArticleType", "26");
                //Liên kết với bài viết qua NewsExtensions
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)NewsType.FacebookArticle,
                    Value = facebookArticleType
                });
            }
            #endregion
            #region RollingNews             
            else if (news.Type == (int)NewsType.RollingNews)
            {
                //var bodyContent1 = body.Get("rolling_content_editor");
                //var bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent1);
                //news.Body = bodyContent;
                //var _rollingTitle = body.Get("rolling_title");
                //if (string.IsNullOrEmpty(_rollingTitle))
                //{
                //    _rollingTitle = news.Title;
                //}
                //news.Title = _rollingTitle;
                //var _rollingStartDate = Utility.ConvertToDateTime(body.Get("rolling_start_date"));
                //if (_rollingStartDate == DateTime.MinValue)
                //{
                //    _rollingStartDate = DateTime.Now;
                //}
                //var _rollingStatus = Utility.ConvertToInt(body.Get("rolling_status"));
                //var showauthor = Utility.ConvertToBoolean(body.Get("show_author"));
                //var showtime = Utility.ConvertToBoolean(body.Get("show_time"));
                //var showRollingNewsLabel = Utility.ConvertToBoolean(body.Get("show_rolling_news_label"));

                //var rollingNews = new RollingNewsServices().GetRollingNewsByRollingNewsId(rollingNewsId);

                //if (rollingNews == null || string.IsNullOrEmpty(rollingNewsId))
                //{
                //    try
                //    {
                //        //Thêm mới RollingNews
                //        var res = AddRollingNews(new NodeJs_RollingNews()
                //        {
                //            Id = rollingNewsId,
                //            Title = _rollingTitle,
                //            Status = _rollingStatus,
                //            ShowAuthor = showauthor,
                //            ShowTime = showtime,
                //            StartDate = _rollingStartDate,
                //            IsShowRollingNewsLabel = showRollingNewsLabel
                //        });

                //        ////Lấy RollingNewsId
                //        //if (res != null && res.Success == true)
                //        //{
                //        //    news.RollingNewsId = Utility.ConvertToInt(res.Data);
                //        //}
                //        //else
                //        //{
                //        //    Logger.WriteLog(Logger.LogType.Error, "RollingNews_Insert => Error");
                //        //    throw new Exception();
                //        //}
                //    }
                //    catch
                //    {
                //        Logger.WriteLog(Logger.LogType.Error, "RollingNews_Insert => Error");
                //        throw new Exception();
                //    }
                //}
                //else
                //{
                //    //cập nhật RollingNews                  
                //    UpdateRollingNews(new NodeJs_RollingNews()
                //    {
                //        Id = rollingNewsId,
                //        Title = _rollingTitle,
                //        Status = _rollingStatus,
                //        ShowAuthor = showauthor,
                //        ShowTime = showtime,
                //        StartDate = _rollingStartDate,
                //        IsShowRollingNewsLabel = showRollingNewsLabel
                //    });
                //}

                //Liên kết với bài viết qua NewsExtensions
                //newsExtensions.Add(new NewsExtensionEntity
                //{
                //    Type = (int)EnumNewsExtensionType.RollingNewsId,
                //    Value = rollingNewsId
                //});

            }
            #endregion
            #region LiveMatch
            else if (news.Type == (int)NewsType.LiveMatch)
            {
                //var bodyContent1 = body.Get("live_match_content_editor");
                //var bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent1);
                //news.Body = bodyContent;
                //var LogoTeamA = body.Get("logo_team_a");
                //var LogoTeamB = body.Get("logo_team_b");
                //var Referee = body.Get("referee");
                //var scoreOfTeamA = Utility.ConvertToInt(body.Get("score_of_team_a"));
                //var scoreOfTeamB = Utility.ConvertToInt(body.Get("score_of_team_b"));
                //var TimeOfMatchBegin = Utility.ConvertToDateTime(body.Get("time_of_match_begin"));
                //var stadium = body.Get("stadium");
                //var teama = body.Get("team_a");
                //var teamb = body.Get("team_b");
                //var liveMatchTitle = body.Get("live_match_title");
                //var linkLiveTV1 = body.Get("link_live_tv1");
                //var linkLiveTV2 = body.Get("link_live_tv2");
                //var linkLiveTV3 = body.Get("link_live_tv3");
                //var linkLiveTV4 = body.Get("link_live_tv4");
                //var linkLiveTV5 = body.Get("link_live_tv5");
                //var showOnLiveTV = Utility.ConvertToBoolean(body.Get("show_on_live_tv"));

                ////var liveMatchId = body.Get("live_match_id") ?? "";
                //if (!string.IsNullOrEmpty(liveMatchId))
                //{
                //    var match = new LiveMatchServices().GetLiveMatchById(liveMatchId);
                //    match.LogoTeamA = LogoTeamA;
                //    match.LogoTeamB = LogoTeamB;
                //    match.Title = liveMatchTitle;
                //    match.TeamA = teama;
                //    match.TeamB = teamb;
                //    match.Stadium = stadium;
                //    match.Referee = Referee;
                //    match.ScoreOfTeamA = scoreOfTeamA;
                //    match.ScoreOfTeamB = scoreOfTeamB;
                //    match.TimeOfMatchBegin = TimeOfMatchBegin;
                //    match.LinkLiveTV1 = linkLiveTV1;
                //    match.LinkLiveTV2 = linkLiveTV2;
                //    match.LinkLiveTV3 = linkLiveTV3;
                //    match.LinkLiveTV4 = linkLiveTV4;
                //    match.LinkLiveTV5 = linkLiveTV5;
                //    match.ShowOnLiveTV = showOnLiveTV ? "1" : "0";
                //    match.InfomationBeforeMatch = "";//bodyContent;

                //    var data = new LiveMatchServices().UpdateLiveMatch(match);
                //    if (data.Success)
                //    {
                //        try
                //        {
                //            //ChannelVN.CMS.WcfMapping.ExternalCache.CacheManager.RemoveCacheForMatchInfo(liveMatchId, updateLiveMatchInfoAction);
                //        }
                //        catch (Exception ex)
                //        {
                //            Logger.WriteLog(Logger.LogType.Error, ex.Message + ex.StackTrace);
                //        }
                //    }
                //}
                //else
                //{
                //    var match = new NodeJs_LiveMatchEntity
                //    {
                //        Id = liveMatchId,
                //        LogoTeamA = LogoTeamA,
                //        LogoTeamB = LogoTeamB,
                //        Title = liveMatchTitle,
                //        TeamA = teama,
                //        TeamB = teamb,
                //        Stadium = stadium,
                //        Referee = Referee,
                //        ScoreOfTeamA = scoreOfTeamA,
                //        ScoreOfTeamB = scoreOfTeamB,
                //        TimeOfMatchBegin = TimeOfMatchBegin,
                //        LinkLiveTV1 = linkLiveTV1,
                //        LinkLiveTV2 = linkLiveTV2,
                //        LinkLiveTV3 = linkLiveTV3,
                //        LinkLiveTV4 = linkLiveTV4,
                //        LinkLiveTV5 = linkLiveTV5,
                //        ShowOnLiveTV = showOnLiveTV ? "1" : "0",
                //        InfomationBeforeMatch = ""//bodyContent
                //    };
                //    //Lấy liveMatchId
                //    try
                //    {
                //        var data = new LiveMatchServices().InsertLiveMatchV2(match, ref liveMatchId);
                //    }
                //    catch
                //    {
                //        Logger.WriteLog(Logger.LogType.Error, "LiveMatch_Insert => Error");
                //        throw new Exception();
                //    }
                //}
                ////Liên kết với bài viết qua NewsExtensions
                //newsExtensions.Add(new NewsExtensionEntity
                //{
                //    Type = (int)EnumNewsExtensionType.LiveMatchId,
                //    Value = liveMatchId.ToString()
                //});
            }
            #endregion
            #region Interview            
            else if (news.Type == (int)NewsType.Interview)
            {
                //var bodyContent1 = GetQueryString.GetPost("interviewContentEditor", string.Empty);
                //var bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent1);
                //news.Body = bodyContent;
                //var isactive = GetQueryString.GetPost("isactive", false);
                //var isfocus = GetQueryString.GetPost("isfocus", false);
                //var time = GetQueryString.GetPost("interviewtime", DateTime.MaxValue);
                ////var interviewId = news.InterviewId;

                //var interview = new NodeJs_Interview()
                //{
                //    //Id = interviewId,
                //    //Title = news.Title,
                //    //IsActived = isactive,
                //    //IsFocus = isfocus,
                //    //StartDate = time,
                //    //CreatedBy = currentUsername,
                //    //LastModifiedBy = currentUsername
                //    Id = interviewId,
                //    Title = news.Title,
                //    StartDate = time,
                //    IsFocus = isfocus,
                //    IsActived = isactive,
                //    CreatedBy = currentUsername
                //};
                //try
                //{
                //    var data=new WcfActionResponse();
                //    if (string.IsNullOrEmpty(interviewId))
                //    {
                //        data=new InterviewServices().InterviewV3Insert(interview, ref interviewId);
                //    }
                //    else
                //    {
                //        data= new InterviewServices().InterviewV3Update(interview);
                //    }
                //    if (data.Success)
                //    {
                //        try
                //        {
                //            //ChannelVN.CMS.WcfMapping.ExternalCache.CacheManager.RemoveCacheOnUpdateInterview(interviewId);
                //        }
                //        catch (Exception ex)
                //        {
                //            Logger.WriteLog(Logger.LogType.Error, ex.Message + ex.StackTrace);
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{
                //    Logger.WriteLog(Logger.LogType.Error, ex.Message + ex.StackTrace);
                //}

                ////bài phỏng vấn                
                //newsExtensions.Add(new NewsExtensionEntity
                //{
                //    Type = (int)EnumNewsExtensionType.InterviewId,
                //    Value = interviewId.ToString()
                //});
            }
            #endregion

            #endregion

            return base.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, authorList, newsChildOrder, sourceId, newsExtensions, action);
        }

        public override WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions, bool isNotify = false)
        {
            //var body = Request.Content.ReadAsFormDataAsync().Result;                     

            #region Xử lý News Extension

            //bài phỏng vấn            
            //if (news.InterviewId > 0)
            var interviewId = body.Get("InterviewId") ?? string.Empty;
            if (!string.IsNullOrEmpty(interviewId))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.InterviewId,
                    Value = interviewId
                });
            }

            //bài tường thuật sự kiện
            var rollingNewsId = body.Get("RollingNewsId") ?? string.Empty;
            if (!string.IsNullOrEmpty(rollingNewsId))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.RollingNewsId,
                    Value = rollingNewsId
                });
            }

            //interactive
            var interactiveId = Utility.ConvertToInt(body.Get("InteractiveId"));
            if (interactiveId > 0)
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.LandingPageId,
                    Value = interactiveId.ToString()
                });
            }

            var donateCode = body.Get("DonateCode");
            if (!string.IsNullOrEmpty(donateCode))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.SohaNews_DonateCode,
                    Value = donateCode
                });
            }

            if (!string.IsNullOrEmpty(body.Get("IsShowBlinkText")))
            {
                var isShowBlinkText = Utility.ConvertToInt(body.Get("IsShowBlinkText"));
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.SohaNews_ShowBlinkText,
                    Value = isShowBlinkText + ""
                });
            }

            //google news            
            if (!string.IsNullOrEmpty(body.Get("googlenews")))
            {
                var isGoogleNews = Utility.ConvertToInt(body.Get("googlenews"));
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.IsGoogleNews,
                    Value = isGoogleNews.ToString()
                });
            }

            //facebook            
            if (!string.IsNullOrEmpty(body.Get("FacebookArticle")))
            {
                var isFacebookArticle = Utility.ConvertToInt(body.Get("FacebookArticle"));
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.IsFacebookArticle,
                    Value = isFacebookArticle.ToString()
                });
            }

            var isSensitiveNews = Utility.ConvertToInt(body.Get("sensitivenews"),0);
            if (isSensitiveNews > 0)
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.IsSensitiveNews,
                    Value = isSensitiveNews.ToString()
                });
            }

            var isHot24 = body.Get("nb24");
            if (!string.IsNullOrEmpty(isHot24))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.SohaNews_NB24h,
                    Value = isHot24 == "on" ? "1" : "0"
                });
            }

            var newsRecommendIdList = body.Get("SpecialNewsRelationIdList");
            if (!string.IsNullOrEmpty(newsRecommendIdList))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.SohaNews_RecommendNews,
                    Value = newsRecommendIdList
                });
            }

            //trực tiếp bóng đá
            var liveMatchId = body.Get("LiveMatchId");
            if (!string.IsNullOrEmpty(liveMatchId))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.LiveMatchId,
                    Value = liveMatchId.ToString()
                });
            }

            if (news.Type == (int)NewsType.LandingPage && interactiveId > 0)
            {
                try
                {
                    var bodyContent = GetLandingPageContent(interactiveId);
                    news.Body = bodyContent;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            //is show avatar
            var showavatar = body.Get("is_show_avatar");
            if (!string.IsNullOrEmpty(showavatar))
            {
                newsExtensions.Add(new NewsExtensionEntity
                {

                    Type = (int)EnumNewsExtensionType.ShowAvatar,
                    Value = showavatar
                });
            }

            //cho phep tu add multi extension
            var multiExtension = body.Get("ExtensionInfo") ?? string.Empty;
            if (!string.IsNullOrEmpty(multiExtension))
            {
                try
                {
                    var data = NewtonJson.Deserialize<List<NewsExtensionEntity>>(multiExtension);
                    if (data != null && data.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            if (!newsExtensions.Exists(s => s.Type == item.Type))
                            {
                                newsExtensions.Add(new NewsExtensionEntity
                                {
                                    Type = item.Type,
                                    Value = item.Value
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            #endregion

            #region Dạng bài đặc biệt

            #region Magazine
            if (news.Type == (int)NewsType.Magazine)
            {
                var magazineType = body.Get("magazine_type");
                if (string.IsNullOrEmpty(magazineType)) { magazineType = "1"; }
                var avatar6 = body.Get("avatar6");

                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.MagazineType,
                    Value = magazineType
                });

                if (magazineType == "1")
                {
                    if (!string.IsNullOrEmpty(avatar6))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {

                            Type = (int)EnumNewsExtensionType.MagazineCoverMobile,
                            Value = avatar6
                        });
                    }

                    //video cover
                    var videocover = body.Get("magazine_video_cover");
                    if (!string.IsNullOrEmpty(videocover))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {

                            Type = (int)EnumNewsExtensionType.MagazineVideoCover,
                            Value = videocover
                        });
                    }

                    //cover text
                    var videotextlayer = body.Get("magazine_video_text_layer");
                    if (!string.IsNullOrEmpty(videotextlayer))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {

                            Type = (int)EnumNewsExtensionType.MagazineVideoCoverText,
                            Value = videotextlayer
                        });
                    }
                }
                else if (magazineType == "2")
                {
                    var magazinWebZipUrl = body.Get("magazine_web_zip_url");
                    var magazinMobileZipUrl = body.Get("magazine_mobile_zip_url");

                    if (!string.IsNullOrEmpty(magazinWebZipUrl))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {
                            Type = (int)EnumNewsExtensionType.MagazineWebUrl,
                            Value = magazinWebZipUrl
                        });

                        news.Body = RequestHtml(magazinWebZipUrl);
                    }

                    if (!string.IsNullOrEmpty(magazinMobileZipUrl))
                    {
                        newsExtensions.Add(new NewsExtensionEntity
                        {
                            Type = (int)EnumNewsExtensionType.MagazineMobileUrl,
                            Value = magazinMobileZipUrl
                        });
                    }
                }
            }
            #endregion
            #region Bài BigStory
            else if (news.Type == (int)NewsType.BigStory)
            {
                //goi nodejs
                var bigStoryId = body.Get("big_story_id");
                var rawItems = body.Get("big_story_items");
                var rawFocusItems = body.Get("big_story_focus_items");
                var bigStory = new BigStoryServices().BigStory_GetById(bigStoryId);

                if (bigStory == null || bigStoryId == "")
                {
                    //Thêm mới BigStory
                    var res = AddBigStory(new NodeJs_BigStoryEntity()
                    {
                        Avatar = "",
                        CreatedBy = news.CreatedBy,
                        CreatedDate = DateTime.Now,
                        Description = news.InitSapo,
                        Title = news.Title,
                        IsFocus = 0,
                        Status = 1,
                    });

                    //Lấy BigStoryId
                    try
                    {
                        if (res != null && res.Success == true)
                        {
                            bigStoryId = res.Data;
                        }
                        else
                        {
                            Logger.WriteLog(Logger.LogType.Error, "BigStory_Insert => Error");
                            throw new Exception();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    //cập nhật BigStory
                    bigStory.Title = news.Title;
                    bigStory.Description = news.InitSapo;
                    bigStory.ModifiedBy = news.LastModifiedBy;
                    bigStory.ModifiedDate = DateTime.Now;
                    UpdateBigStory(bigStory);
                }

                //Liên kết với bài viết qua NewsExtensions
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)EnumNewsExtensionType.BigstoryId,
                    Value = bigStoryId
                });
                //Cập nhật BigStoryItem
                if (!string.IsNullOrEmpty(rawItems))
                    UpdateBigStoryItems(bigStoryId, rawItems, rawFocusItems, news.CreatedBy);

                //var bigStoryId = Utility.ConvertToInt(body.Get("big_story_id"));
                //var rawItems = body.Get("big_story_items");
                //var rawFocusItems = body.Get("big_story_focus_items");
                //var bigStory = new BigStoryServices().BigStory_GetById(bigStoryId);

                //if (bigStory == null || bigStoryId == 0)
                //{
                //    //Thêm mới BigStory
                //    var res = AddBigStory(new BigStoryEntity()
                //    {
                //        Avatar = "",
                //        CreatedBy = news.CreatedBy,
                //        CreatedDate = DateTime.Now,
                //        Description = news.InitSapo,
                //        Title = news.Title,
                //        IsFocus = false,
                //        Status = 1,
                //    });

                //    //Lấy BigStoryId
                //    try
                //    {
                //        if (res != null && res.Success == true)
                //        {
                //            bigStoryId = Utility.ConvertToInt(res.Data);
                //        }
                //        else
                //        {
                //            Logger.WriteLog(Logger.LogType.Error, "BigStory_Insert => Error");
                //            throw new Exception();
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //    }
                //}
                //else
                //{
                //    //cập nhật BigStory
                //    bigStory.Title = news.Title;
                //    bigStory.Description = news.InitSapo;
                //    bigStory.LastModifiedBy = news.LastModifiedBy;
                //    bigStory.LastModifiedDate = DateTime.Now;
                //    UpdateBigStory(bigStory);
                //}

                ////Liên kết với bài viết qua NewsExtensions
                //newsExtensions.Add(new NewsExtensionEntity
                //{
                //    Type = (int)EnumNewsExtensionType.BigstoryId,
                //    Value = bigStoryId.ToString()
                //});
                ////Cập nhật BigStoryItem
                //UpdateBigStoryItems(bigStoryId, rawItems, rawFocusItems, news.CreatedBy);
            }
            #endregion
            #region FacebookArticle
            else if (news.Type == (int)NewsType.FacebookArticle)
            {
                var facebookArticleType = GetQueryString.GetPost("FacebookArticleType", "26");
                //Liên kết với bài viết qua NewsExtensions
                newsExtensions.Add(new NewsExtensionEntity
                {
                    Type = (int)NewsType.FacebookArticle,
                    Value = facebookArticleType
                });
            }
            #endregion
            #region RollingNews             
            else if (news.Type == (int)NewsType.RollingNews)
            {
                //var bodyContent1 = body.Get("rolling_content_editor");
                //var bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent1);
                //news.Body = bodyContent;
                //var _rollingTitle = body.Get("rolling_title");
                //if (string.IsNullOrEmpty(_rollingTitle))
                //{
                //    _rollingTitle = news.Title;
                //}
                //news.Title = _rollingTitle;
                //var _rollingStartDate = Utility.ConvertToDateTime(body.Get("rolling_start_date"));
                //if (_rollingStartDate == DateTime.MinValue)
                //{
                //    _rollingStartDate = DateTime.Now;
                //}
                //var _rollingStatus = Utility.ConvertToInt(body.Get("rolling_status"));
                //var showauthor = Utility.ConvertToBoolean(body.Get("show_author"));
                //var showtime = Utility.ConvertToBoolean(body.Get("show_time"));
                //var showRollingNewsLabel = Utility.ConvertToBoolean(body.Get("show_rolling_news_label"));

                //var rollingNews = new RollingNewsServices().GetRollingNewsByRollingNewsId(rollingNewsId);

                //if (rollingNews == null || string.IsNullOrEmpty(rollingNewsId))
                //{
                //    try
                //    {
                //        //Thêm mới RollingNews
                //        var res = AddRollingNews(new NodeJs_RollingNews()
                //        {
                //            Id = rollingNewsId,
                //            Title = _rollingTitle,
                //            Status = _rollingStatus,
                //            ShowAuthor = showauthor,
                //            ShowTime = showtime,
                //            StartDate = _rollingStartDate,
                //            IsShowRollingNewsLabel = showRollingNewsLabel
                //        });

                //        ////Lấy RollingNewsId
                //        //if (res != null && res.Success == true)
                //        //{
                //        //    news.RollingNewsId = Utility.ConvertToInt(res.Data);
                //        //}
                //        //else
                //        //{
                //        //    Logger.WriteLog(Logger.LogType.Error, "RollingNews_Insert => Error");
                //        //    throw new Exception();
                //        //}
                //    }
                //    catch
                //    {
                //        Logger.WriteLog(Logger.LogType.Error, "RollingNews_Insert => Error");
                //        throw new Exception();
                //    }
                //}
                //else
                //{
                //    //cập nhật RollingNews                  
                //    UpdateRollingNews(new NodeJs_RollingNews()
                //    {
                //        Id = rollingNewsId,
                //        Title = _rollingTitle,
                //        Status = _rollingStatus,
                //        ShowAuthor = showauthor,
                //        ShowTime = showtime,
                //        StartDate = _rollingStartDate,
                //        IsShowRollingNewsLabel = showRollingNewsLabel
                //    });
                //}

                //Liên kết với bài viết qua NewsExtensions
                //newsExtensions.Add(new NewsExtensionEntity
                //{
                //    Type = (int)EnumNewsExtensionType.RollingNewsId,
                //    Value = rollingNewsId
                //});

            }
            #endregion
            #region LiveMatch
            else if (news.Type == (int)NewsType.LiveMatch)
            {
                //var bodyContent1 = body.Get("live_match_content_editor");
                //var bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent1);
                //news.Body = bodyContent;
                //var LogoTeamA = body.Get("logo_team_a");
                //var LogoTeamB = body.Get("logo_team_b");
                //var Referee = body.Get("referee");
                //var scoreOfTeamA = Utility.ConvertToInt(body.Get("score_of_team_a"));
                //var scoreOfTeamB = Utility.ConvertToInt(body.Get("score_of_team_b"));
                //var TimeOfMatchBegin = Utility.ConvertToDateTime(body.Get("time_of_match_begin"));
                //var stadium = body.Get("stadium");
                //var teama = body.Get("team_a");
                //var teamb = body.Get("team_b");
                //var liveMatchTitle = body.Get("live_match_title");
                //var linkLiveTV1 = body.Get("link_live_tv1");
                //var linkLiveTV2 = body.Get("link_live_tv2");
                //var linkLiveTV3 = body.Get("link_live_tv3");
                //var linkLiveTV4 = body.Get("link_live_tv4");
                //var linkLiveTV5 = body.Get("link_live_tv5");
                //var showOnLiveTV = Utility.ConvertToBoolean(body.Get("show_on_live_tv"));

                //if (!string.IsNullOrEmpty(liveMatchId))
                //{
                //    var match = new LiveMatchServices().GetLiveMatchById(liveMatchId);
                //    match.LogoTeamA = LogoTeamA;
                //    match.LogoTeamB = LogoTeamB;
                //    match.Title = liveMatchTitle;
                //    match.TeamA = teama;
                //    match.TeamB = teamb;
                //    match.Stadium = stadium;
                //    match.Referee = Referee;
                //    match.ScoreOfTeamA = scoreOfTeamA;
                //    match.ScoreOfTeamB = scoreOfTeamB;
                //    match.TimeOfMatchBegin = TimeOfMatchBegin;
                //    match.LinkLiveTV1 = linkLiveTV1;
                //    match.LinkLiveTV2 = linkLiveTV2;
                //    match.LinkLiveTV3 = linkLiveTV3;
                //    match.LinkLiveTV4 = linkLiveTV4;
                //    match.LinkLiveTV5 = linkLiveTV5;
                //    match.ShowOnLiveTV = showOnLiveTV ? "1" : "0";
                //    match.InfomationBeforeMatch = "";//bodyContent;

                //    var data = new LiveMatchServices().UpdateLiveMatch(match);
                //    if (data.Success)
                //    {
                //        try
                //        {
                //            //ChannelVN.CMS.WcfMapping.ExternalCache.CacheManager.RemoveCacheForMatchInfo(liveMatchId, updateLiveMatchInfoAction);
                //        }
                //        catch (Exception ex)
                //        {
                //            Logger.WriteLog(Logger.LogType.Error, ex.Message + ex.StackTrace);
                //        }
                //    }
                //}
                //else
                //{
                //    var match = new NodeJs_LiveMatchEntity
                //    {
                //        Id = liveMatchId,
                //        LogoTeamA = LogoTeamA,
                //        LogoTeamB = LogoTeamB,
                //        Title = liveMatchTitle,
                //        TeamA = teama,
                //        TeamB = teamb,
                //        Stadium = stadium,
                //        Referee = Referee,
                //        ScoreOfTeamA = scoreOfTeamA,
                //        ScoreOfTeamB = scoreOfTeamB,
                //        TimeOfMatchBegin = TimeOfMatchBegin,
                //        LinkLiveTV1 = linkLiveTV1,
                //        LinkLiveTV2 = linkLiveTV2,
                //        LinkLiveTV3 = linkLiveTV3,
                //        LinkLiveTV4 = linkLiveTV4,
                //        LinkLiveTV5 = linkLiveTV5,
                //        ShowOnLiveTV = showOnLiveTV ? "1" : "0",
                //        InfomationBeforeMatch = ""//bodyContent
                //    };
                //    //Lấy liveMatchId
                //    try
                //    {
                //        var data = new LiveMatchServices().InsertLiveMatchV2(match, ref liveMatchId);
                //    }
                //    catch
                //    {
                //        Logger.WriteLog(Logger.LogType.Error, "LiveMatch_Insert => Error");
                //        throw new Exception();
                //    }
                //}
                ////Liên kết với bài viết qua NewsExtensions
                //newsExtensions.Add(new NewsExtensionEntity
                //{
                //    Type = (int)EnumNewsExtensionType.LiveMatchId,
                //    Value = liveMatchId.ToString()
                //});
            }
            #endregion
            #region Interview                        
            else if (news.Type == (int)NewsType.Interview)
            {
                //var bodyContent1 = GetQueryString.GetPost("interviewContentEditor", string.Empty);
                //var bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent1);
                //news.Body = bodyContent;
                //var isactive = GetQueryString.GetPost("isactive", false);
                //var isfocus = GetQueryString.GetPost("isfocus", false);
                //var time = GetQueryString.GetPost("interviewtime", DateTime.MaxValue);
                ////var interviewId = news.InterviewId;

                //var interview = new NodeJs_Interview()
                //{
                //    //Id = interviewId,
                //    //Title = news.Title,
                //    //IsActived = isactive,
                //    //IsFocus = isfocus,
                //    //StartDate = time,
                //    //CreatedBy = currentUsername,
                //    //LastModifiedBy = currentUsername
                //    Id = interviewId,
                //    Title = news.Title,
                //    StartDate = time,
                //    IsFocus = isfocus,
                //    IsActived = isactive,
                //    CreatedBy = currentUsername
                //};
                //try
                //{
                //    var data = new WcfActionResponse();
                //    if (string.IsNullOrEmpty(interviewId))
                //    {
                //        data = new InterviewServices().InterviewV3Insert(interview, ref interviewId);
                //    }
                //    else
                //    {
                //        data = new InterviewServices().InterviewV3Update(interview);
                //    }
                //    if (data.Success)
                //    {
                //        try
                //        {
                //            //ChannelVN.CMS.WcfMapping.ExternalCache.CacheManager.RemoveCacheOnUpdateInterview(interviewId);
                //        }
                //        catch (Exception ex)
                //        {
                //            Logger.WriteLog(Logger.LogType.Error, ex.Message + ex.StackTrace);
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{
                //    Logger.WriteLog(Logger.LogType.Error, ex.Message + ex.StackTrace);
                //}

                ////bài phỏng vấn                
                //newsExtensions.Add(new NewsExtensionEntity
                //{
                //    Type = (int)EnumNewsExtensionType.InterviewId,
                //    Value = interviewId.ToString()
                //});
            }
            #endregion

            #endregion

            return base.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, isRebuildLink, authorList, newsChildOrder, sourceId, publishedContent, newsExtensions, isNotify);
        }

        #region NewsInUsing
        private string GetLandingPageContent(int interactiveId)
        {
            var url = CmsChannelConfiguration.GetAppSetting("LandingPageGetDataUrl");
            if (url != "")
            {
                var request = (HttpWebRequest)WebRequest.Create(url);

                var sb = new StringBuilder();
                sb.Append("secretKey=ahur3478edfjh23losa9d212sj3e2asldoijd29");
                sb.Append("&fn=GetIndexPage");
                sb.Append("&dataType=html");
                sb.AppendFormat("&interactiveId={0}", interactiveId);
                var data = Encoding.ASCII.GetBytes(sb.ToString());


                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                return new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            return "";
        }
        #endregion

        #region SyncData
        string RequestHtml(string url)
        {
            string result = "";
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Credentials = CredentialCache.DefaultCredentials;
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                result = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return result;
        }
        //protected void SyncData(long newsId, string bodyContent)
        //{
        //    HtmlDocument htmlDoc = new HtmlDocument();
        //    htmlDoc.OptionFixNestedTags = true;
        //    htmlDoc.OptionCheckSyntax = false;
        //    htmlDoc.LoadHtml(bodyContent);

        //    var photoIds = new List<int>();
        //    var videoIds = new List<int>();
        //    var photoDes = new List<string>();
        //    var elms = htmlDoc.DocumentNode.SelectNodes("//*[@class='VCSortableInPreviewMode'][@type='Photo']//img");
        //    if (elms != null)
        //    {
        //        for (int i = 0; i < elms.Count; i++)
        //        {
        //            photoIds.Add(elms[i].GetAttributeValue("photoid", 0));
        //        }
        //    }

        //    var elms1 = htmlDoc.DocumentNode.SelectNodes("//*[@class='PhotoCMS_Caption']");
        //    if (elms1 != null)
        //    {
        //        for (int i = 0; i < elms1.Count; i++)
        //        {
        //            photoDes.Add(elms1[i].InnerText);
        //        }
        //    }

        //    //Video có 2 dạng mã nhúng
        //    var elms2 = htmlDoc.DocumentNode.SelectNodes("//*[@class='VCSortableInPreviewMode'][@type='Video']//object");
        //    if (elms2 != null)
        //    {
        //        for (int i = 0; i < elms2.Count; i++)
        //        {
        //            videoIds.Add(elms2[i].GetAttributeValue("videoid", 0));
        //        }
        //    }

        //    var elms3 = htmlDoc.DocumentNode.SelectNodes("//*[contains(@class,'VCSortableInPreviewMode')][@type='VideoStream']");
        //    if (elms3 != null)
        //    {
        //        for (int i = 0; i < elms3.Count; i++)
        //        {
        //            videoIds.Add(elms3[i].GetAttributeValue("videoid", 0));
        //        }
        //    }

        //    photoIds = photoIds.Where(it => it != 0).ToList();
        //    videoIds = videoIds.Where(it => it != 0).ToList();
        //    try
        //    {
        //        var photoRes = NewsServices.UpdatePhotoPublishedInNews(newsId, String.Join(";", photoIds), String.Join(";", photoDes));
        //        var videoRes = MediaServices.SyncNewsAndVideo(String.Join(";", videoIds), newsId);

        //        if (photoIds.Count > 0 || videoIds.Count > 0)
        //        {
        //            NewsServices.UpdatePhotoVideoCount(newsId, photoIds.Count, videoIds.Count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //    }
        //}
        #endregion

        #region BigStory

        private WcfActionResponse AddBigStory(NodeJs_BigStoryEntity newBigStory)
        {
            //return new BigStoryServices().BigStory_Insert(newBigStory);
            //goi nodejs
            var id = "";
            var data = new BigStoryServices().BigStory_Insert(newBigStory, ref id);
            data.Data = id;
            return data;
        }

        private WcfActionResponse UpdateBigStory(NodeJs_BigStoryEntity bigStory)
        {
            return new BigStoryServices().BigStory_Update(bigStory);
        }
        private WcfActionResponse UpdateBigStoryItems(string bigStoryId, string rawItems, string rawFocusItems, string username)
        {
            //goi nodejs
            var items = JsonConvert.DeserializeObject<List<BigStoryItemMappingEntity>>(rawItems);
            var listItems = new List<BigStoryItemMappingEntity>();
            listItems = items.Select(bigStoryItem => new BigStoryItemMappingEntity
            {
                BigStoryId = bigStoryId,
                CreatedBy = username,
                CreatedDate = DateTime.Now,
                Avatar = bigStoryItem.Avatar,
                Body = bigStoryItem.Body,
                IsFocus = bigStoryItem.IsFocus,
                IsHighlight = bigStoryItem.IsHighlight,
                IsHot = bigStoryItem.IsHot,
                PublishedDate = bigStoryItem.PublishedDate,
                Status = bigStoryItem.Status,
                TabId = bigStoryItem.TabId,
                Title = bigStoryItem.Title,
                Url = bigStoryItem.Url,
                Id = bigStoryItem.BigStoryId,
                IsRemove = bigStoryItem.IsRemove
            }).ToList();

            var res = new BigStoryServices().BigStoryItem_InsertByList(listItems, bigStoryId);


            //var items = JsonConvert.DeserializeObject<List<BigStoryItemMappingEntity>>(rawItems);
            //var focusitems = JsonConvert.DeserializeObject<List<BigStoryFocusItemEntity>>(rawFocusItems);
            //foreach (var bigStoryItem in items)
            //{
            //    bigStoryItem.BigStoryId = bigStoryId;
            //    bigStoryItem.CreatedBy = username;
            //    bigStoryItem.CreatedDate = DateTime.Now;
            //}

            //var res = new BigStoryServices().BigStoryItem_UpdateByList(items, bigStoryId);

            //tam comment
            //foreach (var bigStoryFocusItem in focusitems)
            //{
            //    bigStoryFocusItem.BigStoryId = bigStoryId;
            //    bigStoryFocusItem.CreatedBy = username;
            //    bigStoryFocusItem.CreatedDate = DateTime.Now;
            //}
            //var listfocus = focusitems.ToArray();
            //var res2 = BigStoryServices.BigStoryFocusItem_UpdateByList(listfocus, bigStoryId);

            if (res == null || (res != null && res.Success == false)) //tam comment: || res2 == null || (res2 != null && res2.Success == false)
            {
                Logger.WriteLog(Logger.LogType.Error, "BigStoryServices.BigStoryItem_UpdateByList => Error");
                return res;
            }
            return res;
        }
        #endregion

        #region RollingNews

        private WcfActionResponse AddRollingNews(NodeJs_RollingNews newRollingNews)
        {
            var rollingNewsId = "";
            var res = new RollingNewsServices().InsertRollingNewsV2(newRollingNews, ref rollingNewsId);
            res.Success = res.Success;
            res.Message = res.Message;
            res.Data = rollingNewsId.ToString();

            return res;
        }
        private WcfActionResponse UpdateRollingNews(NodeJs_RollingNews rollingNews)
        {
            var res = new RollingNewsServices().UpdateRollingNews(rollingNews);
            res.Success = res.Success;
            res.Message = res.Success ? "Cập nhật thành công!" : res.Message;
            res.Data = res.Data;
            return res;
        }

        #endregion

        #region Constructors

        public NewsServices()
        {

        }
        private NameValueCollection body { get; set; }
        public NewsServices(NameValueCollection body) : this()
        {
            this.body = body;
        }

        #endregion
    }
}