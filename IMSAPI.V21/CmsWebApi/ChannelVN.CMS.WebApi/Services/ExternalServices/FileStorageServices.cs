﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class FileStorageServices
    {
        #region FileManager

        public const string FILE_MANAGER_HTTPSERVER = "FILE_MANAGER_HTTPSERVER";
        public const string FILE_MANAGER_HTTPSERVER_IP = "FILE_MANAGER_HTTPSERVER_IP";

        public const string FILE_MANAGER_HTTPDOWNLOAD = "FILE_MANAGER_HTTPDOWNLOAD";
        public const string FILE_MANAGER_HTTPDOWNLOAD_THUMB = "FILE_MANAGER_HTTPDOWNLOAD_THUMB";
        public const string FILE_MANAGER_HTTPUPLOAD = "FILE_MANAGER_HTTPUPLOAD";
        public const string FILE_MANAGER_HTTPUPLOAD_BACKUP = "FILE_MANAGER_HTTPUPLOAD_BACKUP";
        public const string FILE_MANAGER_SECRET_KEY = "FILE_MANAGER_SECRET_KEY";
        public const string FILE_MANAGER_PREFIX = "FILE_MANAGER_PREFIX";
        public const string FILE_MANAGER_INCLUDING_PREFIX = "FILE_MANAGER_INCLUDING_PREFIX";

        public const string FILE_MANAGER_SPLITCHAR = "FILE_MANAGER_SPLITCHAR";
        public const string FILE_MANAGER_UPLOAD_NAMESPACE = "FILE_MANAGER_UPLOAD_NAMESPACE";
        public const string FILE_MANAGER_UPLOAD_EXPIRED_TIME = "FILE_MANAGER_UPLOAD_EXPIRED_TIME";
        public const string FILE_MANAGER_UPLOAD_CALLBACK = "FILE_MANAGER_UPLOAD_CALLBACK";
        public const string FILE_MANAGER_UPLOAD_MAXLENGTH = "FILE_MANAGER_UPLOAD_MAXLENGTH";

        #endregion

        private static readonly string HttpServer = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_HTTPSERVER);
        private static readonly string HttpServerIp = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_HTTPSERVER_IP);

        private static readonly string SecretKey = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_SECRET_KEY);
        private static readonly string Prefix = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_PREFIX);
        private static readonly bool IsIncludingPrefix = CmsChannelConfiguration.GetAppSettingInBoolean(FILE_MANAGER_INCLUDING_PREFIX);

        private static readonly string HttpDownload = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_HTTPDOWNLOAD);

        private static readonly string HttpUpload = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_HTTPUPLOAD);

        private static readonly string SplitChar = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_SPLITCHAR);
        private static readonly string UploadNamespace = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_UPLOAD_NAMESPACE);
        private static readonly int UploadExpiredTime = CmsChannelConfiguration.GetAppSettingInInt32(FILE_MANAGER_UPLOAD_EXPIRED_TIME);

        private static readonly string FormatApiGetDirectoriesList = HttpServer + "ls?secret_key={0}&dirname={1}&reverse={2}&sort_by={3}";
        private static readonly string FormatApiMakeDirectory = HttpServer + "make_dir?secret_key={0}&dirname={1}";
        private static readonly string FormatApiDeleteDirectory = HttpServer + "remove_dir?secret_key={0}&dirname={1}";
        private static readonly string FormatApiRenameDirectory = HttpServer + "rename?secret_key={0}&from={1}&to={2};";

        private static readonly string FormatApiGetFileInfo = HttpServer + "get_file_info?secret_key={0}&filename={1}";
        private static readonly string FormatApiDeleteFile = HttpServer + "delete?secret_key={0}&filename={1}";
        private static readonly string FormatApiUploadDataFile = HttpServer + "upload";

        private static readonly string FormatApiDownLoadAllFile = HttpDownload + "compress?files={0}&name={1}.zip";
        private static readonly string FormatApiDownLoadOneFile = HttpDownload + "{0}";
        private static readonly string FormatRemoveThumbCached = HttpServerIp + "delete_thumbnail_cache?secret_key={0}&filename={1}";

        private static readonly string FormatApiUnArchiveFileZip = HttpServerIp + "unarchive?secret_key={0}&archive_file={1}";
        private static readonly string FormatApiGetStatusUnArchiveFileZip = HttpServerIp + "get_api_status?status_key={0}&is_unarchive={1}";

        public static ChannelvnDirInfo ListDir(string dirName, int order, string sortBy)
        {
            if (dirName.IndexOf('/') < 0) dirName += "/";
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiGetDirectoriesList, SecretKey, dirName, order, sortBy));
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null) return null;

                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                webResponse.Close();
                webResponse.Dispose();
                return DeserializeDirInfo(result);
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static string MakeDirectory(string dirName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiMakeDirectory, SecretKey, dirName));
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";

                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                webResponse.Close();
                webResponse.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static string RenameDirectory(string dirName, string newDirName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiRenameDirectory, SecretKey, dirName, newDirName));
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";

                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                webResponse.Close();
                webResponse.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static string DeleteDirectory(string dirName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiDeleteDirectory, SecretKey, dirName));
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";

                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                webResponse.Close();
                webResponse.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static ChannelvnFileInfo GetFileInfo(string fileName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) fileName = Prefix + "/" + fileName;
            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiGetFileInfo, SecretKey, fileName));
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";
                webRequest.KeepAlive = false;
                webRequest.Timeout = 10 * 1000;
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null) return null;

                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                Logger.WriteLog(Logger.LogType.Trace, result);
                webResponse.Close();
                webResponse.Dispose();
                return DeserializeFileInfo(result);
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n====> FileName: " + fileName);
                return null;
            }
        }

        public static string DeleteFile(string dirName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiDeleteFile, SecretKey, dirName));
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";

                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                webResponse.Close();
                webResponse.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static string DeleteThumbCached(string fileName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) fileName = Prefix + "/" + fileName;
            try
            {
                var url = string.Format(FormatRemoveThumbCached, SecretKey, fileName);
                Logger.WriteLog(Logger.LogType.Debug, "DeleteThumbCached: " + url);
                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";

                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static string UploadFile(string dirName, Stream fileStream)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            try
            {
                var streamLength = fileStream.Length;
                var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiUploadDataFile, SecretKey, dirName));
                var userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.1) Gecko/20060111 Firefox/1.5.0.1";
                //webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "POST";
                webRequest.ContentType = "multipart/form-data; boundary=-----------------------------28947758029299";
                webRequest.UserAgent = userAgent;
                webRequest.CookieContainer = new CookieContainer();
                webRequest.ContentLength = streamLength;  // We need to count how many bytes we're sending. 

                var data = new byte[streamLength];
                fileStream.Read(data, 0, (int)streamLength);

                using (var requestStream = webRequest.GetRequestStream())
                {
                    // Push it out there
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();
                }

                // get the response
                var webResponse = webRequest.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                webResponse.Close();
                webResponse.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public static string UploadFile(string dirName, HttpPostedFile httpPostedFile)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiUploadDataFile, SecretKey, dirName));
                var userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.1) Gecko/20060111 Firefox/1.5.0.1";
                //webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "POST";
                webRequest.ContentType = "multipart/form-data; boundary=-----------------------------28947758029299";
                webRequest.UserAgent = userAgent;
                webRequest.CookieContainer = new CookieContainer();
                webRequest.ContentLength = httpPostedFile.ContentLength;  // We need to count how many bytes we're sending. 

                var data = new byte[httpPostedFile.ContentLength];
                var myStream = httpPostedFile.InputStream;
                myStream.Read(data, 0, httpPostedFile.ContentLength);

                using (var requestStream = webRequest.GetRequestStream())
                {
                    // Push it out there
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();
                }

                // get the response
                var webResponse = webRequest.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                webResponse.Close();
                webResponse.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ex.Message;
            }
        }

        public static bool UploadImage(string currentPolicy, string currentSignature, string fileName, string contentType, string base64Data)
        {
            var query = "";
            try
            {
                query = "policy=" + HttpContext.Current.Server.UrlEncode(currentPolicy) +
                    "&signature=" + HttpContext.Current.Server.UrlEncode(currentSignature) +
                    "&source=" + HttpContext.Current.Server.UrlEncode(base64Data);
                var requestParamsInBytes = Encoding.UTF8.GetBytes(query);

                var httpRequest = (HttpWebRequest)WebRequest.Create(HttpUpload);
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                httpRequest.ContentLength = requestParamsInBytes.Length;
                httpRequest.Method = "POST";
                httpRequest.KeepAlive = true;
                httpRequest.Timeout = 5 * 1000;
                httpRequest.MaximumResponseHeadersLength = int.MaxValue;

                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    streamWriter.Write(requestParamsInBytes, 0, requestParamsInBytes.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    httpResponse.Close();
                    httpResponse.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, "UploadImage(" + query + ") => " + ex.ToString());
            }
            return false;
        }

        public static bool UploadImage(string fileName, Stream fileData, bool overWrite)
        {
            try
            {
                string uploadaction = HttpServer + "upload";
                NameValueCollection parameters = new NameValueCollection();

                if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix) && !fileName.StartsWith(Prefix + "/"))
                    fileName = Prefix + "/" + fileName.TrimStart('/');

                parameters.Add("filename", fileName);
                parameters.Add("secret_key", SecretKey);
                parameters.Add("fix_orientation", "1");
                if (overWrite)
                    parameters.Add("overwrite", "1");

                if (!string.IsNullOrEmpty(UploadNamespace))
                    parameters.Add("namespace", UploadNamespace);

                HttpWebResponse resp = (HttpWebResponse)HttpMethodHelper.PostFile(new Uri(uploadaction), parameters, fileData, fileName, ReturnExtension(Path.GetExtension(fileName)), "filedata", null, null);
                bool result = false;
                if (resp.StatusCode == HttpStatusCode.OK) result = true;
                resp.Close();
                resp.Dispose();
                return result;
            }
            catch
            {
                return false;
            }
        }

        public static bool UploadFile(string currentPolicy, string currentSignature, string fileName, string contentType, string base64Data)
        {
            var query = "";
            try
            {
                query = "policy=" + HttpContext.Current.Server.UrlEncode(currentPolicy) +
                    "&signature=" + HttpContext.Current.Server.UrlEncode(currentSignature) +
                    "&source=" + HttpContext.Current.Server.UrlEncode(base64Data);
                var requestParamsInBytes = Encoding.UTF8.GetBytes(query);

                var httpRequest = (HttpWebRequest)WebRequest.Create(HttpUpload + "/upload");
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                httpRequest.ContentLength = requestParamsInBytes.Length;
                httpRequest.Method = "POST";
                httpRequest.KeepAlive = true;
                httpRequest.Timeout = 5 * 1000;
                httpRequest.MaximumResponseHeadersLength = int.MaxValue;

                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    streamWriter.Write(requestParamsInBytes, 0, requestParamsInBytes.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    httpResponse.Close();
                    httpResponse.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, "UploadImage(" + query + ") => " + ex.ToString());
            }
            return false;
        }

        public static bool UploadFile(string currentPolicy, string currentSignature, string fileName, string base64Data)
        {
            var query = "";
            try
            {
                query = "policy=" + HttpContext.Current.Server.UrlEncode(currentPolicy) +
                "&signature=" + HttpContext.Current.Server.UrlEncode(currentSignature) +
                "&source=" + HttpContext.Current.Server.UrlEncode(base64Data);
                var requestParamsInBytes = Encoding.UTF8.GetBytes(query);

                var httpRequest = (HttpWebRequest)WebRequest.Create(HttpUpload);
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                httpRequest.ContentLength = requestParamsInBytes.Length;
                httpRequest.Method = "POST";
                httpRequest.KeepAlive = true;
                httpRequest.Timeout = 5 * 1000;
                httpRequest.MaximumResponseHeadersLength = int.MaxValue;

                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    streamWriter.Write(requestParamsInBytes, 0, requestParamsInBytes.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                
                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    httpResponse.Close();
                    httpResponse.Dispose();
                    return true;
                }

                Logger.WriteLog(Logger.LogType.Debug, "CHINHNB End log request StatusCode: " + httpResponse.StatusCode + " => currentPolicy: " + HttpContext.Current.Server.UrlEncode(currentPolicy));

                return false;
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError) //400 errors
                {
                    var response = ex.Response as HttpWebResponse;

                    if (response != null)
                    {
                        Logger.WriteLog(Logger.LogType.Debug, "1.CHINHNB HTTP Status Code: " + (int)response.StatusCode + " => HttpUpload: " + HttpUpload + " => currentPolicy: " + HttpContext.Current.Server.UrlEncode(currentPolicy));                        
                    }
                }
                else
                {
                    Logger.WriteLog(Logger.LogType.Debug, "2.CHINHNB HTTP Status Code: " + ex.Status + " => HttpUpload: " + HttpUpload + " => currentPolicy: " + HttpContext.Current.Server.UrlEncode(currentPolicy));
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UploadFile =>currentPolicy(" + HttpContext.Current.Server.UrlEncode(currentPolicy) + ") => HttpUpload: " + HttpUpload + " => " + ex.ToString());
                return false;
            }            
        }


        public static bool UploadFileZip(string currentPolicy, string currentSignature, string fileName, string base64Data)
        {
            var query = "";
            try
            {
                query = "policy=" + HttpContext.Current.Server.UrlEncode(currentPolicy) +
                "&signature=" + HttpContext.Current.Server.UrlEncode(currentSignature) +
                "&source=" + HttpContext.Current.Server.UrlEncode(base64Data);
                var requestParamsInBytes = Encoding.UTF8.GetBytes(query);

                var httpRequest = (HttpWebRequest)WebRequest.Create(HttpUpload);
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                httpRequest.ContentLength = requestParamsInBytes.Length;
                httpRequest.Method = "POST";
                httpRequest.KeepAlive = true;
                httpRequest.Timeout = 30 * 1000;
                httpRequest.MaximumResponseHeadersLength = int.MaxValue;

                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    streamWriter.Write(requestParamsInBytes, 0, requestParamsInBytes.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    Logger.WriteLog(Logger.LogType.Debug, "CHINHNB OK UploadFileZip StatusCode: " + httpResponse.StatusCode + " => currentPolicy: " + HttpContext.Current.Server.UrlEncode(currentPolicy) + " => signature: " + HttpContext.Current.Server.UrlEncode(currentSignature));

                    httpResponse.Close();
                    httpResponse.Dispose();
                    return true;
                }

                Logger.WriteLog(Logger.LogType.Debug, "CHINHNB End log request StatusCode: " + httpResponse.StatusCode + " => currentPolicy: " + HttpContext.Current.Server.UrlEncode(currentPolicy));

                return false;
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError) //400 errors
                {
                    var response = ex.Response as HttpWebResponse;

                    if (response != null)
                    {
                        Logger.WriteLog(Logger.LogType.Debug, "1.CHINHNB HTTP Status Code: " + (int)response.StatusCode + " => currentPolicy: " + HttpContext.Current.Server.UrlEncode(currentPolicy) + " Msg=> "+ ex.Message);
                    }
                }
                else
                {
                    Logger.WriteLog(Logger.LogType.Debug, "2.CHINHNB HTTP Status Code: " + ex.Status + " => currentPolicy: " + HttpContext.Current.Server.UrlEncode(currentPolicy));
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UploadFile =>currentPolicy(" + HttpContext.Current.Server.UrlEncode(currentPolicy) + ") => HttpUpload: "+ HttpUpload + " => " + ex.ToString());
                return false;
            }
        }
        public static string UnArchiveFileZip(string fileName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix) && !fileName.StartsWith(Prefix + "/"))
                fileName = Prefix + "/" + fileName.TrimStart('/');
            try
            {
                var url = string.Format(FormatApiUnArchiveFileZip, SecretKey, fileName);
                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";
                webRequest.KeepAlive = true;
                webRequest.Timeout = 15 * 1000;

                Logger.WriteLog(Logger.LogType.Debug, "1.UnArchiveFileZip: url=> " + url);

                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                {
                    Logger.WriteLog(Logger.LogType.Debug, "2.UnArchiveFileZip: webResponse = null");
                    return null;
                }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();

                Logger.WriteLog(Logger.LogType.Debug, "3.UnArchiveFileZip: result => "+ result);

                webResponse.Close();
                webResponse.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();

                //dynamic obj = NewtonJson.Deserialize<dynamic>(resp);
                //var messageFromServer = obj.error.message;

                Logger.WriteLog(Logger.LogType.Debug, "4.UnArchiveFileZip: result => " + resp);                
                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static string GetStatusUnArchiveFileZip(string statusKey)
        {
            try
            {
                var url = string.Format(FormatApiGetStatusUnArchiveFileZip, statusKey, true);
                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";
                webRequest.KeepAlive = true;
                webRequest.Timeout = 5 * 1000;

                Logger.WriteLog(Logger.LogType.Debug, "1.GetStatusUnArchiveFileZip: url=> " + url);

                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                {
                    Logger.WriteLog(Logger.LogType.Debug, "2.GetStatusUnArchiveFileZip: webResponse = null");
                    return null;
                }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();

                Logger.WriteLog(Logger.LogType.Debug, "3.GetStatusUnArchiveFileZip: result => " + result);

                webResponse.Close();
                webResponse.Dispose();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /*
        public static string UploadData(string userName, string path, string fileName, string dataBase64)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) fileName = Prefix + "/" + fileName;
            var webRequest = (HttpWebRequest)WebRequest.Create(FormatApiUploadDataFile);
            var boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            var boundarybytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
            webRequest.ContentType = "multipart/form-data; boundary=" + boundary;

            webRequest.Method = "POST";
            const string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";

            var policy = CreatePolicyForUpload(userName, path, fileName);

            try
            {
                using (var requestStream = webRequest.GetRequestStream())
                {
                    requestStream.Write(boundarybytes, 0, boundarybytes.Length);
                    requestStream.Write(boundarybytes, 0, boundarybytes.Length);
                    var formitem = string.Format(formdataTemplate, "policy", policy[1]);
                    var formitembytes = Encoding.UTF8.GetBytes(formitem);
                    requestStream.Write(formitembytes, 0, formitembytes.Length);

                    requestStream.Write(boundarybytes, 0, boundarybytes.Length);
                    formitem = string.Format(formdataTemplate, "signature", policy[2]);
                    formitembytes = Encoding.UTF8.GetBytes(formitem);
                    requestStream.Write(formitembytes, 0, formitembytes.Length);

                    requestStream.Write(boundarybytes, 0, boundarybytes.Length);
                    formitem = string.Format(formdataTemplate, "source", dataBase64);
                    formitembytes = Encoding.UTF8.GetBytes(formitem);
                    requestStream.Write(formitembytes, 0, formitembytes.Length);

                    requestStream.Write(boundarybytes, 0, boundarybytes.Length);

                    requestStream.Close();
                    requestStream.Dispose();
                }

                using (var response = (HttpWebResponse)webRequest.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = string.Format("Upload failed. Received HTTP {0}", response.StatusCode);
                        throw new Exception(message);
                    }
                    var stringIn = new StreamReader(response.GetResponseStream());
                    return stringIn.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(ex.Message);
            }
        }
*/

        public static bool UpdateFile(string currentPolicy, string currentSignature, string fileUrl, string fileData, bool isBase64)
        {
            var query = string.Empty;
            var source = HttpContext.Current.Server.UrlEncode("data:application/x-rar-compressed;base64," + Serialize(fileData));
            if (isBase64)
            {
                source=HttpContext.Current.Server.UrlEncode(fileData);
            }            
            try
            {                
                query = "policy=" + HttpContext.Current.Server.UrlEncode(currentPolicy) +
                    "&signature=" + HttpContext.Current.Server.UrlEncode(currentSignature) +
                    "&source=" + source;
                var data = Encoding.UTF8.GetBytes(query);

                var httpRequest = (HttpWebRequest)WebRequest.Create(HttpUpload);
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                httpRequest.ContentLength = data.Length;
                httpRequest.Method = "POST";
                httpRequest.KeepAlive = true;
                httpRequest.Timeout = 5 * 1000;
                httpRequest.MaximumResponseHeadersLength = int.MaxValue;

                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    streamWriter.Write(data, 0, data.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    httpResponse.Close();
                    httpResponse.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UpdateFile => " + " ,currentPolicy =>" + HttpContext.Current.Server.UrlEncode(currentPolicy) + " ,ex =>" + ex.ToString());
                return false;
            }
            return false;
        }

        public static string removeCachedFile(string fileName)
        {            
            try
            {
                var url = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPREMOVECACHED_THUMB") + "/" + fileName;
                
                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "GET";

                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        private static string Serialize(string fileData)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(fileData);
            return Convert.ToBase64String(buffer);
        }

        public static string[] CreatePolicyForUpload(string userName, string path, string filename, bool overwrite)
        {
            try
            {
                var addMin = UploadExpiredTime > 0 ? UploadExpiredTime : 25; // minutes
                var strRandom = Guid.NewGuid().ToString("N").Substring(0, 5);
                var maxRequest = CmsChannelConfiguration.GetAppSettingInInt32(FILE_MANAGER_UPLOAD_MAXLENGTH);

                //Logger.WriteLog(Logger.LogType.Warning, "====== Create policy: " + filename);
                string policy = "{";
                policy += "\"expires\": \"" + DateTime.Now.AddMinutes(addMin).ToString("r") + "\",";
                policy += "\"namespace\": \"" + UploadNamespace + "\",";
                policy += "\"redirect\": \"\",";
                policy += "\"directory\": \"" + (IsIncludingPrefix ? Prefix + "/" + path : path) + "\",";
                policy += "\"filename\": \"" + filename + "\",";
                policy += "\"overwrite\": \"" + (overwrite ? "1" : "0") + "\",";
                policy += "\"fix_orientation\":1,";
                policy += "\"content-length-range\": \"10-" + maxRequest + "\"";
                policy += "}";
                //Logger.WriteLog(Logger.LogType.Warning, "Policy: " + policy);
                var signature = CreateSignature(SecretKey, policy);
                //Logger.WriteLog(Logger.LogType.Warning, "Signature: " + signature);
                Logger.WriteLog(Logger.LogType.Warning, "====== Create policy: " + policy + " =>Signature: " + signature + " =>FileName: " + filename);

                return new[] { strRandom, Base64.Encode(policy), signature, path, policy };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string[] CreatePolicy(string userName, string path)
        {
            try
            {
                var addMin = UploadExpiredTime > 0 ? UploadExpiredTime : 25; // minutes
                var strRandom = Guid.NewGuid().ToString("N").Substring(0, 5);
                var videoCallback = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_UPLOAD_CALLBACK);
                var maxRequest = CmsChannelConfiguration.GetAppSettingInInt32(FILE_MANAGER_UPLOAD_MAXLENGTH);

                strRandom = "-" + strRandom;
                string policy = "{";
                policy += "\"expires\": \"" + DateTime.Now.AddMinutes(addMin).ToString("r") + "\",";
                policy += "\"namespace\": \"" + UploadNamespace + "\",";
                policy += "\"directory\": \"" + path + "\",";
                //policy += "\"convert\": \"1\",";
                policy += "\"filename-prefix\":  \"\",";
                policy += "\"filename-suffix\": \"" + strRandom + "\",";
                //policy += "\"callback_url\": \"" + videoCallback + "\",";
                policy += "\"fix_orientation\":1,";
                policy += "\"content-length-range\": \"2048-" + maxRequest + "\"";
                //policy += ",\"content-type\": \"video/x-flv\"";
                policy += "}";
                var signature = CreateSignature(SecretKey, policy);

                return new[] { strRandom, Base64.Encode(policy), signature, path };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string CreateSignature(string secretKey, string policy)
        {
            var encoding = new ASCIIEncoding();
            var policyBytes = encoding.GetBytes(policy);
            var base64Policy = Convert.ToBase64String(policyBytes);

            var secretKeyBytes = encoding.GetBytes(secretKey);
            var hmacsha1 = new HMACSHA256(secretKeyBytes);

            var base64PolicyBytes = encoding.GetBytes(base64Policy);
            var signatureBytes = hmacsha1.ComputeHash(base64PolicyBytes);

            return Convert.ToBase64String(signatureBytes);
        }

        public static string[] getPolicyForUpload(string directory, string filename, bool overwrite)
        {
            try
            {
                var addMin = UploadExpiredTime > 0 ? UploadExpiredTime : 25; // minutes
                var strRandom = Guid.NewGuid().ToString("N").Substring(0, 5);
                var maxRequest = CmsChannelConfiguration.GetAppSettingInInt32(FILE_MANAGER_UPLOAD_MAXLENGTH);                
                string policy = "{";
                policy += "\"expires\": \"" + DateTime.Now.AddMinutes(addMin).ToString("r") + "\",";
                policy += "\"namespace\": \"" + UploadNamespace + "\",";
                policy += "\"redirect\": \"\",";
                policy += "\"directory\": \"" + directory + "\",";
                policy += "\"filename\": \"" + filename + "\",";
                policy += "\"overwrite\": \"" + (overwrite ? "1" : "0") + "\",";
                policy += "\"fix_orientation\":1,";
                policy += "\"content-length-range\": \"10-" + maxRequest + "\"";
                policy += "}";
                
                var signature = CreateSignature(SecretKey, policy);                
                return new[] { strRandom, Base64.Encode(policy), signature, policy };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Private methods

        private static ChannelvnDirInfo DeserializeDirInfo(string json)
        {
            try
            {
                return NewtonJson.Deserialize<ChannelvnDirInfo>(json);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static ChannelvnFileInfo DeserializeFileInfo(string json)
        {
            try
            {
                return NewtonJson.Deserialize<ChannelvnFileInfo>(json);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string GetFolder(string folderName)
        {
            try
            {
                return folderName.StartsWith(Prefix) ? folderName.Substring(Prefix.Length) : folderName;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //public static string EncryptUserName(string userName)
        //{
        //    if (SecretKey.ToLower() != "b364ab758f285180e7a4ae14eeb9a275".ToLower())
        //    {
        //        userName = userName + SecretKey;
        //    }
        //    string userNameEncrypt = Crypton.EncryptForHTML(userName);

        //    if (SecretKey.ToLower() != "b364ab758f285180e7a4ae14eeb9a275".ToLower())
        //        userNameEncrypt = userNameEncrypt.Length > 30 ? userNameEncrypt.Substring(0, 30) : userNameEncrypt;
        //    else
        //        userNameEncrypt = userNameEncrypt.Length > 20 ? userNameEncrypt.Substring(0, 20) : userNameEncrypt;


        //    userNameEncrypt = Utility.UnicodeToKoDauAndGach(userNameEncrypt);
        //    return userNameEncrypt;
        //}

        #endregion

        public static string GetSessionDownload(string file, string nameFile, bool allfile)
        {
            // Gán session download file 
            var guiId = DateTime.Now.Ticks;
            var sessionName = guiId.ToString();

            if (allfile == true)
            {

                HttpContext.Current.Session[sessionName] = string.Format(FormatApiDownLoadAllFile, file, nameFile);

            }
            else
            {
                HttpContext.Current.Session[sessionName] = string.Format(FormatApiDownLoadOneFile, file);
            }

            return sessionName;
        }

        public static void DownLoadFile(string filename, string session)
        {
            if (session != "")
            {

                if (HttpContext.Current.Session[session] != null)
                {
                    var fileExt = Path.GetExtension(filename).ToLower();
                    Stream stream = null;
                    int bytesToRead = 10000;
                    byte[] buffer = new Byte[bytesToRead];

                    try
                    {
                        HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(HttpContext.Current.Session[session].ToString());
                        fileReq.ContentType = ReturnExtension(fileExt);
                        fileReq.Method = "GET";

                        HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                        stream = fileResp.GetResponseStream();
                        var resp = HttpContext.Current.Response;

                        resp.ContentType = ReturnExtension(fileExt);
                        resp.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                        resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());
                        int length;
                        do
                        {
                            if (resp.IsClientConnected)
                            {

                                length = stream.Read(buffer, 0, (int)bytesToRead);
                                resp.OutputStream.Write(buffer, 0, length);
                                resp.Flush();
                                buffer = new Byte[bytesToRead];
                            }
                            else
                            {
                                length = -1;
                            }
                        } while (length > 0);
                        fileResp.Close();
                        fileResp.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal,
                                        "DownLoadFile('" + filename + "', '" + session + "') error => " + ex);
                        HttpContext.Current.Response.ContentType = "text/plain";
                        HttpContext.Current.Response.Write("File không tồn tại");
                        HttpContext.Current.Response.End();
                    }
                    finally
                    {
                        if (stream != null)
                        {
                            stream.Close();
                        }
                    }

                }
                else
                {
                    HttpContext.Current.Response.ContentType = "text/plain";
                    HttpContext.Current.Response.Write("File không tồn tại");
                    HttpContext.Current.Response.End();
                }
            }
        }

        public static string ReturnExtension(string fileExtension)
        {
            switch (fileExtension.ToLower())
            {
                case ".htm":
                case ".html":
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".doc":
                    return "application/ms-word";
                case ".tiff":
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".xls":
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case ".jpeg":
                    return "image/jpeg";
                case ".png":
                    return "image/png";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                case ".mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".ppt":
                    return "application/mspowerpoint";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";
                default:
                    return "application/octet-stream";
            }
        }
    }

    public class HttpMethodHelper
    {
        public static HttpWebResponse HttpGet(string uri, CookieContainer cookiescontainer)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
            if (cookiescontainer != null)
            {
                webRequest.CookieContainer = cookiescontainer;
            }
            else
            {
                webRequest.CookieContainer = new CookieContainer();
            }

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                return (HttpWebResponse)webResponse;
            }
            catch
            {
            }
            return null;
        }

        public static HttpWebResponse HttpPost(string uri, string parameters, CookieContainer cookiescontainer)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            if (cookiescontainer != null)
            {
                webRequest.CookieContainer = cookiescontainer;
            }
            else
            {
                webRequest.CookieContainer = new CookieContainer();
            }
            byte[] bytes = Encoding.ASCII.GetBytes(parameters);
            Stream os = null;
            try
            {
                webRequest.ContentLength = bytes.Length;
                os = webRequest.GetRequestStream();
                os.Write(bytes, 0, bytes.Length);
            }
            catch
            {
                return null;
            }
            finally
            {
                if (os != null)
                {
                    os.Close();
                }
            }

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                return (HttpWebResponse)webResponse;
            }
            catch (Exception e)
            {
            }
            return null;
        }

        /// <summary>
        /// Uploads a stream using a multipart/form-data POST.
        /// </summary>
        /// <param name="requestUri"></param>
        /// <param name="postData">A NameValueCollection containing form fields to post with file data</param>
        /// <param name="fileData">An open, positioned stream containing the file data</param>
        /// <param name="fileName">Optional, a name to assign to the file data.</param>
        /// <param name="fileContentType">Optional. If omitted, registry is queried using <paramref name="fileName"/>. 
        /// If content type is not available from registry, application/octet-stream will be submitted.</param>
        /// <param name="fileFieldName">Optional, a form field name to assign to the uploaded file data. 
        /// If ommited the value 'file' will be submitted.</param>
        /// <param name="cookies">Optional, can pass null. Used to send and retrieve cookies. 
        /// Pass the same instance to subsequent calls to maintain state if required.</param>
        /// <param name="headers">Optional, headers to be added to request.</param>
        /// <returns></returns>
        /// Reference: 
        /// http://tools.ietf.org/html/rfc1867
        /// http://tools.ietf.org/html/rfc2388
        /// http://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.2
        /// 
        public static WebResponse PostFile(Uri requestUri, NameValueCollection postData, Stream fileData, string fileName,
                                           string fileContentType, string fileFieldName, CookieContainer cookies, NameValueCollection headers)
        {
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(requestUri);
            webrequest.Timeout = 1000 * 120;
            fileFieldName = string.IsNullOrEmpty(fileFieldName) ? "file" : fileFieldName;
            if (headers != null)
            {
                foreach (string key in headers.AllKeys)
                {
                    string[] values = headers.GetValues(key);
                    if (values != null)
                        foreach (string value in values)
                        {
                            webrequest.Headers.Add(key, value);
                        }
                }
            }
            webrequest.Method = "POST";
            if (cookies != null)
            {
                webrequest.CookieContainer = cookies;
            }
            string boundary = "----------" + DateTime.Now.Ticks.ToString("x", CultureInfo.InvariantCulture);
            webrequest.ContentType = "multipart/form-data; boundary=" + boundary;
            StringBuilder sbHeader = new StringBuilder();
            // add form fields, if any
            if (postData != null)
            {
                foreach (string key in postData.AllKeys)
                {
                    string[] values = postData.GetValues(key);
                    if (values != null)
                        foreach (string value in values)
                        {
                            sbHeader.AppendFormat("--{0}\r\n", boundary);
                            sbHeader.AppendFormat("Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}\r\n", key,
                                                  value);
                        }
                }
            }
            if (fileData != null)
            {
                sbHeader.AppendFormat("--{0}\r\n", boundary);
                sbHeader.AppendFormat("Content-Disposition: form-data; name=\"{0}\"; {1}\r\n", fileFieldName,
                                      string.IsNullOrEmpty(fileName)
                                          ?
                                              ""
                                          : string.Format(CultureInfo.InvariantCulture, "filename=\"{0}\";",
                                                          Path.GetFileName(fileName)));

                sbHeader.AppendFormat("Content-Type: {0}\r\n\r\n", fileContentType);
            }

            byte[] header = Encoding.UTF8.GetBytes(sbHeader.ToString());
            byte[] footer = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            long contentLength = header.Length + (fileData != null ? fileData.Length : 0) + footer.Length;
            //long contentLength = 372 + 52204 + 33;
            webrequest.ContentLength = contentLength;

            using (Stream requestStream = webrequest.GetRequestStream())
            {
                requestStream.Write(header, 0, header.Length);
                if (fileData != null)
                {
                    // write the file data, if any
                    byte[] buffer = new Byte[checked((uint)Math.Min(4096, (int)fileData.Length))];
                    int bytesRead;
                    while ((bytesRead = fileData.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        requestStream.Write(buffer, 0, bytesRead);
                    }
                }
                // write footer
                requestStream.Write(footer, 0, footer.Length);
                return webrequest.GetResponse();
            }
        }

        /// <summary>
        /// Uploads a file using a multipart/form-data POST.
        /// </summary>
        /// <param name="requestUri"></param>
        /// <param name="postData">A NameValueCollection containing form fields to post with file data</param>
        /// <param name="fileName">The physical path of the file to upload</param>
        /// <param name="fileContentType">Optional. If omitted, registry is queried using <paramref name="fileName"/>. 
        /// If content type is not available from registry, application/octet-stream will be submitted.</param>
        /// <param name="fileFieldName">Optional, a form field name to assign to the uploaded file data. 
        /// If ommited the value 'file' will be submitted.</param>
        /// <param name="cookies">Optional, can pass null. Used to send and retrieve cookies. 
        /// Pass the same instance to subsequent calls to maintain state if required.</param>
        /// <param name="headers">Optional, headers to be added to request.</param>
        /// <returns></returns>
        public static WebResponse PostFile(Uri requestUri, NameValueCollection postData, string fileName,
                                           string fileContentType, string fileFieldName, CookieContainer cookies,
                                           NameValueCollection headers)
        {
            using (FileStream fileData = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return PostFile(requestUri, postData, fileData, fileName, fileContentType, fileFieldName, cookies,
                                headers);
            }
        }


    }
}