﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class AdmicroServices
    {
        //call vung noi bat dung 5 tin
        public static void TranferDataNewsFocusAndTimeline(string arrid)
        {
            try
            {
                var context = HttpContext.Current;
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                {
                    var sourceName = CmsChannelConfiguration.GetAppSetting("SourceName_Admicro");
                    var url = CmsChannelConfiguration.GetAppSetting("DomainAdmHot_Admicro") ?? "http://news.admicro.vn:8182/ar";
                    var username = CmsChannelConfiguration.GetAppSetting("User_Admicro") ?? "admicro";
                    var password = CmsChannelConfiguration.GetAppSetting("Passw_Admicro") ?? "prJbMAtbuyMtXgee";

                    var task = Task.Run(() =>
                    {
                        HttpContext.Current = context;
                        Logger.WriteLog(Logger.LogType.Debug, string.Format("Start push to cd for {0} url {1}", sourceName, url));
                        using (WebClient wc = new WebClient())
                        {
                            try
                            {
                                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                var reqparm = new System.Collections.Specialized.NameValueCollection();
                                reqparm.Add("email", username);
                                reqparm.Add("password", password);
                                reqparm.Add("type", "1");
                                reqparm.Add("TopNewsId", arrid);
                                reqparm.Add("SourceName", sourceName);

                                Logger.WriteLog(Logger.LogType.Debug, string.Format("CHINHNB log CD: arrid {0}: channel: {1}", arrid, sourceName));

                                byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                                string responsebody = Encoding.UTF8.GetString(responsebytes);
                                Logger.WriteLog(Logger.LogType.Debug, string.Format("End push to cd for {0} url {1}: {2}", sourceName, url, responsebody));
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                            }
                        }
                    });
                    try
                    {
                        if (task.Wait(TimeSpan.FromSeconds(3)))
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "AdmicroServices.Push => " + ex.Message);
            }
        }

        //call co tin moi va update tin
        public static void TranferNewsToAdmicro(string action, NewsEntity news)
        {
            try
            {
                var context = HttpContext.Current;
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                {
                    var sourceName = CmsChannelConfiguration.GetAppSetting("SourceName_Admicro");
                    var url = CmsChannelConfiguration.GetAppSetting("DomainAdmNews_Admicro") ?? "http://news.admicro.vn:8182";
                    var username = CmsChannelConfiguration.GetAppSetting("User_Admicro") ?? "admicro";
                    var password = CmsChannelConfiguration.GetAppSetting("Passw_Admicro") ?? "prJbMAtbuyMtXgee";
                    var lnkUrl = CmsChannelConfiguration.GetAppSetting("LnkUrl_Admicro");
                    var domainAvatar = CmsChannelConfiguration.GetAppSetting("DomainAvatar_Admicro");
                    var sourceId = CmsChannelConfiguration.GetAppSettingInInt32("SourceId_Admicro");

                    var task = Task.Run(() =>
                    {
                        HttpContext.Current = context;
                        Logger.WriteLog(Logger.LogType.Debug, string.Format("Start push to cd Admicro for {0} url {1} action {2}", sourceName, url, action));

                        using (WebClient wc = new WebClient())
                        {
                            try
                            {
                                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                var reqparm = new System.Collections.Specialized.NameValueCollection();
                                reqparm.Add("email", username);
                                reqparm.Add("password", password);
                                reqparm.Add("NewsId", news.Id.ToString());
                                reqparm.Add("title", news.Title);
                                reqparm.Add("ZoneId", news.ZoneId.ToString());
                                var fullUrl = news.Url;
                                if (news.Url.StartsWith("http://") || news.Url.StartsWith("https://"))
                                    fullUrl = news.Url;
                                else
                                    fullUrl = lnkUrl + news.Url;
                                reqparm.Add("Url", fullUrl);
                                reqparm.Add("Sapo", news.Sapo);
                                reqparm.Add("content", news.Body);
                                var avatar = news.Avatar;
                                if (news.Avatar.StartsWith("http://") || news.Avatar.StartsWith("https://"))
                                    avatar = news.Avatar;
                                else
                                    avatar = domainAvatar + news.Avatar;
                                reqparm.Add("Avatar", avatar);
                                reqparm.Add("ChannelID", sourceId.ToString());
                                reqparm.Add("SourceName", sourceName);
                                reqparm.Add("tags", news.TagItem);
                                reqparm.Add("PublishDate", news.DistributionDate.ToString("yyyy-MM-dd HH:mm:ss"));

                                Logger.WriteLog(Logger.LogType.Debug, string.Format("CHINHNB log CD Admicro: NewsId {0}: channel: {1}: action: {2}", news.Id.ToString(), sourceName, action));

                                byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                                string responsebody = Encoding.UTF8.GetString(responsebytes);
                                Logger.WriteLog(Logger.LogType.Debug, string.Format("End push to cd Admicro for {0} url {1}: {2}: action: {3}", sourceName, url, responsebody, action));
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, "TranferNewsToAdmicro: " + ex.Message);
                            }
                        }
                    });
                    try
                    {
                        if (task.Wait(TimeSpan.FromSeconds(3)))
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "AdmicroServices.Push => " + ex.Message);
            }
        }

        //Call khi xoa tin
        public static void TranferDeleteNewsToAdmicro(string newsId)
        {
            try
            {
                var context = HttpContext.Current;
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                {
                    var sourceName = CmsChannelConfiguration.GetAppSetting("SourceName_Admicro");
                    var url = CmsChannelConfiguration.GetAppSetting("DomainAdmApi_Admicro") ?? "http://news.admicro.vn:8182/api?action=delete&amp;newsid={0}&amp;apiKey=IEYRtW2cb7A5Gs54A1wKElECBL65GVls&amp;sourceNews={1}";
                    var domainAdmApi = string.Format(url, newsId, sourceName);

                    var task = Task.Run(() =>
                    {
                        HttpContext.Current = context;
                        Logger.WriteLog(Logger.LogType.Debug, string.Format("Start push to cd Admicro for {0} url {1}", sourceName, domainAdmApi));

                        using (WebClient wc = new WebClient())
                        {
                            try
                            {
                                var result = wc.DownloadString(domainAdmApi);
                                Logger.WriteLog(Logger.LogType.Debug, string.Format("End push to cd Admicro for {0} url {1}: {2}", sourceName, domainAdmApi, result));
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                            }
                        }
                    });
                    try
                    {
                        if (task.Wait(TimeSpan.FromSeconds(3)))
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "AdmicroServices.Push => " + ex.Message);
            }
        }
    }
}