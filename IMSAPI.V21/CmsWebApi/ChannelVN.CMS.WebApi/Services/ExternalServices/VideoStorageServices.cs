﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Web;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class VideoStorageServices
    {
        #region VideoManager

        public const string VIDEO_MANAGER_HTTPSERVER = "VIDEO_MANAGER_HTTPSERVER";
        public const string VIDEO_MANAGER_THUMBSERVER = "VIDEO_MANAGER_THUMBSERVER";
        public const string VIDEO_MANAGER_HTTPDOWNLOAD = "VIDEO_MANAGER_HTTPDOWNLOAD";
        public const string VIDEO_MANAGER_HTTPUPLOAD = "VIDEO_MANAGER_HTTPUPLOAD";
        public const string VIDEO_MANAGER_HTTPUPLOAD_BACKUP = "VIDEO_MANAGER_HTTPUPLOAD_BACKUP";
        public const string VIDEO_MANAGER_SECRET_KEY = "VIDEO_MANAGER_SECRET_KEY";
        public const string VIDEO_MANAGER_CLIENT_HASH = "VIDEO_MANAGER_CLIENT_HASH";
        public const string VIDEO_MANAGER_PREFIX = "VIDEO_MANAGER_PREFIX";
        public const string VIDEO_MANAGER_INCLUDING_PREFIX = "VIDEO_MANAGER_INCLUDING_PREFIX";
        public const string VIDEO_MANAGER_EMBED_HOSTING_SSL = "VIDEO_MANAGER_EMBED_HOSTING_SSL";
        public const string VIDEO_MANAGER_EMBED_HOSTING = "VIDEO_MANAGER_EMBED_HOSTING";

        public const string VIDEO_MANAGER_CUSTOM_PNAME = "VIDEO_MANAGER_CUSTOM_PNAME";

        public const string VIDEO_MANAGER_SPLITCHAR = "VIDEO_MANAGER_SPLITCHAR";
        public const string VIDEO_MANAGER_UPLOAD_NAMESPACE = "VIDEO_MANAGER_UPLOAD_NAMESPACE";
        public const string VIDEO_MANAGER_UPLOAD_EXPIRED_TIME = "VIDEO_MANAGER_UPLOAD_EXPIRED_TIME";
        public const string VIDEO_MANAGER_UPLOAD_CALLBACK = "VIDEO_MANAGER_UPLOAD_CALLBACK";
        public const string VIDEO_MANAGER_UPLOAD_CALLBACK_UPDATE_INFO = "VIDEO_MANAGER_UPLOAD_CALLBACK_UPDATE_INFO";
        public const string VIDEO_MANAGER_UPLOAD_MAXLENGTH = "VIDEO_MANAGER_UPLOAD_MAXLENGTH";

        /* API VideoManager */
        public const string API_VIDEO_CONVERT_MP4 = "API_ConvertVideo";
        public const string API_VIDEO_RESIZE = "API_ResizeVideo";
        public const string API_VIDEO_CROP = "API_CropVideo";
        public const string API_VIDEO_GET_EMBED_CROP = "API_GetEmbedCropVideo";
        public const string API_VIDEO_CHECK_CONVERT = "API_GetUrl";
        public const string API_VIDEO_GET_EMBED_URL = "API_VideoEmbedUrl";
        public const string API_VIDEO_GET_EMBED_URL_BYKEY = "API_VideoGetEmbedByKey";
        public const string API_VIDEO_GET_MEDIA_INFO_BY_FILE_PATH = "API_VideoGetMediaInfoByPath";
        public const string VIDEO_MANAGER_UPDATE_THUMB = "API_UpdateThumb";
        public const string VIDEO_MANAGER_SYNC = "API_SyncVideo";
        public const string VIDEO_MANAGER_SETLOGOVIDEO = "API_SetLogoVideo";
        public const string VIDEO_MANAGER_TOKEN = "APiGetVideoPath.TokenKey";

        public const string API_VIDEO_UPDATE_VIDEO_CONFIG = "API_UpdateVideoConfig";
        public const string API_VIDEO_GET_VIDEO_CONFIG_BY_KEY_VIDEO = "API_GetVideoConfigByKeyVideo";

        public const string VIDEO_MANAGER_WATERMARK = "VIDEO_MANAGER_WATERMARK";
        public const string VIDEO_MANAGER_WATERMARK_POSITION = "VIDEO_MANAGER_WATERMARK_POSITION";
        public const string VIDEO_MANAGER_WATERMARK_EMBED_IN_VIDEO = "VIDEO_MANAGER_WATERMARK_EMBED_IN_VIDEO";

        public const string VIDEO_SHARELINK = "ShareLinkFormat";


        public const string ApiVideoCrawlerStatus = "API_VideoCrawlerStatus";
        public const string ApiVideoCrawlerAdd = "API_VideoCrawlerAdd";

        public const string ApiVideoCutVideo = "API_VideoCutVideo";

        public const string ApiVideoGetUrl = "API_VideoGetUrl";
        public const string ApiVideoGetMediaLinkByKey = "API_VideoGetMediaLinkByKey";
        //Cương NP - 2014-10-06
        public const string ApiVideoGetThumbVideoByPath = "API_VideoGetThumbVideoByPath";
        /* Video Youtube*/
        public const string ApiVideoYoutubeGetList = "API_VideoYoutubeGetList";
        public const string ApiVideoYoutubeGetListDownloadedByAccountName = "API_VideoYoutubeGetListDownloadedByAccountName";
        public const string ApiVideoYoutubeGetDetail = "API_VideoYoutubeGetDetail";
        public const string ApiVideoYoutubeUpload = "API_VideoYoutubeUpload";
        public const string ApiVideoYoutubeReTryUpload = "API_VideoYoutubeReTryUpload";
        public const string ApiVideoYoutubeMarkUsed = "API_VideoYoutubeMarkUsed";
        public const string ApiVideoYoutubeMarkUsedListMedia = "API_VideoYoutubeMarkUsedListMedia";
        public const string ApiVideoGetMediaKey = "API_VideoGetMediaKey";
        /* Video Cut*/
        public const string ApiVideoCutGetList = "API_VideoCutGetList";
        public const string ApiVideoCutGetDetail = "API_VideoCutGetDetail";
        public const string ApiVideoCutUpload = "API_VideoCutUpload";
        public const string ApiVideoCutMarkUsed = "API_VideoCutMarkUsed";
        #endregion


        private static readonly string HttpServer = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_HTTPSERVER);
        private static readonly string SecretKey = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_SECRET_KEY);
        private static readonly string ClientHash = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_CLIENT_HASH);
        private static readonly string TokenGetPath = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_TOKEN);
        private static readonly string Prefix = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_PREFIX);
        private static readonly bool IsIncludingPrefix = CmsChannelConfiguration.GetAppSettingInBoolean(VIDEO_MANAGER_INCLUDING_PREFIX);
        public static readonly string CustomPname = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_CUSTOM_PNAME);

        private static readonly string HttpDownload = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_HTTPDOWNLOAD);

        private static readonly string HttpUpload = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_HTTPUPLOAD);
        private static readonly string SplitChar = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_SPLITCHAR);
        private static readonly string UploadNamespace = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_UPLOAD_NAMESPACE);
        private static readonly int UploadExpiredTime = CmsChannelConfiguration.GetAppSettingInInt32(VIDEO_MANAGER_UPLOAD_EXPIRED_TIME);
        private static readonly string HttpUpdateThumb = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_UPDATE_THUMB);
        private static readonly string HttpSetLogo = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_SETLOGOVIDEO);
        private static readonly string HttpSyncVideo = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_SYNC);
        /* Video Youtube Begin*/
        private static readonly string FormatApiVideoYoutubeGetList = CmsChannelConfiguration.GetAppSetting(ApiVideoYoutubeGetList) +
            "?clientHash={0}&accountName={1}&keyword={2}&fromDate={3}&untilDate={4}&status={5}&pageIndex={6}&pageSize={7}";
        private static readonly string FormatApiVideoYoutubeGetListDownloadedByAccountName = CmsChannelConfiguration.GetAppSetting(ApiVideoYoutubeGetListDownloadedByAccountName) +
            "?listMediaId={0}&accountName={1}";
        private static readonly string FormatApiVideoYoutubeGetDetail = CmsChannelConfiguration.GetAppSetting(ApiVideoYoutubeGetDetail) +
       "?mediaId={0}";
        private static readonly string FormatApiVideoYoutubeUpload = CmsChannelConfiguration.GetAppSetting(ApiVideoYoutubeUpload);
        private static readonly string FormatApiVideoYoutubeReTryUpload = CmsChannelConfiguration.GetAppSetting(ApiVideoYoutubeReTryUpload);
        private static readonly string FormatApiVideoYoutubeMarkUsed = CmsChannelConfiguration.GetAppSetting(ApiVideoYoutubeMarkUsed);
        private static readonly string FormatApiVideoYoutubeMarkUsedListMedia = CmsChannelConfiguration.GetAppSetting(ApiVideoYoutubeMarkUsedListMedia);
        private static readonly string FormatApiVideoGetMediaKey = CmsChannelConfiguration.GetAppSetting(ApiVideoGetMediaKey) +
            "?token={0}&clientHash={1}&account={2}&path={3}&name={4}&ext={5}";
        private static readonly string FormatApiVideoGetVideoInfo = CmsChannelConfiguration.GetAppSetting(API_VIDEO_GET_MEDIA_INFO_BY_FILE_PATH) +
            "?token={0}&clientHash={1}&account={2}&path={3}&name={4}&ext={5}";
        /* Video Youtube End*/
        /* Video Cut Begin*/
        private static readonly string FormatApiVideoCutGetList = CmsChannelConfiguration.GetAppSetting(ApiVideoCutGetList) +
            "?clientHash={0}&accountName={1}&keyword={2}&fromDate={3}&untilDate={4}&status={5}&pageIndex={6}&pageSize={7}";
        private static readonly string FormatApiVideoCutGetDetail = CmsChannelConfiguration.GetAppSetting(ApiVideoCutGetDetail) +
       "?mediaId={0}";
        private static readonly string FormatApiVideoCutUpload = CmsChannelConfiguration.GetAppSetting(ApiVideoCutUpload);
        private static readonly string FormatApiVideoCutMarkUsed = CmsChannelConfiguration.GetAppSetting(ApiVideoCutMarkUsed);
        /* Video Cut End*/
        private static readonly string FormatApiGetDirectoriesList = HttpServer +
                                                                     "ls?secret_key={0}&dirname={1}&reverse={2}&sort_by={3}";

        private static readonly string FormatApiMakeDirectory = HttpServer + "make_dir?secret_key={0}&dirname={1}";
        private static readonly string FormatApiDeleteDirectory = HttpServer + "remove_dir?secret_key={0}&dirname={1}";
        private static readonly string FormatApiRenameDirectory = HttpServer + "rename?secret_key={0}&from={1}&to={2};";

        private static readonly string FormatApiCallback =
            CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_UPLOAD_CALLBACK) + "?client_hash={0}&account={1}&key={2};";

        private static readonly string FormatApiCallbackForCreateInfoByFilePath =
            CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_UPLOAD_CALLBACK_UPDATE_INFO) + "?client_hash={0}&account={1}&file_name={2}";

        private static readonly string FormatApiGetFileInfo = HttpServer + "get_file_info?secret_key={0}&filename={1}";
        private static readonly string FormatApiDeleteFile = HttpServer + "delete?secret_key={0}&filename={1}";

        private static readonly string FormatApiConvertMp4 = CmsChannelConfiguration.GetAppSetting(API_VIDEO_CONVERT_MP4) + "?client_hash={0}&account={1}&filename={2}&fileNew={3}&size={4}&format={5}";

        private static readonly string FormatApiResize = CmsChannelConfiguration.GetAppSetting(API_VIDEO_RESIZE) + "?client_hash={0}&account={1}&filename={2}&fileNew={3}&size={4}";

        private static readonly string FormatApiCrop = CmsChannelConfiguration.GetAppSetting(API_VIDEO_CROP) + "?client_hash={0}&account={1}&filename={2}&fileNew={3}&x={4}&y={5}&width={6}&height={7}";

        private static readonly string FormatApiGetEmbedCrop = CmsChannelConfiguration.GetAppSetting(API_VIDEO_GET_EMBED_CROP) + "?token={1}&client_hash={2}&account={3}&fileName={4}";

        private static readonly string FormatApiCheckConvert = CmsChannelConfiguration.GetAppSetting(API_VIDEO_CHECK_CONVERT) + "?client_hash={0}&path={1}&name={2}&ext={3}";

        private static readonly string FormatApiGetEmbed = CmsChannelConfiguration.GetAppSetting(API_VIDEO_GET_EMBED_URL);

        private static readonly string FormatApiGetEmbedByKey = CmsChannelConfiguration.GetAppSetting(API_VIDEO_GET_EMBED_URL_BYKEY) + "?token={0}&key={1}";

        private static readonly string FormatVideoCrawlerStatus = CmsChannelConfiguration.GetAppSetting(ApiVideoCrawlerStatus) + "?k={0}&u={1}&a={2}";

        private static readonly string FormatVideoCrawlerAdd = CmsChannelConfiguration.GetAppSetting(ApiVideoCrawlerAdd) + "?k={0}&a={1}&u={2}&p={3}";


        private static readonly string FormatVideoCutVideo = CmsChannelConfiguration.GetAppSetting(ApiVideoCutVideo) + "?_account={0}&in_file={1}&out_file={2}&client_hash={3}&start={4}&duration={5}&token={6}";


        private static readonly string FormatVideoGetUrl = CmsChannelConfiguration.GetAppSetting(ApiVideoGetUrl) + "?mk={0}";
        private static readonly string FormatVideoGetMediaLinkByKey = CmsChannelConfiguration.GetAppSetting(ApiVideoGetMediaLinkByKey) + "?token={0}&key={1}";
        private static readonly string FormatVideoGetThumbVideoByPath = CmsChannelConfiguration.GetAppSetting(ApiVideoGetThumbVideoByPath) + "?secret_key={0}&filename={1}";

        private static readonly string FormatApiUpdateVideoConfig = CmsChannelConfiguration.GetAppSetting(API_VIDEO_UPDATE_VIDEO_CONFIG);
        private static readonly string FormatApiGetVideoConfigByKeyVideo = CmsChannelConfiguration.GetAppSetting(API_VIDEO_GET_VIDEO_CONFIG_BY_KEY_VIDEO);

        #region Video Youtube methods
        public static ResponseMediaList VideoYoutubeList(string accountName, string keyword, string fromDate, string untilDate, int status, int pageIndex, int pageSize)
        {
            var result = string.Empty;
            var apiUrl = string.Format(FormatApiVideoYoutubeGetList, ClientHash, accountName, keyword, fromDate, untilDate, status, pageIndex, pageSize);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("GET failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
                if (!string.IsNullOrEmpty(result))
                {
                    var data = NewtonJson.Deserialize<ResponseMediaList>(result);
                    if (data != null) return data;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return new VideoStorageServices.ResponseMediaList();
        }
        public static ResponseMediaYoutubeShortInfoList VideoYoutubeListDownloadedByAccountName(string listMediaId, string accountName)
        {
            var result = string.Empty;
            var apiUrl = string.Format(FormatApiVideoYoutubeGetListDownloadedByAccountName, listMediaId, accountName);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("GET failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
                if (!string.IsNullOrEmpty(result))
                {
                    var data = NewtonJson.Deserialize<ResponseMediaYoutubeShortInfoList>(result);
                    if (data != null) return data;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return new VideoStorageServices.ResponseMediaYoutubeShortInfoList();
        }
        public static ResponseMedia VideoYoutubeDetail(int mediaId)
        {
            var apiUrl = string.Format(FormatApiVideoYoutubeGetDetail, mediaId);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";
                string result;
                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("GET failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }

                if (!string.IsNullOrEmpty(result))
                {
                    var data = NewtonJson.Deserialize<ResponseMedia>(result);
                    if (data != null) return data;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public class ResponseMediaAction
        {
            public string Message { get; set; }
            public bool Success { get; set; }
        }
        public class ResponseYoutubeMediaAction
        {
            public ResponseMediaEntity UploadMediaResult { get; set; }
        }
        public class ResponseMediaEntity
        {
            public string Message { get; set; }
            public bool Success { get; set; }
        }
        public static ResponseMediaAction VideoYoutubeUpload(string url, string accountName)
        {
            //var description = "";
            //var filePath = "";
            //var path = filePath.Substring(0, filePath.LastIndexOf('/') + 1);
            //var fileName = filePath.Substring(filePath.LastIndexOf('/') + 1);
            //var ext = fileName.Substring(fileName.LastIndexOf('.'));
            //fileName = fileName.Substring(0, fileName.LastIndexOf('.'));var description = "";
            var encryptAccountName = EncryptUserName(accountName);
            var path = encryptAccountName + "/" + string.Format("{0:yyyy/MM/dd}", DateTime.Now) + "/";

            var description = "";
            var fileName = "";
            var ext = "";
            fileName = "";
            var result = new ResponseMediaAction() { Success = false };
            var strNewValue = "{\"clientHash\":\"" + ClientHash + "\"," + "\"secretKey\":\"" + SecretKey + "\"," + "\"accountName\":\"" + accountName + "\"," +
                "\"url\":\"" + url + "\"," + "\"path\":\"" + path + "\", \"name\":\"" + fileName + "\", \"ext\":\"" + ext + "\"," + "\"description\":\"" + description + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiVideoYoutubeUpload);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var errorMessage = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(errorMessage);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = NewtonJson.Deserialize<ResponseMediaAction>(stIn.ReadToEnd());
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }

        public static ResponseMediaEntity VideoYoutubeUploadWithDescription(string url, string description, string accountName)
        {
            //var description = "";
            //var filePath = "";
            //var path = filePath.Substring(0, filePath.LastIndexOf('/') + 1);
            //var fileName = filePath.Substring(filePath.LastIndexOf('/') + 1);
            //var ext = fileName.Substring(fileName.LastIndexOf('.'));
            //fileName = fileName.Substring(0, fileName.LastIndexOf('.'));var description = "";
            var encryptAccountName = EncryptUserName(accountName);
            var path = encryptAccountName + "/" + string.Format("{0:yyyy/MM/dd}", DateTime.Now) + "/";

            //var description = "";
            var fileName = "";
            var ext = "";
            fileName = "";
            var result = new ResponseYoutubeMediaAction() { UploadMediaResult = new ResponseMediaEntity() { Success = false } };
            var strNewValue = "{\"clientHash\":\"" + ClientHash + "\"," + "\"secretKey\":\"" + SecretKey + "\"," + "\"accountName\":\"" + accountName + "\"," +
                "\"url\":\"" + url + "\"," + "\"path\":\"" + path + "\", \"name\":\"" + fileName + "\", \"ext\":\"" + ext + "\"," + "\"description\":\"" + description + "\"}";
            Logger.WriteLog(Logger.LogType.Debug, strNewValue);
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiVideoYoutubeUpload);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var errorMessage = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(errorMessage);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = NewtonJson.Deserialize<ResponseYoutubeMediaAction>(stIn.ReadToEnd());
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result.UploadMediaResult;
        }

        public static string RetryDownloadVideoYoutube(int mediaId, string accountName)
        {

            var result = string.Empty;
            var strNewValue = "{\"clientHash\":\"" + ClientHash + "\"," + "\"secretKey\":\"" + SecretKey + "\"," + "\"accountName\":\"" + accountName + "\"," +
                "\"mediaId\":\"" + mediaId + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiVideoYoutubeReTryUpload);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }
        public static string MarkUsed(int mediaId)
        {
            var result = string.Empty;
            var strNewValue = "{\"clientHash\":\"" + ClientHash + "\"," + "\"secretKey\":\"" + SecretKey + "\"," + "\"mediaId\":\"" + mediaId + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiVideoYoutubeMarkUsed);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }
        public static string MarkUsedListMedia(string listMediaId, string accountName)
        {
            var result = string.Empty;
            var strNewValue = "{\"clientHash\":\"" + ClientHash + "\",\"secretKey\":\"" + SecretKey + "\",\"listMediaId\":\"" + listMediaId + "\"," + "\"accountName\":\"" + accountName + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiVideoYoutubeMarkUsedListMedia);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }
        public static string VideoGetMediaKey(string account, string path, string name, string ext)
        {
            var token = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_TOKEN);
            var apiUrl = string.Format(FormatApiVideoGetMediaKey, token, ClientHash, account, path, name, ext);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";
                string result;
                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("GET failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }

                if (!string.IsNullOrEmpty(result))
                {
                    var data = result.Replace("\"", "");
                    return data;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }
        public static VideoInfo VideoGetInfo(string account, string path, string name, string ext)
        {
            var token = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_TOKEN);
            var apiUrl = string.Format(FormatApiVideoGetVideoInfo, token, ClientHash, account, path, name, ext);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";
                string result;
                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("GET failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }

                return NewtonJson.Deserialize<VideoInfo>(result);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }
        public enum EnumStatusVideoYoutube
        {
            /// <summary>
            /// Dùng để lọc trên danh sách
            /// </summary>
            All = -1,
            /// <summary>
            /// Đang xếp hàng đợi tới lượt tải
            /// </summary>
            Pending = 0,
            /// <summary>
            /// Đang trong quá trình tải
            /// </summary>
            Downloading = 1,
            /// <summary>
            /// Đã tải thành công
            /// </summary>
            Completed = 2,
            /// <summary>
            /// Đã tải nhưng bị lỗi
            /// </summary>
            Fail = 3,
            /// <summary>
            /// Đã sử dụng
            /// </summary>
            MarkUsed = 4
        }

        public class VideoInfo
        {
            public string ClientHash { get; set; }
            public string Created { get; set; }
            public string Embed { get; set; }
            public string MediaDuration { get; set; }
            public string MediaExt { get; set; }
            public string MediaHeight { get; set; }
            public string MediaId { get; set; }
            public string MediaKey { get; set; }
            public string MediaName { get; set; }
            public string MediaPath { get; set; }
            public string MediaSize { get; set; }
            public string MediaTag { get; set; }
            public string MediaThumb { get; set; }
            public string MediaTitle { get; set; }
            public string MediaUser { get; set; }
            public string MediaWidth { get; set; }
            public string SysEmbedFormat { get; set; }
            public string SysEmbedId { get; set; }
            public string UserEmbedH { get; set; }
            public string UserEmbedW { get; set; }
        }

        public class ResponseMediaList
        {
            public string Message { get; set; }
            public bool Success { get; set; }
            public int TotalRows { get; set; }
            public List<MediaYoutubeEntity> MediaEntities { get; set; }
        }
        public class MediaYoutubeShortInfoEntity
        {
            public int Id { get; set; }
            public string FilePath { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string MediaKey { get; set; }
            public string MediaThumb { get; set; }
            public string ThumbYoutube { get; set; }
            public string Embed { get; set; }

        }
        public class ResponseMediaYoutubeShortInfoList
        {
            public string Message { get; set; }
            public bool Success { get; set; }
            public int TotalRows { get; set; }
            public List<MediaYoutubeShortInfoEntity> MediaShortInfoEntities { get; set; }
        }
        public class ResponseMedia
        {
            public string Message { get; set; }
            public bool Success { get; set; }
            public int TotalRows { get; set; }
            public MediaYoutubeEntity MediaEntity { get; set; }
        }
        public class MediaYoutubeEntity
        {
            private DateTime _createDate;
            private bool _isCreatedDateNull = true;
            private DateTime _modifiedDate;
            private bool _isModifiedDateNull = true;
            private DateTime _completedDate;
            private bool _isCompletedDateNull = true;

            /// <summary>
            /// MediaId
            /// </summary>
            public int Id { get; set; }

            /// <summary>
            /// Link youtube
            /// </summary>
            public string Url { get; set; }

            /// <summary>
            /// Người tải
            /// </summary>
            public string AccountName { get; set; } //nvarchar(50),>

            /// <summary>
            /// ClientHash của video
            /// </summary>
            public string ClientHash { get; set; } //nvarchar(50),>

            /// <summary>
            /// SecretKey
            /// </summary>
            public string SecretKey { get; set; } // nvarchar(300)

            /// <summary>
            /// Trạng thái
            /// </summary>
            public int Status { get; set; } //int,>


            public bool IsCreatedDateNull
            {
                get { return _isCreatedDateNull; }
                set { _isCreatedDateNull = value; }
            }

            /// <summary>
            /// Ngày tải
            /// </summary>
            public DateTime CreatedDate
            {
                get
                {
                    if (IsCreatedDateNull) _createDate = DateTime.MinValue;
                    return _createDate;
                }
                set
                {
                    _isCreatedDateNull = false;
                    _createDate = value;
                }
            } //datetime,>

            /// <summary>
            /// Ngày tải lưu dạng UNIX
            /// </summary>
            public long CreatedDateStamp { get; set; } //bigint,>

            public bool IsModifiedDateNull
            {
                get { return _isModifiedDateNull; }
                set { _isModifiedDateNull = value; }
            }

            /// <summary>
            /// Ngày updat trạng thái
            /// </summary>
            public DateTime ModifiedDate
            {
                get
                {
                    if (IsModifiedDateNull) _modifiedDate = DateTime.MinValue;
                    return _modifiedDate;
                }
                set
                {
                    _isModifiedDateNull = false;
                    _modifiedDate = value;
                }
            } //datetime,>

            /// <summary>
            /// Ngày updat trạng thái, lưu dạng UNIX
            /// </summary>
            public long ModifiedDateStamp { get; set; } //bigint,>

            public bool IsCompletedDateNull
            {
                get { return _isCompletedDateNull; }
                set { _isCompletedDateNull = value; }
            }

            /// <summary>
            /// Ngày hoàn thành
            /// </summary>
            public DateTime CompletedDate
            {
                get
                {
                    if (IsCompletedDateNull) _completedDate = DateTime.MinValue;
                    return _completedDate;
                }
                set
                {
                    _isCompletedDateNull = false;
                    _completedDate = value;
                }
            } //datetime,>

            /// <summary>
            /// Ngày hoàn thành, lưu dạng UNIX
            /// </summary>
            public long ComplatedDateStamp { get; set; } //bigint,>

            /// <summary>
            /// Đường dẫn file
            /// </summary>
            public string Path { get; set; }

            /// <summary>
            /// Tên file
            /// </summary>
            public string Name { get; set; } //nvarchar(500),>

            /// <summary>
            /// Mở rộng file ("đuôi" file: .mp4,.flv,...)
            /// </summary>
            public string Extension { get; set; }

            /// <summary>
            /// Mô tả
            /// </summary>
            public string Description { get; set; } //nvarchar(max),>

            /// <summary>
            /// Video key
            /// </summary>
            public string MediaKey { get; set; } //nvarchar(100)

            /// <summary>
            /// Tiêu đề video, mặc định lấy từ Youtube
            /// </summary>
            public string Title { get; set; }  //nvarchar(1000)

            /// <summary>
            /// Ảnh thumb lấy từ MediaPlatform
            /// </summary>
            public string MediaThumb { get; set; }//nvarchar(1000)

            /// <summary>
            /// Ảnh thumb lấy từ Youtube
            /// </summary>
            public string ThumbYoutube { get; set; }//nvarchar(1000)
            /// <summary>
            /// Mã nhúng video
            /// </summary>
            public string Embed { get; set; }

        }
        #endregion

        #region Video Cut methods
        public static ResponseMediaEditedList VideoCutList(string accountName, string keyword, string fromDate, string untilDate, int status, int pageIndex, int pageSize)
        {
            var result = string.Empty;
            var apiUrl = string.Format(FormatApiVideoCutGetList, ClientHash, accountName, keyword, fromDate, untilDate, status, pageIndex, pageSize);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("GET failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
                if (!string.IsNullOrEmpty(result))
                {
                    var data = NewtonJson.Deserialize<ResponseMediaEditedList>(result);
                    if (data != null) return data;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }
        public static ResponseMediaEdited VideoCutDetail(int mediaId)
        {
            var apiUrl = string.Format(FormatApiVideoCutGetDetail, mediaId);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";
                string result;
                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("GET failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }

                if (!string.IsNullOrEmpty(result))
                {
                    var data = NewtonJson.Deserialize<ResponseMediaEdited>(result);
                    if (data != null) return data;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public static string VideoCutUpload(string accountName, string title, string originalFile, string cutFile, string start, string duration)
        {
            var result = string.Empty;
            var strNewValue = "{\"clientHash\":\"" + ClientHash + "\"," + "\"secretKey\":\"" + SecretKey + "\"," + "\"accountName\":\"" + accountName + "\"," +
                 "\"title\":\"" + title + "\"," + "\"originalFile\":\"" + originalFile + "\"," + "\"cutFile\":\"" + cutFile + "\", \"start\":\"" + start + "\", \"duration\":\"" + duration + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiVideoCutUpload);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }
        public static string CutMarkUsed(int mediaId)
        {
            var result = string.Empty;
            var strNewValue = "{\"clientHash\":\"" + ClientHash + "\"," + "\"secretKey\":\"" + SecretKey + "\"," + "\"mediaId\":\"" + mediaId + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiVideoCutMarkUsed);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }
        public class MediaEditedEntity
        {
            private DateTime _createDate;
            private bool _isCreatedDateNull = true;
            private DateTime _modifiedDate;
            private bool _isModifiedDateNull = true;
            private DateTime _completedDate;
            private bool _isCompletedDateNull = true;

            /// <summary>
            /// MediaId
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// Title
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// Link youtube
            /// </summary>
            public string Url { get; set; }

            /// <summary>
            /// Người tải
            /// </summary>
            public string AccountName { get; set; } //nvarchar(50),>

            /// <summary>
            /// ClientHash của video
            /// </summary>
            public string ClientHash { get; set; } //nvarchar(50),>

            /// <summary>
            /// SecretKey
            /// </summary>
            public string SecretKey { get; set; } // nvarchar(300)

            /// <summary>
            /// Trạng thái
            /// </summary>
            public int Status { get; set; } //int,>

            public bool IsCreatedDateNull
            {
                get { return _isCreatedDateNull; }
                set { _isCreatedDateNull = value; }
            }

            /// <summary>
            /// Ngày tải
            /// </summary>
            public DateTime CreatedDate
            {
                get
                {
                    if (IsCreatedDateNull) _createDate = DateTime.MinValue;
                    return _createDate;
                }
                set
                {
                    _isCreatedDateNull = false;
                    _createDate = value;
                }
            } //datetime,>

            /// <summary>
            /// Ngày tải lưu dạng UNIX
            /// </summary>
            public long CreatedDateStamp { get; set; } //bigint,>

            public bool IsModifiedDateNull
            {
                get { return _isModifiedDateNull; }
                set { _isModifiedDateNull = value; }
            }

            /// <summary>
            /// Ngày updat trạng thái
            /// </summary>
            public DateTime ModifiedDate
            {
                get
                {
                    if (IsModifiedDateNull) _modifiedDate = DateTime.MinValue;
                    return _modifiedDate;
                }
                set
                {
                    _isModifiedDateNull = false;
                    _modifiedDate = value;
                }
            } //datetime,>

            /// <summary>
            /// Ngày updat trạng thái, lưu dạng UNIX
            /// </summary>
            public long ModifiedDateStamp { get; set; } //bigint,>

            public bool IsCompletedDateNull
            {
                get { return _isCompletedDateNull; }
                set { _isCompletedDateNull = value; }
            }

            /// <summary>
            /// Ngày hoàn thành
            /// </summary>
            public DateTime CompletedDate
            {
                get
                {
                    if (IsCompletedDateNull) _completedDate = DateTime.MinValue;
                    return _completedDate;
                }
                set
                {
                    _isCompletedDateNull = false;
                    _completedDate = value;
                }
            } //datetime,>

            /// <summary>
            /// Ngày hoàn thành, lưu dạng UNIX
            /// </summary>
            public long ComplatedDateStamp { get; set; } //bigint,>

            /// <summary>
            /// Đường dẫn file
            /// </summary>
            public string OriginalFileName { get; set; }

            /// <summary>
            /// Tên file
            /// </summary>
            public string CutFileName { get; set; } //nvarchar(500),>

            /// <summary>
            /// Mở rộng file ("đuôi" file: .mp4,.flv,...)
            /// </summary>
            public string Start { get; set; }

            /// <summary>
            /// Mô tả
            /// </summary>
            public string Duration { get; set; } //nvarchar(max),>

            /// <summary>
            /// Video key
            /// </summary>
            public string MediaKey { get; set; } //nvarchar(100)

            /// <summary>
            /// Ảnh thumb lấy từ MediaPlatform
            /// </summary>
            public string MediaThumb { get; set; }//nvarchar(1000)

            /// <summary>
            /// Mã nhúng video
            /// </summary>
            public string Embed { get; set; }
        }
        public class ResponseMediaEdited
        {
            public string Message { get; set; }
            public bool Success { get; set; }
            public int TotalRows { get; set; }
            public MediaEditedEntity MediaEntity { get; set; }
        }
        public class ResponseMediaEditedList
        {
            public string Message { get; set; }
            public bool Success { get; set; }
            public int TotalRows { get; set; }
            public List<MediaEditedEntity> MediaEntities { get; set; }
        }
        public enum EnumStatusVideoCut
        {
            /// <summary>
            /// Dùng để lọc trên danh sách
            /// </summary>
            All = -1,
            /// <summary>
            /// Đang xếp hàng đợi tới lượt tải
            /// </summary>
            Pending = 0,
            /// <summary>
            /// Đang trong quá trình tải
            /// </summary>
            Downloading = 1,
            /// <summary>
            /// Đã tải thành công
            /// </summary>
            Completed = 2,
            /// <summary>
            /// Đã tải nhưng bị lỗi
            /// </summary>
            Fail = 3,
            /// <summary>
            /// Đã sử dụng
            /// </summary>
            MarkUsed = 4
        }
        #endregion
        public static ChannelvnDirInfo ListDir(string dirName, int order, string sortBy)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiGetDirectoriesList, SecretKey, dirName, order, sortBy));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            try
            {
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null) return null;

                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                return DeserializeDirInfo(result);
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static VideoDirInfo ListDirAndFile(string dirName, int order, string sortBy)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;
            var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiGetDirectoriesList, SecretKey, dirName, order, sortBy));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            try
            {
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null) return null;

                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var videoDirInfo = new VideoDirInfo();
                var dirInfo = DeserializeDirInfo(result);
                var fileList = new List<VideoFileInfo>();
                if (dirInfo != null)
                {
                    videoDirInfo.DirInfo = dirInfo;
                    if (dirInfo.files != null)
                    {
                        var path = dirName;
                        if (dirName[dirName.Length - 1] != '/') path += "/";
                        fileList.AddRange(dirInfo.files.Select(file => GetFileInfo(path + file)));
                        videoDirInfo.FileList = fileList;
                    }
                }
                else videoDirInfo = null;
                return videoDirInfo;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static string MakeDirectory(string dirName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;

            var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiMakeDirectory, SecretKey, dirName));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            try
            {
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                {
                    return null;
                }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static string RenameDirectory(string dirName, string newDirName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;

            var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiRenameDirectory, SecretKey, dirName, newDirName));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            try
            {
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                {
                    return null;
                }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static string DeleteDirectory(string dirName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;

            var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiDeleteDirectory, SecretKey, dirName));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            try
            {
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                {
                    return null;
                }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static VideoFileInfo GetFileInfo(string fileName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) fileName = Prefix + "/" + fileName;
            //Logger.WriteLog(Logger.LogType.Debug, "GetFileInfo() => " + string.Format(FormatApiGetFileInfo, SecretKey, fileName));
            var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiGetFileInfo, SecretKey, fileName));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            try
            {
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null) return null;

                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                return DeserializeFileInfo(result);
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static bool GetFullFileInfoByFilePath(string filePath, string accountName, out VideoFileInfo videoFileInfo,
            out VideoInfo videoInfo)
        {
            try
            {
                var path = filePath.Replace(System.IO.Path.GetFileName(filePath), "");
                var filename = Path.GetFileNameWithoutExtension(filePath);
                var extension = Path.GetExtension(filePath);

                videoInfo = VideoStorageServices.VideoGetInfo(accountName, path, filename, extension);
                videoFileInfo = VideoStorageServices.GetFileInfo(filePath);

                return videoInfo != null && videoFileInfo != null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            videoInfo = null;
            videoFileInfo = null;
            return false;
        }

        public static string DeleteFile(string dirName)
        {
            if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix)) dirName = Prefix + "/" + dirName;

            var webRequest = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiDeleteFile, SecretKey, dirName));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            try
            {
                // get the response
                var webResponse = webRequest.GetResponse();
                if (webResponse == null)
                {
                    return null;
                }
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();

                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static bool VideoCallbackUploadForCreateInfoByFilePath(string fileName)
        {
            try
            {
                var videoCallback = string.Format(FormatApiCallbackForCreateInfoByFilePath, ClientHash, "admin", fileName);
                WebRequest request = WebRequest.Create(videoCallback);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return false;
        }

        public static string[] CreatePolicy(string userName, string path, bool useWatermark = false, bool convertMp3Version = false)
        {
            try
            {
                var addMin = UploadExpiredTime > 0 ? UploadExpiredTime : 25; // minutes
                var strRandom = Guid.NewGuid().ToString("N").Substring(0, 5);
                var videoCallback = string.Format(FormatApiCallback, ClientHash, userName, strRandom);
                var maxRequest = CmsChannelConfiguration.GetAppSettingInInt64(VIDEO_MANAGER_UPLOAD_MAXLENGTH);
                var useWaterMark = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_WATERMARK);
                var waterMarkPosition = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_WATERMARK_POSITION);
                var embedWaterMarkIntoVideo = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_WATERMARK_EMBED_IN_VIDEO);

                var policy = "{";
                policy += "\"expires\": \"" + DateTime.Now.AddMinutes(addMin).ToString("r") + "\",";
                policy += "\"namespace\": \"" + UploadNamespace + "\",";
                policy += "\"directory\": \"" + path + "\",";
                policy += "\"convert\": \"1\",";
                policy += "\"filename-prefix\": \"\",";
                policy += "\"filename-suffix\": \"-" + strRandom + "\",";
                policy += "\"callback_url\": \"" + videoCallback + "\",";
                policy += "\"content-length-range\": \"2048-" + maxRequest + "\"";
                //policy += ",\"content-type\": \"video/x-flv\"";

                if (convertMp3Version)
                {
                    policy += "\"mp3\": \"1\",";
                }

                if (!useWatermark)
                {
                    if (!string.IsNullOrEmpty(useWaterMark))
                    {
                        policy += "\"watermark\": \"" + useWaterMark + "\",";
                        //policy += "\"watermark_position\": \"" + waterMarkPosition + "\",";
                    }
                }
                else
                {
                    policy += "\"watermark\": \"1\",";
                }
                policy += "}";

                var signature = CreateSignature(SecretKey, policy);

                return new[] { strRandom, Base64.Encode(policy), signature, path };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static string[] CreatePolicyByFile(string userName, string path, string filename, bool useWatermark = false, bool convertMp3Version = false)
        {
            try
            {
                var addMin = UploadExpiredTime > 0 ? UploadExpiredTime : 25; // minutes
                var strRandom = Guid.NewGuid().ToString("N").Substring(0, 5);
                var videoCallback = string.Format(FormatApiCallback, ClientHash, userName, strRandom);
                var maxRequest = CmsChannelConfiguration.GetAppSettingInInt64(VIDEO_MANAGER_UPLOAD_MAXLENGTH);
                var useWaterMark = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_WATERMARK);
                var waterMarkPosition = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_WATERMARK_POSITION);
                var embedWaterMarkIntoVideo = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_WATERMARK_EMBED_IN_VIDEO);

                var policy = "{";
                policy += "\"expires\": \"" + DateTime.Now.AddMinutes(addMin).ToString("r") + "\",";
                policy += "\"namespace\": \"" + UploadNamespace + "\",";
                policy += "\"directory\": \"" + path + "\",";
                policy += "\"filename\": \"" + filename + "\",";
                policy += "\"convert\": \"1\",";
                policy += "\"filename-prefix\": \"\",";
                policy += "\"filename-suffix\": \"-" + strRandom + "\",";
                policy += "\"callback_url\": \"" + videoCallback + "\",";

                if (convertMp3Version)
                {
                    policy += "\"mp3\": \"1\",";
                }

                if (!useWatermark)
                {
                    if (IsVideoFile(filename))
                    {
                        if (!string.IsNullOrEmpty(embedWaterMarkIntoVideo))
                        {
                            policy += "\"watermark\": \"" + embedWaterMarkIntoVideo + "\",";
                            //policy += "\"watermark_position\": \"" + waterMarkPosition + "\",";
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(useWaterMark))
                        {
                            policy += "\"watermark\": \"" + useWaterMark + "\",";
                            //policy += "\"watermark_position\": \"" + waterMarkPosition + "\",";
                        }
                    }
                }
                else
                {
                    policy += "\"watermark\": \"1\",";
                }

                policy += "\"content-length-range\": \"2048-" + maxRequest + "\"";
                //policy += ",\"content-type\": \"video/x-flv\"";
                policy += "}";
                //Logger.WriteLog(Logger.LogType.Trace, "CreatePolicyByFile[" + fileName + " | " + IsVideoFile(fileName) + "] => " + policy);

                var signature = CreateSignature(SecretKey, policy);

                return new[] { strRandom, Base64.Encode(policy), signature, path };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string CreateSignature(string secretKey, string policy)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] policyBytes = encoding.GetBytes(policy);
            string base64Policy = Convert.ToBase64String(policyBytes);

            byte[] secretKeyBytes = encoding.GetBytes(secretKey);
            HMACSHA256 hmacsha1 = new HMACSHA256(secretKeyBytes);

            byte[] base64PolicyBytes = encoding.GetBytes(base64Policy);
            byte[] signatureBytes = hmacsha1.ComputeHash(base64PolicyBytes);

            return Convert.ToBase64String(signatureBytes);
        }

        public static string ConvertMp4(string filePath, string fileSize, string account)
        {
            var result = string.Empty;
            var path = filePath.Substring(0, filePath.LastIndexOf('/'));
            var fileName = filePath.Substring(filePath.LastIndexOf('/'));
            var ext = GetExtension(fileName);
            const string filetype = "mp4";
            var newFileName = path + fileName.Replace(ext, "") + filetype;
            var isExists = false;
            try
            {
                var videoInfo = GetFileInfo(newFileName);
                if (videoInfo != null && videoInfo.filename != "")
                    isExists = true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            if (!isExists)
            {
                try
                {
                    var urlConvert = string.Format(FormatApiConvertMp4, ClientHash, account, filePath, newFileName,
                                                   fileSize, filetype);
                    var req = (HttpWebRequest)WebRequest.Create(urlConvert);
                    req.Method = "GET";
                    req.ContentType = "application/json; charset=utf-8";
                    var stIn = new StreamReader(req.GetResponse().GetResponseStream());
                    result = stIn.ReadToEnd();

                    stIn.Close();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            else
            {
                result = "Video này đã được convert. Hãy chọn video khác.";
            }

            return result;
        }


        public static string Resize(string filePath, string fileSize)
        {
            var result = string.Empty;
            return result;
        }

        public static string GetEmbed(string filePath, string accountName)
        {
            var result = string.Empty;

            var path = filePath.Substring(0, filePath.LastIndexOf('/') + 1);
            var fileName = filePath.Substring(filePath.LastIndexOf('/') + 1);

            var ext = fileName.Substring(fileName.LastIndexOf('.'));

            fileName = fileName.Substring(0, fileName.LastIndexOf('.'));

            var strNewValue = "{\"account\":\"" + accountName + "\", \"client_hash\":\"" + ClientHash +
                              "\", \"path\":\"" + path + "\", \"name\":\"" + fileName + "\", \"ext\":\"" + ext + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                Logger.WriteLog(Logger.LogType.Trace, FormatApiGetEmbed);
                Logger.WriteLog(Logger.LogType.Trace, "FormatApiGetEmbed => " + strNewValue);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiGetEmbed);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }

                Logger.WriteLog(Logger.LogType.Trace, "FormatApiGetEmbed result => " + result);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }

        public static string GetEmbed(string filePath, string fileName, string fileExtension, string accountName)
        {
            var result = string.Empty;

            var strNewValue = "{\"account\":\"" + accountName + "\", \"client_hash\":\"" + ClientHash +
                              "\", \"path\":\"" + filePath + "\", \"name\":\"" + fileName + "\", \"ext\":\"" +
                              fileExtension + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FormatApiGetEmbed);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }

        public static string GetEmbed(string keyVideo)
        {
            var result = string.Empty;

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(string.Format(FormatApiGetEmbedByKey, TokenGetPath, keyVideo));
                request.Method = "GET";

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return result;
        }

        public static string EncryptUserName(string userName)
        {
            if (!string.IsNullOrEmpty(SecretKey))
            {
                userName = userName + SecretKey;
            }

            var userNameEncrypt = Crypton.EncryptForHTML(userName);
            userNameEncrypt = userNameEncrypt.Length > 30 ? userNameEncrypt.Substring(0, 30) : userNameEncrypt;
            userNameEncrypt = Utility.UnicodeToKoDauAndGach(userNameEncrypt);
            return userNameEncrypt;
        }

        public static string GetEmbedCrop(string filePath, string accountName)
        {
            var result = string.Empty;

            var apiUrl = string.Format(FormatApiGetEmbedCrop, TokenGetPath, ClientHash, accountName, filePath);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return result;
        }

        public static string UpdateThumbnail(string accountName, string videoKey, string thumb)
        {
            var result = string.Empty;

            var token = APICertificate.Certificate.CreateSignature(ClientHash, accountName);

            var strNewValue = "{\"account\":\"" + accountName + "\", \"client_hash\":\"" + ClientHash + "\", \"key\":\"" +
                              videoKey + "\", \"thumb_url\":\"" + thumb + "\", \"token\":\"" + token + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(HttpUpdateThumb);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }
        public static string UpdateVideoLogo(string accountName, string videoKey, bool hasLogo)
        {
            var result = string.Empty;

            var token = APICertificate.Certificate.CreateSignature(videoKey, videoKey);

            var strNewValue = "{\"token\":\"" + token + "\", \"logo\":\"" + hasLogo.ToString().ToLower() + "\", \"key\":\"" + videoKey + "\", \"skin\":\"\", \"adv\":\"\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(HttpSetLogo);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }

        #region Media config

        public static bool UpdateVideoConfig(string keyVideo, int configType, string configValue)
        {
            var strNewValue = NewtonJson.Serialize(new
            {
                mediaKey = keyVideo,
                type = configType,
                value = configValue
            });
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(FormatApiUpdateVideoConfig);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return false;
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    var result = stIn.ReadToEnd();
                    stIn.Close();

                    return (result.IndexOf("\"Code\":200") > 0);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return false;
        }
        public static List<MediaConfigEntity> GetVideoConfigByKeyVideo(string keyVideo)
        {
            var strNewValue = NewtonJson.Serialize(new
            {
                mediaKey = keyVideo
            });
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(FormatApiGetVideoConfigByKeyVideo);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return new List<MediaConfigEntity>();
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    var result = stIn.ReadToEnd();
                    stIn.Close();
                    var list = NewtonJson.Deserialize<MediaConfigListEntity>(result);
                    return list.GetMediaConfigByMediaKeyResult;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return new List<MediaConfigEntity>();
        }

        #region Config => Interactive sticker
        public static bool UpdateVideoConfigForInteractiveSticker(string keyVideo, List<InteractiveSticker> listInteractiveSticker)
        {
            return UpdateVideoConfig(keyVideo, (int)MediaConfigType.InteractiveSticker, NewtonJson.Serialize(listInteractiveSticker));
        }
        public static List<InteractiveSticker> VideoConfigForInteractiveSticker(string keyVideo)
        {
            var returnData = new List<InteractiveSticker>();

            var configs = GetVideoConfigByKeyVideo(keyVideo);
            if (configs != null)
            {
                foreach (MediaConfigEntity config in configs)
                {
                    if (config.Type == (int)MediaConfigType.InteractiveSticker)
                    {
                        returnData = NewtonJson.Deserialize<List<InteractiveSticker>>(config.Value);
                        break;
                    }
                }
            }
            return returnData;
        }
        #endregion

        #region Config => Media special source
        public enum SpecialMediaSource : int
        {
            NormalClip = 0,
            EPLClip = 1,
            EPLRecut = 2
        }
        public static bool UpdateVideoConfigForSpecialMediaSource(string keyVideo, SpecialMediaSource specialMediaSouce)
        {
            return UpdateVideoConfig(keyVideo, (int)MediaConfigType.MediaSpecialSource, ((int)specialMediaSouce).ToString());
        }
        public static SpecialMediaSource VideoConfigForSpecialMediaSource(string keyVideo)
        {
            var configs = GetVideoConfigByKeyVideo(keyVideo);
            if (configs != null)
            {
                foreach (MediaConfigEntity config in configs)
                {
                    if (config.Type == (int)MediaConfigType.MediaSpecialSource)
                    {
                        var source = SpecialMediaSource.NormalClip;
                        if (Enum.TryParse(config.Value, out source))
                        {
                            return source;
                        }
                        break;
                    }
                }
            }
            return SpecialMediaSource.NormalClip;
        }
        #endregion

        #region Config => Media Expired Time
        public static bool UpdateVideoConfigForMediaExpiredTime(string keyVideo, DateTime expiredTime)
        {
            return UpdateVideoConfig(keyVideo, (int)MediaConfigType.MediaExpiredTime, expiredTime.ToString("yyyy-MM-dd HH:mm:ss"));
        }
        public static DateTime VideoConfigForMediaExpiredTime(string keyVideo)
        {
            var returnData = new List<InteractiveSticker>();

            var configs = GetVideoConfigByKeyVideo(keyVideo);
            if (configs != null)
            {
                foreach (MediaConfigEntity config in configs)
                {
                    if (config.Type == (int)MediaConfigType.MediaExpiredTime)
                    {
                        return Utility.ConvertToDateTime(config.Value);
                    }
                }
            }
            return DateTime.MinValue;
        }
        #endregion

        #endregion

        public static string SyncVideoToVideoApi(string accountName, string title, string tagNameList, int cateId,
                                                 string cateName, string cateRelationIds, string videoRelationIds,
                                                 int noAdv, bool logo, int videoStatus, string keyVideo)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(cateRelationIds)) cateRelationIds = cateRelationIds.Replace(";", ",");
            if (!string.IsNullOrEmpty(videoRelationIds)) videoRelationIds = videoRelationIds.Replace(";", ",");
            var mediaInfo = new MediaInfo
            {
                media_key = keyVideo,
                account_cms = accountName,
                cat_name = cateName,
                no_adv = noAdv.ToString(),
                catid = cateId.ToString(),
                catid_relation = cateRelationIds,
                client_hash = ClientHash,
                logo = logo.ToString(),
                lst_media_relation = videoRelationIds,
                status = videoStatus.ToString(),
                tag = "",
                title = HttpUtility.UrlEncode(title),
                token = ""
            };
            var postData = NewtonJson.Serialize(mediaInfo);
            var req = (HttpWebRequest)WebRequest.Create(HttpSyncVideo);
            req.Method = "POST";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            req.ContentType = "application/json; charset=utf-8";
            // Set the ContentLength property of the WebRequest.
            req.ContentLength = byteArray.Length;
            // Get the request stream.

            try
            {
                using (Stream dataStream = req.GetRequestStream())
                {
                    // Write the data to the request stream.
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    // Close the Stream object.
                    dataStream.Close();
                    // Get the response.
                    using (WebResponse response = req.GetResponse())
                    {
                        // Display the status.
                        Console.WriteLine("Status: {0}", ((HttpWebResponse)response).StatusDescription);
                        // Get the stream containing content returned by the server.
                        Stream streamreader = response.GetResponseStream();
                        // Open the stream using a StreamReader for easy access.
                        if (streamreader != null)
                        {
                            StreamReader reader = new StreamReader(streamreader);
                            // Read the content.
                            result = reader.ReadToEnd();
                            // Clean up the streams.
                            reader.Close();
                        }
                        dataStream.Close();
                        response.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return result;
        }

        public class InteractiveSticker
        {
            public int Type { get; set; }
            public List<Timeline> Timelines { get; set; }
        }
        public class Timeline
        {
            public string Start { get; set; }
            public string End { get; set; }
            public string KeyVideo { get; set; }
            public string Position { get; set; }
        }

        public class MediaInfo
        {
            [DataMember]
            public string client_hash { get; set; }
            [DataMember]
            public string account_cms { get; set; }
            [DataMember]
            public string token { get; set; }
            [DataMember]
            public string title { get; set; }

            [DataMember]
            public string tag { get; set; }
            [DataMember]
            public string catid { get; set; }
            [DataMember]
            public string cat_name { get; set; }

            [DataMember]
            public string catid_relation { get; set; }
            [DataMember]
            public string lst_media_relation { get; set; }
            [DataMember]
            public string media_key { get; set; }
            [DataMember]
            public string status { get; set; }
            [DataMember]
            public string no_adv { get; set; }
            [DataMember]
            public string logo { get; set; }
        }
        [DataContract]
        public class MediaConfigEntity
        {
            [DataMember]
            public string MediaKey { get; set; }
            [DataMember]
            public int Type { get; set; }
            [DataMember]
            public string Value { get; set; }
        }
        public enum MediaConfigType : int
        {
            [DataMember]
            InteractiveSticker = 1,
            [DataMember]
            MediaSpecialSource = 2,
            [DataMember]
            MediaExpiredTime = 3
        }

        [DataContract]
        public class MediaConfigListEntity
        {
            [DataMember]
            public List<MediaConfigEntity> GetMediaConfigByMediaKeyResult { get; set; }
        }

        public static string VideoCrawlerStatus(string url, string accountName)
        {
            var result = string.Empty;

            var apiUrl = string.Format(FormatVideoCrawlerStatus, ClientHash, url, accountName);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return result;

        }
        public static VideoCrawlerStatusDataEntity GetVideoCrawlerStatus(string url, string accountName)
        {
            var result = string.Empty;

            var apiUrl = string.Format(FormatVideoCrawlerStatus, ClientHash, url, accountName);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }

                if (!string.IsNullOrEmpty(result))
                {
                    var data = NewtonJson.Deserialize<VideoCrawlerStatusResultEntity>(result);
                    if (data != null) return data.d;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;

        }

        public class VideoCrawlerStatusResultEntity
        {
            public enum ReturnCode : int
            {
                UploadSuccess = 5,
                UploadFailed = 4,
                UploadInProcess = 0
            }
            public VideoCrawlerStatusDataEntity d { get; set; }
        }
        public class VideoCrawlerStatusDataEntity
        {
            public string __type { get; set; }
            public int Code { get; set; }
            public string Embed { get; set; }
            public string Message { get; set; }
            public bool Status { get; set; }
        }

        public static string VideoCrawlerAdd(string accountName, string url, string folder)
        {
            var result = string.Empty;
            var apiUrl = string.Format(FormatVideoCrawlerAdd, ClientHash, accountName, url, folder);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return result;

        }

        public static string CutVideo(string accountName, string inFile, string outFile, string start, string duration)
        {
            var result = string.Empty;
            var token = APICertificate.Certificate.CreateSignature(ClientHash, accountName);
            var strNewValue = "{\"account\":\"" + accountName + "\", \"client_hash\":\"" + ClientHash + "\", \"in_file\":\"" +
                              inFile + "\", \"out_file\":\"" + outFile + "\", \"start\":\"" + start + "\", \"duration\":\"" + duration + "\", \"token\":\"" + token + "\"}";
            byte[] bytes = Encoding.UTF8.GetBytes(strNewValue);

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(FormatVideoCutVideo);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message + "\n" + strNewValue);
            }

            return result;
        }

        public static string GetUrlVideo(string key)
        {
            var result = string.Empty;

            var apiUrl = string.Format(FormatVideoGetUrl, key);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return result;

        }
        public static string GetMediaLinkByKey(string key)
        {
            var result = string.Empty;
            var token = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_TOKEN);
            var apiUrl = string.Format(FormatVideoGetMediaLinkByKey, token, key);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return result;

        }

        public static string GetMediaLinkRedirectByKey(string key)
        {
            var result = string.Empty;
            var token = CmsChannelConfiguration.GetAppSetting(VIDEO_MANAGER_TOKEN);
            var apiUrl = string.Format(FormatVideoGetMediaLinkByKey, token, key);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return GetFinalRedirect(result);

        }

        public static string GetFinalRedirect(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
                return url;

            int maxRedirCount = 8;  // prevent infinite loops
            string newUrl = url.Replace("\\", "").Replace("\"", "");
            do
            {
                HttpWebRequest req = null;
                HttpWebResponse resp = null;
                try
                {
                    req = (HttpWebRequest)HttpWebRequest.Create(newUrl);
                    req.Method = "HEAD";
                    req.AllowAutoRedirect = false;
                    resp = (HttpWebResponse)req.GetResponse();
                    switch (resp.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            return newUrl;
                        case HttpStatusCode.Redirect:
                        case HttpStatusCode.MovedPermanently:
                        case HttpStatusCode.RedirectKeepVerb:
                        case HttpStatusCode.RedirectMethod:
                            newUrl = resp.Headers["Location"];
                            if (newUrl == null)
                                return url;

                            if (newUrl.IndexOf("://", System.StringComparison.Ordinal) == -1)
                            {
                                // Doesn't have a URL Schema, meaning it's a relative or absolute URL
                                Uri u = new Uri(new Uri(url), newUrl);
                                newUrl = u.ToString();
                            }
                            break;
                        default:
                            return newUrl;
                    }
                    url = newUrl;
                }
                catch (WebException)
                {
                    // Return the last known good URL
                    return newUrl;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (resp != null)
                        resp.Close();
                }
            } while (maxRedirCount-- > 0);

            return newUrl;
        }

        public static string GetThumbVideoByPath(string filePath)
        {
            var result = string.Empty;
            var apiUrl = string.Format(FormatVideoGetThumbVideoByPath, SecretKey, filePath);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                Logger.WriteLog(Logger.LogType.Info, apiUrl);
                request.Method = "GET";

                using (var response = (HttpWebResponse)
                                      request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(result);
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    stIn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error,
                     string.Format("key={0};filePath={1};ex={2}", SecretKey, filePath, ex));
            }
            return result;
        }


        #region Private methods

        private static ChannelvnDirInfo DeserializeDirInfo(string json)
        {
            try
            {
                return NewtonJson.Deserialize<ChannelvnDirInfo>(json);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static VideoFileInfo DeserializeFileInfo(string json)
        {
            try
            {
                return NewtonJson.Deserialize<VideoFileInfo>(json);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string GetFolder(string folderName)
        {
            try
            {
                return folderName.StartsWith(Prefix) ? folderName.Substring(Prefix.Length) : folderName;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string GetExtension(string fileName)
        {
            var count = fileName.LastIndexOf('.') + 1;
            var ext = fileName.Substring(count, fileName.Length - count);
            return ext;
        }

        private static bool IsVideoFile(string fileName)
        {
            var ext = GetExtension(fileName);
            return (ext == "mp4" || ext == "3gp" || ext == "flv" || ext == "mpeg" || ext == "mp3");
        }

        #endregion
    }
}