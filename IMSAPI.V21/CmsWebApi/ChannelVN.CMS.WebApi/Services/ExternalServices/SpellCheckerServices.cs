﻿using ChannelVN.CMS.WcfMapping.ServiceCheckSpelling;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class SpellCheckerServices
    {
        public static DCorrected CheckSpelling(string document)
        {
            return new iSCServiceClient().CheckSpelling(document);
        }

        private struct HttpRequestReturnValue
        {
            public DCorrected CheckSpellingResult;
        }
        //public static DCorrected CheckSpellingForHttpRequest(string content)
        //{
        //    var serviceRequest = CmsChannelConfiguration.GetAppSetting("SpellCheckServiceForHttpRequest");
        //    if (!serviceRequest.EndsWith("/")) serviceRequest += "/";
        //    serviceRequest += "CheckSpelling";

        //    if (content != string.Empty) content = HttpUtility.HtmlDecode(content);

        //    var objRequest = new
        //    {
        //        document = content
        //    };
        //    var strNewValue = NewtonJson.Serialize(objRequest);
        //    var bytes = Encoding.UTF8.GetBytes(strNewValue);
        //    try
        //    {
        //        var request = (HttpWebRequest)WebRequest.Create(serviceRequest);
        //        request.Method = "POST";
        //        request.ContentLength = bytes.Length;
        //        request.ContentType = "application/json; charset=utf-8";
        //        using (Stream requestStream = request.GetRequestStream())
        //        {
        //            requestStream.Write(bytes, 0, bytes.Length);
        //        }
        //        using (var response = (HttpWebResponse)request.GetResponse())
        //        {
        //            var result = "";

        //            if (response.StatusCode != HttpStatusCode.OK)
        //            {
        //                result = String.Format(
        //                    "POST failed. Received HTTP {0}",
        //                    response.StatusCode);
        //                throw new ApplicationException(result);
        //            }
        //            var stIn = new StreamReader(response.GetResponseStream());
        //            result = stIn.ReadToEnd();
        //            stIn.Close();

        //            return NewtonJson.Deserialize<HttpRequestReturnValue>(result).CheckSpellingResult;
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //    }
        //    return null;
        //}
    }
}