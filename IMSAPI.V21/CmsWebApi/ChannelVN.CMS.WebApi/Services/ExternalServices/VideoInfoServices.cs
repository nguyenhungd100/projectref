﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.WcfMapping.ServicesVideoInfo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class VideoInfoServices
    {
        //public static List<TrackingMediaInfo> GetVideoTracking(DateTime fromDate, DateTime toDate, params string[] videoKeys)
        //{
        //    try
        //    {
        //        var listMediaKeys = videoKeys.Aggregate("", (current, videoKey) => current + ("," + videoKey));
        //        if (!string.IsNullOrEmpty(listMediaKeys))
        //        {
        //            listMediaKeys = listMediaKeys.Remove(0, 1);

        //            var trackingInfo = new TrackingInfo
        //            {
        //                domains = CmsChannelConfiguration.GetAppSetting("VIDEO_MANAGER_DOMAIN"),
        //                media_keys = listMediaKeys,
        //                from_date = fromDate.ToString(CultureInfo.InvariantCulture),
        //                to_date = toDate.ToString(CultureInfo.InvariantCulture)
        //            };
        //            var videoInfo = new InfoClient();
        //            var responseObject = videoInfo.TrackingVieo(trackingInfo);
        //            if (responseObject.Status)
        //            {
        //                return new List<TrackingMediaInfo>(responseObject.Data);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.ToString());
        //    }
        //    return new List<TrackingMediaInfo>();
        //}
        public static List<TrackingMediaInfo> GetVideoTracking(string wcfServiceMethodUrl, DateTime fromDate, DateTime toDate, params string[] videoKeys)
        {
            try
            {
                if(videoKeys.Length<=0)
                    return new List<TrackingMediaInfo>();

                var listMediaKeys = videoKeys.Aggregate("", (current, videoKey) => current + ("," + videoKey));
                if (!string.IsNullOrEmpty(listMediaKeys))
                {
                    listMediaKeys = listMediaKeys.Remove(0, 1);
                    if (fromDate == DateTime.MinValue) fromDate = new DateTime(1980, 1, 1);
                    if (toDate == DateTime.MinValue) toDate = DateTime.Now;

                    var req =
                        (HttpWebRequest)WebRequest.Create(wcfServiceMethodUrl);
                    req.ContentType = "text/plain; charset=utf-8";
                    req.UserAgent = "Mozilla/5.0 (Windows NT 5.2; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0";
                    req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                    req.Method = "POST";
                    req.ContentType = "application/json; charset=utf-8";
                    req.Timeout = 10000;

                    var domain = CmsChannelConfiguration.GetAppSetting("VIDEO_MANAGER_DOMAIN");
                    //domain = "";
                    var strNewValue = "{\"media_keys\":\"" + listMediaKeys + "\", \"domains\":\"" + domain +
                                         "\", \"from_date\":\"" + fromDate.ToString("yyyy-MM-dd") + "\", \"to_date\":\"" + toDate.ToString("yyyy-MM-dd") + "\"}";
                    req.ContentLength = strNewValue.Length;


                    // Write the request
                    var strJsonReturn = "";
                    try
                    {
                        var stOut = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
                        stOut.Write(strNewValue);
                        stOut.Close();
                        var stIn = new StreamReader(req.GetResponse().GetResponseStream());
                        var strResponse = stIn.ReadToEnd();
                        stIn.Close();

                        strJsonReturn = strResponse;

                        stOut.Dispose();
                        stIn.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, strJsonReturn + ex.Message);
                    }

                    if (string.IsNullOrEmpty(strJsonReturn))
                        return new List<TrackingMediaInfo>();

                    var responseObject = NewtonJson.Deserialize<ResponseObject1>(strJsonReturn);
                    if (responseObject!=null && responseObject.Status && responseObject.Data != null)
                    {
                        return new List<TrackingMediaInfo>(responseObject.Data);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TrackingMediaInfo>();
        }
    }
}