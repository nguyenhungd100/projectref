﻿using System;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using System.Net;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class AdtechServiceForHomeStream
    {
        public static string ApiUrlFormat
        {
            get
            {
                var urlFormat = CmsChannelConfiguration.GetAppSetting("ADTECH_API_URL_FOR_HOME_STREAM");
                if (!urlFormat.EndsWith("&"))
                    urlFormat += "&";
                return urlFormat;
            }
        }

        public static bool AddNewsIntoHomeStream(int type, long newsId)
        {
            var command = "add";
            var extParam = "newsid=" + newsId + "&to=s" + type;

            var url = string.Format(ApiUrlFormat, command, extParam);

            try
            {
                //Logger.WriteLog(Logger.LogType.Warning, "AddNewsIntoHomeStream[" + type + ", " + newsId + "] => " + url);

                var updateRequest = (HttpWebRequest)WebRequest.Create(url);
                updateRequest.KeepAlive = true;
                updateRequest.Method = "GET";
                updateRequest.Timeout = 60 * 60 * 1000;
                updateRequest.ContentType = "text/xml; encoding='utf-8'";
                updateRequest.MaximumAutomaticRedirections = 10; // cho phép tối đa 10 lần request vì bên ngoài có thể redirect 1 vài lần để xóa cache các page khác. update 7/3/2015
                updateRequest.AllowAutoRedirect = true;
                updateRequest.Referer = url;
                updateRequest.UserAgent = "Mozilla/5.0";

                var updateResponse = (HttpWebResponse)updateRequest.GetResponse();

                CmsTrigger.Instance.OnChangeAdtechStream();

                return (updateResponse.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return false;
        }
        public static bool RemoveNewsIntoHomeStream(long newsId)
        {
            var command = "del";
            var extParam = "newsid=" + newsId;

            var url = string.Format(ApiUrlFormat, command, extParam);

            try
            {
                Logger.WriteLog(Logger.LogType.Debug, "RemoveNewsIntoHomeStream[" + newsId + "] => " + url);

                var updateRequest = (HttpWebRequest)WebRequest.Create(url);
                updateRequest.KeepAlive = true;
                updateRequest.Method = "GET";
                updateRequest.Timeout = 60 * 60 * 1000;
                updateRequest.ContentType = "text/xml; encoding='utf-8'";
                updateRequest.MaximumAutomaticRedirections = 10; // cho phép tối đa 10 lần request vì bên ngoài có thể redirect 1 vài lần để xóa cache các page khác. update 7/3/2015
                updateRequest.AllowAutoRedirect = true;
                updateRequest.Referer = url;
                updateRequest.UserAgent = "Mozilla/5.0";

                var updateResponse = (HttpWebResponse)updateRequest.GetResponse();

                CmsTrigger.Instance.OnChangeAdtechStream();

                return (updateResponse.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return false;
        }
        public static bool ReplaceNewsIntoHomeStream(long oldNewsId, long newNewsId)
        {
            var command = "replace";
            var extParam = "from=" + oldNewsId + "&to=" + newNewsId;

            var url = string.Format(ApiUrlFormat, command, extParam);

            try
            {
                Logger.WriteLog(Logger.LogType.Debug, "ReplaceNewsIntoHomeStream[" + oldNewsId + ", " + newNewsId + "] => " + url);

                var updateRequest = (HttpWebRequest)WebRequest.Create(url);
                updateRequest.KeepAlive = true;
                updateRequest.Method = "GET";
                updateRequest.Timeout = 60 * 60 * 1000;
                updateRequest.ContentType = "text/xml; encoding='utf-8'";
                updateRequest.MaximumAutomaticRedirections = 10; // cho phép tối đa 10 lần request vì bên ngoài có thể redirect 1 vài lần để xóa cache các page khác. update 7/3/2015
                updateRequest.AllowAutoRedirect = true;
                updateRequest.Referer = url;
                updateRequest.UserAgent = "Mozilla/5.0";

                var updateResponse = (HttpWebResponse)updateRequest.GetResponse();

                CmsTrigger.Instance.OnChangeAdtechStream();

                Logger.WriteLog(Logger.LogType.Debug, updateResponse.StatusCode.ToString());
                return (updateResponse.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return false;
        }

        public static bool ReplaceNewsAtHomeStream(long newNewsId, int stream, int position)
        {
            var command = "replaceat";
            var extParam = "newsid=" + newNewsId + "&s=" + stream + "&p=" + position;

            var url = string.Format(ApiUrlFormat, command, extParam);

            try
            {
                Logger.WriteLog(Logger.LogType.Debug, "ReplaceNewsAtHomeStream[Stream" + stream + ",Position " + position + ",NewsId " + newNewsId + "] => " + url);

                var updateRequest = (HttpWebRequest)WebRequest.Create(url);
                updateRequest.KeepAlive = true;
                updateRequest.Method = "GET";
                updateRequest.Timeout = 60 * 60 * 1000;
                updateRequest.ContentType = "text/xml; encoding='utf-8'";
                updateRequest.MaximumAutomaticRedirections = 10; // cho phép tối đa 10 lần request vì bên ngoài có thể redirect 1 vài lần để xóa cache các page khác. update 7/3/2015
                updateRequest.AllowAutoRedirect = true;
                updateRequest.Referer = url;
                updateRequest.UserAgent = "Mozilla/5.0";

                var updateResponse = (HttpWebResponse)updateRequest.GetResponse();

                CmsTrigger.Instance.OnChangeAdtechStream();

                Logger.WriteLog(Logger.LogType.Debug, updateResponse.StatusCode.ToString());
                return (updateResponse.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return false;
        }
        public static bool SetNewsIntoHomeStreamAndLockPosition(long newsId, int type, int position, DateTime fromTime, DateTime toTime)
        {
            var command = "stick";
            var extParam = "newsid=" + newsId + "&block=s" + type + "&pos=" + position + "&start=" + fromTime.ToString("yyyy-MM-dd_HH:mm") + "&end=" + toTime.ToString("yyyy-MM-dd_HH:mm");

            var url = string.Format(ApiUrlFormat, command, extParam);

            try
            {
                Logger.WriteLog(Logger.LogType.Warning, "SetNewsIntoHomeStreamAndLockPosition[" + newsId + ", " + type + ", " + position + ", " + fromTime + ", " + toTime + "] => " + url);

                var updateRequest = (HttpWebRequest)WebRequest.Create(url);
                updateRequest.KeepAlive = true;
                updateRequest.Method = "GET";
                updateRequest.Timeout = 60 * 60 * 1000;
                updateRequest.ContentType = "text/xml; encoding='utf-8'";
                updateRequest.MaximumAutomaticRedirections = 10; // cho phép tối đa 10 lần request vì bên ngoài có thể redirect 1 vài lần để xóa cache các page khác. update 7/3/2015
                updateRequest.AllowAutoRedirect = true;
                updateRequest.Referer = url;
                updateRequest.UserAgent = "Mozilla/5.0";

                var updateResponse = (HttpWebResponse)updateRequest.GetResponse();

                CmsTrigger.Instance.OnChangeAdtechStream();

                return (updateResponse.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return false;
        }
    }
}