﻿using ChannelVN.CMS.WcfMapping.ServiceSuggestTag;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class ResearchTagTokenServices
    {
        public static List<TagSugguestTagItem> GetTagSugguest(string document, int minWord, int maxWord)
        {
            try
            {
                var data = new IECServiceClient().GetTagSugguest(document, minWord, maxWord, CmsTrigger.Instance.TagSugguestChannel);
                return new List<TagSugguestTagItem>(data);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<TagSugguestTagItem>();
        }
        public static List<TagSugguestTagItem> GetTagSugguestRank(string document, int minWord, int maxWord, int minRank)
        {
            try
            {
                var data = new IECServiceClient().GetTagSugguestRank(document, minWord, maxWord, minRank, CmsTrigger.Instance.TagSugguestChannel);
                return new List<TagSugguestTagItem>(data);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<TagSugguestTagItem>();
        }
        public static List<string> GetTagSugguestAll(string document, int minWord, int maxWord)
        {
            var data = new IECServiceClient().GetTagSugguestAll(document, minWord, maxWord);
            return new List<string>(data);
        }
        public static TestTagSugguestResultTag GetTagAll(string title, string sapo, string content, int category, long newsid, int minWord, int maxWord, TagSugguestConnectionName channel = TagSugguestConnectionName.Tags)
        {
            //return new IECServiceClient().GetTagAll(title, sapo, content, CmsTrigger.Instance.TagSugguestChannel, category, newsid, minWord, maxWord);
            if (channel == TagSugguestConnectionName.Tags)
            {
                if (CmsTrigger.Instance.TagSugguestChannel == TagSugguestConnectionName.Tags)
                {
                    category = 0;
                }
                return new IECServiceClient().GetTagAll(title, sapo, content, CmsTrigger.Instance.TagSugguestChannel,
                    category, newsid, minWord, maxWord, false);
            }
            else
            {
                return new IECServiceClient().GetTagAll(title, sapo, content, channel,
                    category, newsid, minWord, maxWord, false);
            }
        }
        public static bool PostTagAccept(int category, long newsid, string[] ltag)
        {
            return true;
        }
        public static bool PostTagAccept(int category, long newsid, string url, long idSession, List<TagPostEntities> ltag, TagSugguestConnectionName channel = TagSugguestConnectionName.Tags)
        {
            return true;
            if (channel == TagSugguestConnectionName.Tags)
            {
                if (CmsTrigger.Instance.TagSugguestChannel == TagSugguestConnectionName.Tags)
                {
                    category = 0;
                }
                return new IECServiceClient().PostTagAccept(CmsTrigger.Instance.TagSugguestChannel, category, newsid, url, idSession, ltag.ToArray());
            }
            else
            {
                return new IECServiceClient().PostTagAccept(channel, category, newsid, url, idSession, ltag.ToArray());
            }
        }
    }
}