﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class AdtechServiceForViewVideo
    {
        public static string ApiUrlFormat
        {
            get
            {                
                return CmsChannelConfiguration.GetAppSetting("ADTECH_API_URL_FOR_VIDEO_VIEW");
            }
        }

        public static ViewVideo GetViewVideo(string keys, DateTime dtFromDate, DateTime dtToDate)
        {
            var listView = new ViewVideo();
            try
            {
                var query = string.Empty;
                if (dtFromDate==DateTime.MinValue && dtToDate == DateTime.MinValue)
                    query = "keys=" + keys;
                else                   
                    query = "keys=" + keys + "&from_date="+ dtFromDate.ToString("yyyy-MM-dd") + "&to_date=" + dtToDate.ToString("yyyy-MM-dd");
                var requestParamsInBytes = Encoding.UTF8.GetBytes(query);

                var httpRequest = (HttpWebRequest)WebRequest.Create(ApiUrlFormat);
                httpRequest.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
                httpRequest.ContentLength = requestParamsInBytes.Length;
                httpRequest.Method = "POST";
                httpRequest.KeepAlive = true;
                httpRequest.Timeout = 5000;
                httpRequest.MaximumResponseHeadersLength = int.MaxValue;

                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    streamWriter.Write(requestParamsInBytes, 0, requestParamsInBytes.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (var responseStream = httpResponse.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                var result = reader.ReadToEnd().Trim();
                                if (!string.IsNullOrEmpty(result))
                                {
                                    listView = NewtonJson.Deserialize<ViewVideo>(result);
                                }
                            }
                    }

                    httpResponse.Close();
                    httpResponse.Dispose();
                }

                return listView;
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError) //400 errors
                {
                    var response = ex.Response as HttpWebResponse;

                    if (response != null)
                    {
                        Logger.WriteLog(Logger.LogType.Debug, "1.CHINHNB HTTP GetViewVideo => Status Code: " + (int)response.StatusCode + " Msg=> " + ex.Message);
                    }
                }
                else
                {
                    Logger.WriteLog(Logger.LogType.Debug, "2.CHINHNB HTTP GetViewVideo => Status Code: " + ex.Status);
                }
                return listView;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetViewVideo => " + ex.ToString());
                return listView;
            }
        }
    }
    public class ViewVideo
    {
        public List<ViewVideoEntity> videos { get; set; }
    }
    public class ViewVideoEntity
    {
        public string key { get; set; }
        public string text { get; set; }
        public int play { get; set; }
    }
}