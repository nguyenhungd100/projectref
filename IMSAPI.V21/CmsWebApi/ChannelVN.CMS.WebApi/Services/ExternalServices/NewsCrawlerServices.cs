﻿using ChannelVN.CMS.WcfMapping.ServiceCrawler;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class NewsCrawlerServices
    {
        //public static ResponseInfo CrawlerRaw(string token, string url)
        //{
        //    return new ActionClient().CrawlerRaw(token, url);
        //}
        public static newsInfo CrawlerLink(string url)
        {
            try {
                return new CrawlerClient().CrawlerLink(url, true);
            }
            catch
            {
                return null;
            }
        }
    }
}