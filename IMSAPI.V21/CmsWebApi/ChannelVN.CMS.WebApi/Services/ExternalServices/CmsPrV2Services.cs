﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.SocialNetwork.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class CmsPrV2Services
    {
        private static string ApiCmsPrV2Url = CmsChannelConfiguration.GetAppSetting("ApiCmsPrV2Url");
        private static string ApiCmsPrV2SecretKey = CmsChannelConfiguration.GetAppSetting("ApiCmsPrV2SecretKey");
        private static int ApiCmsPrV2ChannelId = CmsChannelConfiguration.GetAppSettingInInt32("ApiCmsPrV2ChannelId");
        private static string ApiCmsPrV2ChannelName = CmsChannelConfiguration.GetAppSetting("ApiCmsPrV2ChannelName");
        private static string AuthorPRV2 = CmsChannelConfiguration.GetAppSetting("AuthorPRV2");
        private static Dictionary<string, string> TokenDictionary = new Dictionary<string, string> {
            { "listnews","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbWQiOiJsaXN0bmV3cyIsImlzcyI6IklNUyIsImlhdCI6MTU0MzYyMjQwMCwiZXhwIjoyMTc2MjgzMjIwfQ.JIcq4iUEw8Cgeb9iNO9-F0Q_o92dYc5eaqR3R_pCDgM" },
            { "get_news","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbWQiOiJnZXRfbmV3cyIsImlzcyI6IklNUyIsImlhdCI6MTU0MzYyMjQwMCwiZXhwIjoyMTc2MjgzMjIwfQ.ew9YnDZkLCj0jMY0WMFHaOl4JlSQqbAFto-cqaYaUm8" },
            { "get_news_extension","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbWQiOiJnZXRfbmV3c19leHRlbnNpb24iLCJpc3MiOiJJTVMiLCJpYXQiOjE1NDM2MjI0MDAsImV4cCI6MjE3NjI4MzIyMH0.Go823UasJfpB3x3IBC-xOGMxeVQt30zKEL47rYH4eCM" },
            { "get_bookinginfo","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbWQiOiJnZXRfYm9va2luZ2luZm8iLCJpc3MiOiJJTVMiLCJpYXQiOjE1NDM2MjI0MDAsImV4cCI6MjE3NjI4MzIyMH0.E4eDgp0jZqpNt54scE4N6XKKsnTXms8js5dN_9RQ-_w" },
            { "update_status","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbWQiOiJ1cGRhdGVfc3RhdHVzIiwiaXNzIjoiSU1TIiwiaWF0IjoxNTQzNjIyNDAwLCJleHAiOjIxNzYyODMyMjB9.3qaDzQCE1dTPYZQ35buTtQWJd0Cnft0nNTbSYX6x9LE" }
        };

        public async static Task<CmsPrResponseListData> GetListNewsPrByStatus(NewsPrStatus status, int zoneId, int pageIndex, int pageSize)
        {
            try {
                if (ApiCmsPrV2ChannelId < 0)
                    return new CmsPrResponseListData();

                using (var client = new HttpClient())
                {
                    var raw = new
                    {
                        status = (int)status,
                        channel_id = ApiCmsPrV2ChannelId,
                        zone_id = zoneId,
                        pageindex = pageIndex,
                        pagesize = pageSize
                    };

                    client.DefaultRequestHeaders.Add("Authorization", TokenDictionary["listnews"]);
                    var content = new StringContent(NewtonJson.Serialize(raw), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(ApiCmsPrV2Url, content);
                    response.EnsureSuccessStatusCode();

                    var responseString = await response.Content.ReadAsStringAsync();

                    var responseData = NewtonJson.Deserialize<CmsPrResponseListData>(responseString);
                    if (responseData != null)
                        return responseData;

                    return new CmsPrResponseListData();
                }
            }catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new CmsPrResponseListData();
            }           
        }       

        public async static Task<NewsEntityApi> GetNewsPrDetail(long contentId)
        {
            try
            {
                if (ApiCmsPrV2ChannelId < 0)
                    return null;

                using (var client = new HttpClient())
                {
                    var raw = new
                    {
                        news_id = contentId
                    };

                    client.DefaultRequestHeaders.Add("Authorization", TokenDictionary["get_news"]);
                    var content = new StringContent(NewtonJson.Serialize(raw), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(ApiCmsPrV2Url, content);
                    response.EnsureSuccessStatusCode();

                    var responseString = await response.Content.ReadAsStringAsync();

                    var responseData = NewtonJson.Deserialize<CmsPrResponseDetailData>(responseString);
                    if (responseData != null && responseData.data!=null)
                        return responseData.data;

                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        public async static Task<bool> UpdateNewsPrStatus(long contentId, NewsPrStatus subStatus, string message, string replyUser, long channelNewsId,long bookingId, string url, string note)
        {
            try
            {
                if (ApiCmsPrV2ChannelId < 0)
                    return false;

                using (var client = new HttpClient())
                {
                    var raw = new
                    {
                        news_id = contentId,
                        channel_news_id=channelNewsId,
                        booking_id= bookingId,
                        status = (int)subStatus,
                        url=url,
                        message=message,
                        note=note,
                        editor= replyUser
                    };

                    client.DefaultRequestHeaders.Add("Authorization", TokenDictionary["update_status"]);
                    var content = new StringContent(NewtonJson.Serialize(raw), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(ApiCmsPrV2Url, content);
                    response.EnsureSuccessStatusCode();

                    var responseString = await response.Content.ReadAsStringAsync();

                    var responseData = NewtonJson.Deserialize<CmsPrResponseUpdateStatusData>(responseString);
                    if (responseData != null && responseData.success)
                    {
                        //update permission
                        ActivityBo.LogUpdatePrStatus(WcfExtensions.WcfMessageHeader.Current.ClientUsername, message);

                        return responseData.success;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public async static Task<NewsExtensionApiEdit> GetNewsExtension(long contentId)
        {
            try
            {
                if (ApiCmsPrV2ChannelId < 0)
                    return new NewsExtensionApiEdit();

                using (var client = new HttpClient())
                {
                    var raw = new
                    {
                        news_id = contentId
                    };

                    client.DefaultRequestHeaders.Add("Authorization", TokenDictionary["get_news_extension"]);
                    var content = new StringContent(NewtonJson.Serialize(raw), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(ApiCmsPrV2Url, content);
                    response.EnsureSuccessStatusCode();

                    var responseString = await response.Content.ReadAsStringAsync();

                    var responseData = NewtonJson.Deserialize<NewsExtensionApiEdit>(responseString);
                    if (responseData != null && responseData.success)
                        return responseData;

                    return new NewsExtensionApiEdit();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new NewsExtensionApiEdit();
            }
        }

        public async static Task<BookingInfoApi> GetBookingId(int bookingId)
        {
            try
            {
                if (ApiCmsPrV2ChannelId < 0)
                    return null;

                using (var client = new HttpClient())
                {
                    var raw = new
                    {
                        bookinginfo_id = bookingId
                    };

                    client.DefaultRequestHeaders.Add("Authorization", TokenDictionary["get_bookinginfo"]);
                    var content = new StringContent(NewtonJson.Serialize(raw), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(ApiCmsPrV2Url, content);
                    response.EnsureSuccessStatusCode();

                    var responseString = await response.Content.ReadAsStringAsync();

                    var responseData = NewtonJson.Deserialize<BookingInfoApi>(responseString);
                    if (responseData != null && responseData.success)
                        return responseData;

                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }
        #region Entity

        public enum NewsPrStatus
        {
            Pending = 0,
            WaitForApprove = 1,
            Approved = 2,
            Return = 3,
            Recieved = 5,
            Published = 4
        }
        
        public class CmsPrResponseListData
        {
            public bool success { get; set; }
            public List<NewsPrWithSimpleFieldEntity> data { get; set; }
            public int total_record { get; set; }
            public string message { get; set; }

            //public int ErrorCode { get; set; }
            //public int Total { get; set; }
            //public int Type { get; set; }
            //public string Content { get; set; }
        }
                
        public class NewsPrWithSimpleFieldEntity
        {
            /// <summary>
            /// News Id
            /// </summary>
            public long ContentID { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string EncryptContentId { get { return ContentID.ToString(); } }
            /// <summary>
            /// 
            /// </summary>
            public long BookingId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string EncryptBookingId { get { return BookingId.ToString(); } }
            /// <summary>
            /// News Title
            /// </summary>
            public string Headline { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Sapo { get; set; }
            /// <summary>
            /// Ảnh đại diện
            /// </summary>
            public string Avatar { get; set; }
            public string Avatar2 { get; set; }
            public string Avatar3 { get; set; }
            public string Avatar4 { get; set; }
            public string Avatar5 { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string AvatarThumb { get; set; }
            /// <summary>
            /// Ngày tạo
            /// </summary>
            public DateTime CreatedDate { get; set; }
            /// <summary>
            /// Trạng thái
            /// </summary>
            public int Status { get; set; }
            /// <summary>
            /// Vị trí PR
            /// </summary>
            public int Mode { get; set; }
            /// <summary>
            /// Lượt xem
            /// </summary>
            public int Views { get; set; }
            /// <summary>
            /// Full Link
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// Chuyên mục
            /// </summary>
            public string ZoneTitle { get; set; }
            /// <summary>
            /// Site
            /// </summary>
            public string ChannelName { get; set; }
            /// <summary>
            /// ZoneId
            /// </summary>
            public int ZoneId { get; set; }
            /// <summary>
            /// ChannelId
            /// </summary>
            public int ChannelId { get; set; }
            /// <summary>
            /// Thời gian tồn tại 
            /// </summary>
            public int Duration { get; set; }
            /// <summary>
            /// Ngày xb
            /// </summary>
            public DateTime DeployDate { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public DateTime PublishedDate { get; set; }
            /// <summary>
            /// Người đặt booking
            /// </summary>
            public string Booker { get; set; }
            /// <summary>
            /// Người gửi
            /// </summary>
            public string LastSender { get; set; }
            /// <summary>
            /// Số hợp đồng
            /// </summary>
            public string ContractNo { get; set; }
            public string TagsItem { get; set; }
            public string Tags { get; set; }
            public int LocationId { get; set; }
            public string LocationName { get; set; }
            public int DistributionId { get; set; }
            public string Substatus { get; set; }
            public string CountUnread { get; set; }
            public int NewsType { get; set; }

        }

        public class CmsPrResponseDetailData
        {
            public bool success { get; set; }
            public string message { get; set; }
            public NewsEntityApi data { get; set; }            
        }

        public class NewsEntityApiEdit
        {            
            public NewsEntityApi NewsInfo { get; set; }
            public List<NewsExtensionApi> NewsExtension { get; set; }            
        }

        public class NewsExtensionApi
        {
            public long NewsId { get; set; }
            public int NumericValue { get; set; }
            public int Type { get; set; }
            public string Value { get; set; }
        }

        public class NewsExtensionApiEdit
        {
            public bool success { get; set; }
            public string message { get; set; }
            public List<NewsExtensionApi> data { get; set; }
        }

        public class NewsEntityApi
        {
            /// <summary>
            /// News Id
            /// </summary>
            public long ContentID { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string EncryptContentId { get { return ContentID.ToString(); } }
            /// <summary>
            /// 
            /// </summary>
            public long BookingId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string EncryptBookingId { get { return BookingId.ToString(); } }
            /// <summary>
            /// News Title
            /// </summary>
            public string Headline { get; set; }
            /// <summary>
            /// Ảnh đại diện
            /// </summary>
            public string Avatar { get; set; }
            public string Avatar2 { get; set; }
            public string Avatar3 { get; set; }
            public string Avatar4 { get; set; }
            public string Avatar5 { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string AvatarThumb { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int Approver { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Author { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string AvatarDesc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int AvatarWidth { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int AvatarHeight { get; set; }
            /// <summary>
            /// Nội dung chi tiết
            /// </summary>
            public string Body { get; set; }
            /// <summary>
            /// Mô tả ngắn
            /// </summary>
            public string Teaser { get; set; }
            /// <summary>
            /// Kiểu hiển thị
            /// </summary>
            public byte DisplayType { get; set; }
            /// <summary>
            /// Người sửa
            /// </summary>
            public string Editor { get; set; }
            /// <summary>
            /// Nguồn
            /// </summary>
            public string Source { get; set; }
            /// <summary>
            /// Tiêu đề phụ
            /// </summary>
            public string SubTitle { get; set; }
            /// <summary>
            /// ID của người cập nhật
            /// </summary>
            public int UpdateUserID { get; set; }
            /// <summary>
            /// Ngày tạo
            /// </summary>
            public DateTime CreatedDate { get; set; }
            /// <summary>
            /// Substatus
            /// </summary>
            public int Substatus { get; set; }
            /// </summary>
            /// Trạng thái
            /// </summary>
            public int Status { get; set; }
            /// <summary>
            /// Vị trí PR
            /// </summary>
            public int Mode { get; set; }
            /// <summary>
            /// Lượt xem
            /// </summary>
            public int Views { get; set; }
            /// <summary>
            /// Full Link
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// Chuyên mục
            /// </summary>
            public string ZoneTitle { get; set; }
            /// <summary>
            /// Site
            /// </summary>
            public string ChannelName { get; set; }
            /// <summary>
            /// Thời gian tồn tại 
            /// </summary>
            public int Duration { get; set; }
            /// <summary>
            /// Ngày xb
            /// </summary>
            public DateTime DeployDate { get; set; }
            /// <summary>
            /// Người đặt booking
            /// </summary>
            public string Booker { get; set; }
            /// <summary>
            /// Người gửi
            /// </summary>
            public string LastSender { get; set; }
            /// <summary>
            /// Số hợp đồng
            /// </summary>
            public string ContractNo { get; set; }
            public bool IsApproved { get; set; }
            public string TagsItem { get; set; }
            public string Tags { get; set; }
            public int LocationId { get; set; }
            public string LocationName { get; set; }

            public string ContentRelation { get; set; }

            public string Version { get; set; }
            public string Protected { get; set; }
            public string Byline { get; set; }
            public string TagLine { get; set; }
            public string ZoneID { get; set; }
            public string SmallAvatar { get; set; }
            public string NewsID { get; set; }
            public string lang { get; set; }
            public string WordCount { get; set; }
            public DateTime DistributionDate { get; set; }
            public string ChannelID { get; set; }
            public long DistributionID { get; set; }
            public string BookingName { get; set; }
            public string Message { get; set; }
            public DateTime PublishedDate { get; set; }
            public DateTime ExpireDate { get; set; }
            public string BookingStatus { get; set; }
            public string TimeSlotID { get; set; }
            public string IsFocus { get; set; }
            public string AdStore { get; set; }
            public string PRNumber { get; set; }
            public int Type { get; set; }
            public int NewsType { get; set; }
        }

        public class CmsPrResponseUpdateStatusData
        {
            public bool success { get; set; }
            public string message { get; set; }            
        }

        public class BookingInfoApi
        {
            public bool success { get; set; }
            public string message { get; set; }
            public BookingInfo data { get; set; }
        }

        public class BookingInfo
        {
            public int book_id { get; set; }
            public string contract { get; set; }
            public string contractcrc { get; set; }
            public string book_date { get; set; }
            public string time_from { get; set; }
            public string time_to { get; set; }
            public int site_id { get; set; }
            public string domain { get; set; }
            public string note { get; set; }
            public string type { get; set; }
            public int channel_id { get; set; }
            public string channel_name { get; set; }
            public int fm_id { get; set; }
            public int mode { get; set; }
            public string fm_name { get; set; }
            public string art_title { get; set; }
            public int status { get; set; }
        }
        #endregion
    }
}