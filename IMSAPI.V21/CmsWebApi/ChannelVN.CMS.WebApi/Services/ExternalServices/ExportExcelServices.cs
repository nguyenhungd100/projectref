﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using ExcelUtility = ExcelUtilities.Library.Excel;
using OfficeOpenXml;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.BoSearch.Entity;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    internal static class ExportExcelServices
    {
        #region Common
        public static void DeleteDirectoryContents(string directory)
        {
            var info = new DirectoryInfo(directory);

            foreach (var file in info.GetFiles())
                file.Delete();

            foreach (var dir in info.GetDirectories())
                dir.Delete(true);
        }

        public static void UnzipFile(string zipFileName, string targetDirectory)
        {
            new FastZip().ExtractZip(zipFileName, targetDirectory, null);
        }

        public static void ZipDirectory(string sourceDirectory, string zipFileName)
        {
            new FastZip().CreateZip(zipFileName, sourceDirectory, true, null);
        }

        public static IList<string> ReadStringTable(Stream input)
        {
            var stringTable = new List<string>();

            using (var reader = XmlReader.Create(input))
                for (reader.MoveToContent(); reader.Read();)
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "t")
                        stringTable.Add(reader.ReadElementString());

            return stringTable;
        }

        public static void ReadWorksheet(Stream input, IList<string> stringTable, DataTable data)
        {
            using (var reader = XmlReader.Create(input))
            {
                DataRow row = null;
                int columnIndex = 0;
                string type;
                int value;

                for (reader.MoveToContent(); reader.Read();)
                    if (reader.NodeType == XmlNodeType.Element)
                        switch (reader.Name)
                        {
                            case "row":
                                row = data.NewRow();
                                data.Rows.Add(row);

                                columnIndex = 0;

                                break;

                            case "c":
                                type = reader.GetAttribute("t");
                                reader.Read();
                                value = int.Parse(reader.ReadElementString(), CultureInfo.InvariantCulture);

                                if (type == "s")
                                    row[columnIndex] = stringTable[value];
                                else
                                    row[columnIndex] = value;

                                columnIndex++;

                                break;
                        }
            }
        }

        public static IList<string> CreateStringTables(DataTable data, out IDictionary<string, int> lookupTable)
        {
            var stringTable = new List<string>();
            lookupTable = new Dictionary<string, int>();

            foreach (DataRow row in data.Rows)
                foreach (DataColumn column in data.Columns)                    
                    if (column.DataType == typeof(string))
                    {
                        //var value = (string)row[column];
                        var value = Convert.ToString(row[column]??string.Empty);

                        if (!lookupTable.ContainsKey(value))
                        {
                            lookupTable.Add(value, stringTable.Count);
                            stringTable.Add(value);
                        }
                    }

            return stringTable;
        }

        public static void WriteStringTable(Stream output, IList<string> stringTable)
        {
            using (var writer = XmlWriter.Create(output))
            {
                writer.WriteStartDocument(true);

                writer.WriteStartElement("sst", "http://schemas.openxmlformats.org/spreadsheetml/2006/main");
                writer.WriteAttributeString("count", stringTable.Count.ToString(CultureInfo.InvariantCulture));
                writer.WriteAttributeString("uniqueCount", stringTable.Count.ToString(CultureInfo.InvariantCulture));

                foreach (var str in stringTable)
                {
                    writer.WriteStartElement("si");
                    writer.WriteElementString("t", str);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
        }

        public static string RowColumnToPosition(int row, int column)
        {
            return ColumnIndexToName(column) + RowIndexToName(row);
        }

        public static string ColumnIndexToName(int columnIndex)
        {
            var second = (char)(((int)'A') + columnIndex % 26);

            columnIndex /= 26;

            if (columnIndex == 0)
                return second.ToString();
            else
                return ((char)(((int)'A') - 1 + columnIndex)).ToString() + second.ToString();
        }

        public static string RowIndexToName(int rowIndex)
        {
            return (rowIndex + 1).ToString(CultureInfo.InvariantCulture);
        }

        public static void WriteWorksheet(Stream output, DataTable data, IDictionary<string, int> lookupTable)
        {
            using (XmlTextWriter writer = new XmlTextWriter(output, Encoding.UTF8))
            {
                writer.WriteStartDocument(true);

                writer.WriteStartElement("worksheet");
                writer.WriteAttributeString("xmlns", "http://schemas.openxmlformats.org/spreadsheetml/2006/main");
                writer.WriteAttributeString("xmlns:r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");

                writer.WriteStartElement("dimension");
                var lastCell = RowColumnToPosition(data.Rows.Count - 1, data.Columns.Count - 1);
                writer.WriteAttributeString("ref", "A1:" + lastCell);
                writer.WriteEndElement();

                writer.WriteStartElement("sheetViews");
                writer.WriteStartElement("sheetView");
                writer.WriteAttributeString("tabSelected", "1");
                writer.WriteAttributeString("workbookViewId", "0");
                writer.WriteEndElement();
                writer.WriteEndElement();

                writer.WriteStartElement("sheetFormatPr");
                writer.WriteAttributeString("defaultRowHeight", "15");
                writer.WriteEndElement();

                writer.WriteStartElement("sheetData");
                WriteWorksheetData(writer, data, lookupTable);
                writer.WriteEndElement();

                writer.WriteStartElement("pageMargins");
                writer.WriteAttributeString("left", "0.7");
                writer.WriteAttributeString("right", "0.7");
                writer.WriteAttributeString("top", "0.75");
                writer.WriteAttributeString("bottom", "0.75");
                writer.WriteAttributeString("header", "0.3");
                writer.WriteAttributeString("footer", "0.3");
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        }

        public static void WriteWorksheetData(XmlTextWriter writer, DataTable data, IDictionary<string, int> lookupTable)
        {
            var rowsCount = data.Rows.Count;
            var columnsCount = data.Columns.Count;
            string relPos;

            for (int row = 0; row < rowsCount; row++)
            {
                writer.WriteStartElement("row");
                relPos = RowIndexToName(row);
                writer.WriteAttributeString("r", relPos);
                writer.WriteAttributeString("spans", "1:" + columnsCount.ToString(CultureInfo.InvariantCulture));

                for (int column = 0; column < columnsCount; column++)
                {
                    object value = data.Rows[row][column];

                    writer.WriteStartElement("c");
                    relPos = RowColumnToPosition(row, column);
                    writer.WriteAttributeString("r", relPos);

                    var str = value as string;
                    if (str != null)
                    {
                        writer.WriteAttributeString("t", "s");
                        value = lookupTable[str];
                    }

                    writer.WriteElementString("v", value.ToString());

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            try {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }

                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }

                    dataTable.Rows.Add(values);

                }
                //put a breakpoint here and check datatable
                return dataTable;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region NPOI
        public static IWorkbook WriteExcelWithNPOI(DataTable dt, string extension)
        {

            IWorkbook workbook;

            if (extension == "xlsx")
            {
                workbook = new XSSFWorkbook();
            }
            else if (extension == "xls")
            {
                workbook = new HSSFWorkbook();
            }
            else {
                workbook = new HSSFWorkbook();
                //throw new Exception("This format is not supported");
            }

            ISheet sheet1 = workbook.CreateSheet("Sheet 1");

            //make a header row
            IRow row1 = sheet1.CreateRow(0);

            for (int j = 0; j < dt.Columns.Count; j++)
            {

                ICell cell = row1.CreateCell(j);
                string columnName = dt.Columns[j].ToString();
                cell.SetCellValue(columnName);
            }

            //loops through data
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IRow row = sheet1.CreateRow(i + 1);
                for (int j = 0; j < dt.Columns.Count; j++)
                {

                    ICell cell = row.CreateCell(j);
                    string columnName = dt.Columns[j].ToString();
                    cell.SetCellValue(dt.Rows[i][columnName].ToString());
                }
            }

            return workbook;            
        }
        #endregion

        #region Excel

        static string filePath = string.Empty;
        static ExcelUtility excel = null;
        static ExcelWorksheet ws = null;

        public static string ExportViewVideo(string username, string tempExcel, DateTime startDate, DateTime endDate, List<VideoSearchEntity> items, int pageIndex, int page)
        {
            try {
                
                if (pageIndex == 1)
                {
                    var expoExcel = CmsChannelConfiguration.GetAppSetting("FolderExportView");
                    tempExcel = HttpContext.Current.Server.MapPath(tempExcel);
                    var folderExport = HttpContext.Current.Server.MapPath(expoExcel);
                    var fileName = string.Format("{0}_{1}.xlsx", username, string.Format("{0:yyyymmddHHmmssFFF}", DateTime.Now));
                    filePath = folderExport.EndsWith("\\") ? folderExport + fileName : string.Format("{0}\\{1}", folderExport, fileName);
                    var rowConfig = tempExcel.Replace(".xlsx", ".xml");

                    excel = new ExcelUtility(tempExcel, filePath, rowConfig);
                    ws = excel.Worksheet(1);
                    excel.RowConfig.Header.Data.Add("startdate", string.Format("{0:dd/MM/yyyy}", startDate));
                    excel.RowConfig.Header.Data.Add("enddate", string.Format("{0:dd/MM/yyyy}", endDate));
                    excel.SetHeader(ws, excel.RowConfig.Header);
                }

                //for theo page
                excel.DataBind(ws, items);

                if(pageIndex == page)
                    excel.Save();

                return filePath;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public static string ExportViewNews(string username, string tempExcel, DateTime startDate, DateTime endDate, List<NewsSearchViewEntity> items, int pageIndex, int page)
        {
            try
            {
                if (pageIndex == 1)
                {
                    var expoExcel = CmsChannelConfiguration.GetAppSetting("FolderExportView");
                    tempExcel = HttpContext.Current.Server.MapPath(tempExcel);
                    var folderExport = HttpContext.Current.Server.MapPath(expoExcel);
                    var fileName = string.Format("{0}_{1}.xlsx", username, string.Format("{0:yyyymmddHHmmssFFF}", DateTime.Now));
                    filePath = folderExport.EndsWith("\\") ? folderExport + fileName : string.Format("{0}\\{1}", folderExport, fileName);
                    var rowConfig = tempExcel.Replace(".xlsx", ".xml");

                    excel = new ExcelUtility(tempExcel, filePath, rowConfig);
                    ws = excel.Worksheet(1);
                    excel.RowConfig.Header.Data.Add("startdate", string.Format("{0:dd/MM/yyyy}", startDate));
                    excel.RowConfig.Header.Data.Add("enddate", string.Format("{0:dd/MM/yyyy}", endDate));
                    excel.SetHeader(ws, excel.RowConfig.Header);
                }

                //for theo page
                excel.DataBind(ws, items);

                if (pageIndex == page)
                    excel.Save();

                return filePath;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion
    }    
}