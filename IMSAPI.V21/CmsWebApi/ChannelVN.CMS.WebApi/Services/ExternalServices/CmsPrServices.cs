﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using ChannelVN.CMS.WcfMapping.External.AdmicroPrApi;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class CmsPrServices
    {
        #region NewsPr Entity

        public enum NewsPrStatus
        {
            AllStatus = -1,
            WaitForApprove = 0,
            Approved = 1
        }
        public enum ChannelId
        {
            Unknown = 0,
            Kenh14 = 1,
            GenK = 2,
            Afamily = 4,
            CafeF = 5,
            Skds = 6,
            GiaDinh = 7,
            VnEconomy = 8,
            Dddn = 9,
            DanTri = 10,
            GameK = 11,
            AutoPro = 12,
            Nld = 13,
            Soha = 14,
            CafeBiz = 15,
            VTV = 16,
            IctNews = 17,
            VTVEnglish = 18,
            VTVCD24 = 19,
            GiaDinhThoiDai = 20,
            TieuDungVNE = 21,
            TuoiTre = 22,
            TTVN = 23,
            TechVccloud = 24,
            VtvVfc = 25,
            WebTheThao = 26,
            TienPhong = 27,
            Sport5 = 28,
            ThoiDai = 29,
            SucKhoeHangNgay = 30,
            Demo = 31,
            ToQuoc=32,
            Vinfast = 33
        }
        public enum NewsPrRetailStatus
        {
            AllStatus = -1,
            Running = 0,
            WatingApprove = 1,
            Finished = 2,
            Stopped = 3,
            Setting = 4,
            WatingContract = 5,
            ContractReady = 6
        }
        public static string ChannelName
        {
            get
            {
                switch (CmsTrigger.Instance.CmsPrChannelId)
                {
                    case ChannelId.Kenh14:
                        return "kenh14.vn";
                    case ChannelId.GenK:
                        return "genk.vn";
                    case ChannelId.Afamily:
                        return "afamily.vn";
                    case ChannelId.CafeF:
                        return "cafef.vn";
                    case ChannelId.Skds:
                        return "suckhoedoisong.vn";
                    case ChannelId.GiaDinh:
                        return "giadinh.net.vn";
                    case ChannelId.VnEconomy:
                        return "vneconomy.vn";
                    case ChannelId.Dddn:
                        return "dddn.com.vn";
                    case ChannelId.DanTri:
                        return "dantri.com.vn";
                    case ChannelId.GameK:
                        return "gamek.vn";
                    case ChannelId.AutoPro:
                        return "autopro.com.vn";
                    case ChannelId.Nld:
                        return "nld.com.vn";
                    case ChannelId.Soha:
                        return "soha.vn";
                    case ChannelId.CafeBiz:
                        return "cafebiz.vn";
                    case ChannelId.GiaDinhThoiDai:
                        return "giadinhthoidai.giadinhnet.vn";
                    case ChannelId.Unknown:
                        return "";
                }
                return "";
            }
        }

        public static string GetChannelName(int channelId)
        {
            switch (channelId)
            {
                case (int)ChannelId.Kenh14:
                    return "kenh14.vn";
                case (int)ChannelId.GenK:
                    return "genk.vn";
                case (int)ChannelId.Afamily:
                    return "afamily.vn";
                case (int)ChannelId.CafeF:
                    return "cafef.vn";
                case (int)ChannelId.Skds:
                    return "suckhoedoisong.vn";
                case (int)ChannelId.GiaDinh:
                    return "giadinh.net.vn";
                case (int)ChannelId.VnEconomy:
                    return "vneconomy.vn";
                case (int)ChannelId.Dddn:
                    return "dddn.com.vn";
                case (int)ChannelId.DanTri:
                    return "dantri.com.vn";
                case (int)ChannelId.GameK:
                    return "gamek.vn";
                case (int)ChannelId.AutoPro:
                    return "autopro.com.vn";
                case (int)ChannelId.Nld:
                    return "nld.com.vn";
                case (int)ChannelId.Soha:
                    return "soha.vn";
                case (int)ChannelId.GiaDinhThoiDai:
                    return "giadinhthoidai.giadinhnet.vn";
                case (int)ChannelId.Unknown:
                    return "";
            }
            return "";

        }

        #region For List NewsPr

        [Serializable]
        private class CmsPrResponseListData
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public List<NewsPrWithSimpleFieldEntity> Data { get; set; }
            public string Content { get; set; }
            public int TotalRow { get; set; }
            public int ErrorCode { get; set; }
            public int Total { get; set; }
            public int Type { get; set; }
        }
        /// <summary>
        /// News entity shorted
        /// </summary>
        [Serializable]
        public class NewsPrWithSimpleFieldEntity
        {
            /// <summary>
            /// News Id
            /// </summary>
            public int ContentID { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string EncryptContentId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int BookingId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string EncryptBookingId { get; set; }
            /// <summary>
            /// News Title
            /// </summary>
            public string Headline { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Sapo { get; set; }
            /// <summary>
            /// Ảnh đại diện
            /// </summary>
            public string Avatar { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string AvatarThumb { get; set; }
            /// <summary>
            /// Ngày tạo
            /// </summary>
            public DateTime CreatedDate { get; set; }
            /// <summary>
            /// Trạng thái
            /// </summary>
            public int Status { get; set; }
            /// <summary>
            /// Vị trí PR
            /// </summary>
            public int Mode { get; set; }
            /// <summary>
            /// Lượt xem
            /// </summary>
            public int Views { get; set; }
            /// <summary>
            /// Full Link
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// Chuyên mục
            /// </summary>
            public string ZoneTite { get; set; }
            /// <summary>
            /// Site
            /// </summary>
            public string ChannelName { get; set; }
            /// <summary>
            /// ZoneId
            /// </summary>
            public int ZoneId { get; set; }
            /// <summary>
            /// ChannelId
            /// </summary>
            public int ChannelId { get; set; }
            /// <summary>
            /// Thời gian tồn tại 
            /// </summary>
            public int Duration { get; set; }
            /// <summary>
            /// Ngày xb
            /// </summary>
            public DateTime DeployDate { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public DateTime PublishedDate { get; set; }
            /// <summary>
            /// Người đặt booking
            /// </summary>
            public string Booker { get; set; }
            /// <summary>
            /// Người gửi
            /// </summary>
            public string LastSender { get; set; }
            /// <summary>
            /// Số hợp đồng
            /// </summary>
            public string ContractNo { get; set; }
            public string TagsItem { get; set; }
            public string Tags { get; set; }
            public int LocationId { get; set; }
            public string LocationName { get; set; }
            public int DistributionId { get; set; }
            public string Substatus { get; set; }
            public string CountUnread { get; set; }

        }

        #endregion

        #region For NewsPr detail

        [Serializable]
        private class CmsPrResponseDetailData
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public NewsEntityApi Data { get; set; }
            public string Content { get; set; }
            public int TotalRow { get; set; }
            public int ErrorCode { get; set; }
            public int Total { get; set; }
            public int Type { get; set; }
        }

        [Serializable]
        public class NewsEntityApi
        {
            /// <summary>
            /// News Id
            /// </summary>
            public int ContentID { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string EncryptContentId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public long BookingId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string EncryptBookingId { get; set; }
            /// <summary>
            /// News Title
            /// </summary>
            public string Headline { get; set; }
            /// <summary>
            /// Ảnh đại diện
            /// </summary>
            public string Avatar { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string AvatarThumb { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int Approver { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Author { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string AvatarDesc { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int AvatarWidth { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int AvatarHeight { get; set; }
            /// <summary>
            /// Nội dung chi tiết
            /// </summary>
            public string Body { get; set; }
            /// <summary>
            /// Mô tả ngắn
            /// </summary>
            public string Teaser { get; set; }
            /// <summary>
            /// Kiểu hiển thị
            /// </summary>
            public byte DisplayType { get; set; }
            /// <summary>
            /// Người sửa
            /// </summary>
            public int Editor { get; set; }
            /// <summary>
            /// Nguồn
            /// </summary>
            public string Source { get; set; }
            /// <summary>
            /// Tiêu đề phụ
            /// </summary>
            public string SubTitle { get; set; }
            /// <summary>
            /// ID của người cập nhật
            /// </summary>
            public int UpdateUserID { get; set; }
            /// <summary>
            /// Ngày tạo
            /// </summary>
            public DateTime CreatedDate { get; set; }
            /// <summary>
            /// Substatus
            /// </summary>
            public int Substatus { get; set; }
            /// </summary>
            /// Trạng thái
            /// </summary>
            public int Status { get; set; }
            /// <summary>
            /// Vị trí PR
            /// </summary>
            public int Mode { get; set; }
            /// <summary>
            /// Lượt xem
            /// </summary>
            public int Views { get; set; }
            /// <summary>
            /// Full Link
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// Chuyên mục
            /// </summary>
            public string ZoneTite { get; set; }
            /// <summary>
            /// Site
            /// </summary>
            public string ChannelName { get; set; }
            /// <summary>
            /// Thời gian tồn tại 
            /// </summary>
            public int Duration { get; set; }
            /// <summary>
            /// Ngày xb
            /// </summary>
            public DateTime DeployDate { get; set; }
            /// <summary>
            /// Người đặt booking
            /// </summary>
            public string Booker { get; set; }
            /// <summary>
            /// Người gửi
            /// </summary>
            public string LastSender { get; set; }
            /// <summary>
            /// Số hợp đồng
            /// </summary>
            public string ContractNo { get; set; }
            public bool IsApproved { get; set; }
            public string TagsItem { get; set; }
            public string Tags { get; set; }
            public int LocationId { get; set; }
            public string LocationName { get; set; }

            public string ContentRelation { get; set; }

            public string Version { get; set; }
            public string Protected { get; set; }
            public string Byline { get; set; }
            public string TagLine { get; set; }
            public string ZoneID { get; set; }
            public string SmallAvatar { get; set; }
            public string NewsID { get; set; }
            public string lang { get; set; }
            public string WordCount { get; set; }
            public string DistributionDate { get; set; }
            public string ChannelID { get; set; }
            public string DistributionID { get; set; }
            public string BookingName { get; set; }
            public string Message { get; set; }
            public string PublishDate { get; set; }
            public string BookingStatus { get; set; }
            public string TimeSlotID { get; set; }
            public string IsFocus { get; set; }
            public string AdStore { get; set; }
            public string PRNumber { get; set; }
        }

        #endregion

        #region For Job detail

        [Serializable]
        public class JobDetailEntity
        {
            public JobEntity Job { get; set; }
            public List<JobAssignmentEntity> ProcessUsers { get; set; }
            public List<JobAssignmentEntity> FollowUsers { get; set; }
            public List<JobTargetEntity> Targets { get; set; }
            public List<JobFileAttachmentEntity> FileAttachments { get; set; }
            public List<JobAssignmentEntity> UnreadComment { get; set; }
        }
        [Serializable]
        public class JobEntity
        {
            public int Id { get; set; }
            public int JobParentId { get; set; }
            public int ZoneId { get; set; }
            public string Name { get; set; }
            public string JobDescription { get; set; }
            public string JobNote { get; set; }
            public DateTime StartedDate { get; set; }
            public DateTime Deadline { get; set; }
            public DateTime FinishedDate { get; set; }
            public double PercentComplete { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
            public int Status { get; set; }
            public int ContentId { get; set; }
            public string Channel { get; set; }
            public string ListProcessUser { get; set; }
            public string ListFollowUser { get; set; }
            public int TotalParentDiscussion { get; set; }
        }
        [Serializable]
        public class JobFileAttachmentEntity
        {
            public int Id { get; set; }
            public int JobId { get; set; }
            public int JobReportId { get; set; }
            public string FileName { get; set; }
            public string FileUrl { get; set; }
            public long FileSize { get; set; }
            public string Avatar { get; set; }
            public string Note { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedDate { get; set; }
        }
        [Serializable]
        public class JobAssignmentEntity
        {
            public int Id { get; set; }
            public int JobId { get; set; }
            public int JobReportId { get; set; }
            public string UserAssigned { get; set; }
            public int AssignmentType { get; set; }
            public bool IsHighlight { get; set; }
            public int UnreadComment { get; set; }
        }
        [Serializable]
        public class JobTargetEntity
        {
            public int Id { get; set; }
            public int JobId { get; set; }
            public int TargetType { get; set; }
            public int TargetValue { get; set; }
        }

        #endregion

        #region Job report

        public class JobReportEntity
        {
            public int Id { get; set; }
            public int JobId { get; set; }
            public int ParentId { get; set; }
            public string ReportContent { get; set; }
            public double PercentComplete { get; set; }
            public DateTime CreatedDate { get; set; }
            public string CreatedBy { get; set; }
            public string ListReceiveUser { get; set; }
            public int Privacy { get; set; }
            public int UnreadComment { get; set; }
            public string Channel { get; set; }
            public List<JobFileAttachmentEntity> FileAttachments { get; set; }
        }

        #endregion

        #region Channel User
        public class ChannelUserEntity
        {
            public int UserId { get; set; }
            public string UserName { get; set; }
            public string FullName { get; set; }
            public string Channel { get; set; }
        }
        #endregion

        [Serializable]
        private class CmsPrRetailResponseListData
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public List<NewsPrRetailEntity> PRBookings { get; set; }
            public string Content { get; set; }
            public int TotalRow { get; set; }
            public int ErrorCode { get; set; }
            public int Total { get; set; }
            public int Type { get; set; }
        }

        [Serializable]
        public class NewsPrRetailEntity
        {
            public string PrBookingId { set; get; }
            public string SiteName { set; get; }
            public string ArticleTitle { set; get; }
            public string ArticleDesc { set; get; }
            public string StartDate { set; get; }
            public string TimeSlot { set; get; }
            public string CreateDate { set; get; }
            public string[] Avatars { set; get; }
            public string[] Clips { set; get; }
            public string[] Links { set; get; }
            public string Status { set; get; }
            public GameInfoEntity GameInfo { set; get; }
            public PackageEntity Package { set; get; }
            public CustomerInfoEntity Customer { set; get; }
        }

        [Serializable]
        public class GameInfoEntity
        {
            public int GameInfoId { set; get; }
            public string UserName { set; get; }
            public string Name { set; get; }
            public string Author { set; get; }
            public string Home { set; get; }
            public string CompanyName { set; get; }
            public string CompanyPhone { set; get; }
            public string CompanyAddress { set; get; }
            public string CompanyFax { set; get; }
            public string CompanyFaxNumber { set; get; }
            public int Status { set; get; }
            public string DateCreated { set; get; }
        }

        [Serializable]
        public class PackageEntity
        {
            public string PacName { set; get; }
            public string PacType { set; get; }
            public string Price { set; get; }
        }

        [Serializable]
        public class CustomerInfoEntity
        {
            public string Name { set; get; }
            public string Email { set; get; }
            public string Phone { set; get; }
            public string Fax { set; get; }
            public string Adress { set; get; }
        }

        #endregion

        private static string ApiCmsPrUrl = CmsChannelConfiguration.GetAppSetting("ApiCmsPrUrl");
        private static string ApiCmsPrSecretKey = CmsChannelConfiguration.GetAppSetting("ApiCmsPrSecretKey");

        public static string CurrentCmrPrAccount
        {
            get { return HttpContext.Current.User.Identity.Name + "-" + ChannelName; }
        }

        private enum RequestMethod
        {
            Get = 0,
            Post = 1
        }
        private static HttpWebRequest CreateWebRequest(string url, RequestMethod method)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.ContentType = "application/json; charset=utf-8";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            req.Method = (method == RequestMethod.Get ? "GET" : "POST");
            return req;
        }
        private static string ValidResponseData(string responseData)
        {
            var result = responseData.Replace(@"\""", @"""").Replace(@"\/", @"/").Replace(@"\\", @"\");
            if (!string.IsNullOrEmpty(result) && result.StartsWith("\"")) result = result.Remove(0, 1);
            if (!string.IsNullOrEmpty(result) && result.EndsWith("\"")) result = result.Remove(result.Length - 1, 1);
            return result;
        }

        #region NewApi

        public static List<NewsPrWithSimpleFieldEntity> GetListNewsPrByStatus(NewsPrStatus status, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            //Logger.WriteLog(Logger.LogType.Trace, "NewsPR Request (" + DateTime.Now + "): Begin");
            if (CmsTrigger.Instance.CmsPrChannelId == ChannelId.Unknown)
                return new List<NewsPrWithSimpleFieldEntity>();
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "PrListContentPending?secretKey={0}&channelId={1}&userUnread={2}&substatus={3}&zoneId={4}&pageIndex={5}&pagesize={6}",
                    ApiCmsPrSecretKey, (int)CmsTrigger.Instance.CmsPrChannelId, CurrentCmrPrAccount, (int)status, zoneId, pageIndex, pageSize);
            //Logger.WriteLog(Logger.LogType.Trace, "NewsPR Request (" + DateTime.Now + "): " + url);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var responseData = NewtonJson.Deserialize<CmsPrResponseListData>(ValidResponseData(result));
                totalRow = responseData.TotalRow;
                if (string.IsNullOrEmpty(responseData.Data + ""))
                {
                    return new List<NewsPrWithSimpleFieldEntity>();
                }
                return responseData.Data;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<NewsPrWithSimpleFieldEntity>();
        }
        public static List<NewsPrWithSimpleFieldEntity> GetListNewsPrByStatus(NewsPrStatus status, int zoneId, int channelId, int pageIndex, int pageSize, ref int totalRow)
        {
            Logger.WriteLog(Logger.LogType.Trace, "NewsPR Request By Status (" + DateTime.Now + "): Begin");
            if (CmsTrigger.Instance.CmsPrChannelId == ChannelId.Unknown)
                return new List<NewsPrWithSimpleFieldEntity>();
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "PrListContentPending?secretKey={0}&channelId={1}&userUnread={2}&substatus={3}&zoneId={4}&pageIndex={5}&pagesize={6}",
                    ApiCmsPrSecretKey, channelId, HttpContext.Current.User.Identity.Name + "-" + GetChannelName(channelId), (int)status, zoneId, pageIndex, pageSize);
            Logger.WriteLog(Logger.LogType.Trace, "NewsPR Request By Status (" + DateTime.Now + "): " + url);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var responseData = NewtonJson.Deserialize<CmsPrResponseListData>(ValidResponseData(result));
                totalRow = responseData.TotalRow;
                if (string.IsNullOrEmpty(responseData.Data + ""))
                {
                    return new List<NewsPrWithSimpleFieldEntity>();
                }
                return responseData.Data;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<NewsPrWithSimpleFieldEntity>();
        }
        public static List<NewsPrWithSimpleFieldEntity> GetListNewsPrPublished(NewsPrStatus status, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            Logger.WriteLog(Logger.LogType.Trace, "NewsPR Request published (" + DateTime.Now + "): Begin");
            if (CmsTrigger.Instance.CmsPrChannelId == ChannelId.Unknown)
                return new List<NewsPrWithSimpleFieldEntity>();
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "PrListContentApprove?secretKey={0}&channelId={1}&userUnread={2}&substatus={3}&zoneId={4}&pageIndex={5}&pagesize={6}",
                    ApiCmsPrSecretKey, (int)CmsTrigger.Instance.CmsPrChannelId, CurrentCmrPrAccount, (int)status, zoneId, pageIndex, pageSize);
            Logger.WriteLog(Logger.LogType.Trace, "NewsPR Request published (" + DateTime.Now + "): " + url);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var responseData = NewtonJson.Deserialize<CmsPrResponseListData>(ValidResponseData(result));
                totalRow = responseData.TotalRow;
                return responseData.Data;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<NewsPrWithSimpleFieldEntity>();
        }
        public static bool RecieveNewsPr(int contentId, int distributionId, string message)
        {
            if (CmsTrigger.Instance.CmsPrChannelId == ChannelId.Unknown)
                return false;
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "RecieveContentApi?secretKey={0}&channelId={1}&channelName={2}&username={3}&contentId={4}&distributionId={5}&message={6}",
                    ApiCmsPrSecretKey, (int)CmsTrigger.Instance.CmsPrChannelId, ChannelName, HttpContext.Current.User.Identity.Name, contentId, distributionId, message);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim().Replace("\"", "") == "OK";
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return false;
        }
        public static bool ReturnNewsPr(int contentId, int distributionId, string message, string replyUser)
        {
            if (CmsTrigger.Instance.CmsPrChannelId == ChannelId.Unknown)
                return false;
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "ReturnContentApi?secretKey={0}&channelId={1}&channelName={2}&username={3}&contentId={4}&distributionId={5}&message={6}&replyer={7}",
                    ApiCmsPrSecretKey, (int)CmsTrigger.Instance.CmsPrChannelId, ChannelName, HttpContext.Current.User.Identity.Name, contentId, distributionId, message, replyUser);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim().Replace("\"", "") == "OK";
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return false;
        }
        public static bool UpdateNewsPrStatus(int contentId, NewsPrStatus subStatus, string message, string replyUser)
        {
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "ContentUpdateSubstatus?secretKey={0}&channelName={1}&username={2}&contentId={3}&substatus={4}&message={5}",
                    ApiCmsPrSecretKey, ChannelName, HttpContext.Current.User.Identity.Name, contentId, (int)subStatus, message);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim().Replace("\"", "") == "OK";
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return false;
        }
        public static NewsEntityApi GetNewsPrDetail(int contentId)
        {
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "GetContentDetail?secretKey={0}&contentId={1}", ApiCmsPrSecretKey, contentId);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var responseData = NewtonJson.Deserialize<CmsPrResponseDetailData>(ValidResponseData(result));
                Logger.WriteLog(Logger.LogType.Trace, ValidResponseData(result));
                return responseData.Data;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return null;
        }

        public static JobDetailEntity GetJobDetailByNewsPrId(int contentId)
        {
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "PrGetJobDetailByContentId?secretKey={0}&contentId={1}",
                    ApiCmsPrSecretKey, contentId);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var response = NewtonJson.Deserialize<JobDetailEntity>(ValidResponseData(result));
                return response;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return null;
        }

        public static List<JobReportEntity> GetJobReportByJobId(int jobId, int parentJobReportId, int privacy)
        {
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "PrListJobReportByJobId?secretKey={0}&jobId={1}&parentId={2}&privacy={3}",
                    ApiCmsPrSecretKey, jobId, parentJobReportId, privacy);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {
                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var responseData = NewtonJson.Deserialize<List<JobReportEntity>>(ValidResponseData(result));
                return responseData;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<JobReportEntity>();
        }
        public static bool SendJobReport(int jobId, int parentReportId, string listFile, string listReceivers, string note)
        {
            if (CmsTrigger.Instance.CmsPrChannelId == ChannelId.Unknown)
                return false;
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "InsertReportApi?secretKey={0}&channelId={1}&channelName={2}&username={3}&jobId={4}&parentId={5}&listFile={6}&listReceivers={7}&note={8}",
                    ApiCmsPrSecretKey, (int)CmsTrigger.Instance.CmsPrChannelId, ChannelName, CurrentCmrPrAccount.Split(new string[] { "-" }, StringSplitOptions.None)[0], jobId,
                    parentReportId, listFile, listReceivers, note);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim().Replace("\"", "") == "Succesfully";
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return false;
        }

        public static List<ChannelUserEntity> GetUserCmsPr(string channelName)
        {
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "ListUserByChannelName?secretKey={0}&ChannelName={1}",
                    ApiCmsPrSecretKey, channelName);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {
                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var responseData = NewtonJson.Deserialize<List<ChannelUserEntity>>(ValidResponseData(result));
                return responseData;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<ChannelUserEntity>();
        }

        public static bool ClearUnread(int jobId, int jobReportId)
        {
            string secretKey = ApiCmsPrSecretKey;
            string username = CurrentCmrPrAccount;
            string url = ApiCmsPrUrl + "ClearUnread?jobId=" + jobId + "&jobReportId=" + jobReportId + "&username=" + username + "&secretKey=" + secretKey;
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.ContentType = "application/json; charset=utf-8";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            req.Method = "GET";
            try
            {
                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim().Replace("\"", "") == "Succesfully";
                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString()); return false;
            }
        }

        public static bool PublishNewsPr(string userName, long newsId, long contentId, long distributionId, int zoneId,
                                         string title, string sapo, string avatar, string body,
                                         DateTime distributionDate, string newsUrl, int wordcount)
        {
            //return true;
            Logger.WriteLog(Logger.LogType.Trace, "PublishNewsPr: (int)CmsTrigger.Instance.CmsPrChannelId)" + newsId);
            if (CmsTrigger.Instance.CmsPrChannelId == ChannelId.Unknown)
                return false;
            var url = ApiCmsPrUrl + "PublishedNewsForApi";
            try
            {
                Logger.WriteLog(Logger.LogType.Trace, "Channel Name: " + ChannelName + "/" + userName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, "Ex1: " + ex.StackTrace);
            }
            var frontEndUrl = CmsChannelConfiguration.GetAppSetting("FRONT_END_URL");
            if (frontEndUrl.EndsWith("/"))
            {
                frontEndUrl = frontEndUrl.Substring(0, frontEndUrl.Length - 1);
            }
            try
            {
                var apiParams = new
                {
                    newsId = newsId,
                    channelId = (int)CmsTrigger.Instance.CmsPrChannelId,
                    channelName = ChannelName,
                    contentId = contentId,
                    distributionId = distributionId,
                    headline = title,
                    teaser = sapo,
                    body = body,
                    username = userName,
                    message = "",
                    url = !newsUrl.StartsWith("http://") ? frontEndUrl + newsUrl : newsUrl,
                    publishedDate = distributionDate.ToString("dd/MM/yyyy HH:mm:ss"),
                    secretKey = ApiCmsPrSecretKey
                };
                //Logger.WriteLog(Logger.LogType.Trace, "apiParams: " + NewtonJson.Serialize(apiParams));
                var strNewValue = NewtonJson.Serialize(apiParams);
                var bytes = Encoding.UTF8.GetBytes(strNewValue);
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = "POST";
                    request.ContentLength = bytes.Length;
                    request.ContentType = "application/json; charset=utf-8";
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(bytes, 0, bytes.Length);
                    }
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        var result = "";

                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            result = String.Format(
                                "POST failed. Received HTTP {0}",
                                response.StatusCode);
                            Logger.WriteLog(Logger.LogType.Error, (new ApplicationException(result)).ToString());

                            return false;
                        }
                        var stIn = new StreamReader(response.GetResponseStream());
                        result = stIn.ReadToEnd();
                        Logger.WriteLog(Logger.LogType.Trace, result);
                        stIn.Close();

                        return true;
                    }
                }
                catch (WebException ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "PublishNewsPr ex: " + ex.StackTrace);
                return false;
            }
        }

        public static ContractInfo ShowContractInfo(string contractNo, int mode, int zoneId)
        {
            ContractInfo info = null;
            try
            {
                int siteId = (int)CmsTrigger.Instance.CmsPrChannelId;
                // Nếu là Xem ăn chơi của K14/AF thì đặt mặc định mode = 6 ~ Tiểu mục. 
                // Nhưng vẫn lấy giá của Loại 4
                if (zoneId == 163 && siteId == (int)ChannelId.Afamily || zoneId == 135 && siteId == (int)ChannelId.Kenh14)
                {
                    mode = 4;
                }
                PRApi api = new PRApi();
                var result = api.pr_get_info_contract(contractNo, zoneId, mode, 0);
                api.Dispose();

                info = NewtonJson.Deserialize<ContractInfo>(result);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return info;
        }
        public class ContractInfo
        {
            public List<ThongTinHopDong> Table { get; set; }
            public List<ThongTinPhanBo> Table1 { get; set; }
            public List<ThongTinBaiViet> Table2 { get; set; }
        }
        public class ThongTinHopDong
        {
            public int hopdongid { get; set; }
            public string sohopdong { get; set; }
            public string ngaydanhso { get; set; }
            public string tensale { get; set; }
            public string phongban { get; set; }
            public string tenkhachhang { get; set; }
            public double tonggiatri { get; set; }
            public double tientreo { get; set; }
            public double tienconlai { get; set; }
            public int codau { get; set; }
        }
        public class ThongTinPhanBo
        {
            public int phanboid { get; set; }
            public int? hd_id { get; set; }
            public string nhanhang { get; set; }
            public int? website_id { get; set; }
            public string tenwebsite { get; set; }
            public int? soluong { get; set; }
            public string donvitinh { get; set; }
            public int? donviid { get; set; }
            public double? dongia { get; set; }
            public int? chietkhau { get; set; }
            public int? iskhuyenmai { get; set; }
            public double? thanhtientruocck { get; set; }
            public double? tientreo { get; set; }
            public double? chenhlech { get; set; }
            public string SoHopDongHT { get; set; }
            public bool check { get; set; }
        }
        public class ThongTinBaiViet
        {
            public int gia_baiviet { get; set; }
            public int tiengiadinh_hd { get; set; }
            public int iswarning { get; set; }
            public int sotienlech { get; set; }
            public string notes { get; set; }
        }
        #endregion

        public static string BookingOnlineApiUrl = CmsChannelConfiguration.GetAppSetting("BookingOnlineApiUrl");
        public static string BookingOnlineApiKey = CmsChannelConfiguration.GetAppSetting("BookingOnlineApiKey");
        public static List<NewsPrRetailEntity> GetListNewsPrByChannel(int pageindex, int pagesize, string startdate, string enddate, string sitedomain, int status)
        {
            if (CmsTrigger.Instance.CmsPrChannelId == ChannelId.Unknown)
                return new List<NewsPrRetailEntity>();
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "BookingOnlineGetList?pageindex={0}&pagesize={1}&startdate={2}&enddate={3}&channelid={4}&status={5}&secretKey={6}",
                    pageindex, pagesize, startdate, enddate, (int)CmsTrigger.Instance.CmsPrChannelId, (int)status, ApiCmsPrSecretKey);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var responseData = NewtonJson.Deserialize<CmsPrRetailResponseListData>(ValidResponseData(result));
                if (responseData == null)
                {
                    return new List<NewsPrRetailEntity>();
                }
                //totalRow = responseData.TotalRow;
                return responseData.PRBookings;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return null;
        }
        public static NewsPrRetailEntity GetNewsPrRetailById(int bookingId)
        {
            var url =
               string.Format(
                   ApiCmsPrUrl +
                   "BookingOnlineGetDetail?bookingid={0}&secretKey={1}",
                   bookingId, ApiCmsPrSecretKey);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                var responseData = NewtonJson.Deserialize<NewsPrRetailEntity>(ValidResponseData(result));
                //totalRow = responseData.TotalRow;
                return responseData;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }
        public static bool UpdateNewsPrDetailStatus(int bookingid, int status, string contractno, string contractdate, string comment, string seller)
        {
            var url =
                string.Format(
                    ApiCmsPrUrl +
                    "CmsPRJson.svc/BookingOnlineUpdate?bookingid={0}&status={1}&contractno={2}&contractdate={3}&comment={4}&seller={5}&secretKey={6}",
                    bookingid, (int)status, contractno, contractdate, comment, seller, ApiCmsPrSecretKey);
            var req = CreateWebRequest(url, RequestMethod.Get);
            try
            {

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                return result == "success";
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return true;
        }
        
    }
}