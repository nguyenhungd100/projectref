﻿using System;
using System.Text.RegularExpressions;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WcfMapping.External.ServicePegaCms;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class PegaCmsServices
    {
        public enum PegaSource
        {
            Unknown = 0,
            DanTri = 1,
            Kenh14 = 2,
            Afamily = 3,
            CafeF = 4,
            CafeBiz = 5,
            AutoPro = 6,
            GenK = 7,
            SohaNews = 8,
            VnEconomy = 9,
            GameK = 10,
            TechVccloud = 11,
            VtvVfc=12,
            WebTheThao = 13,
            TienPhong = 14,
            Sport5 = 15,
            ThoiDai = 16,
            SucKhoeHangNgay=17,
            Demo = 18,
            ToQuoc=19,
            Vinfast = 20
        }

        public static int NotifyInsertNews(NewsDetailForEditEntity newsDetail)
        {
            var result = 0;
            var pegaCmsSource = (int)CmsTrigger.Instance.PegaCmsSource;
            if (pegaCmsSource <= 0) return 0;

            try
            {
                if (newsDetail != null && newsDetail.NewsInZone != null)
                {
                    var news = newsDetail.NewsInfo;
                    var newsId = news.Id;

                    if (news.Status == (int)NewsStatus.Published && news.PegaBreakingNews > 0)
                    {
                        var actionType = (news.PegaBreakingNews == 1 ? EnumNewsLogExternalAction.PegaBreakingNewsNotification : EnumNewsLogExternalAction.PegaHotNewsNotification);
                        var logExternalAction = new NewsServices().GetExternalActionLogByNewsIdAndActionType(newsId, actionType);
                        if (logExternalAction == null || logExternalAction.Count == 0)
                        {
                            var videoUrl = "";

                            #region Get Video Url if NewsVideo Or NewsImageAndVideo

                            if (news.Type == (int)NewsType.Video || news.Type == (int)NewsType.ImageAndVideo)
                            {
                                try
                                {
                                    var regex = Regex.Match(news.Body, "(<object.*>)(.*)(<\\/object>)",
                                                            RegexOptions.Singleline | RegexOptions.Compiled |
                                                            RegexOptions.IgnoreCase);
                                    if (regex.Success)
                                    {
                                        var firstObjectHtml = regex.Value;
                                        var movieHtmlRegex = Regex.Match(firstObjectHtml,
                                                                         @"<param.*?value=[""'?](.+?)[""'?].*?>",
                                                                         RegexOptions.Singleline | RegexOptions.Compiled |
                                                                         RegexOptions.IgnoreCase);
                                        if (movieHtmlRegex.Success)
                                        {
                                            var keyVideo =
                                                movieHtmlRegex.Value.Substring(
                                                    movieHtmlRegex.Value.IndexOf("key=",
                                                                                 StringComparison
                                                                                     .CurrentCultureIgnoreCase) +
                                                    4);
                                            keyVideo = keyVideo.Substring(0,
                                                                          keyVideo.IndexOf("&",
                                                                                           StringComparison
                                                                                               .CurrentCultureIgnoreCase));
                                            videoUrl = VideoStorageServices.GetUrlVideo(keyVideo);

                                            if (!string.IsNullOrEmpty(videoUrl))
                                                videoUrl = videoUrl.Replace("\\", "").Replace("\"", "");
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                                }
                            }

                            #endregion

                            #region Apply News info

                            var appUrl = string.Format(CmsChannelConfiguration.GetAppSetting("PegaCmsAppLinkFormat"),
                                                       news.Url);
                            // If K14
                            if (pegaCmsSource == 2)
                            {
                                appUrl = appUrl.Replace(".chn", ".htm");
                            }
                            var newsForNotify = new NewsInfoPost
                            {
                                NewsID = Utility.ConvertToString(news.Id),
                                Title = news.Title,
                                Sapo = news.Sapo,
                                Content = string.Empty,
                                PublishDate = Utility.ConvertToString(news.DistributionDate),
                                URL = appUrl,
                                IsVideo = Utility.ConvertToString(!string.IsNullOrEmpty(videoUrl)),
                                VideoURL = videoUrl,
                                Source = pegaCmsSource.ToString(),
                                IsBreaking = (news.PegaBreakingNews == 1).ToString(),
                                Status = "3"
                            };
                            if (news.PegaBreakingNews == 1) // Breaking
                            {
                                newsForNotify.Image =
                                    string.Format(
                                        CmsChannelConfiguration.GetAppSetting(FileStorageServices.FILE_MANAGER_HTTPDOWNLOAD_THUMB), 600,
                                        350,
                                        news.Avatar);
                            }
                            else // Hot
                            {
                                newsForNotify.Image =
                                    string.Format(
                                        CmsChannelConfiguration.GetAppSetting(FileStorageServices.FILE_MANAGER_HTTPDOWNLOAD_THUMB), 200,
                                        200,
                                        news.Avatar);
                            }

                            #endregion

                            result = new InsertNewsClient().Insert(newsForNotify);

                            new NewsServices().InsertExternalActionLog(new NewsLogExternalActionEntity
                            {
                                NewsId = newsId,
                                ExternalActionType = (int)actionType
                            });

                            //Logger.WriteLog(Logger.LogType.Debug,"NotifyInsertNews(" + NewtonJson.Serialize(newsForNotify) + ") = " + result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return result;
        }
    }
}