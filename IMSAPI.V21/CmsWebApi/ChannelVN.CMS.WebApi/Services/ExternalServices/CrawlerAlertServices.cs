﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class CrawlerAlertServices
    {
        private static int AllowApi = CmsChannelConfiguration.GetAppSettingInInt32("AllowCrawlerAlert");
        private static string Api = CmsChannelConfiguration.GetAppSetting("CrawlerAlertApi") ?? "http://utils1.channelvn.net/ApiCrawler.ashx";
        public static void AddMonitor(long newsId, string title, string sourceUrl)
        {
            if (!string.IsNullOrEmpty(title) && AllowApi==1)
            {
                WebClient client = new WebClient();
                try
                {
                    client.Encoding = Encoding.UTF8;
                    var apiUrlFormat = Api + "?id={0}&title={1}&url={2}&sourceID={3}&type=insert";
                    var apiUrl = string.Format(apiUrlFormat, newsId, title, sourceUrl, (int)CmsTrigger.Instance.CmsPrChannelId);
                    var result = client.DownloadString(apiUrl);
                    //Logger.WriteLog(Logger.LogType.Trace, string.Format("Add monitor Crawler Alert Services NewsID {0}, [{1}],  Url: {2} => {3} ", newsId, title, sourceUrl, result));
                }
                catch (Exception ex) { Logger.WriteLog(Logger.LogType.Error, ex.ToString()); }
                finally
                {
                    client.Dispose();
                }
            }            
        }

        public static List<AlertNewsEntity> GetAlert()
        {
            try
            {
                //if (HttpRuntime.Cache["CrawlerAlertList"] == null)
                //{
                if (AllowApi == 1)
                {
                    WebClient client = new WebClient();
                    try
                    {
                        client.Encoding = Encoding.UTF8;
                        var apiUrlFormat = Api + "?sourceID={0}&type=get";
                        var apiUrl = string.Format(apiUrlFormat, (int)CmsTrigger.Instance.CmsPrChannelId);
                        var result = client.DownloadString(apiUrl);
                        //Logger.WriteLog(Logger.LogType.Trace, result);
                        //HttpRuntime.Cache.Insert(
                        //    "CrawlerAlertList",
                        //    result,
                        //    null,
                        //    DateTime.Now.AddMinutes(15),
                        //    Cache.NoSlidingExpiration,
                        //    CacheItemPriority.Low,
                        //    null);
                        return NewtonJson.Deserialize<List<AlertNewsEntity>>(result);
                    }
                    finally
                    {
                        client.Dispose();
                    }
                }
                //}
                //else
                //{
                //    var data = NewtonJson.Deserialize<List<AlertNewsEntity>>(HttpRuntime.Cache["CrawlerAlertList"].ToString());
                //    if(data!=null && data.Count <= 0)
                //    {
                //        WebClient client = new WebClient();
                //        try
                //        {
                //            client.Encoding = Encoding.UTF8;
                //            var apiUrlFormat = Api + "?sourceID={0}&type=get";
                //            var apiUrl = string.Format(apiUrlFormat, (int)CmsTrigger.Instance.CmsPrChannelId);
                //            var result = client.DownloadString(apiUrl);
                //            Logger.WriteLog(Logger.LogType.Trace, result);
                //            HttpRuntime.Cache.Insert(
                //                "CrawlerAlertList",
                //                result,
                //                null,
                //                DateTime.Now.AddMinutes(15),
                //                Cache.NoSlidingExpiration,
                //                CacheItemPriority.Low,
                //                null);
                //            return NewtonJson.Deserialize<List<AlertNewsEntity>>(result);
                //        }
                //        finally
                //        {
                //            client.Dispose();
                //        }
                //    }
                //    return data;
                //}

                return new List<AlertNewsEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new List<AlertNewsEntity>();
            }
        }

        public static void RemoveAlert(long newsId)
        {
            try
            {
                var apiUrlFormat = Api + "?sourceID={0}&id={1}&type=remove";
                var apiUrl = string.Format(apiUrlFormat, (int)CmsTrigger.Instance.CmsPrChannelId, newsId);
                var result = GetWebContent(apiUrl);
                Logger.WriteLog(Logger.LogType.Trace, string.Format("Remove Crawler Alert Services NewsID {0} => {1} ", newsId, result));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
        }        

        private static string GetWebContent(string url, int timeOut = 10000)
        {
            try
            {
                string result = "";

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = timeOut;
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";

                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream != null)
                        {
                            using (var reader = new StreamReader(stream))
                                result = reader.ReadToEnd();
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetWebContent(" + url + ") error => " + ex);

                return "";
            }
        }
    }
    public class AlertNewsEntity
    {
        #region CONSTRUCTORS
        public AlertNewsEntity()
        {

            this.m_NewsID = 0L;
            this.m_NewsTitle = "";
            this.m_NewsNote = "";
            this.m_newsUrl = "";
            this.m_CreateDate = DateTime.Now;
            this.m_cache = true;
            this.m_Status = 1;
        }

        #endregion

        #region ATTRIBUTES
        private long m_NewsID;
        private string m_NewsTitle;
        private string m_newsUrl;
        private DateTime m_CreateDate;
        private int m_Status;
        private bool m_cache;
        private string m_NewsNote;
        #endregion

        #region PROPERTIES
        public long NewsID
        {
            get
            {
                return this.m_NewsID;
            }
            set
            {
                this.m_NewsID = value;
            }
        }

        public string NewsTitle
        {
            get
            {
                return this.m_NewsTitle;
            }
            set
            {
                this.m_NewsTitle = value;
            }
        }

        public string NewsNote
        {
            get
            {
                return this.m_NewsNote;
            }
            set
            {
                this.m_NewsNote = value;
            }
        }

        public string newsUrl
        {
            get
            {
                return this.m_newsUrl;
            }
            set
            {
                this.m_newsUrl = value;
            }
        }

        public DateTime CreateDate
        {
            get
            {
                return this.m_CreateDate;
            }
            set
            {
                this.m_CreateDate = value;
            }
        }


        public bool cache
        {
            get
            {
                return this.m_cache;
            }
            set
            {
                this.m_cache = value;
            }
        }

        public int Status
        {
            get
            {
                return this.m_Status;
            }
            set
            {
                this.m_Status = value;
            }
        }

        #endregion

    }
}