﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class ChannelvnDirInfo
    {
        private string _upload_by;

        public string upload_by
        {
            get { return _upload_by; }
            set { _upload_by = value; }
        }
        private string _dirname;

        public string dirname
        {
            get { return _dirname; }
            set { _dirname = value; }
        }
        /**
        private string[] _contents;

        public string[] contents
        {
            get { return _contents; }
            set { _contents = value; }
        }
        /**/
        // update 25/03/2010
        private string[] _files = { };

        public string[] files
        {
            get { return _files; }
            set { _files = value; }
        }
        private string[] _directories = { };

        public string[] directories
        {
            get { return _directories; }
            set { _directories = value; }
        }
        // update 25/03/2010
        private string _last_modified;

        public string last_modified
        {
            get { return _last_modified; }
            set { _last_modified = value; }
        }
    }

    public class ChannelvnFileInfo
    {
        private string _size;
        private string _upload_time;
        private string _format;
        private int _height;
        private int _width;
        private string[] _thumbs;
        private string _upload_by;
        private string _filename;
        private string _md5;

        public string size
        {
            get { return _size; }
            set { _size = value; }
        }

        public string upload_time
        {
            get { return _upload_time; }
            set { _upload_time = value; }
        }

        public string format
        {
            get { return _format; }
            set { _format = value; }
        }

        public int height
        {
            get { return _height; }
            set { _height = value; }
        }

        public int width
        {
            get { return _width; }
            set { _width = value; }
        }

        public string[] thumbs
        {
            get { return _thumbs; }
            set { _thumbs = value; }
        }

        public string upload_by
        {
            get { return _upload_by; }
            set { _upload_by = value; }
        }

        public string filename
        {
            get { return _filename; }
            set { _filename = value; }
        }

        public string md5
        {
            get { return _md5; }
            set { _md5 = value; }
        }
    }

    public class VideoFileInfo
    {
        /// <summary>
        /// "phapluattp/IRKccccccccccccqj1X7kHscQTq71/2012/11/06/Nhansac-01d2a.flv"
        /// </summary>
        public string filename { get; set; }

        /// <summary>
        /// Size of video: 2451682 Bytes
        /// </summary>
        public string size { get; set; }

        /// <summary>
        /// MD5 = null
        /// </summary>
        public string md5 { get; set; }

        /// <summary>
        /// UploadTime: "Tuesday 06/11/2012 09:42:18",
        /// </summary>
        public string upload_time { get; set; }

        /// <summary>
        /// UploadBy: phapluattp ~ namespace
        /// </summary>
        public string upload_by { get; set; }

        /// <summary>
        /// BitRate: 471 Kb
        /// </summary>
        public string bitrate { get; set; }

        /// <summary>
        /// video_bitrate: 366 Kb
        /// </summary>
        public string video_bitrate { get; set; }

        /// <summary>
        /// Duration: "00:00:41.66"
        /// </summary>
        public string duration { get; set; }

        /// <summary>
        /// Audio Frequency: 44100
        /// </summary>
        public string audio_frequency { get; set; }

        /// <summary>
        /// video_size: "320x240"
        /// </summary>
        public string video_size { get; set; }

        /// <summary>
        /// "mp3"
        /// </summary>
        public string audio_codec { get; set; }

        /// <summary>
        /// flv1
        /// </summary>
        public string video_codec { get; set; }

        /// <summary>
        /// audio_bitrate:96
        /// </summary>
        public string audio_bitrate { get; set; }

        /// <summary>
        ///  "http://video-thumbs.vcmedia.vn/phapluattp/IRKccccccccccccqj1X7kHscQTq71/2012/11/06/Nhansac-01d2a.jpg"
        /// </summary>
        public string thumb_url { get; set; }

        /// <summary>
        /// flv
        /// </summary>
        public string type { get; set; }
    }

    public class VideoDirInfo
    {
        public ChannelvnDirInfo DirInfo { get; set; }
        public List<VideoFileInfo> FileList { get; set; }
    }
}