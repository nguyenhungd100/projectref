﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class ContentDeliveryServices
    {
        public static void PushToDataCD(string strParamValue, string action, string distributionDate, string jsonKey = "{}")
        {
            try
            {
                var context = HttpContext.Current;
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    var task = Task.Run(() =>
                    {
                        HttpContext.Current = context;
                        Logger.WriteLog(Logger.LogType.Debug, string.Format("1.Start push to cd for {0} action {1}", jsonKey, action));
                        var url = CmsChannelConfiguration.GetAppSetting("UrlPushDataCD");
                        var channelName = WcfExtensions.WcfMessageHeader.Current.Namespace;
                        if (AppSettings.GetBool("IsDevEnv"))
                        {
                            channelName = CmsChannelConfiguration.GetAppSetting("NameSpacePushDataCD_DEV")?? "vtvvfcdev";
                        }
                        using (WebClient wc = new WebClient())
                        {
                            var sessionId = Guid.NewGuid().ToString();
                            try {                                
                                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                var reqparm = new System.Collections.Specialized.NameValueCollection();                                
                                reqparm.Add("sessionid", sessionId);
                                reqparm.Add("objid", jsonKey);
                                reqparm.Add("action", action);
                                reqparm.Add("obj", strParamValue);
                                reqparm.Add("channel", channelName);
                                reqparm.Add("distributiondate", distributionDate);

                                Logger.WriteLog(Logger.LogType.Debug, string.Format("2.CHINHNB log CD: {0} action {1}: {2} by sessionid: {3} channel: {4}", jsonKey, action, ""/*strParamValue*/, sessionId, channelName));

                                byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                                string responsebody = Encoding.UTF8.GetString(responsebytes);
                                if (!string.IsNullOrEmpty(responsebody))
                                {
                                    //test
                                    //responsebody = "{\"Success\": false, \"data\": \"\", \"Message\": \"insert redis err\"}";

                                    var result = NewtonJson.Deserialize<ResultCD>(responsebody);
                                    if (result!=null && !result.Success)
                                    {
                                        //false push CD => push queue để đẩy lại
                                        //notify cd
                                        TelegramNotification.SendNotifyCDErrorAsync(NewtonJson.Serialize(new
                                        {
                                            domainCD=url,
                                            channel = channelName,
                                            action,
                                            sessionId = sessionId,
                                            objId = jsonKey,
                                            message = result?.Message
                                        }));

                                        //push queue redis                                
                                        BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueCD("queuepushdatacderror", sessionId, jsonKey, action, strParamValue, channelName, distributionDate, result?.Message);
                                    }
                                }

                                Logger.WriteLog(Logger.LogType.Debug, string.Format("3.End push to cd for {0} action {1}: {2} by sessionid: {3} channel: {4}", jsonKey, action, responsebody, sessionId, channelName));
                            }
                            catch(Exception ex)
                            {
                                //notify cd
                                TelegramNotification.SendNotifyCDErrorAsync(NewtonJson.Serialize(new {
                                    domainCD = url,
                                    channel = channelName,
                                    action,
                                    sessionId= sessionId,
                                    objId = jsonKey,
                                    message = ex.Message
                                }));

                                //push queue redis                                
                                BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueCD("queuepushdatacderror", sessionId, jsonKey, action, strParamValue, channelName, distributionDate, ex.Message);
                                //log
                                Logger.WriteLog(Logger.LogType.Error, "4.Error => jsonKey:"+ jsonKey + ", action: " + action + ", ex: " + ex.Message);                                
                            }
                        }
                    });
                    try
                    {
                        if (task.Wait(TimeSpan.FromSeconds(3)))
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ContentDelivery.Push => " + ex.Message);
            }
        }
        public class ResultCD
        {
            public bool Success { get; set; }
            public string Data { get; set; }
            public string Message { get; set; }
        }

        public static ResultStreamCD ListStreamDataCD()
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowStreamDataCD") == 1)
                {
                    var sec= CmsChannelConfiguration.GetAppSetting("SecretkeyStreamDataCD");
                    var url = CmsChannelConfiguration.GetAppSetting("UrlStreamDataCD");
                    var channelName = WcfExtensions.WcfMessageHeader.Current.Namespace;
                    using (WebClient wc = new WebClient())
                    {
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        wc.Headers["sec_key"] = sec;
                        var reqparm = new System.Collections.Specialized.NameValueCollection();
                        reqparm.Add("sec_key", sec);
                        reqparm.Add("channel", channelName);

                        byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                        string responsebody = Encoding.UTF8.GetString(responsebytes);
                        if (!string.IsNullOrEmpty(responsebody))
                        {                            
                            return NewtonJson.Deserialize<ResultStreamCD>(responsebody);                            
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ListStreamDataCD => " + ex.Message);
                return null;
            }
        }

        public static bool SaveStreamV2(string account, string positionData)
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {                    
                    var url = CmsChannelConfiguration.GetAppSetting("UrlPushDataCD");
                    var channelName = WcfExtensions.WcfMessageHeader.Current.Namespace;
                    using (WebClient wc = new WebClient())
                    {
                        var sessionId = Guid.NewGuid().ToString();

                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";                        
                        var reqparm = new System.Collections.Specialized.NameValueCollection();
                        if (string.IsNullOrEmpty(positionData))
                            positionData = "[]";
                        
                        var jsonKey = "{}";
                        var obj = "{\"PositionData\":" + positionData + ",\"Account\":" + "\"" + account + "\"}";

                        reqparm.Add("sessionid", sessionId);
                        reqparm.Add("objid", jsonKey);
                        reqparm.Add("action", "savestreamv2");
                        reqparm.Add("obj", obj);
                        reqparm.Add("channel", channelName);
                        reqparm.Add("distributiondate", DateTime.Now.ToString());

                        Logger.WriteLog(Logger.LogType.Debug, string.Format("2.CHINHNB log CD: {0} action {1}: {2} by sessionid: {3} channel: {4}", jsonKey, "savestreamv2", obj, sessionId, channelName));

                        byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                        string responsebody = Encoding.UTF8.GetString(responsebytes);

                        var result = NewtonJson.Deserialize<ResultCD>(responsebody);
                        if (result != null)
                        {
                            Logger.WriteLog(Logger.LogType.Debug, string.Format("3.End push to cd for {0} action {1}: {2} by sessionid: {3} channel: {4}, responsebody: {5}", jsonKey, "savestreamv2", obj, sessionId, channelName, responsebody));
                            return result.Success;
                        }                        
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "SaveStreamV2 => " + ex.Message);
                return false;
            }
        }
        public class ResultStreamCD
        {
            public List<string> block1 { get; set; }
            public List<string> block2 { get; set; }
            public List<string> block3 { get; set; }            
        }
    }
}