﻿using ChannelVN.CMS.WcfMapping.ServiceExpertSuggestion;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class ExpertSuggestionService
    {
        #region Seoer review
        public static WcfActionResponse SendReview(NewsReviewEntity entity, string UserIdTags)
        {
            var client = new ExpertSuggestionServiceClient();
            try {
                return client.Receive(entity, UserIdTags);
            }
            finally
            {
                client.Close();
            }            
        }
        public static NewsReviewEntity NewsReview_GetByNewsId(long NewsId, NewsReviewStatus status, string Domain)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.NewsReview_GetByNewsId(NewsId, status, Domain);
            }
            finally
            {
                services.Close();
            }

        }
        public static int NewsReview_CounterDone(string ZoneIds)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.NewsReview_CounterDone(ZoneIds, Namespace);
            }
            finally
            {
                services.Close();
            }
        }
        public static NewsReviewEntity NewsReview_Done(long NewsId, string Domain)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.NewsReview_Done(NewsId, Domain);
            }
            finally
            {
                services.Close();
            }

        }
        public static WcfActionResponse NewsReview_ChangeStatus(long NewsId, NewsReviewStatus status, string Domain)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.NewsReview_ChangeStatus(NewsId, status, Domain);
            }
            catch (Exception ex)
            {
                return new WcfActionResponse { Success = false };
            }
            finally
            {
                services.Close();
            }

        }
        public static List<NewsReviewEntity> NewsReview_GetListByStatus(NewsReviewStatus status, string ZoneIds, string Domain, DateTime DateFrom, DateTime DateTo, int PageSize, int PageIndex, ref int TotalRow)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var data= services.NewsReview_GetListByStatus(status, ZoneIds, Domain, DateFrom, DateTo, PageSize, PageIndex, ref TotalRow);

                return data.ToList();
            }
            finally
            {
                services.Close();
            }

        }
        #endregion
        #region Expert Review
        public static WcfActionResponse SendExpertReview(ExpertReviewEntity entity)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.ExpertReceive(entity);
            }
            finally
            {
                services.Close();
            }
        }
        public static ImsGetUserEntity[] ListExpertUser(int ZoneId, long NewsId, int pageIndex, int pageSize, ref int totalRow)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.GetUserByZoneId(ZoneId, Namespace, NewsId, pageIndex, pageSize, ref totalRow);
            }
            finally
            {
                services.Close();
            }
        }
        public static List<NewsIdEntity> GetListNewsIdReviewDoneByNewsIds(string newsids)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.GetListNewsIdReviewDoneByNewsIds(newsids, Namespace).ToList();
            }
            finally
            {
                services.Close();
            }
        }
        public static ExpertReviewEntity GetExpertReviewByNewsId(long newsid)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.GetExpertReviewByNewsId(newsid, Namespace);
            }
            finally
            {
                services.Close();
            }
        }
        public static WcfActionResponse ExpertReview_ChangeStatus(int newsId, ExpertReviewStatus status)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.ExpertReview_ChangeStatus(newsId, Namespace, status);
            }
            finally
            {
                services.Close();
            }

        }
        #endregion

        #region Tag Queue
        public static List<TagFullInfoEntity> Tag_GetByQueue()
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.Tag_GetByQueue(Namespace).ToList();
            }
            finally
            {
                services.Close();
            }

        }
        public static bool TagQueue_UpdateStatus(string idTags)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.TagQueue_UpdateStatus(idTags, Namespace);
            }
            finally
            {
                services.Close();
            }

        }
        #endregion
        #region SeoerReview Queue
        public static List<QueueSeoerReviewEntity> Queue_SeoerReview_GetList()
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.Queue_SeoerReview_GetList(Namespace).ToList();
            }
            finally
            {
                services.Close();
            }

        }
        public static bool Queue_SeoerReview_UpdateStatus(string newsIds)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.Queue_SeoerReview_UpdateStatus(newsIds, Namespace);
            }
            finally
            {
                services.Close();
            }

        }
        #endregion

        public static TagHotEntity[] TagHot_GetListByToday()
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.TagHot_GetListByToday(Namespace);
            }
            finally
            {
                services.Close();
            }

        }

        public static ImsGetUserEntity[] GetAllUser()
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.GetAllUser();
            }
            finally
            {
                services.Close();
            }

        }

        #region Discussion
        public static DiscussionV2DetailEntity GetJobDetailByContentId(long ContentId, string UserName)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.GetJobDetailByContentId(ContentId, UserName);
            }
            finally
            {
                services.Close();
            }

        }
        public static DiscussionReportDetailEntity[] GetListDiscussionByTopic(long topicId, string UserName)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.GetListDiscussionByTopic(topicId, UserName);
            }
            finally
            {
                services.Close();
            }

        }
        public static WcfActionResponse Ims_InsertDiscussion(string listFile, string CreatedBy, string Content, string receivers, int topicId, int parentId)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.Ims_InsertDiscussion(listFile, CreatedBy, Content, receivers, topicId, parentId);
            }
            finally
            {
                services.Close();
            }

        }
        #endregion

        #region BoxThreadEmbedQueue
        public static List<QueueBoxThreadEmbedEntity> BoxThreadEmbedGetListQueue()
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.GetQueueBoxThreadList().ToList();
            }
            finally
            {
                services.Close();
            }
        }

        public static bool BoxThreadEmbedUpdateStatus(int id, int status)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                return services.UpdateStatus(id, status);
            }
            finally
            {
                services.Close();
            }
        }
        #endregion

        #region Thread Queue
        public static List<ThreadInChannelEntity> ThreadQueue_GetByNamespace()
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.QueueThread_GetByNamespace(Namespace).ToList();
            }
            finally
            {
                services.Close();
            }

        }
        public static bool ThreadQueue_UpdateStatus(string idTags)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.ThreadQueue_UpdateStatus(idTags, Namespace);
            }
            finally
            {
                services.Close();
            }
        }
        #endregion

        #region Tag

        public static TagSuggestionEntity[] TagSuggestion(string keyword, int pageSize, int pageIndex, ref int totalRow)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.TagSuggestion(keyword, Namespace, pageSize, pageIndex, ref totalRow);
            }
            finally
            {
                services.Close();
            }
        }

        //Tag In Thread
        public static TagSuggestionEntity[] IMS_GetTagInThread(int threadId)
        {
            var services = new ExpertSuggestionServiceClient();
            try
            {
                var Namespace = WcfMessageHeader.Current.Namespace; //AppConfigs.CmsApiNamespace;
                return services.IMS_GetTagInThread(threadId, Namespace);
            }
            finally
            {
                services.Close();
            }
        }
        #endregion
    }
}