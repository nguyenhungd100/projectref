﻿using ChannelVN.CMS.Common.ChannelConfig;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.IO;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public class VcStorageServices
    {
        #region CONFIG Video configuration

        private static string VideoSecretkey
        {
            get
            {
                return CmsChannelConfiguration.GetAppSetting(VideoStorageServices.VIDEO_MANAGER_SECRET_KEY);
            }
        }
        private static string VideoClientHash
        {
            get
            {
                return CmsChannelConfiguration.GetAppSetting(VideoStorageServices.VIDEO_MANAGER_CLIENT_HASH);
            }
        }
        private static string VideoNamespace
        {
            get
            {
                return CmsChannelConfiguration.GetAppSetting(VideoStorageServices.VIDEO_MANAGER_UPLOAD_NAMESPACE);
            }
        }

        private static int VideoMaxFileSize
        {
            get
            {
                return CmsChannelConfiguration.GetAppSettingInInt32(FileStorageServices.FILE_MANAGER_UPLOAD_MAXLENGTH);
            }
        }
        private static int VideoRequestTimeExpired
        {
            get
            {
                return CmsChannelConfiguration.GetAppSettingInInt32(FileStorageServices.FILE_MANAGER_UPLOAD_EXPIRED_TIME);
            }
        }

        #endregion

        #region CONFIG Api Urls

        private static string VideoUploadApiUrl
        {
            get
            {
                var apiUrl = CmsChannelConfiguration.GetAppSetting("VIDEO_MANAGER_HTTPUPLOAD");
                if (string.IsNullOrEmpty(apiUrl))
                {
                    apiUrl = "http://upload2.vcmedia.vn/upload";
                }
                return apiUrl;
            }
        }
        private static string VideoCallbackAfterDownloadApiUrl
        {
            get
            {
                var apiUrl = CmsChannelConfiguration.GetAppSetting("VIDEO_MANAGER_CALLBACK_AFTER_DOWNLOAD");
                if (string.IsNullOrEmpty(apiUrl))
                {
                    apiUrl = "https://vscc1-hosting.vcmedia.vn/CallBack.svc/CallBackAfterDownload?account={0}&client_hash={1}&save_file_path={2}";
                }
                return apiUrl;
            }
        }
        private static string VideoCallbackAfterUploadApiUrl
        {
            get
            {
                var apiUrl = CmsChannelConfiguration.GetAppSetting(VideoStorageServices.VIDEO_MANAGER_UPLOAD_CALLBACK);
                if (string.IsNullOrEmpty(apiUrl))
                {
                    apiUrl = "http://vscc1.hosting.vcmedia.vn/CallBack.svc/MediaCallBack?client_hash={0}&account={1}&key={2}";
                }
                return apiUrl;
            }
        }

        #endregion

        public static bool DownloadVideoFromExternalLink(string externalUrl, string saveFilePath, bool useWatermark = false, string callbackUrl = "")
        {
            try
            {
                if (string.IsNullOrEmpty(externalUrl)) return false;
                if (string.IsNullOrEmpty(saveFilePath))
                {
                    var externalFileName = externalUrl.Substring(externalUrl.LastIndexOf("/") + 1);
                    saveFilePath = "video/" + DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + externalFileName;
                }
                else
                {
                    if (saveFilePath.StartsWith("/")) saveFilePath = saveFilePath.Remove(0, 1);
                }

                if (!saveFilePath.Contains("/")) saveFilePath = "video/" + DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day + "/" + saveFilePath;

                var postParameters = new Dictionary<string, object>
                {
                    {"secret_key", VideoSecretkey},
                    {"filename", saveFilePath},
                    {"source", externalUrl},
                    {"convert", "1"},
                    {"overwrite", "1"},
                    {"watermark", useWatermark ? "1" : "0"},
                    {"callback_url", callbackUrl}
                };
                const string userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.1) Gecko/20060111 Firefox/1.5.0.1";
                var result = "";
                var errorMess = "";
                var filename = "";
                //Logger.WriteLog(Logger.LogType.Trace, VideoUploadApiUrl + " => " + NewtonJson.Serialize(postParameters));
                using (HttpWebResponse webResponse = FormUpload.MultipartFormDataPost(VideoUploadApiUrl, userAgent, postParameters, ref result, ref errorMess, filename))
                {
                    // Process response
                    if (webResponse == null)
                    {
                        return false;
                    }
                    else
                    {
                        using (StreamReader responseReader = new StreamReader(webResponse.GetResponseStream()))
                        {
                            string fullResponse = responseReader.ReadToEnd();
                            webResponse.Close();
                            responseReader.Close();
                            return fullResponse.Equals("ok", StringComparison.CurrentCultureIgnoreCase);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "VcStorageServices.DownloadVideoFromExternalLink('" + externalUrl + "', '" + saveFilePath + "', " + useWatermark + ", '" + callbackUrl + "') => " + ex.ToString());
            }
            return false;
        }
        public static bool DownloadVideoFromExternalLink_Overwrite(string externalUrl, string saveFilePath, bool useWatermark = false, string callbackUrl = "")
        {
            try
            {
                if (string.IsNullOrEmpty(externalUrl)) return false;

                var postParameters = new Dictionary<string, object>
                {
                    {"secret_key", VideoSecretkey},
                    {"filename", saveFilePath},
                    {"source", externalUrl},
                    {"convert", "1"},
                    {"overwrite", "1"},
                    {"watermark", useWatermark ? "1" : "0"},
                    {"callback_url", callbackUrl}
                };
                const string userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.1) Gecko/20060111 Firefox/1.5.0.1";
                var result = "";
                var errorMess = "";
                var filename = "";
                //Logger.WriteLog(Logger.LogType.Trace, VideoUploadApiUrl + " => " + NewtonJson.Serialize(postParameters));
                using (HttpWebResponse webResponse = FormUpload.MultipartFormDataPost(VideoUploadApiUrl, userAgent, postParameters, ref result, ref errorMess, filename))
                {
                    // Process response
                    if (webResponse == null)
                    {
                        return false;
                    }
                    else
                    {
                        using (StreamReader responseReader = new StreamReader(webResponse.GetResponseStream()))
                        {
                            string fullResponse = responseReader.ReadToEnd();
                            webResponse.Close();
                            responseReader.Close();
                            return fullResponse.Equals("ok", StringComparison.CurrentCultureIgnoreCase);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "VcStorageServices.DownloadVideoFromExternalLink('" + externalUrl + "', '" + saveFilePath + "', " + useWatermark + ", '" + callbackUrl + "') => " + ex.ToString());
            }
            return false;
        }
        public static bool VideoDownloadCallback(string saveFilePath, string account, ref string keyVideo)
        {
            if (string.IsNullOrEmpty(saveFilePath)) return false;

            var apiResult = "";
            try
            {
                var requestUrl = string.Format(VideoCallbackAfterDownloadApiUrl, account, VideoClientHash, saveFilePath);
                //Logger.WriteLog(Logger.LogType.Debug, "VideoDownloadCallback() => " + requestUrl);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
                request.ContentType = "application/json; charset=utf-8";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        apiResult = String.Format(
                            "GET failed. Received HTTP {0}",
                            response.StatusCode);
                        throw new ApplicationException(apiResult);
                    }
                    StreamReader stIn = new StreamReader(response.GetResponseStream());
                    apiResult = stIn.ReadToEnd();
                    stIn.Close();
                    Logger.WriteLog(Logger.LogType.Debug, "VideoDownloadCallback() result => " + apiResult);

                    var returnObject = NewtonJson.Deserialize<ResponseInfo>(apiResult);
                    keyVideo = Utility.ConvertToString(returnObject.Data);
                    return returnObject.Code == RETURN_CODE.OK;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return false;
        }

        #region Utility

        [DataContract]
        private enum RETURN_CODE
        {
            [EnumMember]
            UNKNOWN = 100,
            [EnumMember]
            OK = 200,
            [EnumMember]
            NO_CONTENT = 204,
            [EnumMember]
            BAD_REQUEST = 400,
            [EnumMember]
            UNAUTHORIZED = 401,
            [EnumMember]
            NOT_FOUND = 404,
            [EnumMember]
            REQUEST_TIMEOUT = 408,
            [EnumMember]
            NO_RESPONSE = 444,
            [EnumMember]
            INTERNAL_SERVER_ERROR = 500,
            [EnumMember]
            REQUEST_API_SE_ERROR = 501,
            [EnumMember]
            SQL_ERROR = 502,
            [EnumMember]
            NO_FOUND_DATA_SE = 503,
            [EnumMember]
            CLIENT_HASH_NO_EXIST = 504,
            [EnumMember]
            ERROR_INSERT_MEDIA = 505,
            [EnumMember]
            NOT_FOUND_FILE_AFTER_INSERT = 506,
            [EnumMember]
            ERROR_UPDATE_THUMB = 507,
            [EnumMember]
            ERROR_GET_INFO_FILE = 508,
            [EnumMember]
            PARAM_EMPTY = 509,
            [EnumMember]
            VIDEO_DANG_SU_LY = 510,
            [EnumMember]
            VIDEO_DA_CONVERT = 511,
            [EnumMember]
            VIDEO_IS_MP4_NOT_CONVERT = 512,
            [EnumMember]
            VIDEO_DANG_SU_LY_OR_CHUA_CONVERT = 513,
            [EnumMember]
            VIDEO_KHONG_THE_CONVERT = 514,
            [EnumMember]
            SQL_ERROR_GET_FILE_INFO = 515,
            [EnumMember]
            NOT_FOUND_OR_UNAUTHORIZED = 516,
            [EnumMember]
            ADDED_TO_SHARE = 517,
            [EnumMember]
            VIDEO_IS_NO_SHARE_OR_NO_EXIST = 518,
            [EnumMember]
            ERROR_DOWMNLOAD_IMAGE = 519
        }
        [DataContract]
        private class ResponseInfo
        {
            [DataMember]
            public RETURN_CODE Code { set; get; }
            [DataMember]
            public object Message { set; get; }
            [DataMember]
            public object Data { set; get; }
        }

        private static class FormUpload
        {
            private static readonly Encoding encoding = Encoding.UTF8;
            private static readonly string[] HttpCode = { HttpStatusCode.Conflict.ToString(), HttpStatusCode.RequestTimeout.ToString(), HttpStatusCode.UnsupportedMediaType.ToString(), HttpStatusCode.InternalServerError.ToString(), HttpStatusCode.GatewayTimeout.ToString() };
            private static readonly string[] HttpMeaning = { "File này đã tồn tại", "Truy vấn TimeOut", "Server không hỗ trợ định dạng file", "Lỗi server", "Gateway Timeout" };

            public static HttpWebResponse MultipartFormDataPost(string postUrl, string userAgent, Dictionary<string, object> postParameters, ref string result, ref string errorMess, string filename)
            {
                string formDataBoundary = "-----------------------------28947758029299";
                string contentType = "multipart/form-data; boundary=" + formDataBoundary;

                byte[] formData = GetMultipartFormData(postParameters, formDataBoundary);

                return PostForm(postUrl, userAgent, contentType, formData, ref result, ref errorMess, filename);
            }
            public static HttpWebResponse MultipartFormDataPostNormal(string postUrl, string userAgent, Dictionary<string, object> postParameters, ref string result, ref string errorMess, string filename)
            {
                string formDataBoundary = "-----------------------------28947758029299";
                string contentType = "multipart/form-data; boundary=" + formDataBoundary;

                byte[] formData = GetMultipartFormDataNormal(postParameters, formDataBoundary);

                return PostForm(postUrl, userAgent, contentType, formData, ref result, ref errorMess, filename);
            }

            public static HttpWebResponse PostForm(string postUrl, string userAgent, string contentType, byte[] formData,
                                                   ref string result, ref string errorMess, string filename)
            {
                string strFormat = "-Upload file \"{0}\" {1}";
                try
                {
                    HttpWebRequest request = WebRequest.Create(postUrl) as HttpWebRequest;
                    if (request == null)
                    {
                        throw new NullReferenceException("request is not a http request");
                    }
                    // Set up the request properties
                    request.Method = "POST";
                    request.Timeout = 1000 * 60 * 10;
                    request.KeepAlive = true;
                    request.ContentType = contentType;
                    request.UserAgent = userAgent;
                    request.CookieContainer = new CookieContainer();
                    request.ContentLength = formData.Length;  // We need to count how many bytes we're sending. 
                    HttpWebResponse res = null;
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        // Push it out there
                        requestStream.Write(formData, 0, formData.Length);
                        requestStream.Close();
                        requestStream.Dispose();
                    }

                    try
                    {
                        res = (HttpWebResponse)request.GetResponse();
                        result += string.Format(strFormat, filename, "thành công") + "<br />";
                    }
                    catch (Exception ex)
                    {
                        strFormat += " : {2}";
                        result += string.Format(strFormat, filename, "không thành công", GetMeaning(ex.Message)) + "<br />";
                        errorMess += GetErrorCode(ex.Message);
                        Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                    }
                    return res;// request.GetResponse() as HttpWebResponse;
                }
                catch (Exception ex)
                {
                    strFormat += " : {2}";
                    result += string.Format(strFormat, filename, "không thành công", "lỗi kết nối") + "<br />";
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }
                return null;
            }

            public static byte[] GetMultipartFormData(Dictionary<string, object> postParameters, string boundary)
            {
                Stream formDataStream = new System.IO.MemoryStream();

                foreach (KeyValuePair<string, object> param in postParameters)
                {
                    if (param.Value is FileParameter)
                    {
                        FileParameter fileToUpload = (FileParameter)param.Value;

                        // Add just the first part of this param, since we will write the file data directly to the Stream
                        string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\";\r\nContent-Type: {3}\r\n\r\n",
                            boundary,
                            param.Key,
                            fileToUpload.FileName ?? param.Key,
                            //"application/octet-stream");
                            fileToUpload.ContentType ?? "application/octet-stream");

                        formDataStream.Write(encoding.GetBytes(header), 0, header.Length);

                        // Write the file data directly to the Stream, rather than serializing it to a string.
                        formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
                        // Thanks to feedback from commenters, add a CRLF to allow multiple files to be uploaded
                        formDataStream.Write(encoding.GetBytes("\r\n"), 0, 2);
                    }
                    else
                    {
                        string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}\r\n",
                            boundary,
                            param.Key,
                            param.Value);
                        formDataStream.Write(encoding.GetBytes(postData), 0, postData.Length);
                    }
                }

                // Add the end of the request
                string footer = "\r\n--" + boundary + "--\r\n";
                formDataStream.Write(encoding.GetBytes(footer), 0, footer.Length);

                // Dump the Stream into a byte[]
                formDataStream.Position = 0;
                byte[] formData = new byte[formDataStream.Length];
                formDataStream.Read(formData, 0, formData.Length);
                formDataStream.Close();

                return formData;
            }

            public static byte[] GetMultipartFormDataNormal(Dictionary<string, object> postParameters, string boundary)
            {
                Stream formDataStream = new System.IO.MemoryStream();

                foreach (KeyValuePair<string, object> param in postParameters)
                {
                    if (param.Value is FileParameter)
                    {
                        FileParameter fileToUpload = (FileParameter)param.Value;

                        // Add just the first part of this param, since we will write the file data directly to the Stream
                        string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\";\r\nContent-Type: {3}\r\n\r\n",
                            boundary,
                            param.Key,
                            fileToUpload.FileName ?? param.Key,
                            //"application/octet-stream");
                            fileToUpload.ContentType ?? "application/octet-stream");

                        formDataStream.Write(encoding.GetBytes(header), 0, header.Length);

                        // Write the file data directly to the Stream, rather than serializing it to a string.
                        formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
                        // Thanks to feedback from commenters, add a CRLF to allow multiple files to be uploaded
                        formDataStream.Write(encoding.GetBytes("\r\n"), 0, 2);
                    }
                    else
                    {
                        string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                            boundary,
                            param.Key,
                            param.Value);
                        formDataStream.Write(encoding.GetBytes(postData), 0, postData.Length);
                    }
                }

                // Add the end of the request
                string footer = "\r\n--" + boundary + "--\r\n";
                formDataStream.Write(encoding.GetBytes(footer), 0, footer.Length);

                // Dump the Stream into a byte[]
                formDataStream.Position = 0;
                byte[] formData = new byte[formDataStream.Length];
                formDataStream.Read(formData, 0, formData.Length);
                formDataStream.Close();

                return formData;
            }

            public class FileParameter
            {
                private byte[] _File = new byte[] { };
                private string _FileName = string.Empty;
                private string _ContentType = string.Empty;

                public byte[] File
                {
                    get { return _File; }
                    set { _File = value; }
                }
                public string FileName
                {
                    get { return _FileName; }
                    set { _FileName = value; }
                }

                public string ContentType
                {
                    get { return _ContentType; }
                    set { _ContentType = value; }
                }
                public FileParameter(byte[] file) : this(file, null) { }
                public FileParameter(byte[] file, string filename) : this(file, filename, null) { }
                public FileParameter(byte[] file, string filename, string contenttype)
                {
                    File = file;
                    FileName = filename;
                    ContentType = contenttype;
                }
            }
            private static string GetMeaning(string str)
            {
                for (int i = 0; i < HttpCode.Length; i++)
                {
                    if (str.ToLower().Contains(HttpCode[i].ToLower()))
                        return HttpMeaning[i];
                }
                return "Lỗi không xác định";
            }
            private static string GetErrorCode(string str)
            {
                for (int i = 0; i < HttpCode.Length; i++)
                {
                    if (str.ToLower().Contains(HttpCode[i].ToLower()))
                        return HttpCode[i];
                }
                return string.Empty;
            }
        }

        #endregion
    }
}