﻿using System;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.External.Kenh14.HotNewsConfig;
using ChannelVN.CMS.Entity.External.Kenh14.HotNewsConfig;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using System.Collections.Generic;
using ChannelVN.CMS.WebApi.Services.Common;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices.Kenh14
{
    public class HotNewsConfigServices
    {

        public WcfActionResponse UpdateHotNewsConfig(HotNewsConfigEntity hotNewsConfigEntity)
        {
            try
            {
                var errorCode = HotNewsConfigBo.UpdateHotNewsConfig(hotNewsConfigEntity);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(hotNewsConfigEntity.ZoneId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteHotNewsConfig(int zoneId)
        {
            try
            {
                var errorCode = HotNewsConfigBo.DeleteHotNewsConfig(zoneId);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(zoneId.ToString(), "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<HotNewsConfigEntity> GetListHotNewsConfig()
        {
            return HotNewsConfigBo.GetListHotNewsConfig();
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}