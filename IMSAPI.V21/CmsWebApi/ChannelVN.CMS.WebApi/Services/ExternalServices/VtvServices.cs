﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Services.ExternalServices
{
    public static class VtvServices
    {        
        public static List<object> GetVideoZoneListVTV()
        {
            var data = new List<object>();

            //cached xử sau

            if (data != null && data.Count<=0)
            {
                var result = string.Empty;
                var apiUrl = CmsChannelConfiguration.GetAppSetting("UrlGetZoneVideoVtv");
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(apiUrl);
                    request.Method = "GET";

                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            result = string.Format("POST failed. Received HTTP {0}", response.StatusCode);
                            throw new ApplicationException(result);
                        }
                        var stIn = new StreamReader(response.GetResponseStream());
                        result = stIn.ReadToEnd();
                        stIn.Close();
                    }

                    if (!string.IsNullOrEmpty(result))
                    {
                        data = NewtonJson.Deserialize<List<object>>(result);
                        if (data != null) return data;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return data;
        }

        public static List<object> ImportProgramScheduleDetailApiVtv(DateTime datetime, int vtvId)
        {
            var data = new List<object>();

            //cached xử sau

            if (data != null && data.Count <= 0)
            {
                var result = string.Empty;
                var apiUrl = CmsChannelConfiguration.GetAppSetting("UrlGetProgramScheduleDetailToVtv");
                var date = datetime.Day + "-" + datetime.Month + "-" + datetime.Year;
                if (datetime.Month < 10)
                {
                    date = "0" + datetime.Day + "-0" + datetime.Month + "-" + datetime.Year;
                }                
                var apiFomat = string.Format(apiUrl, date, vtvId);
                try
                {
                    var request = (HttpWebRequest)WebRequest.Create(apiFomat);
                    request.Method = "GET";

                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            result = string.Format("POST failed. Received HTTP {0}", response.StatusCode);
                            throw new ApplicationException(result);
                        }
                        var stIn = new StreamReader(response.GetResponseStream());
                        result = stIn.ReadToEnd();
                        stIn.Close();
                    }

                    if (!string.IsNullOrEmpty(result))
                    {
                        data = NewtonJson.Deserialize<List<object>>(result);
                        if (data != null) return data;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return data;
        }
    }

    [DataContract]
    public enum VtvProgramChannelId
    {
        [EnumMember]
        VTV1 =1,
        [EnumMember]
        VTV2 =2,
        [EnumMember]
        VTV3 =3,
        [EnumMember]
        VTV4 =4,
        [EnumMember]
        VTV5 =5,
        [EnumMember]
        VTV6 =6,
        [EnumMember]
        VTV7 =7,
        [EnumMember]
        VTV8 =8,
        [EnumMember]
        VTV9 =9,
        [EnumMember]
        VTVLive =10,
        [EnumMember]
        VTVCab2PhimViet =11,
        [EnumMember]
        VTVCab6HayTV =13,
        [EnumMember]
        VTVCab8BiBi =14,
        [EnumMember]
        VTVCab1 =18,
        [EnumMember]
        VTVCab3ThethaoTV =19,
        [EnumMember]
        VTVCab7DDramas =20,
        [EnumMember]
        VTVCab17Yeah1 =21,
        [EnumMember]
        VTV5TayNamBo =25,
        [EnumMember]
        VTVNews =26
    }
}