﻿using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services
{
    public class Services
    {
        private static readonly Assembly CurrentAssembly = Assembly.GetExecutingAssembly();        

        public static T CreatedInstance<T>(NameValueCollection body) where T : class
        {
            var className = typeof(T).Name;
            var assembly = "ChannelVN.CMS.WebApi.Services.Base." + className;
            var nameSpace = WcfMessageHeader.Current.Namespace;//AppConfigs.CmsApiNamespace;

            assembly = assembly.Replace(".Base." + className, ".External." + nameSpace + "." + className);

            var basenews = (T)CurrentAssembly.CreateInstance(assembly, false,
                                                                        BindingFlags.CreateInstance, null,
                                                                        new object[] { body },
                                                                        System.Globalization.CultureInfo.CurrentCulture,
                                                                        null);
            if (basenews == null)
            {
                //Full
                assembly = "ChannelVN.CMS.WebApi.Services.External.Full."+ className;

                basenews = (T)CurrentAssembly.CreateInstance(assembly, false,
                                                                        BindingFlags.CreateInstance, null,
                                                                        new object[] { body },
                                                                        System.Globalization.CultureInfo.CurrentCulture,
                                                                        null);
            }

            return basenews;
        }
    }
}