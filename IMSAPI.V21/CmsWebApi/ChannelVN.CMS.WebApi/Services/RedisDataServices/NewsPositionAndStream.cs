﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.WebApi.Services.Base;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Services.RedisDataServices
{
    public class NewsPositionAndStream
    {
        public static void UpdateAdtechStreamData()
        {
            try
            {                
                var enablePush = CmsChannelConfiguration.GetAppSettingInInt32("EnablePushTopHotTimeline");
                if (enablePush == 1)
                {
                    var lstNewsForRedis = new NewsServices().NewsHot_AllForRedis();
                    var redisSetting = CmsChannelConfiguration.GetAppSetting("REDIS_HOST_FOR_NEWS_POSITION_TIMELINE");
                    if (!string.IsNullOrEmpty(redisSetting) && redisSetting.Split(':').Length >= 2)
                    {
                        var hostConfigs = redisSetting.Split(':');
                        var redisHost = hostConfigs[0];
                        var redisPort = Utility.ConvertToInt(hostConfigs[1]);
                        var redisDb = Utility.ConvertToInt(hostConfigs[2]);
                        var redisPass = hostConfigs.Length >= 4 ? Utility.ConvertToString(hostConfigs[3]) : "";
                        RedisClient redisClient = !string.IsNullOrEmpty(redisPass) ? new RedisClient(redisHost, redisPort, redisPass) : new RedisClient(redisHost, redisPort);
                        redisClient.Db = redisDb;
                        // Last item is password
                        if (hostConfigs.Length > 3)
                        {
                            redisClient.Password = hostConfigs[3];
                        }
                        //var redisClient = new RedisStackClient(redisHost, redisPort, redisDb);
                        string redisKey = "IMS:NewsTopHot:" + CmsChannelConfiguration.CurrentChannelNamespace;
                        try
                        {
                            if (redisClient.ContainsKey(redisKey)) redisClient.Remove(redisKey);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, "NewsHot_SaveToRedis() => " + ex.ToString());
                        }
                        redisClient.Add<List<NewsHotEntity>>(redisKey, lstNewsForRedis, DateTime.Now.AddYears(1).TimeOfDay);
                        var listIds = "";
                        foreach (var item in lstNewsForRedis)
                        {
                            listIds += ";" + item.newsId;
                        }
                        Logger.WriteLog(Logger.LogType.Trace, "NewsHot_SaveToRedis() => " + listIds);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        public static void UpdateAdtechStreamData_CafeF()
        {
            try
            {
                try
                {
                    var redisLogSetting = CmsChannelConfiguration.GetAppSetting("REDIS_HOST_FOR_CLIENT_ERROR_TRACKING");
                    var hostLogConfigs = redisLogSetting.Split(':');
                    var redisLogHost = hostLogConfigs[0];
                    var redisLogPort = Utility.ConvertToInt(hostLogConfigs[1]);
                    var redisLogDb = Utility.ConvertToInt(hostLogConfigs[2]);

                    RedisClient redisClientLog = new RedisClient(redisLogHost, redisLogPort, db: redisLogDb);
                    redisClientLog.Incr("UpdateAdtechStreamData");
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                var enablePush = CmsChannelConfiguration.GetAppSettingInInt32("EnablePushTopHotTimeline");
                if (enablePush == 1)
                {
                    var lstNewsForRedis = new NewsServices().NewsHot_AllForRedisByCafeF();
                    var redisSetting = CmsChannelConfiguration.GetAppSetting("REDIS_HOST_FOR_NEWS_POSITION_TIMELINE");
                    if (!string.IsNullOrEmpty(redisSetting) && redisSetting.Split(':').Length >= 2)
                    {
                        var hostConfigs = redisSetting.Split(':');
                        var redisHost = hostConfigs[0];
                        var redisPort = Utility.ConvertToInt(hostConfigs[1]);
                        var redisDb = Utility.ConvertToInt(hostConfigs[2]);
                        var redisPass = hostConfigs.Length >= 4 ? Utility.ConvertToString(hostConfigs[3]) : "";
                        RedisClient redisClient = !string.IsNullOrEmpty(redisPass) ? new RedisClient(redisHost, redisPort, redisPass) : new RedisClient(redisHost, redisPort);
                        redisClient.Db = redisDb;
                        // Last item is password
                        if (hostConfigs.Length > 3)
                        {
                            redisClient.Password = hostConfigs[3];
                        }
                        //var redisClient = new RedisStackClient(redisHost, redisPort, redisDb);
                        string redisKey = "IMS:NewsTopHot:" + CmsChannelConfiguration.CurrentChannelNamespace;
                        try
                        {
                            if (redisClient.ContainsKey(redisKey)) redisClient.Remove(redisKey);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, "NewsHot_SaveToRedis() => " + ex.ToString());
                        }
                        redisClient.Add(redisKey, lstNewsForRedis, DateTime.Now.AddYears(1).TimeOfDay);
                        var listIds = "";
                        foreach (var item in lstNewsForRedis)
                        {
                            listIds += ";" + item.NewsId;
                        }
                        Logger.WriteLog(Logger.LogType.Trace, "NewsHot_SaveToRedis() => " + listIds);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
        public static void UpdateNewsPositionData(int type, int zone)
        {
            try
            {
                //try
                //{
                //    var redisLogSetting = CmsChannelConfiguration.GetAppSetting("REDIS_HOST_FOR_CLIENT_ERROR_TRACKING");
                //    var hostLogConfigs = redisLogSetting.Split(':');
                //    var redisLogHost = hostLogConfigs[0];
                //    var redisLogPort = Utility.ConvertToInt(hostLogConfigs[1]);
                //    var redisLogDb = Utility.ConvertToInt(hostLogConfigs[2]);

                //    RedisClient redisClientLog = new RedisClient(redisLogHost, redisLogPort, db: redisLogDb);
                //    redisClientLog.Incr("UpdateNewsPositionData");
                //}
                //catch (Exception ex)
                //{
                //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                //}
                var enablePush = CmsChannelConfiguration.GetAppSettingInInt32("EnablePushTopHotTimeline");
                if (enablePush == 1)
                {
                    var lstNewsPosition = new NewsServices().GetNewsPositionByPositionType(type, zone, "");
                    //try
                    //{
                    //    var strParamValue = NewtonJson.Serialize(lstNewsPosition);
                    //    var jsonKey = "{\"ZoneId\":" + zone + ", \"TypeId\":" + type + "}";
                    //    ContentDeliveryServices.PushToEs(strParamValue, "updatenewsposition", string.Empty, jsonKey);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    //}
                    var redisSetting = CmsChannelConfiguration.GetAppSetting("REDIS_HOST_FOR_NEWS_POSITION_TIMELINE");
                    if (!string.IsNullOrEmpty(redisSetting) && redisSetting.Split(':').Length >= 2)
                    {
                        var hostConfigs = redisSetting.Split(':');
                        var redisHost = hostConfigs[0];
                        var redisPort = Utility.ConvertToInt(hostConfigs[1]);
                        var redisDb = Utility.ConvertToInt(hostConfigs[2]);
                        var redisPass = hostConfigs.Length >= 4 ? Utility.ConvertToString(hostConfigs[3]) : "";
                        RedisClient redisClient = !string.IsNullOrEmpty(redisPass) ? new RedisClient(redisHost, redisPort, redisPass) : new RedisClient(redisHost, redisPort);
                        redisClient.Db = redisDb;
                        // Last item is password
                        if (hostConfigs.Length > 3)
                        {
                            redisClient.Password = hostConfigs[3];
                        }
                        //var redisClient = new RedisStackClient(redisHost, redisPort, redisDb);
                        string redisKeyForNewsPosition = string.Format("IMS:NewsPositionManual:{0}[{1}_{2}]", CmsChannelConfiguration.CurrentChannelNamespace, type, zone);
                        try
                        {
                            if (redisClient.ContainsKey(redisKeyForNewsPosition)) redisClient.Remove(redisKeyForNewsPosition);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, "NewsPosition_SaveToRedis(" + type + ", " + zone + ") => " + ex.ToString());
                        }
                        redisClient.Add<List<NewsPositionEntity>>(redisKeyForNewsPosition, lstNewsPosition, DateTime.Now.AddYears(1).TimeOfDay);
                        var listIds = "";
                        foreach (var item in lstNewsPosition)
                        {
                            listIds += ";" + item.NewsId;
                        }
                        //Logger.WriteLog(Logger.LogType.Warning, "NewsPosition_SaveToRedis(" + type + ", " + zone + ") => " + listIds);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }
    }
}