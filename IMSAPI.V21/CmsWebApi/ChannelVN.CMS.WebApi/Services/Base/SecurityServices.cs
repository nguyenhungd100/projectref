﻿using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BO.Base.AuthenticateLog;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BoCached.Entity.Security;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.SecureLib.TwoFactor;
using ChannelVN.SocialNetwork.BO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class SecurityServices
    {
        #region UserDataBo

        public WcfActionResponse<UserCachedEntity> ValidAccount(string username, string password, ref UserCachedEntity userInfo)
        {
            try
            {                
                var errorCode = UserDataBo.ValidAccount(username, password,ref userInfo);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    userInfo.User.Password = "";
                    return WcfActionResponse<UserCachedEntity>.CreateSuccessResponse();
                }
                else
                {
                    userInfo = null;
                    return WcfActionResponse<UserCachedEntity>.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                userInfo = null;
                return WcfActionResponse<UserCachedEntity>.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }
        public WcfActionResponse CheckUser(string username)
        {
            try
            {
                var errorCode = UserDataBo.CheckUser(username);
                if (errorCode == ErrorMapping.ErrorCodes.ValidAccountInvalidUsername)
                {
                    return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                else
                {
                    return WcfActionResponse.CreateSuccessResponse(ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }
        public WcfActionResponse ValidAccountWithMd5EncryptPassword(string username, string password)
        {
            try
            {
                var errorCode = UserDataBo.ValidAccountWithMd5EncryptPassword(username, password);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }

        public WcfActionResponse ValidSmsCode(string username, string smsCode)
        {
            var errorCode = UserDataBo.ValidSmsCode(username, smsCode);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse ResetPassword(string encryptUserId, string newPassword, string accountName)
        {
            var errorCode = UserDataBo.ResetPassword(encryptUserId, newPassword, accountName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse ResetOTP(string encryptUserId, string accountName)
        {
            var errorCode = UserDataBo.ResetOTP(encryptUserId, accountName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse ResetMyPassword(string accountName, string newPassword, string cfNewPassword, string oldPassword)
        {
            var errorCode = UserDataBo.ResetPassword(accountName, newPassword, cfNewPassword, oldPassword);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        //public WcfActionResponse AddNewUser(UserWithPermissionEntity userWithPermission)
        //{
        //    var newUserId = 0;
        //    var encryptUserId = "";
        //    var errorCode = UserDataBo.AddNew(userWithPermission.User, ref newUserId, ref encryptUserId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse();
        //    }
        //    else
        //    {
        //        return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //    }
        //}

        //public WcfActionResponse UpdateUser(UserWithPermissionEntity userWithPermission, string accountName)
        //{
        //    var errorCode = UserDataBo.Update(userWithPermission.User, accountName);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        UserDataBo.GrantListPermission(userWithPermission.User.Id, userWithPermission.UserPermissions);
        //        return WcfActionResponse.CreateSuccessResponse();
        //    }
        //    else
        //    {
        //        return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //    }
        //}

        public WcfActionResponse UpdateUserProfile(UserEntity user, string accountName)
        {
            var errorCode = UserDataBo.UpdateProfile(user, accountName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse AddnewUserProfile(UserEntity user, string accountName, ref int userId)
        {
            var encryptUserId = "";
            var errorCode = UserDataBo.AddNew(user, accountName, ref userId, ref encryptUserId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse UpdateAvatar(string accountName, string avatar)
        {
            var errorCode = UserDataBo.UpdateAvatar(accountName, avatar);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse UpdateTelegramId(string accountName, long telegramId, string otp, string nameSpace)
        {
            //chuyen username=>userid
            //var user = UserDataBo.GetUserByUsername(accountName);

            var userId = Utility.ConvertToInt(accountName);                 
            var user = UserDataBo.GetById(userId);
            if (user == null) {
                user = UserDataBo.GetUserByUsername(accountName);
                if (user == null)
                    return WcfActionResponse.CreateErrorResponse(210586, "Tài khoản này không còn tồi tại.");
            }
            
            var secretKey = GetOtpSecretKey(user.UserName, user.Mobile, nameSpace);

            Logger.WriteLog(Logger.LogType.Trace, "UpdateTelegramId => GetOtpSecretKey: " + user.UserName.ToLower() +"__"+ user.Mobile+ "__" +nameSpace.ToLower());

            if (!TimeBasedOneTimePassword.IsValid(secretKey, otp))
            {
                return WcfActionResponse.CreateErrorResponse(210585, "Link đăng ký tích hợp tài khoản Telegram này không còn hiệu lực. Vui lòng quay lại IMS và thực hiện lại.");
            }

            var errorCode = UserDataBo.UpdateTelegramId(user.UserName, telegramId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //var secretKeyOtp = GetOtpSecretKey(user.UserName, user.Mobile);
                //var createOtp = TimeBasedOneTimePassword.GetPassword(secretKeyOtp);

                return WcfActionResponse.CreateSuccessResponse("Success.");
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse ResetTelegramId(string encryptUserId, long telegramId, string nameSpace)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            var user = BoCached.Base.Account.UserDalFactory.GetUserById(userId);
            if (user == null)
            {
                return WcfActionResponse.CreateErrorResponse(210586, "Tài khoản này không còn tồi tại.");
            }            
            
            var errorCode = UserDataBo.UpdateTelegramId(user.UserName, telegramId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                Logger.WriteLog(Logger.LogType.Trace, "ResetTelegramId => telegramId: " + telegramId + "__" + user.UserName + "__" + user.Mobile + "__" + nameSpace.ToLower());

                return WcfActionResponse.CreateSuccessResponse("Success.");
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public UserEntity GetUserByUserId(string encryptUserId)
        {
            return UserDataBo.GetById(encryptUserId);
        }

        public UserEntity GetUserByUsername(string username)
        {
            return UserDataBo.GetUserByUsername(username);
        }

        public WcfActionResponse UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            var errorCode = UserDataBo.UpdateOtpSecretKeyForUsername(username, otpSecretKey);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse IsUserHasPermissionInZone(string username, EnumPermission permissionId, int zoneId)
        {
            var errorCode = UserDataBo.IsUserHasPermissionInZone(username, (int)permissionId, zoneId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse IsUserInGroupPermission(string username, GroupPermission groupPermissionId)
        {
            var errorCode = UserDataBo.IsUserInGroupPermission(username, (int)groupPermissionId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public List<UserPermissionEntity> GetPermisionByUsernameAndPermissionId(string username, EnumPermission permissionId)
        {
            return UserDataBo.GetListByUsernameAndPermissionId(username, (int)permissionId);
        }

        public List<UserPermissionEntity> GetPermisionByUsernameAndZoneId(string username, int zoneId)
        {
            return UserDataBo.GetPermisionByUsernameAndZoneId(username, zoneId);
        }

        public List<PermissionEntity> GetPermisionByUsername(string username)
        {
            return UserDataBo.GetPermissionByUsername(username);
        }

        public List<UserPermissionEntity> GetPermisionByUsername2(string username)
        {
            return PermissionBo.GetPermissionByUsername2(username);
        }

        public List<UserPermissionEntity> GetPermisionByUserId(string encryptUserId, bool isGetChildZone)
        {
            int userId = CryptonForId.DecryptIdToInt(encryptUserId);
            return PermissionBo.GetListByUserId(userId, isGetChildZone);
        }

        public WcfActionResponse UpdateUserPermissionByUserId(string encryptUserId, List<UserPermissionEntity> userPermission, bool isFullPermission, bool isFullZone, int isRole, bool isSendOver, string accountName)
        {
            try
            {
                var userId = CryptonForId.DecryptIdToInt(encryptUserId);
                var user = UserDataBo.GetById(userId);
                if(user==null)
                    return WcfActionResponse.CreateErrorResponse("User not found.");

                user.IsFullPermission = isFullPermission;
                user.IsFullZone = isFullZone;
                user.IsRole = isRole;
                user.IsSendOver = isSendOver;
                user.ModifiedDate = DateTime.Now;

                var response = UserDataBo.Update(user, accountName);
                if (response == ErrorMapping.ErrorCodes.Success)
                {
                    //if (!isFullPermission || !isFullZone)
                    //{
                        var errorCode = UserDataBo.GrantListPermission(userId, userPermission);
                        if (errorCode == ErrorMapping.ErrorCodes.Success)
                        {
                            //update permission
                            ActivityBo.LogUpdatePermission(accountName, user.UserName);

                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        else
                        {
                            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                        }
                    //}
                    //else
                    //{
                    //    return WcfActionResponse.CreateSuccessResponse();
                    //}
                }
                return WcfActionResponse.CreateErrorResponse((int)response, ErrorMapping.Current[response]);
            }
            catch(Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }

        public WcfActionResponse User_ChangeStatus(string userName, UserStatus status, string accountName)
        {
            var errorCode = UserDataBo.ChangeStatus(userName, status, accountName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        #endregion

        #region PermissionTemplate

        public List<PermissionTemplateEntity> PermissionTemplateGetAll()
        {
            return PermissionBo.PermissionTemplateGetAll();
        }

        //public WcfActionResponse PermissionTemplateUpdate(PermissionTemplateEntity template)
        //{
        //    try
        //    {
        //        var response = PermissionBo.PermissionTemplateUpdate(template);
        //        return (response == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
        //         WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        //        return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }
        //}
        public WcfActionResponse UserPermission_Insert(string encryptUserId, int tempId, bool isFullPermission, bool isFullZone, string accountName)
        {
            try
            {
                var userId = CryptonForId.DecryptIdToInt(encryptUserId);
                var user = UserDataBo.GetById(userId);
                user.IsFullPermission = isFullPermission;
                user.IsFullZone = isFullZone;
                var response = UserDataBo.Update(user, accountName);
                if (response == ErrorMapping.ErrorCodes.Success)
                {
                    if (!isFullPermission || !isFullZone)
                    {
                        var errorCode = UserBo.UserPermission_Insert(userId, tempId);
                        if (errorCode == ErrorMapping.ErrorCodes.Success)
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        else
                        {
                            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                        }
                    }
                    else
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)response, ErrorMapping.Current[response]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public WcfActionResponse PermissionTemplateDelete(int templateId)
        {
            var response = PermissionBo.PermissionTemplateDelete(templateId);
            return (response == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
             WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]));
        }

        public List<PermissionTemplateDetailEntity> GetListPermissionDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            return PermissionBo.GetListPermissionDetailByTemplateId(templateId, isGetChildZone);
        }

        public List<PermissionTemplateDetailEntity> GetPermisstionTemplateDetailById(int templateId)
        {
            return PermissionBo.GetPermisstionTemplateDetailById(templateId);
        }

        public WcfActionResponse PermissionTemplateDetailUpdate(int templateId, List<PermissionTemplateDetailEntity> permissionTemplates)
        {
            var response = PermissionBo.PermissionTemplateDetailUpdate(templateId, permissionTemplates);
            if (response == ErrorMapping.ErrorCodes.Success) return WcfActionResponse.CreateSuccessResponse();
            else return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]);
        }
        public WcfActionResponse RemoveAllPermissionTemplateDetailByPermissionTemplateId(int templateId)
        {
            var response = PermissionBo.RemoveAllPermissionTemplateDetailByPermissionTemplateId(templateId);
            if (response == ErrorMapping.ErrorCodes.Success) return WcfActionResponse.CreateSuccessResponse();
            else return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]);
        }
        public PermissionTemplateWithPermissionDetailEntity GetPermissionTemplateWithPermissionDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            return PermissionBo.GetPermissionTemplateWithPermissionDetailByTemplateId(templateId, isGetChildZone);
        }

        public PermissionTemplateEntity GetPermissionTemplateById(int templateId)
        {
            return PermissionBo.GetPermissionTemplateById(templateId);
        }
        public WcfActionResponse PermissionTemplateUpdate(int templateId, string templateName)
        {
            var response = PermissionBo.PermissionTemplateUpdate(templateId, templateName);
            if (response == ErrorMapping.ErrorCodes.Success) return WcfActionResponse.CreateSuccessResponse();
            else return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]);
        }
        public WcfActionResponse PermissionTemplateInsert(string templateName)
        {
            var id = 0;
            var response = PermissionBo.PermissionTemplateInsert(templateName, ref id);
            if (response == ErrorMapping.ErrorCodes.Success) return WcfActionResponse.CreateSuccessResponse(id.ToString(),"");
            else return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]);
        }
        #endregion

        #region UserBo

        public UserWithPermissionDetailEntity GetUserWithPermissionDetailByUserId(string encryptUserId, bool isGetChildZone)
        {             
            return UserBo.GetUserWithPermissionDetailByUserId(encryptUserId, isGetChildZone);
        }
                
        public List<UserStandardEntity> SearchUser(string keyword, UserStatus status, UserSortExpression sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                UserStatus userStatus;
                if (!Enum.TryParse(status.ToString(), out userStatus))
                {
                    userStatus = UserStatus.Unknow;
                }
                UserSortExpression sortBy;
                if (!Enum.TryParse(sortOrder.ToString(), out sortBy))
                {
                    sortBy = UserSortExpression.CreateDateDesc;
                }
                return UserBo.Search(keyword, userStatus, sortBy, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<UserStandardEntity>();
            }
        }
        public List<UserStandardEntity> SearchUser2(string keyword, UserStatus status, int start, int rows, ref int totalRow)
        {
            try
            {
                return UserBo.Search2(keyword, status, start, rows, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<UserStandardEntity>();
            }
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitESAllUser(string name, DateTime startDate, DateTime endDate, int pageSize,string action=null)
        {
            try
            {
                return UserBo.InitESAllUser(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllUser(string name, DateTime startDate, DateTime endDate, int pageSize,string action=null)
        {
            try
            {
                return UserBo.InitRedisAllUser(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllUserPermission(string id, string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return UserBo.InitRedisAllUserPermission(id, name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<UserStandardEntity> GetListReporterUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(EnumPermission.ArticleReporter, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }
        public List<UserStandardEntity> GetListEditorUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(EnumPermission.ArticleEditor, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }
        public List<UserStandardEntity> GetListSecretaryUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(EnumPermission.ArticleAdmin, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }

        public List<UserStandardEntity> GetUserListByPermissionListAndZoneList(string zoneIds,
                                                                               params EnumPermission[] permissionIds)
        {
            return UserBo.GetUserListByPermissionListAndZoneList(zoneIds, permissionIds);
        }
        public List<UserStandardEntity> GetListUserFullPermisstion()
        {
            return UserBo.GetListUserFullPermisstion();
        }
        public List<UserStandardEntity> GetUserWithFullPermissionAndZoneId(long newsId, string currentUsername)
        {
            return UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, currentUsername);
        }

        public string GetOtpSecretKeyByUsername(string username)
        {
            return UserBo.GetOtpSecretKeyByUsername(username);
        }

        public string GetTelegramByUsername(string username)
        {
            var user = UserBo.GetUserByUsername(username);
            if(user!=null && user.TelegramId > 0)
                return user.TelegramId.ToString();
            return string.Empty;
        }

        public List<UserSearchAutocompleteEntity> User_SearchAutocomplete(int ZoneId, string Keyword, bool isShowTKTS)
        {
            return UserBo.UserSearchAutocomplete(ZoneId, Keyword, isShowTKTS);
        }
        #endregion

        #region UserPenName

        public WcfActionResponse EditPenName(UserPenNameEntity penName)
        {
            var errorCode = UserPenNameBo.EditPenName(penName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse DeletePenNameById(int id)
        {
            var errorCode = UserPenNameBo.DeletePenNameById(id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public List<UserPenNameEntity> SearchPenName(string email, string username, int pageIndex, int pageSize, ref int totalRow)
        {
            return UserPenNameBo.SearchPenName(email, username, pageIndex, pageSize, ref totalRow);
        }
        public UserPenNameEntity GetByVietId(long vietId)
        {
            return UserPenNameBo.GetByVietId(vietId);
        }
        #endregion

        #region OTP

        private string OTP_SECRETKEY = CmsChannelConfiguration.GetAppSetting("OTP_SECRETKEY") ??"feskfhk3jh4k3h4kjsdfshifusdfds";
        //public static Dictionary<string, DateTime> OTPDics = new Dictionary<string, DateTime>();
        public bool IsValidOTP(object otp, string username, out bool isExpired)
        {
            isExpired = false;
            var otpLevel = CmsChannelConfiguration.GetAppSettingInInt32("OTP_LEVEL");
            if (otpLevel > 1)
            {
                try
                {
                    if (otp != null && username != null)
                    {
                        if (!string.IsNullOrEmpty(otp + ""))
                        {
                            var user = GetUserByUsername(username);
                            if (user != null)
                            {
                                var secretKey = GetOtpSecretKey(username, user.Mobile);                                
                                if (TimeBasedOneTimePassword.IsValid(secretKey, otp.ToString()))
                                {
                                    return true;
                                }                                
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }
            }
            else
            {
                return true;
            }
            return false;
        }        
        public WcfActionResponse IsValidOTP(object otp, string username)
        {
            //tam comment de clien tu config
            var otpLevel = CmsChannelConfiguration.GetAppSettingInInt32("OTP_LEVEL");
            if (otpLevel > 1)
            {
                try
                {
                    if (otp != null && username != null)
                    {
                        if (!string.IsNullOrEmpty(otp + ""))
                        {
                            var user = GetUserByUsername(username);
                            if (user != null)
                            {
                                var secretKey = GetOtpSecretKey(username, user.Mobile);
                                
                                if (TimeBasedOneTimePassword.IsValid(secretKey, otp.ToString()))
                                {
                                    //OTPDics.Remove(secretKey);
                                    return WcfActionResponse.CreateSuccessResponse();
                                }                                
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }
            }
            else
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse(12000, "Mã xác nhận không chính xác!");
        }
        public WcfActionResponse IsValidOTPV2(object otp, string username)
        {           
            try
            {
                if (otp != null && username != null)
                {
                    if (!string.IsNullOrEmpty(otp + ""))
                    {
                        var user = GetUserByUsername(username);
                        if (user != null)
                        {
                            var secretKey = GetOtpSecretKey(username, user.Mobile);

                            if (TimeBasedOneTimePassword.IsValid(secretKey, otp.ToString()))
                            {
                                //OTPDics.Remove(secretKey);
                                return WcfActionResponse.CreateSuccessResponse();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(12001, ex.Message);
            }
            
            return WcfActionResponse.CreateErrorResponse(12000, "Mã xác nhận không chính xác!");
        }
        public string GetOtpSecretKey(string username, string mobile)
        {
            var currentChannelNamespace = WcfExtensions.WcfMessageHeader.Current.Namespace?.Trim().ToLower();
            return TimeBasedOneTimePassword.GeneratePrivateKey(string.Format("{0}{1}{2}{3}", OTP_SECRETKEY,currentChannelNamespace,username?.Trim().ToLower(),mobile?.Trim().ToLower()));
        }
        public string GetOtpSecretKey(string username, string mobile, string nameSpace)
        {
            return TimeBasedOneTimePassword.GeneratePrivateKey(string.Format("{0}{1}{2}{3}", OTP_SECRETKEY, nameSpace?.Trim().ToLower(), username?.Trim().ToLower(), mobile?.Trim().ToLower()));
        }

        public string GetOtpAndSendOtp(string username, string nameSpace)
        {
            try {
                var user = GetUserByUsername(username);
                if (user != null && user.TelegramId.HasValue && user.TelegramId.Value > 0)
                {
                    var secretKey = GetOtpSecretKey(username, user.Mobile, nameSpace);
                    var otp = TimeBasedOneTimePassword.GetPassword(secretKey);
                    var key = WcfExtensions.AppConfigs.CmsApiPasscode;
                    return OtpSend(otp, user.TelegramId.Value.ToString(), nameSpace, key);                    
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public WcfActionResponse GetOtpAndSendSMS(string username, string nameSpace)
        {            
            try
            {                                
                var user = GetUserByUsername(username);
                if (user == null) { return WcfActionResponse.CreateErrorResponse("Không tìm thấy thông tin user."); }
                if (user != null && user.TelegramId.HasValue && user.TelegramId.Value>0) { return WcfActionResponse.CreateSuccessResponse("Tài khoản này đã được đăng ký nhận OTP qua telegram."); }
                if (user != null && !string.IsNullOrEmpty(user.Mobile))
                {
                    var secretKey = GetOtpSecretKey(username, user.Mobile, nameSpace);
                    var otp = TimeBasedOneTimePassword.GetPassword(secretKey,60);                    
                    //dk_vccorp|chinhnb|123456|236556
                    var botLink = CmsChannelConfiguration.GetAppSetting("BotTelegramApiUrl");
                    if (string.IsNullOrEmpty(botLink)) botLink = "https://t.me/im2channelvnbot?start={0}";
                     var cuPhap = Base64.Encode(string.Format("dk_{0}|{1}|{2}|{3}", nameSpace?.Trim().ToLower(), username?.Trim().ToLower(), otp?.Trim().ToLower(), otp?.Trim().ToLower()));
                    var linkBot = string.Format(botLink, cuPhap);
                    if(SendSmsMsg(user.Mobile, linkBot, nameSpace))
                    {                        
                        return WcfActionResponse.CreateSuccessResponse("Đã gửi link đăng ký Telegram tới số điện thoại của bạn.");
                    }
                    return WcfActionResponse.CreateErrorResponse("Lỗi trong quá trình gửi link đăng ký tới số điện thoại.");
                }
                return WcfActionResponse.CreateErrorResponse("Tài khoản chưa cập nhật số điện thoại để gửi SMS.");
            }
            catch(Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse(ex.Message);
            }
        }

        public string OtpSend(string otp, string telegramId, string np, string secretKey)
        {
            try
            {
                var urlApiBot = CmsChannelConfiguration.GetAppSetting("AppBotTelegramApiUrl");
                if (string.IsNullOrEmpty(urlApiBot))
                {
                    return "1";
                }

                if (!string.IsNullOrEmpty(otp) && !string.IsNullOrEmpty(telegramId))
                {
                    var action = "otp/send";
                    secretKey = Crypton.Md5Encrypt(secretKey);
                    string url = string.Format(urlApiBot + action + "?secretkey={0}&otp={1}&telegramid={2}&np={3}", secretKey, otp, telegramId, np);

                    Logger.WriteLog(Logger.LogType.Trace, "Begin OtpSend: AppBotTelegramApiUrl => " + url);

                    WebRequest webrequest = WebRequest.Create(url);
                    webrequest.Credentials = CredentialCache.DefaultCredentials;
                    webrequest.Timeout = 10000;
                    WebResponse response = webrequest.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);

                    var result = reader.ReadToEnd();

                    Logger.WriteLog(Logger.LogType.Trace, "End OtpSend: AppBotTelegramApiUrl => " + url);

                    reader.Close();
                    reader.Dispose();
                    response.Close();
                    response.Dispose();

                    return result;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OtpSend => " + ex.Message);
                return string.Empty;
            }
        }
        #endregion

        #region Authenticate Log
        public WcfActionResponse AddnewAuthLog(AuthenticateLogEntity auth, ref long logId)
        {
            var errorCode = AuthenticateLogBo.Insert(auth, ref logId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }
        #endregion

        #region sms
        public static bool SendSmsOTP(string mobile, string password, string nameSpace)
        {
            try
            {
                var ns = CmsChannelConfiguration.GetAppSetting("CMS_NAME_SPACE");
                if (string.IsNullOrEmpty(ns)) ns = nameSpace;                
                string msg1 = "Cảnh giác: Nếu bạn không thao tác vào cms mà xuất hiện tin nhắn mã OTP, vui lòng đổi mật khẩu cms và báo kỹ thuật VCC.";
                string msg = "{2} - {1} OTP: {0}." + msg1;
                var serviceUrl = CmsChannelConfiguration.GetAppSetting("SmsApiUrl");
                if (string.IsNullOrEmpty(serviceUrl)) serviceUrl = "https://api.bipbip.vn/api/send";
                var serviceUsername = CmsChannelConfiguration.GetAppSetting("SmsApiUsername_ForOtp");
                if (string.IsNullOrEmpty(serviceUsername)) serviceUsername = "channelvn";
                var servicePassword = CmsChannelConfiguration.GetAppSetting("SmsApiPassword_ForOtp");
                if (string.IsNullOrEmpty(servicePassword)) servicePassword = "sd943fr97";
                var requestParams =
                    new
                    {
                        username = serviceUsername,
                        password = servicePassword,
                        brandname = "VCCorp",
                        message = string.Format(Utility.UnicodeToKoDau(msg), password, ns, DateTime.Now.Hour + ":" + DateTime.Now.Minute),
                        recipients = new[]
                        {
                            new
                            {
                                message_id = DateTime.Now.ToFileTimeUtc(),
                                number = mobile
                            }
                        }
                    };
                var stringValue = NewtonJson.Serialize(requestParams);
                Logger.WriteLog(Logger.LogType.Trace, "SMS OTP params => " + stringValue);
                var byteValues = Encoding.UTF8.GetBytes(stringValue);
                //Logger.WriteLog(Logger.LogType.Debug, "SEND SMS OTP: " + mobile);
                var request = (HttpWebRequest)WebRequest.Create(serviceUrl);
                request.Method = "POST";
                request.ContentLength = byteValues.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(byteValues, 0, byteValues.Length);
                }
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var result = "";

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = string.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        Logger.WriteLog(Logger.LogType.Error, result);
                        return false;
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    Logger.WriteLog(Logger.LogType.Trace, "SMS OTP result => " + result);
                    stIn.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        public static bool SendVoiceOTP(string mobile, string password, string nameSpace)
        {
            try
            {
                var ns = CmsChannelConfiguration.GetAppSetting("CMS_NAME_SPACE");
                if (string.IsNullOrEmpty(ns)) ns = nameSpace;                
                const string msg =
                    "{0}";
                var serviceUrl = CmsChannelConfiguration.GetAppSetting("SmsApiUrl");
                if (string.IsNullOrEmpty(serviceUrl)) serviceUrl = "https://api.bipbip.vn/api/send";
                var serviceUsername = CmsChannelConfiguration.GetAppSetting("SmsApiUsername_ForOtp");
                if (string.IsNullOrEmpty(serviceUsername)) serviceUsername = "channelvn";
                var servicePassword = CmsChannelConfiguration.GetAppSetting("SmsApiPassword_ForOtp");
                if (string.IsNullOrEmpty(servicePassword)) servicePassword = "sd943fr97";
                var requestParams =
                    new
                    {
                        username = serviceUsername,
                        password = servicePassword,
                        brandname = "VCCorp",
                        kind = "voice",
                        message = string.Format(Utility.UnicodeToKoDau(msg), password, ns, DateTime.Now.Hour + ":" + DateTime.Now.Minute),
                        recipients = new[]
                        {
                            new
                            {
                                message_id = DateTime.Now.ToFileTimeUtc(),
                                number = mobile
                            }
                        }
                    };
                var stringValue = NewtonJson.Serialize(requestParams);
                Logger.WriteLog(Logger.LogType.Trace, "Voice OTP params => " + stringValue);
                var byteValues = Encoding.UTF8.GetBytes(stringValue);
                //Logger.WriteLog(Logger.LogType.Debug, "SEND SMS OTP: " + mobile);
                var request = (HttpWebRequest)WebRequest.Create(serviceUrl);
                request.Method = "POST";
                request.ContentLength = byteValues.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(byteValues, 0, byteValues.Length);
                }
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var result = "";

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = string.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        Logger.WriteLog(Logger.LogType.Error, result);
                        return false;
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    Logger.WriteLog(Logger.LogType.Trace, "SMS OTP result => " + result);
                    stIn.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        public static bool SendSmsMsg(string mobile, string _msg, string nameSpace, bool autoAddPrefix = true)
        {
            try
            {
                var ns = CmsChannelConfiguration.GetAppSetting("CMS_NAME_SPACE");
                if (string.IsNullOrEmpty(ns)) ns = nameSpace;
                var serviceUrl = CmsChannelConfiguration.GetAppSetting("SmsApiUrl");
                if (string.IsNullOrEmpty(serviceUrl)) serviceUrl = "http://api.bipbip.vn/api/send";
                var serviceUsername = CmsChannelConfiguration.GetAppSetting("SmsApiUsername_ForNotify");
                if (string.IsNullOrEmpty(serviceUsername)) serviceUsername = "channelvn";
                var servicePassword = CmsChannelConfiguration.GetAppSetting("SmsApiPassword_ForNotify");
                if (string.IsNullOrEmpty(servicePassword)) servicePassword = "sd943fr97";
                string message = autoAddPrefix ? string.Format("{0} - IMS {1}: {2}", DateTime.Now.Hour + ":" + DateTime.Now.Minute, ns, _msg) : string.Format("{0} - {1}: {2}", DateTime.Now.Hour + ":" + DateTime.Now.Minute, ns, _msg);
                var requestParams =
                    new
                    {
                        username = serviceUsername,
                        password = servicePassword,
                        brandname = "VCCorp",
                        message = Utility.UnicodeToKoDau(message),
                        recipients = new[]
                        {
                            new
                            {
                                message_id = DateTime.Now.ToFileTimeUtc(),
                                number = mobile
                            }
                        }
                    };
                var stringValue = NewtonJson.Serialize(requestParams);
                Logger.WriteLog(Logger.LogType.Trace, "SMS Notify params => " + stringValue);
                var byteValues = Encoding.UTF8.GetBytes(stringValue);
                var request = (HttpWebRequest)WebRequest.Create(serviceUrl);
                request.Method = "POST";
                request.ContentLength = byteValues.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(byteValues, 0, byteValues.Length);
                }
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var result = "";

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = string.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        Logger.WriteLog(Logger.LogType.Error, result);
                        return false;
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    Logger.WriteLog(Logger.LogType.Trace, "SMS Notify result => " + result);
                    stIn.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        public static bool SendMessageOtp(string mobile, string message, string nameSpace, bool autoAddPrefix = true)
        {
            try
            {
                var ns = CmsChannelConfiguration.GetAppSetting("CMS_NAME_SPACE");
                if (string.IsNullOrEmpty(ns)) ns = nameSpace;
                var serviceUrl = CmsChannelConfiguration.GetAppSetting("SmsApiUrl");
                if (string.IsNullOrEmpty(serviceUrl)) serviceUrl = "https://api.bipbip.vn/api/send";
                var serviceUsername = CmsChannelConfiguration.GetAppSetting("SmsApiUsername_ForOtp");
                if (string.IsNullOrEmpty(serviceUsername)) serviceUsername = "channelvn";
                var servicePassword = CmsChannelConfiguration.GetAppSetting("SmsApiPassword_ForOtp");
                if (string.IsNullOrEmpty(servicePassword)) servicePassword = "sd943fr97";

                string smsMessage = autoAddPrefix ? string.Format("{0} - IMS {1}: {2}", DateTime.Now.Hour + ":" + DateTime.Now.Minute, ns, message) : string.Format("{0} - {1}", DateTime.Now.Hour + ":" + DateTime.Now.Minute, message);
                var requestParams =
                    new
                    {
                        username = serviceUsername,
                        password = servicePassword,
                        brandname = "VCCorp",
                        message = Utility.UnicodeToKoDau(smsMessage),
                        recipients = new[]
                        {
                            new
                            {
                                message_id = DateTime.Now.ToFileTimeUtc(),
                                number = mobile
                            }
                        }
                    };
                var stringValue = NewtonJson.Serialize(requestParams);
                Logger.WriteLog(Logger.LogType.Trace, "SMS message OTP params => " + stringValue);
                var byteValues = Encoding.UTF8.GetBytes(stringValue);
                var request = (HttpWebRequest)WebRequest.Create(serviceUrl);
                request.Method = "POST";
                request.ContentLength = byteValues.Length;
                request.ContentType = "application/json; charset=utf-8";
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(byteValues, 0, byteValues.Length);
                }
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var result = "";

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        result = string.Format(
                            "POST failed. Received HTTP {0}",
                            response.StatusCode);
                        Logger.WriteLog(Logger.LogType.Error, result);
                        return false;
                    }
                    var stIn = new StreamReader(response.GetResponseStream());
                    result = stIn.ReadToEnd();
                    Logger.WriteLog(Logger.LogType.Trace, "SMS OTP result => " + result);
                    stIn.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}