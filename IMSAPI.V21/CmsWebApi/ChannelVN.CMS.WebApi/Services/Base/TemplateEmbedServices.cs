﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.TemplateEmbed;
using ChannelVN.CMS.Entity.Base.TemplateEmbed;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class TemplateEmbedServices
    {
        public List<TemplateEmbedEntity> SearchTemplateEmbed(string keyword, int channelId, int typeId, EnumTemplateEmbedStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return TemplateEmbedBo.SearchTemplateEmbed(keyword, channelId, typeId, status, pageIndex, pageSize, ref totalRow);
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}