﻿using ChannelVN.CMS.BO.Base.RollingNews;
using ChannelVN.CMS.BO.Base.Views;
using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class ViewsServices
    {
        #region Views

        public List<NodeJs_ViewsEntity> SortViews(int pageIndex, int pageSize, string newsIds, string sort, int skipSort, long viewFrom, long viewTo, ref int totalRow)
        {
            return ViewsBo.SortViews(pageIndex, pageSize, newsIds, sort, skipSort, viewFrom, viewTo, ref totalRow);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}