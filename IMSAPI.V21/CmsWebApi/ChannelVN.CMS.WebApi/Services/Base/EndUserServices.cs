﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.EndUser;
using System;
using System.Collections.Generic;
using System.Globalization;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.BO.Base.EndUser;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class EndUserServices
    {
        public WcfActionResponse SaveEndUser(EndUserEntity channelEntity)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.SaveEndUser(channelEntity, ref videoChannelId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<EndUserSearchEntity> SearchEndUser(string keyword, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {            
            return EndUserBo.SearchEndUser(keyword, status, fromDate, toDate, pageIndex, pageSize, ref totalRows);
        }

        public EndUserEntity GetEndUserDetail(int id, string currentUsername)
        {
            return EndUserBo.GetEndUserDetail(id, currentUsername);
        }

        #region follow
        public WcfActionResponse UserFollowPlayList(PlayListFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserFollowPlayList(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UserFollowZoneVideo(ZoneVideoFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserFollowZoneVideo(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UserFollowChannel(VideoChannelFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserFollowChannel(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UserFollowLabel(VideoLabelFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserFollowLabel(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UserFollowPublisher(PublisherFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserFollowPublisher(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region unfollow
        public WcfActionResponse UserUnFollowPlayList(PlayListFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserUnFollowPlayList(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UserUnFollowZoneVideo(ZoneVideoFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserUnFollowZoneVideo(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UserUnFollowChannel(VideoChannelFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserUnFollowChannel(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UserUnFollowLabel(VideoLabelFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserUnFollowLabel(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UserUnFollowPublisher(PublisherFollowEntity userFollow)
        {
            try
            {
                var videoChannelId = 0;
                var result = EndUserBo.UserUnFollowPublisher(userFollow);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region Thống kê

        public object StatictisEndUser(string username, DateTime fromDate, DateTime toDate)
        {
            return EndUserBo.StatictisEndUser(username, fromDate, toDate);
        }

        public List<UserStatisticEntity> GetStatisticStatus(int status, DateTime startDate, DateTime endDate)
        {
            return EndUserBo.GetStatisticStatus(status, startDate, endDate);
        }

        public List<PlayListCountEntity> StatictisFollowPlaylist(string username, DateTime fromDate, DateTime toDate)
        {
            return EndUserBo.StatictisFollowPlaylist(username, fromDate, toDate);
        }

        public List<VideoChannelCountEntity> StatictisFollowVideoChannel(string username, DateTime fromDate, DateTime toDate)
        {
            return EndUserBo.StatictisFollowVideoChannel(username, fromDate, toDate);
        }

        #endregion
    }
}