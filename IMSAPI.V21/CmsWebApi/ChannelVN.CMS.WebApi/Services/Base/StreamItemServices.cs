﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BO.Base.StreamItem;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.Base.StreamItem;
using ChannelVN.CMS.WebApi.Services.ExternalServices;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class StreamItemServices
    {
        #region StreamItem      

        public WcfActionResponse InsertStreamItem(StreamItemEntity streamItem, ref long id)
        {
            WcfActionResponse responseData;
            try
            {                
                var errorCode = StreamItemBo.InsertStreamItem(streamItem, ref id);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    streamItem.Id = id;
                    responseData = WcfActionResponse.CreateSuccessResponse(streamItem.Id.ToString(), "Success.");
                    
                    try
                    {                        
                        var strParamValue = NewtonJson.Serialize(streamItem, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + streamItem.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertstreamitem", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                    
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, " InsertStreamItem => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateStreamItem(StreamItemEntity streamItem)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = StreamItemBo.UpdateStreamItem(streamItem);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(streamItem.Id.ToString(),"Success.");

                    try
                    {
                        var strParamValue = NewtonJson.Serialize(streamItem, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + streamItem.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatestreamitem", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, " UpdateStreamItem => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteStreamItemById(long id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = StreamItemBo.DeleteStreamItemById(id);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();

                    try
                    {                        
                        var jsonKey = "{\"Id\":" + id + "}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "deletestreamitem", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, "DeleteStreamItemById => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public List<StreamItemEntity> SearchStreamItem(string keyword, int typeId, int mode, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return StreamItemBo.SearchStreamItem(keyword, typeId, mode, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);
        }

        public long CountStreamItem()
        {
            return StreamItemBo.CountStreamItem();
        }

        public StreamItemEntity GetStreamItemById(long id)
        {
            return StreamItemBo.GetStreamItemById(id);
        }

        public StreamItemDetailEntity GetNewsForStreamItemById(long newsId)
        {            
            return StreamItemBo.GetNewsForStreamItemById(newsId);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByStreamItemAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return StreamItemBo.InitEsByStreamItemAsync(name, startDate, endDate, pageSize, action);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByStreamItemAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return StreamItemBo.InitRedisByStreamItemAsync(name, startDate, endDate, pageSize, action);
        }

        #endregion

        #region StreamItemMobile

        public WcfActionResponse InsertStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            WcfActionResponse responseData;
            try
            {
                long id = 0;
                var errorCode = StreamItemBo.InsertStreamItemMobile(streamItem, ref id);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    streamItem.Id = id;
                    responseData = WcfActionResponse.CreateSuccessResponse(streamItem.Id.ToString(), "Success.");

                    try
                    {
                        var strParamValue = NewtonJson.Serialize(streamItem, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + streamItem.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertstreamitemmobile", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }

                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, " InsertStreamItemMobile => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = StreamItemBo.UpdateStreamItemMobile(streamItem);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(streamItem.Id.ToString(), "Success.");

                    try
                    {
                        var strParamValue = NewtonJson.Serialize(streamItem, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + streamItem.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatestreamitemmobile", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, " UpdateStreamItemMobile => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteStreamItemMobileById(long id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = StreamItemBo.DeleteStreamItemMobileById(id);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();

                    try
                    {
                        var jsonKey = "{\"Id\":" + id + "}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "deletestreamitemmobile", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, "DeleteStreamItemMobileById => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public List<StreamItemMobileEntity> SearchStreamItemMobile(int typeId, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return StreamItemBo.SearchStreamItemMobile(typeId, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);
        }

        public StreamItemMobileEntity GetStreamItemMobileById(long id)
        {
            return StreamItemBo.GetStreamItemMobileById(id);
        }



        #endregion
    }
}