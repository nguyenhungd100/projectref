﻿using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.WebApi.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.BO.Base.Interview;
using ChannelVN.CMS.Common;
using System.Globalization;
using ChannelVN.CMS.BO.Nodejs;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class InterviewServices
    {
        #region Interview

        #region Get
        //public InterviewDetailEntity GetInterviewForEdit(int interviewId)
        //{
        //    return InterviewBo.GetInterviewForEdit(interviewId);
        //}
        //public InterviewEntity GetInterviewByInterviewId(int interviewId)
        //{
        //    return InterviewBo.GetInterviewByInterviewId(interviewId);
        //}
        //public InterviewWithSimpleFieldEntity GetInterviewShortInfoByInterviewId(int interviewId)
        //{
        //    return InterviewBo.GetInterviewShortInfoByInterviewId(interviewId);
        //}
        //public List<InterviewWithSimpleFieldEntity> SearchInterview(string keyword, int isFocus, int isActived, int status, DateTime startDateFrom, DateTime startDateTo, int pageIndex, int pageSize, ref int totalRow)
        //{
        //    return InterviewBo.SearchInterview(keyword, isFocus, isActived, status, startDateFrom,
        //                                        startDateTo, pageIndex, pageSize, ref totalRow);
        //}
        //public List<InterviewForSuggestionEntity> SearchInterviewForSuggestion(int top, string keyword, int isFocus, int isActived)
        //{
        //    return InterviewBo.SearchInterviewForSuggestion(top, keyword, isFocus, isActived);
        //}
        //public InterviewPublishedEntity GetInterviewPublishedByInterviewId(int interviewId)
        //{
        //    return InterviewBo.GetInterviewPublishedByInterviewId(interviewId);
        //}

        #endregion

        #region Update

        //public WcfActionResponse InsertInterview(InterviewEntity interview, ref int interviewId)
        //{
        //    var errorCode = InterviewBo.InsertInterview(interview, ref interviewId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewId.ToString(CultureInfo.CurrentCulture), ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse UpdateInterview(InterviewEntity interview)
        //{
        //    var errorCode = InterviewBo.UpdateInterview(interview);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interview.Id.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse DeleteInterviewByInterviewId(int interviewId)
        //{
        //    var errorCode = InterviewBo.DeleteInterviewByInterviewId(interviewId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse UpdateInterviewFocus(int interviewId, bool isFocus)
        //{
        //    var errorCode = InterviewBo.UpdateInterviewFocus(interviewId, isFocus);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse UpdateInterviewActive(int interviewId, bool isActived)
        //{
        //    var errorCode = InterviewBo.UpdateInterviewActive(interviewId, isActived);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse PublishInterviewContent(int interviewId)
        //{
        //    var errorCode = InterviewBo.PublishInterviewContent(interviewId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse RefreshRollingNewsEvent(int interviewId)
        //{
        //    var errorCode = InterviewBo.UpdateInterviewQuestionIntoRedis(interviewId);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}

        #endregion

        #endregion

        #region Interview channel

        #region Get

        //public InterviewChannelEntity GetInterviewChannelByInterviewChannelId(int interviewChannelId)
        //{
        //    return InterviewBo.GetInterviewChannelByInterviewChannelId(interviewChannelId);
        //}
        //public List<InterviewChannelEntity> GetInterviewChannelDetailByInterviewId(int interviewId, string username)
        //{
        //    return InterviewBo.GetInterviewChannelDetailByInterviewId(interviewId, username);
        //}
        //public List<InterviewChannelEntity> GetInterviewChannelByInterviewIdAndUsername(int interviewId, string username)
        //{
        //    return InterviewBo.GetInterviewChannelByInterviewIdAndUsername(interviewId, username);
        //}
        //public bool CheckChannelRoleForUser(int interviewId, string username, EnumInterviewChannelRole channelRole)
        //{
        //    return InterviewBo.CheckChannelRoleForUser(interviewId, username, channelRole);
        //}
        //public List<InterviewChannelEntity> GetInterviewChannelProcess(int interviewChannelId)
        //{
        //    return InterviewBo.GetInterviewChannelProcess(interviewChannelId);
        //}

        #endregion

        #region Update

        //public WcfActionResponse InsertInterviewChannel(InterviewChannelEntity interviewChannel, ref int interviewChannelId)
        //{
        //    var errorCode = InterviewBo.InsertInterviewChannel(interviewChannel, ref interviewChannelId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewChannelId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse UpdateInterviewChannel(InterviewChannelEntity interviewChannel)
        //{
        //    try
        //    {
        //        var errorCode = InterviewBo.UpdateInterviewChannel(interviewChannel);
        //        if (errorCode == ErrorMapping.ErrorCodes.Success)
        //        {
        //            return WcfActionResponse.CreateSuccessResponse(interviewChannel.Id.ToString(CultureInfo.CurrentCulture),
        //                                                           ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //        }
        //        return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                     ErrorMapping.Current[errorCode]);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        //        return WcfActionResponse.CreateErrorResponse(ex);
        //    }
        //}
        //public WcfActionResponse RemoveInterviewChannel(int interviewChannelId)
        //{
        //    var errorCode = InterviewBo.RemoveInterviewChannel(interviewChannelId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewChannelId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        #endregion

        #endregion

        #region Interview question

        #region Get
        //public InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId)
        //{
        //    return InterviewBo.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
        //}
        //public List<InterviewQuestionWithSimpleFieldEntity> SearchInterviewQuestion(int interviewId, string keyword, int type, string username, EnumInterviewQuestionFieldFilterForUsername fieldFilterForUsername, EnumInterviewQuestionStatus status, bool invertStatus, EnumInterviewQuestionOrder orderBy, int pageIndex, int pageSize, ref int totalRow)
        //{
        //    return InterviewBo.SearchInterviewQuestion(interviewId, keyword, type, username, fieldFilterForUsername, status, invertStatus, orderBy, pageIndex, pageSize,
        //                                                        ref totalRow);
        //}
        //public List<InterviewQuestionEntity> GetAllPublishedQuestionByInterviewId(int interviewId)
        //{
        //    return InterviewBo.GetAllPublishedQuestionByInterviewId(interviewId);
        //}

        #endregion

        #region Working flow
        //public WcfActionResponse ReceivedInterviewQuestion(int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.ReceivedInterviewQuestion(interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse DistributeInterviewQuestion(int interviewQuestionId, int interviewChannelId)
        //{
        //    var errorCode = InterviewBo.DistributeInterviewQuestion(interviewQuestionId, interviewChannelId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse ForwardInterviewQuestion(int interviewQuestionId, int interviewChannelId)
        //{
        //    var errorCode = InterviewBo.ForwardInterviewQuestion(interviewQuestionId, interviewChannelId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse SendAnswerForInterviewQuestion(int interviewQuestionId, string answerContent)
        //{
        //    var errorCode = InterviewBo.SendAnswerForInterviewQuestion(interviewQuestionId, answerContent);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse SendInterviewQuestionWaitForPublish(int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.SendInterviewQuestionWaitForPublish(interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse ReturnInterviewQuestion(int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.ReturnInterviewQuestion(interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse PublishInterviewQuestion(int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.PublishInterviewQuestion(interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse UnpublishInterviewQuestion(int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.UnpublishInterviewQuestion(interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        #endregion

        #region Update

        //public WcfActionResponse InsertInterviewQuestion(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.InsertInterviewQuestion(interviewQuestion, ref interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse InsertExternalInterviewQuestion(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.InsertExternalInterviewQuestion(interviewQuestion, ref interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse InsertExtensionInterviewQuestionData(int interviewId, string data,
        //                                                              ref int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.InsertExtensionInterviewQuestionData(interviewId, data, ref interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse UpdateInterviewQuestion(InterviewQuestionEntity interviewQuestion)
        //{
        //    var errorCode = InterviewBo.UpdateInterviewQuestion(interviewQuestion);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestion.Id.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse UpdateInterviewQuestionPriority(int interviewId, string listOfInterviewQuestionId)
        //{
        //    var errorCode = InterviewBo.UpdateInterviewQuestionPriority(interviewId, listOfInterviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse DeleteInterviewQuestion(int interviewQuestionId)
        //{
        //    var errorCode = InterviewBo.DeleteInterviewQuestion(interviewQuestionId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        #endregion

        #endregion

        #region Interview channel process

        #region Update

        //public WcfActionResponse UpdateInterviewChannelProcess(int interviewChannelId, string listProcessInterviewChannelId)
        //{
        //    var errorCode = InterviewBo.UpdateInterviewChannelProcess(interviewChannelId, listProcessInterviewChannelId);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse(interviewChannelId.ToString(CultureInfo.CurrentCulture),
        //                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        #endregion

        #endregion

        #region InterviewV3

        #region GET
        public NodeJs_InterviewEntity InterviewV3GetById(string Id)
        {
            return InterviewV3Bo.InterviewV3GetById(Id);
        }
        public NodeJs_InterviewEntity InterviewV3GetInterview(string Id)
        {
            return InterviewV3Bo.InterviewV3GetInterview(Id);
        }
        public List<NodeJs_InterviewEntity> SearchInterviewV3(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return InterviewV3Bo.SearchInterviewV3(keyword, pageIndex, pageSize, ref totalRow);
        }
        public List<NodeJs_InterviewEntity2> ListInterviewByIds(string ids)
        {
            return InterviewV3Bo.ListInterviewByIds(ids);
        }
        #endregion

        #region UPDATE
        public WcfActionResponse InterviewV3Insert(NodeJs_Interview interviewV3Entity, ref string interviewId)
        {
            var errorCode = InterviewV3Bo.InterviewV3Insert(interviewV3Entity, ref interviewId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                        ? WcfActionResponse.CreateSuccessResponse()
                        : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewV3Update(NodeJs_Interview interviewV3Entity)
        {
            var errorCode = InterviewV3Bo.InterviewV3Update(interviewV3Entity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                         ? WcfActionResponse.CreateSuccessResponse()
                         : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        //public WcfActionResponse InterviewV3UpdateStatus(int id, int status)
        //{
        //    var errorCode = InterviewV3Bo.InterviewV3UpdateStatus(id, status);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}
        #endregion

        #endregion

        #region InterviewV3Guests
        #region GET
        public NodeJs_InterviewGuestEntity InterviewV3GuestsGetById(string id)
        {
            return InterviewV3Bo.InterviewV3GuestsGetById(id);
        }

        public List<NodeJs_InterviewGuestEntity> InterviewV3GuestsGetByInterviewId(string interviewId, int status)
        {
            return InterviewV3Bo.InterviewV3GuestsGetByInterviewId(interviewId, status);
        }

        //public InterviewV3GuestsEntity InterviewV3GuestsGetInterviewIdAndUserName(int interviewId, string UserName)
        //{
        //    return InterviewV3Bo.InterviewV3GuestsGetInterviewIdAndUserName(interviewId, UserName);
        //}

        //public List<InterviewV3GuestsEntity> InterviewV3GuestsGetByInterviewId(int interviewId, bool status)
        //{
        //    return InterviewV3Bo.InterviewV3GuestsGetByInterviewId(interviewId, status);
        //}
        #endregion

        #region UPDATE
        public WcfActionResponse InterviewV3GuestsInsert(NodeJs_InterviewGuestEntity interviewV3Guests, ref string guestId)
        {                      
            var errorCode = InterviewV3Bo.InterviewV3GuestsInsert(interviewV3Guests, ref guestId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewV3GuestsUpdateInfo(NodeJs_InterviewGuestEntity interviewV3Guests)
        {
            var errorCode = InterviewV3Bo.InterviewV3GuestsUpdateInfo(interviewV3Guests);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        //public WcfActionResponse InterviewV3GuestsUpdateStatus(int guestId, bool status)
        //{
        //    var errorCode = InterviewV3Bo.InterviewV3GuestsUpdateStatus(guestId, status);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        public WcfActionResponse InterviewV3GuestsDelete(string guestId)
        {
            var errorCode = InterviewV3Bo.InterviewV3GuestsDelete(guestId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion
        #endregion

        #region InterviewV3Question
        #region GET
        //public List<InterviewV3QuestionEntity> SearchInterviewV3Question(int interviewId, string keyword, EnumInterviewV3QuestionStatus status, EnumInterviewV3QuestionOrder orderBy, int pageIndex, int pageSize, ref int totalRow)
        //{
        //    return InterviewV3Bo.SearchInterviewV3Question(interviewId, keyword, status, orderBy, pageIndex, pageSize,
        //                                                        ref totalRow);
        //}
        public List<NodeJs_InterviewQuestionEntity> InterviewV3QuestionGetListByInterviewId(string interviewId, EnumNodeJsInterviewQuestionStatus status)
        {
            return InterviewV3Bo.InterviewV3QuestionGetListByInterviewId(interviewId, status);
        }

        //public List<InterviewV3QuestionDisplayListEntity> InterviewV3QuestionSimpleListByInterviewId(int interviewId)
        //{
        //    return InterviewV3Bo.InterviewV3QuestionSimpleListByInterviewId(interviewId);
        //}

        //public List<InterviewV3QuestionEntity> InterviewV3QuestionGetListByStatus(int interviewId, EnumInterviewV3QuestionStatus status, EnumInterviewV3QuestionOrder orderBy)
        //{
        //    return InterviewV3Bo.InterviewV3QuestionGetListByStatus(interviewId, status, orderBy);
        //}

        public NodeJs_InterviewQuestionEntity InterviewV3QuestionGetById(string id)
        {
            return InterviewV3Bo.InterviewV3QuestionGetById(id);
        }

        public WcfActionResponse InsertExtensionInterviewV3QuestionData(int interviewId, string data,
                                                                      ref int interviewQuestionId)
        {
            var errorCode = InterviewV3Bo.InsertExtensionInterviewV3QuestionData(interviewId, data, ref interviewQuestionId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(interviewQuestionId.ToString(CultureInfo.CurrentCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                         ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateInterviewV3QuestionPriority(int interviewId, string listOfInterviewQuestionId)
        {
            var errorCode = InterviewV3Bo.UpdateInterviewV3QuestionPriority(interviewId, listOfInterviewQuestionId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(interviewId.ToString(CultureInfo.CurrentCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                         ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region UPDATE
        //public WcfActionResponse InterviewV3QuestionUpdateQuestionContent(int questionId, string questionContent, string updateBy)
        //{
        //    var errorCode = InterviewV3Bo.InterviewV3QuestionUpdateQuestionContent(questionId, questionContent, updateBy);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        //public WcfActionResponse InterviewV3QuestionUpdateStatus(int questionId, EnumInterviewV3QuestionStatus status)
        //{
        //    var errorCode = InterviewV3Bo.InterviewV3QuestionUpdateStatus(questionId, status);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        //public WcfActionResponse InterviewV3QuestionUpdatePriority(int questionId, int priority)
        //{
        //    var errorCode = InterviewV3Bo.InterviewV3QuestionUpdatePriority(questionId, priority);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        public WcfActionResponse InterviewV3QuestionUpdate(NodeJs_InterviewQuestionEntity interviewV3QuestionEntity)
        {
            var errorCode = InterviewV3Bo.InterviewV3QuestionUpdate(interviewV3QuestionEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            //if (errorCode == ErrorMapping.ErrorCodes.Success)
            //{
            //    return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            //}
            //return WcfActionResponse.CreateErrorResponse((int)errorCode,
            //                                             ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewV3QuestionInsert(NodeJs_InterviewQuestionEntity interviewV3QuestionEntity, ref string questionId)
        {
            var errorCode = InterviewV3Bo.InterviewV3QuestionInsert(interviewV3QuestionEntity, ref questionId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewV3QuestionPublish(string id, string interview_id, string published_by)
        {
            var errorCode = InterviewV3Bo.InterviewQuestionPublish(id, interview_id, published_by);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            //if (errorCode == ErrorMapping.ErrorCodes.Success)
            //{
            //    return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            //}
            //return WcfActionResponse.CreateErrorResponse((int)errorCode,
            //                                             ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewV3QuestionUnPublish(string id, string interview_id, string published_by)
        {
            var errorCode = InterviewV3Bo.InterviewQuestionUnPublish(id, interview_id, published_by);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            //if (errorCode == ErrorMapping.ErrorCodes.Success)
            //{
            //    return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            //}
            //return WcfActionResponse.CreateErrorResponse((int)errorCode,
            //                                             ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewQuestionReturnWaitForAnswer(string id, string interview_id, string published_by)
        {
            var errorCode = InterviewV3Bo.InterviewQuestionReturnWaitForAnswer(id, interview_id, published_by);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            //if (errorCode == ErrorMapping.ErrorCodes.Success)
            //{
            //    return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            //}
            //return WcfActionResponse.CreateErrorResponse((int)errorCode,
            //                                             ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewQuestionUpdatePriority(string id, int Priority, string modified_by)
        {
            var errorCode = InterviewV3Bo.InterviewQuestionUpdatePriority(id, Priority, modified_by);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewV3QuestionReceiveQuestion(int questionId, string processUser, EnumInterviewV3QuestionStatus status)
        {
            var errorCode = InterviewV3Bo.InterviewV3QuestionReceiveQuestion(questionId, processUser, status);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(questionId.ToString(CultureInfo.CurrentCulture), ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        

        public WcfActionResponse InterviewV3QuestionDelete(string answersId)
        {
            var errorCode = InterviewV3Bo.InterviewV3QuestionDelete(answersId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                     ? WcfActionResponse.CreateSuccessResponse()
                     : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion
        #endregion

        #region InterviewV3Answers
        #region GET
        public NodeJs_InterviewAnswerEntity InterviewV3AnswersGetById(string id)
        {
            return InterviewV3Bo.InterviewV3AnswersGetById(id);
        }
        //public List<InterviewV3AnswersEntity> InterviewV3AnswersGetCustomListByQuestionId(int questionId)
        //{
        //    return InterviewV3Bo.InterviewV3AnswersGetListByQuestionId(questionId);
        //}

        public List<NodeJs_InterviewAnswerEntity> InterviewV3AnswersGetListByQuestionId(string questionId)
        {
            return InterviewV3Bo.InterviewV3AnswersGetListByQuestionId(questionId);
        }
        #endregion

        #region UPDATE
        public WcfActionResponse InterviewV3AnswersInsert(NodeJs_InterviewAnswerEntity interviewV3AnswersEntity, ref string answerId)
        {
            var errorCode = InterviewV3Bo.InterviewV3AnswersInsert(interviewV3AnswersEntity, ref answerId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            //if (errorCode == ErrorMapping.ErrorCodes.Success)
            //{
            //    return WcfActionResponse.CreateSuccessResponse(answerId.ToString(CultureInfo.CurrentCulture), ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            //}
            //return WcfActionResponse.CreateErrorResponse((int)errorCode,
            //                                             ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InterviewV3AnswersUpdate(NodeJs_InterviewAnswerEntity interviewV3AnswersEntity)
        {
            var errorCode = InterviewV3Bo.InterviewV3AnswersUpdate(interviewV3AnswersEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        //public WcfActionResponse InterviewV3AnswersUpdateAnswerContent(int answerId, string content, string updateBy)
        //{
        //    var errorCode = InterviewV3Bo.InterviewV3AnswersUpdateAnswerContent(answerId, content, updateBy);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        //public WcfActionResponse InterviewV3AnswersUpdateAnswerStatus(int answerId, bool status)
        //{
        //    var errorCode = InterviewV3Bo.InterviewV3AnswersUpdateAnswerStatus(answerId, status);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
        //    }
        //    return WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                 ErrorMapping.Current[errorCode]);
        //}

        public WcfActionResponse InterviewV3AnswersDelete(string answersId)
        {
            var errorCode = InterviewV3Bo.InterviewV3AnswersDelete(answersId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                     ? WcfActionResponse.CreateSuccessResponse()
                     : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}