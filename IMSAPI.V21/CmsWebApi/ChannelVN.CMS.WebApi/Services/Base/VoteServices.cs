﻿using System;
using System.Globalization;
using System.Linq;
using ChannelVN.CMS.BO.Base.Vote;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.Entity.ErrorCode;
using System.Collections.Generic;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.WebApi.Models;
using ChannelVN.CMS.Common.ChannelConfig;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class VoteServices
    {
        #region Vote

        public WcfActionResponse Insert(VoteEntity voteEntity, int zoneId, string zoneListId)
        {
            var voteId = 0;
            if (VoteBo.Insert(voteEntity, zoneId, zoneListId, ref voteId) == ErrorMapping.ErrorCodes.Success)
            {
                try {
                    if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                    {
                        var listAnswer = new List<VoteAnswersEntity>();
                        var objCd = new VoteFullInfoCdData();
                        voteEntity.Id = voteId;
                        objCd.VoteAnswers = VoteBo.GetListAnswersByVoteId(voteEntity.Id);
                        voteEntity.CreatedDate = DateTime.Now;
                        voteEntity.ModifiedDate = DateTime.Now;
                        objCd.VoteInfo = voteEntity;
                        objCd.ZoneId = zoneId;
                        objCd.ZoneIdList = zoneListId;
                        var jsonKey = "{\"Id\":" + voteEntity.Id + "}";
                        var strPram = NewtonJson.Serialize(objCd, "yyyy-MM-dd'T'HH:mm:ss");
                        ContentDeliveryServices.PushToDataCD(strPram, "insertvote", string.Empty, jsonKey);
                    }
                }
                catch { }

                return WcfActionResponse.CreateSuccessResponse(voteId.ToString(), ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse Update(VoteEntity voteEntity, int zoneId, string zoneListId)
        {
            if (VoteBo.Update(voteEntity, zoneId, zoneListId) == ErrorMapping.ErrorCodes.Success)
            {
                try {
                    if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                    {
                        var listAnswer = new List<VoteAnswersEntity>();
                        var objCd = new VoteFullInfoCdData();
                        objCd.VoteAnswers = VoteBo.GetListAnswersByVoteId(voteEntity.Id);
                        voteEntity.CreatedDate = DateTime.Now;
                        voteEntity.ModifiedDate = DateTime.Now;
                        objCd.VoteInfo = voteEntity;
                        objCd.ZoneId = zoneId;
                        objCd.ZoneIdList = zoneListId;
                        var jsonKey = "{\"Id\":" + voteEntity.Id + "}";
                        var strPram = NewtonJson.Serialize(objCd, "yyyy-MM-dd'T'HH:mm:ss");
                        ContentDeliveryServices.PushToDataCD(strPram, "updatevote", string.Empty, jsonKey);
                    }
                }
                catch { }               

                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse Delete(int id)
        {
            if (VoteBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    ContentDeliveryServices.PushToDataCD("{}", "deletevote", string.Empty, jsonKey);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public VoteDetailEntity GetInfo(int id)
        {
            return VoteBo.GetInfo(id);
        }

        public List<VoteEntity> GetList(string keyword, int zoneId, int pageIndex, int pageSize, bool isGetTotal, ref int totalRow)
        {
            return VoteBo.GetList(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow);
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByVoteAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return VoteBo.InitEsByVoteAsync(name, startDate, endDate, pageSize, action);
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByVoteAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return VoteBo.InitRedisByVoteAsync(name, startDate, endDate, pageSize, action);
        }
        public WcfActionResponse InsertAnswers(VoteAnswersEntity voteAnswersEntity)
        {
            var voteAnswersId = 0;

            if (VoteBo.InsertAnswers(voteAnswersEntity, ref voteAnswersId) == ErrorMapping.ErrorCodes.Success)
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    var jsonKey = "{\"Id\":" + voteAnswersId + "}";
                    var strPram = NewtonJson.Serialize(voteAnswersEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "insertvoteanswers", string.Empty, jsonKey);
                }

                return WcfActionResponse.CreateSuccessResponse(voteAnswersId.ToString(CultureInfo.InvariantCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse InsertListAnswers(VoteAnswersEntity[] lstVoteAnswers)
        {
            var errorCode = new ErrorMapping.ErrorCodes();
            var voteAnswersId = 0;
            foreach (var voteAnswersEntity in lstVoteAnswers)
            {
                errorCode = VoteBo.InsertAnswers(voteAnswersEntity, ref voteAnswersId);
            }
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateAnswers(VoteAnswersEntity voteAnswersEntity)
        {
            var errorCode = VoteBo.UpdateAnswers(voteAnswersEntity);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    var jsonKey = "{\"Id\":" + voteAnswersEntity.Id + "}";
                    var strPram = NewtonJson.Serialize(voteAnswersEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updatevoteanswers", string.Empty, jsonKey);
                }
            }
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdatePriorityAnswers(string listAnswersId)
        {
            var errorCode = VoteBo.UpdatePriorityAnswers(listAnswersId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    var jsonKey = "{\"ListAnswersId\":" + listAnswersId + "}";
                    var strPram = NewtonJson.Serialize(listAnswersId, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updatepriorityvoteanswers", string.Empty, jsonKey);
                }
            }
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateListAnswers(VoteAnswersEntity[] lstVoteAnswers)
        {
            var errorCode = new ErrorMapping.ErrorCodes();
            foreach (var voteAnswersEntity in lstVoteAnswers)
            {
                errorCode = VoteBo.UpdateAnswers(voteAnswersEntity);
            }
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteAnswers(int id)
        {
            var errorCode = VoteBo.DeleteAnswers(id);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var strPram = NewtonJson.Serialize(id, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "deletevoteanswers", string.Empty, jsonKey);
                }
            }
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public List<VoteAnswersEntity> GetListAnswersByVoteId(int voteId)
        {
            var voteAnswers = VoteBo.GetListAnswersByVoteId(voteId);
            try
            {
                var extVoteAnswers = ExternalCms.Bo.VoteBo.GetListAnswersByVoteId(voteId);
                for (var i = 0; i < voteAnswers.Count; i++)
                {
                    var ext = extVoteAnswers.Find(it => it.VoteItemId == voteAnswers[i].Id);
                    if (ext != null)
                    {
                        voteAnswers[i].VoteRate = ext.VoteRate;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }

            return voteAnswers;            
        }

        public VoteAnswersEntity GetAnswersDetail(int voteAnswersId)
        {
            return VoteBo.GetAnswersDetail(voteAnswersId);
        }

        #endregion

        #region VoteYesNo

        #region VoteYesNoGroup

        public List<VoteYesNoGroupEntity> GetAllVoteYesNoGroup()
        {
            return VoteYesNoBo.GetAllVoteYesNoGroup();
        }
        public VoteYesNoGroupEntity GetVoteYesNoGroupByVoteYesNoGroupId(int voteYesNoGroupId)
        {
            return VoteYesNoBo.GetVoteYesNoGroupByVoteYesNoGroupId(voteYesNoGroupId);
        }

        public WcfActionResponse UpdateVoteYesNoGroupPriority(string sortedListIds)
        {
            var errorCode = VoteYesNoBo.UpdateVoteYesNoGroupPriority(sortedListIds);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertVoteYesNoGroup(VoteYesNoGroupEntity voteYesNoGroup)
        {
            var voteYesNoId = 0;
            var errorCode = VoteYesNoBo.InsertVoteYesNoGroup(voteYesNoGroup, ref voteYesNoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(voteYesNoId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateVoteYesNoGroup(VoteYesNoGroupEntity voteYesNoGroup)
        {
            var errorCode = VoteYesNoBo.UpdateVoteYesNoGroup(voteYesNoGroup);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteVoteYesNoGroup(int voteYesNoGroupId)
        {
            var errorCode = VoteYesNoBo.DeleteVoteYesNoGroup(voteYesNoGroupId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region VoteYesNo

        public List<VoteYesNoEntity> SearchVoteYesNo(int voteYesNoGroupId, string keyword, int isFocus, EnumVoteYesNoStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VoteYesNoBo.SearchVoteYesNo(voteYesNoGroupId, keyword, isFocus, status, pageIndex, pageSize, ref totalRow);
        }
        public VoteYesNoEntity GetVoteYesNoByVoteYesNoId(int voteYesNoId)
        {
            return VoteYesNoBo.GetVoteYesNoByVoteYesNoId(voteYesNoId);
        }

        public WcfActionResponse UpdateVoteYesNoPriority(string sortedListIds)
        {
            var errorCode = VoteYesNoBo.UpdateVoteYesNoPriority(sortedListIds);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertVoteYesNo(VoteYesNoEntity voteYesNo)
        {
            int voteYesNoId = 0;
            var errorCode = VoteYesNoBo.InsertVoteYesNo(voteYesNo, ref voteYesNoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(voteYesNoId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateVoteYesNo(VoteYesNoEntity voteYesNo)
        {
            var errorCode = VoteYesNoBo.UpdateVoteYesNo(voteYesNo);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteVoteYesNo(int voteYesNoId)
        {
            var errorCode = VoteYesNoBo.DeleteVoteYesNo(voteYesNoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }


        public WcfActionResponse InsertVoteInNews(int voteId, long newsId)
        {
            if (VoteBo.InsertVoteInNews(voteId, newsId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(voteId.ToString(), ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }
    }
}