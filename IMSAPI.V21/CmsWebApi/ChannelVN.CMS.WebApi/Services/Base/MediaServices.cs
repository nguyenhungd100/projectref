﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.FileUpload;
using ChannelVN.CMS.BO.Base.Photo;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.Base.Video;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.FileUpload;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.Base.PhotoGroup;
using ChannelVN.CMS.BO.Base.PhotoGroup;
using System.Globalization;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.Common.ChannelConfig;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class MediaServices
    {
        #region Videos

        #region Video playlists

        #region Updates        
        public WcfActionResponse SavePlayList(PlaylistEntity playlistEntity, string zoneIdList)
        {
            var playlistId = 0;
            if (PlaylistBo.Save(playlistEntity, zoneIdList, ref playlistId) == ErrorMapping.ErrorCodes.Success)
            {
                //push cd khi update publish và unpublish
                if (playlistId > 0)
                {
                    CmsTrigger.Instance.OnUpdatePlayList(playlistEntity);
                }
                //update playlist -> publish all video status=3 chờ xuất bản
                if(playlistId > 0)
                {
                    var playListDetail = GetVideoPlaylistById(playlistId);
                    var playListInfo = playListDetail.PlaylistInfo;
                    if (playListDetail != null && playListInfo != null && playListInfo.Status == (int)EnumPlayListStatus.Published)
                    {
                        var userDoAction = playlistEntity.CreatedBy;
                        var totalRow = 0;
                        var maxPriority = 0;
                        var listVideos = VideoBo.GetVideoInPlaylist(playlistId, 1, 9999, ref totalRow, ref maxPriority);

                        if (totalRow > 0)
                        {
                            var listVideoPublish = listVideos.Where(n => n.Status == 3).ToList();
                            if (listVideoPublish.Count > 0)
                            {
                                foreach (var item in listVideoPublish)
                                {
                                    var success = VideoDal.Publish(item.Id, userDoAction);
                                    if (success)
                                    {
                                        //redis
                                        BoCached.Base.Video.VideoDalFactory.Publish(item.Id, userDoAction);
                                        //es
                                        BoSearch.Base.Video.VideoDalFactory.ChangeStatus(item.Id, 1, userDoAction);

                                        CmsTrigger.Instance.OnPublishVideo(item.Id);

                                        ActivityBo.LogPublishVideo(item.Id, userDoAction, item.Name);
                                    }
                                }
                            }
                        }
                    }
                }
                return WcfActionResponse.CreateSuccessResponse(playlistId.ToString(CultureInfo.CurrentCulture), null);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse RemoveVideoOutOfPlaylist(int playlistId, string videoIdList)
        {
            if (PlaylistBo.RemoveVideoOutOfPlaylist(playlistId, videoIdList) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(playlistId.ToString(CultureInfo.CurrentCulture),null);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse AddVideoIntoPlayList(int videoId, int playlistId, int priority)
        {
            if (PlaylistBo.AddVideo(videoId, playlistId, priority) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }       

        public WcfActionResponse AddVideoListIntoPlaylist(string addVideoIds, string deleteVideoIds,string waitPublishVideoIds, int playlistId, string playlistName)
        {
            if (PlaylistBo.AddVideoList(addVideoIds, deleteVideoIds, playlistId, playlistName) == ErrorMapping.ErrorCodes.Success)
            {
                if (playlistId>0)
                {
                    //list add gồm cả list wait ->Hoạt tự loại
                    CmsTrigger.Instance.OnUpdateVideoListInPlayList(playlistId, addVideoIds, deleteVideoIds, waitPublishVideoIds);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse DeleteVideoPlaylistById(int playlistId)
        {
            if (PlaylistBo.DeleteById(playlistId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(playlistId.ToString(CultureInfo.CurrentCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse DeleteVideoPlaylistByIdList(string playlistIdList)
        {
            if (PlaylistBo.DeleteByIdList(playlistIdList) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse PublishPlaylist(int playlistId)
        {
            try
            {
                var userDoAction = WcfMessageHeader.Current.ClientUsername;
                var result = PlaylistBo.PublishPlaylist(playlistId, userDoAction);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    //push cd khi publish
                    if (playlistId > 0)
                    {
                        CmsTrigger.Instance.OnPublishPlayList(playlistId);
                    }

                    var totalRow = 0;
                    var maxPriority = 0;
                    var listVideos = VideoBo.GetVideoInPlaylist(playlistId, 1, 9999, ref totalRow, ref maxPriority);

                    if (totalRow > 0)
                    {
                        var listVideoPublish = listVideos.Where(n => n.Status == 3).ToList();
                        if (listVideoPublish.Count > 0)
                        {
                            foreach (var item in listVideoPublish)
                            {
                                var success = VideoDal.Publish(item.Id, userDoAction);
                                if (success)
                                {
                                    //redis
                                    BoCached.Base.Video.VideoDalFactory.Publish(item.Id, userDoAction);
                                    //es
                                    BoSearch.Base.Video.VideoDalFactory.ChangeStatus(item.Id, 1, userDoAction);

                                    CmsTrigger.Instance.OnPublishVideo(item.Id);                                    

                                    ActivityBo.LogPublishVideo(item.Id, userDoAction, item.Name);
                                }
                            }
                        }
                    }

                    return WcfActionResponse.CreateSuccessResponse();
                }
                else if(result== ErrorMapping.ErrorCodes.PlayListNotPublish)
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.PlayListNotPublish,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.PlayListNotPublish]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UnpublishPlaylist(int playlistId)
        {
            try
            {
                if (PlaylistBo.UnpublishPlaylist(playlistId) == ErrorMapping.ErrorCodes.Success)
                {
                    //push cd khi unpublish
                    if (playlistId > 0)
                    {
                        CmsTrigger.Instance.OnUnPublishPlayList(playlistId);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SendPlaylist(int playlistId, string username)
        {
            try
            {
                var result = PlaylistBo.SendPlayList(playlistId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse ReturnPlayList(int playlistId, string username)
        {
            try
            {
                var result = PlaylistBo.ReturnPlayList(playlistId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse MoveTrashPlaylist(int playlistId, string username)
        {
            try
            {
                if (PlaylistBo.MoveTrash(playlistId, username) == ErrorMapping.ErrorCodes.Success)
                {
                    //push cd khi xoa -> publish khong có nut xóa -> ko can băn cd
                    //if (playlistId > 0)
                    //{
                    //    CmsTrigger.Instance.OnMoveTrashPlayList(playlistId);
                    //}
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse ChangeMode(int playlistId, EnumPlayListMode mode)
        {
            try
            {
                if (PlaylistBo.ChangeMode(playlistId, mode) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateVideoPriorotyInPlayList(int playlistId, List<VideoPriorityEntity> listVideoPriority)
        {
            try
            {                
                if (PlaylistBo.UpdateVideoPriorotyInPlayList(playlistId, listVideoPriority) == ErrorMapping.ErrorCodes.Success)
                {
                    if (playlistId > 0)
                    {
                        CmsTrigger.Instance.UpdateVideoPriorotyInPlayList(playlistId, listVideoPriority);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region Gets

        public PlaylistEntity GetPlayListById(int playlistId)
        {
            return PlaylistBo.GetPlayListById(playlistId);
        }

        public PlayListCachedEntity GetVideoPlaylistById(int playlistId)
        {
            return PlaylistBo.GetById(playlistId);
        }

        public PlaylistEntity[] GetListVideoPlaylistByVideoId(int videoId)
        {
            return PlaylistBo.GetListByVideoId(videoId).ToArray();
        }

        public PlaylistEntity[] GetListVideoPlaylistPaging(int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            return PlaylistBo.GetListPaging(pageIndex, pageSize, status, keyword, sortOder, ref totalRow).ToArray();
        }

        public PlaylistEntity[] GetMyPlaylist(string username, int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            return PlaylistBo.GetMyPlaylist(username, pageIndex, pageSize, status, keyword, sortOder, ref totalRow).ToArray();
        }

        public List<PlayListSearchEntity> SearchPlayList(string username, int zoneId, EnumPlayListStatus status, string keyword, EnumPlayListMode mode, EnumPlayListSort sortOder, DateTime createdDateFrom, DateTime createdDateTo, DateTime distributionDateFrom, DateTime distributionDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            return PlaylistBo.SearchPlayList(username, zoneId, status, keyword, mode, sortOder, createdDateFrom, createdDateTo, distributionDateFrom, distributionDateTo, pageIndex, pageSize, ref totalRow);
        }

        public List<CountStatus> CountPlaylist(string userName, string listStatus)
        {
            return PlaylistBo.CountPlaylist(userName, listStatus);
        }

        public List<PlaylistCountFollowEntity> CountVideoFollow(string playListIds)
        {
            return PlaylistBo.CountVideoFollow(playListIds);
        }

        public PlaylistCounterEntity[] GetPlaylistCounter()
        {
            return PlaylistBo.GetPlaylistCounter().ToArray();
        }

        public List<PlaylistWithZoneEntity> GetLastDaysHavePlaylistWithZone(DateTime fromDate, DateTime toDate, int group, int team, int mode)
        {
            return PlaylistBo.GetLastDaysHavePlaylistWithZone(fromDate, toDate, group, team, mode);
        }

        public List<PlaylistWithZoneEntity> GetTopDayHaveVideo(int top)
        {
            return PlaylistBo.GetTopDayHaveVideo(top);
        }

        public List<PlaylistInZoneEntity> GetZoneByPlaylist(int playlistId)
        {
            return PlaylistBo.GetZoneByPlaylist(playlistId);
        }

        #endregion

        #endregion

        #region Video data

        #region Updates

        public WcfActionResponse UpdateConvertedMode(string listVideoId)
        {
            try
            {
                var result = VideoBo.UpdateConvertedMode(listVideoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideo(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.Save(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideoV4(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveV4(videoEntity, tagIds, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    if (videoEntity.Id>0 && videoEntity.Status==(int)EnumVideoStatus.Published)
                    {
                        CmsTrigger.Instance.OnUpdateVideo(videoEntity, tagIds, zoneIdList, playlistIdList, channelIdList);
                    }                    
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SaveVideoV5(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveV5(videoEntity, tagIds, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    if (videoEntity.Id > 0 && videoEntity.Status == (int)EnumVideoStatus.Published)
                    {
                        CmsTrigger.Instance.OnUpdateVideo(videoEntity, tagIds, zoneIdList, playlistIdList, channelIdList);
                    }
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SaveVideoInitDB(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveVideoInitDB(videoEntity, tagIds, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
                
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SaveVideoInfo(VideoEntity videoEntity)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveVideoInfo(videoEntity, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SaveVideoV3(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveV3(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideoName(VideoEntity videoEntity)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveName(videoEntity);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideoNameAndDescription(VideoEntity videoEntity)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveNameAndDescription(videoEntity);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideoV2(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveV2(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateVideoMode(int mode, int id, string username)
        {
            try
            {
                var result = VideoBo.UpdateMode(mode, id, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    var strParamValue = NewtonJson.Serialize(new {Id= id, Mode=mode }, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + id + ",\"Mode\":" + mode + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatemodevideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);

                    return WcfActionResponse.CreateSuccessResponse(id.ToString(CultureInfo.CurrentCulture),null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateStatusVideoByHashId(string hashId, string keyVideo, string username)
        {
            try
            {
                var result = VideoBo.UpdateStatusVideoByHashId(hashId, keyVideo, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(hashId, null);
                }
                
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateVideoAvatar(string avatar, int id)
        {
            try
            {
                var result = VideoBo.UpdateAvatar(avatar, id, WcfMessageHeader.Current.ClientUsername);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(id.ToString(CultureInfo.CurrentCulture),null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateVideoViews(string keyVideo, int views)
        {
            try
            {
                var result = VideoBo.UpdateViews(keyVideo, views);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateListVideoViews(string listKeyVideo, string listViewVideo)
        {
            try
            {
                var result = VideoBo.UpdateListVideoViews(listKeyVideo, listViewVideo);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        
        public WcfActionResponse AddVideoFileToVideo(VideoEntity videoEntity)
        {
            try
            {
                var videoId = 0;
                if (VideoBo.AddVideoFileToVideo(videoEntity, ref videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
       
        public WcfActionResponse UpdateNewsIdForListVideoId(string idList, long newsId)
        {
            try
            {
                if (VideoBo.UpdateNewsIdForList(idList, newsId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse DeleteVideoById(int videoId)
        {
            try
            {
                var result = VideoBo.DeleteById(videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    //Xóa video ko co nut trong xuat ban->tam ko ban cd
                    //if (videoId > 0)
                    //{
                    //    CmsTrigger.Instance.OnDeleteVideo(videoId);
                    //}
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SendVideo(int videoId, string username)
        {
            try
            {
                var result = VideoBo.SendVideo(videoId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse ReturnVideo(int videoId, string username)
        {
            try
            {
                var result = VideoBo.ReturnVideo(videoId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse PublishVideo(int videoId, string username)
        {
            try
            {
                var result = VideoBo.PublishVideo(videoId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    CmsTrigger.Instance.OnPublishVideo(videoId);

                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse PublishVideoInNews(int videoId, DateTime distributionDate)
        {
            try
            {
                var result = VideoBo.PublishVideoInNews(videoId, WcfMessageHeader.Current.ClientUsername, distributionDate);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    CmsTrigger.Instance.OnPublishVideo(videoId);

                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UnpublishVideo(int videoId, string username)
        {
            try
            {
                var result = VideoBo.UnpublishVideo(videoId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    if (videoId > 0)
                    {
                        CmsTrigger.Instance.OnUnpublishVideo(videoId);
                    }                    

                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse DeleteVideoByIdList(string videoIdList)
        {
            try
            {
                if (VideoBo.DeleteByIdList(videoIdList) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse ChangeVideoStatus(int videoId)
        {
            try
            {
                if (VideoBo.ChangeStatus(videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse ChangeVideoStatusByList(string videoIdList)
        {
            try
            {
                if (VideoBo.ChangeStatusByList(videoIdList) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SyncNewsAndVideo(string videoIdList, long newsId)
        {
            try
            {
                if (VideoBo.SyncNewsAndVideo(videoIdList, newsId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SyncNewsAndVideoWithStatus(string videoIdList, long newsId, EnumVideoStatus status, int excludeStatus)
        {
            try
            {
                if (VideoBo.SyncNewsAndVideoWithStatus(videoIdList, newsId, status, excludeStatus) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        /// <summary>
        /// Tướng Cương: Update KeyVideo By FileName VTVCD24
        /// </summary>
        /// <param name="keyVideo">khi bên transcode callback về thì update vào Keyvideo khi đấy mới được dùng</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateKeyVideoByFileName(string keyVideo, string fileName)
        {
            try
            {
                var result = VideoBo.UpdateKeyVideoByFileName(keyVideo, fileName);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(keyVideo.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region Gets

        public List<VideoKeyEntity> GetListVideoWaitConvert(int top)
        {
            return VideoBo.GetListVideoWaitConvert(top);
        }

        public VideoEntity GetDetailVideo(int videoId)
        {
            var zoneName = "";
            return VideoBo.GetById(videoId, ref zoneName);
        }
        public List<CountStatus> CountVideo(string currentUsername, string status)
        {
            return VideoBo.CountVideo(currentUsername, status);
        }
        public List<CountStatus> CountVideoSingle(string currentUsername, string status)
        {
            return VideoBo.CountVideoSingle(currentUsername, status);
        }
        public VideoEntity GetDetailVideoByVideoExternalId(string videoExternalId)
        {
            var zoneName = "";
            return VideoBo.GetByVideoExternalId(videoExternalId, ref zoneName);
        }
        public VideoEntity GetDetailVideoV2(int videoId)
        {
            var zoneName = "";
            return VideoBo.GetByIdV2(videoId, ref zoneName);
        }

        public VideoEntity GetVideoByFileName(string fileName)
        {
            return VideoBo.GetByFileName(fileName);
        }
        public VideoEntity GetDetailVideoByKeyvideo(string keyvideo)
        {
            var zoneName = "";
            return VideoBo.GetByKeyvideo(keyvideo, ref zoneName);
        }
        public VideoDetailForEdit GetDetailVideoForEdit(int videoId)
        {
            try
            {
                var zoneName = string.Empty;
                var objectForSerialize = new VideoDetailForEdit();
                var videoRelationList = new List<VideoEntity>();

                var videoEntity = VideoBo.GetById(videoId, ref zoneName);
                // Add VideoEntity Serialized
                objectForSerialize.VideoInfo = videoEntity;
                if (videoEntity != null) {
                    objectForSerialize.VideoRelation = VideoBo.GetListRelationByIdList(videoEntity.VideoRelation);
                }
                objectForSerialize.VideoByAuthor = VideoBo.GetListAuthorInVideo(videoId);

                var zoneRelateList = ZoneVideoBo.GetListZoneRelation(videoId);
                var tagList = VideoBo.GetVideoTagListByVideo(videoId);
                var playlistList = PlaylistBo.GetListByVideoId(videoId);
                var videoChannelList = VideoChannelBo.GetListVideoChannelByVideoId(videoId);                
                objectForSerialize.TagList = tagList;
                objectForSerialize.ZoneVideoList = zoneRelateList;
                objectForSerialize.Playlist = playlistList;
                objectForSerialize.VideoChannelList = videoChannelList;
                if (BoConstants.VideoShareLink.IndexOf("{2}") > 0)
                {
                    objectForSerialize.ShareLink = "";
                }
                else
                {
                    objectForSerialize.ShareLink = string.Format(BoConstants.VideoShareLink, Utility.UnicodeToKoDauAndGach(videoEntity.Name), videoId);
                }
                    
                try
                {
                    if (WcfMessageHeader.Current.Namespace == "VTV")
                    {
                        objectForSerialize.VideoExtension = VideoBo.GetVideoExtensionByVideoId(videoId);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                return objectForSerialize;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return null;
        }
        public VideoDetailForEdit GetDetailVideoForEditV2(int videoId)
        {
            try
            {
                var zoneName = string.Empty;
                var objectForSerialize = new VideoDetailForEdit();
                var videoRelationList = new List<VideoEntity>();

                var videoEntity = VideoBo.GetByIdV2(videoId, ref zoneName);
                // Add VideoEntity Serialized
                objectForSerialize.VideoInfo = videoEntity;
                if (videoEntity != null)
                    objectForSerialize.VideoRelation = VideoBo.GetListRelationByIdList(videoEntity.VideoRelation);

                var zoneRelateList = ZoneVideoBo.GetListZoneRelation(videoId);
                var tagList = VideoBo.GetVideoTagListByVideo(videoId);
                var playlistList = PlaylistBo.GetListByVideoId(videoId);                
                objectForSerialize.TagList = tagList;
                objectForSerialize.ZoneVideoList = zoneRelateList;
                objectForSerialize.Playlist = playlistList;
                objectForSerialize.ShareLink = BoConstants.VideoShareLink;
                try
                {
                    if (WcfMessageHeader.Current.Namespace == "VTV")
                    {
                        objectForSerialize.VideoExtension = VideoBo.GetVideoExtensionByVideoId(videoId);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                return objectForSerialize;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return null;
        }

        public VideoEntity[] GetListMyVideo(string username, int pageIndex, int pageSize, string keyword, int status, int sortOrder, int cateId, int playlistId, ref int totalRow)
        {
            return VideoBo.GetListMyVideo(username, pageIndex, pageSize, keyword, status, sortOrder, cateId, playlistId,
                                               ref totalRow).ToArray();
        }

        public List<VideoEntity> GetListVideoByIdList(string videoIdList)
        {
            return VideoBo.GetListByIdList(videoIdList);
        }

        public List<VideoEntity> GetInitVideoByListVideoId(string videoIdList)
        {
            return VideoBo.GetInitVideoByListVideoId(videoIdList);
        }

        public List<PlaylistEntity> InitEsRedisByListPlayListId(string playListIdList)
        {
            return PlaylistBo.InitEsRedisByListPlayListId(playListIdList);
        }

        public VideoEntity[] GetListVideoByCate(int pageIndex, int pageSize, int zoneId, int playlistId, int status, string keyword, int sortOrder, ref int totalRow)
        {
            return
                VideoBo.GetListPagingByZone(pageIndex, pageSize, zoneId, playlistId, status, keyword, sortOrder,
                                            ref totalRow).ToArray();
        }

        public VideoEntity[] GetListVideoInPlaylist(int pageIndex, int pageSize, int playlistId, int status, string keyword, int sortOder, ref int totalRow)
        {
            return VideoBo.GetListPagingInPlaylist(pageIndex, pageSize, playlistId, status, keyword, sortOder, ref totalRow).ToArray();
        }

        public VideoEntity[] GetListAllVideoInPlaylist(int playlistId)
        {
            return VideoBo.GetAllVideoInPlaylist(playlistId).ToArray();
        }

        public VideoEntity[] GetListAllVideoInPlaylistByMode(int playlistId, int mode)
        {
            return VideoBo.GetAllVideoInPlaylistByMode(playlistId, mode).ToArray();
        }

        public List<VideoEntity> GetVideoInPlaylist(int playlistId, int pageIndex, int pageSize, ref int totalRow, ref int maxPriority)
        {
            return VideoBo.GetVideoInPlaylist(playlistId, pageIndex, pageSize, ref totalRow, ref maxPriority);
        }

        public VideoEntity GetVideoNewByPlayListId(int playlistId)
        {
            return VideoBo.GetVideoNewByPlayListId(playlistId);
        }

        public List<VideoSearchEntity> ListVideoByStatus(string username, EnumVideoStatus status, EnumVideoSortOrder order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.ListVideoByStatus(username, (int)status, (int)order, mode, pageIndex, pageSize, ref totalRow);
        }

        public List<VideoSearchEntity> ListVideoByStatusSingle(string username, EnumVideoStatus status, EnumVideoSortOrder order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.ListVideoByStatusSingle(username, (int)status, (int)order, mode, pageIndex, pageSize, ref totalRow);
        }

        public List<VideoSearchEntity> ListMyVideoSingle(string username, int zoneVideoId, string keyword, DateTime fromDate, DateTime toDate, EnumVideoSortOrder order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.ListMyVideoSingle(username, zoneVideoId, keyword, fromDate, toDate, (int)order, mode, pageIndex, pageSize, ref totalRow);
        }

        public List<VideoSearchEntity> ListVideoByParentId(int parentId, EnumVideoStatus status, ref int totalRow)
        {
            return VideoBo.ListVideoByParentId(parentId, (int)status, ref totalRow);
        }

        public List<VideoSearchEntity> SearchVideo(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.Search(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow);
        }

        public List<VideoSearchEntity> ExportVideoPublish(string zoneIds, string createdBy, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.ExportVideoPublish(zoneIds, createdBy, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public List<VideoSearchEntity> SearchVideoSingle(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchVideoSingle(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow);
        }


        public List<Video> SearchVideoVTV(int zoneVideoId, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.SearchVTV(zoneVideoId, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public List<VideoSearchEntity> SearchVideoKeyByZone(DateTime fromDate, DateTime toDate, string username, List<string> zoneVideoId, int playlistId, string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.SearchVideoKeyByZone(fromDate, toDate, username, zoneVideoId, playlistId, keyword, (int)status, pageIndex, pageSize, ref totalRow);
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByVideoAsync(string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return VideoBo.InitEsByVideoAsync(name, startPage, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByVideoAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return VideoBo.InitRedisByVideoAsync(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public VideoEntity[] SearchVideoForList(string username, int zoneVideoId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchForList(username, zoneVideoId, keyword, (int)status, (int)order, fromDate, toDate,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public List<VideoDistributionEntity> SearchDistribution(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchDistribution(keyword, viewDate, mode, zoneIds, pageIndex, pageSize, ref totalRow);
        }
        public List<VideoDistributionEntity> SearchDistributionV2(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchDistributionV2(keyword, viewDate, mode, zoneIds, pageIndex, pageSize, ref totalRow);
        }
        public VideoEntity[] SearchVideoV2(string username, int videoFolderId, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchV2(username, videoFolderId, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoEntity[] SearchVideoAd(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, bool allowAd, ref int totalRow)
        {
            return
                VideoBo.SearchAd(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, allowAd, ref totalRow).ToArray();
        }
        public VideoEntity[] SearchVideoExcludeBomb(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchExcludeBomb(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoExportEntity[] SearchExportVideo(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchExport(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoEntity[] SearchVideoWithExcludeZones(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, string excludeZones, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchWithExcludeZones(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode, excludeZones,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }

        public VideoInNewsEntity[] GetVideoInNewsList(int videoId)
        {
            return VideoBo.GetVideoInNewsList(videoId).ToArray();
        }

        public VideoInfoForSync GetVideoInfoForSynchonize(int videoId)
        {
            var zoneName = string.Empty;
            var video = VideoBo.GetById(videoId, ref zoneName);
            if (null == video)
            {
                return null;
            }
            var tags = string.Empty;
            var zoneIds = string.Empty;

            #region Gets TagList
            var tagListInVideo = new List<VideoTagEntity>();
            try
            {
                tagListInVideo = VideoBo.GetVideoTagListByVideo(video.Id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            #endregion

            #region Gets ZoneList
            try
            {
                var zoneList = ZoneVideoBo.GetListZoneRelation(video.Id);
                if (zoneList.Count > 0)
                {
                    zoneIds = zoneList.Aggregate(tags, (current, zone) => current + (", " + zone.Id));
                    if (zoneIds != "") zoneIds = zoneIds.Remove(0, 2);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            #endregion

            return new VideoInfoForSync
            {
                Video = video,
                TagList = tagListInVideo,
                ZoneIds = zoneIds
            };
        }

        public VideoCounterEntity[] GetVideoCounter()
        {
            return VideoBo.GetVideoCounter().ToArray();
        }

        public string[] GetLastPublishedForUpdateView(DateTime fromDate)
        {
            return VideoBo.GetLastPublishedForUpdateView(fromDate).ToArray();
        }

        public VideoExportEntity[] GetRoyalties(DateTime startDate, DateTime endDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = VideoBo.GetRoyalties(startDate, endDate, pageIndex, pageSize);
                totalRow = VideoBo.CountRoyalties(startDate, endDate);
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new VideoExportEntity[] { };
        }

        public VideoExportEntity[] GetRoyaltiesByZone(DateTime startDate, DateTime endDate, int pageIndex, int pageSize, ref int totalRow, string zones)
        {
            try
            {
                var data = VideoBo.GetRoyaltiesByZone(startDate, endDate, pageIndex, pageSize, zones);
                totalRow = VideoBo.CountRoyaltiesByZone(startDate, endDate, zones);
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new VideoExportEntity[] { };
        }

        public List<VideoSharingEntity> GetListSharing(string keyword, string zoneIds, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.GetListSharing(keyword, zoneIds, dateFrom, dateTo, pageIndex, pageSize, ref totalRow);
        }
        public VideoEntity[] GetListVideoByHour(string zoneVideoIds, int Hour)
        {
            return VideoBo.GetListVideoByHour(zoneVideoIds, Hour).ToArray();
        }

        #endregion

        #region ZoneVideo

        public List<ZoneVideoEntity> GetVideoZoneListByParentId(int parentId, EnumZoneVideoStatus status)
        {
            return ZoneVideoBo.GetListByParentId(parentId, (int)status);
        }

        public List<ZoneVideoEntity> GetPrivateZoneVideo(string username, int parentZoneId, int status)
        {
            return ZoneVideoBo.GetPrivateZoneVideo(username, parentZoneId, status);
        }

        public ZoneVideoEntity[] GetVideoZoneListAsTreeview(int parentId, EnumZoneVideoStatus status, string prefix)
        {
            var zoneList = ZoneVideoBo.GetListByParentId(parentId, (int)status);
            return ZoneVideoBo.BindAllOfZoneVideoToTreeviewFullDepth(zoneList, prefix).ToArray();
        }

        public PlaylistGroupEntity[] GetPlaylistGroupAsTreeview(int parentId, string prefix)
        {
            var zoneList = VideoBo.GetPlaylistGroupByParentId(parentId);
            return VideoBo.BindAllOfPlaylistGroupToTreeviewFullDepth(zoneList, prefix).ToArray();
        }

        public ZoneVideoEntity[] GetVideoZoneListRelation(int videoId)
        {
            return ZoneVideoBo.GetListZoneRelation(videoId).ToArray();
        }

        public ZoneVideoEntity GetVideoZoneByVideoZoneId(int videoZoneId)
        {
            return ZoneVideoBo.GetZoneVideoByZoneVideoId(videoZoneId);
        }

        public ZoneVideoDetailEntity GetVideoZoneDetailByVideoZoneId(int videoZoneId)
        {
            return ZoneVideoBo.GetZoneVideoDetailByZoneVideoId(videoZoneId);
        }

        public WcfActionResponse InsertVideoZone(ZoneVideoEntity zoneVideo, string listVideoTagId, ref int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.InsertVideoZone(zoneVideo, listVideoTagId, ref zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    zoneVideo.Id = zoneVideoId;
                    if (zoneVideo.Id > 0 && zoneVideo.Status==(int)EnumZoneVideoStatus.Actived)
                    {
                        CmsTrigger.Instance.OnInsertZoneVideo(zoneVideo);
                    }
                    return WcfActionResponse.CreateSuccessResponse(zoneVideoId.ToString(CultureInfo.CurrentCulture));
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse InsertVideoZoneInitDB(ZoneVideoEntity zoneVideo, string listVideoTagId)
        {
            try
            {
                if (ZoneVideoBo.InsertVideoZoneInitDB(zoneVideo, listVideoTagId) == ErrorMapping.ErrorCodes.Success)
                {                    
                    return WcfActionResponse.CreateSuccessResponse(zoneVideo.Id.ToString(CultureInfo.CurrentCulture));
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateVideoZone(ZoneVideoEntity zoneVideo, string listVideoTagId)
        {
            try
            {
                if (ZoneVideoBo.UpdateVideoZone(zoneVideo, listVideoTagId) == ErrorMapping.ErrorCodes.Success)
                {
                    if (zoneVideo.Id > 0)
                    {
                        CmsTrigger.Instance.OnUpdateZoneVideo(zoneVideo);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse MoveVideoZoneUp(int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.MoveVideoZoneUp(zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    if (zoneVideoId > 0)
                    {
                        CmsTrigger.Instance.OnMoveUpZoneVideo(zoneVideoId);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse MoveVideoZoneDown(int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.MoveVideoZoneDown(zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    if (zoneVideoId > 0)
                    {
                        CmsTrigger.Instance.OnMoveDownZoneVideo(zoneVideoId);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoZone(int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.DeleteVideoZone(zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    if (zoneVideoId > 0)
                    {
                        CmsTrigger.Instance.OnDeleteZoneVideo(zoneVideoId);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateAvatarZoneVideo(string zoneVideoAvatars)
        {
            try
            {
                if (ZoneVideoBo.UpdateAvatarZoneVideo(zoneVideoAvatars) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse GetZoneVideoIdByNewsZoneId(string newsZoneId, ref int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.GetZoneVideoIdByNewsZoneId(newsZoneId, ref zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(zoneVideoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllZoneVideo(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return ZoneVideoBo.InitRedisAllZoneVideo(name, startDate, endDate, pageSize, action);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllPlayList(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return PlaylistBo.InitRedisAllPlayList(name, startDate, endDate, pageSize, action);
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitEsAllPlayList(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return PlaylistBo.InitEsAllPlayList(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region VideoLabel       

        public WcfActionResponse InsertVideoLabel(VideoLabelEntity videoLabel, ref int videoLabelId)
        {
            try
            {
                if (VideoLabelBo.InsertVideoLabel(videoLabel, ref videoLabelId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoLabelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateVideoLabel(VideoLabelEntity videoLabel)
        {
            try
            {
                if (VideoLabelBo.UpdateVideoLabel(videoLabel) == ErrorMapping.ErrorCodes.Success)
                {
                    if(videoLabel.Id > 0)
                    {
                        CmsTrigger.Instance.OnUpdateVideoLabel(videoLabel);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoLabel(int videoLabelId)
        {
            try
            {
                if (VideoLabelBo.DeleteVideoLabel(videoLabelId) == ErrorMapping.ErrorCodes.Success)
                {
                    if (videoLabelId > 0)
                    {
                        CmsTrigger.Instance.OnDeleteVideoLabel(videoLabelId);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public VideoLabelEntity GetVideoLabelById(int labelId)
        {
            return VideoLabelBo.GetVideoLabelById(labelId);
        }

        public List<VideoLabelEntity> GetVideoLabelListByParentId(int parentId, int isSystem)
        {
            return VideoLabelBo.GetListByParentId(parentId, isSystem);
        }

        public WcfActionResponse MoveVideoLabelUp(int zoneVideoId)
        {
            try
            {
                if (VideoLabelBo.MoveVideoLabelUp(zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse MoveVideoLabelDown(int zoneVideoId)
        {
            try
            {
                if (VideoLabelBo.MoveVideoLabelDown(zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region VideoFromYoutube

        public WcfActionResponse InsertVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id)
        {
            try
            {
                var statusUpdate = VideoBo.InsertVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList, ref id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate, ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }

        public WcfActionResponse UpdateVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList)
        {
            try
            {
                var statusUpdate = VideoBo.UpdateVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate, ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }

        public WcfActionResponse SaveVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id)
        {
            try
            {
                var statusUpdate = VideoBo.SaveVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList, ref id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate, ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }

        public WcfActionResponse UpdateVideoFromYoutubeStatus(int id, string avatar, string filePath, EnumVideoFromYoutubeStatus status, string htmlCode, string keyVideo, string pname, string fileName, string duration, string size, int capacity)
        {
            try
            {
                var statusUpdate = VideoBo.UpdateVideoFromYoutubeStatus(id, avatar, filePath, status, htmlCode, keyVideo, pname,
                                                                        fileName, duration, size, capacity);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate, ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("MediaServices.UpdateVideoFromYoutubeStatus() => {0}", ex));
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoFromYoutube(int id)
        {
            try
            {
                var statusUpdate = VideoBo.DeleteVideoFromYoutube(id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                                 ErrorMapping.Current[
                                                                     ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SendVideoFromYoutube(int id)
        {
            try
            {
                var statusUpdate = VideoBo.SendVideoFromYoutube(id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                                 ErrorMapping.Current[
                                                                     ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse PublishVideoFromYoutube(int id)
        {
            try
            {
                var statusUpdate = VideoBo.PublishVideoFromYoutube(id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate,
                                                                 ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public VideoFromYoutubeEntity GetVideoFromYoutubeById(int id)
        {
            try
            {
                return VideoBo.GetVideoFromYoutubeById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public VideoFromYoutubeForEditEntity GetVideoFromYoutubeForEdit(int id)
        {
            try
            {
                return VideoBo.GetVideoFromYoutubeForEdit(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public VideoFromYoutubeEntity[] GetListVideoFromYoutube(string accountName, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.GetListVideoFromYoutube(accountName, status, order, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public List<VideoFromYoutubeDetailEntity> SearchVideoFromYoutube(string username, string keyword, EnumVideoFromYoutubeStatus status, int zoneVideoId, int playlistId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.SearchVideoFromYoutube(username, keyword, status, zoneVideoId, playlistId, fromDate,
                                                  toDate, pageIndex, pageSize, ref totalRow);
        }

        public List<VideoFromYoutubeEntity> GetUploadingVideoFromYoutube()
        {
            try
            {
                return VideoBo.GetUploadingVideoFromYoutube();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("MediaServices.GetUploadingVideoFromYoutube() => {0}", ex));
                return new List<VideoFromYoutubeEntity>();
            }
        }

        #endregion

        #region BoxVideoEmbed
        public List<BoxVideoEmbedEntity> GetListVideoEmbed(int zoneId, int type)
        {
            return BoxVideoEmbedBo.GetListVideoEmbed(zoneId, type);
        }

        public WcfActionResponse InsertVideoEmbed(BoxVideoEmbedEntity videoEmbedBox)
        {

            WcfActionResponse responseData;

            var errorCode = BoxVideoEmbedBo.Insert(videoEmbedBox);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;

        }

        public WcfActionResponse UpdateVideoEmbed(string listVideoId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedBo.Update(listVideoId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (responseData.Success)
            {
                try
                {
                    if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                    {
                        var currentNewsPosition = new EmbedVideoEsData();
                        var jsonKey = "{\"ZoneId\":" + zoneId + ", \"Type\":" + type + "}";
                        currentNewsPosition.ListVideoId = listVideoId.Split(',').ToArray();
                        currentNewsPosition.TypeVideoEmbed = type;
                        currentNewsPosition.ZoneId = zoneId;
                        var jsonValue = NewtonJson.Serialize(currentNewsPosition);
                        ContentDeliveryServices.PushToDataCD(jsonValue, "updateboxvideosembed", string.Empty, jsonKey);
                        Logger.WriteLog(Logger.LogType.Debug, jsonValue);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            return responseData;
        }
        public WcfActionResponse UpdateVideoEmbed_Epl(List<BoxVideoEmbedEplEntity> listEmbed)
        {

            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedBo.UpdateEpl(listEmbed);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteVideoEmbed(long videoId, int zoneId, int type)
        {

            WcfActionResponse responseData;

            var newEncryptVideoId = String.Empty;
            var errorCode = BoxVideoEmbedBo.Delete(videoId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;

        }
        #endregion

        #region BoxPlayEmbed
        public List<BoxPlayEmbedEntity> GetListPlayEmbed(int zoneId, int type)
        {
            return BoxVideoEmbedBo.GetListPlayEmbed(zoneId, type);
        }

        public WcfActionResponse UpdatePlayEmbed(string listObject, int zoneId, int type, string name, int displayStyle, int priority)
        {

            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedBo.UpdatePlay(listObject, zoneId, type, name, displayStyle, priority);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                int typeId = (int)VideoPositionType.VideoBoxMuc;
                CmsTrigger.Instance.OnUpdateVideoPlayEmbed(typeId, listObject);
            }
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        #endregion

        #region BoxHighLightEmbed
        public List<BoxVideoHighlightEmbedEntity> GetVideoHighlightEmbed()
        {
            return BoxVideoEmbedBo.GetVideoHighLightEmbed();
        }

        public WcfActionResponse UpdateVideoHighlightEmbed(string listObject)
        {
            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedBo.UpdateVideoHighlight(listObject);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                int typeId = (int)VideoPositionType.VideoHighLight;
                CmsTrigger.Instance.OnUpdateVideoHighlightEmbed(typeId, listObject);
            }
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        #endregion

        #region BoxProgramHotHomeEmbed
        public List<BoxProgramHotHomeEmbedEntity> GetProgramHotHomeEmbed(int zoneId)
        {
            return BoxVideoEmbedBo.GetProgramHotHomeEmbed(zoneId);
        }

        public WcfActionResponse UpdateProgramHotHomeEmbed(string name, int zoneId, string listObject)
        {
            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedBo.UpdateProgramHotHome(name, zoneId, listObject);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                int typeId = (int)VideoPositionType.VideoProgramHotHome;
                if (zoneId > 0)
                {
                    typeId = (int)VideoPositionType.VideoProgramHotMuc;
                }
                CmsTrigger.Instance.OnUpdateProgramHotHomeEmbed(typeId, zoneId, listObject);
            }
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        #endregion

        #region BoxVideoEmbedByNewsZone
        public BoxVideoEmbedByNewsZoneEntity[] GetListVideoEmbedByNewsZone(int zoneId, int type)
        {
            return BoxVideoEmbedByNewsZoneBo.GetListVideoEmbed(zoneId, type).ToArray();
        }

        public WcfActionResponse InsertVideoEmbedByNewsZone(BoxVideoEmbedByNewsZoneEntity videoEmbedBox)
        {

            WcfActionResponse responseData;

            var errorCode = BoxVideoEmbedByNewsZoneBo.Insert(videoEmbedBox);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;

        }

        public WcfActionResponse UpdateVideoEmbedByNewsZone(string listVideoId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedByNewsZoneBo.Update(listVideoId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteVideoEmbedByNewsZone(long videoId, int zoneId, int type)
        {

            WcfActionResponse responseData;

            var newEncryptVideoId = String.Empty;
            var errorCode = BoxVideoEmbedByNewsZoneBo.Delete(videoId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;

        }
        #endregion

        #region BoxProgramEmbed

        public List<ZoneVideoEntity> SearchProgram(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoxVideoProgramEmbedBo.SearchProgramEmbed(zoneId, keyword, pageIndex, pageSize, ref totalRow);
        }
        public BoxVideoProgramEmbedEntity[] GetListProgramEmbed(int zoneId, int type)
        {
            return BoxVideoProgramEmbedBo.GetListProgramEmbed(zoneId, type).ToArray();
        }

        public WcfActionResponse UpdateProgramEmbed(string listVideoId, int zoneId, int type)
        {

            WcfActionResponse responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            try
            {
                var errorCode = BoxVideoProgramEmbedBo.Update(listVideoId, zoneId, type);
                responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return responseData;
        }
        #endregion

        #endregion

        #region Video Thread
        public List<VideoThreadEntity> SearchVideoThread(string keyword, bool isHotThread, EnumVideoThreadStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoThreadBo.SearchVideoThread(keyword, isHotThread, (int)status, pageIndex, pageSize, ref totalRow);
        }

        public VideoThreadDetailEntity GetVideoThreadById(int id)
        {
            return VideoThreadBo.GetVideoThreadById(id);
        }

        public WcfActionResponse InsertVideoThread(VideoThreadEntity videoThread, string videoIdList, ref int videoThreadId)
        {
            var result = VideoThreadBo.InsertVideoThread(videoThread, videoIdList, ref videoThreadId);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(videoThreadId.ToString(CultureInfo.InvariantCulture),
                                                                 ErrorMapping.Current[ErrorMapping.ErrorCodes.Success])
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse UpdateVideoThread(VideoThreadEntity videoThread, string videoIdList)
        {
            var result = VideoThreadBo.UpdateVideoThread(videoThread, videoIdList);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse DeleteVideoThread(int videoThreadId)
        {
            var result = VideoThreadBo.DeleteVideoThread(videoThreadId);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse UpdateVideoInVideoThread(int videoThreadId, string videoIdList)
        {
            var result = VideoThreadBo.UpdateVideoInVideoThread(videoThreadId, videoIdList);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }
        #endregion

        #region Video tag

        public List<VideoTagEntity> SearchVideoTag(string keyword, int parentId, EnumVideoTagMode tagMode, EnumVideoTagStatus status, EnumVideoTagSort orderBy, bool getTagHasVideoOnly, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoTagBo.SearchVideoTag(keyword, parentId, tagMode, status, orderBy, getTagHasVideoOnly, pageIndex,
                                     pageSize, ref totalRow);
        }
        public List<VideoTagEntity> GetVideoTagByZoneVideoId(int zoneVideoId)
        {
            return VideoTagBo.GetVideoTagByZoneVideoId(zoneVideoId);
        }
        public VideoTagEntity GetVideoTagById(int id)
        {
            try
            {
                return VideoTagBo.GetVideoTagById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public VideoTagEntity GetVideoTagByName(string name)
        {
            try
            {
                return VideoTagBo.GetVideoTagByName(name);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public WcfActionResponse InsertVideoTag(VideoTagEntity videoTag, ref int videoTagId)
        {
            var result = VideoTagBo.InsertVideoTag(videoTag, ref videoTagId);

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    videoTag.Id = videoTagId;
                    var jsonKey = "{\"Id\":" + videoTag.Id + "}";
                    ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(videoTag, "yyyy-MM-dd'T'HH:mm:ss"), "insertvideotag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }
        public WcfActionResponse UpdateVideoTagMode(int videoTagId, bool isHotTag)
        {
            var result = VideoTagBo.UpdateVideoTagMode(videoTagId, isHotTag);

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                try
                {                    
                    var jsonKey = "{\"Id\":" + videoTagId + "}";

                    ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(new { videoTagId, isHotTag }, "yyyy-MM-dd'T'HH:mm:ss"), "updatevideotaghot", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse UpdateVideoTag(VideoTagEntity videoTag)
        {
            var result = VideoTagBo.UpdateVideoTag(videoTag);

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + videoTag.Id + "}";
                    ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(videoTag, "yyyy-MM-dd'T'HH:mm:ss"), "updatevideotag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse UpdateVideoTagWithTagName(string videoTagName, ref int videoTagId)
        {
            try
            {
                var result = VideoTagBo.UpdateVideoTag(videoTagName, ref videoTagId);

                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    try
                    {
                        var jsonKey = "{\"Id\":" + videoTagId + "}";
                        ContentDeliveryServices.PushToDataCD("{\"Id\":" + videoTagId + "}", "insertquickvideotag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }                    
                }

                return result == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("MediaServices.UpdateVideoTagWithTagName() => {0}", ex));
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        #endregion

        #region Video Folder

        public WcfActionResponse InsertVideoFolder(VideoFolderEntity videoFolder)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoFolderId = 0;

            if (VideoFolderBo.InsertVideoFolder(videoFolder, ref videoFolderId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolderId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateVideoFolder(VideoFolderEntity videoFolder)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (VideoFolderBo.UpdateVideoFolder(videoFolder) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolder.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteVideoFolder(int videoFolderId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (VideoFolderBo.DeleteVideoFolder(videoFolderId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolderId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public VideoFolderEntity GetVideoFolderByVideoFolderId(int videoFolderId)
        {
            return VideoFolderBo.GetVideoFolderByVideoFolderId(videoFolderId);
        }

        public VideoFolderEntity[] GetVideoFolderByParentId(int parentId, string createdBy)
        {
            return VideoFolderBo.GetVideoFolderByParentId(parentId, createdBy).ToArray();
        }

        public VideoFolderEntity[] VideoFolderSearch(int parentId, string name, string createdBy, int status)
        {
            return VideoFolderBo.VideoFolderSearch(parentId, name, createdBy, status).ToArray();
        }

        public List<VideoFolderEntity> GetVideoFolderWithTreeView(string createBy)
        {
            return VideoFolderBo.GetAllFolderWithTreeView(createBy);
        }

        #endregion

        #region VideoEPL
        public VideoEPLEntity[] VideoEPLByZoneId(int zoneId, DateTime dateFrom, DateTime dateTo)
        {
            return VideoEPLBo.VideoEPLByZone(zoneId, dateFrom, dateTo).ToArray();
        }
        public VideoEPLZoneToZoneEntity[] VideoEPLZoneAndZone(int zoneId1, int zoneId2, DateTime dateFrom, DateTime dateTo)
        {
            return VideoEPLBo.VideoEPLZoneAndZone(zoneId1, zoneId2, dateFrom, dateTo).ToArray();
        }
        #endregion

        #endregion

        #region Photos

        #region ZonePhoto
        public List<ZonePhotoEntity> GetPrivateZonePhoto(string username, int parentZoneId, int status)
        {
            return ZonePhotoBo.GetPrivateZonePhoto(username, parentZoneId, status);
        }
        public WcfActionResponse InsertZonePhoto(ZonePhotoEntity zonePhoto, ref int zonePhotoId)
        {
            try
            {
                if (ZonePhotoBo.InsertZonePhoto(zonePhoto, ref zonePhotoId) == ErrorMapping.ErrorCodes.Success)
                {
                    zonePhoto.Id = zonePhotoId;
                    if (zonePhoto.Id > 0 && zonePhoto.Status == (int)EnumZonePhotoStatus.Actived)
                    {
                        //cd
                        CmsTrigger.Instance.OnInsertZonePhoto(zonePhoto);
                    }
                    return WcfActionResponse.CreateSuccessResponse(zonePhotoId.ToString(CultureInfo.CurrentCulture));
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateZonePhoto(ZonePhotoEntity zonePhoto)
        {
            try
            {
                if (ZonePhotoBo.UpdateZonePhoto(zonePhoto) == ErrorMapping.ErrorCodes.Success)
                {
                    if (zonePhoto.Id > 0)
                    {
                        //cd
                        CmsTrigger.Instance.OnUpdateZonePhoto(zonePhoto);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public List<ZonePhotoEntity> GetZonePhotoListByParentId(int parentId, EnumZonePhotoStatus status)
        {
            return ZonePhotoBo.GetListByParentId(parentId, (int)status);
        }
        public ZonePhotoEntity GetZonePhotoById(int id)
        {
            return ZonePhotoBo.GetZonePhotoById(id);
        }
        public WcfActionResponse MoveZonePhotoUp(int zonePhotoId)
        {
            try
            {
                if (ZonePhotoBo.MoveZonePhotoUp(zonePhotoId) == ErrorMapping.ErrorCodes.Success)
                {
                    if (zonePhotoId > 0)
                    {
                        CmsTrigger.Instance.OnMoveUpZonePhoto(zonePhotoId);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse MoveZonePhotoDown(int zonePhotoId)
        {
            try
            {
                if (ZonePhotoBo.MoveZonePhotoDown(zonePhotoId) == ErrorMapping.ErrorCodes.Success)
                {
                    if (zonePhotoId > 0)
                    {
                        CmsTrigger.Instance.OnMoveDownZonePhoto(zonePhotoId);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse DeleteZonePhoto(int zonePhotoId)
        {
            try
            {
                if (ZonePhotoBo.DeleteZonePhoto(zonePhotoId) == ErrorMapping.ErrorCodes.Success)
                {
                    if (zonePhotoId > 0)
                    {
                        CmsTrigger.Instance.OnDeleteZonePhoto(zonePhotoId);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        #endregion

        #region PhotoEvent
        public List<PhotoEventEntity> SearchPhotoEvent(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return PhotoEventBo.SerchPhotoEvent(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);
        }
        public PhotoEventEntity GetPhotoEventById(int id)
        {
            return PhotoEventBo.GetPhotoEventById(id);
        }
        public WcfActionResponse InsertPhotoEvent(PhotoEventEntity photoEvent, ref int id)
        {
            try
            {
                if (PhotoEventBo.InsertPhotoEvent(photoEvent, ref id) == ErrorMapping.ErrorCodes.Success)
                {
                    photoEvent.Id = id;
                    if (photoEvent.Id > 0 && photoEvent.Status == (int)EnumZonePhotoStatus.Actived)
                    {
                        //cd
                        try
                        {
                            var strParamValue = NewtonJson.Serialize(photoEvent, "yyyy-MM-dd'T'HH:mm:ss");
                            var jsonKey = "{\"Id\":" + photoEvent.Id + "}";
                            ContentDeliveryServices.PushToDataCD(strParamValue, "insertphotoevent", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, ex.Message);
                        }
                    }
                    return WcfActionResponse.CreateSuccessResponse(id.ToString(CultureInfo.CurrentCulture));
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdatePhotoEvent(PhotoEventEntity photoEvent)
        {
            try
            {
                if (PhotoEventBo.UpdatePhotoEvent(photoEvent) == ErrorMapping.ErrorCodes.Success)
                {
                    if (photoEvent.Id > 0)
                    {
                        //cd
                        try
                        {
                            var strParamValue = NewtonJson.Serialize(photoEvent, "yyyy-MM-dd'T'HH:mm:ss");
                            var jsonKey = "{\"Id\":" + photoEvent.Id + "}";
                            ContentDeliveryServices.PushToDataCD(strParamValue, "updatephotoevent", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, ex.Message);
                        }
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }               
        public WcfActionResponse DeletePhotoEvent(int id)
        {
            try
            {
                if (PhotoEventBo.DeletePhotoEvent(id) == ErrorMapping.ErrorCodes.Success)
                {
                    if (id > 0)
                    {
                        try
                        {
                            var jsonKey = "{\"Id\":" + id + "}";
                            ContentDeliveryServices.PushToDataCD(jsonKey, "deletephotoevent", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, ex.Message);
                        }
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        #endregion

        #region Photo

        public WcfActionResponse InsertPhoto(PhotoEntity photo, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoId = 0L;

            if (PhotoBo.InsertPhoto(photo, tagIdList, ref videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse InsertPhotoV2(PhotoEntity photo, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var photoId = 0L;

            if (PhotoBo.InsertPhotoV2(photo, tagIdList, ref photoId) == ErrorMapping.ErrorCodes.Success)
            {
                photo.Id = photoId;
                //cd   
                //if (photo.Id > 0 && photo.Status == (int)EnumPhotoStatus.Published)
                //{             
                    CmsTrigger.Instance.OnInsertPhoto(photo, tagIdList);
                //}         

                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = photoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhoto(PhotoEntity photo, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhoto(photo, tagIdList) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = photo.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdatePhotoV2(PhotoEntity photo, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var photoDb = PhotoBo.GetById(photo.Id);
            if (photoDb == null)
            {
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.PhotoNotFound;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.PhotoNotFound];
            }

            photo.Status = photoDb.Status;
            photo.CreatedBy = photoDb.CreatedBy;
            photo.CreatedDate = photoDb.CreatedDate;


            if (PhotoBo.UpdatePhotoV2(photo, tagIdList) == ErrorMapping.ErrorCodes.Success)
            {
                //cd
                //if (photo.Id > 0 && photo.Status == (int)EnumPhotoStatus.Published)
                //{
                    CmsTrigger.Instance.OnUpdatePhoto(photo, tagIdList);
                //}

                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = photo.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse SavePhotoByAuthor(PhotoByAuthorEntity photoByAuthorEntity)
        {
            var photoByAuthorId = 0;
            var responseData = WcfActionResponse.CreateSuccessResponse();
            if (PhotoByAuthorBo.SavePhotoByAuthor(photoByAuthorEntity, ref photoByAuthorId) != ErrorMapping.ErrorCodes.Success)
            { 
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeletePhoto(long photoId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.DeletePhoto(photoId) == ErrorMapping.ErrorCodes.Success)
            {
                //cd
                CmsTrigger.Instance.OnDeletePhoto(photoId);

                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = photoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public List<CountAlbumStatus> CountPhoto(string userName, string listStatus, string userAction)
        {
            return PhotoBo.CountPhoto(userName, listStatus, userAction);
        }

        public WcfActionResponse DeletePhotoInAlbum(string listPhotoId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.DeletePhotoInAlbum(listPhotoId) == ErrorMapping.ErrorCodes.Success)
            {
                //cd
                CmsTrigger.Instance.OnUnPublishPhotoIds(listPhotoId);

                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = listPhotoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public List<PhotoPublishedEntity> PublishPhoto(string listPhotoId, int albumId, long newsId, int zoneId, string distributedBy)
        {
            return PhotoBo.PublishPhoto(listPhotoId, albumId, newsId, zoneId, distributedBy);
        }

        public WcfActionResponse SendPhoto(long photoId, string username)
        {
            try
            {
                var result = PhotoBo.SendPhoto(photoId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {                    
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse PublishPhotoV2(long photoId, string username)
        {
            try
            {
                var result = PhotoBo.PublishPhotoV2(photoId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    //cd
                    CmsTrigger.Instance.OnPublishPhoto(photoId);

                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UnPublishPhoto(long photoId, string username)
        {
            try
            {
                var result = PhotoBo.UnPublishPhoto(photoId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    //cd
                    CmsTrigger.Instance.OnUnPublishPhoto(photoId);

                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse ReturnPhoto(long photoId, string username)
        {
            try
            {
                var result = PhotoBo.ReturnPhoto(photoId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {                    
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public PhotoDetailEntity GetPhotoByPhotoId(long videoId)
        {
            return PhotoBo.GetPhotoByPhotoId(videoId);
        }
        public PhotoDetailEntity GetPhotoByPhotoIdV2(long videoId)
        {
            return PhotoBo.GetPhotoByPhotoIdV2(videoId);
        }

        public PhotoEntity GetPhotoByImageUrl(string imageUrl)
        {
            return PhotoBo.GetPhotoByImageUrl(imageUrl);
        }

        public PhotoEntity[] SearchPhoto(string keyword, int videoLabelId, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                PhotoBo.SearchPhoto(keyword, videoLabelId, zoneId, fromDate, toDate, createdBy, pageIndex, pageSize,
                                    ref totalRow).ToArray();
        }
        public List<PhotoEntity> SearchPhotoV2(string keyword, int zoneId, int albumId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return PhotoBo.SearchPhotoV2(keyword, zoneId, albumId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
        }
        public List<PhotoEntity> ListPhotoByAlbumId(int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            return PhotoBo.ListPhotoByAlbumId(albumId, pageIndex, pageSize, ref totalRow);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitEsAllPhoto(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return PhotoBo.InitEsAllPhoto(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Photo published

        public WcfActionResponse UpdatePhotoPublished(PhotoPublishedEntity videoPublished)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoPublished(videoPublished) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoPublished.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoPublishedAlbumId(PhotoPublishedUpdateEntity videoPublished)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoPublishedAlbumid(videoPublished) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoPublished.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoPublishedInUsed(string listPhotoPublishedIdInUsed, long newsId, int albumId, string createdBy)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoPublishedInUsed(listPhotoPublishedIdInUsed, newsId, albumId, createdBy) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }        

        public WcfActionResponse UpdatePhotoPublishedPriority(string listPhotoPublishedId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoPublishedPriority(listPhotoPublishedId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoPublishedEntity GetPhotoPublishedByPhotoPublishedId(long videoPublishedId)
        {
            return PhotoBo.GetPhotoPublishedByPhotoPublishedId(videoPublishedId);
        }

        public PhotoPublishedEntity[] GetPhotoPublishedByPhotoId(long videoId)
        {
            return PhotoBo.GetPhotoPublishedByPhotoId(videoId).ToArray();
        }

        public WcfActionResponse DeleteListPhotoPublished(string listPhotoPublishedId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.DeletePhotoPublished(listPhotoPublishedId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public List<PhotoPublishedEntity> SearchPhotoPublishedInAlbum(string keyword, int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            return PhotoBo.SearchInAlbum(keyword, albumId, pageIndex, pageSize, ref totalRow);
        }

        #endregion

        #region Album

        public WcfActionResponse InsertAlbum(AlbumEntity album, string tagIdList, string eventIdList="")
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();
            var albumId = 0;

            if (AlbumBo.InsertAlbum(album, tagIdList, eventIdList, ref albumId) == ErrorMapping.ErrorCodes.Success)
            {
                album.Id = albumId;
                //cd
                //if (album.Id > 0 && album.Status == (int)EnumPhotoAlbumStatus.Published)
                //{
                    CmsTrigger.Instance.OnInsertAlbum(album, tagIdList);
                //}
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = albumId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateAlbum(AlbumEntity album, string tagIdList, string eventIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var albumDb = AlbumBo.GetById(album.Id);
            if (albumDb == null)
            {
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.PhotoNotFound;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.PhotoNotFound];
            }

            album.Status = albumDb.Status;
            album.CreatedBy = albumDb.CreatedBy;
            album.CreatedDate = albumDb.CreatedDate;
            if (AlbumBo.UpdateAlbum(album, tagIdList, eventIdList) == ErrorMapping.ErrorCodes.Success)
            {
                //cd
                //if (album.Id > 0 && album.Status == (int)EnumPhotoAlbumStatus.Published)
                //{
                    CmsTrigger.Instance.OnUpdateAlbum(album, tagIdList);
                //}
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = album.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse SaveAlbumByAuthor(AlbumByAuthorEntity albumByAuthorEntity)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();
            if (AlbumByAuthorBo.SaveAlbumByAuthor(albumByAuthorEntity) != ErrorMapping.ErrorCodes.Success)
            {            
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;    
        }

        public WcfActionResponse DeleteAlbum(int albumId, string accountName)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (AlbumBo.DeleteAlbum(albumId, accountName) == ErrorMapping.ErrorCodes.Success)
            {
                //cd 
                CmsTrigger.Instance.OnDeleteAlbum(albumId);

                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = albumId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.Success = false;
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public List<CountAlbumStatus> CountAlbum(string userName, string listStatus, string userAction)
        {
            return AlbumBo.CountAlbum(userName, listStatus, userAction);
        }

        public WcfActionResponse SendAlbum(int albumId, string username)
        {
            try
            {
                var result = AlbumBo.SendAlbum(albumId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse PublishAlbum(int albumId, string username)
        {
            try
            {
                var result = AlbumBo.PublishAlbum(albumId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    //cd
                    CmsTrigger.Instance.OnPublishAlbum(albumId);

                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UnPublishAlbum(int albumId, string username)
        {
            try
            {
                var result = AlbumBo.UnPublishAlbum(albumId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    //cd
                    CmsTrigger.Instance.OnUnPublishPhoto(albumId);

                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse ReturnAlbum(int albumId, string username)
        {
            try
            {
                var result = AlbumBo.ReturnAlbum(albumId, username);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse AddPhotoInAlbum(string addPhotoIds, string deletePhotoIds, string waitPublishPhotoIds, int albumId, string albumName)
        {
            if (AlbumBo.AddPhotoInAlbum(addPhotoIds, deletePhotoIds, albumId, albumName) == ErrorMapping.ErrorCodes.Success)
            {
                if (albumId > 0)
                {
                    //list add gồm cả list wait ->Hoạt tự loại
                    CmsTrigger.Instance.OnUpdatePhotoInAlbum(albumId, addPhotoIds, deletePhotoIds, waitPublishPhotoIds);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public AlbumDetailEntity GetAlbumByAlbumId(int albumId)
        {
            return AlbumBo.GetAlbumByAlbumId(albumId);
        }

        public List<AlbumEntity> SearchAlbum(string keyword, int zoneId,int eventId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return AlbumBo.SearchAlbum(keyword, zoneId, eventId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitEsAllPhotoAlbum(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return AlbumBo.InitEsAllPhotoAlbum(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Album published

        public WcfActionResponse UpdateAlbumPublished(AlbumPublishedEntity albumPublished)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (AlbumBo.UpdateAlbumPublished(albumPublished) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = albumPublished.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateAlbumPublishedInUsed(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (AlbumBo.UpdateAlbumPublishedInUsed(listAlbumPublishedIdInUsed, newsId, createdBy) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }        

        public AlbumPublishedEntity GetAlbumPublishedByAlbumPublishedId(long albumPublishedId)
        {
            return AlbumBo.GetAlbumPublishedByAlbumPublishedId(albumPublishedId);
        }

        public AlbumPublishedEntity[] GetAlbumPublishedByAlbumId(long albumId)
        {
            return AlbumBo.GetAlbumPublishedByAlbumId(albumId).ToArray();
        }

        #endregion

        #region Photo tag

        public List<PhotoTagEntity> SearchPhotoTag(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return PhotoTagBo.SearchPhotoTag(keyword, status, pageIndex, pageSize, ref totalRow);
        }        
        public PhotoTagEntity GetPhotoTagById(int id)
        {            
            return PhotoTagBo.GetPhotoTagById(id);            
        }

        public PhotoTagEntity GetPhotoTagByName(string name)
        {            
            return PhotoTagBo.GetPhotoTagByName(name);            
        }

        public WcfActionResponse InsertPhotoTag(PhotoTagEntity photoTag, ref int id)
        {
            var result = PhotoTagBo.InsertPhotoTag(photoTag, ref id);

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    photoTag.Id = id;
                    var jsonKey = "{\"Id\":" + photoTag.Id + "}";
                    ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(photoTag, "yyyy-MM-dd'T'HH:mm:ss"), "insertphototag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }        

        public WcfActionResponse UpdatePhotoTag(PhotoTagEntity photoTag)
        {
            var result = PhotoTagBo.UpdatePhotoTag(photoTag);

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + photoTag.Id + "}";
                    ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(photoTag, "yyyy-MM-dd'T'HH:mm:ss"), "updatephototag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }
        public WcfActionResponse UpdatePhotoTagWithTagName(string tagName, ref int tagId)
        {
            try
            {
                var result = PhotoTagBo.UpdatePhotoTag(tagName, ref tagId);

                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    try
                    {
                        var jsonKey = "{\"Id\":" + tagId + "}";
                        ContentDeliveryServices.PushToDataCD("{\"Id\":" + tagId + "}", "insertquickphototag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }

                return result == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("MediaServices.UpdateVideoTagWithTagName() => {0}", ex));
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region Photo label

        public WcfActionResponse InsertPhotoLabel(PhotoLabelEntity videoLabel)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoLabelId = 0;

            if (PhotoLabelBo.InsertPhotoLabel(videoLabel, ref videoLabelId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoLabelId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoLabel(PhotoLabelEntity videoLabel)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoLabelBo.UpdatePhotoLabel(videoLabel) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoLabel.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeletePhotoLabel(int videoLabelId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoLabelBo.DeletePhotoLabel(videoLabelId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoLabelId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoLabelEntity GetPhotoLabelByPhotoLabelId(int videoLabelId)
        {
            return PhotoLabelBo.GetPhotoLabelByPhotoLabelId(videoLabelId);
        }

        public PhotoLabelEntity[] GetPhotoLabelByParentLabelId(int parentLabelId, string createdBy)
        {
            return PhotoLabelBo.GetPhotoLabelByParentLabelId(parentLabelId, createdBy).ToArray();
        }

        #endregion

        #region Photo Folder

        public WcfActionResponse InsertPhotoFolder(PhotoFolderEntity videoFolder)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoFolderId = 0;

            if (PhotoFolderBo.InsertPhotoFolder(videoFolder, ref videoFolderId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolderId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoFolder(PhotoFolderEntity videoFolder)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoFolderBo.UpdatePhotoFolder(videoFolder) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolder.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeletePhotoFolder(int videoFolderId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoFolderBo.DeletePhotoFolder(videoFolderId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolderId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoFolderEntity GetPhotoFolderByPhotoFolderId(int videoFolderId)
        {
            return PhotoFolderBo.GetPhotoFolderByPhotoFolderId(videoFolderId);
        }

        public PhotoFolderEntity[] GetPhotoFolderByParentId(int parentId, string createdBy)
        {
            return PhotoFolderBo.GetPhotoFolderByParentId(parentId, createdBy).ToArray();
        }

        public PhotoFolderEntity[] PhotoFolderSearch(int parentId, string name, string createdBy, int status)
        {
            return PhotoFolderBo.PhotoFolderSearch(parentId, name, createdBy, status).ToArray();
        }

        public List<PhotoFolderEntity> GetFolderWithTreeView(string createBy)
        {
            return PhotoFolderBo.GetAllFolderWithTreeView(createBy);
        }

        #endregion

        #region PhotoGroup

        public WcfActionResponse InsertPhotoGroupDetailInPhotoTag(PhotoGroupDetailInPhotoTagEntity video)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoId = 0;

            if (PhotoGroupBo.InsertPhotoGroupDetailInPhotoTag(video) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse InsertPhotoGroupDetail(PhotoGroupDetailEntity video)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoId = 0;

            if (PhotoGroupBo.InsertPhotoGroupDetail(video, ref videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdatePhotoGroupDetail(PhotoGroupDetailEntity video)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoGroupBo.UpdatePhotoGroupDetail(video) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = video.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdatePhotoGroupDetailStatus(int id, EnumPhotoGroupDetailStatus status)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoGroupBo.UpdatePhotoGroupDetailStatus(id, status) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse DeletePhotoGroupDetail(int videoId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoGroupBo.DeletePhotoGroupDetail(videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse DeletePhotoGroupDetailInPhotoTag(int videoId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoGroupBo.DeletePhotoGroupDetailInPhotoTag(videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public PhotoGroupDetailEntity GetPhotoGroupDetail(int videoId)
        {
            return PhotoGroupBo.GetPhotoGroupDetailById(videoId);
        }
        public List<PhotoGroupDetailInZonePhotoEntity> GetZonePhotoByPhotoId(int id)
        {
            return ZonePhotoBo.GetZonePhotoByPhotoId(id);
        }
        public PhotoGroupDetailEntity[] SearchPhotoGroupDetail(string keyword, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, EnumPhotoGroupDetailStatus status, int isHot, int videoGroupId, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                PhotoGroupBo.SearchPhotoGroupDetail(keyword, zoneId, fromDate, toDate, createdBy, status, isHot, videoGroupId, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public PhotoGroupDetailCountEntity[] CountPhotoGroupDetail(string username)
        {
            var count = PhotoGroupBo.CountPhotoGroupDetail(username);
            if (count != null) return count.ToArray();
            return
                new PhotoGroupDetailCountEntity[0];
        }
        public PhotoGroupEntity[] GetAllPhotoGroup(ref int totalRow)
        {
            return
                PhotoGroupBo.GetAllPhotoGroup(ref totalRow).ToArray();
        }

        #endregion

        #endregion        

        #region EmbedAlbum

        public List<EmbedAlbumEntity> SearchEmbedAlbum(string keyword, int type, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return EmbedAlbumBo.SearchEmbedAlbum(keyword, type, zoneId, status, pageIndex, pageSize, ref totalRow);
        }
        public EmbedAlbumForEditEntity GetEmbedAlbumForEditByEmbedAlbumId(int embedAlbumId, int topPhoto)
        {
            return EmbedAlbumBo.GetEmbedAlbumForEditByEmbedAlbumId(embedAlbumId, topPhoto);
        }
        public WcfActionResponse CreateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit)
        {
            var errorCode = EmbedAlbumBo.CreateEmbedAlbum(embedAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse CreateEmbedAlbumReturnId(EmbedAlbumForEditEntity embedAlbumForEdit, ref int id)
        {
            var errorCode = EmbedAlbumBo.CreateEmbedAlbumReturnId(embedAlbumForEdit, ref id);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    try
                    {
                        embedAlbumForEdit.EmbedAlbum.Id = id;
                        var strParamValue = NewtonJson.Serialize(embedAlbumForEdit, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + embedAlbumForEdit.EmbedAlbum.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertembedalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch{}
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit)
        {
            var errorCode = EmbedAlbumBo.UpdateEmbedAlbum(embedAlbumForEdit);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    try
                    {                        
                        var strParamValue = NewtonJson.Serialize(embedAlbumForEdit, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + embedAlbumForEdit.EmbedAlbum.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updateembedalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteEmbedAlbum(int embedAlbumId)
        {
            var errorCode = EmbedAlbumBo.DeleteEmbedAlbum(embedAlbumId);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
                {
                    try
                    {
                        var jsonKey = "{\"Id\":" + embedAlbumId + "}";
                        ContentDeliveryServices.PushToDataCD("{}", "deleteembedalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region FileUpload

        public WcfActionResponse InsertFileUpload(FileUploadEntity fileUpload)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var fileUploadId = 0;

            if (FileUploadBo.InsertFileUpload(fileUpload, ref fileUploadId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = fileUploadId.ToString();
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                if (fileUploadId == 0)
                {
                    responseData.Success = false;
                    responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.FileUploadTitleDulicate];
                    responseData.Data = fileUploadId.ToString();
                    responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.FileUploadTitleDulicate;
                }
                else {
                    responseData.Success = false;
                    responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            return responseData;
        }

        public WcfActionResponse UpdateFileUpload(FileUploadEntity fileUpload)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FileUploadBo.UpdateFileUpload(fileUpload) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = fileUpload.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteFileUpload(int fileUploadId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FileUploadBo.DeleteFileUpload(fileUploadId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = fileUploadId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public FileUploadEntity GetFileUpload(int fileUploadId)
        {
            return FileUploadBo.GetFileUploadById(fileUploadId);
        }

        public FileUploadEntity[] SearchFileUpload(string keyword, int zoneId, string ext, string uploadedBy, EnumFileUploadStatus status, int pageIndex, int pageSize, ref int totalRows)
        {
            return
                FileUploadBo.SearchFileUpload(keyword, zoneId, ext, uploadedBy, status, pageIndex, pageSize, ref totalRows).ToArray();
        }

        public FileUploadExtEntity[] GetAllExt()
        {
            return
                 FileUploadBo.GetAllExt().ToArray();
        }

        #endregion

        #region VideoExtension

        public VideoExtensionEntity GetVideoExtensionValue(long videoId, EnumVideoExtensionType type)
        {
            return VideoBo.GetVideoExtensionValue(videoId, type);
        }
        public int GetVideoExtensionMaxValue(long videoId, EnumVideoExtensionType type)
        {
            return VideoBo.GetVideoExtensionMaxValue(videoId, (int)type);
        }
        public WcfResponseData SetVideoExtensionValue(long videoId, EnumVideoExtensionType type, string value)
        {
            var errorCode = VideoBo.SetVideoExtensionValue(videoId, type, value);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfResponseData.CreateSuccessResponse();
            }
            return WcfResponseData.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfResponseData DeleteVideoExtensionByVideoId(long videoId)
        {
            var errorCode = VideoBo.DeleteVideoExtensionByVideoId(videoId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfResponseData.CreateSuccessResponse();
            }
            return WcfResponseData.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<VideoExtensionEntity> GetVideoExtensionByVideoId(long videoId)
        {
            return VideoBo.GetVideoExtensionByVideoId(videoId);
        }
        public List<VideoExtensionEntity> GetVideoExtensionByListVideoId(string listVideoId)
        {
            return VideoBo.GetVideoExtensionByListVideoId(listVideoId);
        }
        public List<VideoExtensionEntity> GetVideoExtensionByTypeAndVaue(EnumVideoExtensionType type, string value)
        {
            return VideoBo.GetVideoExtensionByTypeAndVaue(type, value);
        }

        #endregion

        #region Video External
        public VideoEntity[] SearchVideoFromExternalChannel(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchExternalVideo(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }

        public ZoneVideoEntity[] GetVideoZoneListAsTreeviewFromExternalChannel(int parentId, EnumZoneVideoStatus status, string prefix)
        {
            var zoneList = ZoneVideoBo.GetListByParentIdExternalCms(parentId, (int)status);
            return ZoneVideoBo.BuildTree(zoneList, prefix).ToArray();
        }

        public WcfActionResponse UpdateVideoInNews(string videoInNews, long newsId)
        {
            try
            {
                var result = VideoInNewsBo.UpdateVideoInNews(videoInNews, newsId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(result.ToString(),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoxVideoEmbedEntity[] GetExternalListVideoEmbed(int zoneId, int type)
        {
            return VideoBo.GetExternalListVideoEmbed(zoneId, type).ToArray();
        }

        public string GetErrorMessage(ErrorMapping.ErrorCodes error)
        {
            return ErrorMapping.Current[error];
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public VideoEntity GetDetailExternalVideo(int videoId)
        {
            var zoneName = "";
            return VideoBo.GetById(videoId, ref zoneName);
        }

        #endregion

        #region Video Distribution
        public VideoEntity[] SearchVideoDistribution(string zoneVideoIds, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.SearchVideoDistribution(zoneVideoIds, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoEntity GetDetailVideoDistribution(int videoId)
        {
            var zoneName = "";
            return VideoBo.VideoDistributionGetById(videoId, ref zoneName);
        }
        #endregion

        #region ActiveZone
        public List<ZoneVideoEntity> GetActiveZoneInDay(string zoneIds, DateTime viewDate, int mode)
        {
            return ZoneVideoBo.GetActiveZoneInDay(zoneIds, viewDate, mode);
        }

        public List<ZoneVideoEntity> GetActiveZoneInDayV2(string zoneIds, DateTime viewDate, int mode)
        {
            return ZoneVideoBo.GetActiveZoneInDayV2(zoneIds, viewDate, mode);
        }

        public WcfActionResponse SaveVideoWithoutPermission(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveWithoutPermission(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(result.ToString(),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<VideoModeNotifyEntity> GetNotifyZoneInDay(DateTime viewDate)
        {
            return ZoneVideoBo.GetNotifyZoneInDay(viewDate);
        }

        public PlaylistGroupEntity GetPlaylistGroupByPlaylistId(int playlistId)
        {
            return VideoBo.GetPlaylistGroupByPlaylistId(playlistId);
        }

        public WcfActionResponse InsertPlaylistInVideoGroup(PlaylistInPlaylistGroupEntity playlistGroup)
        {
            try
            {
                if (VideoBo.InsertPlaylistInVideoGroup(playlistGroup) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<PlaylistGroupEntity> GetPlaylistGroupByParentId(int playlistId)
        {
            return VideoBo.GetPlaylistGroupByParentId(playlistId);
        }

        public List<VideoEntity> SearchByPlaylistGroup(int group, int zone)
        {
            return
                VideoBo.SearchByPlaylistGroup(group, zone);
        }
        public WcfActionResponse Playlist_Update_DistributionDate(string playlistIds)
        {
            try
            {
                if (PlaylistBo.Playlist_Update_DistributionDate(playlistIds) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public PlaylistGroupEntity PlaylistGroup_Current(DateTime fromDate, DateTime toDate)
        {
            return PlaylistBo.PlaylistGroup_Current(fromDate, toDate);
        }

        #endregion

        #region Video EPL
        public WcfActionResponse Video_Insert_By_Feed(VideoEntity videoEntity, string tagIds, int MapPlayListTime, string zoneList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.Insert_By_Feed(videoEntity, tagIds, zoneList, MapPlayListTime, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                else if (videoId <= 0)
                    return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Video_Insesert_ZoneRecut(int videoId)
        {
            try
            {
                if (VideoBo.Video_Insesert_ZoneRecut(videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Video_UpdateZonePrimary(int videoId, int ZoneId)
        {
            try
            {
                if (VideoBo.Video_UpdateZonePrimary(videoId, ZoneId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Video_InsertZone(int videoId, string ZoneIds)
        {
            try
            {
                if (VideoBo.Video_InsertZone(videoId, ZoneIds) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Playlist_GetIdByZoneId(int zoneId1, int zoneId2, DateTime DateFrom, DateTime DateTo, int VideoId, ref int PlayListId)
        {
            try
            {
                if (VideoBo.Playlist_GetIdByZoneId(zoneId1, zoneId2, DateFrom, DateTo, VideoId, ref PlayListId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public List<ZoneVideoEntity> ZoneVideo_GetAll()
        {
            return VideoBo.ZoneVideo_GetAll();
        }
        public List<EPLPlayListEntity> EPLPlayListGetAll()
        {
            return VideoEPLBo.EPLPlayListGetAll();
        }

        public List<EPLPlayListEntity> EPLPlayListGetByUser(string userName)
        {
            return VideoEPLBo.EPLPlayListGetByUser(userName);
        }

        public WcfActionResponse InsertVideoToEPLPlaylist(string userName, int videoId)
        {
            try
            {
                if (VideoEPLBo.Insert(userName, videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoFromEPLPlaylist(int userId, int videoId)
        {
            try
            {
                if (VideoEPLBo.Delete(userId, videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region VideoApi
        public bool GetVideoApiById(int videoId)
        {
            return VideoBo.GetVideoApiById(videoId);
        }
        public bool InsertVideoApi(VideoEntity videoEntity)
        {
            return VideoBo.InsertVideoApi(videoEntity);
        }
        public List<VideoApiEntity> GetListVideoApiById(string listVideoId)
        {
            return
                VideoBo.GetListVideoApiById(listVideoId);
        }
        #endregion

        #region Kenh14EPLVideoApi
        public bool Kenh14EPLInsertVideoApi(VideoEntity videoEntity)
        {
            return VideoBo.Kenh14InsertVideoApi(videoEntity);
        }
        public bool Kenh14EPLChangerPublishVideoApi(string keyVideo, int status)
        {
            return VideoBo.Kenh14EPLChangerPublishVideoApi(keyVideo, status);
        }
        public bool Kenh14EPLUpdateVideoApi(VideoEntity videoEntity)
        {
            return VideoBo.Kenh14EPLChangerUpdateVideoApi(videoEntity);
        }
        #endregion

        #region MP3

        public List<Mp3Entity> SearchMp3(string username, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.SearchMp3(username, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public Mp3Entity GetMp3ById(int id)
        {
            return VideoBo.GetMp3ById(id);
        }

        public WcfActionResponse SaveMp3(Mp3Entity mp3Entity)
        {
            try
            {
                var id = 0;
                var result = VideoBo.SaveMp3(mp3Entity, ref id);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(id.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion
    }
}