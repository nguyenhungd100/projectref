﻿using ChannelVN.CMS.BO.Base.Topic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.WebApi.Models;
using ChannelVN.CMS.WebApi.Services.ExternalServices;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class TopicServices
    {
        #region Get
        public TopicEntity GetTopicByName(string nameTopic)
        {
            return TopicBo.GetTopicByName(nameTopic);
        }

        public TopicDetailEntity GetTopicByTopicId(long TopicId)
        {
            return TopicBo.GetTopicByTopicId(TopicId);
        }
        public List<TopicEntity> GetTopicByNewsId(long newsId)
        {
            return TopicBo.GetTopicByNewsId(newsId);
        }
        //app bot
        public TopicDataAppBotEntity GetListTopicAndTopicParrentByNewsId(long newsId)
        {
            return TopicBo.GetListTopicAndTopicParrentByNewsId(newsId);
        }
        public List<TopicEntity> SearchTopic(string keyword, int zoneId, int orderBy, int isHotTopic, int isActive, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            return TopicBo.SearchTopic(keyword, zoneId, orderBy, isHotTopic, isActive, parentId, pageIndex, pageSize, ref totalRow);
        }

        public List<TopicParent> ListParentTopicById(int topicId)
        {
            return TopicBo.ListParentTopicById(topicId);
        }

        public WcfActionResponse ToggleTopicActive(int id)
        {
            var errorCode = TopicBo.ToggleTopicActive(id);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + id + "}";
                var strPram = NewtonJson.Serialize(new { }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatetopicisactive", string.Empty, jsonKey);
            }

            return responseData;
        }

        public WcfActionResponse ToggleTopicIconActive(int id)
        {
            var errorCode = TopicBo.ToggleTopicIconActive(id);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + id + "}";
                var strPram = NewtonJson.Serialize(new { }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatetopicisiconactive", string.Empty, jsonKey);
            }

            return responseData;
        }

        public WcfActionResponse ToggleTopicIsToolbar(int id)
        {
            var errorCode = TopicBo.ToggleTopicIsToolbar(id);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + id + "}";
                var strPram = NewtonJson.Serialize(new { }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatetopicistoolbar", string.Empty, jsonKey);
            }

            return responseData;
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByTopicAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return TopicBo.InitRedisByTopicAsync(name, startDate, endDate, pageSize, action);
        }
        #endregion

        #region Update
        public WcfActionResponse Insert(TopicEntity Topic, int zoneId, string zoneIdList, ref int newTopicId)
        {
            WcfActionResponse responseData;
            try
            {
                if (BO.Common.BoConstants.IsBuildLinkTopic)
                {
                    var urlFormat = BO.Common.BoConstants.UrlFormatForTopic;
                    if (urlFormat.IndexOf("{1}") > 0)
                    {
                        Topic.DisplayUrl = string.Format(urlFormat, Topic.DisplayUrl, "{TopicId}");
                    }
                }

                var errorCode = TopicBo.Insert(Topic, zoneId, zoneIdList, ref newTopicId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {                    
                    try
                    {
                        var data = new TopicFullInfoCdData();
                        Topic.Id = newTopicId;
                        Topic.DisplayUrl = Topic.DisplayUrl.Replace("{TopicId}", newTopicId.ToString());
                        data.Topic = Topic;
                        data.ZoneId = zoneId;
                        data.ZoneIdList = zoneIdList;
                        var strParamValue = NewtonJson.Serialize(data, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + Topic.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "inserttopic", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newTopicId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse Update(TopicEntity Topic, int zoneId, string zoneIdList)
        {
            WcfActionResponse responseData;
            try
            {
                if (BO.Common.BoConstants.IsBuildLinkTopic)
                {
                    var urlFormat = BO.Common.BoConstants.UrlFormatForTopic;
                    if (urlFormat.IndexOf("{1}") > 0)
                    {
                        Topic.DisplayUrl = string.Format(urlFormat, Topic.DisplayUrl, Topic.Id);
                    }
                }

                var errorCode = TopicBo.Update(Topic, zoneId, zoneIdList); 
                               
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    try {
                        var data = new TopicFullInfoCdData();
                        data.Topic = Topic;
                        data.ZoneId = zoneId;
                        data.ZoneIdList = zoneIdList;
                        var strParamValue = NewtonJson.Serialize(data, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + Topic.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatetopic", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                
                return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(Topic.Id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse DeleteById(long TopicId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = TopicBo.DeleteById(TopicId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                    try {
                        var jsonKey = "{\"Id\":" + TopicId + "}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "deletetopic", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateTopicHot(long TopicId, bool isHotTopic)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = TopicBo.UpdateTopicHot(TopicId, isHotTopic);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();                          
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateTopicNews(int TopicId, string deleteNewsId, string addNewsId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = TopicBo.UpdateTopicNews(TopicId, deleteNewsId, addNewsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                    try
                    {
                        var jsonKey = "{\"Id\":" + TopicId + "}";
                        var data = new TopicNewsCdData();
                        data.TopicId = TopicId;
                        data.DeleteNewsId = deleteNewsId;
                        data.AddnewsId = addNewsId;
                        var strParamValue = NewtonJson.Serialize(data, "yyyy-MM-dd'T'HH:mm:ss");
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatetopicnews", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse MultiUpdateTopicNews(string listTopicId, long newsId, int topicId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = TopicBo.MultiUpdateTopicNews(listTopicId, newsId, topicId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                    try
                    {
                        var jsonKey = "{\"NewsId\":" + newsId + ",\"ListTopicId\":\"" + listTopicId + "\",\"TopicId\":\"" + topicId + "\"}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "multiupdatetopicnews", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateTopicNewsHot(long topicId, string addNewsAvatar, string addNewsId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = TopicBo.UpdateTopicNewsHot(topicId, addNewsAvatar, addNewsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                    try
                    {
                        var jsonKey = "{\"Id\":" + topicId + "}";
                        var data = new TopicNewsHotCdData();
                        data.TopicId = topicId;
                        data.AddNewsAvatar = addNewsAvatar;
                        data.AddNewsId = addNewsId;
                        var strParamValue = NewtonJson.Serialize(data, "yyyy-MM-dd'T'HH:mm:ss");
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatetopicnewshot", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateTopicNewsRelated(long newsId, string relatedTopics)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = TopicBo.UpdateTopicNewsRelated(newsId, relatedTopics);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        #endregion

        #region BoxTopicEmbed
        public List<BoxTopicEmbedEntity> ListBoxTopicEmbed(int topicId, int type)
        {
            return TopicBo.ListBoxTopicEmbed(topicId, type);
        }

        public WcfActionResponse InsertBoxTopicEmbed(BoxTopicEmbedEntity box, ref int id)
        {
            var errorCode = TopicBo.InsertBoxTopicEmbed(box, ref id);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var jsonValue = NewtonJson.Serialize(box);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "insertboxtopicembed", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }

        public WcfActionResponse UpdateBoxTopicEmbed(BoxTopicEmbedEntity box)
        {
            var errorCode = TopicBo.UpdateBoxTopicEmbed(box);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(box.Id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {                    
                    var jsonKey = "{\"Id\":" + box.Id + "}";                    
                    var jsonValue = NewtonJson.Serialize(box);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "updateboxtopicembed", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }

        public WcfActionResponse DeleteBoxTopicEmbed(int id)
        {
            var errorCode = TopicBo.DeleteBoxTopicEmbed(id);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var jsonValue = NewtonJson.Serialize(id);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "deleteboxtopicembed", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }
        #endregion

        #region BoxTopicOnPageEmbed
        public List<BoxTopicOnPageEmbedEntity> ListBoxTopicOnPageEmbed(int zoneId, int type)
        {
            return TopicBo.ListBoxTopicOnPageEmbed(zoneId, type);
        }

        public WcfActionResponse UpdateBoxTopicOnPageEmbed(string listTopicId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = TopicBo.UpdateBoxTopicOnPageEmbed(listTopicId, zoneId, type);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {                    
                    var currentTopicEmbed = new ChangeTopicEmbedData();
                    var ArrTopicId = listTopicId.Split(',');
                    var jsonKey = "{\"ZoneId\":" + zoneId + ", \"Type\":" + type + "}";
                    currentTopicEmbed.Type = type;
                    currentTopicEmbed.ZoneId = zoneId;
                    currentTopicEmbed.LastModifiedDate = DateTime.Now.ToString("yyyy/MM/dd HH:ss:mm");
                    currentTopicEmbed.ArrTopicId = ArrTopicId;                    

                    var jsonValue = NewtonJson.Serialize(currentTopicEmbed);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "updateboxtopiconpageembed", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(listTopicId, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse InsertBoxTopicOnPageEmbed(BoxTopicOnPageEmbedEntity box, ref int id)
        {
            var errorCode = TopicBo.InsertBoxTopicOnPageEmbed(box, ref id);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var jsonValue = NewtonJson.Serialize(box);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "insertboxtopiconpageembed", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }

        public WcfActionResponse UpdateBoxTopicOnPageEmbed(BoxTopicOnPageEmbedEntity box)
        {
            var errorCode = TopicBo.UpdateBoxTopicOnPageEmbed(box);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(box.Id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + box.Id + "}";
                    var jsonValue = NewtonJson.Serialize(box);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "updateboxtopiconpageembed", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }

        public WcfActionResponse DeleteBoxTopicOnPageEmbed(int id)
        {
            var errorCode = TopicBo.DeleteBoxTopicOnPageEmbed(id);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var jsonValue = NewtonJson.Serialize(id);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "deleteboxtopiconpageembed", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}