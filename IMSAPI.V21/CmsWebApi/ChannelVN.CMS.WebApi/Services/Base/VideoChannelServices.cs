﻿using ChannelVN.CMS.BO.Base.Video;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class VideoChannelServices
    {
        public WcfActionResponse SaveVideoChannel(VideoChannelEntity channelEntity, string zoneIdList, string labelIdList, string playlistIdList, string videoIdList)
        {
            try
            {
                var videoChannelId = 0;
                var result = VideoChannelBo.SaveVideoChannel(channelEntity, zoneIdList, labelIdList, playlistIdList, videoIdList, ref videoChannelId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoChannelId.ToString(CultureInfo.CurrentCulture), null);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse DeleteChannel(int channelId)
        {
            try
            {
                if (VideoChannelBo.DeleteChannel(channelId) == ErrorMapping.ErrorCodes.Success)
                {                    
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<VideoChannelSearchEntity> SearchVideoChannel(string keyword, string zoneIds, string labelIds, string playlistIds, string videoIds, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {            
            return VideoChannelBo.SearchVideoChannel(keyword, zoneIds, labelIds, playlistIds, videoIds, status, fromDate, toDate, pageIndex, pageSize, ref totalRows);
        }

        public VideoChannelEntity GetVideoChannelDetail(int id, string currentUsername)
        {
            return VideoChannelBo.GetVideoChannelDetail(id, currentUsername);
        }

        public VideoChannelCachedEntity GetVideoChannelDetailEdit(int id, string currentUsername)
        {
            return VideoChannelBo.GetVideoChannelDetailEdit(id, currentUsername);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitEsAllVideoChannel(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return VideoChannelBo.InitEsAllVideoChannel(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllVideoChannel(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return VideoChannelBo.InitRedisAllVideoChannel(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}