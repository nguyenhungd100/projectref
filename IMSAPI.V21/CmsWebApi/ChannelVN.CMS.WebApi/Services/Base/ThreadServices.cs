﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Thread;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using ChannelVN.CMS.WebApi.Services.ExternalServices;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class ThreadServices
    {
        #region Get
        public ThreadDetailEntity GetThreadByThreadId(long threadId)
        {
            return BoFactory.GetInstance<ThreadBo>().GetThreadByThreadId(threadId);
        }
        public List<ThreadEntity> GetThreadByNewsId(long newsId)
        {
            return BoFactory.GetInstance<ThreadBo>().GetThreadByNewsId(newsId);
        }
        public List<ThreadEntity> SearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoFactory.GetInstance<ThreadBo>().SearchThread(keyword, zoneId, orderBy, isHotThread, pageIndex, pageSize, ref totalRow);
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByThreadAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return BoFactory.GetInstance<ThreadBo>().InitEsByThreadAsync(name, startDate, endDate, pageSize, action);
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByThreadAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return BoFactory.GetInstance<ThreadBo>().InitRedisByThreadAsync(name, startDate, endDate, pageSize, action);
        }
        public List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestion(int top, int zoneId, string keyword, int orderBy)
        {
            return BoFactory.GetInstance<ThreadBo>().SearchThreadForSuggestion(top, zoneId, keyword, (int)orderBy);
        }
        #endregion

        #region Update
        public WcfActionResponse Insert(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, ref int newThreadId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().Insert(thread, zoneId, zoneIdList, relationThread, ref newThreadId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    thread.Id = newThreadId;
                    CmsTrigger.Instance.OnAddNewThread(thread, zoneId, zoneIdList, relationThread);
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(thread));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse Update(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().Update(thread, zoneId, zoneIdList, relationThread);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    CmsTrigger.Instance.OnUpdateThread(thread, zoneId, zoneIdList, relationThread);
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(thread));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse DeleteById(long threadId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().DeleteById(threadId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    CmsTrigger.Instance.OnDeleteThread(threadId);
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateThreadHot(long threadId, bool isHotThread)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadHot(threadId, isHotThread);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    var jsonKey = "{\"ThreadId\":" + threadId + "}";
                    var strPram = NewtonJson.Serialize(new { IsHot=isHotThread }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updatethreadishot", string.Empty, jsonKey);

                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateIsInvisibled(long threadId, bool invisibled)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateIsInvisibled(threadId, invisibled);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    var jsonKey = "{\"ThreadId\":" + threadId + "}";
                    var strPram = NewtonJson.Serialize(new { Invisibled=invisibled }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updatethreadinvisibled", string.Empty, jsonKey);

                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateThreadNews(long threadId, string deleteNewsId, string addNewsId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadNews(threadId, deleteNewsId, addNewsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    CmsTrigger.Instance.OnChangeThread(threadId, addNewsId, deleteNewsId);

                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse MultiUpdateThreadNews(string listThreadId, long newsId, int threadId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = ThreadBo.MultiUpdateThreadNews(listThreadId, newsId, threadId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {                    
                    responseData = WcfActionResponse.CreateSuccessResponse();
                    try
                    {
                        var jsonKey = "{\"NewsId\":" + newsId + ",\"ListThreadId\":\"" + listThreadId + "\",\"ThreadId\":" + threadId + "}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "multiupdatethreadnews", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateThreadNewsRelated(long newsId, string relatedThreads)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadNewsRelated(newsId, relatedThreads);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        #endregion

        #region BoxThreadEmbed
        public List<BoxThreadEmbedEntity> GetListThreadEmbed(int zoneId, int type)
        {
            return BoFactory.GetInstance<BoxThreadEmbedBo>().GetListThreadEmbed(zoneId, type);
        }

        public WcfActionResponse InsertThreadEmbed(BoxThreadEmbedEntity threadEmbedBox)
        {
            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Insert(threadEmbedBox);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(string.Empty, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateThreadEmbed(string listThreadId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Update(listThreadId, zoneId, type);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                CmsTrigger.Instance.OnChangeThreadEmbed(listThreadId, type, zoneId);
            }

            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(listThreadId, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteThreadEmbed(long threadId, int zoneId, int type)
        {
            WcfActionResponse responseData;
            var newEncryptThreadId = string.Empty;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Delete(threadId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(threadId.ToString(), "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}