﻿using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Nodejs;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class VideoApiServices
    {
        #region VideoApi

        public static NodeJs_VideoApiEntity GetVideoInfo(string path)
        {
            return VideoApiNodeJsServices.GetVideoInfo(path);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}