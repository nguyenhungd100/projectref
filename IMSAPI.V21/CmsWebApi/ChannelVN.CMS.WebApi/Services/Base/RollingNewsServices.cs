﻿using ChannelVN.CMS.BO.Base.RollingNews;
using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class RollingNewsServices
    {
        #region RollingNews

        public List<NodeJs_RollingNewsEntity> SearchRollingNews(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return RollingNewsBo.SearchRollingNews(fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        //public List<RollingNewsForSuggestionEntity> SearchRollingNewsForSuggestion(string keyword, EnumRollingNewsStatus status, int top)
        //{
        //    return RollingNewsBo.SearchRollingNewsForSuggestion(keyword, status, top);
        //}

        public NodeJs_RollingNewsEntity GetRollingNewsByRollingNewsId(string rollingNewsId)
        {
            return RollingNewsBo.GetRollingNewsByRollingNewsId(rollingNewsId);
        }
        //public WcfActionResponse InsertRollingNews(RollingNewsEntity rollingNews)
        //{
        //    var errorCode = RollingNewsBo.InsertRollingNews(rollingNews);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        public WcfActionResponse InsertRollingNewsV2(NodeJs_RollingNews rollingNews, ref string Id)
        {
            var errorCode = RollingNewsBo.InsertRollingNewsV2(rollingNews, ref Id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateRollingNews(NodeJs_RollingNews rollingNews)
        {
            var errorCode = RollingNewsBo.UpdateRollingNews(rollingNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        //public WcfActionResponse DeleteRollingNews(int rollingNewsId)
        //{
        //    var errorCode = RollingNewsBo.DeleteRollingNews(rollingNewsId);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse ChangeStatusRollingNews(int rollingNewsId, EnumRollingNewsStatus status)
        //{
        //    var errorCode = RollingNewsBo.ChangeStatusRollingNews(rollingNewsId, status);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}

        #endregion

        #region RollingNewsAuthor

        //public List<RollingNewsAuthorEntity> GetRollingNewsAuthorByRollingNewsId(int rollingNewsId)
        //{
        //    return RollingNewsBo.GetRollingNewsAuthorByRollingNewsId(rollingNewsId);
        //}
        //public RollingNewsAuthorEntity GetRollingNewsAuthorByRollingNewsAuthorId(int rollingNewsAuthorId)
        //{
        //    return RollingNewsBo.GetRollingNewsAuthorByRollingNewsAuthorId(rollingNewsAuthorId);
        //}
        //public WcfActionResponse InsertRollingNewsAuthor(RollingNewsAuthorEntity rollingNewsAuthor)
        //{
        //    var errorCode = RollingNewsBo.InsertRollingNewsAuthor(rollingNewsAuthor);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse UpdateRollingNewsAuthor(RollingNewsAuthorEntity rollingNewsAuthor)
        //{
        //    var errorCode = RollingNewsBo.UpdateRollingNewsAuthor(rollingNewsAuthor);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse DeleteRollingNewsAuthor(int rollingNewsAuthorId)
        //{
        //    var errorCode = RollingNewsBo.DeleteRollingNewsAuthor(rollingNewsAuthorId);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}

        #endregion

        #region RollingNewsEvent

        public List<NodeJs_RollingNewsEventEntity> GetRollingNewsEventByRollingNewsId(string rollingNewsId)
        {
            return RollingNewsBo.GetRollingNewsEventByRollingNewsId(rollingNewsId);
        }
        public NodeJs_RollingNewsEventEntity GetRollingNewsEventByRollingNewsEventId(string rollingNewsEventId)
        {
            return RollingNewsBo.GetRollingNewsEventByRollingNewsEventId(rollingNewsEventId);
        }

        public WcfActionResponse InsertRollingNewsEvent(NodeJs_RollingNewsEventEntity rollingNewsEvent, ref string Id)
        {
            var errorCode = RollingNewsBo.InsertRollingNewsEvent(rollingNewsEvent, ref Id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateRollingNewsEvent(NodeJs_RollingNewsEventEntity rollingNewsEvent)
        {
            var errorCode = RollingNewsBo.UpdateRollingNewsEvent(rollingNewsEvent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        //public WcfActionResponse UpdateRollingNewsEventV2(List<RollingNewsEventEntity> rollingNewsEvent, int rollingNewsId)
        //{
        //    var errorCode = RollingNewsBo.UpdateRollingNewsEventV2(rollingNewsEvent, rollingNewsId);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        public WcfActionResponse DeleteRollingNewsEvent(string rollingNewsEventId)
        {
            var errorCode = RollingNewsBo.DeleteRollingNewsEvent(rollingNewsEventId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteRollingNewsEventByRollingNews(string rollingNewsEventId)
        {
            var errorCode = RollingNewsBo.DeleteRollingNewsEventByRollingNewsId(rollingNewsEventId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteRollingNewsEventByListIds(string ids)
        {
            var errorCode = RollingNewsBo.DeleteRollingNewsEventByListIds(ids);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse PublishRollingNewsEvent(string id, string rollingnews_id, string lastModifiedBy)
        {
            var errorCode = RollingNewsBo.PublishRollingNewsEvent(id, rollingnews_id, lastModifiedBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UnpublishRollingNewsEvent(string id, string rollingnews_id, string lastModifiedBy)
        {
            var errorCode = RollingNewsBo.UnpublishRollingNewsEvent(id, rollingnews_id, lastModifiedBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        //public WcfActionResponse UpdateIsFocusForRollingNewsEvent(int rollingNewsEventId, bool isFocus)
        //{
        //    var errorCode = RollingNewsBo.UpdateIsFocusForRollingNewsEvent(rollingNewsEventId, isFocus);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        //public WcfActionResponse RefreshRollingNewsEvent(int rollingNewsEventId)
        //{
        //    var errorCode = RollingNewsBo.UpdateRollingNewsEventIntoRedis(rollingNewsEventId);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}