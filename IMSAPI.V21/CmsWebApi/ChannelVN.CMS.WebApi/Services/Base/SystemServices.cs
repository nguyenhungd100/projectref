﻿using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Linq;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class SystemServices
    {
        private readonly string prefix_name_es = "_es";
        private readonly string prefix_name_redis = "_redis";

        public BoCached.Entity.Init.LogInitAsyncEntity InitDataRedis(string id, string userName, string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null, int zoneid = 0)
        {
            var logInit = new BoCached.Entity.Init.LogInitAsyncEntity {
                Id = name + prefix_name_redis + "_" + startDate.Ticks + "_" + endDate.Ticks,
                Message = "Init field name not available.",
                Date=DateTime.Now
            };
            try
            {
                if (!string.IsNullOrEmpty(name))
                {
                    switch (name.ToLower())
                    {
                        case "news":
                            name = name + prefix_name_redis;
                            logInit = new NewsServices().InitRedisByNewsAsync(name, startPage, startDate, endDate, pageSize, action, zoneid);
                            break;
                        case "news_check_duplicate":
                            name = name + prefix_name_redis;
                            logInit = new NewsServices().InitRedisByNewsCheckDuplicateAsync(name, startPage, startDate, endDate, pageSize, action);
                            break;
                        case "user":
                            name = name + prefix_name_redis;
                            logInit = new SecurityServices().InitRedisAllUser(name, startDate, endDate, pageSize, action);
                            break;
                        case "userpermission":
                            name = name + prefix_name_redis;
                            logInit = new SecurityServices().InitRedisAllUserPermission(id, name, startDate, endDate, pageSize, action);
                            break;
                        case "zone":
                            name = name + prefix_name_redis;
                            logInit = new NewsServices().InitRedisAllZone(name, startDate, endDate, pageSize, action);
                            break;
                        case "zonevideo":
                            name = name + prefix_name_redis;
                            logInit = new MediaServices().InitRedisAllZoneVideo(name, startDate, endDate, pageSize, action);
                            break;
                        case "newsauthor":
                            name = name + prefix_name_redis;
                            logInit = new NewsServices().InitRedisAllNewsAuthor(name, startDate, endDate, pageSize, action);
                            break;
                        case "video":
                            name = name + prefix_name_redis;
                            logInit = new MediaServices().InitRedisByVideoAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "discussiontopic":
                            name = name + prefix_name_redis;
                            logInit = new DiscussionV2Services().InitRedisByDiscussionTopicAsync(userName, name, startDate, endDate, pageSize, action);
                            break;
                        case "quote":
                            name = name + prefix_name_redis;
                            logInit = new Kenh14Services().InitRedisAllQuote(name, startDate, endDate, pageSize, action);
                            break;
                        case "characterinfo":
                            name = name + prefix_name_redis;
                            logInit = new Kenh14Services().InitRedisAllCharacterInfo(name, startDate, endDate, pageSize, action);
                            break;
                        case "tag":
                            name = name + prefix_name_redis;
                            logInit = new TagServices().InitRedisByTagAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "topic":
                            name = name + prefix_name_redis;
                            logInit = new TopicServices().InitRedisByTopicAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "thread":
                            name = name + prefix_name_redis;
                            logInit = new ThreadServices().InitRedisByThreadAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "vote":
                            name = name + prefix_name_redis;
                            logInit = new VoteServices().InitRedisByVoteAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "playlist":
                            name = name + prefix_name_redis;
                            logInit = new MediaServices().InitRedisAllPlayList(name, startDate, endDate, pageSize, action);
                            break;
                        case "videochannel":
                            name = name + prefix_name_redis;
                            logInit = new VideoChannelServices().InitRedisAllVideoChannel(name, startDate, endDate, pageSize, action);
                            break;
                        case "streamitem":
                            name = name + prefix_name_redis;
                            logInit = new StreamItemServices().InitRedisByStreamItemAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "newsextension":
                            name = name + prefix_name_redis;
                            logInit = new NewsServices().InitRedisByNewsExtensionAsync(name, startDate, endDate, pageSize, action);
                            break;                        
                    }
                }
                return logInit;
            }
            catch(Exception ex)
            {
                return new BoCached.Entity.Init.LogInitAsyncEntity
                {
                    Id = name + prefix_name_es + "_" + startDate.Ticks + "_" + endDate.Ticks,
                    Message = "Error: " + ex.Message,
                    Date = DateTime.Now
                };
            }
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitDataES(string userName, string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null, int zoneid = 0)
        {
            var logInit = new BoCached.Entity.Init.LogInitAsyncEntity
            {
                Id = name + prefix_name_es + "_" + startDate.Ticks + "_" + endDate.Ticks,
                Message = "Init field name not available.",
                Date = DateTime.Now
            };
            try
            {
                if (!string.IsNullOrEmpty(name))
                {
                    switch (name.ToLower())
                    {
                        case "news":
                            name = name + prefix_name_es;
                            logInit = new NewsServices().InitEsByNewsAsync(name, startPage, startDate, endDate, pageSize, action, zoneid);
                            break;
                        case "user":
                            name = name + prefix_name_es;
                            logInit = new SecurityServices().InitESAllUser(name, startDate, endDate, pageSize, action);
                            break;
                        case "video":
                            name = name + prefix_name_es;
                            logInit = new MediaServices().InitEsByVideoAsync(name, startPage, startDate, endDate, pageSize, action);
                            break;
                        case "discussion_topic":
                            name = name + prefix_name_es;
                            logInit = new DiscussionV2Services().InitEsByDiscussionTopicAsync(userName, name, startDate, endDate, pageSize, action);
                            break;
                        case "quote":
                            name = name + prefix_name_es;
                            logInit = new Kenh14Services().InitEsByQuoteAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "characterinfo":
                            name = name + prefix_name_es;
                            logInit = new Kenh14Services().InitEsByCharacterInfoAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "tag":
                            name = name + prefix_name_es;
                            logInit = new TagServices().InitEsByTagAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "thread":
                            name = name + prefix_name_es;
                            logInit = new ThreadServices().InitEsByThreadAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "vote":
                            name = name + prefix_name_es;
                            logInit = new VoteServices().InitEsByVoteAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "newsauthor":
                            name = name + prefix_name_es;
                            logInit = new NewsServices().InitEsByAuthorAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "playlist":
                            name = name + prefix_name_es;
                            logInit = new MediaServices().InitEsAllPlayList(name, startDate, endDate, pageSize, action);
                            break;
                        case "videochannel":
                            name = name + prefix_name_es;
                            logInit = new VideoChannelServices().InitEsAllVideoChannel(name, startDate, endDate, pageSize, action);
                            break;
                        case "streamitem":
                            name = name + prefix_name_es;
                            logInit = new StreamItemServices().InitEsByStreamItemAsync(name, startDate, endDate, pageSize, action);
                            break;
                        case "photoalbum":
                            name = name + prefix_name_es;
                            logInit = new MediaServices().InitEsAllPhotoAlbum(name, startDate, endDate, pageSize, action);
                            break;
                        case "photo":
                            name = name + prefix_name_es;
                            logInit = new MediaServices().InitEsAllPhoto(name, startDate, endDate, pageSize, action);
                            break;
                    }
                }
                return logInit;
            }
            catch(Exception ex)
            {
                return new BoCached.Entity.Init.LogInitAsyncEntity
                {
                    Id = name + prefix_name_es + "_" + startDate.Ticks + "_" + endDate.Ticks,
                    Message = "Error: "+ ex.Message,
                    Date = DateTime.Now
                };
            }
        }

        public dynamic InitRedisToSql(string userName, DateTime startDate, DateTime endDate)
        {            
            try
            {
                return new NewsServices().InitRedisToSql(userName, startDate, endDate);
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public bool InitZoneRedisToSql()
        {
            try
            {
                var zones = BoCached.Base.Zone.ZoneDalFactory.GetZoneByParentId(-1);
                if(zones!=null && zones.Count > 0)
                {
                    foreach(var zone in zones)
                    {
                        ZoneDal.Insert(zone);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteIndexRedis(string indexKey)
        {
            try
            {
                BoCached.Base.Account.UserDalFactory.DeleteIndexRedis(indexKey);
                return true;
            }
            catch(Exception ex) {
                return false;
            }
        }
        public bool DeleteIndexEs(string indexKey)
        {
            try
            {
                BoSearch.Base.Account.UserDalFactory.DeleteIndexEs(indexKey);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteNewsIdsEsRedis(string newsids)
        {
            try
            {
                var listId = newsids.Split(',').Select(s=>s.Replace("\n", "").Trim()).ToList();

                BoSearch.Base.Account.UserDalFactory.DeleteNewsIdsEsRedis(listId);
                BoCached.Base.News.NewsDalFactory.DeleteNewsByNewsIdsRedis(listId);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string GenConnectionString(string connect,string key,string action=null)
        {
            try
            {
                if (action=="e_pass")
                {
                    return ChannelVN.CMS.Common.Crypton.Encrypt(connect);
                }
                if (action == "d_pass")
                {
                    return ChannelVN.CMS.Common.Crypton.Decrypt(connect);
                }
                if (action == "d_conn")
                {
                    return ChannelVN.CMS.Common.Crypton.DecryptByKey(connect, key);
                }
             
                return ChannelVN.CMS.Common.Crypton.EncryptByKey(connect, key);
            }
            catch (Exception ex)
            {
                return "Error: gen connection string.";
            }
        }     

        public bool InitEsAndRedisByNewsId(long newsId)
        {
            try
            {
                var newsInfo = new NewsServices().GetNewsByNewsIdForInit(newsId);
                if (newsInfo != null)
                {
                    if (string.IsNullOrEmpty(newsInfo.PublishedBy) && newsInfo.Status == (int)NewsStatus.Published)
                        newsInfo.PublishedBy = newsInfo.LastModifiedBy;

                    BoCached.Base.News.NewsDalFactory.AddNews(newsInfo);
                    //BoCached.Base.News.NewsDalFactory.DeleteNewsByNewsIdRedis(newsId.ToString());
                    if(!BoSearch.Base.News.NewsDalFactory.AddNews(newsInfo))
                        BoSearch.Base.News.NewsDalFactory.UpdateNews(newsInfo);
                }                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
       
        public object InitAndRedisByListVideoId(string listVideoId)
        {
            try
            {
                var videoList = new MediaServices().GetInitVideoByListVideoId(listVideoId);
                if (videoList != null && videoList.Count() > 0)
                {
                    BoCached.Base.Video.VideoDalFactory.InitAllVideo(videoList);

                    var list = videoList.Select(s => new VideoSearchEntity
                    {
                        Id = s.Id,
                        ZoneId = s.ZoneId,
                        Name = s.Name,
                        ZoneIds = ZoneVideoDal.GetListZoneRelation(s.Id).Select(z => z.Id.ToString()).ToArray(),
                        PlaylistIds = PlaylistDal.GetListByVideoId(s.Id).Select(z => z.Id.ToString()).ToArray(),
                        PlaylistName = PlaylistDal.GetListByVideoId(s.Id).Select(z => z.Name.ToString()).ToArray(),
                        ChannelIds = VideoChannelDal.GetVideoChannelByVideoId(s.Id).Select(z => z.Id.ToString()).ToArray(),
                        ChannelName = VideoChannelDal.GetVideoChannelByVideoId(s.Id).Select(z => z.Name.ToString()).ToArray(),
                        CreatedBy = s.CreatedBy,
                        EditedBy = s.EditedBy,
                        PublishBy = s.PublishBy,
                        PublishDate = s.PublishDate,
                        LastModifiedBy = s.LastModifiedBy,
                        LastModifiedDate = s.LastModifiedDate,
                        CreatedDate = s.CreatedDate,
                        DistributionDate = s.DistributionDate,
                        EditedDate = s.EditedDate,
                        Mode = s.Mode,
                        Status = s.Status,
                        Views = s.Views,
                        ZoneName = s.ZoneName,
                        Avatar = s.Avatar,
                        HtmlCode = s.HtmlCode,
                        KeyVideo = s.KeyVideo,
                        ParentId = s.ParentId,
                        Type = s.Type,
                        Duration = s.Duration,
                        ListType = VideoDal.GetListVideoByParentId(s.Id).Select(z => z.Type.ToString()).ToArray(),
                        Namespace= s.Namespace ?? string.Empty
                    }).ToList();
                    BoSearch.Base.Video.VideoDalFactory.InitAllVideo(list);
                }
                return new { status=true,Count= videoList.Count(), data = "Success" };
            }
            catch (Exception ex)
            {
                return new { status = false, Count = 0, data=ex.Message }; ;
            }
        }

        public object InitEsRedisByListPlayListId(string listPlayListId)
        {
            try
            {
                var playList = new MediaServices().InitEsRedisByListPlayListId(listPlayListId);
                if (playList != null && playList.Count() > 0)
                {
                    BoCached.Base.PlayList.PlayListDalFactory.InitAllPlayList(playList);

                    var list = playList.Select(s => new PlayListSearchEntity
                    {
                        Id = s.Id,
                        ZoneId = s.ZoneId,
                        Name = s.Name,
                        EditedDate = s.EditedDate,
                        FollowCount = s.FollowCount,
                        Mode = s.Mode,
                        PublishedDate = s.PublishedDate,
                        VideoCount = s.VideoCount,
                        VideoCountNotPublish = 0,
                        ZoneIds = ZoneVideoDal.GetListZoneRelationByPlayListId(s.Id).Select(z => z.Id.ToString()).ToArray(),
                        CreatedBy = s.CreatedBy,
                        LastModifiedBy = s.LastModifiedBy,
                        EditedBy = s.EditedBy,
                        PublishedBy = s.PublishedBy,
                        CreatedDate = s.CreatedDate,
                        DistributionDate = s.DistributionDate,
                        LastModifiedDate = s.LastModifiedDate,
                        Status = s.Status,
                        Avatar = s.Avatar,
                        ZoneName = s.ZoneName
                    }).ToList();
                    BoSearch.Base.PlayList.PlayListDalFactory.InitAllPlayList(list);
                }
                return new { status = true, Count = playList.Count(), data = "Success" };
            }
            catch (Exception ex)
            {
                return new { status = false, Count = 0, data = ex.Message }; ;
            }
        }
       
        public bool DeleteNewsByNewsIdRedis(string newsId)
        {
            try
            {                
                return BoCached.Base.News.NewsDalFactory.DeleteNewsByNewsIdRedis(newsId);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }    
}