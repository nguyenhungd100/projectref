﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.NewsCacheMonitor;
using ChannelVN.CMS.BO.Base.NewsMobileStream;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsMobileStream;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.BO.Base.NewsMaskOnline;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.Entity.Base.Security;
using System.Threading.Tasks;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.Entity.Base.Nodejs;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using ChannelVN.SocialNetwork.Entity;
using System.Runtime.Serialization;
using ChannelVN.CMS.BO.Base.NewsWarning;
using ChannelVN.CMS.Entity.Base.NewsWarning;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.WebApi.Models;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.SocialNetwork.BO;

namespace ChannelVN.CMS.WebApi.Services.Base
{
    public class NewsServices
    {
        #region NewsExtension

        public NewsExtensionEntity GetNewsExtensionValue(long newsId, EnumNewsExtensionType type)
        {
            return NewsBo.GetNewsExtensionValue(newsId, type);
        }
        public int GetNewsExtensionMaxValue(long newsId, EnumNewsExtensionType type)
        {
            return NewsBo.GetNewsExtensionMaxValue(newsId, (int)type);
        }
        public WcfActionResponse SetNewsExtensionValue(long newsId, EnumNewsExtensionType type, string value)
        {

            var errorCode = NewsBo.SetNewsExtensionValue(newsId, type, value);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                var list = NewsExtensionBo.GetByNewsId(newsId);
                var existData = list.Where(n => n.NewsId == newsId && n.Type == (int)type);
                if (existData.Count() > 0)
                {
                    foreach (var item in existData)
                    {
                        item.Value = value;
                    }
                }
                else {
                    list.Add(new NewsExtensionEntity()
                    {
                        NewsId = newsId,
                        Type = (int)type,
                        Value = value
                    });
                }

                NewsExtensionBo.SetListCachedByNewsId(newsId, list);

                //push cd update newsextension ->cho appbot chay               
                try
                {
                    var jsonKey = "{\"NewsId\":" + newsId + ",\"Type\":" + type + "}";
                    dynamic data = new System.Dynamic.ExpandoObject();
                    data.NewsId = newsId;
                    data.Type = type;
                    data.Value = value;
                    var strPram = NewtonJson.Serialize(data, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updatenewsextension", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }

                return WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "Success.");
            }            

            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        //tham dinh
        //public WcfActionResponse SendExpertInNewsExtension(int expertId, string listNewsId, string note="")
        //{
        //    var newsExtension = new List<NewsExtensionEntity>();
        //    var sendDate = DateTime.Now;
        //    var listIds = listNewsId.Split(',').ToList();
        //    foreach (var id in listIds)
        //    {
        //        var newsId = Utility.ConvertToLong(id);
        //        newsExtension.Add(new NewsExtensionEntity {
        //            NewsId= newsId,
        //            Type=(int)EnumNewsExtensionType.ApproveExpertId,
        //            Value= expertId.ToString()
        //        });
        //        newsExtension.Add(new NewsExtensionEntity
        //        {
        //            NewsId = newsId,
        //            Type = (int)EnumNewsExtensionType.ApproveExpertStatus,
        //            Value = ((int)EnumApproveExpertStatus.WaitingExpert).ToString()
        //        });
        //        newsExtension.Add(new NewsExtensionEntity
        //        {
        //            NewsId = newsId,
        //            Type = (int)EnumNewsExtensionType.ApproveExpertSendDate,
        //            Value = sendDate.ToString("yyyy-MM-ddTHH:mm:ssZ")
        //        });
        //        newsExtension.Add(new NewsExtensionEntity
        //        {
        //            NewsId = newsId,
        //            Type = (int)EnumNewsExtensionType.ApproveExpertNote,
        //            Value = note
        //        });                
        //    }

        //    var errorCode = NewsBo.SendExpertInNewsExtension(newsExtension);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        var task = Task.Run(() =>
        //        {
        //            try
        //            {
        //                foreach (var objE in newsExtension)
        //                {
        //                    var list = NewsExtensionBo.GetByNewsId(objE.NewsId);
        //                    var existData = list.Where(n => n.NewsId == objE.NewsId && n.Type == objE.Type);
        //                    if (existData.Count() <= 0)
        //                    {
        //                        list.Add(new NewsExtensionEntity()
        //                        {
        //                            NewsId = objE.NewsId,
        //                            Type = objE.Type,
        //                            Value = objE.Value
        //                        });

        //                        NewsExtensionBo.SetListCachedByNewsId(objE.NewsId, list);

        //                        //push cd update newsextension
        //                        try
        //                        {
        //                            var jsonKey = "{\"NewsId\":" + objE.NewsId + ",\"Type\":" + objE.Type + "}";
        //                            var strPram = NewtonJson.Serialize(objE, "yyyy-MM-dd'T'HH:mm:ss");
        //                            ContentDeliveryServices.PushToDataCD(strPram, "updatenewsextension", string.Empty, jsonKey);
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            Logger.WriteLog(Logger.LogType.Error, ex.ToString());
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.WriteLog(Logger.LogType.Fatal, "SendExpertInNewsExtension => Task error => " + ex);
        //            }
        //        });
        //        try
        //        {
        //            task.Wait(TimeSpan.FromSeconds(3));
        //        }
        //        catch { }                
        //    }
        //    WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(expertId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

        //    return responseData;
        //}
        public WcfActionResponse ReSendExpertInNewsExtension(int expertId, int expireDate, string note = "")
        {           
            var errorCode = NewsBo.ReSendExpertInNewsExtension(expertId, expireDate, note);
            
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(expertId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        //public WcfActionResponse ConfirmExpertInNewsExtension(long newsId, string accountName)
        //{
        //    var newsExtension = new NewsExtensionEntity
        //    {
        //        NewsId = newsId,
        //        Type = (int)EnumNewsExtensionType.ApproveExpertStatus,
        //        Value = ((int)EnumApproveExpertStatus.ConfirmExpert).ToString()
        //    };

        //    var errorCode = NewsBo.ConfirmExpertInNewsExtension(newsExtension);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        var list = NewsExtensionBo.GetByNewsId(newsId);
        //        var existData = list.Where(n => n.NewsId == newsId && n.Type == newsExtension.Type);
        //        if (existData.Count() > 0)
        //        {
        //            foreach (var item in existData)
        //            {
        //                item.Value = newsExtension.Value;
        //            }
        //        }
        //        else {
        //            list.Add(new NewsExtensionEntity()
        //            {
        //                NewsId = newsId,
        //                Type = newsExtension.Type,
        //                Value = newsExtension.Value
        //            });
        //        }

        //        NewsExtensionBo.SetListCachedByNewsId(newsId, list);

        //        //push cd update newsextension
        //        try
        //        {
        //            var jsonKey = "{\"NewsId\":" + newsId + ",\"Type\":" + newsExtension.Type + "}";
        //            var strPram = NewtonJson.Serialize(newsExtension, "yyyy-MM-dd'T'HH:mm:ss");
        //            ContentDeliveryServices.PushToDataCD(strPram, "updatenewsextension", string.Empty, jsonKey);
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.WriteLog(Logger.LogType.Error, ex.ToString());
        //        }
        //    }
        //    WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

        //    return responseData;
        //}
        //public WcfActionResponse ReturnExpertInNewsExtension(long newsId, string note)
        //{
        //    var newsExtension = new List<NewsExtensionEntity>();

        //    var newsExtensionStatus = new NewsExtensionEntity
        //    {
        //        NewsId = newsId,
        //        Type = (int)EnumNewsExtensionType.ApproveExpertStatus,
        //        Value = ((int)EnumApproveExpertStatus.ReturnExpert).ToString()
        //    };
        //    newsExtension.Add(newsExtensionStatus);

        //    if (!string.IsNullOrEmpty(note))
        //    {
        //        var newsExtensionNote = new NewsExtensionEntity
        //        {
        //            NewsId = newsId,
        //            Type = (int)EnumNewsExtensionType.ApproveExpertNote,
        //            Value = note
        //        };
        //        newsExtension.Add(newsExtensionNote);
        //    }

        //    var errorCode = NewsBo.ReturnExpertInNewsExtension(newsExtension);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        var task = Task.Run(() =>
        //        {
        //            try
        //            {
        //                foreach (var objE in newsExtension)
        //                {
        //                    var list = NewsExtensionBo.GetByNewsId(objE.NewsId);
        //                    var existData = list.Where(n => n.NewsId == objE.NewsId && n.Type == objE.Type);
        //                    if (existData.Count() <= 0)
        //                    {
        //                        list.Add(new NewsExtensionEntity()
        //                        {
        //                            NewsId = objE.NewsId,
        //                            Type = objE.Type,
        //                            Value = objE.Value
        //                        });

        //                        NewsExtensionBo.SetListCachedByNewsId(objE.NewsId, list);

        //                        //push cd update newsextension
        //                        try
        //                        {
        //                            var jsonKey = "{\"NewsId\":" + objE.NewsId + ",\"Type\":" + objE.Type + "}";
        //                            var strPram = NewtonJson.Serialize(objE, "yyyy-MM-dd'T'HH:mm:ss");
        //                            ContentDeliveryServices.PushToDataCD(strPram, "updatenewsextension", string.Empty, jsonKey);
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            Logger.WriteLog(Logger.LogType.Error, ex.ToString());
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.WriteLog(Logger.LogType.Fatal, "SendExpertInNewsExtension => Task error => " + ex);
        //            }
        //        });
        //        try
        //        {
        //            task.Wait(TimeSpan.FromSeconds(3));
        //        }
        //        catch { }
        //    }
        //    WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

        //    return responseData;
        //}
        public List<NewsSimpleExpertEntity> ListNewsByExpertId(int expertId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsExtensionBo.ListNewsByExpertId(expertId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsSimpleExpertEntity> ListNewsNotByExpert(string account, int topicId, int zoneId, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsExtensionBo.ListNewsNotByExpert(account, topicId, zoneId, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsSimpleExpertEntity> ListNewsAllByExpert(int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsExtensionBo.ListNewsAllByExpert(expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public WcfResponseData DeleteNewsExtensionByNewsId(long newsId)
        {
            var errorCode = NewsExtensionBo.DeleteByNewsId(newsId);
            if (errorCode)
            {
                return WcfResponseData.CreateSuccessResponse();
            }
            return WcfResponseData.CreateErrorResponse("Error Delete NewsExtention");
        }
        public List<NewsExtensionEntity> GetNewsExtensionByNewsId(long newsId)
        {
            return NewsBo.GetNewsExtensionByNewsId(newsId);
        }
        public List<NewsExtensionEntity> GetNewsExtensionByListNewsId(string listNewsId)
        {
            return NewsBo.GetNewsExtensionByListNewsId(listNewsId);
        }
        public List<NewsExtensionEntity> GetNewsExtensionByTypeAndVaue(EnumNewsExtensionType type, string value)
        {
            return NewsBo.GetNewsExtensionByTypeAndVaue(type, value);
        }
        public List<NewsExtensionEntity> ListNewsExtensionByIds(string ids, int type)
        {
            return NewsExtensionBo.ListNewsExtensionByIds(ids, type);
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByNewsExtensionAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return NewsExtensionBo.InitRedisByNewsExtensionAsync(name, startDate, endDate, pageSize, action);
        }
        #endregion

        #region NewsInExpire
        public WcfActionResponse SendNewsInExpert(string accountName, int expertId, string listNewsId, string note = "")
        {
            var newsInExpert = new List<NewsInExpertEntity>();
            var sendDate = DateTime.Now;
            var listIds = listNewsId.Split(',').ToList();
            foreach (var id in listIds)
            {
                var newsId = Utility.ConvertToLong(id);
                newsInExpert.Add(new NewsInExpertEntity
                {
                    NewsId = newsId,
                    ExpertId = expertId,
                    Status = (int)EnumApproveExpertStatus.WaitingExpert,
                    SendDate= DateTime.Now,
                    Note=note,
                    AccountName=accountName
                });                
            }

            var errorCode = NewsInExpertBo.SendNewsInExpert(newsInExpert);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //push cd
                try
                {
                    var jsonKey = "{\"ExpertId\":" + expertId + "}";
                    var strPram = NewtonJson.Serialize(new { ListNewsId= listNewsId, Note=note }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "sendnewsexpert", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }
            }
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(expertId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public WcfActionResponse ReSendNewsInExpert(string accountName, int expertId, int expireDate, string note, long newsId)
        {
            var errorCode = NewsInExpertBo.ReSendNewsInExpert(accountName, expertId, expireDate, note, newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //push cd
                try
                {
                    var jsonKey = "{\"ExpertId\":" + expertId + ",\"NewsId\":" + newsId + "}";
                    var strPram = NewtonJson.Serialize(new { ExpireDate = expireDate, Note = note }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "remindnewsexpert", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }
            }
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(expertId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public WcfActionResponse RecoverNewsInExpert(string account, string listNewsId, string note = "")
        {
            //fomat lai listnewsid
            var listid = listNewsId.Split(new string[] { ",", ";"},StringSplitOptions.RemoveEmptyEntries);
            listNewsId = string.Join(",", listid);

            var errorCode = NewsInExpertBo.RecoverNewsInExpert(account, listNewsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //Logger.WriteLog(Logger.LogType.Trace, "RecoverNewsExpert => ListNewsId: "+ listNewsId + " ;Account: "+ account + " ;Note: "+note);
                //push cd
                try
                {
                    var jsonKey = "{\"ListNewsId\":\"" + listNewsId + "\",\"Status\":0}";
                    var strPram = NewtonJson.Serialize(new { ListNewsId = listNewsId, Note = note }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "recovernewsexpert", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }
            }
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(listNewsId, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public WcfActionResponse ConfirmNewsInExpert(long newsId, int expertId, string accountName)
        {
            var newsExpert = new NewsInExpertEntity
            {
                NewsId = newsId,
                ExpertId=expertId,
                Status = (int)EnumApproveExpertStatus.ConfirmExpert,
                ConfirmDate = DateTime.Now,
                AccountName= accountName
            };

            var errorCode = NewsInExpertBo.ConfirmNewsInExpert(newsExpert);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //push cd
                try
                {
                    var jsonKey = "{\"ExpertId\":" + expertId + ",\"NewsId\":" + newsId + "}";
                    var strPram = NewtonJson.Serialize(new { AccountName = accountName }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "confirmnewsexpert", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }
            }
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public WcfActionResponse ReturnNewsInExpert(string accountName, long newsId, int expertId, string note)
        {            
            var newsExpert = new NewsInExpertEntity
            {
                NewsId = newsId,
                ExpertId=expertId,
                Status = (int)EnumApproveExpertStatus.ReturnExpert,
                Note = note,
                ReturnDate=DateTime.Now,
                AccountName= accountName
            };
            
            var errorCode = NewsInExpertBo.ReturnNewsInExpert(newsExpert);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //push cd
                try
                {
                    var jsonKey = "{\"ExpertId\":" + expertId + ",\"NewsId\":" + newsId + "}";
                    var strPram = NewtonJson.Serialize(new { Note = note }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "returnnewsexpert", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                }
            }
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public List<NewsSimpleExpertEntity> ListNewsInExpertByExpertId(int expertId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsInExpertBo.ListNewsInExpertByExpertId(expertId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsSimpleExpertEntity> ListNewsInExpertNotByExpert(string account, int topicId, int zoneId, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsInExpertBo.ListNewsInExpertNotByExpert(account, topicId, zoneId, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsSimpleExpertEntity> ListNewsInExpertAllByExpert(int expertId, int topicId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsInExpertBo.ListNewsInExpertAllByExpert(expertId, topicId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsInExpertEntity> ListNewsExpertByIds(string ids)
        {
            return NewsInExpertBo.ListNewsExpertByIds(ids);
        }
        public WcfActionResponse Expert_UpdateBody(ExpertNewsEntity news)
        {
            try
            {
                var errorCode = NewsBo.Expert_UpdateBody(news);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);                
                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }

        public WcfActionResponse Confirm_UpdateBody(ExpertNewsEntity news)
        {
            try
            {
                var errorCode = NewsBo.Confirm_UpdateBody(news);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    //push CD
                    try
                    {
                        var jsonKey = "{\"NewsId\":" + news.NewsId + "}";
                        var strParamValue = NewtonJson.Serialize(news, "yyyy-MM-dd'T'HH:mm:ss");
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatebodynewsexpert", string.Empty, jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                    }
                }
                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }
        #endregion

        #region For News

        #region Update & Insert 

        //@OK
        public virtual WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions, string action)
        {
            try
            {
                var newEncryptNewsId = string.Empty;
                var newsId = 0L;
                var errorCode = NewsBo.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, ref newsId, ref newEncryptNewsId, new List<string>(authorList), newsChildOrder, sourceId, newsExtensions, action);

                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public virtual WcfActionResponse CrawlerNews(NewsEntity news)
        {
            try
            {
                var newEncryptNewsId = string.Empty;
                var newsId = 0L;
                var errorCode = NewsBo.CrawlerNews(news);

                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public virtual WcfActionResponse AppBotSaveNews(NewsEntity news, string action)
        {
            try
            {
                long id = 0L; 
                var errorCode = NewsBo.AppBotSaveNews(news, action, ref id);

                if (errorCode == ErrorMapping.ErrorCodes.Success && news.Status == (int)NewsStatus.Published)
                {
                    if (id > 0)
                        news.Id = id;
                    CmsTrigger.Instance.OnUpdateNewsWhichPublished(news.Id);
                }

                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(news.Id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);                
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public virtual WcfActionResponse AppCrawlerInitNewsDB(NewsEntity news)
        {
            try
            {                
                var errorCode = NewsBo.AppCrawlerInitNewsDB(news);
                
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(news.Id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        //@OK
        public WcfActionResponse DeleteNews(long newsId, string userName)
        {
            try
            {
                var errorCode = NewsBo.DeleteNews(newsId, userName);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        //@OK
        public virtual WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions, bool isNotify=false)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = NewsBo.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, isRebuildLink, ref newsStatus, new List<string>(authorList), newsChildOrder, sourceId, publishedContent, newsExtensions);

                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);
                if (errorCode == ErrorMapping.ErrorCodes.Success && newsStatus == (int)NewsStatus.Published)
                {
                    CmsTrigger.Instance.OnUpdateNewsWhichPublished(news.Id);
                    //notify for app
                    news.Status = newsStatus;
                    news.ZoneId = zoneId;
                    AppLogNotifyServices.PushNotifyForApp(news, isNotify);

                    if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1) {
                        CmsTrigger.Instance.OnUpdateNewsWhichPublishedToAdmicro(news.Id);
                    }
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        //@OK
        public WcfActionResponse UpdateNewsAvatar(long newsId, string avatar, int position)
        {
            var newsStatus = 0;
            var errorCode = NewsBo.UpdateNewsAvatar(newsId, avatar, position);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            var newsForValidate = GetNewsForValidateById(newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success && newsForValidate.Status == (int)NewsStatus.Published)
            {
                CmsTrigger.Instance.OnUpdateNewsWhichPublished(newsId);

                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                {
                    CmsTrigger.Instance.OnUpdateNewsWhichPublishedToAdmicro(newsId);
                }
            }
            return responseData;
        }

        //@OK
        public WcfActionResponse UpdateOnlyTitle(long newsId, string newsTitle, string currentUsername)
        {
            var errorCode = NewsBo.UpdateOnlyTitle(newsId, newsTitle, currentUsername);

            var newsForValidate = GetNewsForValidateById(newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success && newsForValidate.Status == (int)NewsStatus.Published)
            {
                CmsTrigger.Instance.OnUpdateNewsWhichPublished(newsId);

                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                {
                    CmsTrigger.Instance.OnUpdateNewsWhichPublishedToAdmicro(newsId);
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        //@OK
        public WcfActionResponse UpdateDetail(long newsId, string newsTitle, string sapo, string body, string currentUsername)
        {
            var errorCode = NewsBo.UpdateDetail(newsId, newsTitle, sapo, body, currentUsername);

            var newsForValidate = GetNewsForValidateById(newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success && newsForValidate.Status == (int)NewsStatus.Published)
            {
                CmsTrigger.Instance.OnUpdateNewsWhichPublished(newsId);

                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                {
                    CmsTrigger.Instance.OnUpdateNewsWhichPublishedToAdmicro(newsId);
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateNewsByErrorCheck(NewsEntity news, string tagIdList, string currentUsername)
        {
            var errorCode = NewsBo.UpdateNewsByErrorCheck(news, tagIdList, currentUsername);

            var newsForValidate = GetNewsForValidateById(news.Id);
            if (errorCode == ErrorMapping.ErrorCodes.Success && newsForValidate.Status == (int)NewsStatus.Published)
            {
                CmsTrigger.Instance.OnUpdateNewsWhichPublished(news.Id);

                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                {
                    CmsTrigger.Instance.OnUpdateNewsWhichPublishedToAdmicro(news.Id);
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(news.Id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        //@OK
        public WcfActionResponse UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            var errorCode = NewsBo.UpdateDetailSimple(newsId, newsTitle, avatar, avatar2, avatar3, avatar4, avatar5, accountName);

            var newsForValidate = GetNewsForValidateById(newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success && newsForValidate.Status == (int)NewsStatus.Published)
            {
                CmsTrigger.Instance.OnUpdateNewsWhichPublished(newsId);

                if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                {
                    CmsTrigger.Instance.OnUpdateNewsWhichPublishedToAdmicro(newsId);
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region NewsPr

        public NewsPrEntity GetNewsPrByNewsId(long newsId)
        {
            return NewsPrBo.GetNewsPrByNewsId(newsId);
        }

        public NewsPrEntity GetNewsPrByContentId(long contentId)
        {
            return NewsPrBo.GetNewsPrByContentId(contentId);
        }

        public WcfActionResponse RecieveNewsPrIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId, ref long newsId)
        {
            //var newsId = 0L;
            var errorCode = NewsPrBo.RecieveNewsPrIntoCms(newsInfo, newsPrInfo, listNewsRelationId, ref newsId);            
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,ErrorMapping.Current[errorCode]);

            //khong can ban cd -> NewsPr ko xuat ban
            //if (responseData.Success)
            //{
            //    try {
            //        var jsonKey = "{\"Id\":" + newsId + "}";
            //        dynamic data = new System.Dynamic.ExpandoObject();
            //        data.NewsId = newsId;
            //        data.News = newsInfo;
            //        data.NewsPr = newsPrInfo;
            //        var strPram = NewtonJson.Serialize(data, "yyyy-MM-dd'T'HH:mm:ss");
            //        ContentDeliveryServices.PushToDataCD(strPram, "RecieveNewsPr", string.Empty, jsonKey);
            //    }
            //    catch(Exception ex)
            //    {
            //        Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            //    }
            //}

            return responseData;
        }

        public WcfActionResponse RecieveNewsPrV2IntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId, List<NewsExtensionEntity> newsExtensions, ref long newsId)
        {
            //var newsId = 0L;
            var errorCode = NewsPrBo.RecieveNewsPrV2IntoCms(newsInfo, newsPrInfo, listNewsRelationId, newsExtensions, ref newsId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            
            return responseData;
        }

        public WcfActionResponse RecieveFeedbackNewsIntoCms(NewsEntity newsInfo, ref long newsId)
        {
            var errorCode = NewsBo.RecieveFeedbackNewsIntoCms(newsInfo, ref newsId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateNewsPrInfo(NewsPrEntity newsPrInfo)
        {
            var errorCode = NewsPrBo.UpdateNewsPrInfo(newsPrInfo);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateNewsPrV2Info(NewsPrEntity newsPrInfo, string message)
        {
            var errorCode = NewsPrBo.UpdateNewsPrV2Info(newsPrInfo, message);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                var jsonKey = "{\"NewsId\":\"" + newsPrInfo.NewsId + "\"}";
                var strPram = NewtonJson.Serialize(newsPrInfo, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatenewspr", string.Empty, jsonKey);
            }

            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateNewsPrV2Info_2(NewsPrEntity newsPrInfo, string message)
        {
            var errorCode = NewsPrBo.UpdateNewsPrV2Info_2(newsPrInfo, message);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                var jsonKey = "{\"NewsId\":\"" + newsPrInfo.NewsId + "\"}";
                var strPram = NewtonJson.Serialize(newsPrInfo, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatenewspr", string.Empty, jsonKey);
            }

            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse RecieveNewsPrRetailIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId)
        {
            var newsId = 0L;
            var errorCode = NewsPrBo.RecieveNewsPrRetailIntoCms(newsInfo, newsPrInfo, listNewsRelationId, ref newsId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public NewsPrCurrentDateEntity[] GetListNewsPrCurrentDate(DateTime Date, bool UnPublish, int PageIndex, int PageSize, ref int TotalRows)
        {
            return NewsPrBo.GetListNewsPrCurrentDate(Date, UnPublish, PageIndex, PageSize, ref TotalRows).ToArray();
        }
        #endregion

        #region Status flow

        public WcfActionResponse Send(long newsId, string currentUsername, bool isSendOver, string receiver)
        {
            try
            {
                var errorCode = NewsBo.Send(newsId, currentUsername, isSendOver, receiver);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Action Send => " + ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi gửi tin");
            }
        }
        public WcfActionResponse Receive(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.Receive(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi nhận tin");
            }
        }
        public WcfActionResponse Release(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.Release(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi nhả tin");
            }
        }
        public WcfActionResponse Publish(long newsId, int newsPositionTypeForHome, int newsPositionOrderForHome, int newsPositionTypeForList, int newsPositionOrderForList, string currentUsername, string publishedContent, bool isNotify)
        {
            try
            {
                var newsUrl = "";
                var errorCode = NewsBo.Publish(newsId, newsPositionTypeForHome, newsPositionOrderForHome, newsPositionTypeForList, newsPositionOrderForList, currentUsername, ref newsUrl, publishedContent);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsUrl, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    CmsTrigger.Instance.OnPublishNews(newsId, 0, isNotify);
                    
                    if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                    {
                        CmsTrigger.Instance.OnPublishNewsToAdmicro(newsId, 0);
                    }
                }
                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi xuất bản tin");
            }
        }
        public WcfActionResponse Unpublish(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.Unpublish(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    CmsTrigger.Instance.OnUnpublishNews(newsId);
                    if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                    {
                        CmsTrigger.Instance.OnUnpublishNewsToAdmicro(newsId, 0);
                    }
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi gỡ tin");
            }
        }
        public WcfActionResponse Return(long newsId, string currentUsername, bool isSendOver, string receiver, string note)
        {
            try
            {
                var errorCode = NewsBo.Return(newsId, currentUsername, isSendOver, receiver, note);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,"Có lỗi khi trả lại tin"+ ex.Message.ToString());
            }
        }
        public WcfActionResponse ReturnToCooperator(long newsId, string currentUsername, string note)
        {
            try
            {
                var errorCode = NewsBo.ReturnToCooperator(newsId, currentUsername, note);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi trả lại tin");
            }
        }
        public WcfActionResponse GetBack(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.GetBack(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi rút lại tin");
            }
        }
        public WcfActionResponse ReceiveFromReturnStore(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.ReceiveFromReturnStore(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi nhận lại tin");
            }
        }
        public WcfActionResponse ReceiveFromCooperatorStore(long newsId, string currentUsername, string listZoneId)
        {
            try
            {
                var errorCode = NewsBo.ReceiveFromCooperatorStore(newsId, currentUsername, listZoneId);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi nhận lại tin từ Cộng tác viên");
            }
        }
        public WcfActionResponse MoveToTrash(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.MoveToTrash(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                var newsForValidate = GetNewsForValidateById(newsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success && newsForValidate.Status == (int)NewsStatus.Published)
                {
                    CmsTrigger.Instance.OnUnpublishNews(newsId);
                    if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                    {
                        CmsTrigger.Instance.OnUnpublishNewsToAdmicro(newsId, 0);
                    }
                }

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi xóa tin");
            }
        }
        public WcfActionResponse ReceiveFromTrash(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.ReceiveFromTrash(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi khôi phục tin");
            }
        }
        public WcfActionResponse ReceiveCrawlerNews(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.ReceiveCrawlerNews(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi lấy tin crawler");
            }
        }
        public WcfActionResponse Forward(long newsId, string currentUsername, string receiver)
        {
            try
            {
                var errorCode = NewsBo.Forward(newsId, currentUsername, receiver);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi chuyển tiếp tin");
            }
        }

        public WcfActionResponse RemoveNews(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.RemoveNews(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                //var newsForValidate = GetNewsForValidateById(newsId);
                //if (errorCode == ErrorMapping.ErrorCodes.Success && newsForValidate.Status == (int)NewsStatus.Published)
                //{
                //    CmsTrigger.Instance.OnUnpublishNews(newsId);
                //    if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushToAdmicroCD") == 1)
                //    {
                //        CmsTrigger.Instance.OnUnpublishNewsToAdmicro(newsId, 0);
                //    }
                //}

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi xóa tin");
            }
        }
        #endregion

        #region Search

        public List<NewsSearchEntity> ListNewsByStatus(string username, int typeOfCheck, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, int newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var zoneIds = string.Empty;
            var isRole = 0;
            var listUserPermission = new List<UserPermissionEntity>();
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();                
            }
            //check quyền trả list tin
            switch (newsStatus)
            {
                #region //bài của tôi
                case NewsStatus.Unknow:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.All:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #endregion
                #region //chỉ quyền phong viên mới thấy list này
                case NewsStatus.Temporary:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId== (int)EnumPermission.ArticleReporter).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền biên tập viên mới thấy list này
                case NewsStatus.WaitForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        username = string.Empty;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForEdit:
                    filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    break;
                case NewsStatus.ReturnedToEditor:                    
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        username = string.Empty;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thư ký mới thấy list này
                case NewsStatus.WaitForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditorialSecretary:                    
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) || listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck))
                    {
                        username = string.Empty;

                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }

                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck) || typeOfCheck > 0)
                        {
                            if (typeOfCheck == 1 || typeOfCheck == 2)//1.cho soat loi //2.da soat loi
                            {
                                username = currentUserName;
                                filterFieldForUsername = NewsFilterFieldForUsername.ErrorCheckedBy;

                                if (null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleErrorCheck).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                        }                        
                    }
                    else
                    {
                        username = currentUserName;
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    }                    
                    break;
                case NewsStatus.WaitForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            username = string.Empty;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            username = string.Empty;
                        else
                            return null;
                    }
                    if (null != listUserPermission && listUserPermission.Count > 0)
                    {
                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                        
                    }
                    break;
                case NewsStatus.ReceivedForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {                        
                        username = string.Empty;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                case NewsStatus.MovedToTrash:
                    filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                    break;
                case NewsStatus.ReturnToCooperator:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #region //List trả lại đích danh BTV hoặc TK
                case NewsStatus.ReturnedToMyEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                    break;
                #endregion
                #region //List crawler
                case NewsStatus.CrawlerNews:
                case NewsStatus.CrawlerByVccorp:
                    username = string.Empty;
                    zoneIds = "";
                    break;
                #endregion
            };

            return NewsBo.ListNewsByStatus(username, typeOfCheck, zoneIds, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
        }

        public List<NewsSearchEntity> ListNewsByMultiStatus(string keyword, string author, string zoneIds, DateTime fromDate, DateTime toDate, string username, string listStatus, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            return NewsBo.ListNewsByMultiStatus(keyword, author, zoneIds, fromDate, toDate, username, listStatus, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
        }

        public List<NewsSearchEntity> ListNewsByMultiStatusSubsite(string keyword, string author, string zoneIds, DateTime fromDate, DateTime toDate, string username, string listStatus, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            return NewsBo.ListNewsByMultiStatusSubsite(keyword, author, zoneIds, fromDate, toDate, username, listStatus, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
        }

        public List<NewsSearchEntity> ListNewsProcessing(string keyword, string author, string zoneIds, DateTime fromDate, DateTime toDate, string username, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            return NewsBo.ListNewsProcessing(keyword, author, zoneIds, fromDate, toDate, username, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
        }

        public List<NewsSearchEntity> ListNewsProcessingSubsite(string keyword, string author, string zoneIds, DateTime fromDate, DateTime toDate, string username, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            return NewsBo.ListNewsProcessingSubsite(keyword, author, zoneIds, fromDate, toDate, username, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
        }

        public List<NewsSearchEntity> ListNewsIsPr(string keyword, string username, string zoneIds, NewsStatus newsStatus, List<int> filterFieldForUsername, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {            
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (string.IsNullOrEmpty(zoneIds)) {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                }
            }            

            #region quyền biên tập viên
            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
            {
                filterFieldForUsername.Add(3);
            }
            #endregion

            #region quyền thư ký
            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
            {
                filterFieldForUsername.Add(6);                
            }

            #endregion

            return NewsBo.ListNewsIsPr(keyword, username, zoneIds, newsStatus, filterFieldForUsername, fromDate, toDate, pageIndex, pageSize, ref totalRows);
        }

        public List<NewsSearchEntity> ListNewsIsPrSubsite(string keyword, string username, string zoneIds, NewsStatus newsStatus, List<int> filterFieldForUsername, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else
                    {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                }
            }

            #region quyền biên tập viên
            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
            {
                filterFieldForUsername.Add(3);
            }
            #endregion

            #region quyền thư ký
            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
            {
                filterFieldForUsername.Add(6);
            }

            #endregion

            return NewsBo.ListNewsIsPrSubsite(keyword, username, zoneIds, newsStatus, filterFieldForUsername, fromDate, toDate, pageIndex, pageSize, ref totalRows);
        }


        public List<NewsSearchEntity> ListNewsPublishByDate(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            return NewsBo.ListNewsPublishByDate(fromDate, toDate, pageIndex, pageSize, ref totalRows);
        }
        public List<NewsRoyaltiesV2OutEntity> ListNewsPublishForRoyalties(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            return NewsBo.ListNewsPublishForRoyalties(fromDate, toDate, pageIndex, pageSize, ref totalRows);
        }
        public List<NewsSearchEntity> ListNewsPublish(string accountName, DateTime fromDate, DateTime toDate, string zoneIds, int pageIndex, int pageSize, ref int totalRows)
        {
            return NewsBo.ListNewsPublish(accountName, fromDate, toDate, zoneIds, pageIndex, pageSize, ref totalRows);
        }
        public List<NewsSearchEntity> SearchNewsForSeo(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            return NewsBo.SearchNewsForSeo(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);
        }
        public List<NewsSearchViewEntity> ExportNewsPublish(List<NewsSearchEntity> data, int orderby, long viewFrom, long viewTo, int pageIndex, int pageSize, ref int totalRowsView)
        {
            return NewsBo.ExportNewsPublish(data, orderby, viewFrom, viewTo, pageIndex, pageSize, ref totalRowsView);
        }
        public List<NewsSearchEntity> ListMyNews(string username, List<int> filterFieldForUsername, NewsSortExpression sortOrder, int newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var newsStatus = NewsStatus.Unknow;
            var zoneIds = string.Empty;
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (user.IsFullZone)
                {
                    zoneIds = "";
                }
                else {
                    if (null != listUserPermission && listUserPermission.Count > 0)
                    {
                        zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        //them cho trường hợp crawler tin zoneid=0
                        
                    }
                }
            }

            #region quyền biên tập viên
            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
            {
                filterFieldForUsername.Add(3);               
            }
            #endregion

            #region quyền thư ký
            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
            {
                filterFieldForUsername.Add(6);               
            }
           
            #endregion

            return NewsBo.ListMyNews(username, zoneIds, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
        }
        public List<NewsSearchEntity> SearchNews(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows, string createdBy="")
        {
            var isRole = 0;
            var listUserPermission = new List<UserPermissionEntity>();
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();                
            }
            //check quyền trả list tin
            switch (newsStatus)
            {
                #region //bài của tôi
                case NewsStatus.Unknow:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.All:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #endregion
                #region //chỉ quyền phong viên mới thấy list này
                case NewsStatus.Temporary:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleReporter).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền biên tập viên mới thấy list này
                case NewsStatus.WaitForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForEdit:
                    filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    break;
                case NewsStatus.ReturnedToEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thư ký mới thấy list này
                case NewsStatus.WaitForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditorialSecretary:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) || listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }

                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck) || typeOfCheck > 0)
                        {
                            if (typeOfCheck == 1 || typeOfCheck == 2)//1.cho soat loi //2.da soat loi
                            {
                                username = currentUserName;
                                filterFieldForUsername = NewsFilterFieldForUsername.ErrorCheckedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleErrorCheck).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                        }
                    }
                    else
                    {
                        username = currentUserName;
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    }
                    break;
                case NewsStatus.WaitForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            username = string.Empty;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            username = string.Empty;
                        else
                            return null;
                    }
                    if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                    {
                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                        
                    }
                    break;
                case NewsStatus.ReceivedForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;
                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                case NewsStatus.MovedToTrash:
                    filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                    break;
                case NewsStatus.ReturnToCooperator:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #region //List trả lại đích danh BTV hoặc TK
                case NewsStatus.ReturnedToMyEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                            
                        }
                    }
                    else
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                    break;
                #endregion
                #region //List crawler
                case NewsStatus.CrawlerNews:
                case NewsStatus.CrawlerByVccorp:
                    username = string.Empty;
                    zoneIds = "";
                    break;
                #endregion
            };
            
            return NewsBo.SearchNews(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
        }

        public List<NewsSearchEntity> SearchNewsByZoneParent(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows, string createdBy = "")
        {
            var isRole = 0;
            var listUserPermission = new List<UserPermissionEntity>();
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
            }
            //check quyền trả list tin
            switch (newsStatus)
            {
                #region //bài của tôi
                case NewsStatus.Unknow:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.All:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #endregion
                #region //chỉ quyền phong viên mới thấy list này
                case NewsStatus.Temporary:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleReporter).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền biên tập viên mới thấy list này
                case NewsStatus.WaitForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForEdit:
                    filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    break;
                case NewsStatus.ReturnedToEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thư ký mới thấy list này
                case NewsStatus.WaitForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditorialSecretary:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) || listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck))
                    {
                        username = string.Empty;

                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                        }

                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck) || typeOfCheck > 0)
                        {
                            if (typeOfCheck == 1 || typeOfCheck == 2)//1.cho soat loi //2.da soat loi
                            {
                                username = currentUserName;
                                filterFieldForUsername = NewsFilterFieldForUsername.ErrorCheckedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleErrorCheck).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                        }
                    }
                    else
                    {
                        username = currentUserName;
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    }
                    break;
                case NewsStatus.WaitForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            username = string.Empty;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            username = string.Empty;
                        else
                            return null;
                    }
                    if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                    {
                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                    }
                    break;
                case NewsStatus.ReceivedForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;
                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else
                        return null;
                    break;
                #endregion
                case NewsStatus.MovedToTrash:
                    filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                    break;
                case NewsStatus.ReturnToCooperator:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #region //List trả lại đích danh BTV hoặc TK
                case NewsStatus.ReturnedToMyEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                        if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                    else
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                    break;
                #endregion
                #region //List crawler
                case NewsStatus.CrawlerNews:
                case NewsStatus.CrawlerByVccorp:
                    username = string.Empty;
                    zoneIds = "";
                    break;
                    #endregion
            };

            return NewsBo.SearchNewsByZoneParent(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
        }

        public List<NewsSearchEntity> SearchNewsSeoReviewStatus(string keyword, int reviewStatus, string author, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows, string createdBy = "")
        {
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                            //them cho trường hợp crawler tin zoneid=0
                            //zoneIds = "0," + zoneIds;
                        }
                    }
                }
            }
            //check quyền trả list tin
            switch (newsStatus)
            {
                #region //bài của tôi
                case NewsStatus.Unknow:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.All:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #endregion
                #region //chỉ quyền phong viên mới thấy list này
                case NewsStatus.Temporary:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền biên tập viên mới thấy list này
                case NewsStatus.WaitForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        username = string.Empty;
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditor:
                    //filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        username = string.Empty;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thư ký mới thấy list này
                case NewsStatus.WaitForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        username = string.Empty;
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditorialSecretary:
                    //filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        username = string.Empty;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) || listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck))
                    {
                        username = string.Empty;                        
                    }
                    else
                    {
                        username = currentUserName;
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    }
                    break;
                case NewsStatus.WaitForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            username = string.Empty;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            username = string.Empty;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.ReceivedForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        //filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                        //if (isRole == (int)IsRole.EditorialBoard)
                        //{
                        //    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        //}
                        username = string.Empty;
                    }
                    else
                        return null;
                    break;
                #endregion
                case NewsStatus.MovedToTrash:
                    filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                    break;
                case NewsStatus.ReturnToCooperator:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #region //List trả lại đích danh BTV hoặc TK
                case NewsStatus.ReturnedToMyEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    else
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                    break;
                #endregion
                #region //List crawler
                case NewsStatus.CrawlerNews:
                case NewsStatus.CrawlerByVccorp:
                    username = string.Empty;
                    zoneIds = "";
                    break;
                    #endregion
            };

            return NewsBo.SearchNewsSeoReviewStatus(keyword, reviewStatus, author, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
        }

        public List<NewsSearchEntity> SearchNewsSeoReviewStatusSubsite(string keyword, int reviewStatus, string author, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows, string createdBy = "")
        {
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else
                    {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                            //them cho trường hợp crawler tin zoneid=0
                            //zoneIds = "0," + zoneIds;
                        }
                    }
                }
            }
            //check quyền trả list tin
            switch (newsStatus)
            {
                #region //bài của tôi
                case NewsStatus.Unknow:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.All:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #endregion
                #region //chỉ quyền phong viên mới thấy list này
                case NewsStatus.Temporary:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền biên tập viên mới thấy list này
                case NewsStatus.WaitForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        username = string.Empty;
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditor:
                    //filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        username = string.Empty;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thư ký mới thấy list này
                case NewsStatus.WaitForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        username = string.Empty;
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditorialSecretary:
                    //filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        username = string.Empty;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) || listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck))
                    {
                        username = string.Empty;
                    }
                    else
                    {
                        username = currentUserName;
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    }
                    break;
                case NewsStatus.WaitForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            username = string.Empty;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            username = string.Empty;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.ReceivedForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        //filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                        //if (isRole == (int)IsRole.EditorialBoard)
                        //{
                        //    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        //}
                        username = string.Empty;
                    }
                    else
                        return null;
                    break;
                #endregion
                case NewsStatus.MovedToTrash:
                    filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                    break;
                case NewsStatus.ReturnToCooperator:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #region //List trả lại đích danh BTV hoặc TK
                case NewsStatus.ReturnedToMyEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    else
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                    break;
                #endregion
                #region //List crawler
                case NewsStatus.CrawlerNews:
                case NewsStatus.CrawlerByVccorp:
                    username = string.Empty;
                    zoneIds = "";
                    break;
                    #endregion
            };

            return NewsBo.SearchNewsSeoReviewStatusSubsite(keyword, reviewStatus, author, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
        }

        public List<NewsSearchEntity> SearchNewsPublishES(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows, string createdBy = "")
        {
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                }
            }
            //check quyền trả list tin
            switch (newsStatus)
            {
                #region //bài của tôi
                case NewsStatus.Unknow:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.All:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #endregion
                #region //chỉ quyền phong viên mới thấy list này
                case NewsStatus.Temporary:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền biên tập viên mới thấy list này
                case NewsStatus.WaitForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        username = string.Empty;
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditor:
                    //filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        username = string.Empty;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thư ký mới thấy list này
                case NewsStatus.WaitForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        username = string.Empty;
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditorialSecretary:
                    //filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        username = string.Empty;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck))
                        {
                            if (typeOfCheck == 1 || typeOfCheck == 2)//1.cho soat loi //2.da soat loi
                            {
                                username = currentUserName;
                                filterFieldForUsername = NewsFilterFieldForUsername.ErrorCheckedBy;
                            }
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.WaitForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            username = string.Empty;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            username = string.Empty;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.ReceivedForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        //filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                        //if (isRole == (int)IsRole.EditorialBoard)
                        //{
                        //    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        //}
                        username = string.Empty;
                    }
                    else
                        return null;
                    break;
                #endregion
                case NewsStatus.MovedToTrash:
                    filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                    break;
                case NewsStatus.ReturnToCooperator:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #region //List trả lại đích danh BTV hoặc TK
                case NewsStatus.ReturnedToMyEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    else
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                    break;
                    #endregion
            };

            return NewsBo.SearchNewsPublishES(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
        }

        public List<NewsSearchEntity> SearchNewsPublishSubsiteES(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows, string createdBy = "")
        {
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else
                    {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                }
            }
            //check quyền trả list tin
            switch (newsStatus)
            {
                #region //bài của tôi
                case NewsStatus.Unknow:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.All:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #endregion
                #region //chỉ quyền phong viên mới thấy list này
                case NewsStatus.Temporary:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền biên tập viên mới thấy list này
                case NewsStatus.WaitForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        username = string.Empty;
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForEdit:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditor:
                    //filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        username = string.Empty;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thư ký mới thấy list này
                case NewsStatus.WaitForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        username = string.Empty;
                    else
                        return null;
                    break;
                case NewsStatus.ReceivedForPublish:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    else
                        return null;
                    break;
                case NewsStatus.ReturnedToEditorialSecretary:
                    //filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        username = string.Empty;
                    else
                        return null;
                    break;
                #endregion
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        username = string.Empty;
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleErrorCheck))
                        {
                            if (typeOfCheck == 1 || typeOfCheck == 2)//1.cho soat loi //2.da soat loi
                            {
                                username = currentUserName;
                                filterFieldForUsername = NewsFilterFieldForUsername.ErrorCheckedBy;
                            }
                        }
                    }
                    else
                        return null;
                    break;
                case NewsStatus.WaitForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            username = string.Empty;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            username = string.Empty;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.ReceivedForEditorialBoard:
                    if (!user.IsFullPermission)
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    else
                    {
                        if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        else
                            return null;
                    }
                    break;
                case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                    {
                        //filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                        //if (isRole == (int)IsRole.EditorialBoard)
                        //{
                        //    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                        //}
                        username = string.Empty;
                    }
                    else
                        return null;
                    break;
                #endregion
                case NewsStatus.MovedToTrash:
                    filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                    break;
                case NewsStatus.ReturnToCooperator:
                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    break;
                #region //List trả lại đích danh BTV hoặc TK
                case NewsStatus.ReturnedToMyEditor:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                        filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                    else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                        filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                    else
                        filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                    break;
                    #endregion
            };

            return NewsBo.SearchNewsPublishSubsiteES(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
        }


        public List<NewsSearchEntity> SearchNewsPublishForComment(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows, string createdBy = "")
        {
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                }
            }
            //check quyền trả list tin
            switch (newsStatus)
            {                
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ApproveComment) || listCurrentRole.Exists(e => e == (int)EnumPermission.ReceiveComment) || user.IsFullPermission)
                    {
                        username = string.Empty;                        
                    }
                    else
                        return null;
                    break;                
                #endregion
            };

            return NewsBo.SearchNewsPublishES(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
        }

        public List<NewsSearchEntity> SearchNewsPublishForCommentSubsite(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows, string createdBy = "")
        {
            var isRole = 0;
            var listCurrentRole = new List<int>();
            var currentUserName = username;
            var service = new SecurityServices();
            var user = service.GetUserByUsername(username);
            if (user != null)
            {
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                isRole = user.IsRole;
                listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else
                    {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                }
            }
            //check quyền trả list tin
            switch (newsStatus)
            {
                #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                case NewsStatus.Published:
                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ApproveComment) || listCurrentRole.Exists(e => e == (int)EnumPermission.ReceiveComment) || user.IsFullPermission)
                    {
                        username = string.Empty;
                    }
                    else
                        return null;
                    break;
                    #endregion
            };

            return NewsBo.SearchNewsPublishSubsiteES(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
        }


        public List<NewsInListEntity> SearchNewsForNewsPositionES(string keyword, int zoneId, int displayPosition, int priority, bool showBomb, int pageIndex, int pageSize, ref int totalRows)
        {            
            return NewsBo.SearchNewsForNewsPositionES(keyword, zoneId, displayPosition, priority, showBomb, pageIndex, pageSize, ref totalRows);
        }

        public List<NewsInListEntity> SearchNewsForNewsPositionESInDateNow(string keyword, int zoneId, int displayPosition, int priority, int pageIndex, int pageSize, ref int totalRows)
        {
            return NewsBo.SearchNewsForNewsPositionESInDateNow(keyword, zoneId, displayPosition, priority, pageIndex, pageSize, ref totalRows);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByNewsAsync(string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null, int zoneid = 0)
        {
            try
            {
                return NewsBo.InitEsByNewsAsync(name, startPage, startDate, endDate, pageSize, action, zoneid);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByNewsAsync(string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null, int zoneid = 0)
        {
            try
            {
                return NewsBo.InitRedisByNewsAsync(name, startPage, startDate, endDate, pageSize, action, zoneid);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByNewsCheckDuplicateAsync(string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return NewsBo.InitRedisByNewsCheckDuplicateAsync(name, startPage, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public dynamic InitRedisToSql(string username, DateTime startDate, DateTime endDate)
        {
            try
            {
                return NewsBo.InitRedisToSql(username, startDate, endDate);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public NewsInListEntity[] SearchNewsTemp(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsTemp(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsByNewsId(long NewsId)
        {
            var news = NewsBo.SearchNewsByNewsId(NewsId);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsPublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsPublishedForSecretary(keyword, username, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsTemp_PublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsTemp_PublishedForSecretary(keyword, username, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }

        public NewsInListEntity[] SearchNewsPr(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsPr(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsByUsername(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsTemp_ByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsTemp_ByUsername(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }

        //@OK
        public List<NewsSearchEntity> SearchNewsByType(string username, string keyword, string author, NewsType type, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows, string createdBy)
        {
            if (string.IsNullOrEmpty(zoneIds))
            {                
                var listUserPermission = new List<UserPermissionEntity>();                
                var currentUserName = username;
                var service = new SecurityServices();
                var user = service.GetUserByUsername(username);
                if (user != null)
                {
                    listUserPermission = PermissionBo.GetListByUserId(user.Id, true);                                                       
                }

                switch (type)
                {                                      
                    case NewsType.LiveMatch:
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.LiveSportManager).Select(s => s.ZoneId).Distinct().ToArray());
                            
                        }
                        break;                    
                    case NewsType.RollingNews:
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.RollingNewsManager).Select(s => s.ZoneId).Distinct().ToArray());
                            
                        }
                        break;                    
                    case NewsType.Magazine:                        
                    default:
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                            
                        }
                        break;
                }
                if (user != null)
                {
                    if (user.IsFullPermission || user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                }
            }            

                return NewsBo.SearchNewsByType(keyword, author, type, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows, createdBy);
        }

        public List<NewsSearchEntity> SearchNewsByTypeSubsite(string username, string keyword, string author, NewsType type, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows, string createdBy)
        {
            if (string.IsNullOrEmpty(zoneIds))
            {
                var listUserPermission = new List<UserPermissionEntity>();
                var currentUserName = username;
                var service = new SecurityServices();
                var user = service.GetUserByUsername(username);
                if (user != null)
                {
                    listUserPermission = PermissionBo.GetListByUserId(user.Id, true);
                }

                switch (type)
                {
                    case NewsType.LiveMatch:
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.LiveSportManager).Select(s => s.ZoneId).Distinct().ToArray());

                        }
                        break;
                    case NewsType.RollingNews:
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.RollingNewsManager).Select(s => s.ZoneId).Distinct().ToArray());

                        }
                        break;
                    case NewsType.Magazine:
                    default:
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());

                        }
                        break;
                }
                if (user != null)
                {
                    if (user.IsFullPermission || user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                }
            }

            return NewsBo.SearchNewsByTypeSubsite(keyword, author, type, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows, createdBy);
        }


        public List<NewsMagazineEntity> GetListMagazineByType(string keyword, int type, int magazineType, int pageIndex, int pageSize, ref int totalRows)
        {
            var list = new List<NewsMagazineEntity>();
            var data = NewsBo.GetListMagazineByType(keyword, type, magazineType, pageIndex, pageSize, ref totalRows);

            list.AddRange(data.Select(s => new NewsMagazineEntity
            {
                Id = s.Id,
                EncryptId = s.EncryptId,
                Title = s.Title,
                Avatar = s.Avatar,
                NewsExtensions = NewsExtensionBo.GetByNewsId(s.Id)
            }));

            //if (list != null && list.Count > 0)
            //{
            //    var tempData = new List<NewsMagazineEntity>();
            //    if (magazineType == 1)
            //    {
            //        var listSearch = list.Where(w => w.NewsExtensions.Any(e => e.Type != 3001));

            //        tempData.AddRange(listSearch.Select(s => new NewsMagazineEntity
            //        {
            //            Id = s.Id,
            //            EncryptId = s.EncryptId,
            //            Title = s.Title,
            //            Avatar = s.Avatar
            //        }));
            //        return tempData;
            //    }
            //    else if (magazineType == 2)
            //    {
            //        var listSearch = list.Where(w => w.NewsExtensions.Any(e => e.Type == 3001));

            //        tempData.AddRange(listSearch.Select(s => new NewsMagazineEntity
            //        {
            //            Id = s.Id,
            //            EncryptId = s.EncryptId,
            //            Title = s.Title,
            //            Avatar = s.Avatar
            //        }));
            //        return tempData;
            //    }                
            //}

            return list;
        }

        public List<NewsInListEntity> SearchNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, int priority, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {
            //List<NewsInListEntity> news = null;
            ////var data = BoSearch.Base.News.NewsDalFactory.SearchNewsPublishForPosition(zoneId, keyword, displayPosition, pageIndex, pageSize, ref totalRows);
            //if (news == null)
            //{
            //    news = NewsBo.SearchNewsWhichPublished(zoneId, keyword, type, displayPosition, priority, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRows);
            //}           
            return NewsBo.SearchNewsWhichPublished(zoneId, keyword, type, displayPosition, priority, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRows);
        }
        public List<NewsInListEntity> SearchNewsForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {
            var news = NewsBo.SearchNewsForNewsPosition(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToList();
        }
        public NewsInListEntity[] SearchNewsWhichPublishedForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludePositionTypes, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {
            var news = NewsBo.SearchNewsWhichPublishedForNewsPosition(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, excludePositionTypes, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListWithNotifyEntity[] SearchNewsWithNotification(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, int pageIndex, int pageSize, NewsStatus[] statusArray, ref int totalRow)
        {
            var news = NewsBo.SearchNewsWithNotification(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, pageIndex, pageSize, ref totalRow, statusArray);
            return news.ToArray();
        }
        public NewsInListWithNotifyEntity[] SearchNewsWithNotificationV2(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, NewsSortExpression sortOrder, int pageIndex, int pageSize, NewsStatus[] statusArray, ref int totalRow)
        {
            var news = NewsBo.SearchNewsWithNotificationV2(keyword, username, zoneId, fromDate, toDate, sortOrder, pageIndex, pageSize, ref totalRow, statusArray);
            return news.ToArray();
        }

        public List<NewsPublishForObjectBoxEntity> SearchNewsForNewsRelation(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow, bool showBomb, bool isSearchQuery)
        {
            return NewsBo.SearchNewsForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow, showBomb, isSearchQuery);
        }
        public List<NewsPublishForObjectBoxEntity> SearchNewsForNewsRelation_ByNewsPublishTemp(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow, bool showBomb, bool isSearchQuery)
        {
            return NewsBo.SearchNewsPublishTemp_ForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow, showBomb, isSearchQuery);
        }
        public List<NewsPublishForObjectBoxEntity> SearchNewsForNewsTagRelation(int zoneId, int tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsForNewsTagRelation(zoneId, tagId, pageIndex, pageSize, ref totalRow);
        }

        public NewsPublishForSearchEntity[] SearchNewsPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, ref int totalRow)
        {
            return NewsBo.SearchNewsPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, pageIndex, pageSize, ref totalRow, excludeNewsIds).ToArray();
        }


        public NewsPublishForNewsRelationEntity[] GetNewsRelationByNewsId(long newsId)
        {
            return NewsBo.GetRelatedNewsByNewsId(newsId).ToArray();
        }

        public NewsPublishForNewsRelationEntity[] GetNewsRelationByTypeNewsId(long newsId, int type)
        {
            return NewsBo.GetRelatedNewsByTypeNewsId(newsId, type).ToArray();
        }

        public List<NewsWithAnalyticEntity> GetTopMostNewsInHour(int top, int zoneId)
        {
            return NewsBo.GetTopMostNewsInHour(top, zoneId);
        }

        public List<NewsWithAnalyticEntity> GetHotNews(int top, int day)
        {
            return NewsBo.GetHotNews(top, day);
        }

        public List<NewsWithAnalyticEntity> GetHotNews2(int top, int day)
        {
            return NewsBo.GetHotNews2(top, day);
        }

        public NewsInListEntity[] SearchNewsByStatus(string keyword, string userDoAction, string userForFilter,
                                              NewsFilterFieldForUsername userFieldForFilter,
                                              string zoneIds, DateTime fromDate, DateTime toDate, NewsType newsType,
                                              NewsStatus newsStatus, bool isGetTotalRow, int pageIndex, int pageSize,
                                              ref int totalRow)
        {
            NewsStatus status;
            if (!Enum.TryParse(newsStatus.ToString(), out status))
            {
                status = NewsStatus.Unknow;
            }
            NewsFilterFieldForUsername filterUsername;
            if (!Enum.TryParse(userFieldForFilter.ToString(), out filterUsername))
            {
                filterUsername = NewsFilterFieldForUsername.CreatedBy;
            }
            NewsType type;
            if (!Enum.TryParse(newsType.ToString(), out type))
            {
                type = NewsType.All;
            }

            return NewsBo.SearchNewsByStatus(keyword, userDoAction, userForFilter,
                                                                          filterUsername, zoneIds, fromDate, toDate,
                                                                          type, status, isGetTotalRow, pageIndex,
                                                                          pageSize, ref totalRow).ToArray();
        }

        public NewsEntity[] SearchNewsByTag(string tagIds, string tagNames, bool searchBytag, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByTag(tagIds, tagNames, searchBytag, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInTag(int zoneId, string keyword, long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInTag(zoneId, keyword, tagId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInMc(int zoneId, string keyword, int mcId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInMc(zoneId, keyword, mcId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInThread(int zoneId, string keyword, long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInThread(zoneId, keyword, threadId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInTopic(int zoneId, string keyword, long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInTopic(zoneId, keyword, topicId, pageIndex, pageSize, ref totalRow);
        }

        public List<NewsEntity> SearchNewsByFocusKeyword(string keyword, int top)
        {
            return NewsBo.SearchNewsByFocusKeyword(keyword, top);
        }

        public List<NewsEntity> CheckNewsByKeyword(string keyword, int page, int num, ref int total)
        {
            return NewsBo.CheckNewsByKeyword(keyword, page, num, ref total);
        }

        public NewsEntity[] SearchNewsByTagId(long tagId)
        {
            return NewsBo.SearchNewsByTagId(tagId).ToArray();
        }
        public List<NewsEntity> SearchNewsByTagIdWithPaging(long tagId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByTagIdWithPaging(tagId, status, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsEntity> SearchNewsByMcIdWithPaging(int mcId, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByMcIdWithPaging(mcId, type, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsEntity> SearchNewsByThreadIdWithPaging(long threadId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByThreadIdWithPaging(threadId, status, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsEntity> SearchNewsByTopicIdWithPaging(string keyword, long topicId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByTopicIdWithPaging(keyword, topicId, status, pageIndex, pageSize, ref totalRow);
        }

        public List<NewsEntity> SearchNewsPublishByTopicIdWithPaging(long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishByTopicIdWithPaging(topicId, pageIndex, pageSize, ref totalRow);
        }

        public List<NewsEntity> SearchHotNewsByTopicId(long topicId)
        {
            return NewsBo.SearchHotNewsByTopicId(topicId);
        }

        public NewsCounterEntity[] CounterNewsByStatus(string accountName, int role, string commonStatus)
        {
            return NewsBo.CounterNewsByStatus(accountName, role, commonStatus).ToArray();
        }

        public NewsCounterEntity[] GetNewsCounterByStatus(string accountName, int zoneId)
        {
            return NewsBo.GetNewsCounterByStatus(accountName, zoneId).ToArray();
        }

        public CategoryCounterEntity GetZoneCounter(int zoneId, DateTime fromDate, DateTime endDate)
        {
            return NewsBo.GetZoneCounter(zoneId, fromDate, endDate);
        }

        public NewsForExportEntity[] StatisticsExportNewsData(string zoneIds, string sourceIds, int orderBy, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.StatisticsExportNewsData(zoneIds, sourceIds, orderBy, fromDate, toDate).ToArray();
        }

        public NewsForExportEntity[] StatisticsExportHotNewsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.StatisticsExportHotNewsData(zoneIds, top, fromDate, toDate).ToArray();
        }

        public NewsForExportEntity[] StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.StatisticsExportNewsHasManyCommentsData(zoneIds, top, fromDate, toDate).ToArray();
        }

        public NewsForExportEntity[] StatisticsExportNewsHasManyCommentsDataV2(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            return NewsStatisticBo.StatisticsExportNewsHasManyCommentsData(zoneIds, top, fromDate, toDate).ToArray();
        }

        public NewsForExportEntity[] GetListNewsByDistributionDate(string zoneIds, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.GetListNewsByDistributionDate(zoneIds, fromDate, toDate).ToArray();
        }

        public NewsUnPublishEntity[] SearchNewsUnPublish(string keyword, int ZoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsUnPublish(keyword, ZoneId, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }

        public List<NewsDashBoardUserEntity> GetDataDashBoardUser(string username, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.GetDataDashBoardUser(username, fromDate, toDate);
        }
        public List<string> GetIds(string username, string zoneIds, int top)
        {
            var data= NewsBo.GetIds(username, zoneIds, top);
            if (data != null && data.Count > 0)
                return data.Select(s => s.ToString()).ToList();
            return new List<string>();
        }
        #endregion

        #region Get

        //@OK
        public NewsDetailForEditEntity GetDetail(long newsId, string currentUsername)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsForEditByNewsId(newsId, currentUsername);
        }
        //chinhnb clone cho CDData
        public NewsDetailForEditEntity GetDetailCDData(long newsId, string currentUsername)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsForEditByNewsIdCDData(newsId, currentUsername);
        }
        public NewsEntity GetNewsByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsByNewsId(newsId);
        }
        public NewsEntity GetNewsByNewsIdForInit(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsByNewsIdForInit(newsId);
        }
        public List<CountStatus> CountNews(string userName, string listStatus, int type)
        {
            return NewsBo.CountNews(userName, listStatus, type);
        }
        public NewsEntity GetNewsFullDetail(long newsId)
        {
            return NewsBo.GetNewsInfoById(newsId);
        }
        public int GetNewsTypeByNewsId(long id)
        {
            return NewsBo.GetNewsTypeByNewsId(id);
        }
        public NewsForValidateEntity GetNewsForValidateById(long newsId)
        {
            var data = BoCached.Base.News.NewsDalFactory.GetNewsForValidateById(newsId);
            if (null == data)
            {
                data = NewsBo.GetNewsForValidateById(newsId);
            }
            return data;
        }        

        public NewsEntity GetNewsDetailByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsByNewsId(newsId);
        }

        public NewsForOtherAPIEntity[] GetNewsForOtherAPIByNewsIds(string newsIds)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsForOtherApi(newsIds).ToArray();
        }

        public List<NewsWithAnalyticEntity> GetNewsByListNewsId(NewsStatus status, string listNewsId)
        {
            return NewsBo.GetNewsByListNewsId(status, listNewsId);
        }
        public List<NewsWithExpertEntity> GetAllNewsByListNewsId(string listNewsId)
        {
            return NewsBo.GetAllNewsByListNewsId(listNewsId);
        }
        public List<NewsForExportEntity> GetAllNewsForExportByListNewsId(string listNewsId)
        {
            return NewsBo.GetAllNewsForExportByListNewsId(listNewsId);
        }
        public List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId)
        {
            return NewsBo.GetNewsDistributionDateByListNewsId(listNewsId);
        }

        public NewsInfoForCachedEntity GetNewsInfoForCachedById(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsInfoForCachedById(newsId);
        }

        #region Get News By OriginalId

        public NewsInListEntity[] SearchNewsByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByOriginalId(zoneIds, fromDate, toDate, originalId, pageIndex, pageSize,
                                                    ref totalRow).ToArray();
        }
        public int GetViewCountByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId)
        {
            return NewsBo.GetViewCountByOriginalId(zoneIds, fromDate, toDate, originalId);
        }

        #endregion

        #endregion

        #region News notification

        public WcfActionResponse SetNotification(long newsId, string username, int newsStatus, bool isWarning, string message)
        {
            NewsStatus status;
            if (!Enum.TryParse(newsStatus.ToString(), out status))
            {
                status = NewsStatus.Unknow;
            }

            var errorCode = NewsBo.SetNotification(newsId, username, status, isWarning, message);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ReadNotification(long newsId, string username)
        {
            var errorCode = NewsBo.ReadNotification(newsId, username);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse SetLabelForNews(long newsId, string username, int labelId)
        {
            var errorCode = NewsBo.SetLabelForNews(newsId, username, labelId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public NewsNotificationEntity[] GetNewsNotificationByNewsId(string listNewsId, string username)
        {
            return NewsBo.GetNewsNotificationByNewsId(listNewsId, username).ToArray();
        }
        public NewsNotificationEntity[] GetUnReadNewsNotificationByUsername(string username, DateTime getFromDate, ref int unreadCount)
        {
            return NewsBo.GetUnReadNewsNotificationByUsername(username, getFromDate, ref unreadCount).ToArray();
        }

        #endregion

        #region News MobileStream
        public List<NewsMobileStreamEntity> GetNewsMobileStreamList(int zoneId)
        {
            return BoFactory.GetInstance<NewsMobileStreamBo>().GetList(zoneId);
        }
        public WcfActionResponse SaveNewsMobileStream(List<UpdateNewsMobileStreamEntity> newsMobileStream)
        {
            var errorCode = NewsMobileStreamBo.SaveNewsMobileStream(newsMobileStream);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                foreach (var newsMobilePosition in newsMobileStream)
                {
                    CmsTrigger.Instance.OnChangeNewsMobileStream(newsMobilePosition.NewsId, newsMobilePosition.ZoneId, newsMobilePosition.IsHighlight, newsMobilePosition.Status);
                }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        // Tướng Cương Add
        public WcfActionResponse NewsMobileStream_RemoveTopLessView(int topRemove, int numberOfNewsOnTimeLine, int newsPositionTypeIdOnMobileFocus)
        {
            var errorCode = NewsMobileStreamBo.NewsMobileStream_RemoveTopLessView(topRemove, numberOfNewsOnTimeLine, newsPositionTypeIdOnMobileFocus);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

        }
        #endregion

        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage(string listOfFocusPositionOnLastestnews)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage(listOfFocusPositionOnLastestnews);
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestnews)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId, listOfFocusPositionOnLastestnews);
        }

        public List<NewsPositionEntity> GetNewsPositionByPositionType(int newsPositionType, int zoneId, string listOfOrder)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionByTypeAndZoneId(newsPositionType, zoneId, listOfOrder);
        }
        public List<NewsPositionEntity> GetListNewsPositionAllBom(int newsPositionType, int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionAllBom(newsPositionType, zoneId);
        }
        public List<NewsPositionEntity> GetScheduleByTypeAndOrder(int typeId, int order, int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetScheduleByTypeAndOrder(typeId, order, zoneId);
        }

        public NewsPositionEntity GetNewsPositionByNewsPositionId(int newsPositionId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetNewsPositionById(newsPositionId);
        }

        public WcfActionResponse SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        {
            var errorCode =
                BoFactory.GetInstance<NewsPositionBo>().SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                foreach (var newsPosition in newsPositions)
                {
                    CmsTrigger.Instance.OnChangeNewsPosition(newsPosition.TypeId, newsPosition.ZoneId, newsPosition.Order, newsPosition.NewsId);
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse SaveNewsPositionSchedule(int typeId, int zoneId, int order, string listNewsId, string listScheduleDate, string listRemovePosition, string usernameForAction)
        {
            var errorCode =
                BoFactory.GetInstance<NewsPositionBo>().SaveNewsPositionSchedule(typeId, zoneId, order, listNewsId, listScheduleDate, listRemovePosition, usernameForAction);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    // Chuẩn hóa datetime
                    var arrScheduleDate = listScheduleDate.Replace("-", "/").Split(';');
                    int counter = 0;
                    foreach (var newsId in listNewsId.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        try
                        {
                            dynamic currentNewsPosition = new System.Dynamic.ExpandoObject();
                            var jsonKey = "{\"ZoneId\":" + zoneId + ", \"TypeId\":" + typeId + ", \"NewsId\":" + newsId + "}";
                            currentNewsPosition.TypeId = typeId;
                            currentNewsPosition.ZoneId = zoneId;
                            currentNewsPosition.Order = order;
                            currentNewsPosition.NewsId = newsId;
                            currentNewsPosition.Bomd = true;
                            currentNewsPosition.BombTime = arrScheduleDate[counter];
                            currentNewsPosition.ExpiredDate = string.Empty;
                            ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentNewsPosition), "updatenewsposition", arrScheduleDate[counter], jsonKey);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, ex.Message);
                        }
                        counter++;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UnlockNewsPosition(int positionId)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UnlockNewsPosition(positionId);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                var newsPosition = GetNewsPositionByNewsPositionId(positionId);
                if (newsPosition != null)
                {
                    CmsTrigger.Instance.OnChangeNewsPosition(newsPosition.TypeId, newsPosition.ZoneId, newsPosition.Order, newsPosition.NewsId);
                }
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdatePositionOrder(int typeId, int zoneId, string listPositionId)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UpdatePositionOrder(typeId, zoneId, listPositionId);

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                CmsTrigger.Instance.OnChangeNewsPosition(typeId, zoneId, 0, 0);
            }

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdatePositionOrderByPositionId(long positionId, int positionOrder)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UpdatePositionOrderByPositionId(positionId, positionOrder);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse LockNewsPosition(int positionId, int lockForZoneId, DateTime expiredLock)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().LockNewsPosition(positionId, lockForZoneId, expiredLock);
            return errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
        }

        public List<NewsPositionForMobileEntity> GetNewsPositionForMobile()
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForMobileByTypeAndZoneId((int)NewsPositionTypeForMobile.DefaultType, 0);
        }

        public WcfActionResponse SaveNewsPositionForMobile(NewsPositionForMobileEntity[] newsPositionsForMobile, int avatarIndex, bool checkNewsExists)
        {
            var errorCode =
                BoFactory.GetInstance<NewsPositionBo>().SaveNewsPositionForMobile(
                    new List<NewsPositionForMobileEntity>(newsPositionsForMobile), avatarIndex, checkNewsExists);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            // Log Activity
            //if (responseData.Success)
            //{
            //    var stopTrying = false;
            //    var time = DateTime.Now;
            //    var applicationId = ApplicationEntity.Id(ApplicationEntity.ApplicationId.CommentCheckError);
            //    while (!stopTrying)
            //    {
            //        try
            //        {
            //            ActivityBo.Insert(new ActivityEntity
            //            {
            //                ActionText = "",
            //                ApplicationId = applicationId,
            //                CreatedDate = DateTime.Now,
            //                SourceId = newsPositionsForMobile.CreatedBy,
            //                SourceName = checkNodeCommentEntity.CreatedBy,
            //                DestinationId = string.Empty,
            //                DestinationName = string.Empty,
            //                GroupTimeLine = Utility.SetPublishedDate(),
            //                Message =
            //                    string.Format(ActionTextCheckNode, checkNodeCommentEntity.CreatedBy, newsUrl, newsTitle, ownerNews, actionContent),
            //                OwnerId = checkNodeCommentEntity.CreatedBy,
            //                Type = 0
            //            });
            //            stopTrying = true;
            //        }
            //        catch (Exception ex)
            //        {
            //            if (DateTime.Now.Subtract(time).Milliseconds > 1000)
            //            {
            //                stopTrying = true;
            //                throw ex;
            //            }
            //        }
            //    }
            //}

            return responseData;
        }

        public WcfActionResponse LockPositionByType(long newsPositionId, int lockTypeId, int order, DateTime expiredLock)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().LockPositionByType(newsPositionId, lockTypeId, order, expiredLock);
            return errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse LockPositionByNewsId(long newsId, int zoneId, int lockTypeId, int order, DateTime expiredLock)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().LockPositionByNewsId(newsId, zoneId, lockTypeId, order, expiredLock);
            return errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UnlockPositionByType(int lockTypeId, int order)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UnlockPositionByType(lockTypeId, order);
            return errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse SaveLinkPosition(NewsPositionType typeId, int position, int zoneId, long newsId, int type, string title, string avatar, string url, string sapo)
        {
            var errorCode = NewsPositionBo.SaveLinkPosition(typeId, position, zoneId, newsId, type, title, avatar, url, sapo);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {                    
                    CmsTrigger.Instance.OnChangeLinkPosition((int)typeId, zoneId, position, newsId, type, title, avatar, url, sapo);                    
                }

                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region News history

        public List<NewsHistoryEntity> GetNewsHistoryByNewsId(long newsId, DateTime from, DateTime to)
        {
            return BoFactory.GetInstance<NewsHistoryBo>().GetHistoryByNewsid(newsId,from, to);
        }

        public List<ActivityEntity> SearchLogs(int pageIndex, int pageSize, long applicationId, int actionTypeDetail, int type, string sourceId, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            return BoFactory.GetInstance<NewsHistoryBo>().SearchLogs(pageIndex, pageSize, applicationId, actionTypeDetail, type, sourceId, dateFrom, dateTo, ref totalRows);
        }

        #endregion

        #region News Author

        //@OK
        public WcfActionResponse NewsAuthorInsert(NewsAuthorEntity newsAuthorEntity, ref int newsAuthorId)
        {
            var errorCode = NewsAuthorBo.Insert(newsAuthorEntity, ref newsAuthorId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsAuthorId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        //@OK
        public WcfActionResponse NewsByAuthorInsert(NewsByAuthorEntity newsByAuthorEntity)
        {
            var errorCode = NewsAuthorBo.InsertToNewsByAuthor(newsByAuthorEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public NewsAuthorEntity NewsAuthorAutoCompleted(NewsAuthorEntity newsAuthorEntity)
        {
            return NewsAuthorBo.AutoCompleted(newsAuthorEntity);
        }

        //@OK
        public WcfActionResponse NewsAuthorUpdate(NewsAuthorEntity newsAuthorEntity)
        {
            var errorCode = NewsAuthorBo.Update(newsAuthorEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(newsAuthorEntity), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        //@OK
        public WcfActionResponse NewsAuthorDelete(int id)
        {
            var errorCode = NewsAuthorBo.Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        //@OK
        public NewsAuthorEntity NewsAuthorGetById(int id)
        {
            return NewsAuthorBo.GetById(id);
        }

        //@OK
        public List<NewsAuthorEntity> ListNewsAuthorByUser(string username)
        {
            return NewsAuthorBo.GetListByUserName(username);
        }

        //@OK
        public List<NewsAuthorEntity> ListNewsAuthorSearch(string keyword)
        {
            return NewsAuthorBo.Search(keyword);
        }

        public List<NewsAuthorEntity> NewsAuthorGetAll()
        {
            return NewsAuthorBo.GetAll();
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByAuthorAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return NewsAuthorBo.InitESAllNewsAuthor(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllNewsAuthor(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return NewsAuthorBo.InitRedisAllNewsAuthor(name, startDate, endDate, pageSize, action);
        }

        public List<NewsByAuthorEntity> ListNewsAuthorInNews(long newsId)
        {
            return NewsAuthorBo.GetListInNews(newsId);
        }
        public NewsAuthorEntity GetByUserData(string userData, AuthorType authorType)
        {
            return NewsAuthorBo.GetByUserData(userData, (int)authorType);
        }
        #endregion

        #region News version
        public WcfActionResponse UpdateNewsVersion(NewsEntity news, string lastModifiedBy)
        {
            var errorCode = NewsBo.UpdateVersion(news, lastModifiedBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ReleaseNewsVersion(long newsId, string lastModifiedBy)
        {
            var errorCode = NewsBo.ReleaseVersion(newsId, lastModifiedBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        //public WcfActionResponse CreateVersionForExistsNews(long newsId, string createdBy, ref long newsVersionId)
        //{
        //    var errorCode = NewsBo.CreateVersionForExistsNews(newsId, createdBy, ref newsVersionId);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        public WcfActionResponse ReleaseFirstNewsVersion(long newsId, string userForUpdateAction)
        {
            var errorCode = NewsBo.ReleaseFirstVersion(newsId, userForUpdateAction);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        //public WcfActionResponse RemoveNewsVersion(long newsId, string lastModifiedBy)
        //{
        //    var errorCode = NewsBo.RemoveEditingVersion(newsId, lastModifiedBy);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        public WcfActionResponse AutoSaveNewsVersion(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername)
        {
            //var errorCode = NewsBo.AutoSaveNewsVersion(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername);
            //return errorCode == ErrorMapping.ErrorCodes.Success
            //           ? WcfActionResponse.CreateSuccessResponse()
            //           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            //goi tu nodejs           
            var errorCode = NewsBo.AutoSaveNewsVersion(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

        }

        public List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> GetNewsVersionByNewsId(long newsId)
        {
            //return NewsBo.GetVersionByNewsId(newsId).ToArray();
            //goi tu nodejs
            return NewsBo.GetVersionByNewsId(newsId);
        }
        public List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> GetNewsVersionByNewsIdForUser(long newsId, string lastModifiedBy)
        {
            //return NewsBo.GetListVersionByNewsId(newsId, lastModifiedBy);
            //goi tu nodejs
            return NewsBo.GetListVersionByNewsId(newsId, lastModifiedBy);
        }
        public NewsVersionNodeJsEntity GetNewsVersionById(string id)
        {
            return NewsBo.GetById(id);
        }
        
        public WcfActionResponse RemoveNewsVersionByNewsId(long newsId, string userForUpdateAction)
        {
            var errorCode = NewsBo.RemoveNewsVersionByNewsId(newsId, userForUpdateAction);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse RemoveNewsVersionById(string versionId,string accountName)
        {
            var errorCode = NewsBo.RemoveNewsVersionById(versionId, accountName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #endregion        

        #region For NewsConfig

        public WcfActionResponse SetNewsConfigValue(NewsConfigEntity obj)
        {
            var errorCode = NewsConfigBo.SetValue(obj);
            if(errorCode == ErrorMapping.ErrorCodes.Success)
            {
                var jsonKey = "{\"ConfigName\":\"" + obj.ConfigName + "\"}";                
                var strPram = NewtonJson.Serialize(obj, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatenewsconfig", string.Empty, jsonKey);
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<NewsConfigEntity> GetListConfigByGroupKey(string groupConfigKey)
        {            
            return NewsConfigBo.GetListConfigByGroupKey(groupConfigKey);
        }
        public List<NewsConfigEntity> GetListConfigByAll(int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsConfigBo.GetAll(pageIndex, pageSize, ref totalRow);
        }
        public NewsConfigEntity GetNewsConfigByConfigName(string configName)
        {            
            return NewsConfigBo.GetByConfigName(configName);
        }
        public NewsConfigEntity GetNewsConfigByTypeInitValue(int type, string value)
        {
            return NewsConfigBo.GetNewsConfigByTypeInitValue(type, value);
        }

        public WcfActionResponse SetValueForStaticHtmlTemplate(string configName, string configLabel, string configValue, EnumStaticHtmlTemplateType configType)
        {
            var errorCode = NewsConfigBo.SetValueForStaticHtmlTemplate(configName, configLabel, configValue, configType);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<NewsConfigEntity> GetListConfigForStaticHtmlTemplate(string groupConfigKey, EnumStaticHtmlTemplateType type)
        {
            return NewsConfigBo.GetListConfigForStaticHtmlTemplate(groupConfigKey, type);
        }
        public WcfActionResponse DeleteNewsConfigByConfigName(string configName)
        {
            var errorCode = NewsConfigBo.DeleteNewsConfigByConfigName(configName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                var jsonKey = "{\"ConfigName\":\"" + configName + "\"}";
                var strPram = NewtonJson.Serialize(configName, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "deletenewsconfig", string.Empty, jsonKey);
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region For News OS an News Crawler

        #region News OS

        public NewsOsEntity GetNewsOsDetail(long newsId)
        {
            return NewsOsBo.GetNewsByNewsId(newsId);
        }

        public ListNewsOsEntity[] GetListNewsOs(string keyword, string catId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsOsBo.GetListNews(keyword, catId, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public long ReceiverNewsOs(long newsId, string content, string avatar, string username)
        {
            return NewsOsBo.ReceiverNews(newsId, content, avatar, username);
        }

        public WcfActionResponse DeleteNewsOsById(long newsId)
        {
            var errorCode = NewsOsBo.DeleteById(newsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteNewsOsByListId(string listNewsId)
        {
            var errorCode = NewsOsBo.DeleteByListId(listNewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region News Crawler

        public NewsCrawlerEntity GetNewsCrawlerDetail(int newsId)
        {
            return NewsCrawlerBo.GetNewsByNewsId(newsId);
        }

        public NewsCrawlerEntity[] GetListNewsCrawler(int catId, int siteId, int hot, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsCrawlerBo.GetListNews(catId, siteId, hot, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public long ReceiverNewsCrawler(int newsId, string content, string avatar, string zoneId, string username)
        {
            return NewsCrawlerBo.ReceiverNews(newsId, content, avatar, zoneId, username);
        }

        public WcfActionResponse InsertNewsCrawlerSite(string siteLink, string siteName)
        {
            WcfActionResponse responseData;

            var errorCode = NewsCrawlerBo.InsertSite(siteLink, siteName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public WcfActionResponse InvisibleNewsCrawlerSite(int id)
        {
            WcfActionResponse responseData;

            var errorCode = NewsCrawlerBo.InvisibleSite(id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteNewsCrawlerById(int newsId)
        {
            WcfActionResponse responseData;

            var errorCode = NewsCrawlerBo.DeleteById(newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }
        public WcfActionResponse DeleteNewsCrawlerByListId(string listNewsId)
        {
            WcfActionResponse responseData;

            var errorCode = NewsCrawlerBo.DeleteByListId(listNewsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public CategoryCrawlerEntity[] GetListCrawlerCategory()
        {
            return NewsCrawlerBo.GetListCategoryCrawler().ToArray();
        }
        public SiteCrawlerEntity[] GetListCrawlerSite()
        {
            return NewsCrawlerBo.GetListSiteCrawler().ToArray();
        }

        #endregion

        #endregion

        #region For NewsCachedMonitor

        //public List<UpdateCachedEntity> GetUpdateCachedItemForNews(PageSetting pageSetting)
        //{
        //    return BoFactory.GetInstance<NewsCachedMonitorBo>().GetUpdateCachedItemForNews(pageSetting);
        //}

        public WcfActionResponse UpdateCachedProcessIdForNews(string listOfProcessId)
        {
            var errorCode = BoFactory.GetInstance<NewsCachedMonitorBo>().UpdateCachedProcessIdForNews(listOfProcessId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateCachedProcessIdForTag(string listOfProcessId)
        {
            var errorCode = BoFactory.GetInstance<NewsCachedMonitorBo>().UpdateCachedProcessIdForTag(listOfProcessId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region For Zone

        public ZoneEntity GetZoneById(int zoneId)
        {
            return ZoneBo.GetZoneById(zoneId);
        }

        public ZoneEntity GetZoneDbById(int zoneId)
        {
            return ZoneBo.GetZoneDbById(zoneId);
        }

        public List<ZoneEntity> GetListZoneByParentId(int parentZoneId)
        {
            return ZoneBo.GetListZoneByParentId(parentZoneId);
        }

        public List<ZoneEntity> GetZoneByKeyword(string keyword)
        {
            return ZoneBo.GetZoneByKeyword(keyword);
        }

        public List<ZoneEntity> GetAllZone()
        {
            return ZoneBo.GetAllZone();
        }

        public List<ZoneEntity> GetAllZoneActive()
        {
            return ZoneBo.GetAllZoneActive();
        }

        public List<ZoneWithSimpleFieldEntity> GetPrivateZone(string username, int parentZoneId)
        {
            return ZoneBo.GetPrivateZone(username, parentZoneId);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllZone(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return ZoneBo.InitRedisAllZone(name, startDate, endDate, pageSize, action);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneActiveWithTreeView(bool getParentZoneOnly)
        {
            return ZoneBo.GetAllZoneActiveWithTreeView(getParentZoneOnly);
        }
        public List<ZoneWithSimpleFieldEntity> GetAllZoneWithTreeView(bool getParentZoneOnly)
        {
            return ZoneBo.GetAllZoneWithTreeView(getParentZoneOnly);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneActivedWithTreeView(bool getParentZoneOnly)
        {
            return ZoneBo.GetAllZoneActivedWithTreeView(getParentZoneOnly);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeView(string username)
        {
            return ZoneBo.GetAllZoneByUserWithTreeView(username);
        }

        public List<ZonePhotoWithSimpleFieldEntity> GetAllZonePhotoWithTreeView()
        {
            return ZonePhotoBo.GetAllZonePhotoWithTreeView();
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneByUserAndPermissionWithTreeView(string username, int permissionId)
        {
            return ZoneBo.GetAllZoneByUserWithTreeViewAndPermission(username, permissionId);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneByUserAndListPermissionWithTreeView(string username, params int[] permissionIds)
        {
            return ZoneBo.GetAllZoneByUserWithTreeViewAndPermission(username, permissionIds);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneActivedByUserAndListPermissionWithTreeView(string username, params int[] permissionIds)
        {
            return ZoneBo.GetAllZoneActivedByUserWithTreeViewAndPermission(username, permissionIds);
        }

        public List<ZoneEntity> GetListZoneByUserId(int userId)
        {
            return ZoneBo.GetListZoneByUserId(userId);
        }

        public List<ZoneEntity> GetListZoneByUsername(string username)
        {
            return ZoneBo.GetListZoneByUsername(username);
        }

        public List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            return ZoneBo.GetZoneByNewsId(newsId);
        }

        public List<ZoneDefaultTagEntity> GetAllDefaultTagForZone()
        {
            return ZoneBo.GetAllDefaultTagForZone();
        }

        public WcfActionResponse Insert(ZoneEntity zoneEntity, string username)
        {
            var errorCode = ZoneBo.Insert(zoneEntity, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (responseData.Success)
            {
                var newsestZone = GetAllZone().OrderByDescending(x => x.Id);
                if (newsestZone.Count() > 0)
                {
                    var jsonKey = "{\"Id\":" + newsestZone.FirstOrDefault().Id + "}";
                    zoneEntity.Id = newsestZone.FirstOrDefault().Id;
                    var strPram = NewtonJson.Serialize(zoneEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updatezone", string.Empty, jsonKey);
                }
            }

            return responseData;
        }
        public WcfActionResponse InsertZoneDB(ZoneEntity zoneEntity, string username)
        {
            var errorCode = ZoneBo.InsertZoneDB(zoneEntity, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            //if (responseData.Success)
            //{
            //    var newsestZone = GetAllZone().OrderByDescending(x => x.Id);
            //    if (newsestZone.Count() > 0)
            //    {
            //        var jsonKey = "{\"Id\":" + newsestZone.FirstOrDefault().Id + "}";
            //        zoneEntity.Id = newsestZone.FirstOrDefault().Id;
            //        var strPram = NewtonJson.Serialize(zoneEntity, "yyyy-MM-dd'T'HH:mm:ss");
            //        ContentDeliveryServices.PushToDataCD(strPram, "updatezone", string.Empty, jsonKey);
            //    }
            //}

            return responseData;
        }

        public WcfActionResponse Update(ZoneEntity zoneEntity, string username)
        {
            var errorCode = ZoneBo.Update(zoneEntity, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + zoneEntity.Id + "}";
                var strPram = NewtonJson.Serialize(zoneEntity, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatezone", string.Empty, jsonKey);
            }

            return responseData;
        }

        public WcfActionResponse MoveUp(int zoneId, string username)
        {
            var errorCode = ZoneBo.MoveUp(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse MoveDown(int zoneId, string username)
        {
            var errorCode = ZoneBo.MoveDown(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse ToggleZoneInvisibled(int zoneId, string username)
        {
            var errorCode = ZoneBo.ToggleZoneInvisibled(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + zoneId + "}";
                var strPram = NewtonJson.Serialize(new { }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatezone", string.Empty, jsonKey);
            }

            return responseData;
        }

        public WcfActionResponse ToggleZoneStatus(int zoneId, string username)
        {
            var errorCode = ZoneBo.ToggleZoneStatus(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + zoneId + "}";
                var strPram = NewtonJson.Serialize(new { }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatezone", string.Empty, jsonKey);
            }

            return responseData;
        }

        public WcfActionResponse ToggleZoneAllowComment(int zoneId, string username)
        {
            var errorCode = ZoneBo.ToggleZoneAllowComment(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + zoneId + "}";
                var strPram = NewtonJson.Serialize(new { }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatezone", string.Empty, jsonKey);
            }

            return responseData;
        }        

        public List<ZoneEntity> GetGroupNewsZoneByParentId(int zoneId)
        {
            return ZoneBo.GetGroupNewsZoneByParentId(zoneId);
        }

        #endregion

        #region For NewsUsePhoto
        public WcfActionResponse UpdatePhotoPublishedInNews(long newsId, string photoPublishedIds, string photoPublishedDescriptions)
        {
            var errorCode = BoFactory.GetInstance<NewsBo>().UpdatePhotoPublishedInNews(newsId, photoPublishedIds, photoPublishedDescriptions);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public NewsUsePhotoEntity[] GetAllNewsUsePhotoByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetAllNewsUsePhotoByNewsId(newsId).ToArray();
        }
        public int CountPhotoPublishedByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().CountPhotoPublishedByNewsId(newsId);
        }
        #endregion

        #region For NewsUseVideo

        public WcfActionResponse UpdateVideoInNews(long newsId, string videoIds, string videoDescriptions)
        {
            var errorCode = BoFactory.GetInstance<NewsBo>().UpdateVideoInNews(newsId, videoIds, videoDescriptions);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public NewsUseVideoEntity[] GetAllNewsUseVideoByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetAllNewsUseVideoByNewsId(newsId).ToArray();
        }
        public int CountVideoByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().CountVideoByNewsId(newsId);
        }
        #endregion

        #region For NewsSource

        public WcfActionResponse NewsSourceInsert(NewsSourceEntity newsSourceEntity, ref int newsSourceId)
        {
            var errorCode = BoFactory.GetInstance<NewsSourceBo>().Insert(newsSourceEntity, ref newsSourceId);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse NewsSourceUpdate(NewsSourceEntity newsSourceEntity)
        {
            var errorCode = BoFactory.GetInstance<NewsSourceBo>().Update(newsSourceEntity);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse NewsSourceDelete(int id)
        {
            var errorCode = BoFactory.GetInstance<NewsSourceBo>().Delete(id);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public NewsSourceEntity NewsSourceGetById(int id)
        {
            return BoFactory.GetInstance<NewsSourceBo>().GetById(id);
        }

        public List<NewsSourceEntity> NewsSourceGetAll(string keyword)
        {
            return BoFactory.GetInstance<NewsSourceBo>().GetAll(keyword);
        }

        public NewsBySourceEntity[] ListNewsSourceInNews(long newsId)
        {
            return BoFactory.GetInstance<NewsSourceBo>().GetListInNews(newsId).ToArray();
        }

        #endregion

        #region For EmbedGroup
        public WcfActionResponse EmbedGroupInsert(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            try
            {
                var errorCode = EmbedGroupBo.Insert(embedGroup, listOfZoneId);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse EmbedGroupUpdate(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            try
            {
                var errorCode = EmbedGroupBo.Update(embedGroup, listOfZoneId);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse EmbedGroupUpdateSortOrder(string listOfEmbedGroupId)
        {
            try
            {
                var errorCode = EmbedGroupBo.UpdateSortOrder(listOfEmbedGroupId);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public EmbedGroupEntity[] EmbedGroupGetAll()
        {
            return EmbedGroupBo.GetAll().ToArray();
        }
        public EmbedGroupWithDetailEntity EmbedGroupGetById(int id)
        {
            return EmbedGroupBo.GetById(id);
        }
        public WcfActionResponse EmbedGroupDelete(int id)
        {
            try
            {
                var errorCode = EmbedGroupBo.Delete(id);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        #endregion

        #region For BoxBanner

        public WcfActionResponse InsertBoxBanner(BoxBannerEntity boxBanner, string zoneIdList, ref int id)
        {
            try
            {
                var errorCode = BoxBannerBo.InsertBoxBanner(boxBanner, zoneIdList, ref id);
                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var dataProcess = new BoxBannerCdData();
                    boxBanner.Id = id;
                    dataProcess.BoxBanner = boxBanner;
                    dataProcess.ZoneIdList = zoneIdList;
                    var strParamValue = NewtonJson.Serialize(dataProcess, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + boxBanner.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertboxbanner", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxBanner(BoxBannerEntity boxBanner, string zoneIdList)
        {
            try
            {
                var errorCode = BoxBannerBo.UpdateBoxBanner(boxBanner, zoneIdList);
                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var dataProcess = new BoxBannerCdData();
                    dataProcess.BoxBanner = boxBanner;
                    dataProcess.ZoneIdList = zoneIdList;
                    var strParamValue = NewtonJson.Serialize(dataProcess, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + boxBanner.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateboxbanner", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxBannerPriority(string listOfBoxBannerId)
        {
            try
            {
                var errorCode = BoxBannerBo.UpdateBoxBannerPriority(listOfBoxBannerId);

                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var jsonKey = "{\"ListId\":" + listOfBoxBannerId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "updatepriorityboxbanner", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteBoxBanner(int boxBannerId)
        {
            try
            {
                var errorCode = BoxBannerBo.DeleteBoxBanner(boxBannerId);

                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {                    
                    var jsonKey = "{\"Id\":" + boxBannerId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deleteboxbanner", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoxBannerEntity GetBoxBannerById(int id)
        {
            return BoxBannerBo.GetBoxBannerById(id);
        }

        public List<BoxBannerEntity> GetListBoxBannerByZoneIdAndPosition(int zoneId, int position, EnumBoxBannerStatus status, EnumBoxBannerType type)
        {
            return BoxBannerBo.GetListBoxBannerByZoneIdAndPosition(zoneId, position, status, type);
        }
        public List<BoxBannerZoneEntity> GetListZoneByBoxBannerId(int boxBannerId)
        {
            return BoxBannerBo.GetListZoneByBoxBannerId(boxBannerId);
        }

        #endregion

        #region For BoxInboundComponentEmbed

        public WcfActionResponse InsertBoxInboundComponentEmbed(BoxInboundComponentEmbedEntity item, ref int id)
        {
            try
            {
                var errorCode = BoxInboundComponentEmbedBo.InsertBoxInboundComponentEmbed(item, ref id);
                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {                    
                    item.Id = id;                        
                    var strParamValue = NewtonJson.Serialize(item, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + item.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertboxinboundcomponentembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxInboundComponentEmbed(BoxInboundComponentEmbedEntity item)
        {
            try
            {
                var errorCode = BoxInboundComponentEmbedBo.UpdateBoxInboundComponentEmbed(item);
                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {                    
                    var strParamValue = NewtonJson.Serialize(item, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + item.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateboxinboundcomponentembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxInboundComponentEmbedPriority(string listOfId)
        {
            try
            {
                var errorCode = BoxInboundComponentEmbedBo.UpdateBoxInboundComponentEmbedPriority(listOfId);

                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var jsonKey = "{\"ListId\":" + listOfId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "updatepriorityboxinboundcomponentembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteBoxInboundComponentEmbed(int boxId)
        {
            try
            {
                var errorCode = BoxInboundComponentEmbedBo.DeleteBoxInboundComponentEmbed(boxId);

                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var jsonKey = "{\"Id\":" + boxId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deleteboxinboundcomponentembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoxInboundComponentEmbedEntity GetBoxInboundComponentEmbedById(int id)
        {
            return BoxInboundComponentEmbedBo.GetBoxInboundComponentEmbedById(id);
        }

        public List<BoxInboundComponentEmbedEntity> GetListBoxInboundComponentEmbed(long newsId, int position, int status, int type)
        {
            return BoxInboundComponentEmbedBo.GetListBoxInboundComponentEmbed(newsId, position, status, type);
        }

        #endregion

        #region For BoxLivePageEmbed

        public WcfActionResponse InsertBoxLivePageEmbed(BoxLivePageEmbedEntity item, ref int id)
        {
            try
            {
                var errorCode = BoxLivePageEmbedBo.InsertBoxLivePageEmbed(item, ref id);
                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    item.Id = id;
                    var strParamValue = NewtonJson.Serialize(item, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + item.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertboxlivepageembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxLivePageEmbed(BoxLivePageEmbedEntity item)
        {
            try
            {
                var errorCode = BoxLivePageEmbedBo.UpdateBoxLivePageEmbed(item);
                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var strParamValue = NewtonJson.Serialize(item, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + item.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateboxlivepageembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxLivePageEmbedPriority(string listOfId)
        {
            try
            {
                var errorCode = BoxLivePageEmbedBo.UpdateBoxLivePageEmbedPriority(listOfId);

                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var jsonKey = "{\"ListId\":" + listOfId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "updatepriorityboxlivepageembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteBoxLivePageEmbed(int boxId)
        {
            try
            {
                var errorCode = BoxLivePageEmbedBo.DeleteBoxLivePageEmbed(boxId);

                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var jsonKey = "{\"Id\":" + boxId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deleteboxlivepageembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoxLivePageEmbedEntity GetBoxLivePageEmbedById(int id)
        {
            return BoxLivePageEmbedBo.GetBoxLivePageEmbedById(id);
        }

        public List<BoxLivePageEmbedEntity> GetListBoxLivePageEmbed(string pageName, string pageUrl, int position, int status, int type, int templateId)
        {
            return BoxLivePageEmbedBo.GetListBoxLivePageEmbed(pageName, pageUrl, position, status, type, templateId);
        }

        #endregion

        #region For BoxInboundComponentTemplate

        public WcfActionResponse InsertBoxInboundTemplate(BoxInboundTemplateEntity item, ref int id)
        {
            try
            {
                var errorCode = BoxInboundComponentEmbedBo.InsertBoxInboundTemplate(item, ref id);
                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    item.Id = id;
                    var strParamValue = NewtonJson.Serialize(item, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + item.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertboxinboundcomponenttemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxInboundTemplate(BoxInboundTemplateEntity item)
        {
            try
            {
                var errorCode = BoxInboundComponentEmbedBo.UpdateBoxInboundTemplate(item);
                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var strParamValue = NewtonJson.Serialize(item, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + item.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateboxinboundcomponenttemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }        

        public WcfActionResponse DeleteBoxInboundTemplate(int boxId)
        {
            try
            {
                var errorCode = BoxInboundComponentEmbedBo.DeleteBoxInboundTemplate(boxId);

                var data = errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if (data.Success)
                {
                    var jsonKey = "{\"Id\":" + boxId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deleteboxinboundcomponenttemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoxInboundTemplateEntity GetBoxInboundTemplateById(int id)
        {
            return BoxInboundComponentEmbedBo.GetBoxInboundTemplateById(id);
        }

        public List<BoxInboundTemplateEntity> GetListBoxInboundTemplate(string typeId, string title, string listZoneId, string listTopicId, string listThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoxInboundComponentEmbedBo.GetListBoxInboundTemplate(typeId, title, listZoneId, listTopicId, listThreadId, pageIndex, pageSize, ref totalRow);
        }

        #endregion

        #region NewsLive -  Bài tường thuật

        public WcfActionResponse InsertNewsLive(NewsLiveEntity news, ref NewsLiveDetail returnNews)
        {
            var newsLiveId = 0L;
            var errorCode = NewsLiveBo.Insert(news, ref newsLiveId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(news.Id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success && newsLiveId > 0)
            {
                returnNews = NewsLiveBo.GetDetails(newsLiveId);
            }
            return responseData;
        }

        public WcfActionResponse UpdateNewsLive(NewsLiveEntity news)
        {
            var errorCode = NewsLiveBo.Update(news);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(news.Id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteNewsLive(long id, string accountName)
        {
            var errorCode = NewsLiveBo.Delete(id, accountName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse AddNewsLiveAuthor(NewsLiveAuthorEntity newsLiveAuthorEntity)
        {
            var errorCode = NewsLiveBo.AddAuthor(newsLiveAuthorEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteNewsLiveAuthor(long newsId, int authorId)
        {
            var errorCode = NewsLiveBo.DeleteAuthor(newsId, authorId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse PublishNewsLive(long id, string accountName)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();
            var errorCode = NewsLiveBo.Publish(id, accountName);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse RollbackNewsLive(long id, string accountName)
        {
            var errorCode = NewsLiveBo.Rollback(id, accountName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse TrashNewsLive(long id, string accountName)
        {
            var errorCode = NewsLiveBo.Trash(id, accountName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateContentNewsLive(long id, string content)
        {
            var errorCode = NewsLiveBo.UpdateContent(id, content);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateNewsLiveSettings(NewsLiveSettingsEntity newsLiveSettings)
        {
            var errorCode = NewsLiveBo.UpdateSettings(newsLiveSettings);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public NewsLiveForList GetAllNewsLive(long newsId, NewsLiveStatus status, int order)
        {
            return NewsLiveBo.GetAll(newsId, status, order);
        }

        public NewsLiveEntity[] GetPagingNewsLive(long newsId, NewsLiveStatus status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsLiveBo.GetPaging(newsId, status, order, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public NewsLiveDetail GetDetailNewsLive(long id)
        {
            return NewsLiveBo.GetDetails(id);
        }

        public NewsLiveAuthorEntity[] GetNewsLiveAuthorList(long newsLiveId)
        {
            return NewsLiveBo.GetAuthorList(newsLiveId).ToArray();
        }

        public NewsLiveSettingsEntity GetNewsLiveSettings(long newsId)
        {
            return NewsLiveBo.GetSettings(newsId);
        }

        #endregion

        #region NewsEmbedBox - Box nhung ngoai trang
        public List<NewsEmbedBoxListEntity> GetListNewsEmbedBox(int type, int status)
        {
            return NewsEmbedBoxBo.GetListNewsEmbedBox(type, status);
        }

        public WcfActionResponse InsertNewsEmbedBox(NewsEmbebBoxEntity newsEmbedBox)
        {
            var errorCode = NewsEmbedBoxBo.Insert(newsEmbedBox);
            return errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsEmbedBox.News_ID.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateNewsEmbedBox(string listNewsId, int type)
        {
            var errorCode = NewsEmbedBoxBo.Update(listNewsId, type);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {                    
                    var currentNewsPosition = new EmbedBoxCdData();
                    var jsonKey = "{\"Type\":" + type + "}";
                    currentNewsPosition.ListNewsId = listNewsId.Split(',').ToArray();
                    currentNewsPosition.Type = type;
                    var jsonValue = NewtonJson.Serialize(currentNewsPosition);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "updateboxnews", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }

        public WcfActionResponse UpdateStatusNewsEmbedBox(string listOfBoxNewsId, int type, int status)
        {
            var errorCode = NewsEmbedBoxBo.UpdateStatus(listOfBoxNewsId, type, status);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listOfBoxNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var currentNewsPosition = new EmbedBoxCdData();
                    var jsonKey = "{\"Type\":" + type + "}";
                    currentNewsPosition.ListNewsId = listOfBoxNewsId.Split(',').ToArray();
                    currentNewsPosition.Type = type;
                    var jsonValue = NewtonJson.Serialize(currentNewsPosition);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "updatestatusboxnews", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }

        public WcfActionResponse DeleteNewsEmbedBox(long newsId, int type)
        {
            var errorCode = NewsEmbedBoxBo.Delete(newsId, type);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var currentNewsPosition = new EmbedBoxCdData();
                    var jsonKey = "{\"Type\":" + type + "}";
                    currentNewsPosition.ListNewsId = new string[] { newsId.ToString() };
                    currentNewsPosition.Type = type;
                    var jsonValue = NewtonJson.Serialize(currentNewsPosition);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "deleteboxnews", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            return result;
        }
        #endregion

        #region NewsEmbedBoxOnPage
        public List<NewsEmbedBoxOnPageListEntity> GetListNewsEmbedBoxOnPage(int zoneId, int type)
        {
            return NewsEmbedBoxOnPageBo.GetListNewsEmbedBoxOnPage(zoneId, type);
        }

        public WcfActionResponse InsertNewsEmbedBoxOnPage(NewsEmbedBoxOnPageEntity newsEmbedBox)
        {
            var errorCode = NewsEmbedBoxOnPageBo.Insert(newsEmbedBox);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsEmbedBox.NewsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateNewsEmbedBoxOnPage(string listNewsId, int zoneId, int type)
        {
            var errorCode = NewsEmbedBoxOnPageBo.Update(listNewsId, zoneId, type);
            var result = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var currentNewsPosition = new EmbedBoxOnPageCdData();
                    var jsonKey = "{\"ZoneId\":" + zoneId + ", \"Type\":" + type + "}";
                    currentNewsPosition.ListNewsId = listNewsId.Split(',').ToArray();
                    currentNewsPosition.TypeNewsEmbed = type;
                    currentNewsPosition.ZoneId = zoneId;
                    var jsonValue = NewtonJson.Serialize(currentNewsPosition);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "updateboxnewsembed", string.Empty, jsonKey);                    
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }

        public WcfActionResponse DeleteNewsEmbedBoxOnPage(long newsId, int zoneId, int type)
        {
            var errorCode = NewsEmbedBoxOnPageBo.Delete(newsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region NewsChild
        public WcfActionResponse UpdateNewsChildVersion(string userDoAction, long parentNewsId, int order, string body, ref NewsChildVersionEntity returnNewsChild)
        {
            returnNewsChild = NewsChildBo.UpdateNewsChildVersion(parentNewsId, order, body, userDoAction);
            return (returnNewsChild == null
                                    ? WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                                          ErrorMapping.Current[
                                                                              ErrorMapping.ErrorCodes.BusinessError])
                                    : WcfActionResponse.CreateSuccessResponse());
        }

        public WcfActionResponse UpdateChildVersion(NewsChildVersionEntity entity)
        {
            var errorCode = NewsChildBo.UpdateNewsChildVersion(entity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                               ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse RemoveNewsChildVersion(string userDoAction, long parentNewsId, int order)
        {
            var errorCode = NewsChildBo.RemoveNewsChild(parentNewsId, order, userDoAction);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                               ErrorMapping.Current[errorCode]);
        }

        public NewsChildVersionEntity GetNewsChildDetailVersion(string userDoAction, long parentNewsId, int order)
        {
            return NewsChildBo.GetByNewsIdAndOrder(parentNewsId, order, userDoAction);
        }

        /// <summary>
        /// lưu dữ liệu newsChild theo parentNewsid
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WcfActionResponse InsertNewsChild(NewsChildEntity entity)
        {
            var errorCode = NewsChildBo.InsertNewsChild(entity);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(entity.NewsParentId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        /// <summary>
        /// Cập nhật thông tin news Child
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateNewsChild(NewsChildEntity entity)
        {
            var errorCode = NewsChildBo.UpdateNewsChild(entity);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(entity.NewsParentId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNewsId"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteNewsChild(long parentNewsId, int order)
        {
            var errorCode = NewsChildBo.DeleteNewsChild(parentNewsId, order);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(parentNewsId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNewsId"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteNewsChildVersion(long versionId, int order)
        {
            var errorCode = NewsChildBo.DeleteNewsChildVersion(versionId, order);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(versionId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WcfActionResponse InsertNewsChildVersion(NewsChildVersionEntity entity)
        {
            var errorCode = NewsChildBo.InsertNewsChildVersion(entity);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(entity.NewsParentId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="isGetContent"></param>
        /// <returns></returns>
        public List<NewsChildEntity> GetNewsChildByNewsId(long newsId, bool isGetContent = false)
        {
            return NewsChildBo.GetNewsChildByNewsId(newsId, isGetContent);
        }
        public List<NewsChildVersionEntity> GetNewsChildVersionByNewsId(long newsId, bool isGetContent = false)
        {
            return NewsChildBo.GetNewsChildVersionByNewsId(newsId, isGetContent);
        }

        public NewsChildEntity GetByNewsAndOrderId(long parentNewsId, int order)
        {
            return NewsChildBo.GetByNewsAndOrderId(parentNewsId, order);
        }
        #endregion

        #region News Recommendation
        public NewsInListEntity[] RecommendByActiveUsers(int zoneId, int top)
        {
            //return NewsBo.RecommendByActiveUsers(zoneId, top).ToArray();
            return new NewsInListEntity[] { };
        }
        #endregion

        #region For Royalties

        /// <summary>
        /// Get dánh sách bài viết để chấm nhuận bút
        /// </summary>
        /// <param name="zoneId">Id chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu lấy tin</param>
        /// <param name="endDate">Ngày kết thúc lấy tin</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="order">Order</param>
        /// <param name="isGetTotal">Có get total row hay không</param>
        /// <returns></returns>
        public NewsForRoyaltiesEntity[] GetRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, bool isGetTotal, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                var data = NewsBo.GetRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
                totalRow = isGetTotal ? NewsBo.CountRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId) : data.Count;
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new NewsForRoyaltiesEntity[] { };
        }

        public NewsForRoyaltiesEntity[] GetBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, bool isGetTotal, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                var data = NewsBo.GetBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
                totalRow = isGetTotal ? NewsBo.CountBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId) : data.Count;
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new NewsForRoyaltiesEntity[] { };
        }

        public NewsForRoyaltiesEntity[] GetPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, bool isGetTotal, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                var data = NewsBo.GetPRRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
                totalRow = isGetTotal ? NewsBo.CountPRRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId) : data.Count;
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new NewsForRoyaltiesEntity[] { };
        }
        public List<NewsForRoyaltiesEntity> GetRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author, int pageIndex, int pageSize, int order, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            try
            {
                var data = NewsBo.GetRoyaltiesV2(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, ref totalRow,
                                                 order, NewsCategories, newsType, viewCountFrom, viewCountTo);
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return null;
        }
        public List<NewsForRoyaltiesEntity> GetPRRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author, int pageIndex, int pageSize, int order, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            try
            {
                var data = NewsBo.GetRoyaltiesPRV2(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, ref totalRow,
                                                 order, NewsCategories, newsType, viewCountFrom, viewCountTo);
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return null;
        }


        /// <summary>
        /// Get tong tien nhuận bút
        /// </summary>
        /// <param name="zoneId">Id chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu lấy tin</param>
        /// <param name="endDate">Ngày kết thúc lấy tin</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả</param>
        public decimal GetTotalRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string newsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            return NewsBo.GetTotalPrice(zoneId, startDate, endDate, creator, author, newsCategories, newsType, viewCountFrom, viewCountTo, originalId);
        }

        /// <summary>
        /// Lưu lại giá tiền của bài viết
        /// </summary>
        /// <param name="newsId">Id bài viết</param>
        /// <param name="price">Giá tiền</param>
        /// <returns></returns>
        public WcfActionResponse SetPrice(long newsId, decimal price)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.SetPrice(newsId, price);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        /// <summary>
        /// Update tiền thưởng nhuận bút
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="bonusPrice"></param>
        /// <returns></returns>
        public WcfActionResponse SetBonusPrice(long newsId, decimal bonusPrice)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.SetBonusPrice(newsId, bonusPrice);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="isOnHome"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateIsOnHome(long newsId, bool isOnHome, string accountName)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateIsOnHome(newsId, isOnHome, accountName);
            if (code == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Success = true;
                CmsTrigger.Instance.OnUpdateIsOnHomeNews(newsId, isOnHome);
            }
            else {
                responseData.Success = false;
                responseData.Data = "Error update!";
            }
            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="isFocus"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateIsFocus(long newsId, bool isFocus, string accountName)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateIsFocus(newsId, isFocus, accountName);
            if (code == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Success = true;
                CmsTrigger.Instance.OnUpdateIsFocusNews(newsId, isFocus);
            }
            else {
                responseData.Success = false;
                responseData.Data = "Error update!";
            }
            
            return responseData;
        }

        public WcfActionResponse UpdateSeoReviewStatus(long newsId, string accountName)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateSeoReviewStatus(newsId, accountName);
            if (code == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Success = true;                
            }
            else {
                responseData.Success = false;
                responseData.Data = "Error update!";
            }

            return responseData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayInSlide"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateDisplayInSlide(long id, int displayInSlide)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateDisplayInSlide(id, displayInSlide);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }

        public WcfActionResponse UpdateDisplayPosition(long id, int displayPosition, string accountName)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateDisplayPosition(id, displayPosition, accountName);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        public WcfActionResponse UpdatePriority(long id, int priority, string accountName)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdatePriority(id, priority, accountName);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }

        public bool RemoveNewsPosition(int configPosition, int zoneId, int typeId, string accountName)
        {            
            return NewsBo.RemoveNewsPosition(configPosition, zoneId, typeId, accountName);
        }

    /// <summary>
    /// Lưu lại ghi chú khi chấm nhuận bút
    /// </summary>
    /// <param name="newsId">Id bài viết</param>
    /// <param name="note">Ghi chú</param>
    /// <returns></returns>
    public WcfActionResponse SetNoteRoyalties(long newsId, string note)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.SetNoteRoyalties(newsId, note);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }

        public WcfActionResponse SetNewsCategory(long newsId, int newsCategory)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.SetNewsCategory(newsId, newsCategory);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        #endregion

        #region NewsGroup

        public WcfActionResponse UpdateTopMostRead(int groupNewsId, int withinDays, int rootZoneId, int excludeLastDays)
        {
            var errorCode = NewsGroupBo.UpdateTopMostRead(groupNewsId, withinDays, rootZoneId, excludeLastDays);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostReadDaily(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            var errorCode = NewsGroupBo.UpdateTopMostReadDaily(groupNewsId, rootZoneId, excludeLastDays);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostReadHourly(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            var errorCode = NewsGroupBo.UpdateTopMostReadHourly(groupNewsId, rootZoneId, excludeLastDays);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostComment(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays)
        {
            var errorCode = NewsGroupBo.UpdateTopMostComment(groupNewsId, withinDays, rootZoneId, objectType, excludeLastDays);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public NewsGroupEntity NewsGroupGetById(int id)
        {
            try
            {
                return NewsGroupBo.NewsGroupGetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsGroupEntity();
            }
        }

        public WcfActionResponse UpdateTopMostReadByZone(int groupNewsId, int withinDays, int excludeLastDays, int zoneId, int itemCount)
        {
            var errorCode = NewsGroupBo.UpdateTopMostReadByZone(groupNewsId, withinDays, excludeLastDays, zoneId, itemCount);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostReadV2(int groupNewsId, int withinDays, int excludeLastDays, int itemCount)
        {
            var errorCode = NewsGroupBo.UpdateTopMostReadV2(groupNewsId, withinDays, excludeLastDays, itemCount);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostCommentByZone(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays, int topNews)
        {
            var errorCode = NewsGroupBo.UpdateTopMostCommentByZone(groupNewsId, withinDays, rootZoneId, objectType, excludeLastDays, topNews);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostCommentV2(int groupNewsId, int withinDays, int objectType, int excludeLastDays, int topNews)
        {
            var errorCode = NewsGroupBo.UpdateTopMostCommentV2(groupNewsId, withinDays, objectType, excludeLastDays, topNews);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public List<NewsGroupDetailEntity> UpdateTopMostReadV3(int groupNewsId, int zoneId, int withinHours, int totalRow)
        {
            return NewsGroupBo.UpdateTopMostReadV3(groupNewsId, zoneId, withinHours, totalRow);
        }
        public WcfActionResponse UpdateTopMostLikeShare(int groupNewsId, int zoneId, string lstNewsId, string likeShareCountList)
        {
            var errorCode = NewsGroupBo.UpdateTopMostLikeShare(groupNewsId, zoneId, lstNewsId, likeShareCountList);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public WcfActionResponse UpdateTopCommentCountInDay(int groupNewsId, string lstNewsId, string lstCommentCount)
        {
            var errorCode = NewsGroupBo.UpdateTopCommentCountInDay(groupNewsId, lstNewsId, lstCommentCount);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        #endregion

        #region NewsPositionSchedule

        public WcfActionResponse InsertNewsPositionSchedule(NewsPositionScheduleEntity newsPosition)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().InserNewsPositionSchedule(newsPosition);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public WcfActionResponse UpdateNewsPositionSchedule(NewsPositionScheduleEntity newsPosition)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UpdateNewsPositionSchedule(newsPosition);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public WcfActionResponse DeleteNewsPositionSchedule(int newsPositionScheduleId)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().DeleteNewsPositionSchedule(newsPositionScheduleId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public NewsPositionScheduleEntity[] GetListNewsPositionScheduleGetByPositionTypeId(int positionTypeId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionScheduleGetByPositionTypeId(positionTypeId).ToArray();
        }
        public NewsPositionEntity[] GetListNewsPositionScheduleByPositionTypeIdAndOrder(int typeId, int order, int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionScheduleByPositionTypeIdAndOrder(typeId, order, zoneId).ToArray();
        }
        public WcfActionResponse AutoUpdateNewsPositionSchedule(int maxProcess)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().AutoUpdateNewsPositionSchedule(maxProcess);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            return responseData;
        }

        #endregion

        #region NewsWaitRepublish

        public WcfActionResponse InsertNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newEncryptNewsId = String.Empty;
                var newsId = 0L;
                var errorCode = NewsBo.InsertNewsWaitRepublish(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, ref newsId, ref newEncryptNewsId, new List<string>(authorList), newsChildOrder, sourceId, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newEncryptNewsId, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse ChangeNewsWaitRepublishStatusToProcessed(long newsId, string userForUpdateAction)
        {
            try
            {
                var errorCode = NewsBo.ChangeNewsWaitRepublishStatusToProcessed(newsId, userForUpdateAction);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = NewsBo.UpdateNewsWaitRepublish(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, isRebuildLink, ref newsStatus, new List<string>(authorList), newsChildOrder, sourceId, publishedContent, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public NewsInListEntity[] SearchNewsWaitRepublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsWaitRepublishStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsWaitRepublish(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }

        public NewsDetailForEditEntity GetNewsByNewsWaitRepublishId(long newsId, string currentUsername)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsByNewsWaitRepublishId(newsId, currentUsername);
        }

        public int NewsWaitRepublishCountDown(string username, string zoneIds, NewsFilterFieldForUsername filterFieldForUsername, NewsType newsType)
        {
            return NewsBo.NewsWaitRepublishCountDown(username, zoneIds, filterFieldForUsername, newsType);
        }
        /// <summary>
        /// Lấy danh sách tin bài theo zoneId và theo ngày xuất bản
        /// </summary>
        /// <param name="zoneIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public List<NewsPublishEntity> GetNewsPublishByDate(int zoneIds, DateTime fromDate, DateTime toDate)
        {
            return NewsPublishBo.GetListDataByDate(zoneIds, fromDate, toDate);
        }

        public List<NewsPublishEntity> GetNewsPublishByHour(int hour, int zoneId)
        {
            return NewsPublishBo.GetLastestNewsByHour(hour, zoneId);
        }

        /// <summary>
        /// Lấy thông tin bài viết theo Id và zoneId
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public NewsPublishEntity GetByNewsIdAndZoneId(long newsId, int zoneId)
        {
            return NewsPublishBo.GetByNewsIdAndZoneId(newsId, zoneId);
        }
        #endregion

        #region NewsAnalytic

        public List<NewsAnalyticEntity> GetLastUpdateViewCountInNewsAnalytic(DateTime lastUpdateViewCount)
        {
            return NewsAnalyticBo.GetLastUpdateViewCount(lastUpdateViewCount);
        }

        public List<NewsAnalyticEntity> GetTopLastestNewsInNewsAnalytic(int top)
        {
            return NewsAnalyticBo.GetTopLastestNews(top);
        }

        public List<NewsAnalyticV3Entity> GetTopHighestViewCountByZone(int zone, int hour, int top)
        {
            return NewsAnalyticBo.GetTopHighestViewCountByZone(zone, hour, top);
        }

        public List<NewsAnalyticV3Entity> GetTopHighestCommentCountByZone(int zone, int hour, int top)
        {
            return NewsAnalyticBo.GetTopHighestCommentCountByZone(zone, hour, top);
        }
        public List<NewsPublishEntity> GetListNewsByListNewsId(string listNewsId)
        {
            return NewsPublishBo.GetListNewsByListNewsId(listNewsId);
        }

        #endregion

        #region NewsAnalyticV2

        public List<NewsAnalyticV2Entity> GetLastUpdateViewCount(long lastUpdateViewCountId)
        {
            return NewsAnalyticV2Bo.GetLastUpdateViewCount(lastUpdateViewCountId);
        }

        public List<NewsAnalyticV2Entity> GetAllViewFromDateToNow(DateTime fromDate, int maxRecord)
        {
            return NewsAnalyticV2Bo.GetAllViewFromDateToNow(fromDate, maxRecord);
        }

        public List<NewsAnalyticV2Entity> GetTopMostReadInHour(int top, int zoneId)
        {
            return NewsAnalyticV2Bo.GetTopMostReadInHour(top, zoneId);
        }

        #endregion

        #region NewsAnalyticAdtech

        public WcfActionResponse ResetAdtechViewCount()
        {
            try
            {
                var errorCode = NewsAnalyticAdtechBo.ResetAdtechViewCount();
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                     ? WcfActionResponse.CreateSuccessResponse()
                                                     : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                             ErrorMapping.Current[
                                                                                                 errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateAdtechViewCount(long newsId, int viewCount)
        {
            try
            {
                var errorCode = NewsAnalyticAdtechBo.UpdateAdtechViewCount(newsId, viewCount);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                     ? WcfActionResponse.CreateSuccessResponse()
                                                     : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                             ErrorMapping.Current[
                                                                                                 errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse BatchUpdateAdtechViewCount(string listNewsId, string listViewCount)
        {
            try
            {
                var errorCode = NewsAnalyticAdtechBo.BatchUpdateAdtechViewCount(listNewsId, listViewCount);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                     ? WcfActionResponse.CreateSuccessResponse()
                                                     : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                             ErrorMapping.Current[
                                                                                                 errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<NewsWithAnalyticEntity> GetTopMostReadInHourByAdtechViewCount(int top)
        {
            return NewsAnalyticAdtechBo.GetTopMostReadInHourByAdtechViewCount(top);
        }
        public List<NewsAnalyticEntity> GetLastUpdateAdtechViewCount(DateTime lastUpdateViewCount)
        {
            return NewsAnalyticAdtechBo.GetLastUpdateAdtechViewCount(lastUpdateViewCount);
        }
        public List<NewsAnalyticEntity> GetTopLastestAdtechViewCount(int top)
        {
            return NewsAnalyticAdtechBo.GetTopLastestAdtechViewCount(top);
        }

        #endregion

        #region NewsLogExternalAction

        public List<NewsLogExternalActionEntity> GetExternalActionLogByNewsIdAndActionType(long newsId,
                                                                          EnumNewsLogExternalAction actionType)
        {
            return NewsLogExternalActionBo.GetActionLogByNewsIdAndActionType(newsId, actionType);
        }

        public WcfActionResponse InsertExternalActionLog(NewsLogExternalActionEntity newsLogExternalAction)
        {
            try
            {
                var errorCode = NewsLogExternalActionBo.InsertActionLog(newsLogExternalAction);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region For NewsMaskOnline
        public List<NewsMaskOnlineEntity> ListNewsMaskOnline(string keyword, DateTime fromDate, DateTime toDate,
                                                  EnumMaskSourceId SourceID, EnumMaskStatus status, int pageIndex, int pageSize, int Mode = -1)
        {
            return NewsMaskOnlineBo.ListNewsMaskOnline(keyword, fromDate, toDate, SourceID, (int)status, pageIndex, pageSize, Mode);
        }
        #endregion

        #region for update like count Fb
        public WcfActionResponse UpdateLikeCount(long newsId, int likeCount)
        {
            var errorCode = NewsBo.UpdateLikeCount(newsId, likeCount);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<NewsEntity> SearchNewsByDateRange(DateTime startDate, DateTime endDate, int status)
        {
            return NewsBo.SearchNewsByDateRange(startDate, endDate, status);
        }
        #endregion

        public NewsPublishEntity GetNewsPublishByNewsId(long newsId)
        {
            //var oldEntity = NewsPublishBo.GetNewsPublishByNewsId(newsId);
            //var miEntity = new NewsPublishMiPresssEntity()
            //                   {
            //                       AdStore = oldEntity.AdStore,
            //                       AdStoreUrl = oldEntity.AdStoreUrl,
            //                       Author = oldEntity.Author,
            //                       Avatar = oldEntity.Avatar,
            //                       Avatar2 = oldEntity.Avatar2,
            //                       Avatar3 = oldEntity.Avatar3,
            //                       Avatar4 = oldEntity.Avatar4,
            //                       Avatar5 = oldEntity.Avatar5,
            //                       AvatarCustom = oldEntity.AvatarCustom,
            //                       AvatarDesc = oldEntity.AvatarDesc,
            //                       DisplayInSlide = oldEntity.DisplayInSlide,
            //                       DisplayPosition = oldEntity.DisplayPosition,
            //                       DisplayStyle = oldEntity.DisplayStyle,
            //                       DistributionDate = (oldEntity.DistributionDate - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalSeconds.ToString(),
            //                       EncryptId = oldEntity.EncryptId,
            //                       ExpiredDate = (oldEntity.ExpiredDate - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalSeconds.ToString(),
            //                       Id = oldEntity.Id,
            //                       InitSapo = oldEntity.InitSapo,
            //                       InterviewId = oldEntity.InterviewId,
            //                       IsBreakingNews = oldEntity.IsBreakingNews,
            //                       IsFocus = oldEntity.IsFocus,
            //                       IsPr = oldEntity.IsPr,
            //                       LocationType = oldEntity.LocationType,
            //                       NewsId = oldEntity.NewsId,
            //                       NewsRelation = oldEntity.NewsRelation,
            //                       OriginalId = oldEntity.OriginalId,
            //                       OriginalUrl = oldEntity.OriginalUrl,
            //                       PublishedDate = oldEntity.PublishedDate,
            //                       RollingNewsId = oldEntity.RollingNewsId,
            //                       Sapo = oldEntity.Sapo,
            //                       Source = oldEntity.Source,
            //                       SubTitle = oldEntity.SubTitle,
            //                       TagItem = oldEntity.TagItem,
            //                       TagSubTitleId = oldEntity.TagSubTitleId,
            //                       ThreadId = oldEntity.ThreadId,
            //                       Title = oldEntity.Title,
            //                       Type = oldEntity.Type,
            //                       Url = oldEntity.Url,
            //                       ZoneId = oldEntity.ZoneId
            //                   };
            //return miEntity;
            return NewsPublishBo.GetNewsPublishByNewsId(newsId);
        }

        public NewsContentEntity GetNewsContentByNewsId(long newsId)
        {
            return NewsContentBo.GetNewsContentByNewsId(newsId);
        }

        public List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            return NewsInZoneBo.GetNewsInZoneByNewsId(newsId);
        }

        public WcfActionResponse InsertNewsInZone(NewsInZoneEntity newsInZone)
        {
            try
            {
                var errorCode = NewsInZoneBo.InsertNewsInZone(newsInZone);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {

                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse() { Success = false };
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public NewsEntity GetNewsByTitle(string title)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsByTitle(title);
        }

        public int CountNewsBySource(int sourceId, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.CountNewsBySource(sourceId, fromDate, toDate);
        }

        public WcfActionResponse UpdatePhotoVideoCount(long newsId, int photoCount, int videoCount)
        {
            try
            {
                var errorCode = NewsBo.UpdatePhotoVideoCount(newsId, photoCount, videoCount);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {

                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }

        public WcfActionResponse UpdateNewsRelationJson(long id, List<NewsPublishForNewsRelationEntity> newsRelation)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateNewsRelationJson(id, newsRelation);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }

        public List<NewsInListEntity> SearchNewsPublishedByKeyword(string keyword)
        {
            var news = new List<NewsInListEntity>();
            try
            {
                news = NewsBo.SearchNewsPublishedByKeyword(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return news;
        }

        public List<NewsAppBotEntity> GetListNewsNotIbtempByZoneId(int zoneId, int page, int num, ref int total)
        {
            var news = new List<NewsAppBotEntity>();
            try
            {
                news = NewsBo.GetListNewsNotIbtempByZoneId(zoneId, page, num, ref total);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return news;
        }

        public List<NewsAppBotEntity> GetListNewsNotIbBoundByZoneId(int zoneId, int page, int num, ref int total)
        {
            var news = new List<NewsAppBotEntity>();
            try
            {
                news = NewsBo.GetListNewsNotIbBoundByZoneId(zoneId, page, num, ref total);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return news;
        }

        //public WcfActionResponse UpdateSpecialNewsRelation(long newsId, int zoneId, string newsRelationSpecialId)
        //{
        //    try
        //    {
        //        var newsStatus = 0;
        //        var errorCode = NewsBo.UpdateSpecialNewsRelation(newsId, zoneId, newsRelationSpecialId);
        //        WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
        //                                           ? WcfActionResponse.CreateSuccessResponse(newsId + ";" + newsStatus, "")
        //                                           : WcfActionResponse.CreateErrorResponse((int)errorCode,
        //                                                                                 ErrorMapping.Current[errorCode]);

        //        return responseData;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        //        return WcfActionResponse.CreateErrorResponse(ex);
        //    }
        //}

        #region Full log
        public List<NewsFullLogEntity> GetNewsFullLog(string keyword)
        {
            var news = new List<NewsFullLogEntity>();
            try
            {
                news = NewsBo.GetNewsFullLog(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return news;
        }
        #endregion

        #region News Top Hot
        public WcfActionResponse NewsHot_Insert(NewsHotEntity entity)
        {
            var errorCode = NewsBo.NewsHot_Insert(entity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_Insert2(NewsHotEntity entity)
        {
            var errorCode = NewsBo.NewsHot_Insert2(entity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_Delete(string NewsIds, int type)
        {
            var errorCode = NewsBo.NewsHot_Delete(NewsIds, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_CheckExists()
        {
            var errorCode = NewsBo.NewsHot_CheckExists();
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_UpdateType()
        {
            var errorCode = NewsBo.NewsHot_UpdateType();
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_Lock(int newsHotId)
        {
            var errorCode = NewsBo.NewsHot_Lock(newsHotId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_Unlock(int newsHotId)
        {
            var errorCode = NewsBo.NewsHot_Unlock(newsHotId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public bool NewsHot_ChangeNewsInLockPosition(int newsHotId, long newsId, long currentNewsId, NewsPositionEntity newsPositionEntity)
        {
            var result= NewsBo.NewsHot_ChangeNewsInLockPosition(newsHotId, newsId, newsPositionEntity);
            if (result)
            {                
                CmsTrigger.Instance.OnChangeNewsStreamPosition(newsPositionEntity.TypeId, newsPositionEntity.ZoneId, newsPositionEntity.Order, newsPositionEntity.NewsId, currentNewsId, newsHotId, newsPositionEntity.ExpiredLock, "UpdateNewsTopHot");
            }
            return result;
        }
        public bool NewsHot_AddNewsInLockPosition(NewsPositionEntity addItem)
        {
            var result = NewsBo.NewsHot_AddNewsInLockPosition(addItem);
            if (result)
            {
                CmsTrigger.Instance.OnChangeNewsStreamPosition(addItem.TypeId, addItem.ZoneId, addItem.Order, addItem.NewsId, 0, 0, addItem.ExpiredLock, "AddNewsTopHot");
            }
            return result;            
        }
        public bool NewsHot_DeleteNewsInLockPosition(NewsPositionEntity addItem)
        {
            var result = NewsBo.NewsHot_DeleteNewsInLockPosition(addItem);
            if (result)
            {
                CmsTrigger.Instance.OnChangeNewsStreamPosition(addItem.TypeId, addItem.ZoneId, addItem.Order, addItem.NewsId, 0, 0, addItem.ExpiredLock, "DeleteNewsTopHot");
            }
            return result;
        }
        public long NewsHot_UnpublishOrBomdNews(long newsId)
        {
            return NewsBo.NewsHot_UnpublishOrBomdNews(newsId);
        }
        public List<NewsHotEntity> NewsHot_GetByType(int type)
        {
            return NewsBo.NewsHot_GetByType(type);
        }
        public List<NewsHotEntity> NewsHot_AllForRedis()
        {
            return NewsBo.NewsHot_GetAllForRedis();
        }
        public List<NewsHotCafeFEntity> NewsHot_AllForRedisByCafeF()
        {
            return NewsBo.NewsHot_GetAllForRedisByCafeF();
        }
        #endregion
        #region SreamV2
        public List<NewsHotEntity> ListAllStreamV2()
        {
            var data = new List<NewsHotEntity>();
            try {
                var result = ContentDeliveryServices.ListStreamDataCD();
                if (result != null)
                {
                    if(result.block1!=null && result.block1.Count > 0)
                    {
                        data.AddRange(result.block1.Select((s,i) => new NewsHotEntity { newsId = Utility.ConvertToLong(s), Type=1, SortOrder= (i + 1) }));
                    }
                    if (result.block2 != null && result.block2.Count > 0)
                    {
                        data.AddRange(result.block2.Select((s, i) => new NewsHotEntity { newsId = Utility.ConvertToLong(s), Type = 2, SortOrder = (i + 1) }));
                    }
                    if (result.block3 != null && result.block3.Count > 0)
                    {
                        data.AddRange(result.block3.Select((s, i) => new NewsHotEntity { newsId = Utility.ConvertToLong(s), Type = 3, SortOrder = (i + 1) }));
                    }
                }
                if(data != null && data.Count > 0)
                {
                    var dataDb = BoCached.Base.News.NewsDalFactory.GetNewsByListId(data.Select(s => s.newsId.ToString()).ToList());
                    foreach (var item in data) {
                        foreach(var news in dataDb)
                        {
                            if(item.newsId == news.Id)
                            {
                                item.title = news.Title;
                                item.sapo = news.Sapo;
                                item.pictureUrl = news.Avatar;
                                item.LastModifiedDate = news.LastModifiedDate;
                                item.publishDate = news.DistributionDate;                                
                                item.url = news.Url;
                                item.categoryName = news.ZoneName;
                            }
                        }
                    }
                }
            }
            catch
            {

            }
            return data;
        }

        public bool SaveStreamV2(string account, string positionData, bool locked)
        {

            var data= ContentDeliveryServices.SaveStreamV2(account, positionData);
            if (data)
            {
                ActivityBo.LogSetNewsPositionOnHomeStream(0, account + ":positionData"+ positionData,"",-1, 0, locked);
            }
            return data;
        }
        #endregion

        #region Seoer Update News
        public WcfActionResponse Seoer_Update(SeoerNewsEntity news)
        {
            try
            {
                var errorCode = NewsBo.Seoer_Update(news);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                if(errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    //push CD
                    try
                    {                        
                        var jsonKey = "{\"NewsId\":" + news.NewsId + "}";
                        news.TagInNews = TagNewsDal.GetTagNewsByNewsId(news.NewsId);
                        var strParamValue = NewtonJson.Serialize(news, "yyyy-MM-dd'T'HH:mm:ss");
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updateseoernews", string.Empty, jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                    }
                }
                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }
        #endregion

        #region ViewPlus

        public List<NewsViewPlusListEntity> ViewPlus_GetList(int ZoneId, DateTime DateFrom, DateTime DateTo, int PageIndex, int PageSize, int bidType, ref int TotalRow)
        {
            var news = new List<NewsViewPlusListEntity>();
            try
            {
                news = NewsPrBo.ViewPlus_GetList(ZoneId, DateFrom, DateTo, PageIndex, PageSize, bidType, ref TotalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return news;
        }
        public WcfActionResponse ViewPlus_Insert(ViewPlusEntity viewPlus)
        {
            try
            {
                var errorCode = NewsPrBo.ViewPlus_Insert(viewPlus);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public WcfActionResponse ViewPlus_Delete(string Ids)
        {
            try
            {
                var errorCode = NewsPrBo.ViewPlus_Delete(Ids);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }

        #endregion

        #region ZoneViewPlus
        public List<ZoneViewPlusEntity> GetAllZoneViewPlus_by_TypeId_ZoneId(int typeId, int zoneId)
        {
            return NewsPrBo.GetAllZoneViewPlus_by_TypeId_ZoneId(typeId, zoneId);
        }
        #endregion

        #region NewsWarning
        public WcfActionResponse ContentWarningLog_Insert(long newsId, string Account)
        {
            try
            {
                var errorCode = ContentWarningBo.ContentWarningLog_Insert(newsId, Account);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public WcfActionResponse UpdateContentWarning(ContentWarningEntity contentWarning)
        {
            try
            {
                var errorCode = ContentWarningBo.UpdateContentWarning(contentWarning);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public List<ContentWarningEntity> GetListContentWarningByLastModifiedDate(DateTime fromDate, DateTime toDate)
        {
            var data = new List<ContentWarningEntity>();
            try
            {
                data = ContentWarningBo.GetListContentWarningByLastModifiedDate(fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return data;
        }
        public ContentWarningEntity GetContentWarningByNewsId(long newsId)
        {
            ContentWarningEntity data = null;
            try
            {
                data = ContentWarningBo.GetContentWarningByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return data;
        }
        public List<ContentWarningEntity> GetContentWarningByListNewsId(string listNewsId)
        {
            var data = new List<ContentWarningEntity>();
            try
            {
                data = ContentWarningBo.GetContentWarningByListNewsId(listNewsId);
                if (data!=null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        if (item.ConfirmLog != null)
                            item.ConfirmLog = (from c in item.ConfirmLog
                                               group c by new
                                               {
                                                   c.NewsId,
                                                   c.Account
                                               } into g
                                               select new ContentWarningLogEntity
                                               {
                                                   Account = g.Key.Account,
                                                   CreatedDate = g.Max(m => m.CreatedDate),
                                                   NewsId = g.Key.NewsId,
                                                   Status = g.Max(m => m.Status),
                                               }).ToList();
                    }
                    var result=data.Select(k => new ContentWarningEntity
                    {
                        NewsId = k.NewsId,
                        Level = k.Level,
                        LimitedCategory = k.LimitedCategory,
                        ListTopic = k.ListTopic,
                        ListEnity = k.ListEnity,
                        ListKeyword = k.ListKeyword,
                        ListTopicKeyword = k.ListTopicKeyword,
                        Status = k.Status,
                        ConfirmLog = k.ConfirmLog
                    }).ToList();

                    return result;
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return data;
            }            
        }
        public List<ContentWarningLogEntity> ContentWarningLog_GetByNewsId(long NewsId)
        {
            var data = new List<ContentWarningLogEntity>();
            try
            {
                data = ContentWarningBo.ContentWarningLog_GetByNewsId(NewsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return data;
        }

        public CheckContentWarningEntity CheckNewsContentWarning(string title, string sapo, string content, string topic, string tags)
        {
            CheckContentWarningEntity data = null;
            try
            {                  
                var datastring = new Extension.AdmicroServices().CheckNewsContentWarning(title, sapo, content, topic, tags);
                data = NewtonJson.Deserialize<CheckContentWarningEntity>(datastring);                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return data;
        }
        public CheckContentWarningV2Entity CheckNewsContentWarningV2(string title, string sapo, string content)
        {
            var data = new CheckContentWarningV2Entity();
            try
            {
                var dataTitle = string.Empty;
                var dataSapo = string.Empty;
                var dataBody = string.Empty;
                if (!string.IsNullOrEmpty(title))
                {
                    dataTitle = new Extension.AdmicroServices().CheckNewsContentWarningV2(title);
                }
                if (!string.IsNullOrEmpty(sapo))
                {
                    dataSapo = new Extension.AdmicroServices().CheckNewsContentWarningV2(sapo);
                }
                if (!string.IsNullOrEmpty(content))
                {
                    dataBody = new Extension.AdmicroServices().CheckNewsContentWarningV2(content);
                }

                var data1= NewtonJson.Deserialize<ReasonV2Entity>(dataTitle);
                data.Title = data1;
                var data2 = NewtonJson.Deserialize<ReasonV2Entity>(dataSapo);
                data.Sapo = data2;
                var data3 = NewtonJson.Deserialize<ReasonV2Entity>(dataBody);
                data.Body = data3;
                data.status = (data1.status && data2.status && data3.status) ?"yes":"no";
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return data;
        }

        public ContentWarningEntity UpdateNewsContentWarning(long newsId, string dataEntity, bool isConfirm, string username)
        {
            CheckContentWarningEntity data = null;
            ContentWarningEntity result = null;
            try
            {
                data = NewtonJson.Deserialize<CheckContentWarningEntity>(dataEntity);
                if (data != null && newsId > 0)
                {
                    result = new ContentWarningEntity
                    {
                        Level = data.level,
                        ListKeyword = string.Join(";", data.reason.keywords),
                        ListEnity = string.Join(";", data.reason.entity),                        
                        ListTopic = string.Join(";", data.reason.topics),
                        LimitedCategory = data.reason.limitedCategory,
                        Status = data.status == "yes" ? 1 : 0,
                        NewsId = newsId,
                        ListTopicKeyword = ""                        
                    };
                    TaskUpdateContentWarningAsync(result, isConfirm, username);
                }                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return result;
        }
        public ContentWarningEntity UpdateNewsContentWarningV2(long newsId, string dataEntity, bool isConfirm, string username)
        {
            CheckContentWarningV2Entity data = null;
            ContentWarningEntity result = null;
            try
            {
                data = NewtonJson.Deserialize<CheckContentWarningV2Entity>(dataEntity);
                if (data != null && newsId > 0)
                {
                    var listTitle=new List<string>();
                    listTitle.AddRange(data.Title.typename.bad_words ?? new List<string>());
                    listTitle.AddRange(data.Title.typename.warn_words ?? new List<string>());
                    listTitle.AddRange(data.Title.typename.politic_words ?? new List<string>());

                    var listSapo = new List<string>();
                    listSapo.AddRange(data.Sapo.typename.bad_words??new List<string>());
                    listSapo.AddRange(data.Sapo.typename.warn_words??new List<string>());
                    listSapo.AddRange(data.Sapo.typename.politic_words ?? new List<string>());

                    var listBody = new List<string>();
                    listBody.AddRange(data.Body.typename.bad_words ?? new List<string>());
                    listBody.AddRange(data.Body.typename.warn_words ?? new List<string>());
                    listBody.AddRange(data.Body.typename.politic_words ?? new List<string>());

                    result = new ContentWarningEntity
                    {
                        Level = 1,//data.level,
                        ListKeyword = string.Join(";", listTitle),
                        ListEnity = string.Join(";", listSapo),
                        ListTopic = string.Join(";", listBody),
                        LimitedCategory = string.Empty,//data.reason.limitedCategory,
                        Status = data.status == "yes" ? 1 : 0,
                        NewsId = newsId,
                        ListTopicKeyword = ""
                    };
                    TaskUpdateContentWarningAsync(result, isConfirm, username);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return result;
        }
        private void TaskUpdateContentWarningAsync(ContentWarningEntity contentWarning,bool isConfirm, string username)
        {
            var task = Task.Run(() =>
            {
                try
                {
                    UpdateContentWarning(contentWarning);
                    if (isConfirm)
                    {
                        ContentWarningLog_Insert(contentWarning.NewsId, username);
                    }                    
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, "ContentWarning => TaskUpdateContentWarningAsync() error => " + ex);
                }
            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }            
        }

        public ContentWarningEntity ReCheckNewsContentWarning(long newsId,string username)
        {
            ContentWarningEntity data = null;
            try
            {
                var newsDetail = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId); //GetNewsDetailByNewsId(newsId);

                if (newsDetail != null)
                {
                    var datastring = new Extension.AdmicroServices().CheckNewsContentWarning(newsDetail.Title, newsDetail.Sapo, newsDetail.Body, "", newsDetail.TagItem);

                    data=UpdateNewsContentWarning(newsId, datastring, false, username);                    
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return data;
        }
        public ContentWarningEntity ReCheckNewsContentWarningV2(long newsId, string username)
        {
            ContentWarningEntity data = null;
            try
            {
                var newsDetail = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
                if (newsDetail == null)
                {
                    newsDetail = GetNewsDetailByNewsId(newsId);
                }

                if (newsDetail != null)
                {                    
                    var datastring = CheckNewsContentWarningV2(newsDetail.Title, newsDetail.Sapo, newsDetail.Body);
                    
                    data = UpdateNewsContentWarningV2(newsId, NewtonJson.Serialize(datastring), false, username);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return data;
        }
        #endregion

        #region New methods for optimization
        public List<NewsPublishForSearchEntity> SearchNewsPublishedByKeywordAndZoneId(int zoneId, string keyword, int DisplayPosition, int Priority, bool IsShowNewsBomd, int pageIndex, int pageSize, bool useFullDatabase)
        {
            var total = 0;
            try
            {
                var news = new List<NewsPublishForSearchEntity>();
                if (WcfExtensions.WcfMessageHeader.Current.Namespace.ToLower().Equals("afamily"))
                {
                    //voi afamily thi chi sugget trong ngay
                    news = NewsPublishBo.SearchNewsPublishedByKeywordESInDateNow(keyword, zoneId, DisplayPosition, Priority, pageIndex, pageSize, ref total);
                }
                else {
                    news = NewsPublishBo.SearchNewsPublishedByKeywordES(keyword, zoneId, DisplayPosition, Priority, IsShowNewsBomd, pageIndex, pageSize, ref total);
                    if (news == null || (news != null && news.Count <= 0))
                    {
                        news = NewsPublishBo.SearchNewsPublishedByKeyword(zoneId, keyword, DisplayPosition, Priority, IsShowNewsBomd, pageIndex, pageSize, useFullDatabase);
                    }
                }
                
                return news;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new List<NewsPublishForSearchEntity>();
            }            
        }
        #endregion

        [DataContract]
        public enum EnumLabelType
        {
            [EnumMember]
            LabelType = 1,
            [EnumMember]
            LabelTime = 2
        }
    }
}