﻿using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.WcfMapping.ServiceSuggestTag;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;

namespace ChannelVN.CMS.WebApi.Services.Triggers
{
    public class AFamilyTrigger : CmsTriggerBase
    {
        #region Property

        public override TagSugguestConnectionName TagSugguestChannel
        {
            get { return TagSugguestConnectionName.Afamily_CMS; }
        }

        public override CmsPrServices.ChannelId CmsPrChannelId
        {
            get { return CmsPrServices.ChannelId.Afamily; }
        }

        public override PegaCmsServices.PegaSource PegaCmsSource
        {
            get { return PegaCmsServices.PegaSource.Afamily; }
        }

        public override StatisticTagChannel StatisticTagChannelForUpdateView
        {
            get { return StatisticTagChannel.Afamily; }
        }

        #endregion
    }
}