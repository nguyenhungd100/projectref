﻿using System.Reflection;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WebApi.Services.Triggers.Base
{
    public class CmsTrigger
    {
        private static readonly Assembly CurrentAssembly = Assembly.GetExecutingAssembly();
        public static CmsTriggerBase Instance
        {
            get
            {
                const string baseAssembly = "ChannelVN.CMS.WebApi.Services.Triggers.Base.CmsTriggerBase";
                var assembly = baseAssembly;
                var nameSpace = WcfMessageHeader.Current.Namespace;//AppConfigs.CmsApiNamespace;
                assembly = assembly.Replace(".Base.CmsTriggerBase", "." + nameSpace + "Trigger");

                var trigger = (CmsTriggerBase)
                                          CurrentAssembly.CreateInstance(assembly, false,
                                                                         BindingFlags.CreateInstance, null,
                                                                         null,
                                                                         System.Globalization.CultureInfo.CurrentCulture,
                                                                         null);
                if (trigger == null)
                {
                    //default full
                    assembly = "ChannelVN.CMS.WebApi.Services.Triggers.FullTrigger";

                    trigger = (CmsTriggerBase)
                                          CurrentAssembly.CreateInstance(assembly, false,
                                                                         BindingFlags.CreateInstance, null,
                                                                         null,
                                                                         System.Globalization.CultureInfo.CurrentCulture,
                                                                         null);
                }
                return trigger;
            }
        }
    }
}