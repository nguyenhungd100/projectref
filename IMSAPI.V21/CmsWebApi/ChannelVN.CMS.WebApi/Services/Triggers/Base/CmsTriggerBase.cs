﻿using ChannelVN.CMS.WcfMapping.ServiceSuggestTag;
using System;
using System.Threading;
using System.Web;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Services.Base;
using System.Runtime.Serialization;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.ProgramSchedule.Entity;
using System.Collections.Generic;
using ChannelVN.CMS.WebApi.Services.Extension;
using System.Linq;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.WebApi.Services.RedisDataServices;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.Entity.Base.Photo;
using static ChannelVN.CMS.WebApi.Services.ExternalServices.CmsPrV2Services;

namespace ChannelVN.CMS.WebApi.Services.Triggers.Base
{
    public abstract class CmsTriggerBase
    {
        #region Property

        public abstract TagSugguestConnectionName TagSugguestChannel { get; }

        public abstract CmsPrServices.ChannelId CmsPrChannelId { get; }

        public abstract PegaCmsServices.PegaSource PegaCmsSource { get; }

        public abstract StatisticTagChannel StatisticTagChannelForUpdateView { get; }

        #endregion

        #region Triggers

        #region News

        public virtual void OnPublishNews(long newsId, long idTagSession, bool isNotify, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(async delegate ()
            {
                HttpContext.Current = context;

                var newsDetail = new NewsServices().GetDetailCDData(newsId, "");
                if (newsDetail != null && newsDetail.NewsInfo != null)
                {
                    var news = newsDetail.NewsInfo;
                    //push data cd
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(newsDetail, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + newsDetail.NewsInfo.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertpublishnews", newsDetail.NewsInfo.DistributionDate.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }

                    //notify for app                    
                    AppLogNotifyServices.PushNotifyForApp(news, isNotify);
                    
                    // Push news to pega
                    if (news.Status == (int)NewsStatus.Published)
                    {
                        PegaCmsServices.NotifyInsertNews(newsDetail);
                    }

                    // If NewsPr => Mark content to published status
                    Logger.WriteLog(Logger.LogType.Debug, "=> Trigger OnPublishNews NewsId: " + newsId + ", NewsPR: " + news.IsPr);
                    if (news.IsPr)
                    {                        
                        var newsPr = new NewsServices().GetNewsPrByNewsId(news.Id);
                        Logger.WriteLog(Logger.LogType.Trace, "OnPublishNews: " + NewtonJson.Serialize(newsPr));
                        if (newsPr != null)
                        {
                            var newsEntityApi = new NewsEntityApi();
                            //xb PR V2
                            if (newsPr.ContentId > 0)
                            {
                                try
                                {
                                    var username = newsDetail.NewsInfo.LastModifiedBy;
                                    var msg = string.Format("[{0}] đã xuất bản bài viết IMS", username + "-" + CmsChannelConfiguration.GetAppSetting("ApiCmsPrV2ChannelName"));
                                    var success = CmsPrV2Services.UpdateNewsPrStatus(newsPr.ContentId, CmsPrV2Services.NewsPrStatus.Published, msg, username, news.Id, newsPr.BookingId,news.Url,"");
                                    newsEntityApi = await CmsPrV2Services.GetNewsPrDetail(newsPr.ContentId);
                                    Logger.WriteLog(Logger.LogType.Trace, "Publish NewsPrV2 success: " + success + " =>" + msg);
                                }
                                catch (Exception ex)
                                {
                                    Logger.WriteLog(Logger.LogType.Error, "Publish NewsPrV2 => " + ex.Message);
                                }
                            }

                            var primaryZoneId =
                                (from zone in newsDetail.NewsInZone where zone.IsPrimary select zone.ZoneId)
                                    .FirstOrDefault();
                            if (newsEntityApi != null && newsEntityApi.ContentID > 0 && CmsPrServices.PublishNewsPr(news.LastModifiedBy, news.Id, newsPr.ContentId, newsPr.DistributionId,
                                                            primaryZoneId,
                                                            news.Title, news.Sapo, news.Avatar, news.Body,
                                                            news.DistributionDate, news.Url, news.WordCount))
                            {
                                var viewPlusBannerId = newsPr.ViewPlusBannerId;                                
                                new NewsServices().UpdateNewsPrInfo(new NewsPrEntity
                                {
                                    Id = newsPr.Id,
                                    Mode = newsPr.Mode,
                                    IsFocus = newsPr.IsFocus,
                                    PublishDate = newsEntityApi.DistributionDate,
                                    DistributionDate = newsEntityApi.DistributionDate,
                                    ExpireDate = newsEntityApi.DistributionDate.AddMinutes(newsEntityApi.Duration),
                                    Duration = newsEntityApi.Duration,
                                    ViewPlusBannerId = viewPlusBannerId
                                });
                            }
                            //Begin PushViewPlus
                            try
                            {
                                Logger.WriteLog(Logger.LogType.Debug, "=> Begin PushViewPlus");
                                int webZone = 0;
                                int mobileZone = 0;
                                int showTime = 0;
                                dynamic obj = null;
                                var lst = new NewsServices().GetAllZoneViewPlus_by_TypeId_ZoneId(newsPr.Mode, newsPr.ZoneId);
                                Logger.WriteLog(Logger.LogType.Debug, "==> Get info zone view plus, Mode: " + newsPr.Mode + ", Zone: " + newsPr.ZoneId + ", has " + lst.Count + " record");
                                //Logger.WriteLog(Logger.LogType.Debug, "CHINHNB ===> Begin foreach record");
                                List<int> lstDeskTop = new List<int>();
                                List<int> lstMobile = new List<int>();
                                foreach (var a in lst)
                                {
                                    Logger.WriteLog(Logger.LogType.Debug, "====> Record, ZoneWeb: " + a.Web + ", ZoneMobile: " + a.Mobile + ", Showtime: " + a.ShowTime);
                                    webZone = Convert.ToInt32(a.Web);
                                    mobileZone = Convert.ToInt32(a.Mobile);
                                    showTime = Convert.ToInt32(a.ShowTime);

                                    if (webZone > 0)
                                    {
                                        if (!lstDeskTop.Contains(webZone))
                                        {
                                            var pubdate = newsPr.DistributionDate < news.DistributionDate ? news.DistributionDate : newsPr.DistributionDate;
                                            obj = new System.Dynamic.ExpandoObject();
                                            obj.newsId = newsId;
                                            obj.link = CmsChannelConfiguration.GetAppSetting("FRONT_END_URL").TrimEnd('/') + news.Url;
                                            obj.title = news.Title;
                                            obj.description = news.Sapo;
                                            obj.avatar = news.Avatar.IndexOf("http://") < 0 && news.Avatar.IndexOf("https://") < 0 ? CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD").TrimEnd('/') + "/" + news.Avatar : news.Avatar;
                                            obj.start_time = pubdate;
                                            obj.end_time = showTime > 0 ? pubdate.AddHours(showTime) : pubdate.AddHours(2);
                                            obj.logo = CmsChannelConfiguration.GetAppSetting("LogoViewPlus");
                                            obj.brand_name = a.ZoneName;
                                            obj.zone_ids = webZone;
                                            obj.budget = 1000;
                                            obj.hidden_ads_stats = 1;
                                            obj.channel = CmsChannelConfiguration.CurrentChannelNamespace;
                                            obj.news_type = news.Type;

                                            var jsonKey = "{\"Id\":" + newsId + ",\"TypeId\":" + a.TypeId + ",\"Mode\":" + a.Mode + ",\"ZoneId\":" + a.ZoneId + "}";
                                            var strPram = NewtonJson.Serialize(obj, "yyyy-MM-dd HH:mm:ss");

                                            Logger.WriteLog(Logger.LogType.Debug, "====> Begin push ZoneWeb");
                                            ContentDeliveryServices.PushToDataCD(strPram, "addzoneviewplus", string.Empty, jsonKey);
                                            
                                            lstDeskTop.Add(webZone);
                                        }
                                    }

                                    if (mobileZone > 0)
                                    {
                                        if (!lstMobile.Contains(mobileZone))
                                        {
                                            var pubdate = newsPr.DistributionDate < news.DistributionDate ? news.DistributionDate : newsPr.DistributionDate;
                                            obj = new System.Dynamic.ExpandoObject();
                                            obj.newsId = newsId;
                                            obj.link = CmsChannelConfiguration.GetAppSetting("FRONT_END_URL").TrimEnd('/') + news.Url;
                                            obj.title = news.Title;
                                            obj.description = news.Sapo;
                                            obj.avatar = news.Avatar.IndexOf("http://") < 0 && news.Avatar.IndexOf("https://") < 0 ? CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD").TrimEnd('/') + "/" + news.Avatar : news.Avatar;
                                            obj.start_time = pubdate;
                                            obj.end_time = showTime > 0 ? pubdate.AddHours(showTime) : pubdate.AddHours(2);
                                            obj.logo = CmsChannelConfiguration.GetAppSetting("LogoViewPlus");
                                            obj.brand_name = a.ZoneName;
                                            obj.zone_ids = mobileZone;
                                            obj.budget = 1000;
                                            obj.hidden_ads_stats = 1;
                                            obj.mobile_only = 1;
                                            obj.channel = CmsChannelConfiguration.CurrentChannelNamespace;
                                            obj.news_type = news.Type;

                                            var jsonKey = "{\"Id\":" + newsId + ",\"TypeId\":" + a.TypeId + ",\"Mode\":" + a.Mode + ",\"ZoneId\":" + a.ZoneId + "}";
                                            var strPram = NewtonJson.Serialize(obj, "yyyy-MM-dd HH:mm:ss");
                                            Logger.WriteLog(Logger.LogType.Debug, "====> Begin push ZoneMobile");
                                            ContentDeliveryServices.PushToDataCD(strPram, "addzoneviewplus", string.Empty, jsonKey);
                                            Logger.WriteLog(Logger.LogType.Debug, "====> End push ZoneMobile");
                                            lstMobile.Add(mobileZone);
                                        }
                                    }
                                }
                                Logger.WriteLog(Logger.LogType.Debug, "===> End foreach record");
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, "PushViewPlus: " + ex.Message);
                            }                                
                        }
                    }
                    //    // Publish Video On Publish
                    //    var autoPublishVideo = CmsChannelConfiguration.GetAppSettingInInt32("AutoPublishVideoOnPublishNews");
                    //    if (autoPublishVideo == 1 || autoPublishVideo == 2)
                    //    {
                    //        try
                    //        {
                    //            if (newsDetail.NewsInfo != null)
                    //            {
                    //                var doc = new HtmlDocument();
                    //                doc.LoadHtml(newsDetail.NewsInfo.Body);
                    //                Logger.WriteLog(Logger.LogType.Warning, "1 - " + newsDetail.NewsInfo.Body);
                    //                var lstVideoNode = doc.DocumentNode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes.Contains("videoid") && d.Attributes["class"].Value.Contains("VCSortableInPreviewMode"));
                    //                //Logger.WriteLog(Logger.LogType.Warning, "2 - " + NewtonJson.Serialize(lstVideoNode));
                    //                foreach (var htmlNode in lstVideoNode)
                    //                {
                    //                    var videoId = htmlNode.Attributes["videoid"].Value;
                    //                    if (!string.IsNullOrEmpty(videoId))
                    //                    {
                    //                        var video = MediaServices.GetDetailVideo(Utility.ConvertToInt(videoId));
                    //                        if (video.Id > 0)
                    //                        {
                    //                            try
                    //                            {
                    //                                if (video.Name == "_______" || video.Name == "")
                    //                                {
                    //                                    video.Name = news.Title;
                    //                                    video.EditedBy = news.LastModifiedBy;
                    //                                    MediaServices.SaveVideoName(video);
                    //                                    var primaryZoneTag = "";
                    //                                    var tagForPrimaryZone = MediaServices.GetVideoTagByZoneVideoId(video.ZoneId);
                    //                                    if (tagForPrimaryZone != null)
                    //                                    {
                    //                                        primaryZoneTag = tagForPrimaryZone.Aggregate(primaryZoneTag, (current, videoTagEntity) => current + (";" + videoTagEntity.Name));
                    //                                        if (!string.IsNullOrEmpty(primaryZoneTag)) primaryZoneTag = primaryZoneTag.Remove(0, 1);
                    //                                    }
                    //                                    //Logger.WriteLog(Logger.LogType.Trace, string.Format("SyncVideo(KeyVideo={0}, Tag={1}, noAdv={2})", video.KeyVideo, primaryZoneTag, noAdv));

                    //                                    var strResullt = VideoStorageServices.SyncVideoToVideoApi(newsDetail.NewsInfo.LastModifiedBy, video.Name, primaryZoneTag, video.ZoneId,
                    //                                                                              video.ZoneName,
                    //                                                                              string.Empty, video.VideoRelation, 0, false, video.Status,
                    //                                                                              video.KeyVideo);
                    //                                    Logger.WriteLog(Logger.LogType.Trace, strResullt);
                    //                                }
                    //                            }
                    //                            catch (Exception ex)
                    //                            {
                    //                                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    //                            }
                    //                            if (autoPublishVideo == 1)
                    //                            {
                    //                                MediaServices.PublishVideoInNews(video.Id, video.DistributionDate);
                    //                            }
                    //                            else
                    //                            {
                    //                                MediaServices.PublishVideoInNews(video.Id, news.DistributionDate);
                    //                            }
                    //                            var allowPushVideoSync =
                    //                                CmsChannelConfiguration.GetAppSettingInInt32("EnablePushVideoSync");
                    //                            if (allowPushVideoSync == 1)
                    //                            {
                    //                                PushVideoQueue(video, news.Id);
                    //                            }
                    //                        }
                    //                    }
                    //                }

                    //                // Publish Video Auto
                    //                if (newsDetail.NewsInfo.Type == 13)
                    //                {
                    //                    var videoNamespace = CmsChannelConfiguration.GetAppSetting("VIDEO_MANAGER_UPLOAD_NAMESPACE");
                    //                    var videoAuto = NewtonJson.Deserialize<dynamic>(newsDetail.NewsInfo.Avatar4);
                    //                    string fileName = Utility.ConvertToString(videoAuto.FileName);
                    //                    if (fileName.StartsWith(videoNamespace))
                    //                    {
                    //                        fileName = fileName.Substring(fileName.IndexOf("/") + 1);
                    //                    }
                    //                    var video = MediaServices.GetVideoByFileName(fileName);
                    //                    if (video != null)
                    //                    {
                    //                        //var log = MediaServices.PublishVideo(Utility.ConvertToInt(video.Id));
                    //                        var log = MediaServices.PublishVideoInNews(video.Id, news.DistributionDate);
                    //                        Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(log));
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    //        }
                    //    }
                    //    // Send SMS notify
                    //    if (CmsChannelConfiguration.GetAppSettingInBoolean("EnabledSms") &&
                    //        CmsChannelConfiguration.GetAppSettingInBoolean("EnabledSmsWhenPublishedNews"))
                    //    {
                    //        var username = newsDetail.NewsInfo.LastModifiedBy;
                    //        var user = SecurityServices.GetUserByUsername(username);
                    //        if (user != null)
                    //        {
                    //            SmsServices.SendSmsMsg(user.Mobile,
                    //                string.Format("Bạn vừa xuất bản tin: {0} - {1}",
                    //                    news.Title, CmsChannelConfiguration.GetAppSetting("FRONT_END_URL") + news.Url));
                    //        }
                    //    }                    
                }
            });
            threadProcess.Start();
        }
        public virtual void OnUpdateNewsWhichPublished(long newsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                try {
                    HttpContext.Current = context;
                    var newsDetail = new NewsServices().GetDetailCDData(newsId, "");
                    if (newsDetail != null && newsDetail.NewsInfo != null && newsDetail.NewsInfo.Status == (int)NewsStatus.Published)
                    {                        
                        //push data cd
                        try
                        {                            
                            var strParamValue = NewtonJson.Serialize(newsDetail, "yyyy-MM-dd'T'HH:mm:ss"); ;
                            var jsonKey = "{\"Id\":" + newsDetail.NewsInfo.Id + "}";
                            ContentDeliveryServices.PushToDataCD(strParamValue, "updatepublishnews", newsDetail.NewsInfo.DistributionDate.ToString("yyyy-MM-dd HH:mm"), jsonKey);                            
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, ex.Message);
                        }

                        // Update news PR
                        var news = newsDetail.NewsInfo;
                        if (news.IsPr)
                        {
                            #region Update NewsPr
                            var newsPr = new NewsServices().GetNewsPrByNewsId(news.Id);
                            if (newsPr != null)
                            {
                                var primaryZoneId =
                                    (from zone in newsDetail.NewsInZone where zone.IsPrimary select zone.ZoneId)
                                        .FirstOrDefault();
                                if (CmsPrServices.PublishNewsPr(news.LastModifiedBy, news.Id, newsPr.ContentId, newsPr.DistributionId,
                                                                primaryZoneId,
                                                                news.Title, news.Sapo, news.Avatar, news.Body,
                                                                news.DistributionDate, news.Url, news.WordCount))
                                {
                                    new NewsServices().UpdateNewsPrInfo(new NewsPrEntity
                                    {
                                        Id = newsPr.Id,
                                        Mode = newsPr.Mode,
                                        IsFocus = newsPr.IsFocus,
                                        PublishDate = news.DistributionDate,
                                        DistributionDate = news.DistributionDate,
                                        ExpireDate = news.DistributionDate.AddMinutes(newsPr.Duration),
                                        Duration = newsPr.Duration
                                    });
                                }
                            }
                            #endregion
                        }

                        // Push news to pega
                        PegaCmsServices.NotifyInsertNews(newsDetail);

                        #region
                        //    // Publish Video On Publish
                        //    var autoPublishVideo = CmsChannelConfiguration.GetAppSettingInInt32("AutoPublishVideoOnPublishNews");
                        //    if (autoPublishVideo == 1 || autoPublishVideo == 2)
                        //    {
                        //        #region Publish video in news
                        //        try
                        //        {
                        //            Logger.WriteLog(Logger.LogType.Debug, "Start Auto PublishVideo" + newsId);
                        //            if (newsDetail.NewsInfo != null)
                        //            {
                        //                var doc = new HtmlDocument();
                        //                doc.LoadHtml(newsDetail.NewsInfo.Body);
                        //                var lstVideoNode = doc.DocumentNode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes.Contains("videoid") && d.Attributes["class"].Value.Contains("VCSortableInPreviewMode"));
                        //                foreach (var htmlNode in lstVideoNode)
                        //                {
                        //                    var videoId = htmlNode.Attributes["videoid"].Value;
                        //                    if (!string.IsNullOrEmpty(videoId))
                        //                    {
                        //                        Logger.WriteLog(Logger.LogType.Debug, "Process Video" + videoId);
                        //                        var video = MediaServices.GetDetailVideo(Utility.ConvertToInt(videoId));
                        //                        if (video.Id > 0)
                        //                        {
                        //                            try
                        //                            {
                        //                                if (video.Name == "_______" || video.Name == "")
                        //                                {
                        //                                    video.Name = news.Title;
                        //                                    video.EditedBy = news.LastModifiedBy;
                        //                                    MediaServices.SaveVideoName(video);
                        //                                    var primaryZoneTag = "";
                        //                                    var tagForPrimaryZone = MediaServices.GetVideoTagByZoneVideoId(video.ZoneId);
                        //                                    if (tagForPrimaryZone != null)
                        //                                    {
                        //                                        primaryZoneTag = tagForPrimaryZone.Aggregate(primaryZoneTag, (current, videoTagEntity) => current + (";" + videoTagEntity.Name));
                        //                                        if (!string.IsNullOrEmpty(primaryZoneTag)) primaryZoneTag = primaryZoneTag.Remove(0, 1);
                        //                                    }
                        //                                    //Logger.WriteLog(Logger.LogType.Trace, string.Format("SyncVideo(KeyVideo={0}, Tag={1}, noAdv={2})", video.KeyVideo, primaryZoneTag, noAdv));

                        //                                    var strResullt = VideoStorageServices.SyncVideoToVideoApi(newsDetail.NewsInfo.LastModifiedBy, video.Name, primaryZoneTag, video.ZoneId,
                        //                                                                              video.ZoneName,
                        //                                                                              string.Empty, video.VideoRelation, 0, false, video.Status,
                        //                                                                              video.KeyVideo);
                        //                                    Logger.WriteLog(Logger.LogType.Trace, strResullt);
                        //                                }
                        //                            }
                        //                            catch (Exception ex)
                        //                            {
                        //                                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                        //                            }
                        //                            if (autoPublishVideo == 1)
                        //                            {
                        //                                Logger.WriteLog(Logger.LogType.Debug, NewtonJson.Serialize(MediaServices.PublishVideoInNews(video.Id, video.DistributionDate)));
                        //                            }
                        //                            else
                        //                            {
                        //                                Logger.WriteLog(Logger.LogType.Debug, NewtonJson.Serialize(MediaServices.PublishVideoInNews(video.Id, news.DistributionDate)));
                        //                            }
                        //                            try
                        //                            {
                        //                                var allowPushVideoSync =
                        //                                                                      CmsChannelConfiguration.GetAppSettingInInt32("EnablePushVideoSync");
                        //                                if (allowPushVideoSync == 1)
                        //                                {
                        //                                    PushVideoQueue(video, news.Id);
                        //                                }
                        //                            }
                        //                            catch (Exception ex)
                        //                            {
                        //                                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                        //                            }
                        //                        }
                        //                    }
                        //                }
                        //                // Publish Video Auto
                        //                if (newsDetail.NewsInfo.Type == 13)
                        //                {
                        //                    var videoAuto = NewtonJson.Deserialize<dynamic>(newsDetail.NewsInfo.Avatar4);
                        //                    var video = MediaServices.GetVideoByFileName(Utility.ConvertToString(videoAuto.FileName));
                        //                    if (video != null)
                        //                    {
                        //                        var log = MediaServices.PublishVideoInNews(video.Id, news.DistributionDate);
                        //                        Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(log));
                        //                    }
                        //                }
                        //            }
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            Logger.WriteLog(Logger.LogType.Error, ex.Message);
                        //        }
                        //        #endregion
                        //    }    
                        #endregion
                    }
                }
                catch(Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, "OnUpdateNewsWhichPublished =>" + ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnUnpublishNews(long newsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                //push data cd
                try
                {
                    var strParamValue = string.Empty;
                    UnpublishNewsEsData entityWithId = new UnpublishNewsEsData();
                    entityWithId.Id = newsId;
                    strParamValue = NewtonJson.Serialize(entityWithId);
                    var jsonKey = "{\"Id\":" + newsId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "removepublishnews", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }


                //Delete news PR
                try
                {
                    var newsPr = new NewsServices().GetNewsPrByNewsId(newsId);
                    if (newsPr != null)
                    {
                        if (newsPr.ViewPlusBannerId > 0)
                        {
                            dynamic obj = new System.Dynamic.ExpandoObject();
                            obj.newsId = newsId;
                            obj.banner_id = newsPr.ViewPlusBannerId;
                            obj.channel = CmsChannelConfiguration.CurrentChannelNamespace;

                            var jsonKey = "{\"Id\":" + newsId + "}";
                            var strPram = NewtonJson.Serialize(obj, "yyyy-MM-dd HH:mm:ss");
                            ContentDeliveryServices.PushToDataCD(strPram, "deletezoneviewplus", string.Empty, jsonKey);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }

                //chinhnb tam comment
                //var news = new NewsServices().GetDetail(newsId, "");
                //if (news != null)
                //{
                //    Delete cached for mobile(new version)

                //    DetectV3Services.ChannelvnEmergency(newsId, news.NewsInfo.Url,
                //    DetectV3Services.EnumMobileEmergencyCachedActionType.DeleteNews);
                //    UpdateExternalSiteServices.RemoveNewsOnTTVN(news.NewsInfo.Title);

                //    Delete data from Solr
                //    SolrServices.DeleteItemFromSolr(newsId);
                //}

                //// Delete from TTVN
                ////TagDistributionServices.DeleteTagShareArticle(newsId.ToString());
                //try
                //{
                //    // Unpublish Video In Content
                //    if (news.NewsInfo != null)
                //    {
                //        var doc = new HtmlDocument();
                //        doc.LoadHtml(news.NewsInfo.Body);
                //        var lstVideoNode = doc.DocumentNode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes.Contains("videoid") && d.Attributes["class"].Value.Contains("VCSortableInPreviewMode"));
                //        foreach (var htmlNode in lstVideoNode)
                //        {
                //            var videoId = htmlNode.Attributes["videoid"].Value;
                //            if (!string.IsNullOrEmpty(videoId))
                //            {
                //                var log = MediaServices.UnpublishVideo(Utility.ConvertToInt(videoId));
                //                Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(log));
                //            }
                //        }
                //        // Unpublish Video Auto
                //        if (news.NewsInfo.Type == 13)
                //        {
                //            var videoAuto = NewtonJson.Deserialize<dynamic>(news.NewsInfo.Avatar4);
                //            var video = MediaServices.GetVideoByFileName(Utility.ConvertToString(videoAuto.FileName));
                //            var log = MediaServices.UnpublishVideo(Utility.ConvertToInt(video.Id));
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{
                //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                //}
            });
            threadProcess.Start();
        }
        public virtual void OnUpdateNews(long newsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            HttpContext.Current = context;
            //var newsDetail = NewsServices.GetDetail(newsId, "");
            //if (newsDetail != null && newsDetail.NewsInfo != null)
            //{
            //    var news = newsDetail.NewsInfo;
            //    if (news.Status == (int)NewsStatus.WaitForPublish)
            //    {
            //        var entity = new NewsReviewEntity();
            //        entity.Title = news.Title;
            //        entity.SubTitle = news.SubTitle;
            //        entity.Sapo = news.Sapo;
            //        entity.Avatar = news.Avatar;
            //        entity.Body = news.Body;
            //        entity.CreateBy = extParams[0].ToString();
            //        entity.Domain = extParams[1].ToString();
            //        entity.NewsId = newsId;
            //        entity.Type = 1;

            //        var seo = SeoServices.GetSeoMetaNewsById(newsId);
            //        if(seo != null)
            //        {
            //            entity.MetaKeywords = seo.KeywordFocus;
            //            entity.MetaDescription = seo.MetaDescription;
            //            entity.MetaTitle = seo.MetaTitle;
            //        }
            //        ExpertSuggestionService.SendReview(entity, "");
            //    }
            //}
        }
        public virtual void OnSent(long newsId, params object[] extParams)
        {
            //var context = HttpContext.Current;
            //var threadProcess = new Thread(delegate ()
            //{
            //    HttpContext.Current = context;

            //    var newsDetail = NewsServices.GetDetail(newsId, "");
            //    if (newsDetail.NewsInfo != null)
            //    {
            //        var news = newsDetail.NewsInfo;

            //        EnumPermission[] permissionIds = new EnumPermission[1];
            //        List<string> hashkey;
            //        string sendUser, urlLink, title, message;
            //        object notify;
            //        // Push news to pega
            //        switch (news.Status)
            //        {
            //            //notify bai cho xuat ban
            //            case (int)NewsStatus.WaitForPublish:
            //                permissionIds[0] = EnumPermission.ArticleAdmin;

            //                var listuser = SecurityServices.GetUserListByPermissionListAndZoneList(news.ListZoneId, permissionIds);

            //                hashkey = new List<string>();
            //                if (listuser.Count > 0)
            //                {
            //                    hashkey.AddRange(listuser.Select(item => item.UserName.ToLower()));
            //                }
            //                var listFull = SecurityServices.GetListUserFullPermisstion();
            //                if (listFull.Count > 0)
            //                {
            //                    hashkey.AddRange(listFull.Select(item => item.UserName.ToLower()));
            //                }

            //                //Xóa chính mình khỏi danh sách nhận
            //                hashkey.Remove(news.LastModifiedBy.ToLower());

            //                sendUser = news.LastModifiedBy;
            //                urlLink = CmsChannelConfiguration.GetAppSetting("HOST_BOOKMARK") + "/DefaultV2.aspx#NewsListV2/5";
            //                title = "Có bài mới chờ xuất bản";
            //                message = string.Format("<a class='cmsnotify' href='{0}'><b>" + title + "</b></br><span class='ntfbody'>{1}</span></br>{2}</a>", urlLink, news.Title, sendUser);
            //                notify = new
            //                {
            //                    Icon = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD") + news.Avatar,
            //                    Title = title,
            //                    Body = news.Title,
            //                    Url = urlLink,
            //                    SendUser = sendUser,
            //                    MsgHtml = message
            //                };

            //                NotifyHelper.SendNotifyExtension(sendUser, notify, NotifyHelper.NotifyType.Info, hashkey.ToArray());
            //                NotifyHelper.SendNotify(sendUser, notify, NotifyHelper.NotifyType.Info, hashkey.ToArray());
            //                hashkey.Clear();
            //                break;
            //            //notify bai cho bien tap
            //            case (int)NewsStatus.WaitForEdit:

            //                permissionIds[0] = EnumPermission.ArticleEditor;

            //                var listuser2 = SecurityServices.GetUserListByPermissionListAndZoneList(news.ListZoneId, permissionIds);

            //                hashkey = new List<string>();
            //                if (listuser2.Count > 0)
            //                {
            //                    hashkey.AddRange(listuser2.Select(item => item.UserName.ToLower()));
            //                }

            //                var listFull2 = SecurityServices.GetListUserFullPermisstion();
            //                if (listFull2.Count > 0)
            //                {
            //                    hashkey.AddRange(listFull2.Select(item => item.UserName.ToLower()));
            //                }
            //                //Xóa chính mình khỏi danh sách nhận
            //                hashkey.Remove(news.LastModifiedBy.ToLower());

            //                sendUser = news.LastModifiedBy;
            //                urlLink = CmsChannelConfiguration.GetAppSetting("HOST_BOOKMARK") + "/DefaultV2.aspx#NewsListV2/2";
            //                title = "Có bài mới chờ biên tập";
            //                message = string.Format("<a class='cmsnotify' href='{0}'><b>" + title + "</b></br><span class='ntfbody'>{1}</span></br>{2}</a>", urlLink, news.Title, sendUser);
            //                notify = new
            //                {
            //                    Icon = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD") + news.Avatar,
            //                    Title = title,
            //                    Body = news.Title,
            //                    Url = urlLink,
            //                    SendUser = sendUser,
            //                    MsgHtml = message
            //                };

            //                NotifyHelper.SendNotifyExtension(sendUser, notify, NotifyHelper.NotifyType.Info, hashkey.ToArray());
            //                NotifyHelper.SendNotify(sendUser, notify, NotifyHelper.NotifyType.Info, hashkey.ToArray());
            //                hashkey.Clear();
            //                break;
            //        }
            //    }
            //});
            //threadProcess.Start();
        }
        public virtual void OnUpdateIsOnHomeNews(long newsId, bool isOnHome, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var strParamValue = string.Empty;
                    IsOnHomeNewsEsData entityWithId = new IsOnHomeNewsEsData();
                    entityWithId.Id = newsId;
                    entityWithId.IsOnHome = isOnHome;
                    strParamValue = NewtonJson.Serialize(entityWithId);
                    var jsonKey = "{\"Id\":" + newsId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateisonhome", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }                
            });
            threadProcess.Start();
        }
        public virtual void OnUpdateIsFocusNews(long newsId, bool isFocus, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var strParamValue = string.Empty;
                    IsFocusNewsEsData entityWithId = new IsFocusNewsEsData();
                    entityWithId.Id = newsId;
                    entityWithId.IsFocus = isFocus;
                    strParamValue = NewtonJson.Serialize(entityWithId);
                    var jsonKey = "{\"Id\":" + newsId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateisfocus", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }

        #region push cd to admicro
        public virtual void OnUpdateNewsToAdmicro(long newsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;

                var newsDetail = new NewsServices().GetDetailCDData(newsId, "");
                if (newsDetail != null && newsDetail.NewsInfo != null)
                {
                    var news = newsDetail.NewsInfo;
                    if (newsDetail.NewsInZone != null && newsDetail.NewsInZone.Count > 0)
                    {
                        foreach (var newsinzone in newsDetail.NewsInZone)
                        {
                            if (newsinzone.IsPrimary)
                            {
                                news.ZoneId = newsinzone.ZoneId;
                            }
                        }
                    }
                    //push data cd
                    try
                    {
                        ExternalServices.AdmicroServices.TranferNewsToAdmicro("UpdateNews", news);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();
        }
        public virtual void OnPublishNewsToAdmicro(long newsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;

                var newsDetail = new NewsServices().GetDetailCDData(newsId, "");
                if (newsDetail != null && newsDetail.NewsInfo != null)
                {
                    var news = newsDetail.NewsInfo;
                    if (newsDetail.NewsInZone != null && newsDetail.NewsInZone.Count > 0)
                    {
                        foreach (var newsinzone in newsDetail.NewsInZone)
                        {
                            if (newsinzone.IsPrimary)
                            {
                                news.ZoneId = newsinzone.ZoneId;
                            }
                        }
                    }
                    //push data cd
                    try
                    {
                        ExternalServices.AdmicroServices.TranferNewsToAdmicro("PublishNews", news);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();
        }
        public virtual void OnUpdateNewsWhichPublishedToAdmicro(long newsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;

                var newsDetail = new NewsServices().GetDetailCDData(newsId, "");
                if (newsDetail != null && newsDetail.NewsInfo != null)
                {
                    var news = newsDetail.NewsInfo;
                    if (newsDetail.NewsInZone != null && newsDetail.NewsInZone.Count > 0)
                    {
                        foreach (var newsinzone in newsDetail.NewsInZone)
                        {
                            if (newsinzone.IsPrimary)
                            {
                                news.ZoneId = newsinzone.ZoneId;
                            }
                        }
                    }
                    //push data cd
                    try
                    {
                        ExternalServices.AdmicroServices.TranferNewsToAdmicro("UpdateNewsWhichPublished", news);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();
        }
        public virtual void OnUnpublishNewsToAdmicro(long newsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;

                try
                {
                    if (newsId > 0)
                    {
                        ExternalServices.AdmicroServices.TranferDeleteNewsToAdmicro(newsId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnDeleteNewsToAdmicro(long newsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;

                try
                {
                    if (newsId > 0)
                    {
                        ExternalServices.AdmicroServices.TranferDeleteNewsToAdmicro(newsId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnTopNewsToAdmicro(string topNewsId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;

                try
                {
                    if (!string.IsNullOrEmpty(topNewsId))
                        ExternalServices.AdmicroServices.TranferDataNewsFocusAndTimeline(topNewsId);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        #endregion

        #endregion

        #region NewsPosition

        public virtual void OnChangeNewsPosition(int typeId, int zoneId, int order, long newsId, params object[] extParams)
        {
            try
            {
                var context = HttpContext.Current;
                var threadProcess = new Thread(delegate ()
                {
                    HttpContext.Current = context;
                    ChangeNewsPositionEsData currentNewsPosition = new ChangeNewsPositionEsData();
                    var jsonKey = "{\"ZoneId\":" + zoneId + ", \"TypeId\":" + typeId + "}";
                    currentNewsPosition.TypeId = typeId;
                    currentNewsPosition.ZoneId = zoneId;
                    currentNewsPosition.Order = order;
                    currentNewsPosition.NewsId = newsId;
                    currentNewsPosition.Bomd = false;
                    if (order > 0 && newsId > 0)
                    {
                        ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentNewsPosition), "updatenewsposition", string.Empty, jsonKey);
                    }
                    else
                    {
                        Logger.WriteLog(Logger.LogType.Trace, string.Format("Change news position typeId {0} ZoneId {1}", typeId, zoneId));
                    }

                    NewsPositionAndStream.UpdateNewsPositionData(typeId, zoneId);
                                   
                });
                threadProcess.Start();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OnChangeNewsPosition => " + ex.Message);
            }
        }

        public virtual void OnChangeNewsStreamPosition(int typeId, int zoneId, int order, long newsId, long currentNewsId, int newsTopHotId, DateTime toDate, string action, params object[] extParams)
        {
            try
            {
                var context = HttpContext.Current;
                var threadProcess = new Thread(delegate ()
                {
                    HttpContext.Current = context;
                    var currentNewsPosition = new ChangeNewsStreamPositionEsData();
                    var jsonKey = "{\"ZoneId\":" + 0 + ", \"TypeId\":" + typeId + ", \"SteamType\":" + zoneId + "}";
                    currentNewsPosition.StreamType = zoneId;
                    currentNewsPosition.TypeId = typeId;
                    currentNewsPosition.ZoneId = 0;
                    currentNewsPosition.Order = order;
                    currentNewsPosition.NewsId = newsId;
                    currentNewsPosition.NewsTopHotId = newsTopHotId;
                    currentNewsPosition.CurrentNewsId = currentNewsId;
                    currentNewsPosition.FromDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    currentNewsPosition.ToDate = toDate.ToString("yyyy/MM/dd HH:mm:ss");
                    currentNewsPosition.Action = action;
                    if (order > 0 && newsId > 0)
                    {
                        ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentNewsPosition), "updatenewsstreamposition", string.Empty, jsonKey);
                    }
                    else
                    {
                        Logger.WriteLog(Logger.LogType.Trace, string.Format("Change newsstream position StreamType {0} ZoneId {1}", typeId, zoneId));
                    }                   

                });
                threadProcess.Start();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OnChangeNewsPosition => " + ex.Message);
            }
        }

        public virtual void OnChangeAdtechStream(params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;

                NewsPositionAndStream.UpdateAdtechStreamData();
            });
            threadProcess.Start();
        }

        public virtual void OnChangeLinkPosition(int typeId, int zoneId, int order, long newsId, int type, string title, string avatar, string url, string sapo, params object[] extParams)
        {
            try
            {
                var context = HttpContext.Current;
                var threadProcess = new Thread(delegate ()
                {
                    HttpContext.Current = context;
                    var currentVideoPosition = new ChangeVideoPositionEsData();
                    var jsonKey = "{\"ZoneId\":" + zoneId + ", \"TypeId\":" + typeId + "}";
                    currentVideoPosition.TypeId = typeId;
                    currentVideoPosition.ZoneId = zoneId;
                    currentVideoPosition.Order = order;
                    currentVideoPosition.NewsId = newsId;
                    currentVideoPosition.Bomd = false;
                    currentVideoPosition.Type = type;//3089 + newsid=videoid -> obj video được cài, ten action giữ nguyên hoạt tự check
                    currentVideoPosition.Title = title;
                    currentVideoPosition.Avatar = avatar;
                    currentVideoPosition.Url = url;
                    currentVideoPosition.Sapo = sapo;

                    if (order > 0 && newsId > 0)
                    {
                        ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentVideoPosition), "updatenewsposition", string.Empty, jsonKey);
                    }
                    else
                    {
                        Logger.WriteLog(Logger.LogType.Trace, string.Format("Change video position typeId {0} ZoneId {1}", typeId, zoneId));
                    }

                    //NewsPositionAndStream.UpdateNewsPositionData(typeId, zoneId);

                });
                threadProcess.Start();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OnChangeNewsPosition => " + ex.Message);
            }
        }

        public virtual void OnChangeNewsMobileStream(long newsId, int zoneId, bool isHighlight, int status, params object[] extParams)
        {
            try
            {
                var context = HttpContext.Current;
                var threadProcess = new Thread(delegate ()
                {
                    HttpContext.Current = context;
                    var currentNewsMobileStream = new ChangeNewsMobileStreamEsData();
                    var jsonKey = "{\"ZoneId\":" + zoneId + ", \"NewsId\":" + newsId + "}";
                    currentNewsMobileStream.NewsId = newsId;
                    currentNewsMobileStream.ZoneId = zoneId;
                    currentNewsMobileStream.IsHighlight = isHighlight;
                    currentNewsMobileStream.Status = status;
                    if (newsId > 0)
                    {
                        ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentNewsMobileStream), "updatenewsmobilestream", string.Empty, jsonKey);
                    }
                    else
                    {
                        Logger.WriteLog(Logger.LogType.Trace, string.Format("Change news mobile stream NewsId {0} ZoneId {1}", newsId, zoneId));
                    }                    
                });
                threadProcess.Start();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OnChangeNewsMobileStream => " + ex.Message);
            }
        }

        #endregion

        #region Tag

        public virtual void OnAddNewTag(TagEntity tag, int zoneId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"Id\":" + tag.Id + "}";
                    ChangeTagEsData data = new ChangeTagEsData();
                    data.TagInfo = tag;
                    data.ZoneId = zoneId;
                    var strPram = NewtonJson.Serialize(data, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "addtag", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                // Delete cached for tag (new version)
                //DetectV3Services.ChannelvnEmergency(tag.Id, "", DetectV3Services.EnumMobileEmergencyCachedActionType.DeleteTag);                
            });
            threadProcess.Start();
        }
        public virtual void OnUpdateTag(TagEntity tag, int zoneId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"Id\":" + tag.Id + "}";
                    ChangeTagEsData data = new ChangeTagEsData();
                    data.TagInfo = tag;
                    data.ZoneId = zoneId;
                    var strPram = NewtonJson.Serialize(data, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updatetag", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                // Delete cached for tag (new version)
                //DetectV3Services.ChannelvnEmergency(tag.Id, "", DetectV3Services.EnumMobileEmergencyCachedActionType.DeleteTag);                
            });
            threadProcess.Start();
        }
        public virtual void OnDeleteTag(long tagId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"Id\":" + tagId + "}";
                    var strPram = NewtonJson.Serialize(string.Empty, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "removetag", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                // Delete cached for tag (new version)
                //DetectV3Services.ChannelvnEmergency(tagId, "", DetectV3Services.EnumMobileEmergencyCachedActionType.DeleteTag);
            });
            threadProcess.Start();
        }
        public virtual void OnChangeTag(long tagId, string listAdd, string listDelete, int tagMode, params object[] extParams)
        {
            try
            {
                var context = HttpContext.Current;
                var threadProcess = new Thread(delegate ()
                {
                    HttpContext.Current = context;
                    var currentTag = new TagNewsEsData();
                    currentTag.TagId = tagId;
                    currentTag.TagMode = tagMode;
                    currentTag.ListIdAdded = listAdd.Split(';');
                    currentTag.ListIdRemoved = listDelete.Split(';');

                    ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentTag), "updatetagnews", string.Empty, string.Empty);
                });
                threadProcess.Start();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OnChangeThreadEmbed => " + ex.Message);
            }
        }
        #endregion

        #region Video
        public virtual void OnUpdateVideo(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, string channelIdList, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;
                
                if (videoEntity != null)
                {
                    var videoEntityDB = new MediaServices().GetDetailVideo(videoEntity.Id);
                    if (videoEntityDB != null)
                    {
                        videoEntity = videoEntityDB;
                    }
                    try
                    {
                        var tagids = new string[] { };
                        if (!string.IsNullOrEmpty(tagIds))
                        {
                            tagids = tagIds.Split(';');
                        }
                        var zoneidlist = new string[] { };
                        if (!string.IsNullOrEmpty(zoneIdList))
                        {
                            zoneidlist = zoneIdList.Split(';');
                        }
                        var playlistidlist = new string[] { };
                        if (!string.IsNullOrEmpty(playlistIdList))
                        {
                            playlistidlist = playlistIdList.Split(';');
                        }
                        var channelidlist = new string[] { };
                        if (!string.IsNullOrEmpty(channelIdList))
                        {
                            channelidlist = channelIdList.Split(';');
                        }
                        var videoDetail = new
                        {
                            VideoInfo = videoEntity,
                            TagIds = tagids,
                            ZoneIdList = zoneidlist,
                            PlaylistIdList = playlistidlist,
                            ChannelIdList = channelidlist
                        };
                        var strParamValue = NewtonJson.Serialize(videoDetail, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + videoEntity.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatevideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }              
            });
            threadProcess.Start();

        }

        public virtual void OnPublishVideo(int videoId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                var videoDetail = new MediaServices().GetDetailVideoForEdit(videoId);
                if (videoDetail != null)
                {                    
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(videoDetail, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + videoId + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "publishvideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        
        public virtual void OnUnpublishVideo(int videoId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;                
                try
                {
                    var jsonKey = "{\"Id\":" + videoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "unpublishvideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }

        public virtual void OnDeleteVideo(int videoId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"Id\":" + videoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deletevideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }

        #endregion

        #region PlayList
        public virtual void OnUpdatePlayList(PlaylistEntity playlistEntity, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                var playListDetail = new MediaServices().GetVideoPlaylistById(playlistEntity.Id);
                var playListInfo = playListDetail.PlaylistInfo;
                if (playListDetail != null && playListInfo != null && (playListInfo.Status == (int)EnumPlayListStatus.Published || playListInfo.Status == (int)EnumPlayListStatus.Unpublished))
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(playListDetail, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + playlistEntity.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updateplaylist", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();
        }
        public virtual void OnPublishPlayList(int playlistId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                var playListDetail = new MediaServices().GetVideoPlaylistById(playlistId);
                if (playListDetail != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(playListDetail, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + playlistId + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "publishplaylist", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();
        }
        public virtual void OnUnPublishPlayList(int playlistId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;
               
                try
                {                   
                    var jsonKey = "{\"Id\":" + playlistId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "unpublishplaylist", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }                
            });
            threadProcess.Start();
        }
        public virtual void OnUpdateVideoListInPlayList(int playlistId,string videoIdList, string deleteVideoIds, string waitPublishVideoIds, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var listId = new string[] { };
                    if (!string.IsNullOrEmpty(videoIdList))
                    {
                        listId = videoIdList.Split(';');
                    }
                    var listIdDel = new string[] { };
                    if (!string.IsNullOrEmpty(deleteVideoIds))
                    {
                        listIdDel = deleteVideoIds.Split(';');
                    }
                    var listIdWait = new string[] { };
                    if (!string.IsNullOrEmpty(waitPublishVideoIds))
                    {
                        listIdWait = waitPublishVideoIds.Split(';');
                    }
                    var strParamValue = NewtonJson.Serialize(new { listvideoaddid = listId, lisvideodeleteid= listIdDel, listvideowaitpublishid= listIdWait }, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"PlaylistId\":" + playlistId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatevideoplaylist", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void UpdateVideoPriorotyInPlayList(int playlistId, List<VideoPriorityEntity> listVideoPriority, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {                    
                    var strParamValue = NewtonJson.Serialize(listVideoPriority, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"PlaylistId\":" + playlistId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatepriorityvideoplaylist", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnMoveTrashPlayList(int playlistId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + playlistId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "movetrashplaylist", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        #endregion

        #region VideoPosition

        //Box chương trình nổi bật VideoHighLight = 8
        public virtual void OnUpdateVideoHighlightEmbed(int typeId, string listObject, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    //action: update_video_highlight_embed -> updatevideoposition
                    var jsonKey = "{\"TypeId\":" + typeId + "}";
                    ContentDeliveryServices.PushToDataCD(listObject, "updatevideoposition", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }

        //Box Video(Playlist) mục VideoBoxMuc = 9
        public virtual void OnUpdateVideoPlayEmbed(int typeId, string listObject, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    //action: update_video_play_embed -> updatevideoposition
                    var jsonKey = "{\"TypeId\":" + typeId + "}";
                    ContentDeliveryServices.PushToDataCD(listObject, "updatevideoposition", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }

        //Chương trình hot trang home VideoProgramHotHome = 10
        //Video chương trình Hot trang mục VideoProgramHotMuc=11
        public virtual void OnUpdateProgramHotHomeEmbed(int typeId, int zoneId, string listObject, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"TypeId\":" + typeId + ",\"ZoneId\":" + zoneId + "}";
                    if (typeId == (int)VideoPositionType.VideoProgramHotHome)
                    {
                        //action: update_program_hot_home_embed -> updatevideoposition
                        ContentDeliveryServices.PushToDataCD(listObject, "updatevideoposition", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    if (typeId == (int)VideoPositionType.VideoProgramHotMuc)
                    {
                        //action: update_program_hot_muc_embed -> updatevideoposition
                        ContentDeliveryServices.PushToDataCD(listObject, "updatevideoposition", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }

        #endregion

        #region ProgramChannel: live
        public virtual void OnUpdateProgramChannel(ProgramChannelEntity programChannel, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;
                
                if (programChannel != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(programChannel, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + programChannel.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updateprogramchannel", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnDeleteProgramChannel(int programChannelId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {                    
                    var jsonKey = "{\"Id\":" + programChannelId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deleteprogramchannel", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }
        public virtual void OnMoveUpProgramChannel(int programChannelId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + programChannelId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "moveupprogramchannel", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }
        public virtual void OnMoveDownProgramChannel(int programChannelId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + programChannelId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "movedownprogramchannel", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }

        #region Schedule
        public virtual void OnAddProgramSchedule(ProgramScheduleEntity programSchedule, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (programSchedule != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(programSchedule, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + programSchedule.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "addprogramschedule", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnUpdateProgramSchedule(ProgramScheduleEntity programSchedule, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (programSchedule != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(programSchedule, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + programSchedule.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updateprogramschedule", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnDeleteProgramSchedule(int programScheduleId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + programScheduleId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deleteprogramschedule", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }
        #endregion

        #region ScheduleDetail
        public virtual void OnUpdateListProgramScheduleDetail(List<ProgramScheduleDetailEntity> programScheduleDetails, string listDeleteId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;
                
                try
                {
                    var strParamValue = NewtonJson.Serialize(new { ListProgramScheduleDetail=programScheduleDetails, ListDeleteId=listDeleteId }, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + programScheduleDetails[0].ProgramScheduleId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatelistprogramscheduledetail", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }                
            });
            threadProcess.Start();

        }
        #endregion

        #endregion

        #region ZoneVideo
        public virtual void OnInsertZoneVideo(ZoneVideoEntity zoneVideo, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (zoneVideo != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(zoneVideo, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + zoneVideo.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertzonevideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnUpdateZoneVideo(ZoneVideoEntity zoneVideo, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (zoneVideo != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(zoneVideo, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + zoneVideo.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatezonevideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnDeleteZoneVideo(int zoneVideoId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + zoneVideoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deletezonevideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }
        public virtual void OnMoveUpZoneVideo(int zoneVideoId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + zoneVideoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "moveupzonevideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnMoveDownZoneVideo(int zoneVideoId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + zoneVideoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "movedownzonevideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }

        #endregion

        #region Photo
        public virtual void OnInsertPhoto(PhotoEntity photoEntity, string tagIds, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (photoEntity != null)
                {                    
                    try
                    {
                        var tagids = new string[] { };
                        if (!string.IsNullOrEmpty(tagIds))
                        {
                            tagids = tagIds.Split(';');
                        }                        
                        var strParamValue = NewtonJson.Serialize(photoEntity, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + photoEntity.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertphoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnUpdatePhoto(PhotoEntity photoEntity, string tagIds, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (photoEntity != null)
                {
                    try
                    {
                        var tagids = new string[] { };
                        if (!string.IsNullOrEmpty(tagIds))
                        {
                            tagids = tagIds.Split(';');
                        }
                        var strParamValue = NewtonJson.Serialize(photoEntity, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + photoEntity.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatephoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnPublishPhoto(long photoId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;               
                try
                {                    
                    var jsonKey = "{\"Id\":" + photoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "publishphoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }                
            });
            threadProcess.Start();

        }
        public virtual void OnUnPublishPhoto(long photoId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"Id\":" + photoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "unpublishphoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnUnPublishPhotoIds(string photoIds, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"ListPhotoId\":" + photoIds + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "unpublishphotoids", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnDeletePhoto(long photoId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"Id\":" + photoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deletephoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnUpdatePhotoInAlbum(int albumId, string photoIdList, string deletePhotoIds, string waitPublishPhotoIds, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var listId = new string[] { };
                    if (!string.IsNullOrEmpty(photoIdList))
                    {
                        listId = photoIdList.Split(';');
                    }
                    var listIdDel = new string[] { };
                    if (!string.IsNullOrEmpty(deletePhotoIds))
                    {
                        listIdDel = deletePhotoIds.Split(';');
                    }
                    var listIdWait = new string[] { };
                    if (!string.IsNullOrEmpty(waitPublishPhotoIds))
                    {
                        listIdWait = waitPublishPhotoIds.Split(';');
                    }
                    var strParamValue = NewtonJson.Serialize(new { listphotoaddid = listId, listphotodeleteid = listIdDel, listphotowaitpublishid = listIdWait }, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"AlbumId\":" + albumId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatephotoinalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        #endregion
        #region PhotoAlbum
        public virtual void OnInsertAlbum(AlbumEntity albumEntity, string tagIds, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (albumEntity != null)
                {
                    try
                    {
                        var tagids = new string[] { };
                        if (!string.IsNullOrEmpty(tagIds))
                        {
                            tagids = tagIds.Split(';');
                        }
                        var strParamValue = NewtonJson.Serialize(albumEntity, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + albumEntity.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnUpdateAlbum(AlbumEntity albumEntity, string tagIds, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (albumEntity != null)
                {
                    try
                    {
                        var tagids = new string[] { };
                        if (!string.IsNullOrEmpty(tagIds))
                        {
                            tagids = tagIds.Split(';');
                        }
                        var strParamValue = NewtonJson.Serialize(albumEntity, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + albumEntity.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatealbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();
        }
        public virtual void OnPublishAlbum(int albumId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;
                try
                {
                    var jsonKey = "{\"Id\":" + albumId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "publishalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }
        public virtual void OnUnPublishAlbum(int albumId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"Id\":" + albumId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "unpublishalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnDeleteAlbum(int albumId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                try
                {
                    var jsonKey = "{\"Id\":" + albumId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deletealbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        #endregion

        #region ZonePhoto
        public virtual void OnInsertZonePhoto(ZonePhotoEntity zonePhoto, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (zonePhoto != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(zonePhoto, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + zonePhoto.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertzonephoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnUpdateZonePhoto(ZonePhotoEntity zonePhoto, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (zonePhoto != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(zonePhoto, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + zonePhoto.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatezonephoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnDeleteZonePhoto(int zonePhotoId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + zonePhotoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deletezonephoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }
        public virtual void OnMoveUpZonePhoto(int zonePhotoId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + zonePhotoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "moveupzonephoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }
        public virtual void OnMoveDownZonePhoto(int zonePhotoId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + zonePhotoId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "movedownzonephoto", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();
        }

        #endregion

        #region VideoLabel
        public virtual void OnUpdateVideoLabel(VideoLabelEntity videoLabel, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                if (videoLabel != null)
                {
                    try
                    {
                        var strParamValue = NewtonJson.Serialize(videoLabel, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + videoLabel.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatevideolabel", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
            });
            threadProcess.Start();

        }
        public virtual void OnDeleteVideoLabel(int labelId, params object[] extParams)
        {
            var currentContext = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = currentContext;

                try
                {
                    var jsonKey = "{\"Id\":" + labelId + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deletevideolabel", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            threadProcess.Start();

        }

        #endregion

        #region Thread

        public virtual void OnAddNewThread(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                ThreadFullInfoData objEs = new ThreadFullInfoData();
                thread.CreatedDate = DateTime.Now;
                thread.ModifiedDate = DateTime.Now;
                objEs.ThreadInfo = thread;
                objEs.ZoneId = zoneId;
                objEs.ZoneIdList = zoneIdList;
                objEs.RelationThread = relationThread;
                var jsonKey = "{\"Id\":" + thread.Id + "}";
                var strPram = NewtonJson.Serialize(objEs, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "insertthread", string.Empty, jsonKey);
            });
            threadProcess.Start();
        }
        public virtual void OnUpdateThread(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                ThreadFullInfoData objEs = new ThreadFullInfoData();
                thread.CreatedDate = DateTime.Now;
                thread.ModifiedDate = DateTime.Now;                
                objEs.ThreadInfo = thread;
                objEs.ZoneId = zoneId;
                objEs.ZoneIdList = zoneIdList;
                objEs.RelationThread = relationThread;
                var jsonKey = "{\"Id\":" + thread.Id + "}";
                var strPram = NewtonJson.Serialize(objEs, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatethread", string.Empty, jsonKey);
            });
            threadProcess.Start();
        }
        public virtual void OnDeleteThread(long threadId, params object[] extParams)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
                var jsonKey = "{\"Id\":" + threadId + "}";
                var strPram = NewtonJson.Serialize(string.Empty, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "removethread", string.Empty, jsonKey);
            });
            threadProcess.Start();
        }
        public virtual void OnChangeThreadEmbed(string listThreadId, int typeId, int zoneId, params object[] extParams)
        {
            try
            {
                var context = HttpContext.Current;
                var threadProcess = new Thread(delegate ()
                {
                    HttpContext.Current = context;
                    ChangeThreadEmbedData currentThreadEmbed = new ChangeThreadEmbedData();
                    var ArrThreadId = listThreadId.Split(',');
                    var jsonKey = "{\"ZoneId\":" + zoneId + ", \"TypeId\":" + typeId + "}";
                    currentThreadEmbed.TypeId = typeId;
                    currentThreadEmbed.ZoneId = zoneId;
                    currentThreadEmbed.LastModifiedDate = DateTime.Now.ToString("yyyy/MM/dd HH:ss:mm");
                    currentThreadEmbed.ArrThreadId = ArrThreadId;

                    ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentThreadEmbed), "updatethreadembed", string.Empty, jsonKey);
                });
                threadProcess.Start();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OnChangeThreadEmbed => " + ex.Message);
            }
        }
        public virtual void OnChangeThread(long threadId, string listAdd, string listDelete, params object[] extParams)
        {
            try
            {
                var context = HttpContext.Current;
                var threadProcess = new Thread(delegate ()
                {
                    HttpContext.Current = context;
                    ThreadNewsEsData currentThread = new ThreadNewsEsData();                                     
                    currentThread.ThreadId = threadId;
                    currentThread.ListIdAdded = listAdd.Split(';');
                    currentThread.ListIdRemoved = listDelete.Split(';');

                    ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentThread), "updatethreadnews", string.Empty, string.Empty);
                });
                threadProcess.Start();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OnChangeThreadEmbed => " + ex.Message);
            }
        }

        #endregion

        #endregion

        #region Methods

        public virtual void UpdateRedisDataForHomepage()
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
            });
        }
        public virtual void UpdateRedisDataForZonePage(int zoneId)
        {
            var context = HttpContext.Current;
            var threadProcess = new Thread(delegate ()
            {
                HttpContext.Current = context;
            });
        }

        #endregion

        #region DataContract
        [DataContract]
        public class ChangeNewsPositionEsData
        {
            [DataMember]
            public int TypeId { get; set; }
            [DataMember]
            public int ZoneId { get; set; }
            [DataMember]
            public int Order { get; set; }
            [DataMember]
            public long NewsId { get; set; }
            [DataMember]
            public bool Bomd { get; set; }
            [DataMember]
            public string DistributionDate { get; set; }
        }
        [DataContract]
        public class ChangeNewsMobileStreamEsData
        {
            [DataMember]
            public long NewsId { get; set; }

            [DataMember]
            public int ZoneId { get; set; }

            [DataMember]
            public bool IsHighlight { get; set; }

            [DataMember]
            public int Status { get; set; }
        }
        public class ChangeVideoPositionEsData
        {
            [DataMember]
            public int TypeId { get; set; }
            [DataMember]
            public int ZoneId { get; set; }
            [DataMember]
            public int Order { get; set; }
            [DataMember]
            public long NewsId { get; set; }
            [DataMember]
            public bool Bomd { get; set; }
            [DataMember]
            public string DistributionDate { get; set; }
            [DataMember]
            public string Title { get; set; }
            [DataMember]
            public string Avatar { get; set; }
            [DataMember]
            public string Url { get; set; }
            [DataMember]
            public string Sapo { get; set; }
            [DataMember]
            public int Type { get; set; }
        }
        [DataContract]
        public class ChangeNewsStreamPositionEsData
        {
            [DataMember]
            public int TypeId { get; set; }
            [DataMember]
            public int StreamType { get; set; }
            [DataMember]
            public int ZoneId { get; set; }
            [DataMember]
            public int Order { get; set; }
            [DataMember]
            public long NewsId { get; set; }
            [DataMember]
            public long CurrentNewsId { get; set; }
            [DataMember]
            public int NewsTopHotId { get; set; }
            [DataMember]
            public string Action { get; set; }
            [DataMember]
            public string ToDate { get; set; }
            [DataMember]
            public string FromDate { get; set; }
        }

        [DataContract]
        public class ChangeTagEsData
        {
            [DataMember]
            public TagEntity TagInfo { get; set; }
            [DataMember]
            public int ZoneId { get; set; }
        }

        [DataContract]
        public class TagNewsEsData
        {
            [DataMember]
            public long TagId { get; set; }
            [DataMember]
            public string[] ListIdRemoved { get; set; }
            [DataMember]
            public string[] ListIdAdded { get; set; }
            [DataMember]
            public int TagMode { get; set; }
        }

        [DataContract]
        public class UnpublishNewsEsData
        {
            [DataMember]
            public long Id { get; set; }
        }

        [DataContract]
        public class IsOnHomeNewsEsData
        {
            [DataMember]
            public long Id { get; set; }
            [DataMember]
            public bool IsOnHome { get; set; }
        }

        [DataContract]
        public class IsFocusNewsEsData
        {
            [DataMember]
            public long Id { get; set; }
            [DataMember]
            public bool IsFocus { get; set; }
        }

        [DataContract]
        public class ChangeThreadEmbedData
        {
            [DataMember]
            public int TypeId { get; set; }
            [DataMember]
            public int ZoneId { get; set; }
            [DataMember]
            public string[] ArrThreadId { get; set; }
            [DataMember]
            public string LastModifiedDate { get; set; }
        }

        [DataContract]
        public class ThreadNewsEsData
        {
            [DataMember]
            public long ThreadId { get; set; }           
            [DataMember]
            public string[] ListIdRemoved { get; set; }
            [DataMember]
            public string[] ListIdAdded { get; set; }

        }

        [DataContract]
        public class ThreadFullInfoData
        {            
            [DataMember]
            public ThreadEntity ThreadInfo { get; set; }
            [DataMember]
            public int ZoneId { get; set; }
            [DataMember]
            public string ZoneIdList { get; set; }
            [DataMember]
            public string RelationThread { get; set; }           

        }

        #endregion
    }
}