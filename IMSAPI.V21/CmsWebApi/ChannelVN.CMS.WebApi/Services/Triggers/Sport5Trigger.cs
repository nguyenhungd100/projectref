﻿using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.WcfMapping.ServiceSuggestTag;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;

namespace ChannelVN.CMS.WebApi.Services.Triggers
{
    public class Sport5Trigger : CmsTriggerBase
    {
        #region Property

        public override TagSugguestConnectionName TagSugguestChannel
        {
            get { return TagSugguestConnectionName.Kenh14_CMS; }
        }

        public override CmsPrServices.ChannelId CmsPrChannelId
        {
            get { return CmsPrServices.ChannelId.Sport5; }
        }

        public override PegaCmsServices.PegaSource PegaCmsSource
        {
            get { return PegaCmsServices.PegaSource.Sport5; }
        }

        public override StatisticTagChannel StatisticTagChannelForUpdateView
        {
            get { return StatisticTagChannel.Sport5; }
        }

        #endregion
    }
}