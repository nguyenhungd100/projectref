﻿using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.WcfMapping.ServiceSuggestTag;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;

namespace ChannelVN.CMS.WebApi.Services.Triggers
{
    public class DemoTrigger : CmsTriggerBase
    {
        #region Property

        public override TagSugguestConnectionName TagSugguestChannel
        {
            get { return TagSugguestConnectionName.Kenh14_CMS; }
        }

        public override CmsPrServices.ChannelId CmsPrChannelId
        {
            get { return CmsPrServices.ChannelId.Demo; }
        }

        public override PegaCmsServices.PegaSource PegaCmsSource
        {
            get { return PegaCmsServices.PegaSource.Demo; }
        }

        public override StatisticTagChannel StatisticTagChannelForUpdateView
        {
            get { return StatisticTagChannel.Demo; }
        }

        #endregion
    }
}