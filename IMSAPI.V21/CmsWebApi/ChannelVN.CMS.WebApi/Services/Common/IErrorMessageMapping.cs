﻿using System.ServiceModel;

namespace ChannelVN.CMS.WebApi.Services.Common
{
    [ServiceContract]
    public interface IErrorMessageMapping
    {
        [OperationContract]
        ErrorMessageEntity[] GetErrorMessage();
    }
}
