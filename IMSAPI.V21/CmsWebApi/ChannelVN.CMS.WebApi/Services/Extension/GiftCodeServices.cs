﻿using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.InfoGraph.Entity.ErrorCode;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class GiftCodeServices
    {
        public List<GiftCodeEntity> GetByNewsId(long newsId)
        {
            return GiftCodeBo.GetByNewsId(newsId);
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}