﻿using ChannelVN.CMS.Common;
using ChannelVN.InfoGraph.Bo;
using ChannelVN.InfoGraph.Entity;
using ChannelVN.InfoGraph.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class InfoGraphServices
    {
        #region Info Graph
        public List<InfoGraphEntity> ListInfoGraph(string keyword, string createdBy, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphBo.ListInfoGraph(keyword, createdBy, status, pageIndex, pageSize, ref totalRow);
        }
        public InfoGraphEntity SelectInfoGraph(int id)
        {
            return InfoGraphBo.SelectInfoGraph(id);
        }
        public WcfActionResponse InsertInfoGraph(InfoGraphEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.InsertInfoGraph(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                //actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                //actionResponse.Data = id.ToString();
                //actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                actionResponse = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;

        }
        public WcfActionResponse UpdateInfoGraph(InfoGraphEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.UpdateInfoGraph(elm) == ErrorMapping.ErrorCodes.Success)
            {
                //actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                //actionResponse.Data = elm.Id.ToString();
                //actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                actionResponse = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteInfoGraph(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.DeleteInfoGraph(id) == ErrorMapping.ErrorCodes.Success)
            {
                //actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                //actionResponse.Data = id.ToString();
                //actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                actionResponse = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Info Graph Chart
        public InfoGraphChartEntity SelectInfoGraphChart(string name, int type)
        {
            return InfoGraphBo.SelectInfoGraphChart(name, type);
        }
        public WcfActionResponse InsertInfoGraphChart(InfoGraphChartEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.InsertInfoGraphChart(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Info Graph Folder
        public List<InfoGraphFolderEntity> ListInfoGraphFolder(string keyword, string createdBy, int visible, int pub, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphBo.ListInfoGraphFolder(keyword, createdBy, visible, pub, type, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertInfoGraphFolder(InfoGraphFolderEntity elm, ref int id)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.InsertInfoGraphFolder(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.InsertInfoGraphFolder(elm, ref id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateInfoGraphFolder(InfoGraphFolderEntity elm)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.UpdateInfoGraphFolder(elm) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = elm.Id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.UpdateInfoGraphFolder(elm);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteInfoGraphFolder(int id)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.DeleteInfoGraphFolder(id) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.DeleteInfoGraphFolder(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertInfoGraphUsage(int folderId, string username, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.InsertInfoGraphUsage(folderId, username, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteInfoGraphUsage(int folderId, string username)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.DeleteInfoGraphUsage(folderId, username) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = folderId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region InfoGraph Image
        public List<InfoGraphImageEntity> ListInfoGraphImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphBo.ListInfoGraphImage(keyword, folderId, pageIndex, pageSize, ref totalRow);
        }
        public InfoGraphImageEntity SelectInfoGraphImage(int id)
        {
            return InfoGraphBo.SelectInfoGraphImage(id);
        }
        public WcfActionResponse InsertInfoGraphImage(InfoGraphImageEntity elm, ref int id)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.InsertInfoGraphImage(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.InsertInfoGraphImage(elm, ref id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateInfoGraphImage(InfoGraphImageEntity elm)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.UpdateInfoGraphImage(elm) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = elm.Id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.UpdateInfoGraphImage(elm);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteInfoGraphImage(int id)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.DeleteInfoGraphImage(id) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.DeleteInfoGraphImage(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region InfoGraph Svg
        public List<InfoGraphSvgEntity> ListInfoGraphSvg(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphBo.ListInfoGraphSvg(keyword, folderId, pageIndex, pageSize, ref totalRow);
        }
        public InfoGraphSvgEntity SelectInfoGraphSvg(int id)
        {
            return InfoGraphBo.SelectInfoGraphSvg(id);
        }
        public List<InfoGraphSvgFolderEntity> SvgFolder(bool showAll, string channelName, string keyWord)
        {
            return InfoGraphBo.SvgFolder(showAll, channelName, keyWord);
        }
        public WcfActionResponse UpdateSvg(InfoGraphSvgEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.UpdateSvg(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteSvg(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.DeleteSvg(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse InsertSvgChannel(string channelName, int svgFolderId)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.InsertSvgChannel(channelName, svgFolderId) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = channelName;
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.InsertSvgChannel(channelName, svgFolderId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteSvgChannel(string channelName)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.DeleteSvgChannel(channelName) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = channelName;
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.DeleteSvgChannel(channelName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region InfoGraph Template
        public List<InfoGraphTemplateEntity> ListInfoGraphTemplate(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            return InfoGraphBo.ListInfoGraphTemplate(keyword, folderId, pageIndex, pageSize, ref totalRow);
        }
        public InfoGraphTemplateEntity SelectInfoGraphTemplate(int id)
        {
            return InfoGraphBo.SelectInfoGraphTemplate(id);
        }
        public WcfActionResponse InsertInfoGraphTemplate(InfoGraphTemplateEntity elm, ref int id)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.InsertInfoGraphTemplate(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.InsertInfoGraphTemplate(elm, ref id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateInfoGraphTemplate(InfoGraphTemplateEntity elm)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.UpdateInfoGraphTemplate(elm) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = elm.Id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.UpdateInfoGraphTemplate(elm);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteInfoGraphTemplate(int id)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.DeleteInfoGraphTemplate(id) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = id.ToString();
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.DeleteInfoGraphTemplate(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertTemplateChannel(InfoGraphTemplateChannelEntity elm)
        {
            //var actionResponse = WcfActionResponse.CreateSuccessResponse();
            //if (InfoGraphBo.InsertTemplateChannel(elm) == ErrorMapping.ErrorCodes.Success)
            //{
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
            //    actionResponse.Data = "";
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            //}
            //else
            //{
            //    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
            //    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            //}
            //return actionResponse;

            var errorCode = InfoGraphBo.InsertTemplateChannel(elm);
            return errorCode == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateTemplateChannel(InfoGraphTemplateChannelEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InfoGraphBo.UpdateTemplateChannel(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<InfoGraphTemplateChannelEntity> ListTemplateChannel(int status)
        {
            return InfoGraphBo.ListTemplateChannel(status);
        }
        public InfoGraphTemplateChannelEntity SelectTemplateChannel(int id)
        {
            return InfoGraphBo.SelectTemplateChannel(id);
        }
        #endregion       
    }
}