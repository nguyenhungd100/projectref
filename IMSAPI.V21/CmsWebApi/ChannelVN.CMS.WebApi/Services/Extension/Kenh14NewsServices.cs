﻿using ChannelVN.CMS.BO.External.Kenh14.News;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using System;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class Kenh14NewsServices
    {
        public WcfActionResponse UpdateSpecialNewsRelation(long newsId, int zoneId, string newsRelationSpecialId)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = NewsBo.UpdateSpecialNewsRelation(newsId, zoneId, newsRelationSpecialId);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(newsId + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
    }
}