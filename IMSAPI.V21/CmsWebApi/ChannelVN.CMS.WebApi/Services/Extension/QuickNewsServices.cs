﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class QuickNewsServices
    {
        public Common.ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public QuickNewsEntity ExternalGetQuickNewsByQuickNewsId(long id)
        {
            return QuickNewsBo.ExternalGetQuickNewsByQuickNewsId(id);
        }

        public QuickNewsDetailEntity ExternalGetQuickNewsDetailByQuickNewsId(long id)
        {
            return QuickNewsBo.ExternalGetQuickNewsDetailByQuickNewsId(id);
        }

        public QuickNewsEntity GetQuickNewsDetailByQuickNewsId(long id)
        {
            return QuickNewsBo.GetQuickNewsDetailByQuickNewsId(id);
        }

        public WcfActionResponse ExternalReceiveQuickNews(long id, long newsId, string receivedBy)
        {
            var errorCode = QuickNewsBo.ExternalReceiveQuickNews(id, newsId, receivedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse ExternalDeleteQuickNews(long id, string deletedBy)
        {
            var errorCode = QuickNewsBo.ExternalDeleteQuickNews(id, deletedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse ExternalDeleteMultiQuickNews(string idList, string deletedBy)
        {
            var errorCode = QuickNewsBo.ExternalDeleteMultiQuickNews(idList, deletedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<QuickNewsEntity> ExternalSearchQuickNews(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuickNewsBo.ExternalSearchQuickNews(keyword, type, status, pageIndex, pageSize, ref totalRow);
        }
        public int ExternalCountQuickNews(int type, int status)
        {
            try
            {
                return QuickNewsBo.ExternalCountQuickNews(type, status);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public QuickNewsAuthorEntity ExternalGetQuickNewsAuthorById(long vietId)
        {
            return QuickNewsBo.ExternalGetQuickNewsAuthorById(vietId);
        }

        public QuickNewsAuthorEntity ExternalGetQuickNewsAuthorByEmail(string email)
        {
            return QuickNewsBo.ExternalGetQuickNewsAuthorByEmail(email);
        }

        public List<QuickNewsAuthorEntity> ExternalSearchQuickNewsAuthor(string keyword, string email, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuickNewsBo.ExternalSearchQuickNewsAuthor(keyword, email, status, pageIndex, pageSize, ref totalRow);
        }

        public List<QuickNewsImagesEntity> ExternalGetQuickNewsImagesByQuickNewsId(long quickNewsId)
        {
            return QuickNewsBo.ExternalGetQuickNewsImagesByQuickNewsId(quickNewsId);
        }

        public WcfActionResponse InsertQuickNews(QuickNewsEntity quickNews)
        {
            var errorCode = QuickNewsBo.InsertQuickNews(quickNews);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InsertQuickNewsAuthor(QuickNewsAuthorEntity quickNewsAuthor)
        {
            var errorCode = QuickNewsBo.InsertQuickNewsAuthor(quickNewsAuthor);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InsertQuickNewsImage(QuickNewsImagesEntity quickNewsImage)
        {
            var errorCode = QuickNewsBo.InsertQuickNewsImage(quickNewsImage);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
    }
}