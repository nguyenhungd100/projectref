﻿using System;
using ChannelVN.CMS.Common;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.ForExternalApplication.Bo;
using System.Collections.Generic;
using ChannelVN.ForExternalApplication.Entity.ErrorCode;
using ChannelVN.CMS.Entity.Base.NewsWarning;
using ChannelVN.CMS.Common.ChannelConfig;
using System.Threading.Tasks;
using System.Net;
using System.Text;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class AdmicroServices
    {
        public NewsForAdStoreEntity GetNewsById(ChannelMappingForExternalApplication channelMapping, long newsId)
        {
            return ForAdmicroBo.GetNewsForAdStoreById(channelMapping, newsId);
        }

        public List<NewsForAdStoreEntity> SearchNewsForAdStore(ChannelMappingForExternalApplication channelMapping, string keyword, int zoneId,
                                                                         DateTime fromDistributionDate,
                                                                         DateTime toDistributionDate, int pageIndex,
                                                                         int pageSize, ref int totalRow)
        {
            return ForAdmicroBo.SearchNewsForAdStore(channelMapping, keyword, zoneId, fromDistributionDate, toDistributionDate, pageIndex, pageSize, ref totalRow);
        }

        public WcfActionResponse UpdateAdPageMode(ChannelMappingForExternalApplication channelMapping, long newsId, EnumAdPageMode adPageMode, string adStoreUrl)
        {
            var errorCode = ForAdmicroBo.UpdateAdStoreMode(channelMapping, newsId, adPageMode, adStoreUrl);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        #region ContentWarning
        public string CheckNewsContentWarning(string title, string sapo, string content, string topic, string tags)
        {
            var data = string.Empty;
            try
            {                
                var channelName = CmsChannelConfiguration.GetAppSetting("Admicro_Warning_Api_Channel")?? "soha.vn";
                var url = CmsChannelConfiguration.GetAppSetting("Admicro_Warning_Api_Url") ?? "http://mlapi.admicro.vn/analytic/news/info";

                var task = Task.Run(() =>
                {
                    //Logger.WriteLog(Logger.LogType.Debug, string.Format("Start CheckNewsContentWarning {0} url {1}", channelName, url));

                    using (WebClient wc = new WebClient())
                    {
                        try
                        {
                            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                            var reqparm = new System.Collections.Specialized.NameValueCollection();
                            reqparm.Add("title", title);
                            reqparm.Add("sapo", sapo);
                            reqparm.Add("content", content);
                            reqparm.Add("topic", topic);
                            reqparm.Add("tags", tags);
                            reqparm.Add("channel", channelName);                                

                            //Logger.WriteLog(Logger.LogType.Debug, string.Format("CHINHNB log CheckNewsContentWarning: title {0}: channel: {1}", title, channelName));

                            byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                            data = Encoding.UTF8.GetString(responsebytes);                            

                            //Logger.WriteLog(Logger.LogType.Debug, string.Format("End CheckNewsContentWarning {0} url {1}: {2}", channelName, url, data));
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Error, "TranferNewsToAdmicro: " + ex.Message);
                        }
                    }
                });
                try
                {
                    if (task.Wait(TimeSpan.FromSeconds(5)))
                    {
                        return data;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "AdmicroServices.CheckNewsContentWarning => " + ex.Message);
                return data;
            }
        }

        //Hoạt
        public string CheckNewsContentWarningV2(string content)
        {
            var data = string.Empty;
            try
            {                
                var url = CmsChannelConfiguration.GetAppSetting("Check_Warning_Api_Url");
                if (string.IsNullOrEmpty(url)) url = "http://s8.cnnd.vn/checkf1";
                var token = CmsChannelConfiguration.GetAppSetting("Check_Warning_Api_Token");
                if (string.IsNullOrEmpty(token)) token = "sdfgjklgnvsl@$@#dskmzxcFJHNKLDF";

                using (WebClient wc = new WebClient())
                {
                    try
                    {
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        wc.Headers["token"] = token;
                        var reqparm = new System.Collections.Specialized.NameValueCollection();
                        reqparm.Add("comment", content);
                        byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                        data = Encoding.UTF8.GetString(responsebytes);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "1.CheckNewsContentWarningV2: " + ex.Message);
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "2.CheckNewsContentWarningV2 => " + ex.Message);
                return data;
            }
        }
        #endregion
    }
}