﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.WebApi.Services.ExternalServices;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class QuickAnswerServices
    {        
        public QuickAnswerEntity ExternalGetQuickAnswerById(int id)
        {
            return QuickAnswerBo.ExternalGetQuickAnswerById(id);
        }                
        public WcfActionResponse ExternalReceiveQuickAnswer(int id, string receivedBy)
        {
            var errorCode = QuickAnswerBo.ExternalReceiveQuickAnswer(id, receivedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ExternalDeleteQuickAnswer(int id, string deletedBy)
        {
            var errorCode = QuickAnswerBo.ExternalDeleteQuickAnswer(id, deletedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ExternalDeleteMultiQuickAnswer(string idList, string deletedBy)
        {
            var errorCode = QuickAnswerBo.ExternalDeleteMultiQuickAnswer(idList, deletedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<QuickAnswerEntity> ExternalSearchQuickAnswer(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuickAnswerBo.ExternalSearchQuickAnswer(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public int ExternalCountQuickAnswer(int type, int status)
        {
            try
            {
                return QuickAnswerBo.ExternalCountQuickAnswer(type, status);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public QuickAnswerEntity GetQuickAnswerById(int id)
        {
            return QuickAnswerBo.GetQuickAnswerById(id);
        }
        public WcfActionResponse InsertQuickAnswer(QuickAnswerEntity quickAnswer)
        {
            var errorCode = QuickAnswerBo.InsertQuickAnswer(quickAnswer);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //cd
                try
                {                  
                    var strParamValue = NewtonJson.Serialize(quickAnswer, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + quickAnswer.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertquickanswer", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }

                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateQuickAnswer(QuickAnswerEntity quickAnswer)
        {
            var errorCode = QuickAnswerBo.UpdateQuickAnswer(quickAnswer);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //cd
                try
                {
                    var strParamValue = NewtonJson.Serialize(quickAnswer, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + quickAnswer.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatequickanswer", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }

                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<QuickAnswerEntity> SearchQuickAnswer(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuickAnswerBo.SearchQuickAnswer(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse DeleteQuickAnswer(int id, string deletedBy)
        {
            var errorCode = QuickAnswerBo.DeleteQuickAnswer(id, deletedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //cd
                try
                {
                    var strParamValue = NewtonJson.Serialize(id, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "deletequickanswer", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteMultiQuickAnswer(string idList, string deletedBy)
        {
            var errorCode = QuickAnswerBo.DeleteMultiQuickAnswer(idList, deletedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //cd
                try
                {
                    var strParamValue = NewtonJson.Serialize(idList, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"ListIds\":" + idList + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "deletemultiquickanswer", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}