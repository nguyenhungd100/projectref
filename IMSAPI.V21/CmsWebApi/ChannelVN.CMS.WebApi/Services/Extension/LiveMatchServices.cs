﻿using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.LiveMatch.Bo;
using ChannelVN.LiveMatch.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.LiveMatch.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class LiveMatchServices
    {
        #region LiveMatch

        public List<NodeJs_LiveMatchEntity> SearchLiveMatch(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveMatchBo.SearchLiveMatch(keyword, pageIndex, pageSize, ref totalRow);
        }
        public List<NodeJs_LiveMatchEntity> ListLiveMatchByIds(string ids)
        {
            return LiveMatchBo.ListLiveMatchByIds(ids);
        }
        //public List<LiveMatchEntity> SearchLiveMatchForSuggestion(string keyword, int top)
        //{
        //    return LiveMatchBo.SearchLiveMatchForSuggestion(keyword, top);
        //}
        public NodeJs_LiveMatchEntity GetLiveMatchById(string id)
        {
            return LiveMatchBo.GetLiveMatchById(id);
        }
        //public LiveMatchDetailEntity GetLiveMatchDetail(int liveMatchId)
        //{
        //    return LiveMatchBo.GetLiveMatchDetail(liveMatchId);
        //}
        //public WcfActionResponse InsertLiveMatch(LiveMatchEntity liveMatch)
        //{
        //    var errorCode = LiveMatchBo.InsertLiveMatch(liveMatch);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}
        public WcfActionResponse InsertLiveMatchV2(NodeJs_LiveMatchEntity liveMatch, ref string Id)
        {
            var errorCode = LiveMatchBo.InsertLiveMatchV2(liveMatch, ref Id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateLiveMatch(NodeJs_LiveMatchEntity liveMatch)
        {
            var errorCode = LiveMatchBo.UpdateLiveMatch(liveMatch);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        //public WcfActionResponse DeleteLiveMatch(int liveMatchId)
        //{
        //    var errorCode = LiveMatchBo.DeleteLiveMatch(liveMatchId);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        //}

        #endregion

        #region LiveMatchTimeline

        public List<NodeJs_LiveMatchTimeLinesEntity> GetLiveMatchTimelineByLiveMatchId(string liveMatchId)
        {
            return LiveMatchTimelineBo.GetLiveMatchTimelineByLiveMatchId(liveMatchId);
        }
        public NodeJs_LiveMatchTimeLinesEntity GetLiveMatchTimelineById(string liveMatchTimelineId)
        {
            return LiveMatchTimelineBo.GetLiveMatchTimelineById(liveMatchTimelineId);
        }
        public WcfActionResponse InsertLiveMatchTimeline(NodeJs_LiveMatchTimeLinesEntity liveMatchTimeline, ref string id)
        {
            var errorCode = LiveMatchTimelineBo.InsertLiveMatchTimeline(liveMatchTimeline, ref id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateLiveMatchTimeline(NodeJs_LiveMatchTimeLinesEntity liveMatchTimeline)
        {
            var errorCode = LiveMatchTimelineBo.UpdateLiveMatchTimeline(liveMatchTimeline);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteLiveMatchTimeline(string liveMatchTimelineId)
        {
            var errorCode = LiveMatchTimelineBo.DeleteLiveMatchTimeline(liveMatchTimelineId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region LiveMatchEvent

        public List<NodeJs_LiveMatchEventEntity> GetLiveMatchEventByLiveMatchId(string liveMatchId)
        {
            return LiveMatchEventBo.GetLiveMatchEventByLiveMatchId(liveMatchId);
        }
        public NodeJs_LiveMatchEventEntity GetLiveMatchEventById(string liveMatchEventId)
        {
            return LiveMatchEventBo.GetLiveMatchEventById(liveMatchEventId);
        }
        public WcfActionResponse InsertLiveMatchEvent(NodeJs_LiveMatchEventEntity liveMatchEvent, ref string id)
        {
            var errorCode = LiveMatchEventBo.InsertLiveMatchEvent(liveMatchEvent, ref id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateLiveMatchEvent(NodeJs_LiveMatchEventEntity liveMatchEvent)
        {
            var errorCode = LiveMatchEventBo.UpdateLiveMatchEvent(liveMatchEvent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteLiveMatchEvent(string liveMatchEventId)
        {
            var errorCode = LiveMatchEventBo.DeleteLiveMatchEvent(liveMatchEventId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region LiveMatchPenalty

        public List<NodeJs_LiveMatchPenaltyEntity> GetLiveMatchPenaltyByLiveMatchId(string liveMatchId)
        {
            return LiveMatchPenaltyBo.GetLiveMatchPenaltyByLiveMatchId(liveMatchId);
        }
        public NodeJs_LiveMatchPenaltyEntity GetLiveMatchPenaltyById(string liveMatchPenaltyId)
        {
            return LiveMatchPenaltyBo.GetLiveMatchPenaltyById(liveMatchPenaltyId);
        }
        public WcfActionResponse InsertLiveMatchPenalty(NodeJs_LiveMatchPenaltyEntity liveMatchPenalty, ref string id)
        {
            var errorCode = LiveMatchPenaltyBo.InsertLiveMatchPenalty(liveMatchPenalty, ref id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateLiveMatchPenalty(NodeJs_LiveMatchPenaltyEntity liveMatchPenalty)
        {
            var errorCode = LiveMatchPenaltyBo.UpdateLiveMatchPenalty(liveMatchPenalty);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteLiveMatchPenalty(string liveMatchPenaltyId)
        {
            var errorCode = LiveMatchPenaltyBo.DeleteLiveMatchPenalty(liveMatchPenaltyId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}