﻿using ChannelVN.CMS.BO.Base.DirectTag;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.DirectTag;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class DirectTagServices
    {
        #region DirectTag
        public WcfActionResponse InsertDirectTag(ref int tagId, string tagName, string tagLink, string quoteFormat, int tagType)
        {
            try
            {
                var errorCode = DirectTagBo.InsertDirectTag(ref tagId, tagName, tagLink, quoteFormat, tagType);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(tagName, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateDirectTag(int tagId, string tagName, string tagLink, string quoteFormat, int type)
        {
            try
            {
                var errorCode = DirectTagBo.UpdateDirectTag(tagId, tagName, tagLink, quoteFormat, type);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(tagName, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteDirectTag(string tagIds)
        {
            try
            {
                var errorCode = DirectTagBo.DeleteDirectTag(tagIds);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(tagIds, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse InsertDirectTagNewsMulti(string tagIds, string newsIds, string account)
        {
            try
            {
                var errorCode = DirectTagBo.InsertDirectTagNewsMulti(tagIds, newsIds, account);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(tagIds, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse InsertDirectTagNewsMultiByType(string tagIds, string newsIds, int type, string account)
        {
            try
            {
                var errorCode = DirectTagBo.InsertDirectTagNewsMultiByType(tagIds, newsIds, type, account);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(tagIds, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteDirectTagNewsFromNews(int tagId, long newsId, int type, string account)
        {
            try
            {
                var errorCode = DirectTagBo.DeleteDirectTagNewsFromNews(tagId, newsId, type, account);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(tagId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<DirectTagEntity> SelectAllDirectTagNews(int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return DirectTagBo.SelectAll(pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }

        public List<DirectTagEntity> SelectAllByTypeDirectTagNews(int pageIndex, int pageSize, int type, ref int totalRow)
        {
            try
            {
                return DirectTagBo.SelectAllByType(pageIndex, pageSize, type, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }

        public List<DirectTagEntity> SearchDirectTag(string keyword, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return DirectTagBo.SearchDirectTag(keyword, type, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }

        public DirectTagEntity IsDirectTagAvailable(string tagName)
        {
            try
            {
                return DirectTagBo.IsTagAvailable(tagName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }

        public List<DirectTagEntity> SelectDirectTagByNewsId(long newsId)
        {
            try
            {
                return DirectTagBo.SelectDirectTagByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }
        #endregion
        #region search_news
        public List<NewsPublishForSearchEntity> SearchNewsPublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return DirectTagBo.SearchNewsPublish(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public List<NewsPublishForSearchEntity> SearchNewsPublish_DirectTag_LessThan_3Tag(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return DirectTagBo.SearchNewsPublish_DirectTag_LessThan_3Tag(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        #endregion
    }
}