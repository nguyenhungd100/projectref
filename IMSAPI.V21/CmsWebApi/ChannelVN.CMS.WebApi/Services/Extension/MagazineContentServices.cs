﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.MagazineContent.Bo;
using ChannelVN.MagazineContent.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.MagazineContent.Entity.ErrorCode;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class MagazineContentServices
    {
        public List<MagazineContentEntity> MagazineContent_Search(string keyword, MagazineContentIsHot IsHot, MagazineContentStatus status, string UserName, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return MagazineContentBo.MagazineContentSearch(keyword, IsHot, status, UserName, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public MagazineContentEntity MagazineContent_GetById(int Id)
        {
            return MagazineContentBo.GetMagazineContentById(Id);
        }
        public WcfActionResponse MagazineContent_Update(MagazineContentEntity entity)
        {
            return MagazineContentBo.UpdateMagazineContent(entity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_Insert(MagazineContentEntity entity)
        {
            return MagazineContentBo.InsertMagazineContent(entity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_UpdateStatus(int Id, MagazineContentStatus status, string Username)
        {
            return MagazineContentBo.MagazineContent_UpdateStatus(Id, status, Username) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_UpdateIsHot(int Id, bool IsHot, string Username)
        {
            return MagazineContentBo.MagazineContent_UpdateIsHot(Id, IsHot, Username) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_Published(int Id, DateTime PublishedDate, string Username)
        {
            return MagazineContentBo.MagazineContent_Published(Id, PublishedDate, Username) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_Delete(int Id)
        {
            return MagazineContentBo.DeleteMagazineContent(Id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public object ImportMagazine(string url)
        {
            try
            {
                var html = IMSBrowserHelper.DownloadPage(url, false);

                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(html);

                if (htmlDoc.DocumentNode != null)
                {
                    //title
                    var title = string.Empty;
                    var titleTags = htmlDoc.DocumentNode.SelectNodes("//*[@property='og:title']");
                    if (titleTags != null)
                    {
                        foreach (var tag in titleTags)
                        {
                            title = tag.Attributes["content"].Value;
                        }
                    }

                    //Sapo
                    var sapo = string.Empty;
                    var elms3 = htmlDoc.DocumentNode.SelectNodes("//*[@property='og:description']");
                    if (elms3 != null)
                    {
                        for (int i = 0; i < elms3.Count; i++)
                        {
                            sapo = elms3[i].Attributes["content"].Value;
                        }
                    }
                    //Body
                    var body = string.Empty;
                    var elms2 = htmlDoc.DocumentNode.SelectNodes("//*[@class='sp-detail-content']");
                    if (elms2 != null)
                    {
                        for (int i = 0; i < elms2.Count; i++)
                        {
                            body = elms2[i].InnerHtml;
                        }
                    }

                    //cover
                    var cover = string.Empty;
                    var elms4 = htmlDoc.DocumentNode.SelectNodes("//*[@class='cover-top']");
                    if (elms4 != null)
                    {
                        for (int i = 0; i < elms4.Count; i++)
                        {
                            cover = elms4[i].Attributes["src"].Value;
                        }
                    }
                    else
                    {
                        elms4 = htmlDoc.DocumentNode.SelectNodes("//div[@class='sp-cover']//img");
                        if (elms4 != null && elms4.Count > 0)
                        {
                            cover = elms4.ElementAt(0).Attributes["src"].Value;
                        }
                        else
                        {
                            elms4 = htmlDoc.DocumentNode.SelectNodes("//*[@class='bg-cover']");
                            if (elms4 != null && elms4.Count > 0)
                            {
                                cover = elms4.ElementAt(0).Attributes["src"].Value;
                            }
                        }
                    }

                    //avatar
                    var avatar = string.Empty;
                    var elms5 = htmlDoc.DocumentNode.SelectNodes("//*[@property='og:image']");
                    if (elms5 != null)
                    {
                        for (int i = 0; i < elms5.Count; i++)
                        {
                            avatar = elms5[i].Attributes["content"].Value;
                        }
                    }

                    var data = new
                    {
                        Body = body,
                        Title = title,
                        Sapo = sapo,
                        Avatar = avatar,
                        Cover = cover
                    };
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Url[" + url + "] => " + ex.Message);
                return null;
            }
        }

        public object ImportMiniMagazine(string url)
        {
            try
            {
                var html = IMSBrowserHelper.DownloadPage(url, false);

                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(html);

                if (htmlDoc.DocumentNode != null)
                {
                    //title
                    var title = string.Empty;
                    var titleTags = htmlDoc.DocumentNode.SelectNodes("//*[@property='og:title']");

                    foreach (var tag in titleTags)
                    {
                        title = tag.Attributes["content"].Value;
                    }

                    //Sapo
                    var sapo = string.Empty;
                    var elms3 = htmlDoc.DocumentNode.SelectNodes("//*[@property='og:description']");
                    if (elms3 != null)
                    {
                        for (int i = 0; i < elms3.Count; i++)
                        {
                            sapo = elms3[i].Attributes["content"].Value;
                        }
                    }

                    //Body
                    var body = string.Empty;
                    var elms2 = htmlDoc.DocumentNode.SelectNodes("//*[@class='knc-content']");
                    if (elms2 != null)
                    {
                        for (int i = 0; i < elms2.Count; i++)
                        {
                            body = elms2[i].InnerHtml;
                        }
                    }

                    //cover
                    var cover = string.Empty;
                    var elms4 = htmlDoc.DocumentNode.SelectNodes("//*[@class='kbw-minicover-wrapper']//img");
                    if (elms4 != null)
                    {
                        for (int i = 0; i < elms4.Count; i++)
                        {
                            cover = elms4[i].Attributes["src"].Value;
                        }
                    }                    

                    //avatar
                    var avatar = string.Empty;
                    var elms5 = htmlDoc.DocumentNode.SelectNodes("//*[@property='og:image']");
                    if (elms5 != null)
                    {
                        for (int i = 0; i < elms5.Count; i++)
                        {
                            avatar = elms5[i].Attributes["content"].Value;
                        }
                    }

                    var data = new
                    {
                        Body = body,
                        Title = title,
                        Sapo = sapo,
                        Avatar = avatar,
                        Cover = cover
                    };
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Warning, "Url[" + url + "] => " + ex.Message);
                return null;
            }
        }

        public object ImportMagazinePr(string url)
        {
            try
            {
                var html = IMSBrowserHelper.DownloadPage(url, false);

                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(html);

                if (htmlDoc.DocumentNode != null)
                {
                    //Title
                    var title = string.Empty;
                    var elmsTitle = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Title']");
                    if (elmsTitle != null)
                    {
                        title = System.Net.WebUtility.HtmlDecode(elmsTitle.Attributes["value"].Value);
                    }

                    //Sapo
                    var sapo = string.Empty;
                    var elmsSapo = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Sapo']");
                    if (elmsSapo != null)
                    {
                        sapo = System.Net.WebUtility.HtmlDecode(elmsSapo.Attributes["value"].Value);
                    }

                    //Body
                    var body = string.Empty;
                    var elmsBody = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Body']");
                    if (elmsBody != null)
                    {
                        body = System.Net.WebUtility.HtmlDecode(elmsBody.Attributes["value"].Value);
                    }

                    //AvatarHost                    
                    var avatarHost = "http://channel.mediacdn.vn";
                    var elmsAvatarHost = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='AvatarHost']");
                    if (elmsAvatarHost != null)
                    {
                        avatarHost = System.Net.WebUtility.HtmlDecode(elmsAvatarHost.Attributes["value"].Value);
                    }

                    //Cover => Avatar3
                    var cover = string.Empty;
                    var elmsCove = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Avatar3']");
                    if (elmsCove != null)
                    {
                        cover = System.Net.WebUtility.HtmlDecode(elmsCove.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(cover) && !cover.Contains("http://channel.mediacdn.vn"))
                            cover = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsCove.Attributes["value"].Value);
                    }

                    //Avatar
                    var avatar = string.Empty;
                    var elmsAvatar = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Avatar']");
                    if (elmsAvatar != null)
                    {
                        avatar = System.Net.WebUtility.HtmlDecode(elmsAvatar.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(avatar) && !avatar.Contains("http://channel.mediacdn.vn"))
                            avatar = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsAvatar.Attributes["value"].Value);
                    }

                    //AvatarMiniCover
                    var avatarMiniCover = string.Empty;
                    var elmsAvatarMiniCover = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_8008']");
                    if (elmsAvatarMiniCover != null)
                    {
                        avatarMiniCover = System.Net.WebUtility.HtmlDecode(elmsAvatarMiniCover.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(avatarMiniCover) && !avatarMiniCover.Contains("http://channel.mediacdn.vn"))
                            avatarMiniCover = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsAvatarMiniCover.Attributes["value"].Value);
                    }

                    //coverMobile
                    var coverMobile = string.Empty;
                    var elmsCoverMobile = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Avatar4']");
                    if (elmsCoverMobile != null)
                    {
                        coverMobile = System.Net.WebUtility.HtmlDecode(elmsCoverMobile.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(coverMobile) && !coverMobile.Contains("http://channel.mediacdn.vn"))
                            coverMobile = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsCoverMobile.Attributes["value"].Value);
                    }

                    //CoverVideo
                    var coverVideo = string.Empty;
                    var elmsCoverVideo = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_21061']");
                    if (elmsCoverVideo != null)
                    {
                        coverVideo = System.Net.WebUtility.HtmlDecode(elmsCoverVideo.Attributes["value"].Value);
                    }

                    //AvatarShareFacebook
                    var avatarShareFacebook = string.Empty;
                    var elmsAvatarShareFacebook = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Avatar5']");
                    if (elmsAvatarShareFacebook != null)
                    {
                        avatarShareFacebook = System.Net.WebUtility.HtmlDecode(elmsAvatarShareFacebook.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(avatarShareFacebook) && !avatarShareFacebook.Contains("http://channel.mediacdn.vn"))
                            avatarShareFacebook = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsAvatarShareFacebook.Attributes["value"].Value);
                    }

                    //Color
                    var color = string.Empty;
                    var elmsColor = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_8010']");
                    if (elmsColor != null)
                    {
                        color = System.Net.WebUtility.HtmlDecode(elmsColor.Attributes["value"].Value);
                    }

                    //BackgroundColor
                    var backgroundcolor = string.Empty;
                    var elmsBackgroundColor = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_8009']");
                    if (elmsBackgroundColor != null)
                    {
                        backgroundcolor = System.Net.WebUtility.HtmlDecode(elmsBackgroundColor.Attributes["value"].Value);
                    }

                    //VideoTextLayer
                    var videoTextLayer = string.Empty;
                    var elmsVideoTextLayer = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_21062']");
                    if (elmsVideoTextLayer != null)
                    {
                        videoTextLayer = System.Net.WebUtility.HtmlDecode(elmsVideoTextLayer.Attributes["value"].Value);
                    }

                    //Author
                    var author = string.Empty;
                    var elmsAuthor = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Author']");
                    if (elmsAuthor != null)
                    {
                        author = System.Net.WebUtility.HtmlDecode(elmsAuthor.Attributes["value"].Value);
                    }

                    //avatarDoc
                    var avatarDoc = string.Empty;

                    //avatarNgang
                    var avatarNgang = string.Empty;

                    var data = new
                    {
                        Body = body,
                        Title = title,
                        Sapo = sapo,
                        Avatar = avatar,
                        Cover = cover,
                        AvatarShareFacebook = avatarShareFacebook,
                        AvatarMiniCover = avatarMiniCover,
                        Author = author,

                        CoverVideo = coverVideo,
                        CoverMobile = coverMobile,
                        Color = color,
                        BackgroundColor = backgroundcolor,
                        VideoTextLayer = videoTextLayer,
                        AvataDoc = avatarDoc,
                        AvatarNgang = avatarNgang
                    };
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Warning, "Url[" + url + "] => " + ex.Message);
                return null;
            }
        }

        public object ImportMiniMagazinePr(string url)
        {
            try
            {
                var html = IMSBrowserHelper.DownloadPage(url, false);

                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(html);

                if (htmlDoc.DocumentNode != null)
                {
                    //Title
                    var title = string.Empty;
                    var elmsTitle = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Title']");
                    if (elmsTitle != null)
                    {
                        title = System.Net.WebUtility.HtmlDecode(elmsTitle.Attributes["value"].Value);
                    }

                    //Sapo
                    var sapo = string.Empty;
                    var elmsSapo = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Sapo']");
                    if (elmsSapo != null)
                    {
                        sapo = System.Net.WebUtility.HtmlDecode(elmsSapo.Attributes["value"].Value);
                    }

                    //Body
                    var body = string.Empty;
                    var elmsBody = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Body']");
                    if (elmsBody != null)
                    {
                        body = System.Net.WebUtility.HtmlDecode(elmsBody.Attributes["value"].Value);
                    }

                    //AvatarHost                    
                    var avatarHost = "http://channel.mediacdn.vn";
                    var elmsAvatarHost = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='AvatarHost']");
                    if (elmsAvatarHost != null)
                    {
                        avatarHost = System.Net.WebUtility.HtmlDecode(elmsAvatarHost.Attributes["value"].Value);
                    }

                    //Cover => Avatar3
                    var cover = string.Empty;
                    var elmsCove = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Avatar3']");
                    if (elmsCove != null)
                    {
                        cover = System.Net.WebUtility.HtmlDecode(elmsCove.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(cover) && !cover.Contains("http://channel.mediacdn.vn"))
                            cover = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsCove.Attributes["value"].Value);
                    }

                    //Avatar
                    var avatar = string.Empty;
                    var elmsAvatar = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Avatar']");
                    if (elmsAvatar != null)
                    {
                        avatar = System.Net.WebUtility.HtmlDecode(elmsAvatar.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(avatar) && !avatar.Contains("http://channel.mediacdn.vn"))
                            avatar = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsAvatar.Attributes["value"].Value);
                    }

                    //AvatarMiniCover
                    var avatarMiniCover = string.Empty;
                    var elmsAvatarMiniCover = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_8008']");
                    if (elmsAvatarMiniCover != null)
                    {
                        avatarMiniCover = System.Net.WebUtility.HtmlDecode(elmsAvatarMiniCover.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(avatarMiniCover) && !avatarMiniCover.Contains("http://channel.mediacdn.vn"))
                            avatarMiniCover = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsAvatarMiniCover.Attributes["value"].Value);
                    }

                    //coverMobile
                    var coverMobile = string.Empty;
                    var elmsCoverMobile = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Avatar4']");
                    if (elmsCoverMobile != null)
                    {
                        coverMobile = System.Net.WebUtility.HtmlDecode(elmsCoverMobile.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(coverMobile) && !coverMobile.Contains("http://channel.mediacdn.vn"))
                            coverMobile = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsCoverMobile.Attributes["value"].Value);
                    }

                    //CoverVideo
                    var coverVideo = string.Empty;
                    var elmsCoverVideo = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_21061']");
                    if (elmsCoverVideo != null)
                    {
                        coverVideo = System.Net.WebUtility.HtmlDecode(elmsCoverVideo.Attributes["value"].Value);
                    }

                    //AvatarShareFacebook
                    var avatarShareFacebook = string.Empty;
                    var elmsAvatarShareFacebook = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Avatar5']");
                    if (elmsAvatarShareFacebook != null)
                    {
                        avatarShareFacebook = System.Net.WebUtility.HtmlDecode(elmsAvatarShareFacebook.Attributes["value"].Value);
                        if (!string.IsNullOrEmpty(avatarShareFacebook) && !avatarShareFacebook.Contains("http://channel.mediacdn.vn"))
                            avatarShareFacebook = avatarHost + "/" + System.Net.WebUtility.HtmlDecode(elmsAvatarShareFacebook.Attributes["value"].Value);
                    }

                    //Color
                    var color = string.Empty;
                    var elmsColor = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_8010']");
                    if (elmsColor != null)
                    {
                        color = System.Net.WebUtility.HtmlDecode(elmsColor.Attributes["value"].Value);
                    }

                    //BackgroundColor
                    var backgroundcolor = string.Empty;
                    var elmsBackgroundColor = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_8009']");
                    if (elmsBackgroundColor != null)
                    {
                        backgroundcolor = System.Net.WebUtility.HtmlDecode(elmsBackgroundColor.Attributes["value"].Value);
                    }

                    //VideoTextLayer
                    var videoTextLayer = string.Empty;
                    var elmsVideoTextLayer = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsExtension_21062']");
                    if (elmsVideoTextLayer != null)
                    {
                        videoTextLayer = System.Net.WebUtility.HtmlDecode(elmsVideoTextLayer.Attributes["value"].Value);
                    }

                    //Author
                    var author = string.Empty;
                    var elmsAuthor = htmlDoc.DocumentNode.SelectSingleNode("//*[@id='NewsInfo_Author']");
                    if (elmsAuthor != null)
                    {
                        author = System.Net.WebUtility.HtmlDecode(elmsAuthor.Attributes["value"].Value);
                    }

                    //avatarDoc
                    var avatarDoc = string.Empty;

                    //avatarNgang
                    var avatarNgang = string.Empty;

                    var data = new
                    {
                        Body = body,
                        Title = title,
                        Sapo = sapo,
                        Avatar = avatar,
                        Cover = cover,
                        AvatarShareFacebook = avatarShareFacebook,
                        AvatarMiniCover = avatarMiniCover,
                        Author = author,

                        CoverVideo = coverVideo,
                        CoverMobile = coverMobile,
                        Color = color,
                        BackgroundColor = backgroundcolor,
                        VideoTextLayer = videoTextLayer,
                        AvataDoc = avatarDoc,
                        AvatarNgang = avatarNgang
                    };
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Warning, "Url[" + url + "] => " + ex.Message);
                return null;
            }
        }
    }

    public class NewsCrawlerPRResponseData
    {
        public long ContentID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Body { get; set; }
        public string Sapo { get; set; }
        public string SubTitle { get; set; }
        public string Avatar { get; set; }
        public string AvatarDesc { get; set; }
        public int Approver { get; set; }
        public string PublishedDate { get; set; }
        public string PublishedTime { get; set; }
        public DateTime PublishedDateTime { get; set; }
        public string PublishedDateTimeString { get; set; }
        public int BookingId { get; set; }
        public string ContractNo { get; set; }
        public string ChannelName { get; set; }
        public string Booker { get; set; }
        public string ZoneTite { get; set; }
        public int Duration { get; set; }
        public string Url { get; set; }
        public string ZoneTitle { get; set; }
        public string Avatar3 { get; set; }
        public string Avatar4 { get; set; }
        public string Avatar5 { get; set; }
    }

    public class NewsExtensionCrawlerPRResponseData
    {
        public long NewsId { get; set; }
        public int NumericValue { get; set; }
        public int Type { get; set; }
        public int Value { get; set; }
    }
}