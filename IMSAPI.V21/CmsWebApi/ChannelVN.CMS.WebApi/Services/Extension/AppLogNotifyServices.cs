﻿using ChannelVN.CMS.BO.Base.NewsNotify;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsNotify;
using ChannelVN.CMS.Entity.ErrorCode;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class AppLogNotifyServices
    {
        public enum AppSource
        {
            Unknown = 0,
            DanTri = 1,
            Kenh14 = 2,
            Afamily = 3,
            CafeF = 4,
            CafeBiz = 5,
            AutoPro = 6,
            GenK = 7,
            SohaNews = 8,
            VnEconomy = 9,
            GameK = 10,
            VTV = 11,
            Sport5=12
        }
        public WcfActionResponse InsertNews(NewsNotifyEntity news)
        {
            try
            {
                var newEncryptNewsId = string.Empty;
                var newsId = 0L;
                var errorCode = NewsNotifyBo.InsertNews(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Notify_InsertNews(NewsNotifyEntity news)
        {
            try
            {
                var newEncryptNewsId = string.Empty;
                var newsId = 0L;
                var errorCode = NewsNotifyBo.Notify_InsertNews(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                //Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse InsertNewsApp(NewsAppEntity news)
        {
            try
            {
                var newsId = 0L;
                var errorCode = NewsNotifyBo.InsertNewsApp(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public static void PushNotifyForApp(NewsEntity news, bool isNotify)
        {
            var context = HttpContext.Current;
            var task = Task.Run(() =>
            {
                HttpContext.Current = context;
                //isNotify
                try
                {
                    if (isNotify)
                    {
                        var enableNotify = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting("Enable_Push_Notify"), false);
                        var appSource = Utility.ConvertToInt(CmsChannelConfiguration.GetAppSetting("AppSourceNotify"),0);
                        var data = new AppLogNotifyServices().Notify_InsertNews(new NewsNotifyEntity
                        {
                            IsProcess = enableNotify,
                            NewsId = news.Id,
                            PublishDate = news.DistributionDate,
                            Title = news.Title,
                            Url = news.Url,
                            ChannelId = appSource,//(int)AppSource.Kenh14,
                            Type = news.Type,
                            Sapo=news.Sapo,
                            ZoneId=news.ZoneId,
                            ZoneName=news.ZoneName,
                            SubTitle=news.SubTitle,
                            InitSapo=news.InitSapo,
                            Author=news.Author,
                            Avatar=news.Avatar,
                            Avatar2 = news.Avatar2,
                            Avatar3 = news.Avatar3,
                            Avatar4 = news.Avatar4,
                            Avatar5 = news.Avatar5
                        });

                        if (data.Success && enableNotify)
                        {
                            //push mdc                            
                            try
                            {
                                var url = CmsChannelConfiguration.GetAppSetting("NotifyPushAppV2Url");
                                var token = CmsChannelConfiguration.GetAppSetting("NotifyPushAppV2Token");
                                var appId = CmsChannelConfiguration.GetAppSetting("NotifyPushAppV2Id");
                                var projectId = CmsChannelConfiguration.GetAppSetting("NotifyPushAppV2ProjectId");
                                
                                NotifyMessage obj = new NotifyMessage();
                                obj.message = new PushMessage();
                                obj.message.push = new NewsInfo();
                                obj.message.push.title = news.Title;
                                obj.message.push.body = news.Sapo;
                                obj.message.push.url = news.Url;
                                obj.message.push.news_id = news.Id.ToString();
                                obj.message.push.type = news.Type;
                                obj.appid = appId;
                                obj.projectid = projectId;
                                obj.token = token;
                                obj.os = "";
                                obj.filter = new List<Filter>();

                                var strParamValue = NewtonJson.Serialize(obj);
                                Logger.WriteLog(Logger.LogType.Debug, "Notify.CallAppV2 Object: " + url + " => " + strParamValue);
                                byte[] bytes = Encoding.UTF8.GetBytes(strParamValue);

                                try
                                {
                                    var request = (HttpWebRequest)WebRequest.Create(url);
                                    request.Method = "POST";
                                    request.ContentLength = bytes.Length;
                                    request.ContentType = "application/json";
                                    //Logger.WriteLog(Logger.LogType.Debug, "Notify.CallAppV2 1");
                                    StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());

                                    try
                                    {
                                        requestWriter.Write(strParamValue);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.WriteLog(Logger.LogType.Error, "Notify.CallAppV2 Fail: => " + ex.Message);
                                    }
                                    finally
                                    {
                                        requestWriter.Close();
                                        requestWriter = null;
                                    }
                                    //Logger.WriteLog(Logger.LogType.Debug, "Notify.CallAppV2 2");
                                    HttpWebResponse response2 = (HttpWebResponse)request.GetResponse();
                                    using (StreamReader sr = new StreamReader(response2.GetResponseStream()))
                                    {
                                        //Logger.WriteLog(Logger.LogType.Debug, "Notify.CallAppV2 3");
                                        var result = NewtonJson.Deserialize<NotifyResultV2>(sr.ReadToEnd());
                                        //Logger.WriteLog(Logger.LogType.Debug, "Notify.CallAppV2 4");
                                        if (!result.success)
                                            Logger.WriteLog(Logger.LogType.Error, "Notify.CallAppV2 Fail: => " + result.message);
                                        else
                                        {
                                            Logger.WriteLog(Logger.LogType.Debug, "Notify.CallAppV2 Success: " + result.message_id);
                                            WebClient client = new WebClient();
                                            try
                                            {
                                                Logger.WriteLog(Logger.LogType.Debug, client.DownloadString(CmsChannelConfiguration.GetAppSetting("NotifyPushAppClearCacheUrl")));
                                            }
                                            finally
                                            {
                                                client.Dispose();
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.WriteLog(Logger.LogType.Error, "Notify.CallApp => " + ex.Message);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, "Notify => " + ex.Message);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            });
            try
            {
                if (task.Wait(TimeSpan.FromSeconds(5)))
                {

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        #region entity
        public class NotifyResult
        {
            public bool success { get; set; }
            public string message { get; set; }
        }

        public class NotifyResultV2
        {
            public bool success { get; set; }
            public string message { get; set; }
            public string message_id { get; set; }
        }

        public class NotifyMessage
        {
            public string appid { get; set; }
            public string projectid { get; set; }
            public string token { get; set; }
            public string os { get; set; }
            public List<Filter> filter { get; set; }
            public PushMessage message { get; set; }
        }
        public class GenCacheParam
        {
            public string secret_key { get; set; }
            public string news_id { get; set; }
        }
        public class Filter
        {
            public string key { get; set; }
            public string condition { get; set; }
            public string[] value { get; set; }
        }
        public class PushMessage
        {
            public NewsInfo push { get; set; }
        }
        public class NewsInfo
        {
            public string news_id { get; set; }
            public string title { get; set; }
            public string url { get; set; }
            public string body { get; set; }
            public int type { get; set; }
        }
        #endregion
    }
}