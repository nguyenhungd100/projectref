﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Bo;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.WebApi.Services.ExternalServices;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class Kenh14Services
    {
        #region Quote
        public List<QuoteSearchEntity> SearchQuote(string keyword, int position, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuoteBo.Search(keyword, position, pageIndex, pageSize, ref totalRow);
        }
        public QuoteEntity GetQuoteById(int id)
        {
            return QuoteBo.GetById(id);
        }
        public WcfActionResponse UpdateQuote(QuoteEntity Quote)
        {
            return QuoteBo.UpdateQuote(Quote) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse InsertQuote(QuoteEntity Quote, ref int quoteId)
        {
            return QuoteBo.InsertQuote(Quote, ref quoteId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(quoteId.ToString())
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        #region Init Quote
        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByQuoteAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return QuoteBo.InitEsByQuoteAsync(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllQuote(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return QuoteBo.InitRedisAllQuote(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #endregion

        #region MediaAlbum

        public List<MediaAlbumEntity> SearchMediaAlbum(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MediaAlbumBo.SearchMediaAlbum(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);
        }
        public MediaAlbumForEditEntity GetMediaAlbumForEditByMediaAlbumId(int MediaAlbumId, int topPhoto)
        {
            return MediaAlbumBo.GetMediaAlbumForEditByMediaAlbumId(MediaAlbumId, topPhoto);
        }

        public WcfActionResponse CreateMediaAlbum(MediaAlbumForEditEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.CreateMediaAlbum(MediaAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateMediaAlbum(MediaAlbumForEditEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.UpdateMediaAlbum(MediaAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteMediaAlbum(int MediaAlbumId)
        {
            var errorCode = MediaAlbumBo.DeleteMediaAlbum(MediaAlbumId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteMediaAlbumDetail(int id)
        {
            var errorCode = MediaAlbumBo.DeleteMediaAlbumDetail(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public MediaAlbumDetailEntity GetMediaAlbumDetailForEditById(int mediaAlbumId)
        {
            return MediaAlbumBo.GetMediaAlbumDetailForEditById(mediaAlbumId);
        }
        public WcfActionResponse CreateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.CreateMediaAlbumDetail(MediaAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.UpdateMediaAlbumDetail(MediaAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region BoxBottomEmbed (K14Embed)

        public WcfActionResponse BoxBottomEmbedInsert(BoxBottomEmbedEntity entity, ref int k14EmbedId)
        {
            var errorCode = K14EmbedBo.Insert(entity, ref k14EmbedId);
            var data = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(k14EmbedId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (data.Success)
            {                
                entity.Id = k14EmbedId;                
                var strParamValue = NewtonJson.Serialize(entity, "yyyy-MM-dd'T'HH:mm:ss");
                var jsonKey = "{\"Id\":" + entity.Id + "}";
                ContentDeliveryServices.PushToDataCD(strParamValue, "insertboxbottomembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
            }
            return data;            
        }

        public WcfActionResponse BoxBottomEmbedUpdate(BoxBottomEmbedEntity entity)
        {
            var errorCode = K14EmbedBo.Update(entity);

            var data = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(entity), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (data.Success)
            {                
                var strParamValue = NewtonJson.Serialize(entity, "yyyy-MM-dd'T'HH:mm:ss");
                var jsonKey = "{\"Id\":" + entity.Id + "}";
                ContentDeliveryServices.PushToDataCD(strParamValue, "updateboxbottomembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
            }

            return data;
        }

        public WcfActionResponse BoxBottomEmbedDelete(int id)
        {
            var errorCode = K14EmbedBo.Delete(id);

            var data = errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (data.Success)
            {                
                var jsonKey = "{\"Id\":" + id + "}";
                ContentDeliveryServices.PushToDataCD(jsonKey, "deleteboxbottomembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
            }

            return data;            
        }

        public BoxBottomEmbedEntity BoxBottomEmbedGetById(int id)
        {
            return K14EmbedBo.GetById(id);
        }

        public List<BoxBottomEmbedEntity> ListBoxBottomEmbedSearch(string keyword)
        {
            return K14EmbedBo.Search(keyword);
        }

        public BoxBottomEmbedEntity[] BoxBottomEmbedGetAll()
        {
            return K14EmbedBo.GetAll().ToArray();
        }
        #endregion

        #region Sticker
        public List<StickerEntity> SearchSticker(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return StickerBo.SearchSticker(keyword, pageIndex, pageSize, ref totalRow);
        }

        public StickerEntity GetStickerByStickerId(long stickerId)
        {
            return StickerBo.GetStickerByStickerId(stickerId);
        }

        public WcfActionResponse InsertSticker(StickerEntity sticker)
        {
            WcfActionResponse responseData;
            int newStickerId = 0;
            var errorCode = StickerBo.InsertSticker(sticker, ref newStickerId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                sticker.Id = newStickerId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(sticker), "1");
            }
            else
            {
                if (newStickerId > 0)
                {
                    sticker.Id = newStickerId;
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(sticker), "1");
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            return responseData;
        }

        public WcfActionResponse UpdateSticker(StickerEntity sticker)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = StickerBo.Update(sticker);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(sticker));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteById(long stickerId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = StickerBo.DeleteById(stickerId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse AddNewsSticker(string newsIds, long stickerId)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = StickerBo.AddNews(newsIds, stickerId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }

        public List<StickerEntity> GetListStickerByNewsId(long newsId)
        {
            return StickerBo.GetListStickerByNewsId(newsId);
        }
        #endregion

        #region CharacterInfo
        public List<CharacterInfoSearchEntity> SearchCharacterInfo(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuoteBo.SearchCharacterInfo(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public CharacterInfoEntity GetCharacterInfoById(int id)
        {
            return QuoteBo.GetCharacterInfoById(id);
        }
        public WcfActionResponse UpdateCharacterInfo(CharacterInfoEntity Quote)
        {
            return QuoteBo.UpdateCharacterInfo(Quote) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse InsertCharacterInfo(CharacterInfoEntity Quote, ref int quoteId)
        {
            return QuoteBo.InsertCharacterInfo(Quote, ref quoteId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(quoteId.ToString())
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteCharacterInfoById(int id)
        {
            return QuoteBo.DeleteCharacterInfoById(id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString())
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        #region Init Quote
        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByCharacterInfoAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return QuoteBo.InitEsByCharacterInfoAsync(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllCharacterInfo(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            try
            {
                return QuoteBo.InitRedisAllCharacterInfo(name, startDate, endDate, pageSize, action);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #endregion
    }
}