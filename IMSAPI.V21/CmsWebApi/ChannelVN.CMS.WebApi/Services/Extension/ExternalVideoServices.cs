﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.External.NguoiLaoDong.ExternalVideoEmbed;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.ExternalVideoEmbed;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class ExternalVideoServices
    {
        public ExternalVideoEntity GetExternalVideoById(int id)
        {
            return ExternalVideoBo.GetExternalVideoById(id);
        }
        public List<ExternalVideoEntity> GetUnprocessExternalVideo(string keyword, int zoneVideoId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ExternalVideoBo.GetUnprocessExternalVideo(keyword, zoneVideoId, pageIndex, pageSize, ref totalRow);
        }
        public int GetExternalVideoCount()
        {
            return ExternalVideoBo.GetExternalVideoCount();
        }
        public WcfActionResponse RecieveExternalVideo(int videoId)
        {
            var errorCode = ExternalVideoBo.RecieveExternalVideo(videoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteExternalVideo(int videoId)
        {
            var errorCode = ExternalVideoBo.DeleteExternalVideo(videoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse Insert(ExternalVideoEmbedEntity videoEmbedEntity)
        {
            var errorCode = ExternalVideoEmbedBo.Insert(videoEmbedEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse Delete(int zoneId, int type)
        {
            var errorCode = ExternalVideoEmbedBo.Delete(zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<ExternalVideoEmbedEntity> GetListByZoneType(int zoneId, int type)
        {
            return ExternalVideoEmbedBo.GetListByZoneType(zoneId, type);
        }


        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}