﻿using ChannelVN.CMS.Common;
using ChannelVN.VideoAutoDownloadMonitor.Bo;
using ChannelVN.VideoAutoDownloadMonitor.Entity;
using ChannelVN.VideoAutoDownloadMonitor.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class VideoAutoDownloadMonitorServices
    {
        public List<VideoAutoDownloadMonitorEntity> GetListByStatus(VideoAutoDownloadMonitorStatus status)
        {
            return VideoAutoDownloadMonitorBo.GetListByStatus(status);
        }
        public WcfActionResponse DownloadCompleted(long NewsId, string VideoInfo, string SaveFilePath, string KeyVideo)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VideoAutoDownloadMonitorBo.DownloadCompleted(NewsId, VideoInfo, SaveFilePath, KeyVideo) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse SentDownloadRequest(int Id, string SaveFilePath)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VideoAutoDownloadMonitorBo.SentDownloadRequest(Id, SaveFilePath) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse Update_Status(string Ids, VideoAutoDownloadMonitorStatus Status)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VideoAutoDownloadMonitorBo.Update_Status(Ids, Status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse Update_CounterCheckDownload(string Ids)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VideoAutoDownloadMonitorBo.Update_CounterCheckDownload(Ids) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse InsertVideoAutoDownloadMonitor(VideoAutoDownloadMonitorEntity videoAutoDownloadMonitor, ref int videoAutoDownloadMonitorId)
        {
            var errorCode = VideoAutoDownloadMonitorBo.InsertVideoAutoDownloadMonitor(videoAutoDownloadMonitor, ref videoAutoDownloadMonitorId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(videoAutoDownloadMonitorId.ToString(), ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public VideoAutoDownloadMonitorEntity GetVideoAutoDownloadMonitorByInterviewId(long newsId)
        {
            return VideoAutoDownloadMonitorBo.GetVideoAutoDownloadMonitorByNewsId(newsId);
        }
    }
}