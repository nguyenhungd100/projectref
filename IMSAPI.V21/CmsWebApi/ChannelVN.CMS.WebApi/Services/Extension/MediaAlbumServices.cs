﻿using ChannelVN.CMS.BO.Base.MediaAlbum;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.MediaAlbum;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class MediaAlbumServices
    {
        #region MediaAlbum

        public List<MediaAlbumEntity> SearchMediaAlbum(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MediaAlbumBo.SearchMediaAlbum(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);
        }
        public MediaAlbumEntity GetMediaAlbumByAlbumId(int MediaAlbumId)
        {
            return MediaAlbumBo.GetMediaAlbumByAlbumId(MediaAlbumId);
        }
        public MediaAlbumForEditEntity GetMediaAlbumForEditByMediaAlbumId(int MediaAlbumId, int topPhoto)
        {
            return MediaAlbumBo.GetMediaAlbumForEditByMediaAlbumId(MediaAlbumId, topPhoto);
        }

        public WcfActionResponse CreateMediaAlbum(MediaAlbumEntity MediaAlbumForEdit)
        {
            var newMediaAlbumId = 0;
            var errorCode = MediaAlbumBo.CreateMediaAlbum(MediaAlbumForEdit, ref newMediaAlbumId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + newMediaAlbumId + "}";                                        
                    var strParamValue = NewtonJson.Serialize(MediaAlbumForEdit, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertmediaalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newMediaAlbumId.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateMediaAlbum(MediaAlbumEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.UpdateMediaAlbum(MediaAlbumForEdit);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + MediaAlbumForEdit.Id + "}";                    
                    var strParamValue = NewtonJson.Serialize(MediaAlbumForEdit, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatemediaalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteMediaAlbum(int MediaAlbumId)
        {
            var errorCode = MediaAlbumBo.DeleteMediaAlbum(MediaAlbumId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + MediaAlbumId + "}";
                    var strParamValue = NewtonJson.Serialize(MediaAlbumId, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "deletemediaalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateStatusMediaAlbum(int MediaAlbumId, string action)
        {
            var errorCode = MediaAlbumBo.UpdateStatusMediaAlbum(MediaAlbumId, action);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + MediaAlbumId + "}";
                    var strParamValue = NewtonJson.Serialize(MediaAlbumId, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatestatusmediaalbum", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteMediaAlbumDetail(long id)
        {
            var errorCode = MediaAlbumBo.DeleteMediaAlbumDetail(id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var strParamValue = NewtonJson.Serialize(id, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "deletemediaalbumdetail", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public MediaAlbumDetailEntity GetMediaAlbumDetailForEditById(int mediaAlbumId)
        {
            return MediaAlbumBo.GetMediaAlbumDetailForEditById(mediaAlbumId);
        }
        public WcfActionResponse CreateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {
            var newMediaAlbumId = 0;
            var errorCode = MediaAlbumBo.CreateMediaAlbumDetail(MediaAlbumForEdit, ref newMediaAlbumId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + newMediaAlbumId + "}";
                    var strParamValue = NewtonJson.Serialize(MediaAlbumForEdit, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertmediaalbumdetail", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newMediaAlbumId.ToString(),"Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.UpdateMediaAlbumDetail(MediaAlbumForEdit);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + MediaAlbumForEdit.Id + "}";
                    var strParamValue = NewtonJson.Serialize(MediaAlbumForEdit, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatemediaalbumdetail", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion
    }
}