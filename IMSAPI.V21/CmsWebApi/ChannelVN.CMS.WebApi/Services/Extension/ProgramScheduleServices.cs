﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.ProgramSchedule.Bo;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.ProgramSchedule.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using System.Globalization;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class ProgramScheduleServices
    {
        #region ProgramChannel

        #region Update

        public WcfActionResponse SaveProgramChannel(ProgramChannelEntity programChannel)
        {
            var id = 0;
            if (ProgramChannelBo.Save(programChannel, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                if (programChannel.Id > 0)
                {
                    CmsTrigger.Instance.OnUpdateProgramChannel(programChannel);
                }
                return WcfActionResponse.CreateSuccessResponse(id.ToString(CultureInfo.CurrentCulture), null);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse DeleteProgramById(int id)
        {
            if (ProgramChannelBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                if (id > 0)
                {
                    CmsTrigger.Instance.OnDeleteProgramChannel(id);
                }
                return WcfActionResponse.CreateSuccessResponse(id.ToString(CultureInfo.CurrentCulture), null);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }


        public WcfActionResponse MoveProgramChannelUp(int id)
        {
            try
            {
                if (ProgramChannelBo.MoveUp(id) == ErrorMapping.ErrorCodes.Success)
                {
                    if (id > 0)
                    {
                        CmsTrigger.Instance.OnMoveUpProgramChannel(id);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ProgramChannelNotAvailable,
                                                             ErrorMapping.Current[ErrorMapping.ErrorCodes.ProgramChannelNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse MoveProgramChannelDown(int id)
        {
            try
            {
                if (ProgramChannelBo.MoveDown(id) == ErrorMapping.ErrorCodes.Success)
                {
                    if (id > 0)
                    {
                        CmsTrigger.Instance.OnMoveDownProgramChannel(id);
                    }
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.ProgramChannelNotAvailable,
                                                             ErrorMapping.Current[ErrorMapping.ErrorCodes.ProgramChannelNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region Get

        public ProgramChannelEntity GetProgramChannelDetail(int id)
        {
            return ProgramChannelBo.GetByProgramChannelId(id);
        }

        public List<ProgramChannelEntity> SearchProgramChannel(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return ProgramChannelBo.Search(keyword, status, pageIndex, pageSize, ref totalRow);
        }

        #endregion

        public List<ProgramChannelZoneVideoEntity> GetZoneVideo()
        {
            return ProgramChannelBo.GetZoneVideo();
        }

        #endregion        

        #region Program schedule
        public WcfActionResponse InsertProgramSchedule(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {            
            if (ProgramScheduleBo.InsertProgramSchedule(programSchedule, ref newProgramScheduleId) == ErrorMapping.ErrorCodes.Success)
            {
                if(newProgramScheduleId > 0 && programSchedule.Status==1)
                {
                    programSchedule.Id = newProgramScheduleId;
                    CmsTrigger.Instance.OnAddProgramSchedule(programSchedule);
                }
                return WcfActionResponse.CreateSuccessResponse(newProgramScheduleId.ToString(CultureInfo.CurrentCulture), null);
            }
            
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }
        public WcfActionResponse UpdateProgramSchedule(ProgramScheduleEntity programSchedule)
        {            
            if (ProgramScheduleBo.UpdateProgramSchedule(programSchedule) == ErrorMapping.ErrorCodes.Success)
            {
                if (programSchedule.Id > 0)
                {                    
                    CmsTrigger.Instance.OnUpdateProgramSchedule(programSchedule);
                }
                return WcfActionResponse.CreateSuccessResponse(programSchedule.Id.ToString(CultureInfo.CurrentCulture), null);
            }
            
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }
        public WcfActionResponse DeleteProgramSchedule(int programScheduleId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.DeleteProgramSchedule(programScheduleId) == ErrorMapping.ErrorCodes.Success)
            {
                if (programScheduleId > 0)
                {
                    CmsTrigger.Instance.OnDeleteProgramSchedule(programScheduleId);
                }
                return WcfActionResponse.CreateSuccessResponse(programScheduleId.ToString(CultureInfo.CurrentCulture), null);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }
        public List<ProgramScheduleEntity> SearchProgramSchedule(string keyword, DateTime date, EnumProgramScheduleStatus status, int programChannelId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ProgramScheduleBo.SearchProgramSchedule(keyword, date, status, programChannelId, pageIndex, pageSize, ref totalRow);
        }
        public ProgramScheduleEntity GetProgramScheduleByProgramScheduleId(int id)
        {
            return ProgramScheduleBo.GetProgramScheduleByProgramScheduleId(id);
        }
        public WcfActionResponse CheckExistProgramSchedule(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.CheckExistProgramSchedule(programSchedule, ref newProgramScheduleId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<ProgramScheduleEntity> CheckExistListProgramScheduleByDate(string strDate, int channelId)
        {            
            try
            {
                var lstReturn = new List<ProgramScheduleEntity>();
                var arrDate = strDate.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var sDate in arrDate)
                {
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    var date = DateTime.MinValue;
                    if (DateTime.TryParseExact(sDate, new[] { "d/M/yyyy", "dd/MM/yyyy" }, provider, DateTimeStyles.None, out date))
                    {
                        var entity = new ProgramScheduleEntity()
                        {
                            ProgramChannelId = channelId,
                            ScheduleDate = date
                        };
                        int newProgramScheduleId = 0;
                        CheckExistProgramSchedule(entity,ref newProgramScheduleId);
                        if (newProgramScheduleId > 0)
                        {
                            lstReturn.Add(entity);
                        }
                    }
                }
                return lstReturn;
            }
            catch
            {
                return new List<ProgramScheduleEntity>();
            }

        }

        #endregion

        #region Program schedule detail

        public WcfActionResponse InsertProgramScheduleDetail(ProgramScheduleDetailEntity programScheduleDetail, ref int newProgramScheduleDetailId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.InsertProgramScheduleDetail(programScheduleDetail, ref newProgramScheduleDetailId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(newProgramScheduleDetailId.ToString(CultureInfo.CurrentCulture), null);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse InsertListProgramScheduleDetail(List<ProgramScheduleDetailEntity> programScheduleDetails, string listDeleteId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            try
            {
                var currentUser = WcfMessageHeader.Current.ClientUsername;
                if (!string.IsNullOrEmpty(listDeleteId))
                {
                    ProgramScheduleBo.DeleteListProgramScheduleDetail(listDeleteId);
                }
                foreach (var programScheduleDetailEntity in programScheduleDetails)
                {
                    programScheduleDetailEntity.CreatedBy = currentUser;
                    int newScheduleId = 0;
                    if (programScheduleDetailEntity.Id != 0)
                    {
                        ProgramScheduleBo.UpdateProgramScheduleDetail(programScheduleDetailEntity);
                    }
                    else
                    {
                        ProgramScheduleBo.InsertProgramScheduleDetail(programScheduleDetailEntity, ref newScheduleId);
                    }
                }
                //cd
                if (programScheduleDetails!=null && programScheduleDetails.Count > 0)
                {
                    CmsTrigger.Instance.OnUpdateListProgramScheduleDetail(programScheduleDetails, listDeleteId);
                }

                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse("Error: " + ex.Message);
            }            
        }

        public WcfActionResponse UpdateProgramScheduleDetail(ProgramScheduleDetailEntity programScheduleDetail)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.UpdateProgramScheduleDetail(programScheduleDetail) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(programScheduleDetail.Id.ToString(CultureInfo.CurrentCulture), null);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }
        public WcfActionResponse DeleteProgramScheduleDetail(int programScheduleDetailId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.DeleteProgramScheduleDetail(programScheduleDetailId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(programScheduleDetailId.ToString(CultureInfo.CurrentCulture), null);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }
        public List<ProgramScheduleDetailEntity> GetProgramScheduleDetailByProgramScheduleId(int programScheduleId, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleBo.GetProgramScheduleDetailByProgramScheduleId(programScheduleId, status);
        }
        public List<ProgramScheduleDetailEntity> ProgramScheduleDetailSearchExport(int programChannelId, DateTime fromDate, DateTime toDate, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleBo.ProgramScheduleDetailSearchExport(programChannelId, fromDate, toDate, status);
        }
        public List<ProgramScheduleDetailEntity> GetProgramScheduleDetailByScheduleDate(int programChannelId, DateTime scheduleDate, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleBo.GetProgramScheduleDetailByScheduleDate(programChannelId, scheduleDate, status);
        }
        public ProgramScheduleDetailEntity GetProgramScheduleDetailByProgramScheduleDetailId(int id)
        {
            return ProgramScheduleBo.GetProgramScheduleDetailByProgramScheduleDetailId(id);
        }

        #endregion

        #region Import Program Schedule
        public WcfActionResponse ImportProgramSchedule(List<ProgramChannelEntity> programChannel)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                //var zoneVideo = ProgramChannelBo.GetZoneVideo();
                //var zoneid = 0;
                var currentUser = WcfMessageHeader.Current.ClientUsername;
                foreach (var programChannelEntity in programChannel)
                {
                    //ProgramChannelEntity entity = programChannelEntity;
                    //var zoneVideobyChannelId = zoneVideo.Where(c => c.ProgramChannelId == entity.Id).ToList();
                    foreach (var programScheduleEntity in programChannelEntity.ListProgramSchedule)
                    {
                        int programScheduleId = 0;
                        programScheduleEntity.CreatedBy = currentUser;
                        ProgramScheduleBo.InsertProgramSchedule(programScheduleEntity, ref programScheduleId);
                        foreach (var programScheduleDetailEntity in programScheduleEntity.ListProgramScheduleDetail)
                        {
                            //var zoneVideoEntity = zoneVideobyChannelId.FirstOrDefault(c => c.ZoneName.Contains(programScheduleDetailEntity.Title) || programScheduleDetailEntity.Title.Contains(c.ZoneName));
                            //if (zoneVideoEntity != null)
                            //{
                            //    zoneid = zoneVideoEntity.ZoneId;
                            //}
                            programScheduleDetailEntity.ProgramScheduleId = programScheduleId;
                            programScheduleDetailEntity.CreatedBy = currentUser;
                            programScheduleDetailEntity.CreatedDate = programScheduleEntity.CreatedDate;
                            programScheduleDetailEntity.ShowOnSchedule = false;
                            //programScheduleDetailEntity.ZoneVideoId = zoneid;
                            int newScheduleId = 0;
                            ProgramScheduleBo.InsertProgramScheduleDetail(programScheduleDetailEntity,
                                                                          ref newScheduleId);
                        }
                    }
                }
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}