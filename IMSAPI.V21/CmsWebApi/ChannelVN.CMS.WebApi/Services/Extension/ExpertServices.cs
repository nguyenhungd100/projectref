﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.Expert.Bo;
using ChannelVN.Expert.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class ExpertServices
    {
        public WcfActionResponse ExpertInsert(ExpertEntity expertEntity, ref int expertId)
        {
            var errorCode = ExpertBo.Insert(expertEntity, ref expertId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(expertId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public ExpertEntity ExpertGetById(int id)
        {
            return ExpertBo.GetById(id);
        }

        public WcfActionResponse ExpertUpdate(ExpertEntity expertEntity)
        {
            var errorCode = ExpertBo.Update(expertEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(expertEntity), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<ExpertEntity> ListExpertSearch(string keyword)
        {
            return ExpertBo.Search(keyword);
        }

        public List<ExpertEntity> SearchExpert(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return ExpertBo.SearchExpert(keyword, pageIndex, pageSize, ref totalRow);
        }

        public WcfActionResponse ExpertDelete(int id)
        {
            var errorCode = ExpertBo.Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public ExpertEntity ExpertGetByNewsId(int id)
        {
            return ExpertBo.GetExpertByNewsId(id);
        }

        public WcfActionResponse UpdateExpertInNews(ExpertInNews expertEntity)
        {
            var errorCode = ExpertBo.InsertExpertIntoNews(expertEntity);            

            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateTelegramId(int expertId, long telegramId, string nameSpace, long expireTime)
        {                        
            var expert = ExpertBo.GetById(expertId);
            if (expert == null)
            {                
                return WcfActionResponse.CreateErrorResponse(210586, "Không tìm thấy chuyên gia thẩm định");
            }

            if (expireTime < Utility.DateTimeToSpan10(DateTime.Now))
            {
                return WcfActionResponse.CreateErrorResponse(210585, "Link đăng ký tích hợp tài khoản Telegram này không còn hiệu lực.");
            }

            if(expert!=null && expert.TelegramId > 0)
            {
                return WcfActionResponse.CreateErrorResponse(210587, "Chuyên gia thẩm định này đã được đăng ký Telegram.");
            }

            var errorCode = ExpertBo.UpdateTelegramId(expertId, telegramId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {                
                return WcfActionResponse.CreateSuccessResponse("Success.");
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse ResetTelegram(int expertId, string acc)
        {
            var expert = ExpertBo.GetById(expertId);
            if (expert == null)
            {
                return WcfActionResponse.CreateErrorResponse(210586, "Không tìm thấy chuyên gia thẩm định");
            }
            
            var errorCode = ExpertBo.UpdateTelegramId(expertId, 0);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                Logger.WriteLog(Logger.LogType.Trace, "UserName: "+ acc + " => Reset Telegram For Expert Id: "+ expertId);
                return WcfActionResponse.CreateSuccessResponse("Success.");
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        #region BoxExpertNewsEmbed
        public List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type)
        {
            return BoxExpertNewsEmbedBo.GetListBoxExpertNewsEmbed(zoneId, type);
        }

        public WcfActionResponse InsertBoxExpertNewsEmbed(BoxExpertNewsEmbedEntity newsEmbedBox)
        {
            var errorCode = BoxExpertNewsEmbedBo.Insert(newsEmbedBox);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsEmbedBox.NewsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateBoxExpertNewsEmbed(string listNewsId, int zoneId, int type)
        {
            var errorCode = BoxExpertNewsEmbedBo.Update(listNewsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteBoxExpertNewsEmbed(long newsId, int zoneId, int type)
        {
            var errorCode = BoxExpertNewsEmbedBo.Delete(newsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        public List<ExpertInZone> ExpertGetByExpertId(int id)
        {
            return ExpertBo.GetExpertByExpertId(id);
        }

        public WcfActionResponse UpdateExpertInZone(ExpertInZone expertEntity)
        {
            var errorCode = ExpertBo.InsertExpertIntoZone(expertEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteExpertZone(int expertId)
        {
            var errorCode = ExpertBo.DeleteExpertZone(expertId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<NewsInListEntity> SearchExpertNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {            
            return ExpertBo.SearchExpertNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRows);
        }

        #region ExpertInNews
        public WcfActionResponse ExpertInNews_UpdateNews(int expertId, string deleteNewsId, string addNewsId)
        {
            var errorCode = ExpertBo.ExpertInNews_UpdateNews(expertId, deleteNewsId, addNewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<ExpertInNewsEntity> ExpertInNews_GetByExpertId(int expertId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ExpertBo.ExpertInNews_GetByExpertId(expertId, pageIndex, pageSize, ref totalRow);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            return new ErrorMessageEntity[] { };
        }
    }
}