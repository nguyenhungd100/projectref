﻿using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using System;
using System.Linq;
using ChannelVN.CMS.BO.Nodejs;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class NewsStoresServices
    {
        #region NewsStores

        public static NodeJs_ChannelsEntity ListChannel()
        {
            return NewsStoresNodeJsServices.ListChannel();
        }

        public static NodeJs_NewsResponseEntity SearchNewsStore(string keyword, int channelId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize)
        {
            return NewsStoresNodeJsServices.SearchNewsStore(keyword, channelId, fromDate, toDate, pageIndex, pageSize);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}