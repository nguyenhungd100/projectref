﻿using ChannelVN.Advertisment.Bo;
using ChannelVN.Advertisment.Entity;
using ChannelVN.Advertisment.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class AdvertismentServices
    {
        #region Advertisment

        public List<AdvertismentEntity> GetAllAdvertisment()
        {
            return AdvertismentBo.GetAllAdvertisment();
        }

        public List<AdvertismentEntity> Search(string keyword, EnumAdvertismentType type, int zoneId, int displayStyle, EnumAdvertismentStatus status, int positionId,  int pageIndex, int pageSize, ref int totalRow)
        {
            return AdvertismentBo.Search(keyword, type, zoneId, displayStyle, status, positionId,  pageIndex, pageSize, ref totalRow);
        }

        public List<AdvertismentEntity> GetAdvertismentByPosition(EnumAdvertismentType type, int zoneId, int displayStyle, EnumAdvertismentStatus status, int positionId)
        {
            return AdvertismentBo.GetAdvertismentByPosition(type, zoneId, displayStyle, status, positionId);
        }

        public AdvertismentDetailEntity GetById(int id)
        {
            return AdvertismentBo.GetAdvertismentById(id);
        }                
        public WcfActionResponse Create(AdvertismentEntity obj, string listZones)
        {
            var id = 0;
            var errorCode = AdvertismentBo.InsertAdvertisment(obj, listZones);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";                                        
                    var strParamValue = NewtonJson.Serialize(new { Advertisment = obj, ListZones= listZones }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertadvertisment", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse Update(AdvertismentEntity obj, string listZones)
        {
            var errorCode = AdvertismentBo.UpdateAdvertisment(obj, listZones);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + obj.Id + "}";                    
                    var strParamValue = NewtonJson.Serialize(new { Advertisment = obj, ListZones = listZones }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateadvertisment", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse Delete(int id)
        {
            var errorCode = AdvertismentBo.DeleteById(id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var strParamValue = NewtonJson.Serialize(id, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "deleteadvertisment", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateStatus(int id, EnumAdvertismentStatus status)
        {
            var errorCode = AdvertismentBo.UpdateAdvertismentStatus(id, status);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var strParamValue = NewtonJson.Serialize(new { id, status }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatestatusadvertisment", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion        
    }
}