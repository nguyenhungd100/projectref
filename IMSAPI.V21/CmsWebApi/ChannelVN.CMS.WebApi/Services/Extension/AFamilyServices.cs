﻿using ChannelVN.AFamily.Bo;
using ChannelVN.AFamily.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Services.Common;
using System;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class AFamilyServices
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            return new ErrorMessageEntity[] { };
        }

        public WcfActionResponse UpdateParentNewsId(long newsId, long id)
        {
            try
            {
                var errorCode = NewsBo.UpdateUpdateParentNewsId(newsId, id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
    }
}