﻿using ChannelVN.CMS.BO.Base.NewsRoyaltiesEx;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.NewsRoyaltiesEx;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class NewsRoyaltiesExServices
    {
        public List<NewsSearchEntity> SearchNewsRoyalty(string accountName, string zoneIds, string catIds, DateTime fromDate, DateTime toDate, int pageIndex, int pagaSize, ref int totalRows)
        {
            return NewsRoyaltiesExBo.SearchNewsRoyalty(accountName, zoneIds, catIds, fromDate, toDate, pageIndex, pagaSize, ref totalRows);
        }

        public List<NewsCategoryEntity> GetListCateRoyalty()
        {
            return NewsRoyaltiesExBo.GetListCateRoyalty();
        }
    }
}