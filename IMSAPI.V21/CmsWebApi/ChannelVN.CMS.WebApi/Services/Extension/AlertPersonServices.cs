﻿using System;
using ChannelVN.CMS.BO.Base.AlertPerson;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AlertPerson;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class AlertPersonServices
    {
        public AlertPersonEntity GetAlertPersonById(int personId)
        {
            return BoFactory.GetInstance<AlertPersonBo>().GetAlertPersonById(personId);
        }
        public AlertPersonEntity[] Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoFactory.GetInstance<AlertPersonBo>().Search(keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public WcfActionResponse InsertTag(AlertPersonEntity tag)
        {
            WcfActionResponse responseData;
            int newTagId = 0;
            var errorCode = BoFactory.GetInstance<AlertPersonBo>().InsertTag(tag, ref newTagId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                tag.Id = newTagId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
            }
            else
            {
                if (newTagId > 0)
                {
                    tag.Id = newTagId;
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            return responseData;
        }
        public WcfActionResponse Update(AlertPersonEntity tag)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<AlertPersonBo>().Update(tag);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse DeleteById(int tagId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<AlertPersonBo>().DeleteById(tagId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
    }
}