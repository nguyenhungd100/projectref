﻿using System;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.SEO.BO.SEOMetaVideo;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.BO.SEOMetaNews;
using ChannelVN.SEO.Entity.ErrorCode;
using ChannelVN.SEO.BO.SEOMetaTags;
using ChannelVN.CMS.WebApi.Services.Common;
using System.Collections.Generic;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.SEO.BO.SEOMetaTopic;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class SeoServices
    {
        #region Seo Meta News
        public WcfActionResponse SEOMetaNewsInsert(SEOMetaNewsEntity seoMetaNewsEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var voteId = 0;
            if (SEOMetaNewsBo.Insert(seoMetaNewsEntity) == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"NewsId\":" + seoMetaNewsEntity.NewsId + "}";
                    var strParamValue = NewtonJson.Serialize(seoMetaNewsEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertseometanews", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }

                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = voteId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse SEOMetaNewsUpdate(SEOMetaNewsEntity seoMetaNewsEntity)
        {
            var errorCode = SEOMetaNewsBo.Update(seoMetaNewsEntity);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success)
            {
                try
                {
                    var jsonKey = "{\"NewsId\":" + seoMetaNewsEntity.NewsId + "}";
                    var strParamValue = NewtonJson.Serialize(seoMetaNewsEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateseometanews", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return responseData;
        }
        public SEOMetaNewsEntity GetSeoMetaNewsById(long id)
        {
            return SEOMetaNewsBo.GetSEOMetaNewsById(id);
        }
        #endregion

        #region SEO Meta Tag
        public WcfActionResponse SEOMetaTagsInsert(SEOMetaTagsEntity seoMetaTagsEntity)
        {            
            var tagId = 0;
            var res = SEOMetaTagsBo.Insert(seoMetaTagsEntity, ref tagId);
            if (res == ErrorMapping.ErrorCodes.Success)
            {                
                try
                {                    
                    seoMetaTagsEntity.Id = tagId;
                    var jsonKey = "{\"Id\":" + seoMetaTagsEntity.Id + "}";
                    var strParamValue = NewtonJson.Serialize(seoMetaTagsEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertseometatag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }                

                return WcfActionResponse.CreateSuccessResponse(tagId.ToString(), "Success");                
            }
            else if (res == ErrorMapping.ErrorCodes.DuplicateError)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.DuplicateError, "Chuyên mục này đã có thông tin meta.");
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);                
            }            
        }

        public WcfActionResponse SEOMetaTagsUpdate(SEOMetaTagsEntity seoMetaTagsEntity)
        {
            var errorCode = SEOMetaTagsBo.Update(seoMetaTagsEntity);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (responseData.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + seoMetaTagsEntity.Id + "}";
                    var strParamValue = NewtonJson.Serialize(seoMetaTagsEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateseometatag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return responseData;
        }

        public WcfActionResponse SEOMetaTagsDelete(int id)
        {
            var errorCode = SEOMetaTagsBo.Delete(id);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (responseData.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";                    
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deleteseometatag", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return responseData;
        }
        public SEOMetaTagsEntity GetSeoMetaTagsById(long id)
        {
            return SEOMetaTagsBo.GetSeoMetaTagsById(id);
        }

        public List<SEOMetaTagsEntity> GetListSeoMetaTags(string keyWord, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            return SEOMetaTagsBo.GetListSeoMetaTags(keyWord, zoneId, pageIndex, pageSize, ref totalRow);
        }
        #endregion

        #region Seo Meta Video
        public WcfActionResponse SEOMetaVideoInsert(SEOMetaVideoEntity seoMetaVideoEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var voteId = 0;
            if (SEOMetaVideoBo.Insert(seoMetaVideoEntity) == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"VideoId\":" + seoMetaVideoEntity.VideoId + "}";
                    var strParamValue = NewtonJson.Serialize(seoMetaVideoEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertseometavideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }

                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = voteId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse SEOMetaVideoUpdate(SEOMetaVideoEntity seoMetaVideoEntity)
        {
            var errorCode = SEOMetaVideoBo.Update(seoMetaVideoEntity);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success)
            {
                try
                {
                    var jsonKey = "{\"VideoId\":" + seoMetaVideoEntity.VideoId + "}";
                    var strParamValue = NewtonJson.Serialize(seoMetaVideoEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateseometavideo", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return responseData;
        }
        public SEOMetaVideoEntity GetSeoMetaVideoById(int id)
        {
            return SEOMetaVideoBo.GetSEOMetaVideoById(id);
        }
        #endregion

        #region SEO Meta Topic
        public WcfActionResponse SEOMetaTopicInsert(SEOMetaTopicEntity seoMetaTopicEntity)
        {
            var tagId = 0;
            var res = SEOMetaTopicBo.Insert(seoMetaTopicEntity, ref tagId);
            if (res == ErrorMapping.ErrorCodes.Success)
            {
                try
                {                    
                    var jsonKey = "{\"TopicId\":" + seoMetaTopicEntity.TopicId + "}";
                    var strParamValue = NewtonJson.Serialize(seoMetaTopicEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertseometatopic", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }

                return WcfActionResponse.CreateSuccessResponse(seoMetaTopicEntity.TopicId.ToString(), "Success");
            }
            else if (res == ErrorMapping.ErrorCodes.DuplicateError)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.DuplicateError, "Chuyên mục này đã có thông tin meta.");
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError, ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
            }
        }

        public WcfActionResponse SEOMetaTopicUpdate(SEOMetaTopicEntity seoMetaTopicEntity)
        {
            var errorCode = SEOMetaTopicBo.Update(seoMetaTopicEntity);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (responseData.Success)
            {
                try
                {
                    var jsonKey = "{\"TopicId\":" + seoMetaTopicEntity.TopicId + "}";
                    var strParamValue = NewtonJson.Serialize(seoMetaTopicEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updateseometatopic", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return responseData;
        }

        public WcfActionResponse SEOMetaTopicDelete(int topicid)
        {
            var errorCode = SEOMetaTopicBo.Delete(topicid);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            if (responseData.Success)
            {
                try
                {
                    var jsonKey = "{\"TopicId\":" + topicid + "}";
                    ContentDeliveryServices.PushToDataCD(jsonKey, "deleteseometatopic", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }

            return responseData;
        }
        public SEOMetaTopicEntity GetSeoMetaTopicByTopicId(int id)
        {
            return SEOMetaTopicBo.GetSeoMetaTopicByTopicId(id);
        }

        public List<SEOMetaTopicEntity> GetListSeoMetaTopic(string keyWord, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            return SEOMetaTopicBo.GetListSeoMetaTopic(keyWord, zoneId, pageIndex, pageSize, ref totalRow);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}