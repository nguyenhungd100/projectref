﻿using ChannelVN.CMS.BO.Base.LandingTemplate;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.LandingTemplate;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class LandingTemplateServices
    {
        #region LandingTemplate

        public List<LandingTemplateEntity> Search(string keyword, int cateId, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return LandingTemplateBo.Search(keyword, cateId, status, type, pageIndex, pageSize, ref totalRow);
        }
        public LandingTemplateEntity GetById(int id)
        {
            return LandingTemplateBo.GetById(id);
        }                
        public WcfActionResponse Create(LandingTemplateEntity obj)
        {
            var id = 0;
            var errorCode = LandingTemplateBo.Create(obj, ref id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";                                        
                    var strParamValue = NewtonJson.Serialize(obj, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertlandingtemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse Update(LandingTemplateEntity obj)
        {
            var errorCode = LandingTemplateBo.Update(obj);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + obj.Id + "}";                    
                    var strParamValue = NewtonJson.Serialize(obj, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatelandingtemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse Delete(int id)
        {
            var errorCode = LandingTemplateBo.Delete(id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var strParamValue = NewtonJson.Serialize(id, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "deletelandingtemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateStatus(int id, string action)
        {
            var errorCode = LandingTemplateBo.UpdateStatus(id, action);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                try
                {
                    var jsonKey = "{\"Id\":" + id + "}";
                    var strParamValue = NewtonJson.Serialize(new { id, action }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatestatuslandingtemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region LandingTemplateCategory

        public List<LandingTemplateCategoryEntity> GetAllCategory()
        {
            return LandingTemplateBo.GetAllCategory();
        }
        public LandingTemplateCategoryEntity GetCategoryById(int id)
        {
            return LandingTemplateBo.GetCategoryById(id);
        }
        public WcfActionResponse CreateCategory(LandingTemplateCategoryEntity obj)
        {
            var id = 0;
            var errorCode = LandingTemplateBo.CreateCategory(obj, ref id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //try
                //{
                //    var jsonKey = "{\"Id\":" + id + "}";
                //    var strParamValue = NewtonJson.Serialize(obj, "yyyy-MM-dd'T'HH:mm:ss");
                //    ContentDeliveryServices.PushToDataCD(strParamValue, "insertlandingtemplatecategory", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                //}
                //catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "Success")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateCategory(LandingTemplateCategoryEntity obj)
        {
            var errorCode = LandingTemplateBo.UpdateCategory(obj);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //try
                //{
                //    var jsonKey = "{\"Id\":" + obj.Id + "}";
                //    var strParamValue = NewtonJson.Serialize(obj, "yyyy-MM-dd'T'HH:mm:ss");
                //    ContentDeliveryServices.PushToDataCD(strParamValue, "updatelandingtemplatecategory", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                //}
                //catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteCategory(int id)
        {
            var errorCode = LandingTemplateBo.DeleteCategory(id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                //try
                //{
                //    var jsonKey = "{\"Id\":" + id + "}";
                //    var strParamValue = NewtonJson.Serialize(id, "yyyy-MM-dd'T'HH:mm:ss");
                //    ContentDeliveryServices.PushToDataCD(strParamValue, "deletelandingtemplateCategory", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                //}
                //catch { }
            }
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }        
        #endregion
    }
}