﻿using ChannelVN.BigStory.Bo;
using ChannelVN.BigStory.Entity;
using ChannelVN.BigStory.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class BigStoryServices
    {
        #region BigStory
        public List<NodeJs_BigStoryEntity> BigStory_Search(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return BigStoryBo.Search(fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public NodeJs_BigStoryEntity BigStory_GetById(string Id)
        {
            return BigStoryBo.GetBigStoryById(Id);
        }
        public WcfActionResponse BigStory_Update(NodeJs_BigStoryEntity entity)
        {
            return BigStoryBo.UpdateBigStory(entity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        //public WcfActionResponse BigStory_UpdateAllItem(BigStoryEntity entity, List<BigStoryItemEntity> ListBigStoryItem)
        //{
        //    return BigStoryBo.UpdateBigStory(entity, ListBigStoryItem) == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[
        //                                                           ErrorMapping.ErrorCodes.BusinessError]);
        //}
        public WcfActionResponse BigStory_Insert(NodeJs_BigStoryEntity entity, ref string Id)
        {            

            var errorCode = BigStoryBo.InsertBigStory(entity, ref Id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

        }
        //public WcfActionResponse BigStory_InsertAllItem(BigStoryEntity entity, List<BigStoryItemEntity> ListBigStoryItem)
        //{
        //    return BigStoryBo.InsertBigStory(entity, ListBigStoryItem) == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[
        //                                                           ErrorMapping.ErrorCodes.BusinessError]);
        //}
        //public WcfActionResponse BigStory_UpdateStatus(int Id, BigStoryStatus status)
        //{
        //    return BigStoryBo.BigStory_UpdateStatus(Id, (int)status) == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[
        //                                                           ErrorMapping.ErrorCodes.BusinessError]);
        //}
        //public WcfActionResponse BigStory_UpdateIsFocus(int Id, bool IsFocus)
        //{
        //    return BigStoryBo.BigStory_UpdateIsFocus(Id, IsFocus) == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[
        //                                                           ErrorMapping.ErrorCodes.BusinessError]);
        //}
        //public WcfActionResponse BigStory_Delete(int Id)
        //{
        //    return BigStoryBo.DeleteBigStory(Id) == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[
        //                                                           ErrorMapping.ErrorCodes.BusinessError]);
        //}
        #endregion

        #region BigStoryItem
        public List<NodeJs_BigStoryItemEntity> BigStoryItem_GetByBigStoryId(string BigStoryId)
        {
            return BigStoryItemBo.BigStoryItem_GetByBigStoryId(BigStoryId);
        }
        public NodeJs_BigStoryItemEntity BigStoryItem_GetById(string Id)
        {
            return BigStoryItemBo.BigStoryItem_GetById(Id);
        }
        public WcfActionResponse BigStoryItem_Update(NodeJs_BigStoryItemEntity entity)
        {
            return BigStoryItemBo.UpdateBigItemStory(entity);
        }
        public WcfActionResponse BigStoryItem_Insert(NodeJs_BigStoryItemEntity entity, ref string Id)
        {            
            var errorCode = BigStoryItemBo.InsertBigItemStory(entity, ref Id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse BigStoryItem_InsertByList(List<BigStoryItemMappingEntity> list, string bigStoryId)
        {
            return BigStoryItemBo.InsertBigItemStoryByList(list, bigStoryId);
        }
        //public WcfActionResponse BigStoryItem_UpdateByList(List<BigStoryItemEntity> list, int bigStoryId)
        //{
        //    return BigStoryItemBo.UpdateBigItemStoryByList(list, bigStoryId);
        //}
        public WcfActionResponse BigStoryItem_Delete(string Id)
        {
            return BigStoryItemBo.DeleteBigStoryItem(Id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        //public WcfActionResponse BigStoryItem_UpdateIsHighlight(int Id, bool IsHighlight)
        //{
        //    return BigStoryItemBo.BigStoryItem_UpdateIsHighlight(Id, IsHighlight) == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[
        //                                                           ErrorMapping.ErrorCodes.BusinessError]);
        //}
        //public WcfActionResponse BigStoryItem_UpdateStatus(int Id, BigStoryItemStatus status)
        //{
        //    return BigStoryItemBo.BigStoryItem_UpdateStatus(Id, (int)status) == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[
        //                                                           ErrorMapping.ErrorCodes.BusinessError]);
        //}
        #endregion

        #region BigStoryFocusItem
        //public List<BigStoryFocusItemEntity> BigStoryFocusItem_GetByBigStoryId(int BigStoryId)
        //{
        //    return BigStoryFocusItemBo.BigStoryItem_GetByBigStoryId(BigStoryId);
        //}
        //public WcfActionResponse BigStoryFocusItem_Update(BigStoryFocusItemEntity entity)
        //{
        //    return BigStoryFocusItemBo.UpdateBigItemStory(entity);
        //}
        //public WcfActionResponse BigStoryFocusItem_Insert(BigStoryFocusItemEntity entity)
        //{
        //    return BigStoryFocusItemBo.InsertBigItemStory(entity);
        //}
        //public BigStoryFocusItemEntity BigStoryFocusItem_GetById(int Id)
        //{
        //    return BigStoryFocusItemBo.BigStoryItem_GetById(Id);
        //}
        //public WcfActionResponse BigStoryFocusItem_InsertByList(List<BigStoryFocusItemEntity> list)
        //{
        //    return BigStoryFocusItemBo.InsertBigItemStoryByList(list);
        //}
        //public WcfActionResponse BigStoryFocusItem_UpdateByList(List<BigStoryFocusItemEntity> list, int bigStoryId)
        //{
        //    return BigStoryFocusItemBo.UpdateBigItemStoryByList(list, bigStoryId);
        //}
        //public WcfActionResponse BigStoryFocusItem_Delete(int Id)
        //{
        //    return BigStoryFocusItemBo.DeleteBigStoryItem(Id) == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
        //                                                       ErrorMapping.Current[
        //                                                           ErrorMapping.ErrorCodes.BusinessError]);
        //}
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}