﻿using ChannelVN.BrandContent.Bo;
using ChannelVN.BrandContent.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class BrandContentServices
    {
        public WcfActionResponse BrandContent_Insert(BrandContentEntity BrandContent)
        {
            var errorCode = BrandContentBo.BrandContent_Insert(BrandContent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse BrandContent_Update(BrandContentEntity BrandContent)
        {
            var errorCode = BrandContentBo.BrandContent_Update(BrandContent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(BrandContent), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<BrandContentEntity> BrandContent_Search(string Keyword, int pageIndex, int pageSize, ref int TotalRow)
        {
            return BrandContentBo.BrandContent_Search(Keyword, pageIndex, pageSize, ref TotalRow);
        }
        public BrandContentEntity[] BrandInNews_GetByNewsId(long NewsId)
        {
            return BrandContentBo.BrandInNews_GetByNewsId(NewsId).ToArray();
        }
        public BrandContentEntity BrandContent_GetById(int BrandId)
        {
            return BrandContentBo.BrandContent_GetById(BrandId);
        }
        public WcfActionResponse BrandInNews_Insert(string brandIds, long NewsId)
        {
            var errorCode = BrandContentBo.BrandInNews_Insert(brandIds, NewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse BrandContent_Delete(int id)
        {
            var errorCode = BrandContentBo.BrandContent_Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
    }
}