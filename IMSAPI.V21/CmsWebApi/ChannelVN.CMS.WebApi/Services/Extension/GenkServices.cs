﻿using System;
using System.Linq;
using ChannelVN.CMS.BO.External.CafeF;
using ChannelVN.CMS.BO.External.GenK.GiftCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeF.Expert;
using ChannelVN.CMS.Entity.External.GenK.GiftCode;
using ChannelVN.CMS.WebApi.Services.Common;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class GenkServices
    {
        public WcfActionResponse UpdateExpertInNews(ExpertInNews expertEntity)
        {
            var errorCode = NewsAuthorBo.InsertExpertIntoNews(expertEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #region GameGifCode
        public WcfActionResponse InsertGiftCode(long newsId, string giftCode, string tags)
        {            
            if (GameGiftCodeBo.InsertGiftCode(newsId, giftCode, tags) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "");
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)GameProfile.Entity.ErrorCode.ErrorMapping.ErrorCodes.UnknowError, GameProfile.Entity.ErrorCode.ErrorMapping.Current[GameProfile.Entity.ErrorCode.ErrorMapping.ErrorCodes.UnknowError]);                
            }            
        }

        public GameGiftCodeEntity GetStartGiftCodeByNewsId(long newsId)
        {
            return GameGiftCodeBo.GetGiftCodeByNewsId(newsId);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(WcfActionResponse));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}