﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.SocialNetwork.BO.CmsDiscussion;
using ChannelVN.SocialNetwork.Entity;
using ChannelVN.SocialNetwork.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.BO.Base.NewsSocial;
using ChannelVN.CMS.WebApi.Services.ExternalServices;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class SocialServices
    {
        #region Discussion

        public WcfActionResponse CreateDiscussionTopic(DiscussionTopicEntity discussionTopic, string[] discussionTopicTags, DiscussionAttachmentEntity[] discussionAttachments)
        {
            var discussionTopicId = DiscussionBo.CreateDiscussionTopic(discussionTopic, new List<string>(discussionTopicTags), new List<DiscussionAttachmentEntity>(discussionAttachments));
            if (discussionTopicId > 0)
                return WcfActionResponse.CreateSuccessResponse(discussionTopicId.ToString(CultureInfo.InvariantCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse UpdateDiscussionTopic(DiscussionTopicEntity discussionTopic, string[] discussionTopicTags, DiscussionAttachmentEntity[] discussionAttachments)
        {
            var success = DiscussionBo.UpdateDiscussionTopic(discussionTopic, new List<string>(discussionTopicTags), new List<DiscussionAttachmentEntity>(discussionAttachments));
            if (success)
                return WcfActionResponse.CreateSuccessResponse(discussionTopic.Id.ToString(CultureInfo.InvariantCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse FollowDiscussionTopic(long discussionTopicId)
        {
            var success = DiscussionBo.FollowDiscussionTopic(discussionTopicId);
            if (success)
                return WcfActionResponse.CreateSuccessResponse(discussionTopicId.ToString(CultureInfo.InvariantCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse UnfollowDiscussionTopic(long discussionTopicId)
        {
            var success = DiscussionBo.UnfollowDiscussionTopic(discussionTopicId);
            if (success)
                return WcfActionResponse.CreateSuccessResponse(discussionTopicId.ToString(CultureInfo.InvariantCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse UpdateDiscussionTopicPriority(string listDiscussionTopicId)
        {
            if (DiscussionBo.UpdateDiscussionTopicPriority(listDiscussionTopicId))
                return WcfActionResponse.CreateSuccessResponse();
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse UpdateDiscussionPriority(string listDiscussionId)
        {
            if (DiscussionBo.UpdateDiscussionPriority(listDiscussionId))
                return WcfActionResponse.CreateSuccessResponse();
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse ResolveDiscussionTopic(long discussionTopicId, string userDoAction)
        {
            var returnValue = DiscussionBo.ResolveDiscussionTopic(discussionTopicId, userDoAction);
            return returnValue
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse DeleteDiscussionTopic(long discussionTopicId, string userDoAction)
        {
            var returnValue = DiscussionBo.DeleteDiscussionTopic(discussionTopicId, userDoAction);
            return returnValue
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse SendDiscussion(DiscussionEntity discussion, string[] discussionTags, DiscussionAttachmentEntity[] discussionAttachments)
        {
            var discussionId = DiscussionBo.SendDiscussion(discussion, new List<string>(discussionTags), new List<DiscussionAttachmentEntity>(discussionAttachments));
            if (discussionId > 0)
                return WcfActionResponse.CreateSuccessResponse(discussionId.ToString(CultureInfo.InvariantCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                       ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public DiscussionTopicEntity[] GetDiscussionTopicByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long parentId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status, int isFocus, bool isFilterByPermission, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiscussionBo.GetDiscussionTopicByApplicationIdAndObjectId(applicationId, parentId, objectId, userDoAction, keyword, zoneId, status, isFocus, isFilterByPermission, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public DiscussionEntity[] GetDiscussionByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiscussionBo.GetDiscussionByApplicationIdAndObjectId(applicationId, objectId, userDoAction, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public DiscussionWithChildEntity[] GetDiscussionByDiscussionTopicId(long discussionTopicId, long parentDiscussionId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiscussionBo.GetDiscussionByDiscussionTopicId(discussionTopicId, parentDiscussionId, userDoAction, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public DiscussionTopicDetailEntity[] GetDiscussionTopicDetailByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long parentId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status, int isFocus, bool isFilterByPermission, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiscussionBo.GetDiscussionTopicDetailByApplicationIdAndObjectId(applicationId, parentId, objectId, userDoAction, keyword, zoneId, status, isFocus, isFilterByPermission, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public DiscussionDetailEntity[] GetDiscussionDetailByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiscussionBo.GetDiscussionDetailByApplicationIdAndObjectId(applicationId, objectId, userDoAction, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public DiscussionTopicDetailEntity GetDiscussionTopicDetailByDiscussionTopicId(long discussionTopicId, string userDoAction)
        {
            return DiscussionBo.GetDiscussionTopicDetail(discussionTopicId, userDoAction);
        }

        public DiscussionDetailEntity GetDiscussionDetailByDiscussionId(long discussionTopicId, string userDoAction)
        {
            return DiscussionBo.GetDiscussionDetail(discussionTopicId, userDoAction);
        }

        public int CountDiscussionTopicByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status)
        {
            return DiscussionBo.CountDiscussionTopicByApplicationIdAndObjectId(applicationId, objectId, userDoAction, keyword, zoneId, status);
        }

        public int CountDiscussionByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction)
        {
            return DiscussionBo.CountDiscussionByApplicationIdAndObjectId(applicationId, objectId, userDoAction);
        }

        public int CountDiscussionByTopicId(long topicId)
        {
            return DiscussionBo.CountDiscussionByTopicId(topicId);
        }

        public string[] GetListUserInDiscussionTopic(long discussionTopicId, string userDoAction)
        {
            return DiscussionBo.GetListUserInDiscussionTopic(discussionTopicId, userDoAction).ToArray();
        }

        public List<DiscussionInTopicEntity> GetTopDiscussionTopicWithTopDiscussion(int topDiscussionTopic, int topDiscussion, long objectId, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds)
        {
            return DiscussionBo.GetTopDiscussionTopicWithTopDiscussion(topDiscussionTopic, topDiscussion, objectId,
                                                                       userDoAction, status, applicationIds);
        }
        //public List<DiscussionInTopicEntity> GetTopDiscussionTopicAndDiscussionForNews(int topDiscussion, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds)
        //{
        //    return DiscussionBo.GetTopDiscussionTopicAndDiscussionForNews(topDiscussion,
        //                                                               userDoAction, status, applicationIds);
        //}

        public List<DiscussionTopicEntity> GetTopTopicByApplicationIdAndObjectId(int topTopic, long objectId, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds)
        {
            return DiscussionBo.GetTopTopicByApplicationIdAndObjectId(topTopic, objectId, userDoAction, status,
                                                                      applicationIds);
        }

        #endregion

        #region ActivityForNews

        public ActivityEntity[] GetTopActivityForNews(int top, NewsActionType newsActionType)
        {
            return ActivityBo.GetTopLatest(top, (int)newsActionType, (int)EnumActivityType.News).ToArray();
        }

        public ActivityEntity[] GetTopActivityForNewsByMultiAction(int top, NewsActionType[] newsActionTypes)
        {
            var newsActionTypesInString = newsActionTypes.Aggregate("", (current, newsActionType) => current + (";" + (int)newsActionType));
            if (!string.IsNullOrEmpty(newsActionTypesInString))
                newsActionTypesInString = newsActionTypesInString.Substring(1);
            return ActivityBo.GetTopLatest(top, newsActionTypesInString, (int)EnumActivityType.News).ToArray();
        }

        public ActivityEntity[] SearchActivityForNews(int pageIndex, int pageSize, long applicationId, NewsActionType newsActionTypes, string sourceId, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            return ActivityBo.Search("", pageIndex, pageSize, applicationId, (int)newsActionTypes, (int)EnumActivityType.News, sourceId, dateFrom, dateTo, ref totalRows).ToArray();
        }
        public List<ActivityEntity> SearchActivity(string keyword, int pageIndex, int pageSize, long applicationId, NewsActionType newsActionTypes, EnumActivityType type, string sourceId, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            return ActivityBo.Search(keyword, pageIndex, pageSize, applicationId, (int)newsActionTypes, (int)type, sourceId, dateFrom, dateTo, ref totalRows);
        }
        #endregion

        #region CmsDiscussion

        #region For NewsCrawler

        public List<CmsDiscussionEntity> GetTopDiscussionForNewsCrawlerByUrl(string url, int topParentDiscussion, int topChildDiscussion, ref int totalParentDiscussion)
        {
            return CmsDiscussionForNewsCrawlerBo.GetTopDiscussionByUrl(url, topParentDiscussion, topChildDiscussion,
                                                                       ref totalParentDiscussion);
        }

        public WcfActionResponse SendDiscussionForNewsCrawler(string url, long parentDiscussionId, string discussionContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var newDiscussionId = 0L;
            if (CmsDiscussionForNewsCrawlerBo.SendDiscussion(url, parentDiscussionId, discussionContent, ref newDiscussionId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data =
                    NewtonJson.Serialize(CmsDiscussionBo.GetDiscussionByDiscussionId(newDiscussionId));
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        #endregion

        #region For NewsComment

        public List<CmsDiscussionEntity> GetTopDiscussionForNewsComment(long newsId, int topParentDiscussion, int topChildDiscussion, ref int totalParentDiscussion)
        {
            return CmsDiscussionForNewsCommentBo.GetTopDiscussionByNewsId(newsId, topParentDiscussion, topChildDiscussion,
                                                                       ref totalParentDiscussion);
        }

        public WcfActionResponse SendDiscussionForNewsComment(long newsId, long parentDiscussionId, string discussionContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var newDiscussionId = 0L;
            if (CmsDiscussionForNewsCommentBo.SendDiscussion(newsId, parentDiscussionId, discussionContent, ref newDiscussionId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data =
                    NewtonJson.Serialize(CmsDiscussionBo.GetDiscussionByDiscussionId(newDiscussionId));
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        #endregion

        #region For NewsComment

        public List<CmsDiscussionEntity> GetTopDiscussionForJobComment(int jobId, int topParentDiscussion, int topChildDiscussion, ref int totalParentDiscussion)
        {
            return CmsDiscussionForJobManagerBo.GetTopDiscussionByJobId(jobId, topParentDiscussion, topChildDiscussion,
                                                                       ref totalParentDiscussion);
        }

        public WcfActionResponse SendDiscussionForJobComment(int jobId, long parentDiscussionId, string discussionContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var newDiscussionId = 0L;
            if (CmsDiscussionForJobManagerBo.SendDiscussion(jobId, parentDiscussionId, discussionContent, ref newDiscussionId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data =
                    NewtonJson.Serialize(CmsDiscussionBo.GetDiscussionByDiscussionId(newDiscussionId));
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        #endregion

        #region For CalendarComment
        public List<CmsDiscussionEntity> GetTopDiscussionForCalendarComment(int calendarId, int topParentDiscussion, int topChildDiscussion, ref int totalParentDiscussion)
        {
            return CmsDiscussionForCalendarManagerBo.GetTopDiscussionByCalendarId(calendarId, topParentDiscussion, topChildDiscussion,
                                                                       ref totalParentDiscussion);
        }

        public WcfActionResponse SendDiscussionForCalendarComment(int calendarId, long parentDiscussionId, string discussionContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var newDiscussionId = 0L;
            if (CmsDiscussionForCalendarManagerBo.SendDiscussion(calendarId, parentDiscussionId, discussionContent, ref newDiscussionId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data =
                    NewtonJson.Serialize(CmsDiscussionBo.GetDiscussionByDiscussionId(newDiscussionId));
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #endregion

        #region NewsSocialEmbed      

        public WcfActionResponse InsertNewsSocialEmbed(NewsSocialEmbedEntity newsSocial, ref long id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = NewsSocialEmbedBo.InsertNewsSocialEmbed(newsSocial, ref id);
                if (errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    newsSocial.Id = id;
                    responseData = WcfActionResponse.CreateSuccessResponse(newsSocial.Id.ToString(), "Success.");

                    try
                    {
                        var strParamValue = NewtonJson.Serialize(newsSocial, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + newsSocial.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertnewssocialembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }

                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, " InsertNewsSocialEmbed => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateNewsSocialEmbed(NewsSocialEmbedEntity newsSocial)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = NewsSocialEmbedBo.UpdateNewsSocialEmbed(newsSocial);
                if (errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(newsSocial.Id.ToString(), "Success.");

                    try
                    {
                        var strParamValue = NewtonJson.Serialize(newsSocial, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + newsSocial.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatenewssocialembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, " UpdateNewsSocialEmbed => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteNewsSocialEmbedById(long id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = NewsSocialEmbedBo.DeleteNewsSocialEmbedById(id);
                if (errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();

                    try
                    {
                        var jsonKey = "{\"Id\":" + id + "}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "deletenewssocialembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, "DeleteNewsSocialEmbedById => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse OnToggleStatusNewsSocialEmbed(long id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = NewsSocialEmbedBo.OnToggleStatus(id);
                if (errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();

                    try
                    {
                        var jsonKey = "{\"Id\":" + id + "}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "togglestatusnewssocialembed", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, "DeleteNewsSocialContentById => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public List<NewsSocialEmbedEntity> SearchNewsSocialEmbed(int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsSocialEmbedBo.SearchNewsSocialEmbed(status, pageIndex, pageSize, ref totalRow);
        }

        public NewsSocialEmbedEntity GetNewsSocialEmbedById(long id)
        {
            return NewsSocialEmbedBo.GetNewsSocialEmbedById(id);
        }        

        public bool CheckUrlNewsEmbeb(string rawId)
        {
            var obj= NewsSocialEmbedBo.GetNewsSocialEmbedByRawId(rawId);
            if (obj != null && obj.Id > 0)
                return true;
            return false;
        }

        #endregion

        #region NewsSocialContent      

        public WcfActionResponse InsertNewsSocialContent(NewsSocialContentEntity newsSocial, ref long id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = NewsSocialContentBo.InsertNewsSocialContent(newsSocial, ref id);
                if (errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    newsSocial.Id = id;
                    responseData = WcfActionResponse.CreateSuccessResponse(newsSocial.Id.ToString(), "Success.");

                    try
                    {
                        var strParamValue = NewtonJson.Serialize(newsSocial, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + newsSocial.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "insertnewssocialcontent", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }

                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, " InsertNewsSocialContent => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateNewsSocialContent(NewsSocialContentEntity newsSocial)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = NewsSocialContentBo.UpdateNewsSocialContent(newsSocial);
                if (errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(newsSocial.Id.ToString(), "Success.");

                    try
                    {
                        var strParamValue = NewtonJson.Serialize(newsSocial, "yyyy-MM-dd'T'HH:mm:ss");
                        var jsonKey = "{\"Id\":" + newsSocial.Id + "}";
                        ContentDeliveryServices.PushToDataCD(strParamValue, "updatenewssocialcontent", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, " UpdateNewsSocialContent => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteNewsSocialContentById(long id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = NewsSocialContentBo.DeleteNewsSocialContentById(id);
                if (errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();

                    try
                    {
                        var jsonKey = "{\"Id\":" + id + "}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "deletenewssocialcontent", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, "DeleteNewsSocialContentById => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse OnToggleStatusNewsSocialContent(long id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = NewsSocialContentBo.OnToggleStatus(id);
                if (errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();

                    try
                    {
                        var jsonKey = "{\"Id\":" + id + "}";
                        ContentDeliveryServices.PushToDataCD(jsonKey, "togglestatusnewssocialcontent", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                    }
                    catch { }
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, "DeleteNewsSocialContentById => " + ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public List<NewsSocialContentEntity> SearchNewsSocialContent(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsSocialContentBo.SearchNewsSocialContent(keyword, status, pageIndex, pageSize, ref totalRow);
        }

        public NewsSocialContentEntity GetNewsSocialContentById(long id)
        {
            return NewsSocialContentBo.GetNewsSocialContentById(id);
        }

        public bool CheckUrlNewsContent(string rawId)
        {
            var obj = NewsSocialContentBo.GetNewsSocialEmbedByRawId(rawId);
            if (obj != null && obj.Id > 0)
                return true;
            return false;
        }

        #endregion

        #region BoxNewsSocialEmbed       

        public List<BoxNewsSocialEmbedEntity> GetListBoxNewsSocialEmbed(int type, int zoneId)
        {
            return BoxNewsSocialEmbedBo.GetListBoxNewsSocialEmbed(type, zoneId);
        }

        public WcfActionResponse UpdateListBoxNewsSocialEmbed(string listNewsId, int type, int zoneId)
        {
            var errorCode = BoxNewsSocialEmbedBo.Update(listNewsId, type, zoneId);
            var result = errorCode == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, Entity.ErrorCode.ErrorMapping.Current[errorCode]);
            if (result.Success)
            {
                try
                {
                    var currentNews = new BoxNewsSocialEmbedCdData();
                    var jsonKey = "{\"ZoneId\":" + zoneId + ",\"Type\":" + type + "}";
                    currentNews.ListNewsId = listNewsId.Split(',').ToArray();
                    currentNews.TypeNewsEmbed = type;
                    currentNews.ZoneId = zoneId;
                    var jsonValue = NewtonJson.Serialize(currentNews);
                    ContentDeliveryServices.PushToDataCD(jsonValue, "updateboxnewssocialembed", string.Empty, jsonKey);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            return result;
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}