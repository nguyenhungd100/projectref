﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.MenuExtension.Bo;
using ChannelVN.MenuExtension.Entity;
using ChannelVN.MenuExtension.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class MenuExtensionServices
    {
        public List<MenuExtensionEntity> MenuExtension_Search(int ZoneId, MenuExtensionStatus status, int GroupId, int ParentMenuId, bool IsTag)
        {
            return MenuExtensionBo.Search(ZoneId, (int)status, GroupId, ParentMenuId, IsTag);
        }
        public List<MenuExtensionEntity> GetAllWithTreeView(bool getParentZoneOnly)
        {
            return MenuExtensionBo.GetAllMenuExtensionWithTreeView(getParentZoneOnly);
        }
        public MenuExtensionEntity MenuExtension_GetById(int Id)
        {
            return MenuExtensionBo.GetMenuExtensionById(Id);
        }
        public WcfActionResponse MenuExtension_Update(MenuExtensionEntity entity, string listTag)
        {
            var responseData = MenuExtensionBo.UpdateMenuExtension(entity, listTag) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + entity.Id + "}";
                var strPram = NewtonJson.Serialize(new { entity, listTag }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatemenuextension", string.Empty, jsonKey);
            }

            return responseData;
        }

        public WcfActionResponse MenuExtension_Insert(MenuExtensionEntity entity, string listTag, ref int menuExtensionId)
        {
            int MenuExtensionId = 0;
            var responseData = MenuExtensionBo.InsertMenuExtension(entity, listTag, ref menuExtensionId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(MenuExtensionId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            if (responseData.Success)
            {
                entity.Id = MenuExtensionId;
                var jsonKey = "{\"Id\":" + entity.Id + "}";
                var strPram = NewtonJson.Serialize(new { entity , listTag}, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "insertmenuextension", string.Empty, jsonKey);
            }

            return responseData;
        }
        public WcfActionResponse MenuExtension_InsertTag(int menuId, int GroupId, int ZoneId, string tags)
        {
            int MenuExtensionId = 0;
            var responseData = MenuExtensionBo.InsertTag(menuId, GroupId, ZoneId, tags) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(MenuExtensionId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + menuId + "}";
                var strPram = NewtonJson.Serialize(new { menuId, GroupId, ZoneId, tags }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "inserttagmenuextension", string.Empty, jsonKey);
            }

            return responseData;
        }
        public WcfActionResponse MenuExtension_UpdateStatus(int Id, MenuExtensionStatus status)
        {

            var responseData = MenuExtensionBo.MenuExtension_UpdateStatus(Id, (int)status) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            if (responseData.Success)
            {                
                var jsonKey = "{\"Id\":" + Id + "}";
                var strPram = NewtonJson.Serialize(new { status }, "yyyy-MM-dd'T'HH:mm:ss");
                ContentDeliveryServices.PushToDataCD(strPram, "updatestatusmenuextension", string.Empty, jsonKey);
            }
            return responseData;
        }

        public WcfActionResponse MenuExtension_Delete(int Id)
        {
            var responseData = MenuExtensionBo.DeleteMenuExtension(Id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            if (responseData.Success)
            {
                var jsonKey = "{\"Id\":" + Id + "}";
                
                ContentDeliveryServices.PushToDataCD(jsonKey, "deletemenuextension", string.Empty, jsonKey);
            }
            return responseData;
        }
    }
}