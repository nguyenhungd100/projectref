﻿using System;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.MagazineTemplate.Bo;
using ChannelVN.MagazineTemplate.Entity;
using ChannelVN.MagazineTemplate.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Services.Common;
using ChannelVN.CMS.WebApi.Services.ExternalServices;

namespace ChannelVN.CMS.WebApi.Services.Extension
{
    public class MagazineTemplateServices
    {

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }


        #region MagazineTemplate

        public MagazineTemplateEntity GetMagazineTemplateById(int Id)
        {
            return MagazineTemplateBo.GetMagazineTemplateById(Id); ;
        }

        public WcfActionResponse InsertMagazineTemplate(MagazineTemplateEntity entity, ref int newId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineTemplateBo.InsertMagazineTemplate(entity, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;

                try
                {                    
                    entity.Id = newId;                    
                    var strParamValue = NewtonJson.Serialize(entity, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + newId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "insertmagazinetemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateMagazineTemplate(MagazineTemplateEntity entity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineTemplateBo.UpdateMagazineTemplate(entity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;

                try
                {                    
                    var strParamValue = NewtonJson.Serialize(entity, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + entity.Id + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "updatemagazinetemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse DeleteMagazineTemplate(int magazineId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineTemplateBo.DeleteMagazineTemplate(magazineId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;

                try
                {                    
                    var strParamValue = NewtonJson.Serialize(magazineId, "yyyy-MM-dd'T'HH:mm:ss");
                    var jsonKey = "{\"Id\":" + magazineId + "}";
                    ContentDeliveryServices.PushToDataCD(strParamValue, "deletemagazinetemplate", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), jsonKey);
                }
                catch { }
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public MagazineTemplateEntity[] MagazineTemplateSearch(string keyword, int type, EnumMagazineTemplateStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MagazineTemplateBo.MagazineTemplateSearch(keyword, type, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion        
    }
}