﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class TopicNewsCdData
    {
        [DataMember]
        public long TopicId { get; set; }
        [DataMember]
        public string DeleteNewsId { get; set; }
        [DataMember]
        public string AddnewsId { get; set; }
    }
}