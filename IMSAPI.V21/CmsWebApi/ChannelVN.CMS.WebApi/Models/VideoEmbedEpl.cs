﻿
namespace ChannelVN.CMS.WebApi.Models
{
    public class VideoEmbedEpl
    {
        public long VideoId { get; set; }
        public string Embed { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int ZoneId { get; set; }
        public int SortOrder { get; set; }
    }
}