﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class EmbedBoxOnPageCdData
    {
        [DataMember]
        public string[] ListNewsId { get; set; }
        [DataMember]
        public int TypeNewsEmbed { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string[] ListDate { get; set; }
        [DataMember]
        public string[] ListExpireHour { get; set; }
    }
}