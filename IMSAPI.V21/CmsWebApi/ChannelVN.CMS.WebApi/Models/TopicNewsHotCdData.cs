﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class TopicNewsHotCdData
    {
        [DataMember]
        public long TopicId { get; set; }
        [DataMember]
        public string AddNewsAvatar { get; set; }
        [DataMember]
        public string AddNewsId { get; set; }
    }
}