﻿using ChannelVN.CMS.Entity.Base.Vote;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class VoteFullInfoCdData
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public VoteEntity VoteInfo { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public List<VoteAnswersEntity> VoteAnswers { set; get; }
    }
}