﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class EmbedBoxCdData
    {
        [DataMember]
        public string[] ListNewsId { get; set; }
        [DataMember]
        public int Type { get; set; }
    }
}