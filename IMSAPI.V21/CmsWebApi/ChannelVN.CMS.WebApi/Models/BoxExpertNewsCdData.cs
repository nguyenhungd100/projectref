﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class BoxExpertNewsCdData
    {
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ListNewsId { get; set; }
    }
}