﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class TagEmbedCdData
    {
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ListTagId { get; set; }
        [DataMember]
        public string ListTitle { get; set; }
        [DataMember]
        public string ListUrl { get; set; }
    }
}