﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Models.Base
{
    public class TagSuggestEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int NewsCount { get; set; }
        public string Url { get; set; }
        public int ZoneId { get; set; }
        public string ZoneName { get; set; }
        public string ZoneUrl { get; set; }
    }

    public class TagToken
    {        
        public string Name { get; set; }
        public int Rank { get; set; }
        public bool IsApproved { get; set; }        
    }

    public class Concept
    {
        public TagToken[] TagTokens { get; set; }
        public string Message { get; set; }
        public object[] ActiveTopics { get; set; }
        public object[] Categories { get; set; }
        public object[] ConceptTopics { get; set; }
    }
}