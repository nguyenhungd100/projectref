﻿using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.LiveMatch.Entity;
using ChannelVN.SEO.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Models.Base
{
    public class SearchNewsResponse
    {
        public int TotalRow { get; set; }
        public List<NewsSearchEntity> News { get; set; }
    }

    public class SearchNewsPublishResponse
    {
        public int TotalRow { get; set; }
        public List<NewsPublishForObjectBoxEntity> News { get; set; }
    }

    public class SearchTagResponse
    {
        public int TotalRow { get; set; }
        public List<TagEntity> Tags { get; set; }
    }

    public class SearchThreadResponse
    {
        public int TotalRow { get; set; }
        public List<ThreadEntity> Threads { get; set; }
    }

    public class SearchLivematchResponse
    {
        public int TotalRow { get; set; }
        public List<NodeJs_LiveMatchEntity> Livematch { get; set; }
    }

    public class MagazineTreeFolder
    {
        public string Id { get; set; }
        public string Parent { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
    }

    public class MagazineJsonResponse
    {
        public List<string> files { get; set; }
        public List<string> directories { get; set; }
        public string last_modified { get; set; }
        public string upload_by { get; set; }
    }

    public class SearchNewsMagazineEntity
    {
        public int TotalRow { get; set; }
        public List<NewsMagazineEntity> News { get; set; }
    }

    public class NewsMagazineEntity
    {
        public long Id { get; set; }
        
        public string EncryptId { get; set; }
       
        public string Title { get; set; }

        public string Avatar { get; set; }

        public List<NewsExtensionEntity> NewsExtensions { get; set; }
    }

    public class NewsDetailEntity
    {
        public NewsEntity News { get; set; }
        public SEOMetaNewsEntity Seo { get; set; }
    }

    public class SearchNewsForSeoResponse
    {
        public int TotalRow { get; set; }
        public List<ExpertNewsInListEntity> News { get; set; }
    }

    public class ExpertNewsInListEntity
    {        
        public long Id { get; set; }
        
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        
        public string Title { get; set; }
        
        public string Sapo { get; set; }
        
        public string Avatar { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public string CreatedBy { get; set; }
        
        public string EditedBy { get; set; }
        
        public DateTime PublishedDate { get; set; }
        
        public string PublishedBy { get; set; }
        
        public DateTime DistributionDate { get; set; }
        
        public string Note { get; set; }
        
        public int ViewCount { get; set; }
        
        public string Url { get; set; }
        
        public long ActiveUsers { get; set; }
        
        public string ZoneName { get; set; }
        
        public int ZoneId { get; set; }
        
        public int MobileCount { get; set; }
        
        public string TagItem { get; set; }
        
        public bool IsOnHome { get; set; }
    }
}