﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChannelVN.CMS.WebApi.Models.Base
{
    public class VideoApiEntity
    {
        public int videoID { get; set; }
        public string mathCode { get; set; }
        public string hashTags { get; set; }
        public string title { get; set; }
        public string file { get; set; }
        public string duration { get; set; }
        public string img { get; set; }
        public string uploadtime { get; set; }
        public int type { get; set; }
        [JsonProperty("namespace")]
        public string nameSpace { get; set; }
        public int isClone { get; set; }
    }
}