﻿using ChannelVN.CMS.BoSearch.Entity;
using System.Collections.Generic;

namespace ChannelVN.CMS.WebApi.Models.Base
{
    public class SearchVideoResponse
    {
        public int TotalRow { get; set; }
        public List<VideoSearchEntity> Videos { get; set; }
    }

    public class SearchVideoVTVResponse
    {
        public int TotalRow { get; set; }
        public List<Video> Videos { get; set; }
    }
}