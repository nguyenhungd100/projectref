﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.WebApi.Models
{
    public class NewsPositionAction
    {
        #region Data contracts
        [DataContract]
        public enum NewsPositionEditGroup
        {
            [EnumMember]
            ForHomePage = 1,
            [EnumMember]
            ForListPage = 2
        }
        [DataContract]
        public enum NewsPositionType
        {
            [EnumMember]
            HomeNewsFocus = 1,
            [EnumMember]
            HomeLastestNews = 2,
            [EnumMember]
            ListNewsFocusByZone = 3,
            [EnumMember]
            LastestNewsByZone = 4,
            [EnumMember]
            HomeMobileFocus = 5,
            [EnumMember]
            HomeNewsFocus_MienNam = 6

        }
        [DataContract]
        public enum NewsPositionTypeForSohaNews
        {
            [EnumMember]
            HomeNewsFocus = 1,
            [EnumMember]
            HomeLastestNews = 2,
            [EnumMember]
            ListNewsFocusByZone = 3,
            [EnumMember]
            LastestNewsByZone = 4,
            [EnumMember]
            HomeTimelineLocked = 5,
            [EnumMember]
            HomeMobileFocus = 6
        }
        [DataContract]
        public class NewsPositionForHomePageEntity : EntityBase
        {
            [DataMember]
            public List<NewsPositionEntity> HighlightHomeFocus;
        }
        [DataContract]
        public class NewsPositionForListPageEntity : EntityBase
        {
            [DataMember]
            public List<NewsPositionEntity> HighlightListFocusByZone;
        }
        #endregion
    }
}