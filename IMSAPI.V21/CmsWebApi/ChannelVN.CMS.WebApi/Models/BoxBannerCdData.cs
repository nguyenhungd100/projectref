﻿using ChannelVN.CMS.Entity.Base.News;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class BoxBannerCdData
    {
        [DataMember]
        public BoxBannerEntity BoxBanner { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
    }
}