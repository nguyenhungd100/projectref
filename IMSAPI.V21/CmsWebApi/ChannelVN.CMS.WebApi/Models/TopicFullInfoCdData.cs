﻿using ChannelVN.CMS.Entity.Base.Topic;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.WebApi.Models
{
    [DataContract]
    public class TopicFullInfoCdData
    {
        [DataMember]
        public TopicEntity Topic { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
    }
}