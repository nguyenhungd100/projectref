﻿using ChannelVN.CMS.WebApi.Caching;
using Microsoft.Owin;
using Owin;
using System.Web.Routing;

[assembly: OwinStartup(typeof(ChannelVN.CMS.WebApi.Startup))]
namespace ChannelVN.CMS.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigurationOAuth(app);
            //create queuedb
            //StreamManager.LoadSetup();

            //job auto recover
            //WebBackgrounderSetup.Start();
            //QueueJobHost.Start();
        }
    }
}