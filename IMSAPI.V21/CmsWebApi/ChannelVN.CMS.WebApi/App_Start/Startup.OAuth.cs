﻿using ChannelVN.CMS.WebApi.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.IdentityModel.Tokens;
using Thinktecture.IdentityModel.Tokens;

namespace ChannelVN.CMS.WebApi
{
    public partial class Startup
    {
        public void ConfigurationOAuth(IAppBuilder app)
        {
            // We can make use of OWIN to manage instances of objects for us, on a per request basis.
            // The pattern is comparabletoIoC, inthat youtell the “container” how to create an instance of
            // a specific type of object, then request the instance using a ​Get<T>​ method.

            // To enable bearer authentication.
            app.UseJwtBearerAuthentication(OAuthAuthenticationOptions);

            // To expose an OAuth endpoint so that the client can request a token(by passinga
            // user name and password).
            app.UseOAuthBearerTokens(OAuthAuthorizationOptions);
        }

        static Startup()
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new HmacSigningCredentials(secret).SigningKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = issuer,

                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = audience,

                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };

            OAuthAuthenticationOptions = new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { audience },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                    new SymmetricKeyIssuerSecurityTokenProvider(issuer, secret)
                },
                TokenValidationParameters = tokenValidationParameters,
                Provider = new CustomOAuthBearerAuthenticationProvider("access_token")
            };

            OAuthAuthorizationOptions = new OAuthAuthorizationServerOptions
            {
                //#if DEBUG
                AllowInsecureHttp = true,
                //#endif
                TokenEndpointPath = new PathString("/oauth2/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromSeconds(tokenExpireTime),
                Provider = new CustomOAuthAuthorizationServerProvider(),
                AccessTokenFormat = new CustomJwtFormat(issuer, secret, tokenValidationParameters)                
            };            
        }

        public static JwtBearerAuthenticationOptions OAuthAuthenticationOptions
        {
            get;
            private set;
        }

        public static OAuthAuthorizationServerOptions OAuthAuthorizationOptions
        {
            get;
            private set;
        }

        private static readonly string audience = "Any";

        private static readonly string issuer = Common.Crypton.Encrypt("Micro.Service");/*WcfExtensions.AppConfigs.CmsApiNamespace*/

        private static readonly byte[] secret = TextEncodings.Base64Url.Decode(SHA.GenerateSHA256String(WcfExtensions.AppConfigs.CmsApiSecretKey));

        private static readonly long tokenExpireTime = WcfExtensions.AppConfigs.CmsApiTokenExpireTime;

        /// <summary>
        /// SHA Helpers
        /// </summary>
        public static class SHA
        {
            public static string GenerateSHA256String(string inputString)
            {
                var sha256 = System.Security.Cryptography.SHA256Managed.Create();
                var bytes = System.Text.Encoding.UTF8.GetBytes(inputString);
                var hash = sha256.ComputeHash(bytes);
                return GetStringFromHash(hash);
            }

            public static string GenerateSHA512String(string inputString)
            {
                var sha512 = System.Security.Cryptography.SHA512Managed.Create();
                var bytes = System.Text.Encoding.UTF8.GetBytes(inputString);
                var hash = sha512.ComputeHash(bytes);
                return GetStringFromHash(hash);
            }

            private static string GetStringFromHash(byte[] hash)
            {
                var result = new System.Text.StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    result.Append(hash[i].ToString("X2"));
                }
                return result.ToString();
            }
        }
    }
}