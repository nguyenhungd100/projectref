﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.MessageQueue;
using ChannelVN.CMS.MessageQueue.Constants;
using ChannelVN.CMS.MessageQueue.ExchangeQueue;
using ChannelVN.CMS.WebApi.Services.Base;
using Newtonsoft.Json;
using System;

namespace ChannelVN.CMS.WebApi.Caching
{
    public static class StreamManager
    {
        public static void LoadSetup()
        {
            #region redis
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.REDIS, Recovery = true, Retry = 3 }, (msg, ack) =>
              {
                  try
                  {
                    //TODO somethings
                    var queue = JsonConvert.DeserializeObject<QueueEntity<object>>(msg.Payload.ToString());
                      var res = false;
                      if (queue != null && queue.Data != null)
                      {
                          switch (queue.Action)
                          {
                              case ActionName.ADD_NEWS:
                                  var obj = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj.Id);
                                      if (newsInfo != null)
                                      {
                                          if (string.IsNullOrEmpty(newsInfo.PublishedBy) && newsInfo.Status == (int)NewsStatus.Published)
                                              newsInfo.PublishedBy = newsInfo.LastModifiedBy;
                                          res = BoCached.Base.News.NewsDalFactory.AddNews(newsInfo);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.AddNews(obj);
                                  }
                                  if (res || obj == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_1:
                                  var obj1 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj1 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj1.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToRecieveFromReturnStore(newsInfo.Id, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToRecieveFromReturnStore(obj1.Id, obj1.Status);
                                  }
                                  if (res || obj1 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_2:
                                  var obj2 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj2 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj2.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(obj2.Id, obj2.LastModifiedBy, obj2.Status);
                                  }
                                  if (res || obj2 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_3:
                                  var obj3 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj3 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj3.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReceivedForEdit(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.EditedBy, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReceivedForEdit(obj3.Id, obj3.LastModifiedBy, obj3.EditedBy, obj3.Status);
                                  }
                                  if (res || obj3 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_4:
                                  var obj4 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj4 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj4.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(newsInfo.Id, newsInfo.ReturnedBy, newsInfo.Note, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(obj4.Id, obj4.ReturnedBy, obj4.Note, obj4.Status);
                                  }
                                  if (res || obj4 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_5:
                                  var obj5 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj5 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj5.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(obj5.Id, obj5.LastModifiedBy, obj5.Status);
                                  }
                                  if (res || obj5 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_6:
                                  var obj6 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj6 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj6.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(newsInfo.Id, newsInfo.EditedBy, newsInfo.ApprovedBy, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(obj6.Id, obj6.EditedBy, obj6.ApprovedBy, obj6.Status);
                                  }
                                  if (res || obj6 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_7:
                              case ActionName.UPDATE_NEWS_STATUS_20:
                                  var obj7 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj7 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj7.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(newsInfo.Id, newsInfo.EditedBy, newsInfo.ReturnedBy, newsInfo.Note, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(obj7.Id, obj7.EditedBy, obj7.ReturnedBy, obj7.Note, obj7.Status);
                                  }
                                  if (res || obj7 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_8:
                                  var obj8 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj8 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj8.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToPublished(newsInfo.Id, 0, 0, 0, 0, newsInfo.PublishedBy, Utility.SetPublishedDate(newsInfo.DistributionDate), newsInfo.Body);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToPublished(obj8.Id, 0, 0, 0, 0, obj8.PublishedBy, Utility.SetPublishedDate(obj8.DistributionDate), obj8.Body);
                                  }
                                  if (res || obj8 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_9:
                                  var obj9 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj9 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj9.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToMovedToTrash(newsInfo.Id, newsInfo.LastModifiedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToMovedToTrash(obj9.Id, obj9.LastModifiedBy);
                                  }
                                  if (res || obj9 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_10:
                                  var obj10 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj10 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj10.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToUnPublished(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToUnPublished(obj10.Id, obj10.LastModifiedBy, obj10.Status);
                                  }
                                  if (res || obj10 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_11:
                                  var obj11 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj11 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj11.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnToCooperator(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.Note, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnToCooperator(obj11.Id, obj11.LastModifiedBy, obj11.Note, obj11.Status);
                                  }
                                  if (res || obj11 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_14:
                                  var obj14 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj14 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj14.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEditorialBoard(newsInfo.Id, newsInfo.ApprovedBy, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEditorialBoard(obj14.Id, obj14.ApprovedBy, obj14.Status);
                                  }
                                  if (res || obj14 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_16:
                                  var obj16 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj16 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj16.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReceivedForEditorialBoard(newsInfo.Id, newsInfo.ApprovedBy, newsInfo.PublishedBy, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReceivedForEditorialBoard(obj16.Id, obj16.ApprovedBy, obj16.PublishedBy, obj16.Status);
                                  }
                                  if (res || obj16 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_STATUS_19:
                                  var obj19 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (obj19 != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj19.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(newsInfo.Id, newsInfo.ApprovedBy, newsInfo.ReturnedBy, newsInfo.Note, newsInfo.Status);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(obj19.Id, obj19.ApprovedBy, obj19.ReturnedBy, obj19.Note, obj19.Status);
                                  }
                                  if (res || obj19 == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_FORWARD_TO_EDITOR:
                                  var objForward = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (objForward != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objForward.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusForwardToOtherEditor(newsInfo.Id, newsInfo.EditedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusForwardToOtherEditor(objForward.Id, objForward.EditedBy);
                                  }
                                  if (res || objForward == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_FORWARD_TO_SECRETARY:
                                  var objSecretry = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (objSecretry != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objSecretry.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusForwardToOtherSecretary(newsInfo.Id, newsInfo.ApprovedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.ChangeStatusForwardToOtherSecretary(objSecretry.Id, objSecretry.ApprovedBy);
                                  }
                                  if (res || objSecretry == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_ISONHOME:
                                  var objIsOnHome = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (objIsOnHome != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objIsOnHome.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.UpdateNewsIsOnHome(newsInfo.Id, newsInfo.IsOnHome, newsInfo.LastModifiedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.UpdateNewsIsOnHome(objIsOnHome.Id, objIsOnHome.IsOnHome, objIsOnHome.LastModifiedBy);
                                  }
                                  if (res || objIsOnHome == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_ISFOCUS:
                                  var objIsFocus = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (objIsFocus != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objIsFocus.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.UpdateNewsIsOnHome(newsInfo.Id, newsInfo.IsFocus, newsInfo.LastModifiedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.UpdateNewsIsOnHome(objIsFocus.Id, objIsFocus.IsFocus, objIsFocus.LastModifiedBy);
                                  }
                                  if (res || objIsFocus == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_ONLYTITLE:
                                  var objOnlyTitle = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (objOnlyTitle != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objOnlyTitle.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.UpdateOnlyNewsTitle(newsInfo.Id, newsInfo.Title, newsInfo.Url, newsInfo.LastModifiedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.UpdateOnlyNewsTitle(objOnlyTitle.Id, objOnlyTitle.Title, objOnlyTitle.Url, objOnlyTitle.LastModifiedBy);
                                  }
                                  if (res || objOnlyTitle == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_DETAIL:
                                  var objDetail = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (objDetail != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objDetail.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.UpdateDetail(newsInfo.Id, newsInfo.Title, newsInfo.Sapo, newsInfo.Body, newsInfo.Url, newsInfo.LastModifiedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.UpdateDetail(objDetail.Id, objDetail.Title, objDetail.Sapo, objDetail.Body, objDetail.Url, objDetail.LastModifiedBy);
                                  }
                                  if (res || objDetail == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_DETAILSIMPLE:
                                  var objDetailSimple = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (objDetailSimple != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objDetailSimple.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.UpdateDetailSimple(newsInfo.Id, newsInfo.Title, newsInfo.Avatar, newsInfo.Avatar2, newsInfo.Avatar3, newsInfo.Avatar4, newsInfo.Avatar5, newsInfo.LastModifiedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.UpdateDetailSimple(objDetailSimple.Id, objDetailSimple.Title, objDetailSimple.Avatar, objDetailSimple.Avatar2, objDetailSimple.Avatar3, objDetailSimple.Avatar4, objDetailSimple.Avatar5, objDetailSimple.LastModifiedBy);
                                  }
                                  if (res || objDetailSimple == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_NEWS_CHECKERROR:
                                  var objCheckError = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                  if (objCheckError != null)
                                  {
                                      var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objCheckError.Id);
                                      if (newsInfo != null)
                                      {
                                          res = BoCached.Base.News.NewsDalFactory.UpdateNewsByErrorCheck(newsInfo, newsInfo.Url, objCheckError.Tag, objCheckError.LastModifiedBy);
                                      }
                                      else
                                          res = BoCached.Base.News.NewsDalFactory.UpdateNewsByErrorCheck(objCheckError, objCheckError.Url, objCheckError.Tag, objCheckError.LastModifiedBy);
                                  }
                                  if (res || objCheckError == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.ADD_ZONE:
                              case ActionName.UPDATE_ZONE:
                              case ActionName.UPDATE_ZONE_INVISIBLED:
                              case ActionName.UPDATE_ZONE_STATUS:
                              case ActionName.UPDATE_ZONE_COMMENT:
                                  var objZone = JsonConvert.DeserializeObject<ZoneEntity>(queue.Data.ToString());
                                  if (objZone != null)
                                  {
                                      var zoneInfo = new NewsServices().GetZoneDbById(objZone.Id);
                                      if (zoneInfo != null)
                                      {
                                          res = BoCached.Base.Zone.ZoneDalFactory.AddZone(zoneInfo);
                                      }
                                      else
                                          res = BoCached.Base.Zone.ZoneDalFactory.AddZone(objZone);
                                  }
                                  if (res || objZone == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.ADD_TAG:
                              case ActionName.UPDATE_TAG:
                                  var objTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                  if (objTag != null)
                                  {
                                      var tagInfo = new TagServices().GetTagDbByTagId(objTag.Id);
                                      if (tagInfo != null)
                                      {
                                          res = BoCached.Base.Tag.TagDalFactory.AddTag(tagInfo);
                                      }
                                      else
                                          res = BoCached.Base.Tag.TagDalFactory.AddTag(tagInfo);
                                  }
                                  if (res || objTag == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.DELETE_TAG:
                                  var objDeleteTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                  if (objDeleteTag != null)
                                  {
                                      res = BoCached.Base.Tag.TagDalFactory.DeleteById(objDeleteTag.Id);
                                  }
                                  if (res || objDeleteTag == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_TAG_HOT:
                                  var objHotTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                  if (objHotTag != null)
                                  {
                                      var tagInfo = new TagServices().GetTagDbByTagId(objHotTag.Id);
                                      if (tagInfo != null)
                                      {
                                          res = BoCached.Base.Tag.TagDalFactory.UpdateTagHot(tagInfo.Id, tagInfo.IsHotTag);
                                      }
                                      else
                                          res = BoCached.Base.Tag.TagDalFactory.UpdateTagHot(objHotTag.Id, objHotTag.IsHotTag);
                                  }
                                  if (res || objHotTag == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              case ActionName.UPDATE_TAG_INVISIBLED:
                                  var objInviTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                  if (objInviTag != null)
                                  {
                                      var tagInfo = new TagServices().GetTagDbByTagId(objInviTag.Id);
                                      if (tagInfo != null)
                                      {
                                          res = BoCached.Base.Tag.TagDalFactory.UpdateTagInvisibled(tagInfo.Id, tagInfo.Invisibled);
                                      }
                                      else
                                          res = BoCached.Base.Tag.TagDalFactory.UpdateTagInvisibled(objInviTag.Id, objInviTag.Invisibled);
                                  }
                                  if (res || objInviTag == null)
                                  {
                                      ack(null);
                                  }
                                  else
                                  {
                                      ack(new Exception());
                                  }
                                  break;
                              default:
                                  ack(new Exception());
                                  break;
                          }
                      }
                  }
                  catch (Exception ex)
                  {
                      ack(ex);
                  }
              });
            #endregion

            #region Es
            ExchangeFactory.CreateStream(new EQConfiguration { Topic = TopicName.ES, Recovery = true, Retry = 3 }, (msg, ack) =>
            {
                try
                {
                    //TODO somethings
                    var queue = JsonConvert.DeserializeObject<QueueEntity<object>>(msg.Payload.ToString());
                    var res = false;
                    if (queue != null && queue.Data != null)
                    {
                        switch (queue.Action)
                        {
                            case ActionName.ADD_NEWS:
                                var obj = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj.Id);
                                    if (newsInfo != null)
                                    {
                                        if (string.IsNullOrEmpty(newsInfo.PublishedBy) && newsInfo.Status == (int)NewsStatus.Published)
                                            newsInfo.PublishedBy = newsInfo.LastModifiedBy;
                                        res = BoSearch.Base.News.NewsDalFactory.AddNews(newsInfo);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.AddNews(obj);
                                }
                                if (res || obj == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS:
                                var objUpdate = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (objUpdate != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objUpdate.Id);
                                    if (newsInfo != null)
                                    {
                                        if (string.IsNullOrEmpty(newsInfo.PublishedBy) && newsInfo.Status == (int)NewsStatus.Published)
                                            newsInfo.PublishedBy = newsInfo.LastModifiedBy;
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateNews(newsInfo);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateNews(objUpdate);
                                }
                                if (res || objUpdate == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_1:
                                var obj1 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj1 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj1.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecieveFromReturnStore(newsInfo.Id, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecieveFromReturnStore(obj1.Id, obj1.Status);
                                }
                                if (res || obj1 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_2:
                                var obj2 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj2 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj2.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(obj2.Id, obj2.LastModifiedBy, obj2.Status);
                                }
                                if (res || obj2 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_3:
                                var obj3 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj3 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj3.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReceivedForEdit(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.EditedBy, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReceivedForEdit(obj3.Id, obj3.LastModifiedBy, obj3.EditedBy, obj3.Status);
                                }
                                if (res || obj3 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_4:
                                var obj4 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj4 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj4.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(newsInfo.Id, newsInfo.ReturnedBy, newsInfo.Note, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(obj4.Id, obj4.ReturnedBy, obj4.Note, obj4.Status);
                                }
                                if (res || obj4 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_5:
                                var obj5 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj5 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj5.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(obj5.Id, obj5.LastModifiedBy, obj5.Status);
                                }
                                if (res || obj5 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_6:
                                var obj6 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj6 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj6.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(newsInfo.Id, newsInfo.EditedBy, newsInfo.ApprovedBy, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(obj6.Id, obj6.EditedBy, obj6.ApprovedBy, obj6.Status);
                                }
                                if (res || obj6 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_7:
                            case ActionName.UPDATE_NEWS_STATUS_20:
                                var obj7 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj7 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj7.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(newsInfo.Id, newsInfo.EditedBy, newsInfo.ReturnedBy, newsInfo.Note, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(obj7.Id, obj7.EditedBy, obj7.ReturnedBy, obj7.Note, obj7.Status);
                                }
                                if (res || obj7 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_8:
                                var obj8 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj8 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj8.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToPublished(newsInfo.Id, newsInfo.PublishedBy, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToPublished(obj8.Id, obj8.PublishedBy, obj8.Status);
                                }
                                if (res || obj8 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_9:
                                var obj9 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj9 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj9.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToMovedToTrash(newsInfo.Id, newsInfo.LastModifiedBy);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToMovedToTrash(obj9.Id, obj9.LastModifiedBy);
                                }
                                if (res || obj9 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_10:
                                var obj10 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj10 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj10.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToUnPublished(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToUnPublished(obj10.Id, obj10.LastModifiedBy, obj10.Status);
                                }
                                if (res || obj10 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_11:
                                var obj11 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj11 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj11.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnToCooperator(newsInfo.Id, newsInfo.LastModifiedBy, newsInfo.Note, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnToCooperator(obj11.Id, obj11.LastModifiedBy, obj11.Note, obj11.Status);
                                }
                                if (res || obj11 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_14:
                                var obj14 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj14 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj14.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEditorialBoard(newsInfo.Id, newsInfo.ApprovedBy, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEditorialBoard(obj14.Id, obj14.ApprovedBy, obj14.Status);
                                }
                                if (res || obj14 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_16:
                                var obj16 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj16 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj16.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReceivedForEditorialBoard(newsInfo.Id, newsInfo.ApprovedBy, newsInfo.PublishedBy, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReceivedForEditorialBoard(obj16.Id, obj16.ApprovedBy, obj16.PublishedBy, obj16.Status);
                                }
                                if (res || obj16 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_STATUS_19:
                                var obj19 = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (obj19 != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(obj19.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(newsInfo.Id, newsInfo.ApprovedBy, newsInfo.ReturnedBy, newsInfo.Note, newsInfo.Status);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(obj19.Id, obj19.ApprovedBy, obj19.ReturnedBy, obj19.Note, obj19.Status);
                                }
                                if (res || obj19 == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_FORWARD_TO_EDITOR:
                                var objForward = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (objForward != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objForward.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusForwardToOtherEditor(newsInfo.Id, newsInfo.EditedBy);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusForwardToOtherEditor(objForward.Id, objForward.EditedBy);
                                }
                                if (res || objForward == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_FORWARD_TO_SECRETARY:
                                var objSecretry = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (objSecretry != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objSecretry.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusForwardToOtherSecretary(newsInfo.Id, newsInfo.ApprovedBy);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.ChangeStatusForwardToOtherSecretary(objSecretry.Id, objSecretry.ApprovedBy);
                                }
                                if (res || objSecretry == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_ISONHOME:
                                var objIsOnHome = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (objIsOnHome != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objIsOnHome.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateNewsIsOnHome(newsInfo.Id, newsInfo.IsOnHome, newsInfo.LastModifiedBy);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateNewsIsOnHome(objIsOnHome.Id, objIsOnHome.IsOnHome, objIsOnHome.LastModifiedBy);
                                }
                                if (res || objIsOnHome == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_ISFOCUS:
                                var objIsFocus = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (objIsFocus != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objIsFocus.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateNewsIsOnHome(newsInfo.Id, newsInfo.IsFocus, newsInfo.LastModifiedBy);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateNewsIsOnHome(objIsFocus.Id, objIsFocus.IsFocus, objIsFocus.LastModifiedBy);
                                }
                                if (res || objIsFocus == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_ONLYTITLE:
                                var objOnlyTitle = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (objOnlyTitle != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objOnlyTitle.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateOnlyNewsTitle(newsInfo.Id, newsInfo.Title, newsInfo.LastModifiedBy);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateOnlyNewsTitle(objOnlyTitle.Id, objOnlyTitle.Title, objOnlyTitle.LastModifiedBy);
                                }
                                if (res || objOnlyTitle == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_NEWS_CHECKERROR:
                                var objCheckError = JsonConvert.DeserializeObject<NewsEntity>(queue.Data.ToString());
                                if (objCheckError != null)
                                {
                                    var newsInfo = new NewsServices().GetNewsByNewsIdForInit(objCheckError.Id);
                                    if (newsInfo != null)
                                    {
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateNewsByErrorCheck(newsInfo.Id, newsInfo.Title, newsInfo.Sapo, newsInfo.Avatar, newsInfo.LastModifiedBy);
                                    }
                                    else
                                        res = BoSearch.Base.News.NewsDalFactory.UpdateNewsByErrorCheck(objCheckError.Id, objCheckError.Title, objCheckError.Sapo, objCheckError.Avatar, objCheckError.LastModifiedBy);
                                }
                                if (res || objCheckError == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.ADD_TAG:
                                var objTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                if (objTag != null)
                                {
                                    var tagInfo = new TagServices().GetTagDbByTagId(objTag.Id);
                                    if (tagInfo != null)
                                    {
                                        res = BoSearch.Base.Tag.TagDalFactory.AddTag(tagInfo);
                                    }
                                    else
                                        res = BoSearch.Base.Tag.TagDalFactory.AddTag(tagInfo);
                                }
                                if (res || objTag == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_TAG:
                                var objUpdateTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                if (objUpdateTag != null)
                                {
                                    var tagInfo = new TagServices().GetTagDbByTagId(objUpdateTag.Id);
                                    if (tagInfo != null)
                                    {
                                        res = BoSearch.Base.Tag.TagDalFactory.UpdateTag(tagInfo);
                                    }
                                    else
                                        res = BoSearch.Base.Tag.TagDalFactory.UpdateTag(objUpdateTag);
                                }
                                if (res || objUpdateTag == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.DELETE_TAG:
                                var objDeleteTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                if (objDeleteTag != null)
                                {
                                    res = BoSearch.Base.Tag.TagDalFactory.DeleteById(objDeleteTag.Id);
                                }
                                if (res || objDeleteTag == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_TAG_HOT:
                                var objHotTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                if (objHotTag != null)
                                {
                                    var tagInfo = new TagServices().GetTagDbByTagId(objHotTag.Id);
                                    if (tagInfo != null)
                                    {
                                        res = BoSearch.Base.Tag.TagDalFactory.UpdateTagHot(tagInfo.Id, tagInfo.IsHotTag);
                                    }
                                    else
                                        res = BoSearch.Base.Tag.TagDalFactory.UpdateTagHot(objHotTag.Id, objHotTag.IsHotTag);
                                }
                                if (res || objHotTag == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            case ActionName.UPDATE_TAG_INVISIBLED:
                                var objInviTag = JsonConvert.DeserializeObject<TagEntity>(queue.Data.ToString());
                                if (objInviTag != null)
                                {
                                    var tagInfo = new TagServices().GetTagDbByTagId(objInviTag.Id);
                                    if (tagInfo != null)
                                    {
                                        res = BoSearch.Base.Tag.TagDalFactory.UpdateTagInvisibled(tagInfo.Id, tagInfo.Invisibled);
                                    }
                                    else
                                        res = BoSearch.Base.Tag.TagDalFactory.UpdateTagInvisibled(objInviTag.Id, objInviTag.Invisibled);
                                }
                                if (res || objInviTag == null)
                                {
                                    ack(null);
                                }
                                else
                                {
                                    ack(new Exception());
                                }
                                break;
                            default:
                                ack(new Exception());
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ack(ex);
                }
            });
            #endregion
        }
    }
}