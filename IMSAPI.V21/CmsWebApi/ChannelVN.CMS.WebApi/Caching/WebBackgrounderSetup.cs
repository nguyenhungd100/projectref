﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MessageQueue;
using ChannelVN.CMS.MessageQueue.Constants;
using System;
using System.Threading.Tasks;

namespace ChannelVN.CMS.WebApi.Caching
{
    public class WebBackgrounderSetup
    {
        private static readonly WebBackgrounder.JobHost _host = new WebBackgrounder.JobHost();
        private static readonly WebBackgrounder.JobManager _jobManager = CreateJobWorkersManager();

        public static void Start()
        {
            var allowQueueDb = AppSettings.GetBool("AllowCmsQueueDb");
            if (allowQueueDb)
            {
                _jobManager?.Start();
            }
        }

        public static void Stop()
        {
            _jobManager?.Stop();
        }

        public static void Shutdown()
        {
            _jobManager?.Dispose();
        }

        private static WebBackgrounder.JobManager CreateJobWorkersManager()
        {
            try
            {
                var allowQueueDb = AppSettings.GetBool("AllowCmsQueueDb");
                if (allowQueueDb)
                {
                    var timeInterval = AppSettings.GetInt32("TimeIntervalQueueJob");
                    if (timeInterval <= 0)
                    {
                        timeInterval = 60;//default 60s
                    }
                    var jobs = new WebBackgrounder.IJob[]
                    {
                new QueueJob(TimeSpan.FromSeconds(timeInterval), TimeSpan.FromSeconds(20))
                    };

                    var manager = new WebBackgrounder.JobManager(jobs, _host);
                    manager.Fail(ex => Logger.WriteLog(Logger.LogType.Fatal, "QueueDBJob => " + ex.Message));
                    return manager;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
    }

    public class QueueJob : WebBackgrounder.Job
    {
        public QueueJob(TimeSpan interval, TimeSpan timeout)
            : base("QueueDBJob", interval, timeout)
        {
        }

        public override Task Execute()
        {
            return new Task(() =>
            {
                Logger.WriteLog(Logger.LogType.Trace, string.Format("QueueDBJob => Name: {0}, interval: {1}, Time: {2}, timeout: {3}", Name, Interval, DateTime.Now.ToString("HH:mm:ss"), Timeout));
                JobQueueRecoveryQueueDB();
            });
        }

        public static void JobQueueRecoveryQueueDB()
        {
            try
            {
                ExchangeFactory.GetStream(TopicName.REDIS).Restore();
                ExchangeFactory.GetStream(TopicName.ES).Restore();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "JobQueueRecoveryQueueDB => " + ex.Message);
            }
        }
    }

    public class QueueJobHost
    {
        private static readonly WebBackgrounder.JobHost _host = new WebBackgrounder.JobHost();

        public static void Start()
        {
            var allowQueueDb = AppSettings.GetBool("AllowCmsQueueDb");
            if (allowQueueDb)
            {
                var timeInterval = AppSettings.GetInt32("TimeIntervalQueueJob");
                if (timeInterval <= 0)
                {
                    timeInterval = 5;
                }

                var workTask = new Task(() => _host.DoWork(new Task(() =>
                {
                    new QueueJob(TimeSpan.FromSeconds(timeInterval), TimeSpan.FromSeconds(20));
                })));

                workTask.Start();
            }
        }

        public static void Stop()
        {
            _host.Stop(false);
        }
    }
}