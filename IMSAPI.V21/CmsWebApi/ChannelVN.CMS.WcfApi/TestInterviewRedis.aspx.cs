﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChannelVN.CMS.BO.Base.Interview;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.RedisClientHelper;

namespace ChannelVN.CMS.WcfApi
{
    public partial class TestInterviewRedis : System.Web.UI.Page
    {
        public class InterviewQuestionForPublishEntity
        {
            public int Id { get; set; }
            public int Priority { get; set; }
            public int Type { get; set; }
            public string Content { get; set; }
            public string Answer { get; set; }
            public string FullName { get; set; }
            public int Sex { get; set; }
            public int Age { get; set; }
            public string Email { get; set; }
            public string Job { get; set; }
            public string Address { get; set; }
            public string Mobile { get; set; }
            public DateTime DistributedDate { get; set; }
            public int ChannelId { get; set; }
            public string ChannelName { get; set; }
            public string ChannelAvatar { get; set; }
        }

        private int DbNumber
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["db"])) return -1;
                return Utility.ConvertToInt(Request.QueryString["db"]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadData();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void LoadData()
        {
            try
            {
                var interviewId = Request.QueryString["interviewId"];
                var allDataKey = string.Format("CmsInterviewData[{0}]", interviewId);

                var redisClient = new RedisStackClient(DbNumber);
                var data = redisClient.Get<List<InterviewQuestionForPublishEntity>>(allDataKey);
                Logger.WriteLog(Logger.LogType.Trace, "db=" + DbNumber + "&key=" + allDataKey + "&data=" + NewtonJson.Serialize(data));

                grvAllEvent.DataSource = data;
                grvAllEvent.DataBind();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
        }

        private void LoadAllDataIntoString()
        {
            var interviewId = Utility.ConvertToInt(Request.QueryString["interviewId"]);
            const string interviewQuestionHtmlFormatForAllContent = "<p style=\"text-align: justify; padding: 1% 0px;\"><em><strong>{1}</strong> ({0})</em></p><p style=\"text-align: justify; padding: 1% 0px;\"><strong>{2}:</strong> {3}</p>";
            var allInterviewContent = new StringBuilder();
                var interviewQuestions = InterviewBo.GetAllPublishedQuestionByInterviewId(interviewId);
            if (interviewQuestions != null && interviewQuestions.Count > 0)
            {
                foreach (var interviewQuestion in interviewQuestions)
                {
                    var askInfo = interviewQuestion.FullName;
                    askInfo += interviewQuestion.Sex == 1 ? " - Nam" : (interviewQuestion.Sex == 0 ? " - Nữ" : "");
                    if (interviewQuestion.Age > 0)
                    {
                        askInfo += " " + interviewQuestion.Age + " tuổi";
                    }
                    if (!string.IsNullOrEmpty(interviewQuestion.Address))
                    {
                        askInfo += " - " + interviewQuestion.Address;
                    }
                    allInterviewContent.AppendFormat(interviewQuestionHtmlFormatForAllContent, askInfo,
                        interviewQuestion.Content, interviewQuestion.ChannelName,
                        interviewQuestion.Answer);
                }
            }
            ltrData.Text = allInterviewContent.ToString();
        }

        private void LoadDataAll()
        {
            try
            {
                var interviewId = Request.QueryString["interviewId"];
                var allDataKey = string.Format("CmsInterviewData[{0}]", interviewId);

                var redisClient = new RedisStackClient(DbNumber);

                var data = redisClient.Get<string>(allDataKey + "all");
                ltrData.Text = data;

                grvAllEvent.DataSource = data;
                grvAllEvent.DataBind();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                var interviewId = Utility.ConvertToInt(Request.QueryString["interviewId"]);
                var allDataKey = string.Format("CmsInterviewData[{0}]", interviewId);

                //if (interviewId > 0)
                //{
                //    InterviewBo.UpdateInterviewQuestionIntoRedis(interviewId);

                //    LoadData();
                //}
                //LoadDataAll();
                //InterviewBo.UpdateInterviewQuestionIntoRedis(interviewId);
                var redisClient = new RedisStackClient(DbNumber);
                ltrData.Text = redisClient.Get<string>(allDataKey + "all");
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                //var rollingNewsId = Request.QueryString["rollingNewsId"];
                //var allDataKey = string.Format("CmsInterviewData[{0}]", rollingNewsId);

                //RedisHelper.Remove(DbNumber, allDataKey);

                LoadAllDataIntoString();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
        }
    }
}