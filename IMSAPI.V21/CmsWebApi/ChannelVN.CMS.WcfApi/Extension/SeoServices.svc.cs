﻿using System;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.SEO.BO.SEOMetaVideo;
using ChannelVN.WcfExtensions;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.BO.SEOMetaNews;
using ChannelVN.SEO.Entity.ErrorCode;
using ChannelVN.SEO.BO.SEOMetaTags;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SeoServices : ISeoServices
    {
        #region Seo Meta News
        public WcfActionResponse SEOMetaNewsInsert(SEOMetaNewsEntity seoMetaNewsEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var voteId = 0;
            if (SEOMetaNewsBo.Insert(seoMetaNewsEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = voteId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse SEOMetaNewsUpdate(SEOMetaNewsEntity seoMetaNewsEntity)
        {
            var errorCode = SEOMetaNewsBo.Update(seoMetaNewsEntity);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }


        public SEOMetaNewsEntity GetSeoMetaNewsById(long id)
        {
            return SEOMetaNewsBo.GetSEOMetaNewsById(id);
        }
        #endregion

        #region SEO Meta Tag
        public WcfActionResponse SEOMetaTagsInsert(SEOMetaTagsEntity seoMetaTagsEntity)
        {

            var wcfResponseData = WcfActionResponse.CreateSuccessResponse();

            var voteId = 0;
            if (SEOMetaTagsBo.Insert(seoMetaTagsEntity) == ErrorMapping.ErrorCodes.Success)
            {
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                wcfResponseData.Data = voteId.ToString();
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                wcfResponseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                wcfResponseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return wcfResponseData;
        }

        public WcfActionResponse SEOMetaTagsUpdate(SEOMetaTagsEntity seoMetaTagsEntity)
        {
            var errorCode = SEOMetaTagsBo.Update(seoMetaTagsEntity);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse SEOMetaTagsDelete(int id)
        {
            var errorCode = SEOMetaTagsBo.Delete(id);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public SEOMetaTagsEntity GetSeoMetaTagsById(long id)
        {
            return SEOMetaTagsBo.GetSeoMetaTagsById(id);
        }

        public SEOMetaTagsEntity[] GetListSeoMetaTags(string keyWord, int zoneId, int pageIndex, int pageSize, int totalRow)
        {
            return SEOMetaTagsBo.GetListSeoMetaTags(keyWord, zoneId, pageIndex, pageSize, ref  totalRow).ToArray();            
        }
        #endregion

        #region Seo Meta Video
        public WcfActionResponse SEOMetaVideoInsert(SEOMetaVideoEntity seoMetaVideoEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var voteId = 0;
            if (SEOMetaVideoBo.Insert(seoMetaVideoEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = voteId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse SEOMetaVideoUpdate(SEOMetaVideoEntity seoMetaVideoEntity)
        {
            var errorCode = SEOMetaVideoBo.Update(seoMetaVideoEntity);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }


        public SEOMetaVideoEntity GetSeoMetaVideoById(int id)
        {
            return SEOMetaVideoBo.GetSEOMetaVideoById(id);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
