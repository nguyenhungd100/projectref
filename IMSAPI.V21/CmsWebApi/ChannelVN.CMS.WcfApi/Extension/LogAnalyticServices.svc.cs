﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.LogAnalytics;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.LogAnalytics;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LogAnalyticServices : ILogAnalyticServices
    {
        public WcfActionResponse UpdateLog(LogAnalyticsEntity log)
        {
            try
            {
                var errorCode = LogAnalyticsBo.Update(log);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse("", "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
    }
}
