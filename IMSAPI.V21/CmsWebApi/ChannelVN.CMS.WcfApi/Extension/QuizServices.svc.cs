﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Quiz.Bo;
using ChannelVN.Quiz.Entity;
using ChannelVN.Quiz.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class QuizServices : IQuizServices
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }


        public WcfActionResponse InsertQuiz(QuizEntity interactiveContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuizBo.InsertQuiz(interactiveContent) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateQuiz(QuizEntity interactiveContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuizBo.UpdateQuiz(interactiveContent) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateQuizConfig(int id, string interactiveData, string interactiveConfig)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuizBo.UpdateQuizConfig(id, interactiveData, interactiveConfig) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse QuizPublish(int id, string interactiveData, string interactiveConfig)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuizBo.QuizPublish(id, interactiveData, interactiveConfig) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public QuizEntity GetQuizById(int id)
        {
            return QuizBo.GetQuizById(id);
        }

        public List<QuizEntity> SearchQuiz(string keyword, EnumQuizType type, string createdBy, int status, int siteId,
            int pageIndex, int pageSize, ref int totalRow)
        {
            return QuizBo.SearchQuiz(keyword, type, createdBy, status, siteId,
                pageIndex, pageSize, ref totalRow);
        }

        #region QuizResult

        public WcfActionResponse InsertQuizResult(QuizResultEntity quiz)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuizResultBo.InsertQuizResult(quiz) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public QuizResultEntity GetQuizResultByAccount(long id, long quizId, string email, string phone, string facebook, string google, string vietId)
        {
            return QuizResultBo.GetQuizResultByAccount(id, quizId, email, phone, facebook, google, vietId);
        }

        public List<QuizResultEntity> SearchQuizResult(long quizId, string phone, string facebook, string google, string vietId, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuizResultBo.SearchQuizResult(quizId, phone, facebook, google, vietId, pageIndex, pageSize, ref totalRow);
        }

        #endregion
    }
}
