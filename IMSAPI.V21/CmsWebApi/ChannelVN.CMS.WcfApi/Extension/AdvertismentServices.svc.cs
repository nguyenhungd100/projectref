﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.Advertisment.Bo;
using ChannelVN.Advertisment.Entity;
using ChannelVN.Advertisment.Entity.ErrorCode;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AdvertismentServices : IAdvertismentServices
    {

        public AdvertismentEntity[] GetAllAdvertisment()
        {
            return AdvertismentBo.GetAllAdvertisment().ToArray();
        }

        public AdvertismentEntity[] GetAdvertismentByPosition(Advertisment.Entity.EnumAdvertismentType typeId, int zoneId, int displayStyle, EnumAdvertismentStatus status, int positionId)
        {
            return AdvertismentBo.GetAdvertismentByPosition(typeId, zoneId, displayStyle, status, positionId).ToArray();
        }

        public AdvertismentDetailEntity GetAdvertismentById(int id)
        {
            return AdvertismentBo.GetAdvertismentById(id);
        }

        public CMS.Common.WcfActionResponse UpdateAdvertisment(Advertisment.Entity.AdvertismentEntity info, string listZones)
        {
            return AdvertismentBo.UpdateAdvertisment(info, listZones) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public CMS.Common.WcfActionResponse UpdateAdvertismentStatus(int id, EnumAdvertismentStatus status)
        {
            var errorCode = AdvertismentBo.UpdateAdvertismentStatus(id, status);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteById(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = AdvertismentBo.DeleteById(id);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public Common.ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
