﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.VTV.Bo;
using ChannelVN.VTV.Entity;
using ChannelVN.VTV.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class VtvServices : IVtvServices
    {
        #region CHANNEL
        public List<VtvChannelEntity> GetListChannel(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return VtvChannelBo.GetListChannel(keyword, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertChannel(VtvChannelEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvChannelBo.InsertChannel(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateChannel(VtvChannelEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvChannelBo.UpdateChannel(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public VtvChannelEntity SelectChannel(int id)
        {
            return VtvChannelBo.SelectChannel(id);
        }
        public WcfActionResponse DeleteChannel(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvChannelBo.DeleteChannel(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region SCHEDULE
        public List<VtvScheduleEntity> GetListSchedule(string keyword, int channelId, DateTime filterDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VtvScheduleBo.GetListSchedule(keyword, channelId, filterDate, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertSchedule(VtvScheduleEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvScheduleBo.InsertSchedule(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateSchedule(VtvScheduleEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvScheduleBo.UpdateSchedule(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public VtvScheduleEntity SelectSchedule(int id)
        {
            return VtvScheduleBo.SelectSchedule(id);
        }
        public WcfActionResponse DeleteSchedule(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvScheduleBo.DeleteSchedule(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Program Schedule
        public WcfActionResponse InsertProgramSchedule(VTVProgramScheduleEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVProgramScheduleBo.InsertSchedule(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Program Schedule
        public WcfActionResponse InsertProgramScheduleDetail(List<VTVProgramScheduleDetailEntity> elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            try
            {
                foreach (var vtvProgramScheduleDetailEntity in elm)
                {
                    VTVProgramScheduleDetailBo.InsertSchedule(vtvProgramScheduleDetailEntity, ref id);
                }
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception)
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Schedule Register
        /// <summary>
        /// Lấy danh sách người dùng đăng ký nhận thông tin từ VTV (Lịch phát sóng, Tạp chí truyền hình)
        /// Dữ liệu được lấy từ DB ngoài (VTV_EXT)
        /// minhduongvan-2014/08/20
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="registeredDate">Datetime</param>
        /// <param name="type">int</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List Entity</returns>
        public List<VTVScheduleRegisterEntity> ListScheduleRegister(string keyword, DateTime registeredDate,
                                                    int type, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVScheduleRegisterBo.ListScheduleRegister(keyword, registeredDate, type, status, order, pageIndex, pageSize, ref totalRow);
        }
        /// <summary>
        /// Lấy thông tin người đăng ký nhận quà tặng
        /// minhduongvan-2014/08/20
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public VTVScheduleRegisterEntity GetScheduleRegisterById(int Id)
        {
            return VTVScheduleRegisterBo.GetScheduleRegisterById(Id);
        }
        /// <summary>
        /// Cập nhật thông tin ScheduleRegister
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public WcfActionResponse UpdateScheduleRegisterInfo(VTVScheduleRegisterEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVScheduleRegisterBo.UpdateScheduleRegisterInfo(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse GetScheduleFilePath(int programChannelId, int week, int month, int year)
        {
            return new WcfActionResponse();
        }

        //public VTVScheduleRegisterLogEntity GetScheduleRegisterLogByEmail(int scheduleRegisterId, int week, int month, int year)
        //{
        //    return new VTVScheduleRegisterLogEntity();
        //}

        //public WcfActionResponse LogSendScheduleToCustomer(VTVScheduleRegisterLogEntity log)
        //{
        //    return new WcfActionResponse();
        //}

        #endregion


        #region Schedule Register Log
        public List<VTVScheduleRegisterLogEntity> ListScheduleRegisterLog(int scheduleRegisterId, int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVScheduleRegisterLogBo.ListScheduleRegisterLog(scheduleRegisterId, pageIndex, pageSize, ref totalRow);
        }


        public WcfActionResponse ScheduleRegisterLogInsert(VTVScheduleRegisterLogEntity entity, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVScheduleRegisterLogBo.Insert(entity,ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        #endregion

        #region Gift program
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public List<VTVGiftProgramEntity> ListGiftProgram(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVGiftProgramBo.ListGiftProgram(keyword, status, pageIndex, pageSize, ref totalRow);
        }

        public List<VTVGiftProgramEntity> ListAllGiftProgram()
        {
            return VTVGiftProgramBo.ListAllGiftProgram();
        }
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public VTVGiftProgramEntity GetGiftProgramById(int Id)
        {
            return VTVGiftProgramBo.GetGiftProgramById(Id);
        }
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public WcfActionResponse UpdateGiftProgramInfo(VTVGiftProgramEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVGiftProgramBo.UpdateGiftProgramInfo(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse InsertGiftProgramInfo(VTVGiftProgramEntity info)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVGiftProgramBo.InsertGiftProgramInfo(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteGiftProgram(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVGiftProgramBo.DeleteGiftProgram(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region VTV Gift
        public List<VTVGiftEntity> ListGift(string keyword, int giftProgramId, int status, DateTime from, DateTime to,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVGiftBo.ListGift(keyword, giftProgramId, status, from, to, pageIndex, pageSize, ref totalRow);
        }
        public List<VTVGiftEntity> ListGiftAll()
        {
            return VTVGiftBo.ListGift();
        }
        public VTVGiftEntity GetGiftById(int Id)
        {
            return VTVGiftBo.GetGiftById(Id);
        }

        public WcfActionResponse DeleteGift(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVGiftBo.DeleteGift(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateGiftInfo(VTVGiftEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVGiftBo.UpdateGiftInfo(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse InsertGiftInfo(VTVGiftEntity info)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVGiftBo.InsertGiftInfo(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region VTV Gift Register
        public List<VTVGiftRegisterEntity> ListGiftRegister(string keyword, int giftId, int status, DateTime from, DateTime to, int order,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVGiftRegisterBo.ListGiftRegister(keyword, giftId, status, from, to, order, pageIndex, pageSize, ref totalRow);
        }

        public VTVGiftRegisterEntity GetGiftRegisterById(int Id)
        {
            return VTVGiftRegisterBo.GetGiftRegisterById(Id);
        }
        public WcfActionResponse UpdateGiftRegisterInfo(VTVGiftRegisterEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVGiftRegisterBo.UpdateGiftRegister(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteGiftRegister(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVGiftRegisterBo.DeleteGiftRegister(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region VIDEO THREAD
        public List<VtvVideoThreadEntity> GetListVideoThread(string keyword, int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            return VtvVideoThreadBo.GetListVideoThread(keyword, isHot, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertVideoThread(VtvVideoThreadEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.InsertVideoThread(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateVideoThread(VtvVideoThreadEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.UpdateVideoThread(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteVideoThread(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.DeleteVideoThread(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public VtvVideoThreadEntity SelectVideoThread(int id)
        {
            return VtvVideoThreadBo.SelectVideoThread(id);
        }
        public WcfActionResponse IsHotVideoThread(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.IsHotVideoThread(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region VIDEO THREAD ZONE
        public WcfActionResponse InsertVideoThreadZone(VtvVideoThreadZoneEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.InsertVideoThreadZone(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "ok";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public VtvVideoThreadZoneEntity SelectVideoThreadZone(int videoThreadId, bool isPrimary)
        {
            return VtvVideoThreadBo.SelectVideoThreadZone(videoThreadId, isPrimary);
        }
        public WcfActionResponse DeleteVideoThreadZone(int videoThreadId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.DeleteVideoThreadZone(videoThreadId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = videoThreadId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region VIDEO THREAD VIDEO
        public List<VtvVideoThreadVideoEntity> GetListVideoThreadVideo(int videoThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return VtvVideoThreadBo.GetListVideoThreadVideo(videoThreadId, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertVideoThreadVideo(VtvVideoThreadVideoEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.InsertVideoThreadVideo(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "ok";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteVideoThreadVideo(int videoThreadId, int videoId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.DeleteVideoThreadVideo(videoThreadId, videoId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = videoThreadId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public VtvVideoThreadVideoEntity ExistVideoThreadZone(int videoThreadId, int videoId)
        {
            return VtvVideoThreadBo.ExistVideoThreadZone(videoThreadId, videoId);
        }
        #endregion

        #region VIDEO THREAD TAG
        public List<VtvVideoThreadTagEntity> GetListVideoThreadTag(int videoThreadId)
        {
            return VtvVideoThreadBo.GetListVideoThreadTag(videoThreadId);
        }
        public WcfActionResponse InsertVideoThreadTag(VtvVideoThreadTagEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.InsertVideoThreadTag(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "ok";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteVideoThreadTag(int videoThreadId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VtvVideoThreadBo.DeleteVideoThreadTag(videoThreadId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = videoThreadId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        #endregion

        #region Game Show Register
        public List<VTVLiveShowRegisterEntity> ListLiveShowRegister(string keyword, int status,
                                                          int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVLiveShowRegisterBo.ListLiveShowRegister(keyword, status, pageIndex, pageSize, ref totalRow);
        }

        public WcfActionResponse UpdateLiveShowRegisterInfo(VTVLiveShowRegisterEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVLiveShowRegisterBo.UpdateLiveShowRegister(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public VTVLiveShowRegisterEntity GetLiveShowRegisterById(int Id)
        {
            return VTVLiveShowRegisterBo.GetLiveShowRegisterById(Id);
        }

        public WcfActionResponse UpdateStatusLiveShowRegister(VTVLiveShowRegisterEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVLiveShowRegisterBo.UpdateStatusLiveShowRegister(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Video Record
        public WcfActionResponse InsertVideoRecord(VTVVideoRecordEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVVideoRecordBo.Insert(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateStatusVideoRecord(VTVVideoRecordEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVVideoRecordBo.UpdateStatus(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public List<VTVVideoRecordEntity> ListVideoRecord(string keyword,int programChannel, string accountName, int status,
                                                        DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VTVVideoRecordBo.ListVideoRecord(keyword,programChannel, accountName, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public List<VTVProgramChannelEntity> ListChannel(int status)
        {
            return VTVVideoRecordBo.ListChannel(status);
        }

        public WcfActionResponse UpdateVideoRecordStatusByChannel(int channelId, int status)
        {
            EnumStatusVTVVideoRecord v = new EnumStatusVTVVideoRecord();
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (VTVVideoRecordBo.UpdateStatusByChannel(channelId, status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public VTVVideoRecordEntity GetVideoRecordById(int id)
        {
            return VTVVideoRecordBo.GetVideoRecordById(id);
        }
        #endregion

        public List<SeaGamesScheduleEntity> GetListSeaGamesSchedule(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return SeaGamesScheduleBo.GetList(keyword, pageIndex, pageSize, ref totalRow);
        }

        public SeaGamesScheduleEntity GetSeaGamesScheduleById(int id)
        {
            return SeaGamesScheduleBo.Select(id);
        }

        public WcfActionResponse UpdateSeaGamesSchedule(SeaGamesScheduleEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (SeaGamesScheduleBo.Update(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse DeleteSeaGamesSchedule(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (SeaGamesScheduleBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }


        public NewsPrRoyaltyEntity GetNewsPrRoyaltyByNewsId(long id)
        {
            return NewsPrRoyaltyBo.GetByNewsId(id);
        }

        public WcfActionResponse UpdateNewsPrRoyalty(NewsPrRoyaltyEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (NewsPrRoyaltyBo.Update(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        #region Box CountDown
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        public List<BoxCountDownChannelEntity> ListBoxCountDown(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoxCountDownChannelBo.ListBoxCountDown(keyword, status, pageIndex, pageSize, ref totalRow);
        }

        public List<BoxCountDownChannelEntity> ListAllBoxCountDown()
        {
            return BoxCountDownChannelBo.ListAllBoxCountDown();
        }
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        public BoxCountDownChannelEntity GetBoxCountDownById(int Id)
        {
            return BoxCountDownChannelBo.GetBoxCountDownById(Id);
        }
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public WcfActionResponse UpdateBoxCountDownInfo(BoxCountDownChannelEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BoxCountDownChannelBo.UpdateBoxCountDownInfo(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse InsertBoxCountDown(BoxCountDownChannelEntity info)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BoxCountDownChannelBo.InsertBoxCountDown(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteBoxCountDown(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BoxCountDownChannelBo.DeleteBoxCountDown(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
    }
}
