﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.VCCorp.Bo;
using ChannelVN.VCCorp.Entity;
using ChannelVN.VCCorp.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class VCCorpServices : IVCCorpServices
    {
        public  List<CurriculumVitaeEntity> GetList(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return CurriculumVitaeBo.GetList(keyword, type, status, pageIndex, pageSize, ref totalRow);
        }

        public WcfActionResponse UpdateStatus(CurriculumVitaeEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CurriculumVitaeBo.UpdateStatus(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public CurriculumVitaeEntity GetById(int Id)
        {
            return CurriculumVitaeBo.GetById(Id);
        }
    }
}
