﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.Magazine.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System.Data;
using System.Web;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.Bo;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DashboardServices : IDashboardServices
    {
        #region Thongke
        public StatisticsBoxTongHopEntity Statistics_Boxtonghop(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName)
        {
            return StatisticsBo.BoxTongHop((int)loaiThongKe, ZoneId, UserName);
        }
        public CountCommentEntity Statistics_BinhLuan(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName)
        {
            return StatisticsBo.Statistics_Comment((int)loaiThongKe, ZoneId, UserName);
        }
        public CountUserEntity Statistics_ThanhVien(Constants.loaiThongKe loaiThongKe, string UserName)
        {
            return StatisticsBo.Statistics_User((int)loaiThongKe, UserName);
        }
        public StatisticsProductionNewsPublishEntity Statistics_BaiXuatBan(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName)
        {
            return StatisticsBo.Statistics_NewsPublish((int)loaiThongKe, ZoneId, UserName);
        }
        public StatisticsProductionNewsSendEntity Statistics_BaiGuilen(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName)
        {
            return StatisticsBo.Statistics_NewsSend((int)loaiThongKe, ZoneId, UserName);
        }
        public StatisticsProductionVideoPublishEntity Statistics_VideoXuatBan(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName)
        {
            return StatisticsBo.Statistics_VideoPublish((int)loaiThongKe, ZoneId, UserName);
        }
        public StatisticsProductionVideoSendEntity Statistics_VideoGuiLen(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName)
        {
            return StatisticsBo.Statistics_VideoSend((int)loaiThongKe, ZoneId, UserName);
        }
        public List<ListPercent> Statistics_TiLeBaiTheoCategory(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName)
        {
            return StatisticsBo.Statistics_PercentNewsByCategory((int)loaiThongKe, ZoneId, UserName);

        }
        public List<ListPercent> Statistics_TiLeBaiTheoChuyenmuc(Constants.loaiThongKe loaiThongKe, string UserName)
        {
            return StatisticsBo.Statistics_PercentNewsByZone((int)loaiThongKe, UserName);
        }
        public List<ListPercent> Statistics_TiLeVideoTheoChuyenmuc(Constants.loaiThongKe loaiThongKe, string UserName)
        {
            return StatisticsBo.Statistics_PercentVideoByZone((int)loaiThongKe, UserName);
        }

        public StatisticsView_VisitEntity Statistics_ViewVisitByTime(Constants.loaiThongKe loaiThongKe, string UserName, int ZoneId, Constants.Db Db)
        {
            return StatisticsView_VisitBo.Statistics_ViewVisitByTime((int)loaiThongKe, UserName, ZoneId, (int)Db);
        }
        public StatisticsView_VisitEntityByZone Statistics_ViewVisitByZone(Constants.loaiThongKe loaiThongKe, string UserName, Constants.Db Db)
        {
            return StatisticsView_VisitBo.Statistics_ViewVisitByZone((int)loaiThongKe, UserName, (int)Db);
        }
        public List<TopPVEntity> Statistics_TopPV(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName, int TopSize, string keyword)
        {
            return StatisticsBo.Statistics_TopPV((int)loaiThongKe, ZoneId, UserName, TopSize, keyword);
        }

        public List<ZoneParentEntity> ListZoneParent(string UserName)
        {
            return ZoneBo.ListZoneParent(UserName);
        }
        public List<ZoneEntity> ListZoneByUserName(string UserName)
        {
            return ZoneBo.ListZoneByUserName(UserName);
        }
        public List<ZoneEntity> ListZoneById(int ZoneParentId)
        {
            return ZoneBo.ListZoneById(ZoneParentId);
        }
        public void XoaCacheStatisticsByUserName(string UserName)
        {
            StatisticsBo.XoaCacheStatistics(UserName);
        }
        public void XoaCacheStatistics()
        {
            StatisticsBo.XoaCacheStatistics();
        }
        public void XoaAllCache()
        {
            StatisticsBo.XoaAllCache();
        }
        #endregion
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
