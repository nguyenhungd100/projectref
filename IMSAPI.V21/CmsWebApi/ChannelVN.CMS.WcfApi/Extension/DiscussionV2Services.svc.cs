﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.DiscussionV2.Bo;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DiscussionV2Services : IDiscussionV2Services
    {
        #region Topic
        /// <summary>
        /// Lấy danh sách topic/tin bài thảo luận
        /// </summary>
        /// <param name="userName">string</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List data</returns>
        public List<ViewDiscussionV2Entity> GetListDiscussionTopic(string keyword, int type, bool status, string userName, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiscussionTopicV2Bo.GetListDiscussionTopic(keyword,type,status, userName, pageIndex, pageSize, ref totalRow);
        }
        /// <summary>
        /// Thêm mới topic
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        public WcfActionResponse InsertTopic(DiscussionTopicV2Entity info, List<DiscussionAttachV2Entity> listAttach, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionTopicV2Bo.Insert(info, listAttach, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa topic
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="username">string</param>
        /// <returns></returns>
        public WcfActionResponse DeleteTopic(int id, string username)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionTopicV2Bo.Delete(id,username) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse SetStatusTopic(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionTopicV2Bo.SetStatus(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Thêm user follow theo topic
        /// </summary>
        /// <param name="DiscussionTopicId">int</param>
        /// <param name="listUserFollow">danh sách user cách nhau bởi dấu [;] vd(a;b;c)</param>
        /// <returns></returns>
        public WcfActionResponse InsertFollowTopic(int DiscussionTopicId, string listUserFollow)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionTopicV2Bo.InsertFollowTopic(DiscussionTopicId, listUserFollow, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Cập nhật trạng thái follow cho user
        /// </summary>
        /// <param name="DiscussionTopicId">int</param>
        /// <param name="UserFollow">string</param>
        /// <param name="isFollow">bool</param>
        /// <returns></returns>
        public WcfActionResponse UpdateIsFollowTopic(int DiscussionTopicId, string UserFollow, bool isFollow)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionTopicV2Bo.UpdateIsFollowTopic(DiscussionTopicId, UserFollow, isFollow) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = DiscussionTopicId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public DiscussionTopicV2Entity GetTopicByNewsId(Int64 newsId) {
            return DiscussionTopicV2Bo.GetTopicByNewsId(newsId);
        }
        public DiscussionTopicV2Entity GetTopicById(int topicId)
        {
            return DiscussionTopicV2Bo.GetTopicById(topicId);
        }
        #endregion

        #region Discussion
        /// <summary>
        /// Thêm mới một discussion
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse InsertDiscussion(DiscussionV2Entity info, List<DiscussionAttachV2Entity> listAttach, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionV2Bo.Insert(info, listAttach, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa discussion
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteDiscussion(int id, string userAssigned)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionV2Bo.Delete(id, userAssigned) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Lấy danh sách các thảo luận theo topic
        /// </summary>
        /// <param name="userName">string</param>
        /// <param name="DiscustionTopicId">int</param>
        /// <returns>entity</returns>
        public ViewDiscussionByTopicEntity GetListDiscussionByTopic(string userName, int DiscustionTopicId)
        {
            return DiscussionV2Bo.GetListDiscussionByTopic(userName, DiscustionTopicId);
        }

        public List<ViewDiscussionV2Entity> GetListDiscussionUnreadForDashboard(string userName, ref int totalRow)
        {
            return DiscussionV2Bo.GetListDiscussionUnread(userName, ref totalRow);
        }
        #endregion

        #region Discussion Assign
        public WcfActionResponse InsertMultiUser(int DiscussionTopicId, int DiscussionId, string ListToUser, string ListCCUser)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionAssignV2Bo.InsertMultiUser(DiscussionTopicId, DiscussionId, ListToUser, ListCCUser) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse InsertOnUser(DiscussionAssignV2Entity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionAssignV2Bo.InsertOnUser(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateIsRead(int DiscussionTopicId, string UserAssigned)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionAssignV2Bo.UpdateIsRead(DiscussionTopicId, UserAssigned) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Discussion Attacht
        public WcfActionResponse InsertAttach(DiscussionAttachV2Entity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (DiscussionAttachV2Bo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Lấy danh sách 
        /// </summary>
        /// <param name="discussionId"></param>
        /// <returns></returns>
        public List<DiscussionAttachV2Entity> ListAttachByDiscussionId(int discussionId)
        {
            return DiscussionAttachV2Bo.ListAttachByDiscussionId(discussionId);
        }
        #endregion
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
                                                                    