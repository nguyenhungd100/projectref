﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.SurveyManager.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.SurveyManager.Bo;
using ChannelVN.SurveyManager.Entity;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SurveyManagerServices : ISurveyManagerServices
    {
        public WcfActionResponse Insert(SurveyDetailEntity surveyDetailEntity, int zoneId, string zoneIdList)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (SurveyBo.Insert(surveyDetailEntity, zoneId, zoneIdList) == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse Update(SurveyDetailEntity surveyDetailEntity, int zoneId, string zoneIdList)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (SurveyBo.Update(surveyDetailEntity, zoneId, zoneIdList) == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public SurveyDetailEntity GetInfo(int surveyId)
        {
            return SurveyBo.GetInfo(surveyId);
        }

        public List<SurveyEntity> GetList(string keyword, int zoneId, EnumSurveyType type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return SurveyBo.GetList(keyword, zoneId,type, status, pageIndex, pageSize, ref totalRow);
        }

        public WcfActionResponse Delete(int surveyId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (SurveyBo.Delete(surveyId) == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteQuestion(string listId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (SurveyBo.DeleteQuestion(listId) == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse DeleteAnswers(string listId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (SurveyBo.DeleteAnswers(listId) == Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }


        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
