﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.Magazine.Bo;
using ChannelVN.Magazine.Entity;
using ChannelVN.Magazine.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MagazineServices : IMagazineServices
    {

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }


        #region Magazine

        public MagazineEntity GetMagazineById(int Id)
        {
            return MagazineBo.GetMagazineById(Id); ;
        }

        public WcfActionResponse InsertMagazine(MagazineEntity entity, ref int newId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineBo.InsertMagazine(entity, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateMagazine(MagazineEntity entity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineBo.UpdateMagazine(entity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse DeleteMagazine(int magazineId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineBo.DeleteMagazine(magazineId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public MagazineEntity[] MagazineSearch(string keyword, int type, EnumMagazineStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MagazineBo.MagazineSearch(keyword, type, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion

        #region Magazine News

        public WcfActionResponse InsertMagazineNews(MagazineNewsEntity entity, ref int newId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineBo.InsertMagazineNews(entity, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse DeleteMagazineNews(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineBo.DeleteMagazineNews(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public MagazineNewsEntity[] MagazineNewsSearch(string keyword)
        {
            return MagazineBo.MagazineNewsSearch(keyword).ToArray();
        }

        public WcfActionResponse UpdateMagazineNews(int magazineId, string deleteNewsId, string addNewsId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (MagazineBo.UpdateMagazineNews(magazineId, deleteNewsId, addNewsId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
    }
}
