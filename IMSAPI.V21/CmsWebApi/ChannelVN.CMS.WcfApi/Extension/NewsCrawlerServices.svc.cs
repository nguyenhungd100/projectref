﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.NewsCrawler.BO;
using ChannelVN.NewsCrawler.Bo;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsCrawlerServices : INewsCrawlerServices
    {
        #region Sites


        public SiteDetailEntity GetSiteDetailByUrl(string url)
        {
            return SiteBo.GetSiteDetailByUrl(url);
        }

        public NewsCrawler.Entity.SiteEntity[] SearchSite(int status)
        {
            return SiteBo.Search(status).ToArray();
        }

        public CMS.Common.WcfActionResponse UpdateSite(NewsCrawler.Entity.SiteEntity entity, ref int siteId)
        {
            var data = SiteBo.Update(entity, ref siteId);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
             WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public CMS.Common.WcfActionResponse UpdateSiteStatus(int siteId, int status)
        {
            var data = SiteBo.UpdateStatus(siteId, status);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
             WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public WcfActionResponse DeleteSiteBySiteId(int siteId)
        {
            var data = SiteBo.DeleteSiteBySiteId(siteId);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public WcfActionResponse UpdateSiteDetail(SiteDetailEntity siteDetail)
        {
            var data = SiteBo.UpdateSiteDetail(siteDetail);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        #endregion

        #region Zones
        public NewsCrawler.Entity.ZoneEntity[] SearchZone(int status, int sizeId)
        {
            return ZoneBo.Search(status, sizeId).ToArray();
        }

        public CMS.Common.WcfActionResponse UpdateZone(NewsCrawler.Entity.ZoneEntity entity, ref int zoneId)
        {
            var data = ZoneBo.Update(entity, ref zoneId);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public CMS.Common.WcfActionResponse UpdateZoneStatus(int zoneId, int status)
        {
            var data = ZoneBo.UpdateStatus(zoneId, status);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        #endregion

        #region News
        public NewsCrawler.Entity.NewsInListEntity[] SearchNews(int status, int siteId, int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.Search(status, siteId, zoneId, keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public CMS.Common.WcfActionResponse InsertNews(NewsCrawler.Entity.NewsEntity entity)
        {
            var data = NewsBo.Insert(entity);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public CMS.Common.WcfActionResponse UpdateNews(NewsCrawler.Entity.NewsEntity entity)
        {
            var data = NewsBo.Update(entity);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }
        public ChannelVN.NewsCrawler.Entity.NewsEntity GetNewsById(long id)
        {
            return NewsBo.GetById(id);
        }
        public List<ChannelVN.NewsCrawler.Entity.NewsCountEntity> NewsCounter(string accountName, int channelId)
        {
            return NewsBo.NewsCounter(accountName, channelId);
        }

        #endregion

        #region ErrorReport
        public ErrorReportInListEntity[] SearchErrorReport(int siteId, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ErrorReportBo.Search(siteId, zoneId, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public CMS.Common.WcfActionResponse InsertErrorReport(ErrorReportEntity entity)
        {
            var data = ErrorReportBo.Insert(entity);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public ErrorReportEntity GetErrorReportById(int id)
        {
            return ErrorReportBo.GetById(id);
        }
        #endregion

        public Common.ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        #region NewsCrawlerConfig

        public List<NewsCrawlerConfigEntity> SearchNewsCrawlerConfig(string accountName, int siteId, int zoneId)
        {
            return NewsCrawlerConfigBo.SearchConfig(accountName, siteId, zoneId);
        }

        public WcfActionResponse UpdateNewsCrawlerConfig(NewsCrawler.Entity.NewsCrawlerConfigEntity entity)
        {
            var data = NewsCrawlerConfigBo.Update(entity);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }


        public WcfActionResponse AddZone(NewsCrawler.Entity.NewsCrawlerConfigForEditEntity config, string accountName)
        {
            var data = NewsCrawlerConfigBo.AddZone(config, accountName);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }


        public WcfActionResponse AddSite(NewsCrawler.Entity.SiteEntity site)
        {
            var siteId = 0;
            var data = NewsCrawlerConfigBo.AddSite(site, ref siteId);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(siteId.ToString(), "Success") :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }


        public WcfActionResponse UpdateReadMonitor(string accountName, int siteId, int zoneId, int channelId)
        {
            var data = NewsBo.UpdateReadMonitor(accountName, siteId, zoneId, channelId);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public WcfActionResponse UpdateNewsCrawlerConfigDetail(SiteDetailEntity siteDetail)
        {
            var data = NewsCrawlerConfigBo.UpdateNewsCrawlerConfig(siteDetail);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public WcfActionResponse DeleteNewsCrawlerConfig(int id)
        {
            var data = NewsCrawlerConfigBo.DeleteNewsCrawlerConfig(id);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public WcfActionResponse DeleteNewsCrawlerConfigBySiteIdAndZoneId(int siteId, int zoneId)
        {
            var data = NewsCrawlerConfigBo.DeleteNewsCrawlerConfigBySiteIdAndZoneId(siteId, zoneId);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public WcfActionResponse DeleteNewsCrawlerConfigBySiteId(int siteId)
        {
            var data = NewsCrawlerConfigBo.DeleteNewsCrawlerConfigBySiteId(siteId);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }

        public List<SiteDetailInListEntity> GetAllSiteDetailInMyFavorite()
        {
            return NewsCrawlerConfigBo.GetAllSiteDetailInMyFavorite();
        }

        #endregion

        #region CrawlerSystem

        public List<NewsDetailRegexEntity> GetCrawlerSystemByUrl(string url)
        {
            return SiteBo.GetCrawlerSystemByUrl(url);
        }

        #endregion

        #region CafeBiz
        public List<NewsLinkCafeFEntity> SearchCafeBiz(string keyword, int pageIndex, int pageSize, ref int totalRow, int time = 0, string filter = "A", int siteId = -1, int categoryId = -1, string page = "", int typeId = 0)
        {
            return NewsBo.SearchCafeBiz(keyword, pageIndex, pageSize, ref totalRow, time, filter, siteId, categoryId, page, typeId);
        }
        public NewsLinkDetailCafeFEntity GetByIdCafeBiz(long LinkId)
        {
            return NewsBo.GetByIdCafeBiz(LinkId);
        }
        public WcfActionResponse UpdateStatusCrawlerCafeBiz(int LinkId, string LinkStatus)
        {
            var data = NewsBo.UpdateStatusCrawlerCafeBiz(LinkId, LinkStatus);
            return (data == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
            WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                        ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]));
        }
        #endregion
    }
}
