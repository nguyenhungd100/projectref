﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.LiveGameMatch.Bo;
using ChannelVN.LiveGameMatch.Entity;
using ChannelVN.LiveGameMatch.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LiveGameMatchServices : ILiveGameMatchServices
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
        #region LiveGame
        public WcfActionResponse InsertLiveGame(LiveGameEntity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateLiveGame(LiveGameEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteLiveGame(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<LiveGameEntity> ListLiveGameByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveGameBo.ListLiveGameByPages(search, pageIndex, pageSize, ref totalRow);
        }
        public LiveGameEntity GetLiveGameById(int Id)
        {
            return LiveGameBo.GetLiveGameById(Id);
        }
        #endregion
        #region live game match
        public WcfActionResponse InsertLiveGameMatch(LiveGameMatchEntity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameMatchBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateLiveGameMatch(LiveGameMatchEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameMatchBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteLiveGameMatch(long newsId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameMatchBo.Delete(newsId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = newsId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public LiveGameMatchEntity GetGameMatchById(int Id)
        {
            return LiveGameMatchBo.GetGameMatchById(Id);
        }
        public LiveGameMatchEntity GetGameMatchByNewsId(long newsId)
        {
            return LiveGameMatchBo.GetGameMatchByNewsId(newsId);
        }
        public WcfActionResponse UpdatePublishDate(DateTime publishDate, long publishDateStamp, int gameMatchId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameMatchBo.UpdatePublishDate(publishDate, publishDateStamp, gameMatchId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = gameMatchId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
        #region LiveGame
        public WcfActionResponse InsertLiveGameLeague(LiveGameLeagueEntity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameLeagueBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateLiveGameLeague(LiveGameLeagueEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameLeagueBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteLiveGameLeague(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameLeagueBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<LiveGameLeagueEntity> ListLiveGameLeagueByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveGameLeagueBo.ListLiveGameLeagueByPages(search, pageIndex, pageSize, ref totalRow);
        }
        public LiveGameLeagueEntity GetLiveGameLeagueById(int Id)
        {
            return LiveGameLeagueBo.GetLiveGameLeagueById(Id);
        }
        #endregion
        #region LiveTeamGame
        public WcfActionResponse InsertLiveTeamGame(LiveTeamGameEntity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTeamGameBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateLiveTeamGame(LiveTeamGameEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTeamGameBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteLiveTeamGame(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTeamGameBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<LiveTeamGameEntity> ListLiveTeamGameByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveTeamGameBo.ListLiveTeamGameByPages(search, pageIndex, pageSize, ref totalRow);
        }
        public LiveTeamGameEntity GetLiveTeamGameById(int Id)
        {
            return LiveTeamGameBo.GetLiveTeamGameById(Id);
        }
        public List<LiveTeamGameEntity> ListLiveTeamGameByMultiId(int teamA, int teamB)
        {
            return LiveTeamGameBo.ListLiveTeamGameByMultiId(teamA, teamB);
        }
        #endregion
    }
}
