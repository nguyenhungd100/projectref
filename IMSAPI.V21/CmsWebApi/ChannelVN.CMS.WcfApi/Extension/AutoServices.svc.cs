﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.Auto.Bo;
using ChannelVN.Auto.Entity;
using ChannelVN.Auto.Entity.ErrorCode;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AutoServices : IAutoServices
    {
        public List<Auto.Entity.AutoManufacturerEntity> SearchAutoManufacturer(string keyword, Auto.Entity.EnumAutoManufacturerStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoManufacturerBo.SearchAutoManufacturer(keyword, status, pageIndex, pageSize, ref totalRow);
        }

        public Auto.Entity.AutoManufacturerEntity GetAutoManufacturerByAutoManufacturerId(int id)
        {
            return AutoManufacturerBo.GetAutoManufacturerByAutoManufacturerId(id);
        }

        public CMS.Common.WcfActionResponse InsertAutoManufacturer(Auto.Entity.AutoManufacturerEntity autoManufacturer)
        {
            var errorCode = AutoManufacturerBo.InsertAutoManufacturer(autoManufacturer);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateAutoManufacturer(Auto.Entity.AutoManufacturerEntity autoManufacturer)
        {
            var errorCode = AutoManufacturerBo.UpdateAutoManufacturer(autoManufacturer);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateStatusAutoManufacturer(int id, EnumAutoManufacturerStatus status)
        {
            var errorCode = AutoManufacturerBo.UpdateAutoManufacturerStatus(id, status);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteById(long tagId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = AutoManufacturerBo.DeleteById(tagId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public Auto.Entity.AlbumInAutoManufacturerEntity GetAlbumInAutoManufacturerByAlbumId(int id)
        {
            return AutoManufacturerBo.GetAlbumInAutoManufacturerByAlbumId(id);
        }

        public CMS.Common.WcfActionResponse UpdateAlbumInAutoManufacturer(AlbumInAutoManufacturerEntity albumInAutoManufacturerEntity)
        {
            var errorCode = AutoManufacturerBo.UpdateAlbumInAutoManufacturer(albumInAutoManufacturerEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<AutoManufacturerEntity> GetByParentId(int id)
        {
            return AutoManufacturerBo.GetByParentId(id);
        }

        public CMS.Common.WcfActionResponse InsertAutoModel(Auto.Entity.AutoModelEntity autoModel)
        {
            var errorCode = AutoModelBo.InsertAutoModel(autoModel);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateAutoModel(Auto.Entity.AutoModelEntity autoModel)
        {
            var errorCode = AutoModelBo.UpdateAutoModel(autoModel);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateAvatarAutoModel(Auto.Entity.AutoModelEntity autoModel)
        {
            var errorCode = AutoModelBo.UpdateAvatarAutoModel(autoModel);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteAutoModelById(long tagId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = AutoModelBo.DeleteById(tagId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public AutoModelEntity GetAutoModelByAutoModelId(int id)
        {
            return AutoModelBo.GetAutoModelByAutoModelId(id);
        }
        public AutoModelEntity GetAutoModelByName(string name)
        {
            return AutoModelBo.GetAutoModelByName(name);
        }

        public List<Auto.Entity.AutoModelEntity> SearchAutoModel(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoModelBo.SearchAutoModel(keyword, pageIndex, pageSize, ref totalRow);
        }
        public List<AutoManufacturerEntity> SearchAutoManufacturerByParent(string keyword, EnumAutoManufacturerStatus status, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoManufacturerBo.SearchAutoManufacturerByParent(keyword, status, parentId, pageIndex, pageSize, ref totalRow);
        }
        public AutoModelEntity GetAutoModelByTagId(long id)
        {
            return AutoModelBo.GetAutoModelByTagId(id);
        }

        public List<Auto.Entity.AutoTypeEntity> SearchAutoType(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return AutoTypeBo.SearchAutoType(keyword, pageIndex, pageSize, ref totalRow);
        }

        public Auto.Entity.AutoTypeEntity GetAutoTypeByAutoTypeId(int id)
        {
            return AutoTypeBo.GetAutoTypeByAutoTypeId(id);
        }

        public CMS.Common.WcfActionResponse InsertAutoType(Auto.Entity.AutoTypeEntity autoType)
        {
            var errorCode = AutoTypeBo.InsertAutoType(autoType);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateAutoType(Auto.Entity.AutoTypeEntity autoType)
        {
            var errorCode = AutoTypeBo.UpdateAutoType(autoType);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteAutoTypeById(long tagId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = AutoTypeBo.DeleteById(tagId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public List<AutoModelEntity> SeachModelEmbed(int type)
        {
            return BoxAutoModelEmbedBo.Search(type);
        }

        public CMS.Common.WcfActionResponse UpdateBoxAutoModel(int type, string lst)
        {
            var errorCode = BoxAutoModelEmbedBo.UpdateAutoModel(type, lst);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse InsertAlbumInAutoModel(int autoModelId, int autoAlbumId)
        {
            var errorCode = AutoManufacturerBo.UpdateAlbumInAutoModel(autoModelId, autoAlbumId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public Auto.Entity.AlbumInAutoManufacturerEntity GetAlbumAutoModelByModelId(int id)
        {
            return AutoManufacturerBo.GetAlbumAutoModelByModelId(id);
        }

        public Common.ErrorMessageEntity[] GetErrorMessage()
        {
            return new Common.ErrorMessageEntity[] { };
        }
    }
}
