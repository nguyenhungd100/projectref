﻿using System.Globalization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.Olympia.Bo;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class OlympiaServices : IOlympiaServices
    {
        #region SCORE
        public List<OlympiaScoreEntity> GetListScore(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaScoreBo.GetListScore(Keyword, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertScore(OlympiaScoreEntity elm, ref int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaScoreBo.InsertScore(elm, ref Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateScore(OlympiaScoreEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaScoreBo.UpdateScore(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public OlympiaScoreEntity SelectScore(int Id)
        {
            return OlympiaScoreBo.SelectScore(Id);
        }
        public WcfActionResponse DeleteScore(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaScoreBo.DeleteScore(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region PLAYER
        public List<OlympiaPlayerEntity> GetListPlayer(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaPlayerBo.GetListPlayer(Keyword, pageIndex, pageSize, ref totalRow);
        }
        public List<OlympiaPlayerEntity> GetListPlayerByCompetition(int CompetitionID)
        {
            return OlympiaPlayerBo.GetListPlayerByCompetition(CompetitionID);
        }
        public List<OlympiaPlayerEntity> GetListPlayerByPlayer(int playerId)
        {
            return OlympiaPlayerBo.GetListPlayerByPlayer(playerId);
        }
        public WcfActionResponse InsertPlayer(OlympiaPlayerEntity elm, ref int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaPlayerBo.InsertPlayer(elm, ref Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdatePlayer(OlympiaPlayerEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaPlayerBo.UpdatePlayer(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public OlympiaPlayerEntity SelectPlayer(int Id)
        {
            return OlympiaPlayerBo.SelectPlayer(Id);
        }
        public WcfActionResponse DeletePlayer(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaPlayerBo.DeletePlayer(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion    

        #region COMPETITION
        public List<OlympiaCompetitionEntity> GetListCompetition(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaCompetitionBo.GetListCompetition(Keyword, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertCompetition(OlympiaCompetitionEntity elm, ref int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaCompetitionBo.InsertCompetition(elm, ref Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse InsertResult(OlympiaResultEntity elm, ref int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaCompetitionBo.InsertResult(elm, ref Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        
        public WcfActionResponse UpdateCompetition(OlympiaCompetitionEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaCompetitionBo.UpdateCompetition(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public OlympiaCompetitionEntity SelectCompetition(int Id)
        {
            return OlympiaCompetitionBo.SelectCompetition(Id);
        }
        public WcfActionResponse DeleteCompetition(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaCompetitionBo.DeleteCompetition(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteResult(int competitionID)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaCompetitionBo.DeleteResult(competitionID) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = competitionID.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region SCHEDULE
        public List<OlympiaScheduleEntity> GetListSchedule(string keyword, int ScheduleType, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaScheduleBo.GetListSchedule(keyword, ScheduleType, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertSchedule(OlympiaScheduleEntity elm, ref int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaScheduleBo.InsertSchedule(elm, ref Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateSchedule(OlympiaScheduleEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaScheduleBo.UpdateSchedule(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public OlympiaScheduleEntity SelectSchedule(int Id)
        {
            return OlympiaScheduleBo.SelectSchedule(Id);
        }
        public WcfActionResponse DeleteSchedule(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaScheduleBo.DeleteSchedule(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region CALENDAR
        public List<OlympiaCalendarEntity> GetListCalendar(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaCalendarBo.GetListCalendar(keyword, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertCalendar(OlympiaCalendarEntity elm, ref int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaCalendarBo.InsertCalendar(elm, ref Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateCalendar(OlympiaCalendarEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaCalendarBo.UpdateCalendar(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public OlympiaCalendarEntity SelectCalendar(int Id)
        {
            return OlympiaCalendarBo.SelectCalendar(Id);
        }
        public WcfActionResponse DeleteCalendar(int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaCalendarBo.DeleteCalendar(Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region MEDIA
        public WcfActionResponse UpdateOlympiaMedia(OlympiaMediaEntity olympiaMedia)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var funnyNewsId = 0;

            if (OlympiaMediaBO.UpdateOlympiaMedia(olympiaMedia) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = funnyNewsId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdateOlympiaMediaStatus(int id, EnumOlympiaMediaStatus status)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (OlympiaMediaBO.UpdateOlympiaMediaStatus(id, status) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public OlympiaMediaEntity GetOlympiaMediaById(int id)
        {
            return OlympiaMediaBO.GetOlympiaMediaById(id);
        }
        public OlympiaMediaEntity[] SearchOlympiaMedia(string keyword, int year, int quarter, int month, int week, EnumOlympiaMediaType mediaType, EnumOlympiaMediaStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaMediaBO.SearchOlympiaMedia(keyword, year, quarter, month, week, mediaType, status, pageIndex,
                                                     pageSize, ref totalRow).ToArray();
        }
        #endregion

        #region WINNER

        public List<OlympiaWinnerEntity> SearchOlympiaWinner(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaWinnerBO.SearchOlympiaWinner(keyword, pageIndex, pageSize, ref totalRow);
        }

        public OlympiaWinnerEntity GetOlympiaWinnerById(int id)
        {
            return OlympiaWinnerBO.GetOlympiaWinnerById(id);
        }

        public WcfActionResponse UpdateOlympiaWinner(OlympiaWinnerEntity olympiaWinner)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var funnyNewsId = 0;

            if (OlympiaWinnerBO.UpdateOlympiaWinner(olympiaWinner) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = funnyNewsId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteOlympiaWinnerById(int id)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (OlympiaWinnerBO.DeleteOlympiaWinnerById(id) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        #endregion

        #region FAQ
        public List<OlympiaFaqEntity> ListFaq(string keyword,int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaFaqBo.ListFaq(keyword,status, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse InsertFaq(OlympiaFaqEntity elm, ref Int64 id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaFaqBo.InsertFaq(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateFaq(OlympiaFaqEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaFaqBo.UpdateFaq(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public OlympiaFaqEntity SelectFaq(Int64 id)
        {
            return OlympiaFaqBo.SelectFaq(id);
        }
        public WcfActionResponse DeleteFaq(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (OlympiaFaqBo.DeleteFaq(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
        
    }
}
