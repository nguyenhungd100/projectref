﻿using System.Collections.Generic;
using System.ServiceModel.Activation;
using System.Web.UI.WebControls;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.TraBenh.Bo;
using ChannelVN.TraBenh.Entity;
using ChannelVN.TraBenh.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DiseaseServices : IDiseaseServices
    {
        #region Group Disease
        /// <summary>
        /// Get List Group Disease By Keyword - ngocnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<GroupDiseaseEntity> GroupDiseaseGetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiseaseBo.GroupDiseaseGetList(keyword, pageIndex, pageSize, ref totalRow);
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets tất cả nhóm bệnh > để chèn vào dropdownlist
        /// </summary>
        /// <returns>List Of GroupDiseaseEntity</returns>
        public List<GroupDiseaseEntity> GroupDiseaseGetAll()
        {
            return DiseaseBo.GroupDiseaseGetAll();
        }

        /// <summary>
        /// Update Group Disease - ngocnh
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateGroupDisease(GroupDiseaseEntity info, ref int id)
        {
            return DiseaseBo.UpdateGroupDisease(info, ref id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        /// <summary>
        /// Get Group Disease By Id - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GroupDiseaseEntity GetGroupDiseaseById(int id)
        {
            return DiseaseBo.GetGroupDiseaseById(id);
        }
        /// <summary>
        /// Delete Group Disease By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteGroupDiseaseById(int id)
        {
            return DiseaseBo.DeleteGroupDiseaseById(id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        #endregion

        #region ExpertOnTheDisease
        /// <summary>
        /// Get List Group Disease By Keyword - ngocnh
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<ExpertOnTheDiseaseEntity> ExpertOnTheDiseaseGetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiseaseBo.ExpertOnTheDiseaseGetList(keyword, pageIndex, pageSize, ref totalRow);
        }
        /// <summary>
        /// Update Group Disease - ngocnh
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateExpertOnTheDisease(ExpertOnTheDiseaseEntity info, ref int id)
        {
            return DiseaseBo.UpdateExpertOnTheDisease(info, ref id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        /// <summary>
        /// Get Group Disease By Id - ngocnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseById(int id)
        {
            return DiseaseBo.GetExpertOnTheDiseaseById(id);
        }
        /// <summary>
        /// Delete Group Disease By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteExpertOnTheDiseaseById(int id)
        {
            return DiseaseBo.DeleteExpertOnTheDiseaseById(id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        /// <summary>
        /// Ngocnh
        /// 18/05/2015
        /// Lấy toàn bộ danh sách chuyên gia
        /// </summary>
        /// <returns></returns>
        public List<ExpertOnTheDiseaseEntity> GetAllListExpertOnTheDisease()
        {
            return DiseaseBo.GetAllListExpertOnTheDisease();
        }
        /// <summary>
        /// Ngocnh
        /// 19/05/2015
        /// Get Expert By NewsId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseByNewsId(long id)
        {
            return DiseaseBo.GetExpertOnTheDiseaseByNewsId(id);
        }
        #endregion

        #region Disease


        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo DisaeseId. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="diseaseId"></param>
        /// <returns></returns>
        public DiseaseEntity DiseaseGetById(int diseaseId)
        {
            return DiseaseBo.DiseaseGetById(diseaseId);
        }

        /// <summary>
        /// NANIA
        /// 2015-05-18
        /// Gets danh sách bệnh theo danh sách IDs
        /// </summary>
        /// <param name="diseaseIdList"></param>
        /// <returns></returns>
        public List<DiseaseEntity> DiseaseGetListByIds(string diseaseIdList)
        {
            return DiseaseBo.DiseaseGetListByIds(diseaseIdList);
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo [Name]. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public DiseaseEntity DiseaseGetByName(string name)
        {
            return DiseaseBo.DiseaseGetByName(name);
        }


        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets danh sách thông tin bệnh có lọc theo điều kiện
        /// </summary>
        /// <param name="keyword">Từ khóa: search theo tên bệnh</param>
        /// <param name="status">Trạng thái</param>
        /// <param name="type">Loại bệnh</param>
        /// <param name="mode">(Vị trí trên trang)</param>
        /// <param name="groupId">Nhóm bệnh</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow">CÓ OUTPUT</param>
        /// <returns></returns>
        public List<DiseaseEntity> DiseaseGetList(string keyword, int status, int type, int mode, int groupId, int pageIndex, int pageSize, ref int totalRow)
        {
            return DiseaseBo.DiseaseGetList(keyword, status, type, mode, groupId, pageIndex, pageSize, ref totalRow);
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Trả kết quả suggest theo từ khóa hoặc chữ cái đầu tiên của bệnh
        /// </summary>
        /// <param name="firstChar"></param>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public List<DiseaseEntity> DiseaseSuggest(string firstChar, string keyword, int top)
        {
            return DiseaseBo.DiseaseSuggest(firstChar, keyword, top);
        }

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Cập nhật thông tin bệnh, bao gồm cả thông tin mở rộng: DiseaseStaticInfo
        /// </summary>
        /// <param name="diseaseEntity"></param>
        /// <param name="newsRelationIds"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateDisease(DiseaseEntity diseaseEntity, string newsRelationIds)
        {
            return DiseaseBo.UpdateDisease(diseaseEntity, newsRelationIds) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        /// <summary>
        /// Get Name, Id thông tin bệnh gắn vào bài - ngocnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public List<DiseaseEntity> DiseaseGetByNewsId(long newsId)
        {
            return DiseaseBo.DiseaseGetByNewsId(newsId);
        }

        /// <summary>
        /// Gets danh sách News trong Disease
        /// </summary>
        /// <param name="diseaseId"></param>
        /// <returns></returns>
        public List<NewsInDisease> DiseaseGetNewsDiseaseId(int diseaseId)
        {
            return DiseaseBo.DiseaseGetNewsDiseaseId(diseaseId);
        }

        /// <summary>
        /// Insert Bệnh vào tin theo list Id Bệnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="listDiseases"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateDiseaseInNews(long newsId, string listDiseases)
        {
            return DiseaseBo.UpdateDiseaseInNews(newsId, listDiseases) == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                              ErrorMapping.Current[
                                                                  ErrorMapping.ErrorCodes.BusinessError]);
        }

        /// <summary>
        /// Ngocnh
        /// 22/5/2015
        /// Cập nhật chuyên gia của bệnh vào tin
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="expertId"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateExpertOnDiseaseInNews(long newsId, int expertId)
        {
            return DiseaseBo.UpdateExpertOnDiseaseInNews(newsId, expertId) == ErrorMapping.ErrorCodes.Success
                      ? WcfActionResponse.CreateSuccessResponse()
                      : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                              ErrorMapping.Current[
                                                                  ErrorMapping.ErrorCodes.BusinessError]);
        }

        #endregion

        #region Menu

        public List<MenuEntity> GetAllOfMenu()
        {
            return DiseaseBo.GetAllOfMenu();
        }
        public List<MenuEntity> GetListMenuByParent(int parentId)
        {
            return DiseaseBo.GetListMenuByParent(parentId);
        }
        public MenuEntity GetMenuDetails(int id)
        {
            return DiseaseBo.GetMenuDetails(id);
        }

        public int UpdateMenu(MenuEntity menuEntity)
        {
            return DiseaseBo.UpdateMenu(menuEntity);
        }

        public WcfActionResponse DeleteMenu(int id)
        {
            return DiseaseBo.DeleteMenu(id) == ErrorMapping.ErrorCodes.Success
                ? WcfActionResponse.CreateSuccessResponse()
                : WcfActionResponse.CreateErrorResponse((int) ErrorMapping.ErrorCodes.BusinessError,
                    ErrorMapping.Current[
                        ErrorMapping.ErrorCodes.BusinessError]);
        }

        #endregion
    }
}
