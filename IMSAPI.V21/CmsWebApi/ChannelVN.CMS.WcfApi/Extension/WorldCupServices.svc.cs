﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;
using ChannelVN.WorldCup.Bo;
using ChannelVN.WorldCup.Entity.ErrorCode;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WorldCupServices : IWorldCupServices
    {
        #region Team
        public List<WorldCup.Entity.WCTeamsEntity> SearchTeams(string groupName)
        {
            return WCTeamsBo.SearchTeams(groupName);
        }

        public WorldCup.Entity.WCTeamsEntity GetTeamById(int id)
        {
            return WCTeamsBo.GetWCTeamsById(id);
        }

        public CMS.Common.WcfActionResponse InsertTeam(WorldCup.Entity.WCTeamsEntity team)
        {
            var errorCode = WCTeamsBo.InsertTeam(team);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateTeam(WorldCup.Entity.WCTeamsEntity team)
        {
            var errorCode = WCTeamsBo.UpdateTeam(team);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse DeleteTeam(int id)
        {
            var errorCode = WCTeamsBo.DeteleTeam(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region TopPlayer
        public List<WorldCup.Entity.WCTopPlayersDetailEntity> SearchTopPlayer(string name)
        {
            return WCTeamsBo.SearchTopPlayers(name);
        }

        public WorldCup.Entity.WCTopPlayersEntity GetTopPlayerById(int id)
        {
            return WCTeamsBo.GetTopPlayerById(id);
        }

        public CMS.Common.WcfActionResponse InsertTopPlayer(WorldCup.Entity.WCTopPlayersEntity player)
        {
            var errorCode = WCTeamsBo.InsertTopPlayer(player);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateTopPlayer(WorldCup.Entity.WCTopPlayersEntity player)
        {
            var errorCode = WCTeamsBo.UpdateTopPlayer(player);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse DeleteTopPlayer(int id)
        {
            var errorCode = WCTeamsBo.DeleteTopPlayer(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        public Common.ErrorMessageEntity[] GetErrorMessage()
        {
            return new Common.ErrorMessageEntity[] { };
        }
    }
}
