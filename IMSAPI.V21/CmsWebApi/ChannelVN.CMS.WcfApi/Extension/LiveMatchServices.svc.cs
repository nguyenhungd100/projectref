﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.LiveMatch.Bo;
using ChannelVN.LiveMatch.Entity;
using ChannelVN.LiveMatch.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LiveMatchServices : ILiveMatchServices
    {
        #region LiveMatch

        public List<LiveMatchEntity> SearchLiveMatch(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveMatchBo.SearchLiveMatch(keyword, pageIndex, pageSize, ref totalRow);
        }
        public List<LiveMatchEntity> SearchLiveMatchForSuggestion(string keyword, int top)
        {
            return LiveMatchBo.SearchLiveMatchForSuggestion(keyword, top);
        }
        public LiveMatchEntity GetLiveMatchByLiveMatchId(int liveMatchId)
        {
            return LiveMatchBo.GetLiveMatchByLiveMatchId(liveMatchId);
        }
        public LiveMatchDetailEntity GetLiveMatchDetail(int liveMatchId)
        {
            return LiveMatchBo.GetLiveMatchDetail(liveMatchId);
        }
        public WcfActionResponse InsertLiveMatch(LiveMatchEntity liveMatch)
        {
            var errorCode = LiveMatchBo.InsertLiveMatch(liveMatch);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertLiveMatchV2(LiveMatchEntity liveMatch, ref int Id)
        {
            var errorCode = LiveMatchBo.InsertLiveMatchV2(liveMatch,ref Id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateLiveMatch(LiveMatchEntity liveMatch)
        {
            var errorCode = LiveMatchBo.UpdateLiveMatch(liveMatch);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteLiveMatch(int liveMatchId)
        {
            var errorCode = LiveMatchBo.DeleteLiveMatch(liveMatchId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region LiveMatchTimeline

        public List<LiveMatchTimelineEntity> GetLiveMatchTimelineByLiveMatchId(int liveMatchId)
        {
            return LiveMatchTimelineBo.GetLiveMatchTimelineByLiveMatchId(liveMatchId);
        }
        public LiveMatchTimelineEntity GetLiveMatchTimelineById(int liveMatchTimelineId)
        {
            return LiveMatchTimelineBo.GetLiveMatchTimelineById(liveMatchTimelineId);
        }
        public WcfActionResponse InsertLiveMatchTimeline(LiveMatchTimelineEntity liveMatchTimeline, ref int timelineId)
        {
            var errorCode = LiveMatchTimelineBo.InsertLiveMatchTimeline(liveMatchTimeline, ref timelineId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateLiveMatchTimeline(LiveMatchTimelineEntity liveMatchTimeline)
        {
            var errorCode = LiveMatchTimelineBo.UpdateLiveMatchTimeline(liveMatchTimeline);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteLiveMatchTimeline(int liveMatchTimelineId)
        {
            var errorCode = LiveMatchTimelineBo.DeleteLiveMatchTimeline(liveMatchTimelineId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region LiveMatchEvent

        public List<LiveMatchEventEntity> GetLiveMatchEventByLiveMatchId(int liveMatchId)
        {
            return LiveMatchEventBo.GetLiveMatchEventByLiveMatchId(liveMatchId);
        }
        public LiveMatchEventEntity GetLiveMatchEventById(int liveMatchEventId)
        {
            return LiveMatchEventBo.GetLiveMatchEventById(liveMatchEventId);
        }
        public WcfActionResponse InsertLiveMatchEvent(LiveMatchEventEntity liveMatchEvent)
        {
            var errorCode = LiveMatchEventBo.InsertLiveMatchEvent(liveMatchEvent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateLiveMatchEvent(LiveMatchEventEntity liveMatchEvent)
        {
            var errorCode = LiveMatchEventBo.UpdateLiveMatchEvent(liveMatchEvent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteLiveMatchEvent(int liveMatchEventId)
        {
            var errorCode = LiveMatchEventBo.DeleteLiveMatchEvent(liveMatchEventId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region LiveMatchPenalty

        public List<LiveMatchPenaltyEntity> GetLiveMatchPenaltyByLiveMatchId(int liveMatchId)
        {
            return LiveMatchPenaltyBo.GetLiveMatchPenaltyByLiveMatchId(liveMatchId);
        }
        public LiveMatchPenaltyEntity GetLiveMatchPenaltyById(int liveMatchPenaltyId)
        {
            return LiveMatchPenaltyBo.GetLiveMatchPenaltyById(liveMatchPenaltyId);
        }
        public WcfActionResponse InsertLiveMatchPenalty(LiveMatchPenaltyEntity liveMatchPenalty)
        {
            var errorCode = LiveMatchPenaltyBo.InsertLiveMatchPenalty(liveMatchPenalty);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateLiveMatchPenalty(LiveMatchPenaltyEntity liveMatchPenalty)
        {
            var errorCode = LiveMatchPenaltyBo.UpdateLiveMatchPenalty(liveMatchPenalty);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteLiveMatchPenalty(int liveMatchPenaltyId)
        {
            var errorCode = LiveMatchPenaltyBo.DeleteLiveMatchPenalty(liveMatchPenaltyId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
