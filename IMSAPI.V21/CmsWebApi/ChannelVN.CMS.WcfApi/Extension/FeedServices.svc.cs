﻿using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.Feed.BO;
using ChannelVN.Feed.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FeedServices : IFeedServices
    {
        public bool Insert_FeedItem(FeedItemEntity feedItem, ref int Id)
        {
            return FeedBo.Insert_FeedItem(feedItem, ref Id);
        }
        public bool Update_Status(int Id, FeedItemStatus status, int DownloadMediaId=-1)
        {
            return FeedBo.Update_Status(Id, status,DownloadMediaId);
        }
        public bool Update_Status_ByMediaIds(string MediaIds)
        {
            return FeedBo.Update_Status_ByMediaIds(MediaIds);
        }
        public List<FeedEntity> Feed_GetList()
        {
            return FeedBo.Feed_GetList();
        }
        public List<FeedItemEntity> FeedItem_GetList()
        {
            return FeedBo.FeedItem_GetList();
        }
        public List<FeedItemEntity> FeedItem_GetList_Download()
        {
            return FeedBo.FeedItem_GetList_Download();
        }
        public List<FeedItemEntity> FeedItem_GetList_ReDownload()
        {
            return FeedBo.FeedItem_GetList_ReDownload();
        }

        #region FeedItemV2
        public bool Insert_FeedItemV2(FeedItemV2Entity feedItem, ref int Id)
        {
            return FeedBo.Insert_FeedItemV2(feedItem, ref Id);
        }
        public bool FeedItemV2_UpdateByComplete(FeedItemV2Entity feedItem)
        {
            return FeedBo.FeedItemV2_UpdateByComplete(feedItem);
        }
        public bool FeedItemV2_DownloadFinished(string MediaIds)
        {
            return FeedBo.FeedItemV2_DownloadFinished(MediaIds);
        }
        public bool FeedItemV2_UpdateStatus(int Id,FeedItemStatus Status)
        {
            return FeedBo.FeedItemV2_UpdateStatus(Id, Status);
        }
        public bool FeedItemV2_Downloading(string IdAndMediaIds)
        {
            return FeedBo.FeedItemV2_Downloading(IdAndMediaIds);
        }
        public List<FeedItemV2Entity> FeedItemV2_GetListCheckDownload()
        {
            return FeedBo.FeedItemV2_GetListCheckDownload();
        }
        public List<FeedItemV2Entity> FeedItemV2_GetListDownloadFinished()
        {
            return FeedBo.FeedItemV2_GetListDownloadFinished();
        }
        public FeedItemV2Entity FeedItemV2_GetById(int Id)
        {
            return FeedBo.FeedItemV2_GetById(Id);
        }
        #endregion
    }
}
