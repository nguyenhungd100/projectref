﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.InteractiveVote.Entity;
using ChannelVN.WcfExtensions;
using System.ServiceModel.Activation;
using ChannelVN.InteractiveVote.Bo;
using ChannelVN.InteractiveVote.Entity.ErrorCode;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class InteractiveVoteServices : IInteractiveVoteServices
    {

        public WcfActionResponse InsertInteractiveVote(InteractiveVoteEntity interactiveContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveVoteBo.InsertInteractiveVote(interactiveContent) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateInteractiveVote(InteractiveVoteEntity interactiveContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveVoteBo.UpdateInteractiveVote(interactiveContent) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateInteractiveVoteData(int id, string interactiveData)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveVoteBo.UpdateInteractiveVoteData(id, interactiveData) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public InteractiveVoteEntity GetInteractiveVoteById(int id)
        {
            return InteractiveVoteBo.GetInteractiveVoteById(id);
        }

        public List<InteractiveVoteEntity> SearchInteractiveVote(string keyword, EnumInteractiveVoteType type, string createdBy, int status, int siteId,
            int pageIndex, int pageSize, ref int totalRow)
        {
            return InteractiveVoteBo.SearchInteractiveVote(keyword, type, createdBy, status, siteId,
                pageIndex, pageSize, ref totalRow);
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
