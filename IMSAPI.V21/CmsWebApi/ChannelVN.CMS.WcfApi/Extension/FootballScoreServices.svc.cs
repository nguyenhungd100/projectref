﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.FootballScore.BO;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common.RedisClientHelper;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FootballScoreServices : IFootballScoreServices
    {

        #region FootballScore

        public WcfActionResponse InsertFootballScore(FootballScore.Entity.FootballScoreEntity footballScore)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var newId = 0;

            if (FootballScoreBo.InsertFootballScore(footballScore, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = newId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateFootballScore(FootballScore.Entity.FootballScoreEntity footballScore)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballScoreBo.UpdateFootballScore(footballScore) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = footballScore.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }


        public WcfActionResponse DeleteFootballScore(int footballScoreId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballScoreBo.DeleteFootballScore(footballScoreId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = footballScoreId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdateFootballScorePriority(string listOfPriority)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballScoreBo.UpdateFootballScorePriority(listOfPriority) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public FootballScoreEntity GetFootballScore(int footballScoreId)
        {
            return FootballScoreBo.GetFootballScoreById(footballScoreId);
        }

        public FootballScoreEntity[] SearchFootballScore(string keyword, EnumFootballScoreStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                FootballScoreBo.SearchFootballScore(keyword, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion

        #region LiveSportTV

        public WcfActionResponse InsertLiveSportTV(LiveSportTVEntity liveSportTV)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var newId = 0;

            if (LiveSportTVBo.InsertLiveSportTV(liveSportTV, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = newId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateLiveSportTV(LiveSportTVEntity liveSportTV)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (LiveSportTVBo.UpdateLiveSportTV(liveSportTV) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = liveSportTV.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }


        public WcfActionResponse DeleteLiveSportTV(int liveSportTVId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (LiveSportTVBo.DeleteLiveSportTV(liveSportTVId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = liveSportTVId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public LiveSportTVEntity GetLiveSportTV(int liveSportTVId)
        {
            return LiveSportTVBo.GetLiveSportTVById(liveSportTVId);
        }

        public LiveSportTVEntity[] SearchLiveSportTV(string keyword, EnumLiveSportTVStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                LiveSportTVBo.SearchLiveSportTV(keyword, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion

        #region SeaGameRanking

        [DataContract]
        public class SeaGameRankingEntity
        {
            [DataMember]
            public long Id { get; set; }
            [DataMember]
            public string Title { get; set; }
            [DataMember]
            public string Avatar { get; set; }
            [DataMember]
            public int NumberOfGold { get; set; }
            [DataMember]
            public int NumberOfSilver { get; set; }
            [DataMember]
            public int NumberOfBronze { get; set; }
            [DataMember]
            public int Priority { get; set; }
        }

        private const string RankingKeyFormat = "IMSTempData:SeaGameRanking";
        private static readonly int RedisDb = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));

        public List<SeaGameRankingEntity> GetAllSeaGameRanking()
        {
            var redisClient = new RedisStackClient(RedisDb);
            var allRanking = redisClient.Get<List<SeaGameRankingEntity>>(RankingKeyFormat);
            if (allRanking == null)
            {
                allRanking = new List<SeaGameRankingEntity>();
            }
            return allRanking;
        }
        public SeaGameRankingEntity GetSeaGameRankingById(long id)
        {
            var redisClient = new RedisStackClient(RedisDb);
            var allRanking = redisClient.Get<List<SeaGameRankingEntity>>(RankingKeyFormat);
            if (allRanking != null)
            {
                foreach (var ranking in allRanking)
                {
                    if (ranking.Id == id)
                    {
                        return ranking;
                    }
                }
            }
            return null;
        }
        public WcfActionResponse AddNewsRanking(SeaGameRankingEntity seaGameRanking)
        {
            if (seaGameRanking == null || string.IsNullOrEmpty(seaGameRanking.Title))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, "Chưa nhập tiêu đề.");
            }
            seaGameRanking.Id = long.Parse(DateTime.Now.ToString(BoConstants.NEWS_FORMAT_ID));

            try
            {
                var redisClient = new RedisStackClient(RedisDb);
                var allRanking = redisClient.Get<List<SeaGameRankingEntity>>(RankingKeyFormat);
                if (allRanking == null)
                {
                    allRanking = new List<SeaGameRankingEntity>();
                }
                allRanking.Add(seaGameRanking);
                redisClient.Remove(RankingKeyFormat);
                redisClient.Add(RankingKeyFormat, allRanking);
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, "Có lỗi khi cập nhật.");
            }
        }
        public WcfActionResponse UpdateNewsRanking(SeaGameRankingEntity seaGameRanking)
        {
            if (seaGameRanking == null || seaGameRanking.Id <= 0 || string.IsNullOrEmpty(seaGameRanking.Title))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, "Chưa nhập tiêu đề.");
            }
            try
            {
                var redisClient = new RedisStackClient(RedisDb);
                var allRanking = redisClient.Get<List<SeaGameRankingEntity>>(RankingKeyFormat);
                if (allRanking != null)
                {
                    var count = allRanking.Count;
                    for (var i = 0; i < count; i++)
                    {
                        if (allRanking[i].Id == seaGameRanking.Id)
                        {
                            allRanking[i].Title = seaGameRanking.Title;
                            allRanking[i].Avatar = seaGameRanking.Avatar;
                            allRanking[i].NumberOfGold = seaGameRanking.NumberOfGold;
                            allRanking[i].NumberOfSilver = seaGameRanking.NumberOfSilver;
                            allRanking[i].NumberOfBronze = seaGameRanking.NumberOfBronze;
                            allRanking[i].Priority = seaGameRanking.Priority;
                            break;
                        }
                    }
                }
                redisClient.Remove(RankingKeyFormat);
                redisClient.Add(RankingKeyFormat, allRanking, DateTime.Now.AddYears(2).TimeOfDay);
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, "Có lỗi khi cập nhật.");
            }
        }
        public WcfActionResponse DeleteNewsRanking(long id)
        {
            try
            {
                var redisClient = new RedisStackClient(RedisDb);
                var allRanking = redisClient.Get<List<SeaGameRankingEntity>>(RankingKeyFormat);
                if (allRanking != null)
                {
                    var count = allRanking.Count;
                    for (var i = 0; i < count; i++)
                    {
                        if (allRanking[i].Id == id)
                        {
                            allRanking.RemoveAt(i);
                            break;
                        }
                    }
                }
                redisClient.Remove(RankingKeyFormat);
                redisClient.Add(RankingKeyFormat, allRanking, DateTime.Now.AddYears(2).TimeOfDay);
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, "Có lỗi khi cập nhật.");
            }
        }

        #endregion

        #region FootballScoreDetail

        public WcfActionResponse InsertFootballScoreDetail(FootballScoreDetailEntity footballScoreDetail)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var newId = 0;

            if (FootballScoreDetailBo.InsertFootballScoreDetail(footballScoreDetail, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = newId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateFootballScoreDetail(FootballScoreDetailEntity footballScoreDetail)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballScoreDetailBo.UpdateFootballScoreDetail(footballScoreDetail) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = footballScoreDetail.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteFootballScoreDetail(int footballScoreDetailId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballScoreDetailBo.DeleteFootballScoreDetail(footballScoreDetailId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = footballScoreDetailId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public FootballScoreDetailEntity GetFootballScoreDetailById(int footballScoreDetailId)
        {
            return FootballScoreDetailBo.GetFootballScoreDetailById(footballScoreDetailId);
        }

        public FootballScoreDetailEntity[] SearchFootballScoreDetail(string keyword, int FootballScoreId, EnumFootballScoreDetailStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                FootballScoreDetailBo.SearchFootballScoreDetail(keyword, FootballScoreId, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
