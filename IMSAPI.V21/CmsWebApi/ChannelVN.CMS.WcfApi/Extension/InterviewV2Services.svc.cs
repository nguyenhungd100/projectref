﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.InterviewV2.Bo;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.InterviewV2.Entity.ErrorCode;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class InterviewV2Services : IInterviewV2Services
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
        #region Respondent services
        public WcfActionResponse InsertRespondent(RespondentEntity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (RespondentBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateRespondent(RespondentEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (RespondentBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.ID.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteRespondent(int respondentId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (RespondentBo.Delete(respondentId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = respondentId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<RespondentEntity> ListRespondentByInterView(int interviewId)
        {
            return RespondentBo.ListRespondentInterView(interviewId);
        }
        public List<RespondentEntity> ListRespondentByPagesize(int interviewId, int pageIndex, int pageSize, ref int totalRow)
        {
            return RespondentBo.ListRespondentInterView(interviewId, pageIndex, pageSize, ref totalRow);
        }
        public RespondentEntity GetRespondentById(int respondentId)
        {
            return RespondentBo.RespondentById(respondentId);
        }
        #endregion

        #region Question services
        public List<QuestionEntity> ListQuestionRespondentInterView(int interviewId, int respondentId, EnumStatusQuestion status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuestionBo.ListQuestionRespondentInterView(interviewId, respondentId, status, pageIndex, pageSize, ref totalRow);
        }
        public QuestionEntity GetQuestionById(int questionId)
        {
            return QuestionBo.GetQuestionById(questionId);
        }
        /// <summary>
        /// Trả lời cầu hỏi
        /// </summary>
        /// <param name="AnswerContent"></param>
        /// <param name="status"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public WcfActionResponse Answer(string AnswerContent, EnumStatusQuestion status, int Id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuestionBo.Answer(AnswerContent, status, Id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Thêm mới câu hỏi
        /// </summary>
        /// <param name="questionInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse InsertQuestion(QuestionEntity questionInfo, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuestionBo.InsertQuestion(questionInfo, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Cập nhật câu hỏi
        /// </summary>
        /// <param name="questionInfo"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateQuestion(QuestionEntity questionInfo)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuestionBo.UpdateQuestion(questionInfo) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = questionInfo.ID.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteQuestion(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuestionBo.DeleteQuestion(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<QuestionEntity> ListQuestionByAnswerd(int interviewId, EnumStatusQuestion status,int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuestionBo.ListQuestionByAnswerd(interviewId, status,orderBy, pageIndex, pageSize, ref totalRow);

        }
        public WcfActionResponse UpdatePriority(int interviewId, string listOfInterviewQuestionId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuestionBo.UpdatePriority(interviewId, listOfInterviewQuestionId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = interviewId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse InsertAnswer(string answerContent, int interviewId, EnumStatusQuestion status, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (QuestionBo.InsertAnswer(answerContent,interviewId,status, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Interview services
        public InterviewV2Entity SelectInterviewByNewsId(long newsid)
        {
            return InterviewBo.SelectInterviewByNewsId(newsid);
        }
        public InterviewV2Entity SelectInterviewById(int interviewId)
        {
            return InterviewBo.SelectInterviewById(interviewId);
        }
        public WcfActionResponse InsertInterview(InterviewV2Entity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InterviewBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateStatusInterview(int id, EnumStatusInterview status)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InterviewBo.UpdateStatusInterview(id, status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Log Action
        public WcfActionResponse InsertLog(int interviewId, string userName, string action, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LogActionBo.InsertLog(interviewId, userName, action, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public List<LogActionEntity> ListLog(int interviewId, int pageIndex, int pageSize, ref int totalRow)
        {
            return LogActionBo.ListLog(interviewId, pageIndex, pageSize, ref totalRow);
        }
        #endregion
    }
}
