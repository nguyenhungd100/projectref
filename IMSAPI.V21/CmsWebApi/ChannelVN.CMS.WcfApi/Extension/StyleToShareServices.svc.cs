﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class StyleToShareServices : IStyleToShareServices
    {
        #region Share Style

        public ShareStylesEntity GetShareStyleByShareStyleId(int styleId)
        {
            return StyleToShareBo.GetShareStyleByShareStyleId(styleId);
        }
        public ShareStylesDetailEntity GetShareStyleDetailByShareStyleId(int styleId)
        {
            return StyleToShareBo.GetShareStyleDetailByShareStyleId(styleId);
        }
        public List<ShareStylesEntity> SearchShareStyle(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return StyleToShareBo.SearchShareStyle(keyword, pageIndex, pageSize, ref totalRow);
        }
        public List<ShareStylesDetailEntity> SearchShareStyleDetail(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return StyleToShareBo.SearchShareStyleDetail(keyword, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse DeleteShareStyleByShareStyleId(int styleId)
        {
            var errorCode = StyleToShareBo.DeleteShareStyleByShareStyleId(styleId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int) errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ReceiveShareStyle(int styleId, long newsId)
        {
            var errorCode = StyleToShareBo.ReceiveShareStyle(styleId, newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region Style images

        public List<StyleImagesEntity> GetStyleImageByStyleId(int styleId, int top)
        {
            return StyleToShareBo.GetStyleImageByStyleId(styleId, top);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(SocialNetwork.Entity.ErrorCode.ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = SocialNetwork.Entity.ErrorCode.ErrorMapping.Current[(SocialNetwork.Entity.ErrorCode.ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
