﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.LiveTeamGame.Bo;
using ChannelVN.LiveTeamGame.Entity;
using ChannelVN.LiveTeamGame.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LiveTeamGameServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select LiveTeamGameServices.svc or LiveTeamGameServices.svc.cs at the Solution Explorer and start debugging.
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LiveTeamGameServices : ILiveTeamGameServices
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
        #region LiveTeamGame
        public WcfActionResponse InsertLiveTeamGame(LiveTeamGameEntity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTeamGameBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateLiveTeamGame(LiveTeamGameEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTeamGameBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteLiveTeamGame(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTeamGameBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<LiveTeamGameEntity> ListLiveTeamGameByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveTeamGameBo.ListLiveTeamGameByPages(search, pageIndex, pageSize, ref totalRow);
        }
        public LiveTeamGameEntity GetLiveTeamGameById(int Id)
        {
            return LiveTeamGameBo.GetLiveTeamGameById(Id);
        }
        public List<LiveTeamGameEntity> ListLiveTeamGameByMultiId(int teamA, int teamB) {
            return LiveTeamGameBo.ListLiveTeamGameByMultiId(teamA, teamB);
        }
        #endregion
    }
}
