﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.External.CafeF;
using ChannelVN.CMS.BO.External.GenK.GiftCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeF.Expert;
using ChannelVN.CMS.Entity.External.GenK.GiftCode;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GenkServices : IGenkServices
    {
        public WcfActionResponse UpdateExpertInNews(ExpertInNews expertEntity)
        {
            var errorCode = NewsAuthorBo.InsertExpertIntoNews(expertEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #region GameGifCode
        public WcfActionResponse InsertGiftCode(long newsId, string giftCode, string tags)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (GameGiftCodeBo.InsertGiftCode(newsId, giftCode, tags) ==ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = GameProfile.Entity.ErrorCode.ErrorMapping.Current[GameProfile.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)GameProfile.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)GameProfile.Entity.ErrorCode.ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = GameProfile.Entity.ErrorCode.ErrorMapping.Current[GameProfile.Entity.ErrorCode.ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public GameGiftCodeEntity GetStartGiftCodeByNewsId(long newsId)
        {
            return GameGiftCodeBo.GetGiftCodeByNewsId(newsId);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(WcfActionResponse));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
