﻿using ChannelVN.CMS.Common;
using ChannelVN.MenuExtension.Bo;
using ChannelVN.MenuExtension.Entity;
using ChannelVN.MenuExtension.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MenuExtensionServices : IMenuExtensionServices
    {
        public List<MenuExtensionEntity> MenuExtension_Search(int ZoneId, MenuExtensionStatus status, int GroupId, int ParentMenuId, bool IsTag)
        {
            return MenuExtensionBo.Search(ZoneId,  (int)status, GroupId, ParentMenuId, IsTag);
        }
        public List<MenuExtensionEntity> GetAllWithTreeView(bool getParentZoneOnly)
        {
            return MenuExtensionBo.GetAllMenuExtensionWithTreeView(getParentZoneOnly);
        }
        public MenuExtensionEntity MenuExtension_GetById(int Id)
        {
            return MenuExtensionBo.GetMenuExtensionById(Id);
        }
        public WcfActionResponse MenuExtension_Update(MenuExtensionEntity entity, string listTag)
        {
            return MenuExtensionBo.UpdateMenuExtension(entity, listTag) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
       
        public WcfActionResponse MenuExtension_Insert(MenuExtensionEntity entity, string listTag)
        {
            int MenuExtensionId = 0;
            return MenuExtensionBo.InsertMenuExtension(entity, listTag) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(MenuExtensionId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MenuExtension_InsertTag(int menuId, int GroupId, int ZoneId, string tags)
        {
            int MenuExtensionId = 0;
            return MenuExtensionBo.InsertTag(menuId, GroupId, ZoneId, tags) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(MenuExtensionId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MenuExtension_UpdateStatus(int Id, MenuExtensionStatus status)
        {
            return MenuExtensionBo.MenuExtension_UpdateStatus(Id, (int)status) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
      
        public WcfActionResponse MenuExtension_Delete(int Id)
        {
            return MenuExtensionBo.DeleteMenuExtension(Id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
    }
}
