﻿using ChannelVN.BigStory.Entity.ErrorCode;
using ChannelVN.BigStory.Bo;
using ChannelVN.BigStory.Entity;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BigStoryServices : IBigStoryServices
    {
        #region BigStory
        public List<BigStoryEntity> BigStory_Search(string keyword, string CreatedBy, BigStoryIsFocus IsFocus, BigStoryStatus status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return BigStoryBo.Search(keyword, CreatedBy, (int)IsFocus, (int)status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public BigStoryEntity BigStory_GetById(int Id)
        {
            return BigStoryBo.GetBigStoryById(Id);
        }
        public WcfActionResponse BigStory_Update(BigStoryEntity entity)
        {
            return BigStoryBo.UpdateBigStory(entity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse BigStory_UpdateAllItem(BigStoryEntity entity, List<BigStoryItemEntity> ListBigStoryItem)
        {
            return BigStoryBo.UpdateBigStory(entity, ListBigStoryItem) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse BigStory_Insert(BigStoryEntity entity)
        {
            int BigstoryId = 0;
            return BigStoryBo.InsertBigStory(entity, ref BigstoryId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(BigstoryId.ToString(),"")
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse BigStory_InsertAllItem(BigStoryEntity entity, List<BigStoryItemEntity> ListBigStoryItem)
        {
            return BigStoryBo.InsertBigStory(entity, ListBigStoryItem) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse BigStory_UpdateStatus(int Id, BigStoryStatus status)
        {
            return BigStoryBo.BigStory_UpdateStatus(Id, (int)status) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse BigStory_UpdateIsFocus(int Id, bool IsFocus)
        {
            return BigStoryBo.BigStory_UpdateIsFocus(Id, IsFocus) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        } 
        public WcfActionResponse BigStory_Delete(int Id)
        {
            return BigStoryBo.DeleteBigStory(Id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        } 
        #endregion

        #region BigStoryItem
        public List<BigStoryItemEntity> BigStoryItem_GetByBigStoryId(int BigStoryId)
        {
            return BigStoryItemBo.BigStoryItem_GetByBigStoryId(BigStoryId);
        }
        public BigStoryItemEntity BigStoryItem_GetById(int Id)
        {
            return BigStoryItemBo.BigStoryItem_GetById(Id);
        }
        public WcfActionResponse BigStoryItem_Update(BigStoryItemEntity entity)
        {
            return BigStoryItemBo.UpdateBigItemStory(entity);
        }
        public WcfActionResponse BigStoryItem_Insert(BigStoryItemEntity entity)
        {
            return BigStoryItemBo.InsertBigItemStory(entity);
        }
        public WcfActionResponse BigStoryItem_InsertByList(List<BigStoryItemEntity> list)
        {
            return BigStoryItemBo.InsertBigItemStoryByList(list);
        }
        public WcfActionResponse BigStoryItem_UpdateByList(List<BigStoryItemEntity> list, int bigStoryId)
        {
            return BigStoryItemBo.UpdateBigItemStoryByList(list, bigStoryId);
        }
        public WcfActionResponse BigStoryItem_Delete(int Id)
        {
            return BigStoryItemBo.DeleteBigStoryItem(Id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse BigStoryItem_UpdateIsHighlight(int Id, bool IsHighlight)
        {
            return BigStoryItemBo.BigStoryItem_UpdateIsHighlight(Id, IsHighlight) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse BigStoryItem_UpdateStatus(int Id, BigStoryItemStatus status)
        {
            return BigStoryItemBo.BigStoryItem_UpdateStatus(Id, (int)status) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        #endregion

        #region BigStoryFocusItem
        public List<BigStoryFocusItemEntity> BigStoryFocusItem_GetByBigStoryId(int BigStoryId)
        {
            return BigStoryFocusItemBo.BigStoryItem_GetByBigStoryId(BigStoryId);
        }        
        public WcfActionResponse BigStoryFocusItem_Update(BigStoryFocusItemEntity entity)
        {
            return BigStoryFocusItemBo.UpdateBigItemStory(entity);
        }
        public WcfActionResponse BigStoryFocusItem_Insert(BigStoryFocusItemEntity entity)
        {
            return BigStoryFocusItemBo.InsertBigItemStory(entity);
        }
        public BigStoryFocusItemEntity BigStoryFocusItem_GetById(int Id)
        {
            return BigStoryFocusItemBo.BigStoryItem_GetById(Id);
        }
        public WcfActionResponse BigStoryFocusItem_InsertByList(List<BigStoryFocusItemEntity> list)
        {
            return BigStoryFocusItemBo.InsertBigItemStoryByList(list);
        }
        public WcfActionResponse BigStoryFocusItem_UpdateByList(List<BigStoryFocusItemEntity> list, int bigStoryId)
        {
            return BigStoryFocusItemBo.UpdateBigItemStoryByList(list, bigStoryId);
        }        
        public WcfActionResponse BigStoryFocusItem_Delete(int Id)
        {
            return BigStoryFocusItemBo.DeleteBigStoryItem(Id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }        
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
