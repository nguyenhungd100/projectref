﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.TransferMarket.Bo;
using ChannelVN.TransferMarket.Entity;
using ChannelVN.TransferMarket.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TransferMarketServices : ITransferMarketServices
    {
        #region TransferMarket

        public List<TransferMarketEntity> SearchTransferMarket(string keyword, EnumTransferMarketStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return TransferMarketBo.SearchTransferMarket(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public TransferMarketEntity GetTransferMarketByTransferMarketId(long transferMarketId)
        {
            return TransferMarketBo.GetTransferMarketByTransferMarketId(transferMarketId);
        }
        public WcfActionResponse InsertTransferMarket(TransferMarketEntity transferMarket)
        {
            var errorCode = TransferMarketBo.InsertTransferMarket(transferMarket);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateTransferMarket(TransferMarketEntity transferMarket)
        {
            var errorCode = TransferMarketBo.UpdateTransferMarket(transferMarket);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteTransferMarket(long transferMarketId)
        {
            var errorCode = TransferMarketBo.DeleteTransferMarket(transferMarketId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region TransferMarketNews

        public List<TransferMarketNewsEntity> SearchTransferMarketNews(string keyword, long transferMarketId, EnumTransferMarketNewsLabelType labelType, int mode, EnumTransferMarketNewsStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return TransferMarketBo.SearchTransferMarketNews(keyword, transferMarketId, labelType, mode, status,
                                                pageIndex, pageSize, ref totalRow);
        }
        public TransferMarketNewsEntity GetTransferMarketNewsByTransferMarketNewsId(long transferMarketNewsId)
        {
            return TransferMarketBo.GetTransferMarketNewsByTransferMarketNewsId(transferMarketNewsId);
        }
        public WcfActionResponse InsertTransferMarketNews(TransferMarketNewsEntity transferMarketNews)
        {
            var newsId = 0L;
            var errorCode = TransferMarketBo.InsertTransferMarketNews(transferMarketNews, ref newsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateTransferMarketNews(TransferMarketNewsEntity transferMarketNews)
        {
            var errorCode = TransferMarketBo.UpdateTransferMarketNews(transferMarketNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(transferMarketNews.Id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteTransferMarketNews(long transferMarketNewsId)
        {
            var errorCode = TransferMarketBo.DeleteTransferMarketNews(transferMarketNewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ApproveTransferMarketNews(long transferMarketNewsId)
        {
            var errorCode = TransferMarketBo.ApproveTransferMarketNews(transferMarketNewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UnpublishTransferMarketNews(long transferMarketNewsId)
        {
            var errorCode = TransferMarketBo.UnpublishTransferMarketNews(transferMarketNewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
