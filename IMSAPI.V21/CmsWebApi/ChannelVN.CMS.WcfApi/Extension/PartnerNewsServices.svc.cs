﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.PartnerNews.Bo;
using ChannelVN.PartnerNews.Entity;
using ChannelVN.PartnerNews.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PartnerNewsServices : IPartnerNewsServices
    {
        #region Partner
        /// <summary>
        /// Thêm
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse InsertPartner(PartnerEntity info)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (PartnerBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Cập nhật 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse UpdatePartner(PartnerEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (PartnerBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeletePartner(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (PartnerBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Danh sách có phân trang
        /// </summary>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List data</returns>
        public List<PartnerEntity> SelectAllPartner(int pageIndex, int pageSize, ref int totalRow)
        {
            return PartnerBo.SelectListField(pageIndex, pageSize, ref totalRow);
        }
        public List<PartnerEntity> PartnerSelectListAllField()
        {
            return PartnerBo.SelectListAllField();
        }
        /// <summary>
        /// Lấy thông tin chi tiết
        /// </summary>
        /// <param name="id">int</param>
        /// <returns></returns>
        public PartnerEntity PartnerSelectDataById(int id)
        {
            return PartnerBo.SelectDataById(id);
        }
        #endregion

        #region PartnerNews
        /// <summary>
        /// Thêm
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse InsertPartnerNews(PartnerNewsEntity info)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (PartnerNewsBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Cập nhật 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse UpdatePartnerNews(PartnerNewsEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (PartnerNewsBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Cập nhật Status by Newsid
        /// </summary>
        /// <param name="newsid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateStatusPartnerNewsByNewsId(long newsid, int status)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (PartnerNewsBo.UpdateStatusByNewsId(newsid, status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeletePartnerNews(long id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (PartnerNewsBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Danh sách có phân trang
        /// </summary>
        /// <param name="status"></param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <param name="keyword"></param>
        /// <param name="partnerId"></param>
        /// <returns>List data</returns>
        public List<PartnerNewsEntity> SelectAllPartnerNews(string keyword, int partnerId, EnumPartnerNewsStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return PartnerNewsBo.SelectListField(keyword, partnerId, (int)status, pageIndex, pageSize, ref totalRow);
        }
        public List<PartnerNewsEntity> PartnerNewsSelectListAllField()
        {
            return PartnerNewsBo.SelectListAllField();
        }
        /// <summary>
        /// Lấy thông tin chi tiết lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="id">int</param>
        /// <returns></returns>
        public PartnerNewsEntity PartnerNewsSelectDataById(long id)
        {
            return PartnerNewsBo.SelectDataById(id);
        }
        #endregion

        #region ErrorMapping
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
        #endregion
    }
}
