﻿using System.Collections.Generic;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.Dantri.Bo;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DantriServices : IDantriServices
    {
        public List<CommentUserEntity> Search(string userName, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return CommentUserBo.Search(userName, status, pageIndex, pageSize, ref totalRow);
        }
        public CommentUserEntity GetById(long id)
        {
            return CommentUserBo.GetById(id);
        }
        public WcfActionResponse UpdateCommentUser(CommentUserEntity commentUser)
        {
            return CommentUserBo.UpdateCommentUser(commentUser) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse UpdateStatusCommentUser(CommentUserEntity commentUser)
        {
            return CommentUserBo.UpdateStatusCommentUser(commentUser) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        #region NewsRoyaltyInfo
        public NewsRoyaltyInfoEntity GetNewsRoyaltyInfoByNewsId(long id)
        {
            return NewsRoyaltyInfoBo.GetNewsRoyaltyInfoByNewsId(id);
        }
        public WcfActionResponse InsertNewsRoyaltyInfo(NewsRoyaltyInfoEntity NewsRoyaltyInfo)
        {
            return NewsRoyaltyInfoBo.InsertNewsRoyaltyInfo(NewsRoyaltyInfo) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse UpdateNewsRoyaltyInfo(NewsRoyaltyInfoEntity NewsRoyaltyInfo)
        {
            return NewsRoyaltyInfoBo.UpdateNewsRoyaltyInfo(NewsRoyaltyInfo) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteNewsRoyaltyInfoByNewsId(long tagId)
        {
            return NewsRoyaltyInfoBo.DeleteByNewsId(tagId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteNewsRoyaltyInfoById(long tagId)
        {
            return NewsRoyaltyInfoBo.DeleteById(tagId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        #endregion
        #region NewsRoyaltyAuthor
        public List<NewsRoyaltyAuthorEntity> GetNewsRoyaltyAuthorByNewsId(long id)
        {
            return NewsRoyaltyAuthorBo.GetNewsRoyaltyAuthorByNewsId(id);
        }
        public WcfActionResponse InsertNewsRoyaltyAuthor(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            return NewsRoyaltyAuthorBo.InsertNewsRoyaltyAuthor(NewsRoyaltyAuthor) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse UpdateNewsRoyaltyAuthor(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            return NewsRoyaltyAuthorBo.UpdateNewsRoyaltyAuthor(NewsRoyaltyAuthor) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteNewsRoyaltyAuthorByNewsId(long tagId)
        {
            return NewsRoyaltyAuthorBo.DeleteByNewsId(tagId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteNewsRoyaltyAuthorById(long tagId)
        {
            return NewsRoyaltyAuthorBo.DeleteById(tagId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        #endregion


        #region VideoRoyaltyInfo
        public VideoRoyaltyInfoEntity GetVideoRoyaltyInfoByVideoId(long id)
        {
            return VideoRoyaltyInfoBo.GetVideoRoyaltyInfoByVideoId(id);
        }
        public WcfActionResponse InsertVideoRoyaltyInfo(VideoRoyaltyInfoEntity VideoRoyaltyInfo)
        {
            return VideoRoyaltyInfoBo.InsertVideoRoyaltyInfo(VideoRoyaltyInfo) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse UpdateVideoRoyaltyInfo(VideoRoyaltyInfoEntity VideoRoyaltyInfo)
        {
            return VideoRoyaltyInfoBo.UpdateVideoRoyaltyInfo(VideoRoyaltyInfo) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteVideoRoyaltyInfoByVideoId(long tagId)
        {
            return VideoRoyaltyInfoBo.DeleteByVideoId(tagId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteVideoRoyaltyInfoById(long tagId)
        {
            return VideoRoyaltyInfoBo.DeleteById(tagId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        #endregion
        #region VideoRoyaltyAuthor
        public List<VideoRoyaltyAuthorEntity> GetVideoRoyaltyAuthorByVideoId(long id)
        {
            return VideoRoyaltyAuthorBo.GetVideoRoyaltyAuthorByVideoId(id);
        }
        public WcfActionResponse InsertVideoRoyaltyAuthor(VideoRoyaltyAuthorEntity VideoRoyaltyAuthor)
        {
            return VideoRoyaltyAuthorBo.InsertVideoRoyaltyAuthor(VideoRoyaltyAuthor) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse UpdateVideoRoyaltyAuthor(VideoRoyaltyAuthorEntity VideoRoyaltyAuthor)
        {
            return VideoRoyaltyAuthorBo.UpdateVideoRoyaltyAuthor(VideoRoyaltyAuthor) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteVideoRoyaltyAuthorByVideoId(long tagId)
        {
            return VideoRoyaltyAuthorBo.DeleteByVideoId(tagId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse DeleteVideoRoyaltyAuthorById(long tagId)
        {
            return VideoRoyaltyAuthorBo.DeleteById(tagId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        #endregion
    }
}
