﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CrawlTopNewsServices : ICrawlTopNewsServices
    {
        public List<CrawlTopNewsAgentEntity> GetAllCrawlTopNewsAgent()
        {
            try
            {
                return CrawlTopNewsBo.GetAllCrawlTopNewsAgent();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                if (ex.InnerException != null) Logger.WriteLog(Logger.LogType.Fatal, ex.InnerException.ToString());
                return new List<CrawlTopNewsAgentEntity>();
            }
        }

        public CrawlTopNewsEntity GetCrawlTopNewsById(int id)
        {
            try
            {
                return CrawlTopNewsBo.GetCrawlTopNewsById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                if (ex.InnerException != null) Logger.WriteLog(Logger.LogType.Fatal, ex.InnerException.ToString());
                return new CrawlTopNewsEntity();
            }
        }

        public List<CrawlTopNewsEntity> SearchCrawlTopNews(int newsAngentId, string keyword, int pageIndex, int pageSize,int category, DateTime from, DateTime to,
                                                      ref int totalRow)
        {
            try
            {
                return CrawlTopNewsBo.SearchCrawlTopNews(newsAngentId, keyword, pageIndex, pageSize, category, from, to, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                if (ex.InnerException != null) Logger.WriteLog(Logger.LogType.Fatal, ex.InnerException.ToString());
                return new List<CrawlTopNewsEntity>();
            }
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(WcfActionResponse));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public List<CrawlTopNewsCategoryEntity> GetCategoryByAgent(int id)
        {
            try
            {
                return CrawlTopNewsBo.GetCategoryByAgent(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                if (ex.InnerException != null) Logger.WriteLog(Logger.LogType.Fatal, ex.InnerException.ToString());
                return new List<CrawlTopNewsCategoryEntity>();
            }
        }
    }
}
