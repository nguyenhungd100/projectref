﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.QuestionAnswer;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.GroupQuestion;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class QuestionAnswerServices : IQuestionAnswerServices
    {
        public List<GroupQuestionEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            var news = QuestionAnswerBo.Search(keyword, pageIndex, pageSize, ref totalRow);
            return news;
        }

        public List<EventQuestionEntity> SearchEvent(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            var news = QuestionAnswerBo.SearchEvent(keyword, pageIndex, pageSize, ref totalRow);
            return news;
        }

        public List<QuestionEntity> GetListQuestionByGroup(string keyword, int groupId, int pageIndex, int pageSize, ref int totalRow)
        {
            var news = QuestionAnswerBo.GetListQuestionByGroup(keyword, groupId, pageIndex, pageSize, ref totalRow);
            return news;
        }

        public List<QuestionEntity> GetListQuestionByGroupEvent(string keyword, int groupId, int eventId, int pageIndex, int pageSize, ref int totalRow)
        {
            var news = QuestionAnswerBo.GetListQuestionByGroupEvent(keyword, groupId,eventId, pageIndex, pageSize, ref totalRow);
            return news;
        }

        public List<QuestionEntity> GetListQuestionBefore(string keyword, int groupId, int eventId, int pageIndex, int pageSize, ref int totalRow)
        {
            var news = QuestionAnswerBo.GetListQuestionBefore(keyword, groupId, eventId, pageIndex, pageSize, ref totalRow);
            return news;
        }

        public List<AnswerEntity> GetListAnswer(int questionId)
        {
            var news = QuestionAnswerBo.GetListAnswer(questionId);
            return news;
        }

        public QuestionEntity GetQuestionById(int questionId)
        {
            var news = QuestionAnswerBo.GetQuestionById(questionId);
            return news;
        }
        public GroupQuestionEntity GetGroupQuestionById(int questionId)
        {
            var news = QuestionAnswerBo.GetGroupQuestionById(questionId);
            return news;
        }
        public EventQuestionEntity GetEventQuestionById(int questionId)
        {
            var news = QuestionAnswerBo.GetEventQuestionById(questionId);
            return news;
        }

        public WcfActionResponse UpdateQuestion(QuestionEntity news)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = QuestionAnswerBo.UpdateQuestion(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse InsertQuestion(QuestionEntity news, ref int id)
        {
            try
            {
                var errorCode = QuestionAnswerBo.InsertQuestion(news, ref id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse InsertEventQuestion(EventQuestionEntity news, ref int id)
        {
            try
            {
                var errorCode = QuestionAnswerBo.InsertEventQuestion(news, ref id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse InsertGroupQuestion(GroupQuestionEntity news, ref int id)
        {
            try
            {
                var errorCode = QuestionAnswerBo.InsertGroupQuestion(news, ref id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateGroupQuestion(GroupQuestionEntity news)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = QuestionAnswerBo.UpdateGroupQuestion(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateEventQuestion(EventQuestionEntity news)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = QuestionAnswerBo.UpdateEventQuestion(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse InsertQuestionInEvent(int questionId, int eventId)
        {
            try
            {
                var errorCode = QuestionAnswerBo.InsertQuestionInEvent(questionId, eventId);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse("", "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse InsertAnswer(AnswerEntity news, ref int id)
        {
            try
            {
                var errorCode = QuestionAnswerBo.InsertAnswer(news, ref id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteQuestion(int id)
        {
            try
            {
                var errorCode = QuestionAnswerBo.DeleteQuestion(id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse DeleteGroupQuestion(int id)
        {
            try
            {
                var errorCode = QuestionAnswerBo.DeleteGroupQuestion(id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse DeleteEventQuestion(int id)
        {
            try
            {
                var errorCode = QuestionAnswerBo.DeleteEventQuestion(id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteAnswer(int id)
        {
            try
            {
                var errorCode = QuestionAnswerBo.DeleteAnswer(id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateAnswer(AnswerEntity news)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = QuestionAnswerBo.UpdateAnswer(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
    }
}
