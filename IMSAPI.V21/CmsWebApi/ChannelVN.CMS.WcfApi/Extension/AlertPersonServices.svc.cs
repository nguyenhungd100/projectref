﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.AlertPerson;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AlertPerson;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AlertPersonServices : IAlertPersonServices
    {
        public AlertPersonEntity GetAlertPersonById(int personId)
        {
            return BoFactory.GetInstance<AlertPersonBo>().GetAlertPersonById(personId);
        }
        public AlertPersonEntity[] Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoFactory.GetInstance<AlertPersonBo>().Search(keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public WcfActionResponse InsertTag(AlertPersonEntity tag)
        {
            WcfActionResponse responseData;
            int newTagId = 0;
            var errorCode = BoFactory.GetInstance<AlertPersonBo>().InsertTag(tag, ref newTagId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                tag.Id = newTagId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
            }
            else
            {
                if (newTagId > 0)
                {
                    tag.Id = newTagId;
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            return responseData;
        }
        public WcfActionResponse Update(AlertPersonEntity tag)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<AlertPersonBo>().Update(tag);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse DeleteById(int tagId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<AlertPersonBo>().DeleteById(tagId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
    }
}
