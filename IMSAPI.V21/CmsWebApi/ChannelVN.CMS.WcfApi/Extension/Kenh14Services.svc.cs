﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Bo;
using ChannelVN.Kenh14.Entity;
using ChannelVN.Kenh14.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Kenh14Services : IKenh14Services
    {
        public List<QuoteEntity> SearchQuote(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuoteBo.Search(keyword, pageIndex, pageSize, ref totalRow);
        }
        public QuoteEntity GetQuoteById(long id)
        {
            return QuoteBo.GetById(id);
        }
        public WcfActionResponse UpdateQuote(QuoteEntity Quote)
        {
            return QuoteBo.UpdateQuote(Quote) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse InsertQuote(QuoteEntity Quote)
        {
            return QuoteBo.InsertQuote(Quote) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        #region MediaAlbum

        public List<MediaAlbumEntity> SearchMediaAlbum(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MediaAlbumBo.SearchMediaAlbum(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);
        }
        public MediaAlbumForEditEntity GetMediaAlbumForEditByMediaAlbumId(int MediaAlbumId, int topPhoto)
        {
            return MediaAlbumBo.GetMediaAlbumForEditByMediaAlbumId(MediaAlbumId, topPhoto);
        }

        public WcfActionResponse CreateMediaAlbum(MediaAlbumForEditEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.CreateMediaAlbum(MediaAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateMediaAlbum(MediaAlbumForEditEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.UpdateMediaAlbum(MediaAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteMediaAlbum(int MediaAlbumId)
        {
            var errorCode = MediaAlbumBo.DeleteMediaAlbum(MediaAlbumId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteMediaAlbumDetail(int id)
        {
            var errorCode = MediaAlbumBo.DeleteMediaAlbumDetail(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public MediaAlbumDetailEntity GetMediaAlbumDetailForEditById(int mediaAlbumId)
        {
            return MediaAlbumBo.GetMediaAlbumDetailForEditById(mediaAlbumId);
        }
        public WcfActionResponse CreateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.CreateMediaAlbumDetail(MediaAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {
            var errorCode = MediaAlbumBo.UpdateMediaAlbumDetail(MediaAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region K14Embed

        public WcfActionResponse K14EmbedInsert(K14EmbedEntity k14EmbedEntity, ref int k14EmbedId)
        {
            var errorCode = K14EmbedBo.Insert(k14EmbedEntity, ref k14EmbedId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(k14EmbedId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse K14EmbedUpdate(K14EmbedEntity k14EmbedEntity)
        {
            var errorCode = K14EmbedBo.Update(k14EmbedEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(k14EmbedEntity), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse K14EmbedDelete(int id)
        {
            var errorCode = K14EmbedBo.Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public K14EmbedEntity K14EmbedGetById(int id)
        {
            return K14EmbedBo.GetById(id);
        }

        public K14EmbedEntity[] ListK14EmbedSearch(string keyword)
        {
            return K14EmbedBo.Search(keyword).ToArray();
        }

        public K14EmbedEntity[] K14EmbedGetAll()
        {
            return K14EmbedBo.GetAll().ToArray();
        }
        #endregion

        #region Sticker
        public StickerEntity[] SearchSticker(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return StickerBo.SearchSticker(keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public StickerEntity GetStickerByStickerId(long stickerId)
        {
            return StickerBo.GetStickerByStickerId(stickerId);
        }

        public WcfActionResponse InsertSticker(StickerEntity sticker)
        {
            WcfActionResponse responseData;
            int newStickerId = 0;
            var errorCode = StickerBo.InsertSticker(sticker, ref newStickerId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                sticker.Id = newStickerId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(sticker), "1");
            }
            else
            {
                if (newStickerId > 0)
                {
                    sticker.Id = newStickerId;
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(sticker), "1");
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            return responseData;
        }

        public WcfActionResponse UpdateSticker(StickerEntity sticker)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = StickerBo.Update(sticker);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(sticker));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteById(long stickerId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = StickerBo.DeleteById(stickerId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse AddNewsSticker(string newsIds, long stickerId)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = StickerBo.AddNews(newsIds, stickerId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }

        public List<StickerEntity> GetListStickerByNewsId(long newsId)
        {
            return StickerBo.GetListStickerByNewsId(newsId);
        }
        #endregion
    }
}
