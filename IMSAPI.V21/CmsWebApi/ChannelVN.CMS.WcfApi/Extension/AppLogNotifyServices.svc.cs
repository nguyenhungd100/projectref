﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.NewsNotify;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsNotify;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AppLogNotifyServices : IAppLogNotifyServices
    {
        public WcfActionResponse InsertNews(NewsNotifyEntity news)
        {
            try
            {
                var newEncryptNewsId = String.Empty;
                var newsId = 0L;
                var errorCode = NewsNotifyBo.InsertNews(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Notify_InsertNews(NewsNotifyEntity news)
        {
            try
            {
                var newEncryptNewsId = String.Empty;
                var newsId = 0L;
                var errorCode = NewsNotifyBo.Notify_InsertNews(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse InsertNewsApp(NewsAppEntity news)
        {
            try
            {
                var newsId = 0L;
                var errorCode = NewsNotifyBo.InsertNewsApp(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
    }
}
