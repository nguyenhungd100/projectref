﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.CalendarManager.Bo;
using ChannelVN.CalendarManager.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.CalendarManager.Entity.ErrorCode;
namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CalendarManagerServices : ICalendarManagerServices
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public WcfActionResponse InsertCalendar(CalendarEntity info, ref int newId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CalendarBo.InsertCalendar(info, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = newId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateCalendar(CalendarEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CalendarBo.UpdateCalendar(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateInviteStatus(string user, int status, int calendarId, string reason)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CalendarBo.UpdateInviteStatus(user, status,  calendarId, reason) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = calendarId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteCalendar(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CalendarBo.DeleteCalendar(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteCalendarInvite(int calendarId, string user)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CalendarBo.DeleteCalendarInvite(calendarId, user) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = calendarId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public CalendarEntity GetCalendarById(int id)
        {
            return CalendarBo.GetCalendarById(id);
        }
        public CalendarDetailEntity GetCalendarInviteById(int calendarId)
        {
            return CalendarBo.GetCalendarInviteById(calendarId);
        }
        public List<CalendarEntity> GetDataByUser(int month, string user, int type, int dateType)
        {
            return CalendarBo.GetDataByUser(month, user, type, dateType);
        }
        public List<CalendarEntity> GetDataByDate(DateTime date, string user, int type)
        {
            return CalendarBo.GetDataByDate(date, user, type);
        }
        public List<CalendarEntity> GetDataToNotify(DateTime inputDate, string user)
        {
            return CalendarBo.GetDataToNotify(inputDate, user);
        }
        public WcfActionResponse ProcessCalendarInput(CalendarEntity calendarInfo, List<string> inviteUser, List<string> requiredUser, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CalendarBo.ProcessCalendarInput(calendarInfo, inviteUser, requiredUser, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse InsertLog(CalendarLogEntity logInfo, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CalendarBo.InsertLog(logInfo, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateStatusEvent(int calendarId, int status) {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (CalendarBo.UpdateStatusEvent(calendarId,status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = calendarId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
    }
}

