﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.External.NguoiLaoDong.ExternalVideoEmbed;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.ExternalVideoEmbed;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ExternalVideoServices : IExternalVideoServices
    {
        public ExternalVideoEntity GetExternalVideoById(int id)
        {
            return ExternalVideoBo.GetExternalVideoById(id);
        }
        public List<ExternalVideoEntity> GetUnprocessExternalVideo(string keyword, int zoneVideoId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ExternalVideoBo.GetUnprocessExternalVideo(keyword, zoneVideoId, pageIndex, pageSize, ref totalRow);
        }
        public int GetExternalVideoCount()
        {
            return ExternalVideoBo.GetExternalVideoCount();
        }
        public WcfActionResponse RecieveExternalVideo(int videoId)
        {
            var errorCode = ExternalVideoBo.RecieveExternalVideo(videoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteExternalVideo(int videoId)
        {
            var errorCode = ExternalVideoBo.DeleteExternalVideo(videoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse Insert(ExternalVideoEmbedEntity videoEmbedEntity)
        {
            var errorCode = ExternalVideoEmbedBo.Insert(videoEmbedEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse Delete(int zoneId, int type)
        {
            var errorCode = ExternalVideoEmbedBo.Delete(zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<ExternalVideoEmbedEntity> GetListByZoneType(int zoneId, int type)
        {
            return ExternalVideoEmbedBo.GetListByZoneType(zoneId, type);
        }


        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
