﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.MC.Bo;
using ChannelVN.MC.Entity;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MCServices : IMCServices
    {
        public WcfActionResponse MCInsert(MCEntity expertEntity, ref int expertId)
        {
            var errorCode = MCBo.Insert(expertEntity, ref expertId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(expertId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public MCEntity MCGetById(int id)
        {
            return MCBo.GetById(id);
        }

        public WcfActionResponse MCUpdate(MCEntity expertEntity)
        {
            var errorCode = MCBo.Update(expertEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(expertEntity), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public MCEntity[] ListMCSearch(string keyword)
        {
            return MCBo.Search(keyword).ToArray();
        }

        public MCInNewsEntity[] MC_ListForEditNews(long NewsId)
        {
            return MCBo.ListForEditNews(NewsId).ToArray();
        }

        public WcfActionResponse MCDelete(int id)
        {
            var errorCode = MCBo.Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public MCEntity MCGetByNewsId(int id)
        {
            return MCBo.GetMCByNewsId(id);
        }

        public WcfActionResponse UpdateMCInNews(MCInNews expertEntity)
        {
            var errorCode = MCBo.InsertMCIntoNews(expertEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #region BoxMCNewsEmbed
        public List<MCEntity> GetListMCEmbed(int zoneId, int type)
        {
            return BoxMCEmbedBo.GetListMCEmbed(zoneId, type);
        }

        public WcfActionResponse InsertBoxMCNewsEmbed(BoxMCEmbedEntity newsEmbedBox)
        {
            var errorCode = BoxMCEmbedBo.Insert(newsEmbedBox);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsEmbedBox.MCId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateBoxMCNewsEmbed(string listNewsId, int zoneId, int type)
        {
            var errorCode = BoxMCEmbedBo.Update(listNewsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteBoxMCNewsEmbed(int mcId, int zoneId, int type)
        {
            var errorCode = BoxMCEmbedBo.Delete(mcId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(mcId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        public WcfActionResponse UpdateNewsInTagNews(int mcId, string deleteNewsId, string addNewsId, int newsType)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = MCBo.UpdateNewsInTagNews(mcId, deleteNewsId, addNewsId,
                                                                                   newsType);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public WcfActionResponse MCInNews_Update(string mcIds, long NewsId)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = MCBo.MCInNews_Update(mcIds, NewsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public List<MCInPhoto> GetListPhotoByMcId(int McId, int pageIndex, int pageSize, ref int totalRows)
        {
            return MCBo.GetListPhotoByMcId(McId, pageIndex, pageSize, ref totalRows);
        }
        
        public WcfActionResponse UpdatePhotoInMC(int mcId, string addPhotoId, string deletePhotoId)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = MCBo.UpdatePhotoInMC(mcId, addPhotoId, deletePhotoId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public NewsInListEntity[] SearchMCNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {
            var news = MCBo.SearchMCNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            return new ErrorMessageEntity[] { };
        }

    }
}
