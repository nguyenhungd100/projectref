﻿using ChannelVN.CafeBiz.Bo;
using ChannelVN.CafeBiz.Entity;
using ChannelVN.CafeBiz.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CafeBizServices : ICafeBizServices
    {
        #region ErrorMapping
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
        #endregion

        #region BizDocField
        /// <summary>
        /// Thêm lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse InsertBizDocField(BizDocFieldEntity info)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BizDocFieldBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Cập nhật thông tin lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateBizDocField(BizDocFieldEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BizDocFieldBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteBizDocField(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BizDocFieldBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Danh sách lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="PageIndex">int</param>
        /// <param name="PageSize">int</param>
        /// <param name="TotalRow">int</param>
        /// <returns>List data</returns>
        public List<BizDocFieldEntity> SelectAllBizDocField(int PageIndex, int PageSize, ref int TotalRow)
        {
            return BizDocFieldBo.SelectListField(PageIndex, PageSize, ref TotalRow);
        }
        public List<BizDocFieldEntity> SelectListAllField()
        {
            return BizDocFieldBo.SelectListAllField();
        }
        /// <summary>
        /// Lấy thông tin chi tiết lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="id">int</param>
        /// <returns></returns>
        public BizDocFieldEntity SelectDataById(int id)
        {
            return BizDocFieldBo.SelectDataById(id);
        }
        #endregion

        #region BizDoc
        /// <summary>
        /// Thêm văn bản pháp luật
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse InsertBizDoc(BizDocEntity info)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BizDocBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Cập nhật thông tin văn bản pháp luật
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateBizDoc(BizDocEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BizDocBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa lĩnh vực văn bản pháp luật
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteBizDoc(long id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (BizDocBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Danh sách Văn bản pháp luật
        /// </summary>
        /// <param name="PageIndex">int</param>
        /// <param name="PageSize">int</param>
        /// <param name="TotalRow">int</param>
        /// <returns>List data</returns>
        public List<BizDocEntity> SelectAllBizDoc(int PageIndex, int PageSize, ref int TotalRow, int fieldId = 0, int propId = 0)
        {
            return BizDocBo.SelectListField(PageIndex, PageSize, ref TotalRow, fieldId, propId);
        }
        /// <summary>
        /// Lấy thông tin chi tiết Văn bản pháp luật
        /// </summary>
        /// <param name="id">int</param>
        /// <returns></returns>
        public BizDocEntity SelectBizBocById(long id)
        {
            return BizDocBo.SelectDataById(id);
        }

        #endregion

        #region BizDocProperties
        public List<BizDocPropertiesEntity> SelectListAllProperties()
        {
            return BizDocPropertiesBo.SelectListAllProperties();
        }
        #endregion
    }
}
