﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.EventDaily.Bo;
using ChannelVN.EventDaily.Entity;
using ChannelVN.EventDaily.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class EventDailyServices : IEventDailyServices
    {
        public WcfActionResponse Insert(EventDailyEntity eventDailyEntity, ref int eventDailyId)
        {
            var errorCode = EventDailyBo.Insert(eventDailyEntity, ref eventDailyId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(eventDailyId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public EventDailyEntity EventDailyGetById(int id)
        {
            return EventDailyBo.GetById(id);
        }

        public List<EventDailyEntity> Search(string keyword)
        {
            return EventDailyBo.Search(keyword);
        }

        public WcfActionResponse Update(EventDailyEntity eventDailyEntity)
        {
            var errorCode = EventDailyBo.Update(eventDailyEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(eventDailyEntity), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse Delete(int id)
        {
            var errorCode = EventDailyBo.Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
    }
}
