﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.LiveGameLeague.Bo;
using ChannelVN.LiveGameLeague.Entity;
using ChannelVN.LiveGameLeague.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LiveGameLeagueServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select LiveGameLeagueServices.svc or LiveGameLeagueServices.svc.cs at the Solution Explorer and start debugging.
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LiveGameLeagueServices : ILiveGameLeagueServices
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
        #region LiveGame
        public WcfActionResponse InsertLiveGameLeague(LiveGameLeagueEntity info, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameLeagueBo.Insert(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateLiveGameLeague(LiveGameLeagueEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameLeagueBo.Update(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteLiveGameLeague(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveGameLeagueBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<LiveGameLeagueEntity> ListLiveGameLeagueByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveGameLeagueBo.ListLiveGameLeagueByPages(search, pageIndex, pageSize, ref totalRow);
        }
        public LiveGameLeagueEntity GetLiveGameLeagueById(int Id)
        {
            return LiveGameLeagueBo.GetLiveGameLeagueById(Id);
        }
        #endregion
    }
}
