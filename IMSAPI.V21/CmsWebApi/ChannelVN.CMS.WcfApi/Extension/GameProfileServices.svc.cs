﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.GameProfile.BO;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GameProfileServices : IGameProfileServices
    {
        #region GameBrand

        public GameBrandEntity[] GetAllBrand()
        {
            return GameBrandBo.GetAll().ToArray();
        }
        public GameBrandEntity[] GetListBrand(int pageIndex, int pageSize, ref int totalRow)
        {
            return GameBrandBo.GetList(pageIndex, pageSize, ref totalRow).ToArray();
        }
        public GameBrandEntity[] SearchBrand(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return GameBrandBo.Search(keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public GameBrandEntity GetBrandById(int id)
        {
            return GameBrandBo.GetBrandById(id);
        }
        public WcfActionResponse InsertBrand(GameBrandEntity gameBrandEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBrandBo.InsertBrand(gameBrandEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateBrand(GameBrandEntity gameBrandEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBrandBo.UpdateBrand(gameBrandEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteBrand(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBrandBo.DeleteBrand(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateStatusByID(long productId, byte status)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBrandBo.UpdateStatusByID(productId, status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region GameCategory

        public GameCategoryEntity[] GetAllCategory()
        {
            return GameCategoryBo.GetAll().ToArray();
        }
        public GameCategoryEntity GetCategoryById(int id)
        {
            return GameCategoryBo.GetCategoryById(id);
        }
        public WcfActionResponse InsertCategory(GameCategoryEntity gameCategoryEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameCategoryBo.InsertCategory(gameCategoryEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateCategory(GameCategoryEntity gameCategoryEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameCategoryBo.UpdateCategory(gameCategoryEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteCategory(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameCategoryBo.DeleteCategory(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateCategoryStatusByID(int newsCatId, byte status)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameCategoryBo.UpdateStatusByID(newsCatId, status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Game

        public GameViewForAllListEntity[] SearchGame(string keyword, int catId, int pageIndex, int pageSize, ref int totalRow)
        {
            return GameBo.Search(keyword, catId, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public GameEntity GetGameById(int id)
        {
            return GameBo.GetGameById(id);
        }
        public WcfActionResponse InsertGame(GameEntity gameGameEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var gameIdOut = GameBo.InsertGame(gameGameEntity);
            if (gameIdOut > 0)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = gameIdOut.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateGame(GameEntity gameGameEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBo.UpdateGame(gameGameEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteGame(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBo.DeleteGame(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateGameStatusByID(int gameId, byte status)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBo.UpdateGameStatusByID(gameId, status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateGameIsNewByID(int gameId, bool isNew)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBo.UpdateGameIsNewByID(gameId, isNew) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateGameIsHighLightByID(int gameId, bool isHighLight)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameBo.UpdateGameIsHighLightByID(gameId, isHighLight) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region GameMedia
        public GameMediaEntity[] GetListGameMediaByGameId(int gameId)
        {
            return GameMediaBo.GetListByGameId(gameId).ToArray();
        }
        public GameMediaEntity GetGameMediaById(int id)
        {
            return GameMediaBo.GetGameMediaById(id);
        }
        public int CountGameMediaInGame(int gameId, byte mediaType)
        {
            return GameMediaBo.CountGameMediaInGame(gameId, mediaType);
        }
        public WcfActionResponse UpdateMultiGameMedia(string listName, string listDesc, string listNote, string listUrl, int mediaType, int gameId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameMediaBo.UpdateMultiGameMedia(listName, listDesc, listNote, listUrl, mediaType, gameId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse InsertGameMedia(GameMediaEntity gameMediaEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameMediaBo.InsertGameMedia(gameMediaEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateGameMedia(GameMediaEntity gameMediaEntity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameMediaBo.UpdateGameMedia(gameMediaEntity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteGameMedia(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (GameMediaBo.DeleteGameMedia(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region GameRate
        public GameRateEntity GetGameRateByGameId(int gameId)
        {
            return GateGameRatiesBo.GetGameRateByGameId(gameId);
        }
        public WcfActionResponse UpdateGameRate(GameRateEntity gameRate)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (GateGameRatiesBo.UpdateGameRate(gameRate) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<GateGameRatiesEntity> GetListGameRatiesByDate(DateTime lastModifyDate, ref DateTime maxVoteDate)
        {
            return GateGameRatiesBo.GetListByDate(lastModifyDate, ref maxVoteDate);
        }
        public WcfActionResponse SyncGateGameRaties(GateGameRatiesEntity gameRate)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (GateGameRatiesBo.SyncGateGameRaties(gameRate) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region GameGifCode
        public WcfActionResponse InsertGiftCode(long newsId, string giftCode, string tags)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (GameGiftCodeBo.InsertGiftCode(newsId, giftCode, tags) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public GameGiftCodeEntity GetStartGiftCodeByNewsId(long newsId)
        {
            return GameGiftCodeBo.GetGiftCodeByNewsId(newsId);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
