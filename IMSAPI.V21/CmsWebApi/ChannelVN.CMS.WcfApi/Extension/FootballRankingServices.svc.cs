﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.FootballRanking.BO;
using ChannelVN.FootballRanking.Entity;
using ChannelVN.FootballRanking.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FootballRankingServices : IFootballRankingServices
    {

        #region FootballRanking

        public WcfActionResponse InsertFootballRanking(FootballRanking.Entity.FootballRankingEntity footballRanking)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var newId = 0;

            if (FootballRankingBo.InsertFootballRanking(footballRanking, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = newId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateFootballRanking(FootballRanking.Entity.FootballRankingEntity footballRanking)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballRankingBo.UpdateFootballRanking(footballRanking) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = footballRanking.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }


        public WcfActionResponse DeleteFootballRanking(int footballRankingId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballRankingBo.DeleteFootballRanking(footballRankingId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = footballRankingId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdateFootballRankingPriority(string listOfPriority)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballRankingBo.UpdateFootballRankingPriority(listOfPriority) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public FootballRankingEntity GetFootballRanking(int footballRankingId)
        {
            return FootballRankingBo.GetFootballRankingById(footballRankingId);
        }

        public FootballRankingEntity[] SearchFootballRanking(string keyword, EnumFootballRankingStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                FootballRankingBo.SearchFootballRanking(keyword, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion
        
        #region FootballRankingDetail

        public WcfActionResponse InsertFootballRankingDetail(FootballRankingDetailEntity footballRankingDetail)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var newId = 0;

            if (FootballRankingDetailBo.InsertFootballRankingDetail(footballRankingDetail, ref newId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = newId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateFootballRankingDetail(FootballRankingDetailEntity footballRankingDetail)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballRankingDetailBo.UpdateFootballRankingDetail(footballRankingDetail) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = footballRankingDetail.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteFootballRankingDetail(int footballRankingDetailId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FootballRankingDetailBo.DeleteFootballRankingDetail(footballRankingDetailId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = footballRankingDetailId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public FootballRankingDetailEntity GetFootballRankingDetailById(int footballRankingDetailId)
        {
            return FootballRankingDetailBo.GetFootballRankingDetailById(footballRankingDetailId);
        }

        public FootballRankingDetailEntity[] SearchFootballRankingDetail(string keyword, int FootballRankingId, EnumFootballRankingDetailStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                FootballRankingDetailBo.SearchFootballRankingDetail(keyword, FootballRankingId, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
