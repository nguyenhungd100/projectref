﻿using ChannelVN.Author.Bo;
using ChannelVN.Author.Entity;
using ChannelVN.Author.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AuthorService : IAuthorService
    {
        public List<AuthorEntity> SelectListAuthor() {
            return AuthorBo.SelectListAuthor();
        }

        //public string SelectListAuthorJson() { 
        //    var lst= AuthorBo.SelectListAuthor();
        //    DataContractJsonSerializer serializer = new DataContractJsonSerializer(lst.GetType());
        //    MemoryStream memoryStream = new MemoryStream();
        //    serializer.WriteObject(memoryStream, lst);
        //    string json = Encoding.Default.GetString(memoryStream.ToArray());
        //    return json;
        //}

        public AuthorEntity SelectAuthorById(int authorId) {
            return AuthorBo.SelectAuthorById(authorId);
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public WcfActionResponse InsertNewsAuthor(NewsAuthorEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (AuthorBo.InsertNewsAuthor(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateNewsAuthor(NewsAuthorEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (AuthorBo.UpdateNewsAuthor(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
    }
}
