﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.Newsletter.Bo;
using ChannelVN.Newsletter.Entity;
using ChannelVN.Newsletter.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsletterServices : INewsletterServices
    {
        #region NewsletterRegistration

        public List<NewsletterRegistrationEntity> GetAllNewsletterRegistrationForThread()
        {
            return NewsletterBo.GetAllNewsletterRegistrationForThread();
        }

        public List<NewsletterRegistrationEntity> GetAllNewsletterRegistrationForZone()
        {
            return NewsletterBo.GetAllNewsletterRegistrationForZone();
        }
        public NewsletterRegistrationEntity GetNewsletterRegistrationById(int newsletterId)
        {
            return NewsletterBo.GetNewsletterRegistrationById(newsletterId);
        }
        public List<NewsletterRegistrationEntity> NewsletterRegistrationSearch(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsletterBo.NewsletterRegistrationSearch(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public WcfActionResponse NewsletterRegistrationUpdateStatus(int newsletterId, int status)
        {
            var errorCode = NewsletterBo.NewsletterRegistrationUpdateStatus(newsletterId, status);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse NewsletterRegistrationDelete(int newsletterId)
        {
            var errorCode = NewsletterBo.NewsletterRegistrationDelete(newsletterId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

        }
        #endregion

        #region NewsletterMonitor

        public List<NewsletterMonitorEntity> GetUnprocessNewsletterMonitorItem()
        {
            return NewsletterBo.GetUnprocessNewsletterMonitorItem();
        }
        public WcfActionResponse UpdateProcessNewsletterMonitorItemState(string listOfProcessId)
        {
            var errorCode = NewsletterBo.UpdateProcessNewsletterMonitorItemState(listOfProcessId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region NewsLetter
        public List<NewsletterEntity> SelectData(string keyword, int pageIndex, int pageSize, ref int totalRows)
        {
            return NewsletterBo.SelectData(keyword, pageIndex, pageSize, ref totalRows);
        }
        public NewsletterEntity SelectDataById(int Id)
        {
            return NewsletterBo.SelectDataById(Id);
        }
        public List<NewsletterDetailEntity> SelectNewsletterDetailById(int newsLetterId)
        {
            return NewsletterBo.SelectNewsletterDetailById(newsLetterId);
        }
        public WcfActionResponse InsertNewsLetter(NewsletterEntity entity, List<NewsletterDetailEntity> entityDetail, ref int id)
        {
            var errorCode = NewsletterBo.InsertNewsLetter(entity, entityDetail, ref id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateNewsLetter(NewsletterEntity entity, List<NewsletterDetailEntity> entityDetail)
        {
            var errorCode = NewsletterBo.UpdateNewsLetter(entity, entityDetail);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteNewsLetter(int Id)
        {
            var errorCode = NewsletterBo.DeleteNewsLetter(Id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteAllNewsLetterDetail(int newsLetterId)
        {
            var errorCode = NewsletterBo.DeleteAllNewsLetterDetail(newsLetterId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteNewsLetterDetail(int Id)
        {
            var errorCode = NewsletterBo.DeleteNewsLetterDetail(Id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdatePriority(int newsLetterId, string listPriority)
        {
            var errorCode = NewsletterBo.UpdatePriority(newsLetterId, listPriority);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
