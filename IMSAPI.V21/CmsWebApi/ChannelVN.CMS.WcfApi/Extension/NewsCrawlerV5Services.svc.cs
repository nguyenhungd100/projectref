﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.CrawlerSource;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.CrawlerSource;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsCrawlerV5Services : INewsCrawlerV5Services
    {
        public WcfActionResponse InsertCrawlerSource(CrawlerSourceTopicWrapperEntity news)
        {
            try
            {
                var newsId = 0L;
                var errorCode = CrawlerSourceBo.InsertCrawlerSource(news);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<CrawlerSourceEntity> GetSourceByTopic(string userName, int topicId)
        {
            try
            {
                return CrawlerSourceBo.GetSourceByTopic(userName, topicId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public List<CrawlerSourceEntity> GetSourceByUser(string userName)
        {
            try
            {
                return CrawlerSourceBo.GetSourceByUser(userName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public List<CrawlerSourceTopicEntity> GetTopicByCrawlerSource(int crawlerSourceId)
        {
            try
            {
                return CrawlerSourceBo.GetTopicByCrawlerSource(crawlerSourceId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
    }
}
