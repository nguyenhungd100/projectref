﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.Weather.Bo;
using ChannelVN.Weather.Entity;
using ChannelVN.Weather.Entity.ErrorCode;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.WcfExtensions;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WeatherServices : IWeatherServices
    {

        public WeatherEntity[] GetAllWeather()
        {
            return WeatherBo.GetAllWeather().ToArray();
        }

        public WeatherEntity GetWeatherByDate(DateTime weatherDate)
        {
            return WeatherBo.GetWeatherByDate(weatherDate);
        }

        public WeatherEntity GetWeatherById(int id)
        {
            return WeatherBo.GetWeatherById(id);
        }

        public CMS.Common.WcfActionResponse UpdateWeather(Weather.Entity.WeatherEntity info)
        {
            return WeatherBo.UpdateWeather(info) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse DeleteById(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<WeatherBo>().DeleteById(id);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }


        #region External

        public ExternalWeatherEntity[] ExternalGetAllWeather()
        {
            return ChannelVN.ExternalCms.Bo.WeatherBo.GetAll().ToArray();
        }

        public ExternalWeatherEntity ExternalGetByCodeId(int id)
        {
            return ChannelVN.ExternalCms.Bo.WeatherBo.GetByCodeId(id);
        }

        #endregion
        public Common.ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
