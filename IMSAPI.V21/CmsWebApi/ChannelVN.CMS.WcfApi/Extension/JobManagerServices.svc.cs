﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.JobManager.Bo;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class JobManagerServices : IJobManagerServices
    {
        public WcfActionResponse InsertReport(JobReportEntity report, List<JobFileAttachmentEntity> jobFileAttachments)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            var newReportId = 0;
            if (JobBo.InsertReport(report, jobFileAttachments, ref newReportId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = newReportId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateNormalJob(JobEntity job, List<string> listProcessUser, List<string> listFollowUser, List<JobTargetEntity> jobTargets, List<JobFileAttachmentEntity> jobFileAttachments)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            var newJobId = 0;

            if (JobBo.UpdateNormalJob(job, listProcessUser, listFollowUser, jobTargets, jobFileAttachments, ref newJobId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = newJobId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse JobUpdateStatus(JobEntity job)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (JobBo.JobUpdateStatus(job) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = job.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateTargetJob(JobEntity job, List<string> listFollowUser, List<JobTargetEntity> jobTargets, List<JobFileAttachmentEntity> jobFileAttachments)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            var newJobId = 0;

            if (JobBo.UpdateTargetJob(job, listFollowUser, jobTargets, jobFileAttachments, ref newJobId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = newJobId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse GetUnreadComment(int jobId, string userAssigned)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (JobBo.GetUnreadComment(jobId, userAssigned) == 0)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "0";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                actionResponse.Data = JobBo.GetUnreadComment(jobId, userAssigned).ToString();
            }
            return actionResponse;
        }

        public WcfActionResponse GetHighlightJob(int jobId, string userAssigned)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (JobBo.GetHighlightJob(jobId, userAssigned) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "1";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                actionResponse.Data = "0";
            }
            return actionResponse;
        }

        public WcfActionResponse SetUnreadComment(int jobId, int jobReportId, string userAssigned, int status)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (JobBo.SetUnreadComment(jobId, jobReportId, userAssigned, status) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse HighlightJob(int jobId, string userAssigned)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (JobBo.HighlightJob(jobId, userAssigned) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteJob(int jobId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (JobBo.DeleteJob(jobId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public JobEntity GetJobByJobId(int jobId)
        {
            return JobBo.GetJobByJobId(jobId);
        }

        public JobDetailEntity GetJobDetailByJobId(int jobId)
        {
            return JobBo.GetJobDetailByJobId(jobId);
        }

        public JobDetailEntity GetJobDetailByJobReportId(int jobId, int? jobReportId = 0)
        {
            return JobBo.GetJobDetailByJobId(jobId, (int)jobReportId);
        }

        public JobDetailEntity GetJobDetailAndDiscussionByJobId(int jobId, int topParentDiscussion, int topChildDiscussion)
        {
            return JobBo.GetJobDetailAndDiscussionByJobId(jobId, topParentDiscussion, topChildDiscussion);
        }

        public List<JobEntity> SearchJob(string keyword, EnumJobType jobType, int jobParentId, string createdBy, string userAssigned, EnumJobStatus status, int zoneId, int isHightLight, int isUnreadComment, int pageIndex, int pageSize, ref int totalRow)
        {
            return JobBo.SearchJob(keyword, jobType, jobParentId, createdBy, userAssigned, status, zoneId, isHightLight, isUnreadComment, pageIndex, pageSize, ref totalRow);
        }
        public List<JobEntity> SearchJobWithDiscussion(string keyword, EnumJobType jobType, int jobParentId, string createdBy, string userAssigned, EnumJobStatus status, int zoneId, int isHightLight, int isUnreadComment, int topParentDiscussion, int topChildDiscussion, int pageIndex, int pageSize, ref int totalRow)
        {
            return JobBo.SearchJobWithDiscussion(keyword, jobType, jobParentId, createdBy, userAssigned, status,  zoneId,isHightLight, isUnreadComment, topParentDiscussion, topChildDiscussion, pageIndex, pageSize, ref totalRow);
        }
        public List<JobReportEntity> ListReport(int jobId, int parentId, ref int totalRow)
        {
            return JobBo.ListReport(jobId, parentId, ref totalRow);
        }
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        /// <summary>
        /// Gets the job unread comment by user assigned.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <param name="jobReportId">The job report id.</param>
        /// <param name="UserAssigned">The user assigned.</param>
        /// <returns></returns>
        public bool GetJobUnReadCommentByUserAssigned(int jobId, int jobReportId, string UserAssigned)
        {
            return JobBo.GetJobUnReadCommentByUserAssigned(jobId, jobReportId, UserAssigned);
        }
    }
}
