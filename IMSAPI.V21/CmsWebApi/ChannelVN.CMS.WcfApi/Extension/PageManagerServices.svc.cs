﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.PageManager.Entity;
using ChannelVN.PageManager.Entity.ErrorCode;
using ChannelVN.PageManager.Bo;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PageManagerServices : IPageManagerServices
    {
        #region Page
        public List<PageEntity> GetPageByURL(string pageUrl)
        {
            return PageBo.GetPageByURL(pageUrl);
        }
        public bool PublishPageUseModule(int pageId, string configs, string lastModifiedBy)
        {
            return PageBo.PublishPageUseModule(pageId, configs, lastModifiedBy);
        }
        public bool PublishPageUseModuleByUrl(string pageUrl, string configs, string lastModifiedBy)
        {
            return PageBo.PublishPageUseModuleByUrl(pageUrl, configs, lastModifiedBy);
        }
        #endregion

        #region PageModule
        public List<PageModuleEntity> GetPageModuleByPageId(int pageId)
        {
            return PageModuleBo.GetPageModuleByPageId(pageId);
        }
        #endregion

        #region PageModuleConfig
        public List<PageModuleConfigEntity> GetPageModuleConfigByModuleId(int moduleId)
        {
            return PageModuleConfigBo.GetPageModuleConfigByModuleId(moduleId);
        }
        #endregion

        #region PageUseModuleConfig
        public List<PageUseModuleConfigEntity> GetPageUseModuleConfigByModuleIdAndPageId(int pageId, int moduleId)
        {
            return PageUseModuleConfigBo.GetPageUseModuleConfigByModuleIdAndPageId(pageId, moduleId);
        }

        #endregion

        public WcfActionResponse SavePageModule(int pageId, List<PageUseModuleDetailEntity> allModuleInPage)
        {
            var errorCode = PageUseModuleConfigBo.SavePageModule(pageId, allModuleInPage);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public string GetPagePublishedConfig(int pageId)
        {
            return PageBo.GetPagePublishedConfig(pageId);
        }
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }


    }
}
