﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ExternalInterviewQuestionServices : IExternalInterviewQuestionServices
    {
        public InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId)
        {
            return InterviewQuestionBo.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
        }
        public List<InterviewQuestionEntity> SearchInterviewQuestion(int interviewId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return InterviewQuestionBo.SearchInterviewQuestion(interviewId, keyword, pageIndex, pageSize, ref totalRow);
        }
        public List<InterviewQuestionEntity> GetNotReceiveInterviewQuestion()
        {
            return InterviewQuestionBo.GetNotReceiveInterviewQuestion();
        }
        public int GetInterviewQuestionCount(int interviewId)
        {
            return InterviewQuestionBo.GetInterviewQuestionCount(interviewId);
        }
        public WcfActionResponse ReceiveListInterviewQuestion(string listQuestionId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (InterviewQuestionBo.ReceiveListInterviewQuestion(listQuestionId) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public WcfActionResponse ReceiveExternaQuestion(int interviewQuestionId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (InterviewQuestionBo.ReceiveExternaQuestion(interviewQuestionId) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteExternaQuestion(int interviewQuestionId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (InterviewQuestionBo.DeleteExternaQuestion(interviewQuestionId) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateExternalQuestion(InterviewQuestionEntity interviewQuestion)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (InterviewQuestionBo.UpdateExternalQuestion(interviewQuestion) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public WcfActionResponse InsertExternalQuestion(InterviewQuestionEntity interviewQuestion)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (InterviewQuestionBo.InsertExternalQuestion(interviewQuestion) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
