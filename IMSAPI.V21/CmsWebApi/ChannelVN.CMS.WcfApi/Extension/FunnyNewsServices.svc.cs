﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.FunnyNews.BO;
using ChannelVN.FunnyNews.Entity;
using ChannelVN.FunnyNews.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FunnyNewsServices : IFunnyNewsServices
    {
        #region FunnyNews

        public WcfActionResponse InsertFunnyNews(FunnyNewsEntity funnyNews, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var funnyNewsId = 0;

            if (FunnyNewsBo.InsertFunnyNews(funnyNews, tagIdList, ref funnyNewsId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = funnyNewsId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateFunnyNews(FunnyNewsEntity funnyNews, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FunnyNewsBo.UpdateFunnyNews(funnyNews, tagIdList) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = funnyNews.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdateFunnyNewsStatus(int id, EnumFunnyNewsStatus status)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FunnyNewsBo.UpdateFunnyNewsStatus(id, status) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdateFunnyNewsViewCount(int id, int viewCount)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FunnyNewsBo.UpdateFunnyNewsViewCount(id, viewCount) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteFunnyNews(int funnyNewsId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FunnyNewsBo.DeleteFunnyNews(funnyNewsId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = funnyNewsId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public FunnyNewsEntity GetFunnyNews(int funnyNewsId)
        {
            return FunnyNewsBo.GetFunnyNewsById(funnyNewsId);
        }

        public FunnyNewsEntity[] SearchFunnyNews(string keyword, int zoneId, EnumFunnyNewsMediaType mediaType, DateTime fromDate, DateTime toDate, string createdBy, string author, EnumFunnyNewsStatus status, int isHot, int pageIndex, int pageSize, ref int totalRows)
        {
            return
                FunnyNewsBo.SearchFunnyNews(keyword, zoneId, mediaType, fromDate, toDate, createdBy, author, status, isHot, pageIndex, pageSize, ref totalRows).ToArray();
        }
        public FunnyNewsCountEntity[] CountFunnyNews(string username, int zoneId)
        {
            return
                FunnyNewsBo.CountFunnyNews(username, zoneId).ToArray();
        }

        #endregion

        #region FunnyNewsTags
        public FunnyNewsTagEntity[] GetFunnyNewsTagsByFunnyNewsId(int funnyNewsId)
        {
            return
                FunnyNewsBo.GetFunnyNewsTagsByFunnyNewsId(funnyNewsId).ToArray();
        }
        #endregion

        #region ZoneUseFunnyNews

        public List<ZoneUseFunnyNewsEntity> GetAllZoneUseForFunnyNews()
        {
            return ZoneBo.GetAllZoneUseForFunnyNews();
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
