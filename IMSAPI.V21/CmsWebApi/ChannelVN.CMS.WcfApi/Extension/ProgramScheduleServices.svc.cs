﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.ProgramSchedule.Bo;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.ProgramSchedule.Entity.ErrorCode;
using System.ServiceModel.Activation;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ProgramScheduleServices : IProgramScheduleServices
    {
        #region Program channel

        public WcfActionResponse InsertProgramChannel(ProgramChannelEntity programChannel, ref int newProgramChannelId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramChannelBo.Insert(programChannel, ref newProgramChannelId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateProgramChannel(ProgramChannelEntity programChannel)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramChannelBo.Update(programChannel) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteProgramChannel(int programChannelId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramChannelBo.Delete(programChannelId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse MoveProgramChannelUp(int programChannelId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramChannelBo.MoveUp(programChannelId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse MoveProgramChannelDown(int programChannelId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramChannelBo.MoveDown(programChannelId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<ProgramChannelEntity> GetProgramChannelByStatus(EnumProgramChannelStatus status)
        {
            return ProgramChannelBo.GetByStatus(status);
        }
        public ProgramChannelEntity GetProgramChannelByProgramChannelId(int id)
        {
            return ProgramChannelBo.GetByProgramChannelId(id);
        }
        public List<ProgramChannelZoneVideoEntity> GetZoneVideo()
        {
            return ProgramChannelBo.GetZoneVideo();
        }
        #endregion

        #region Program schedule
        public WcfActionResponse InsertProgramSchedule(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.InsertProgramSchedule(programSchedule, ref newProgramScheduleId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateProgramSchedule(ProgramScheduleEntity programSchedule)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.UpdateProgramSchedule(programSchedule) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteProgramSchedule(int programScheduleId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.DeleteProgramSchedule(programScheduleId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<ProgramScheduleEntity> SearchProgramSchedule(string keyword, string date, EnumProgramScheduleStatus status, int programChannelId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ProgramScheduleBo.SearchProgramSchedule(keyword, date, status, programChannelId, pageIndex, pageSize, ref totalRow);
        }
        public ProgramScheduleEntity GetProgramScheduleByProgramScheduleId(int id)
        {
            return ProgramScheduleBo.GetProgramScheduleByProgramScheduleId(id);
        }

        public WcfActionResponse CheckExistProgramSchedule(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.CheckExistProgramSchedule(programSchedule, ref newProgramScheduleId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        #endregion

        #region Program schedule detail

        public WcfActionResponse InsertProgramScheduleDetail(ProgramScheduleDetailEntity programScheduleDetail, ref int newProgramScheduleDetailId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.InsertProgramScheduleDetail(programScheduleDetail, ref newProgramScheduleDetailId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse InsertListProgramScheduleDetail(List<ProgramScheduleDetailEntity> programScheduleDetails, string listDeleteId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            try
            {
                var currentUser = WcfMessageHeader.Current.ClientUsername;
                if (!string.IsNullOrEmpty(listDeleteId))
                {
                    ProgramScheduleBo.DeleteListProgramScheduleDetail(listDeleteId);
                }
                foreach (var programScheduleDetailEntity in programScheduleDetails)
                {
                    programScheduleDetailEntity.CreatedBy = currentUser;
                    int newScheduleId = 0;
                    if (programScheduleDetailEntity.Id != 0)
                    {
                        ProgramScheduleBo.UpdateProgramScheduleDetail(programScheduleDetailEntity);
                    }
                    else
                    {
                        ProgramScheduleBo.InsertProgramScheduleDetail(programScheduleDetailEntity,
                                                                      ref newScheduleId);
                    }
                }
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateProgramScheduleDetail(ProgramScheduleDetailEntity programScheduleDetail)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.UpdateProgramScheduleDetail(programScheduleDetail) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteProgramScheduleDetail(int programScheduleDetailId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (ProgramScheduleBo.DeleteProgramScheduleDetail(programScheduleDetailId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<ProgramScheduleDetailEntity> GetProgramScheduleDetailByProgramScheduleId(int programScheduleId, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleBo.GetProgramScheduleDetailByProgramScheduleId(programScheduleId, status);
        }
        public List<ProgramScheduleDetailEntity> ProgramScheduleDetailSearchExport(int programChannelId, DateTime fromDate, DateTime toDate, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleBo.ProgramScheduleDetailSearchExport(programChannelId, fromDate, toDate, status);
        }
        public List<ProgramScheduleDetailEntity> GetProgramScheduleDetailByScheduleDate(int programChannelId, DateTime scheduleDate, EnumProgramScheduleDetailStatus status)
        {
            return ProgramScheduleBo.GetProgramScheduleDetailByScheduleDate(programChannelId, scheduleDate, status);
        }
        public ProgramScheduleDetailEntity GetProgramScheduleDetailByProgramScheduleDetailId(int id)
        {
            return ProgramScheduleBo.GetProgramScheduleDetailByProgramScheduleDetailId(id);
        }

        #endregion

        #region Import Program Schedule
        public WcfActionResponse ImportProgramSchedule(List<ProgramChannelEntity> programChannel)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                //var zoneVideo = ProgramChannelBo.GetZoneVideo();
                //var zoneid = 0;
                var currentUser = WcfMessageHeader.Current.ClientUsername;
                foreach (var programChannelEntity in programChannel)
                {
                    //ProgramChannelEntity entity = programChannelEntity;
                    //var zoneVideobyChannelId = zoneVideo.Where(c => c.ProgramChannelId == entity.Id).ToList();
                    foreach (var programScheduleEntity in programChannelEntity.ListProgramSchedule)
                    {
                        int programScheduleId = 0;
                        programScheduleEntity.CreatedBy = currentUser;
                        ProgramScheduleBo.InsertProgramSchedule(programScheduleEntity, ref programScheduleId);
                        foreach (var programScheduleDetailEntity in programScheduleEntity.ListProgramScheduleDetail)
                        {
                            //var zoneVideoEntity = zoneVideobyChannelId.FirstOrDefault(c => c.ZoneName.Contains(programScheduleDetailEntity.Title) || programScheduleDetailEntity.Title.Contains(c.ZoneName));
                            //if (zoneVideoEntity != null)
                            //{
                            //    zoneid = zoneVideoEntity.ZoneId;
                            //}
                            programScheduleDetailEntity.ProgramScheduleId = programScheduleId;
                            programScheduleDetailEntity.CreatedBy = currentUser;
                            programScheduleDetailEntity.CreatedDate = programScheduleEntity.CreatedDate;
                            programScheduleDetailEntity.ShowOnSchedule = false;
                            //programScheduleDetailEntity.ZoneVideoId = zoneid;
                            int newScheduleId = 0;
                            ProgramScheduleBo.InsertProgramScheduleDetail(programScheduleDetailEntity,
                                                                          ref newScheduleId);
                        }
                    }
                }
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
