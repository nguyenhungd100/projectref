﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.ErrorReport.Bo;
using ChannelVN.WcfExtensions;
using ChannelVN.ErrorReport.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.ErrorReport.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ErrorReportServices : IErrorReportServices
    {

        public CMS.Common.WcfActionResponse Insert(ErrorReport.Entity.ErrorReportEntity entity, ref int id)
        {
            var errorCode = ErrorReportBo.Insert(entity, ref id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateTestStatus(int id, int testStatus, string note)
        {
            var errorCode = ErrorReportBo.UpdateTestStatus(id, testStatus, note);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public CMS.Common.WcfActionResponse UpdateProcessStatus(int id, int processStatus, string note, string processBy)
        {
            var errorCode = ErrorReportBo.UpdateProcessStatus(id, processStatus, note, processBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public CMS.Common.WcfActionResponse UpdatePriority(int id, EnumErrorReportPriority priority)
        {
            var errorCode = ErrorReportBo.UpdatePriority(id, priority);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<ErrorReport.Entity.ErrorReportEntity> Search(string keyword, string nameSpace, ErrorReport.Entity.EnumErrorReportType type, DateTime dateFrom, DateTime dateTo, ErrorReport.Entity.EnumErrorReportTestStatus testStatus, ErrorReport.Entity.EnumErrorReportProcessStatus processStatus, ErrorReport.Entity.EnumErrorReportCommonStatus commonStatus, int pageIndex, int pageSize, ref int totalRows)
        {
            return ErrorReportBo.Search(keyword, nameSpace, type, dateFrom, dateTo, testStatus, processStatus,
                commonStatus, pageIndex, pageSize, ref totalRows);
        }

        public ErrorReport.Entity.ErrorReportEntity GetById(int id)
        {
            return ErrorReportBo.GetById(id);
        }
    }
}
