﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.WcfExtensions;
using System.Globalization;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ExternalCommentServices : IExternalCommentServices
    {
        #region Comment
        public CommentEntity[] Search(string objectTitle, EnumCommentObjectType objectType, string title, long parentId, CommentStatus status, long objectId, int pageIndex, int pageSize, string zoneIds, ref int totalRow)
        {
            return CommentBo.Search(objectTitle, objectType, title, parentId, status, objectId, pageIndex, pageSize, zoneIds, ref totalRow).ToArray();
        }
        public CommentEntity GetCommentById(long id)
        {
            return CommentBo.GetCommentById(id);
        }
        public List<CommentCounterEntity> GetCommentCounterByObjectId(string lstId, int objectType)
        {
            return CommentBo.GetCommentCounterByObjectId(lstId, objectType);
        }
        public List<CommentCounterEntity> GetCommentCounterByObjectIdAndStatus(string lstId, int objectType, int status)
        {
            return CommentBo.GetCommentCounterByObjectIdAndStatus(lstId, objectType, status);
        }
        public List<CommentCounterEntity> GetCommentCounterInternalByObjectId(string lstId, int objectType)
        {
            return CommentPublishedBo.GetCommentCounterInternalByObjectId(lstId, objectType);
        }

        public WcfActionResponse ReceiveCommentById(long id, ref long commentPublishedId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentBo.ReceiveCommentById(id, ref commentPublishedId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateCommentById(CommentEntity comment)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (CommentBo.UpdateCommentById(comment) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteCommentById(long commentId, string deletedBy)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (CommentBo.DeleteCommentById(commentId, deletedBy) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public CommentEntity[] StatisticsGetComment(EnumCommentObjectType objectType, CommentStatus status, DateTime fromDate, DateTime toDate)
        {
            return CommentBo.StatisticsGetComment(objectType, status, fromDate, toDate).ToArray();
        }

        public WcfActionResponse RemoveCommentById(long id, string LastModifiedBy)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentBo.RemoveCommentById(id, LastModifiedBy) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse DeleteListComments(string commentIds, string LastModifiedBy)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentBo.DeleteListComments(commentIds, LastModifiedBy) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        //CuongNP: Add-2014/11/25
        public NewsHasNewCommentExtEntity[] GetNewsHasNewCommentExt(string objectTitle, EnumCommentObjectType objectType, CommentStatus status, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {

                return CommentBo.GetNewsHasNewCommentExt(objectTitle, objectType, status, zoneIds, pageIndex, pageSize, ref totalRow).ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new NewsHasNewCommentExtEntity[] { };
        }
        public CommentEntity[] SearchV2(string objectTitle, EnumCommentObjectType objectType, string content, long parentId, CommentStatus status, long objectId, int pageIndex, int pageSize, string zoneIds, ref int totalRow)
        {
            return CommentBo.SearchV2(objectTitle, objectType, content, parentId, status, objectId, pageIndex, pageSize, zoneIds, ref totalRow).ToArray();
        }
        //CuongNP: Add-2015/07/09
        public WcfActionResponse CommentUpdateMailForwarded(long id, string mailForwarded)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (CommentBo.UpdateMailForwarded(id, mailForwarded) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public WcfActionResponse CommentUpdateMailReplied(long id, string mailReplied)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (CommentBo.UpdateMailReplied(id, mailReplied) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        // Ngocnh - 8.1.2015
        public WcfActionResponse InsertComment(CommentEntity comment)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (CommentBo.InsertComment(comment) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        #endregion

        #region CommentPublished

        public CommentPublishedEntity[] SearchCommentPublished(string objectTitle, EnumCommentPublishedObjectType objectType, string title, long parentId, CommentPublishedStatus status, string receivedBy, long objectId, int pageIndex, int pageSize, DateTime publishDate, int TitleSearchMode, ref int totalRow)
        {
            return CommentPublishedBo.SearchCommentPublished(objectTitle, objectType, title, parentId, status, receivedBy, objectId, pageIndex, pageSize, Convert.ToDateTime(publishDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")), TitleSearchMode, ref totalRow).ToArray();
        }
        public CommentPublishedEntity GetCommentPublishedById(long id)
        {
            return CommentPublishedBo.GetCommentPublishedById(id);
        }
        public WcfActionResponse ApproveCommentPublished(long id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.ApproveCommentPublished(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse RemoveCommentPublished(long id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.RemoveCommentPublished(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UnapproveCommentPublished(long id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.UnapproveCommentPublished(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateCommentPublishedById(CommentPublishedEntity commentPublished)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (CommentPublishedBo.UpdateCommentPublishedById(commentPublished) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteCommentPublishedById(long commentPublishedId, string deletedBy)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            try
            {
                if (CommentPublishedBo.DeleteCommentPublishedById(commentPublishedId, deletedBy) == ErrorMapping.ErrorCodes.Success)
                {
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                    actionResponse.Data = "";
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                    actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                actionResponse.Success = false;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Exception;
                actionResponse.Message = ex.Message;
            }
            return actionResponse;
        }
        public CommentPublishedEntity[] StatisticsGetCommentPublished(EnumCommentPublishedObjectType objectType, CommentPublishedStatus status, DateTime fromDate, DateTime toDate)
        {
            return CommentPublishedBo.StatisticsGetCommentPublished(objectType, status, fromDate, toDate).ToArray();
        }

        public WcfActionResponse DeleteListCommentPublished(string commentIds, string LastModifiedBy)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.DeleteListCommentPublished(commentIds, LastModifiedBy) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse ApproveListCommentPublished(string commentIds, string PublishBy)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.ApproveListCommentPublished(commentIds, PublishBy) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }

            return actionResponse;
        }

        public WcfActionResponse UnApproveListCommentPublished(string commentIds, string LastModifiedBy)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.UnApproveListCommentPublished(commentIds, LastModifiedBy) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse CommentPublishedAdminInsert(CommentPublishedEntity comment)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.CommentPublishedAdminInsert(comment) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse ReApproveListCommentPublished(string commentIds, string PublishBy)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.ReApproveListCommentPublished(commentIds, PublishBy) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public CommentPublishedEntity[] GetPublishedDateNewsByNewsId(string NewsIds)
        {
            return CommentPublishedBo.GetPublishedDateNewsByNewsId(NewsIds).ToArray();
        }

        public List<CommentEntity> GetCommentsByNewsId(long newsId, long parentId)
        {
            return CommentPublishedBo.GetCommentsByNewsId(newsId, parentId);
        }

        //CuongNP: Add-2014/11/25
        public NewsHasNewCommentEntity[] GetNewsHasNewComment(string objectTitle, EnumCommentPublishedObjectType objectType, CommentPublishedStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return CommentPublishedBo.GetNewsHasNewComment(objectTitle, objectType, status, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public CommentPublishedEntity[] SearchCommentPublishedV2(string objectTitle, EnumCommentPublishedObjectType objectType, string content, long parentId, CommentPublishedStatus status, string receivedBy, long objectId, string zoneIds, int pageIndex, int pageSize, DateTime publishDate, ref int totalRow)
        {
            return CommentPublishedBo.SearchCommentPublishedV2(objectTitle, objectType, content, parentId, status, receivedBy, objectId, zoneIds, pageIndex, pageSize, Convert.ToDateTime(publishDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")), ref totalRow).ToArray();
        }
        public List<CommentPublishedCountEntity> GetCommentCount(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            var countList = new List<CommentPublishedCountEntity>();
            var commentPublishCount = CommentPublishedBo.CommentPublishedCount(fromDate, toDate, zoneIds);
            var commentCount = CommentBo.CommentCount(fromDate, toDate, zoneIds);
            var count = 0;
            for (var i = 1; i < 4; i++)
            {
                if (i == 1)//Comment chưa duyệt: bao gồm Comment ở DB ngoài với trạng thái là 1 + Comment ở DB CMS với trạng thái là 2
                {
                    var countComment = commentCount.Where(c => c.Status == 1).ToList();
                    if (countComment.Count > 0)
                    {
                        count += countComment[0].Count;
                    }
                    var countPublish = commentPublishCount.Where(c => c.Status == 2).ToList();
                    if (countPublish.Count > 0)
                    {
                        count += countPublish[0].Count;
                    }
                }
                if (i == 2)//Comment đã duyệt: là comment ở DB CMS với trạng thái là 1
                {
                    var countPublish = commentPublishCount.Where(c => c.Status == 1).ToList();
                    if (countPublish.Count > 0)
                    {
                        count += countPublish[0].Count;
                    }
                }
                if (i == 3)//Comment đã xóa: bao gồm comment ở DB ngoài với trạng thái là 3 + Comment ở DB CMS với trạng thái là 3
                {
                    var countComment = commentCount.Where(c => c.Status == 3).ToList();
                    if (countComment.Count > 0)
                    {
                        count += countComment[0].Count;
                    }
                    var countPublish = commentPublishCount.Where(c => c.Status == 3).ToList();
                    if (countPublish.Count > 0)
                    {
                        count += countPublish[0].Count;
                    }
                }
                var commnetPublishConnt = new CommentPublishedCountEntity
                {
                    Status = i,
                    Count = count
                };
                countList.Add(commnetPublishConnt);
                count = 0;
            }
            return countList;
        }

        public CommentPublishedStatisticsEntity CommentPublished_Statistics(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            return CommentPublishedBo.CommentPublished_Statistics(fromDate, toDate, zoneIds);

        }
        public List<CommentPublishedCountByZoneEntity> GetCommentCountByZone(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            var commentPublishCount = CommentPublishedBo.CommentPublishedCountByZone(fromDate, toDate, zoneIds);
            var commentCount = CommentBo.CommentCountByZone(fromDate, toDate, zoneIds);

            var publishCount = commentPublishCount.Count;
            for (var i = publishCount - 1; i >= 0; i--)
            {
                // Comment cho xuat ban
                if (commentPublishCount[i].Status == 2)
                {
                    var countComment = commentCount.Where(c => c.Status == 1 && c.ZoneId == commentPublishCount[i].ZoneId).ToList();
                    foreach (var countCommentItem in countComment)
                    {
                        commentPublishCount[i].Count += countCommentItem.Count;
                    }
                }
                // Comment da xoa
                else if (commentPublishCount[i].Status == 3)
                {
                    var countComment = commentCount.Where(c => c.Status == 3 && c.ZoneId == commentPublishCount[i].ZoneId).ToList();
                    foreach (var countCommentItem in countComment)
                    {
                        commentPublishCount[i].Count += countCommentItem.Count;
                    }
                }
            }
            return commentPublishCount;
        }
        //CuongNP: Add-2015/07/09

        public WcfActionResponse CommentPublishedUpdateMailForwarded(long id, string mailForwarded)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.UpdateMailForwarded(id, mailForwarded) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse CommentPublishedUpdateMailReplied(long id, string mailReplied)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.UpdateMailReplied(id, mailReplied) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region CommentRate

        public WcfActionResponse UpdateUnprocessCommentRate()
        {
            try
            {
                return CommentRateBo.UpdateUnprocessCommentRate()
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                                   ErrorMapping.Current[
                                                                       ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                   ex.Message);
            }
        }

        #endregion


        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }


        public List<CommentRateEntity> GetUnprocessLikeComment(DateTime lastModifieddate)
        {
            return CommentRateBo.GetUnprocessLikeComment(lastModifieddate);
        }

        public WcfActionResponse UpdateLikeCountComment(string value)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = CommentPublishedBo.UpdateLikeCountComment(value);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public List<CommentEntity> SearchObjectByUser(long userId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return CommentBo.SearchObjectByUser(userId, status, pageIndex, pageSize, ref totalRow);
        }

        public List<CommentEntity> SearchCommentByUserObject(long userId, int status, long objectId)
        {
            return CommentBo.SearchCommentByUserObject(userId, status, objectId);
        }

        public WcfActionResponse InsertCommentLogAction(CommentLogEntity commentLog)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (CommentPublishedBo.InsertCommentLogAction(commentLog) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
    }
}
