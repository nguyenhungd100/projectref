﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.InteractiveContent.Bo;
using ChannelVN.InteractiveContent.Entity;
using ChannelVN.InteractiveContent.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class InteractiveServices : IInteractiveServices
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }


        public WcfActionResponse InsertInteractiveContent(InteractiveContentEntity interactiveContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.InsertInteractiveContent(interactiveContent) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateInteractiveContent(InteractiveContentEntity interactiveContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.UpdateInteractiveContent(interactiveContent) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateInteractiveContentConfig(int id, string interactiveData, string interactiveConfig)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.UpdateInteractiveContentConfig(id, interactiveData, interactiveConfig) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse InteractiveContentPublish(int id, string interactiveData, string interactiveConfig)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.InteractiveContentPublish(id, interactiveData, interactiveConfig) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public InteractiveContentEntity GetInteractiveContentById(int id)
        {
            return InteractiveContentBo.GetInteractiveContentById(id);
        }

        public List<InteractiveContentEntity> SearchInteractiveContent(string keyword, EnumInteractiveContentType type, string createdBy, int status, int siteId,
            int pageIndex, int pageSize, ref int totalRow)
        {
            return InteractiveContentBo.SearchInteractiveContent(keyword, type, createdBy, status, siteId,
                pageIndex, pageSize, ref totalRow);
        }

        #region Component

        public WcfActionResponse InsertInteractiveComponent(InteractiveComponentEntity interactiveContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.InsertInteractiveComponent(interactiveContent) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public WcfActionResponse UpdateInteractiveComponent(InteractiveComponentEntity interactiveContent)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.UpdateInteractiveComponent(interactiveContent) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteInteractiveComponent(long id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.DeleteInteractiveComponent(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public InteractiveComponentEntity GetInteractiveComponentById(int id)
        {
            return InteractiveContentBo.GetInteractiveComponentById(id);
        }
        public List<InteractiveComponentEntity> SearchInteractiveComponent(string keyword, string createdBy,
            int pageIndex, int pageSize, ref int totalRow)
        {
            return InteractiveContentBo.SearchInteractiveComponent(keyword, createdBy,
                pageIndex, pageSize, ref totalRow);
        }
        #endregion

        #region Template

        public WcfActionResponse UpdateInteractiveTemplate(InteractiveTemplateEntity it)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.UpdateInteractiveTemplate(it) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteInteractiveTemplate(long id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveContentBo.DeleteInteractiveTemplate(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public InteractiveTemplateEntity GetInteractiveTemplateById(int id)
        {
            return InteractiveContentBo.GetInteractiveTemplateById(id);
        }
        public List<InteractiveTemplateEntity> SearchInteractiveTemplate(EnumInteractiveTemplateType type)
        {
            return InteractiveContentBo.SearchInteractiveTemplate(type);
        }
        #endregion

        #region Interactive Image Folder
        public List<InteractiveImageFolderEntity> ListInteractiveImageFolder()
        {
            return InteractiveImageBo.ListInteractiveImageFolder();
        }

        #endregion

        #region InfoGraph Image
        public List<InteractiveImageEntity> ListInteractiveImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow)
        {
            return InteractiveImageBo.ListInteractiveImage(keyword, folderId, pageIndex, pageSize, ref totalRow);
        }
        public InteractiveImageEntity SelectInteractiveImage(int id)
        {
            return InteractiveImageBo.SelectInteractiveImage(id);
        }
        public WcfActionResponse InsertInteractiveImage(InteractiveImageEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveImageBo.InsertInteractiveImage(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse UpdateInteractiveImage(InteractiveImageEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveImageBo.UpdateInteractiveImage(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteInteractiveImage(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveImageBo.DeleteInteractiveImage(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region Interactive Form Result
        public List<InteractiveFormResultDetailEntity> GetFormResultDetailByFormResultId(int formResultId)
        {
            return InteractiveFormBo.GetFormResultDetailByFormResultId(formResultId);
        }
        public WcfActionResponse UpdateForm(InteractiveFormEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveFormBo.UpdateForm(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = elm.Id;
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public List<InteractiveFormEntity> GetFormByInteractiveId(int interactiveId)
        {
            return InteractiveFormBo.GetFormByInteractiveId(interactiveId);
        }
        public List<InteractiveFormResultEntity> GetFormResultByFormResultId(string formResultId)
        {
            return InteractiveFormBo.GetFormResultByFormResultId(formResultId);
        }
        public WcfActionResponse InsertFormResult(InteractiveFormResultEntity elm, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveFormBo.InsertFormResult(elm, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse DeleteFormResult(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveFormBo.DeleteFormResult(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        #region InteractiveFormResultDetail

        public WcfActionResponse InsertFormResultDetail(InteractiveFormResultDetailEntity elm)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveFormBo.InsertFormResultDetail(elm) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        #endregion


        #region InteractiveDetail
        public WcfActionResponse UpdateInteractiveDetail(InteractiveDetailEntity interactiveDetail)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveDetailBo.UpdateInteractiveDetail(interactiveDetail) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }

        public List<InteractiveDetailForListEntity> GetByInteractiveId(int interactiveId)
        {
            return InteractiveDetailBo.GetByInteractiveId(interactiveId);
        }

        public InteractiveDetailForListEntity GetByPageId(string pageId)
        {
            return InteractiveDetailBo.GetByPageId(pageId);
        }

        public WcfActionResponse DeleteByInteractiveId(int interactiveId)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (InteractiveDetailBo.DeleteByInteractiveId(interactiveId) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
    }
}
