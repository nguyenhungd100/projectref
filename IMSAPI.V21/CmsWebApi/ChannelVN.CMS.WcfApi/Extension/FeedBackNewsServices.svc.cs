﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.ExternalCms.Bo;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FeedBackNewsServices : IFeedBackNewsServices
    {
        #region FeedBackNews

        public FeedBackNewsEntity GetFeedBackNewsById(long id)
        {
            return FeedBackNewsBo.GetFeedBackNewsById(id);
        }

        public List<FeedBackNewsWithSimpleFieldEntity> SearchFeedBackNews(string keyword, long parentId, EnumFeedBackNewsStatus status, EnumFeedBackNewsFilterDateField filterDateField,
                                                      DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize,
                                                      ref int totalRow)
        {
            return FeedBackNewsBo.SearchFeedBackNews(keyword, parentId, status, filterDateField,
                                          dateFrom, dateTo, pageIndex, pageSize,
                                          ref totalRow);
        }

        public List<FeedBackNewsWithSimpleFieldEntity> SearchFeedBackNewsHasExternalNews(int pageIndex, int pageSize, ref int totalRow)
        {
            return FeedBackNewsBo.SearchFeedBackNewsHasExternalNews(pageIndex, pageSize, ref totalRow);
        }

        public WcfActionResponse DeleteFeedBackNewsById(long id)
        {
            var errorCode = FeedBackNewsBo.DeleteFeedBackNewsById(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertFeedBackNews(FeedBackNewsEntity feedBackNews)
        {
            var errorCode = FeedBackNewsBo.InsertFeedBackNews(feedBackNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateFeedBackNews(FeedBackNewsEntity feedBackNews)
        {
            var errorCode = FeedBackNewsBo.UpdateFeedBackNews(feedBackNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse PublishFeedBack(long id, string publishedBy)
        {
            var errorCode = FeedBackNewsBo.PublishFeedBack(id, publishedBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UnpublishFeedBack(long id)
        {
            var errorCode = FeedBackNewsBo.UnpublishFeedBack(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region ExternalFeedBackNews

        public ExternalFeedBackNewsEntity GetExternalFeedBackNewsById(long id)
        {
            return ExternalFeedBackNewsBo.GetExternalFeedBackNewsById(id);
        }

        public List<ExternalFeedBackNewsWithSimpleFieldEntity> SearchExternalFeedBackNews(string keyword, long parentId,
                                                      DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize,
                                                      ref int totalRow)
        {
            return ExternalFeedBackNewsBo.SearchExternalFeedBackNews(keyword, parentId,
                                                      dateFrom, dateTo, pageIndex, pageSize,
                                                      ref totalRow);
        }

        public WcfActionResponse DeleteExternalFeedBackNewsById(long id)
        {
            var errorCode = ExternalFeedBackNewsBo.DeleteExternalFeedBackNewsById(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ReceiveExternalFeedBackNews(long id)
        {
            var errorCode = ExternalFeedBackNewsBo.ReceiveExternalFeedBackNews(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<ExFeedBackNewsEntity> SearchNews(string keyword, int status, int pageIndex, int pageSize,
                                                      ref int totalRow)
        {
            return FeedBackNewsBo.SearchNews(keyword, status, pageIndex, pageSize, ref totalRow);
        }

        public ExFeedBackNewsEntity GetNewsById(long id)
        {
            return FeedBackNewsBo.GetNewsById(id);
        }

        public WcfActionResponse UpdateStatusIms(long newsId, int status, long feedbackId) {
            var errorCode = FeedBackNewsBo.UpdateStatusIms(newsId, status,feedbackId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(WcfActionResponse));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
