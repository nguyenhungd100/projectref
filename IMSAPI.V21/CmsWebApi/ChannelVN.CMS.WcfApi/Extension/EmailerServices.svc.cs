﻿using System;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.Emailer.Bo;
using ChannelVN.Emailer.Entity;
using ChannelVN.Emailer.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class EmailerServices : IEmailerServices
    {
        #region Email

        public WcfActionResponse EmailerInsert(EmailEntity email)
        {
            return EmailBo.Insert(email) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int) ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse EmailerUpdate(EmailEntity email)
        {
            return EmailBo.Update(email) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public EmailEntity EmailerGetById(string email)
        {
            return EmailBo.GetById(email);
        }

        public EmailEntity[] EmailerGetList(string keyword, int status, int groupId, int pageIndex, int pageSize, ref int totalRow)
        {
            return EmailBo.GetList(keyword, pageIndex, pageSize, status, groupId, ref totalRow).ToArray();
        }

        #endregion

        #region Email Group

        public WcfActionResponse EmailerGroupInsert(EmailGroupEntity emailGroupEntity)
        {
            return EmailGroupBo.Insert(emailGroupEntity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse EmailerGroupUpdate(EmailGroupEntity emailGroupEntity)
        {
            return EmailGroupBo.Update(emailGroupEntity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public EmailGroupEntity EmailerGroupGetById(int id)
        {
            return EmailGroupBo.GetById(id);
        }

        public EmailGroupEntity[] EmailerGroupGetList(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return EmailGroupBo.GetList(keyword, pageIndex, pageSize, status, ref totalRow).ToArray();
        }

        #endregion

        #region Email Queue

        public WcfActionResponse EmailerQueueInsert(EmailQueueEntity emailQueueEntity)
        {
            return EmailQueueBo.Insert(emailQueueEntity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse EmailerQueueUpdateStatus(long newsId)
        {
            return EmailQueueBo.UpdateStatus(newsId) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse EmailerQueueUpdateStatusById(int id,int status)
        {
            return EmailQueueBo.UpdateStatusById(id,status) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public EmailQueueEntity[] EmailerQueueGetListTop(int top)
        {
            return EmailQueueBo.GetListTop(top).ToArray();
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
