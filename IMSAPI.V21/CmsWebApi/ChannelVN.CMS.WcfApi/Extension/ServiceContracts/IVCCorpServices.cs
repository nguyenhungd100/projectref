﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.VCCorp.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IVCCorpServices" in both code and config file together.
    [ServiceContract]
    public interface IVCCorpServices
    {
        [OperationContract]
        List<CurriculumVitaeEntity> GetList(string keyword, int type, int status, int pageIndex, int pageSize,
                                            ref int totalRow);

        [OperationContract]
        WcfActionResponse UpdateStatus(CurriculumVitaeEntity info);

        [OperationContract]
        CurriculumVitaeEntity GetById(int Id);
    }
}
