﻿using System;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.LiveTvSchedule.Entity;
using System.Collections.Generic;
using System.ServiceModel;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IVtvServices" in both code and config file together.
    [ServiceContract]
    public interface ILiveTvScheduleServices : IErrorMessageMapping
    {
        [OperationContract]
        List<LiveTvScheduleEntity> GetList(string keyword, int scheduleType, int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<LiveTvScheduleEntity> GetLiveTvScheduleByScheduleDate(DateTime scheduleDate, int programChannelId);
        [OperationContract]
        WcfActionResponse Insert(LiveTvScheduleEntity entity, ref int id);
        [OperationContract]
        WcfActionResponse Update(LiveTvScheduleEntity entity);
        [OperationContract]
        LiveTvScheduleEntity GetById(int id);
        [OperationContract]
        WcfActionResponse Delete(int id);
    }
}
