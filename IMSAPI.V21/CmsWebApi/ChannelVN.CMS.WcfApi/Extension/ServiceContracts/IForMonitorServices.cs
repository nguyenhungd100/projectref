﻿using ChannelVN.CMS.WcfApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IForMonitorServices" in both code and config file together.
    [ServiceContract]
    public interface IForMonitorServices : IErrorMessageMapping
    {
        [OperationContract]
        void DoWork();
    }
}
