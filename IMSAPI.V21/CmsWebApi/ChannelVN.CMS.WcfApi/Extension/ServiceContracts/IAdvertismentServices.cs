﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.Advertisment.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;


namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface IAdvertismentServices : IErrorMessageMapping
    {
        [OperationContract]
        AdvertismentEntity[] GetAllAdvertisment();
        [OperationContract]
        AdvertismentEntity[] GetAdvertismentByPosition(EnumAdvertismentType typeId, int zoneId, int displayStyle, EnumAdvertismentStatus status, int positionId);
        [OperationContract]
        AdvertismentDetailEntity GetAdvertismentById(int id);
        [OperationContract]
        WcfActionResponse UpdateAdvertisment(AdvertismentEntity info, string listZones);
        [OperationContract]
        WcfActionResponse DeleteById(int id);
        [OperationContract]
        WcfActionResponse UpdateAdvertismentStatus(int id, EnumAdvertismentStatus status);

    }
}
