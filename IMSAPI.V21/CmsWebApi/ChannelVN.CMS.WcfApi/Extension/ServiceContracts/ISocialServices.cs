﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.SocialNetwork.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface ISocialServices : IErrorMessageMapping
    {
        #region Discussion

        [OperationContract]
        WcfActionResponse CreateDiscussionTopic(DiscussionTopicEntity discussionTopic, string[] discussionTopicTags, DiscussionAttachmentEntity[] discussionAttachments);
        [OperationContract]
        WcfActionResponse UpdateDiscussionTopic(DiscussionTopicEntity discussionTopic, string[] discussionTopicTags, DiscussionAttachmentEntity[] discussionAttachments);
        [OperationContract]
        WcfActionResponse FollowDiscussionTopic(long discussionTopicId);
        [OperationContract]
        WcfActionResponse UnfollowDiscussionTopic(long discussionTopicId);
        [OperationContract]
        WcfActionResponse UpdateDiscussionTopicPriority(string listDiscussionTopicId);
        [OperationContract]
        WcfActionResponse UpdateDiscussionPriority(string listDiscussionId);
        [OperationContract]
        WcfActionResponse ResolveDiscussionTopic(long discussionTopicId, string userDoAction);
        [OperationContract]
        WcfActionResponse DeleteDiscussionTopic(long discussionTopicId, string userDoAction);
        [OperationContract]
        WcfActionResponse SendDiscussion(DiscussionEntity discussion, string[] discussionTags, DiscussionAttachmentEntity[] discussionAttachments);
        [OperationContract]
        DiscussionTopicEntity[] GetDiscussionTopicByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long parentId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status, int isFocus, bool isFilterByPermission, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        DiscussionEntity[] GetDiscussionByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        DiscussionWithChildEntity[] GetDiscussionByDiscussionTopicId(long discussionTopicId, long parentDiscussionId, string userDoAction, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        DiscussionTopicDetailEntity[] GetDiscussionTopicDetailByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long parentId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status, int isFocus, bool isFilterByPermission, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        DiscussionDetailEntity[] GetDiscussionDetailByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        DiscussionTopicDetailEntity GetDiscussionTopicDetailByDiscussionTopicId(long discussionTopicId, string userDoAction);
        [OperationContract]
        DiscussionDetailEntity GetDiscussionDetailByDiscussionId(long discussionTopicId, string userDoAction);
        [OperationContract]
        int CountDiscussionTopicByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction, string keyword, int zoneId, EnumTopicStatus status);
        [OperationContract]
        int CountDiscussionByApplicationIdAndObjectId(EnumTopicApplicationId applicationId, long objectId, string userDoAction);
        [OperationContract]
        int CountDiscussionByTopicId(long topicId);
        [OperationContract]
        string[] GetListUserInDiscussionTopic(long discussionTopicId, string userDoAction);
        [OperationContract]
        List<DiscussionInTopicEntity> GetTopDiscussionTopicWithTopDiscussion(int topDiscussionTopic, int topDiscussion, long objectId, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds);
        //[OperationContract]
        //List<DiscussionInTopicEntity> GetTopDiscussionTopicAndDiscussionForNews(int topDiscussion, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds);
        
        [OperationContract]
        List<DiscussionTopicEntity> GetTopTopicByApplicationIdAndObjectId(int topTopic, long objectId, string userDoAction, EnumTopicStatus status, params EnumTopicApplicationId[] applicationIds);

        #endregion

        #region ActivityForNews

        [OperationContract]
        ActivityEntity[] GetTopActivityForNews(int top, NewsActionType newsActionType);
        [OperationContract]
        ActivityEntity[] GetTopActivityForNewsByMultiAction(int top, NewsActionType[] newsActionTypes);
        [OperationContract]
        ActivityEntity[] SearchActivityForNews(int pageIndex, int pageSize, long applicationId, NewsActionType newsActionType, DateTime dateFrom, DateTime dateTo, ref int totalRows);

        [OperationContract]
        ActivityEntity[] SearchActivity(int pageIndex, int pageSize, long applicationId, NewsActionType newsActionTypes, EnumActivityType type, DateTime dateFrom, DateTime dateTo, ref int totalRows);

        #endregion

        #region Cms Discussion

        #region For NewsCrawler

        [OperationContract]
        List<CmsDiscussionEntity> GetTopDiscussionForNewsCrawlerByUrl(string url, int topParentDiscussion,
                                                                      int topChildDiscussion,
                                                                      ref int totalParentDiscussion);

        [OperationContract]
        WcfActionResponse SendDiscussionForNewsCrawler(string url, long parentDiscussionId, string discussionContent);

        #endregion

        #region For NewsComment

        [OperationContract]
        List<CmsDiscussionEntity> GetTopDiscussionForNewsComment(long newsId, int topParentDiscussion,
                                                                      int topChildDiscussion,
                                                                      ref int totalParentDiscussion);

        [OperationContract]
        WcfActionResponse SendDiscussionForNewsComment(long newsId, long parentDiscussionId, string discussionContent);

        #endregion

        #region For JobManager

        [OperationContract]
        List<CmsDiscussionEntity> GetTopDiscussionForJobComment(int jobId, int topParentDiscussion,
                                                                      int topChildDiscussion,
                                                                      ref int totalParentDiscussion);

        [OperationContract]
        WcfActionResponse SendDiscussionForJobComment(int jobId, long parentDiscussionId, string discussionContent);

        #endregion

        #region For CalendarManager

        [OperationContract]
        List<CmsDiscussionEntity> GetTopDiscussionForCalendarComment(int calendarId, int topParentDiscussion,
                                                                      int topChildDiscussion,
                                                                      ref int totalParentDiscussion);

        [OperationContract]
        WcfActionResponse SendDiscussionForCalendarComment(int calendarId, long parentDiscussionId, string discussionContent);

        #endregion

        #endregion
    }
}
