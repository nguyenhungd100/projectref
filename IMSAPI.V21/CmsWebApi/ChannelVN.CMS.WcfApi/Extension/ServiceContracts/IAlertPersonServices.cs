﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AlertPerson;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAlertPersonServices" in both code and config file together.
    [ServiceContract]
    public interface IAlertPersonServices
    {
        [OperationContract]
        WcfActionResponse InsertTag(AlertPersonEntity tag);
        [OperationContract]
        WcfActionResponse Update(AlertPersonEntity tag);
        [OperationContract]
        AlertPersonEntity GetAlertPersonById(int personId);
        [OperationContract]
        AlertPersonEntity[] Search(string keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse DeleteById(int tagId);
    }
}
