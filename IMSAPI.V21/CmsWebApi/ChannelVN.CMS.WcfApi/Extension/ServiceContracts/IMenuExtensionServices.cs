﻿using ChannelVN.CMS.Common;
using ChannelVN.MenuExtension.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMenuExtensionServices" in both code and config file together.
    [ServiceContract]
    public interface IMenuExtensionServices
    {
        [OperationContract]
        List<MenuExtensionEntity> MenuExtension_Search(int ZoneId, MenuExtensionStatus status, int GroupId, int ParentMenuId, bool IsTag);
        [OperationContract]
        List<MenuExtensionEntity> GetAllWithTreeView(bool getParentZoneOnly);

        [OperationContract]
        MenuExtensionEntity MenuExtension_GetById(int Id);
        [OperationContract]
        WcfActionResponse MenuExtension_Update(MenuExtensionEntity entity, string listTag);

        [OperationContract]
        WcfActionResponse MenuExtension_Insert(MenuExtensionEntity entity, string listTag);

        [OperationContract]
        WcfActionResponse MenuExtension_UpdateStatus(int Id, MenuExtensionStatus status);

        [OperationContract]
        WcfActionResponse MenuExtension_Delete(int Id);

        [OperationContract]
        WcfActionResponse MenuExtension_InsertTag(int menuId, int GroupId, int ZoneId, string tags);
    }
}
