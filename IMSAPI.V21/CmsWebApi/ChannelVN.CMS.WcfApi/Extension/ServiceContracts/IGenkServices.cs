﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.CafeF.Expert;
using ChannelVN.CMS.Entity.External.GenK.GiftCode;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGenkServices" in both code and config file together.
    [ServiceContract]
    public interface IGenkServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse UpdateExpertInNews(ExpertInNews expertEntity);

        #region GameGiftCode

        [OperationContract]
        WcfActionResponse InsertGiftCode(long newsId, string giftCode, string tags);
        [OperationContract]
        GameGiftCodeEntity GetStartGiftCodeByNewsId(long newsId);

        #endregion
    }
}
