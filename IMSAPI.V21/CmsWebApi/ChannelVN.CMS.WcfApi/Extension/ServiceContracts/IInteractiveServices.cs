﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.InteractiveContent.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IInteractiveServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertInteractiveContent(InteractiveContentEntity interactiveContent);



        [OperationContract]
        WcfActionResponse UpdateInteractiveContent(InteractiveContentEntity interactiveContent);

        [OperationContract]
        WcfActionResponse UpdateInteractiveContentConfig(int id, string interactiveData, string interactiveConfig);

        [OperationContract]
        WcfActionResponse InteractiveContentPublish(int id, string interactiveData, string interactiveConfig);

        [OperationContract]
        InteractiveContentEntity GetInteractiveContentById(int id);

        [OperationContract]
        List<InteractiveContentEntity> SearchInteractiveContent(string keyword, EnumInteractiveContentType type, string createdBy, int status, int siteId,
            int pageIndex, int pageSize, ref int totalRow);

        #region Component
        [OperationContract]
        WcfActionResponse InsertInteractiveComponent(InteractiveComponentEntity interactiveContent);

        [OperationContract]
        WcfActionResponse UpdateInteractiveComponent(InteractiveComponentEntity interactiveContent);

        [OperationContract]
        WcfActionResponse DeleteInteractiveComponent(long id);

        [OperationContract]
        InteractiveComponentEntity GetInteractiveComponentById(int id);

        [OperationContract]
        List<InteractiveComponentEntity> SearchInteractiveComponent(string keyword, string createdBy,
            int pageIndex, int pageSize, ref int totalRow);
        #endregion

        #region Template

        [OperationContract]
        WcfActionResponse UpdateInteractiveTemplate(InteractiveTemplateEntity it);

        [OperationContract]
        WcfActionResponse DeleteInteractiveTemplate(long id);

        [OperationContract]
        InteractiveTemplateEntity GetInteractiveTemplateById(int id);

        [OperationContract]
        List<InteractiveTemplateEntity> SearchInteractiveTemplate(EnumInteractiveTemplateType type);
        #endregion

        #region Interactive Image Folder
        [OperationContract]
        List<InteractiveImageFolderEntity> ListInteractiveImageFolder();
        #endregion

        #region Interactive Image

        [OperationContract]
        List<InteractiveImageEntity> ListInteractiveImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        InteractiveImageEntity SelectInteractiveImage(int id);
        [OperationContract]
        WcfActionResponse InsertInteractiveImage(InteractiveImageEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse UpdateInteractiveImage(InteractiveImageEntity elm);
        [OperationContract]
        WcfActionResponse DeleteInteractiveImage(int id);
        #endregion

        #region Interactive Form Result

        [OperationContract]
        List<InteractiveFormEntity> GetFormByInteractiveId(int interactiveId);
        [OperationContract]
        WcfActionResponse InsertFormResult(InteractiveFormResultEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse DeleteFormResult(int id);

        #endregion

        #region IEFormResult
        [OperationContract]
        WcfActionResponse UpdateForm(InteractiveFormEntity elm);

        [OperationContract]
        WcfActionResponse InsertFormResultDetail(InteractiveFormResultDetailEntity elm);

        [OperationContract]
        List<InteractiveFormResultEntity> GetFormResultByFormResultId(string formResultId);

        [OperationContract]
        List<InteractiveFormResultDetailEntity> GetFormResultDetailByFormResultId(int interactiveId);
        #endregion

        #region InteractiveDetail
        [OperationContract]
        WcfActionResponse UpdateInteractiveDetail(InteractiveDetailEntity interactiveDetail);

        [OperationContract]
        List<InteractiveDetailForListEntity> GetByInteractiveId(int interactiveId);


        [OperationContract]
        InteractiveDetailForListEntity GetByPageId(string pageId);

        [OperationContract]
        WcfActionResponse DeleteByInteractiveId(int interactiveId);

        #endregion
    }
}
