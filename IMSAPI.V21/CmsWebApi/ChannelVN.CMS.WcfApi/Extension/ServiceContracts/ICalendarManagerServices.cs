﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CalendarManager.Entity;
using ChannelVN.CMS.Common;
using System;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface ICalendarManagerServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertCalendar(CalendarEntity info, ref int newId);
        [OperationContract]
        WcfActionResponse UpdateCalendar(CalendarEntity info);
        [OperationContract]
        WcfActionResponse DeleteCalendar(int id);
        [OperationContract]
        WcfActionResponse DeleteCalendarInvite(int calendarId, string user);
        [OperationContract]
        WcfActionResponse UpdateInviteStatus(string user, int status, int calendarId, string reason);
        [OperationContract]
        CalendarEntity GetCalendarById(int id);
        [OperationContract]
        CalendarDetailEntity GetCalendarInviteById(int calendarId);
        [OperationContract]
        List<CalendarEntity> GetDataByUser(int month, string user, int type, int dateType);
        [OperationContract]
        List<CalendarEntity> GetDataByDate(DateTime date, string user, int type);
        [OperationContract]
        List<CalendarEntity> GetDataToNotify(DateTime inputDate, string user);
        [OperationContract]
        WcfActionResponse ProcessCalendarInput(CalendarEntity calendarInfo, List<string> inviteUser, List<string> requiredUser, ref int id);
        [OperationContract]
        WcfActionResponse InsertLog(CalendarLogEntity logInfo, ref int id);
        [OperationContract]
        WcfActionResponse UpdateStatusEvent(int calendarId, int status);
    }
}