﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsNotify;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAppLogNotifyServices" in both code and config file together.
    [ServiceContract]
    public interface IAppLogNotifyServices
    {
        [OperationContract]
        WcfActionResponse InsertNews(NewsNotifyEntity news);
        [OperationContract]
        WcfActionResponse InsertNewsApp(NewsAppEntity news);

        [OperationContract]
        WcfActionResponse Notify_InsertNews(NewsNotifyEntity news);
    }
}
