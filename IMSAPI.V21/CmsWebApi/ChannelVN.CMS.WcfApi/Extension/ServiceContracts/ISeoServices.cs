﻿using ChannelVN.CMS.Common;
using System.ServiceModel;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.SEO.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface ISeoServices : IErrorMessageMapping
    {
        #region Seo Meta News

        [OperationContract]
        WcfActionResponse SEOMetaNewsInsert(SEOMetaNewsEntity seoMetaNewsEntity);

        [OperationContract]
        WcfActionResponse SEOMetaNewsUpdate(SEOMetaNewsEntity seoMetaNewsEntity);

        [OperationContract]
        SEOMetaNewsEntity GetSeoMetaNewsById(long id);
        #endregion

        #region SEO Meta Tag

        [OperationContract]
        WcfActionResponse SEOMetaTagsInsert(SEOMetaTagsEntity seoMetaTagsEntity);

        [OperationContract]
        WcfActionResponse SEOMetaTagsUpdate(SEOMetaTagsEntity seoMetaTagsEntity);

        [OperationContract]
        WcfActionResponse SEOMetaTagsDelete(int id);

        [OperationContract]
        SEOMetaTagsEntity GetSeoMetaTagsById(long id);

        [OperationContract]
        SEOMetaTagsEntity[] GetListSeoMetaTags(string keyWord, int zoneId, int pageIndex, int pageSize,int totalRow);

        #endregion

        #region Seo Meta Video

        [OperationContract]
        WcfActionResponse SEOMetaVideoInsert(SEOMetaVideoEntity seoMetaVideoEntity);

        [OperationContract]
        WcfActionResponse SEOMetaVideoUpdate(SEOMetaVideoEntity seoMetaVideoEntity);

        [OperationContract]
        SEOMetaVideoEntity GetSeoMetaVideoById(int id);
        #endregion
    }
}
