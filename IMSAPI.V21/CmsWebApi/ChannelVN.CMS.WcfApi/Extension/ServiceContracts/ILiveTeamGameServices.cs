﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.LiveTeamGame.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface ILiveTeamGameServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertLiveTeamGame(LiveTeamGameEntity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateLiveTeamGame(LiveTeamGameEntity info);
        [OperationContract]
        WcfActionResponse DeleteLiveTeamGame(int id);
        [OperationContract]
        List<LiveTeamGameEntity> ListLiveTeamGameByPages(string search, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        LiveTeamGameEntity GetLiveTeamGameById(int Id);
        [OperationContract]
        List<LiveTeamGameEntity> ListLiveTeamGameByMultiId(int teamA, int teamB);
    }
}
