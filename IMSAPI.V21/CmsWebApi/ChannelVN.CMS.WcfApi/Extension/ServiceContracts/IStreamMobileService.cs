﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IStreamMobileService : IErrorMessageMapping
    {
        #region News
        [OperationContract]
        void UpdateOffTime_NewsMobileStream(int TotalSize, string fromDate, string toDate);

   

        #endregion
    }
}
