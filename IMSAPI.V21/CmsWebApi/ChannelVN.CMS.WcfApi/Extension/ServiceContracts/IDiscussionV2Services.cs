﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.DiscussionV2.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDiscussionV2Services" in both code and config file together.
    [ServiceContract]
    public interface IDiscussionV2Services : IErrorMessageMapping
    {
        #region Topic
        /// <summary>
        /// Lấy danh sách topic/tin bài thảo luận
        /// </summary>
        /// <param name="userName">string</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List data</returns>
        [OperationContract]
        List<ViewDiscussionV2Entity> GetListDiscussionTopic(string keyword, int type, bool status, string userName, int pageIndex, int pageSize, ref int totalRow);
        
        /// <summary>
        /// Thêm mới một topic
        /// </summary>
        /// <param name="info">Entity</param>
        /// <param name="id">out put</param>
        /// <returns>true/false</returns>
        [OperationContract]
        WcfActionResponse InsertTopic(DiscussionTopicV2Entity info, List<DiscussionAttachV2Entity> listAttach, ref int id);
        /// <summary>
        /// Xóa topic
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="username">string</param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse DeleteTopic(int id,string username);
        [OperationContract]
        WcfActionResponse SetStatusTopic(int id);
        /// <summary>
        /// Thêm user follow theo topic
        /// </summary>
        /// <param name="DiscussionTopicId">int</param>
        /// <param name="listUserFollow">danh sách user cách nhau bởi dấu [;] vd(a;b;c)</param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse InsertFollowTopic(int DiscussionTopicId, string listUserFollow);
        /// <summary>
        /// Cập nhật trạng thái follow cho user
        /// </summary>
        /// <param name="DiscussionTopicId">int</param>
        /// <param name="UserFollow">string</param>
        /// <param name="isFollow">bool</param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse UpdateIsFollowTopic(int DiscussionTopicId, string UserFollow, bool isFollow);
        [OperationContract]
        DiscussionTopicV2Entity GetTopicByNewsId(Int64 newsId);
        [OperationContract]
        DiscussionTopicV2Entity GetTopicById(int topicId);
        #endregion

        #region Discussion
        [OperationContract]
        WcfActionResponse InsertDiscussion(DiscussionV2Entity info, List<DiscussionAttachV2Entity> listAttach, ref int id);
        /// <summary>
        /// Xóa một discussion
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse DeleteDiscussion(int id, string userAssigned);
        /// <summary>
        /// Lấy danh sách các thảo luận theo topic
        /// </summary>
        /// <param name="userName">string</param>
        /// <param name="DiscustionTopicId">int</param>
        /// <returns>entity</returns>
        [OperationContract]
        ViewDiscussionByTopicEntity GetListDiscussionByTopic(string userName, int DiscustionTopicId);
        /// <summary>
        /// Lấy danh sách disucssion chưa đọc theo userAssigned dùng cho dashboard
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="DiscustionTopicId"></param>
        /// <returns></returns>
        [OperationContract]
        List<ViewDiscussionV2Entity> GetListDiscussionUnreadForDashboard(string userName,ref int totalRow);
        #endregion

        #region DiscussionAssign
        [OperationContract]
        WcfActionResponse InsertMultiUser(int DiscussionTopicId, int DiscussionId, string ListToUser, string ListCCUser);
        [OperationContract]
        WcfActionResponse InsertOnUser(DiscussionAssignV2Entity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateIsRead(int DiscussionTopicId, string UserAssigned);
        #endregion

        #region DiscussionAttach
        [OperationContract]
        WcfActionResponse InsertAttach(DiscussionAttachV2Entity info, ref int id);
        [OperationContract]
        List<DiscussionAttachV2Entity> ListAttachByDiscussionId(int discussionId);
        #endregion
    }
}
