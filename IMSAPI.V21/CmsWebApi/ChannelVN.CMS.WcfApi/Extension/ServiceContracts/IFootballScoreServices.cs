﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.FootballScore.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IFootballScoreServices : IErrorMessageMapping
    {
        #region FootballScore

        [OperationContract]
        WcfActionResponse InsertFootballScore(FootballScoreEntity footballScore);
        [OperationContract]
        WcfActionResponse UpdateFootballScore(FootballScoreEntity footballScore);
        [OperationContract]
        WcfActionResponse DeleteFootballScore(int footballScoreId);
        [OperationContract]
        WcfActionResponse UpdateFootballScorePriority(string listOfPriority);
        [OperationContract]
        FootballScoreEntity GetFootballScore(int footballScoreId);
        [OperationContract]
        FootballScoreEntity[] SearchFootballScore(string keyword, EnumFootballScoreStatus status, int pageIndex, int pageSize, ref int totalRow);


        #endregion

        #region LiveSportTV

        [OperationContract]
        WcfActionResponse InsertLiveSportTV(LiveSportTVEntity liveSportTV);
        [OperationContract]
        WcfActionResponse UpdateLiveSportTV(LiveSportTVEntity liveSportTV);
        [OperationContract]
        WcfActionResponse DeleteLiveSportTV(int liveSportTVId);
        [OperationContract]
        LiveSportTVEntity GetLiveSportTV(int liveSportTVId);
        [OperationContract]
        LiveSportTVEntity[] SearchLiveSportTV(string keyword, EnumLiveSportTVStatus status, int pageIndex, int pageSize, ref int totalRow);


        #endregion

        #region SeaGameRanking

        [OperationContract]
        List<FootballScoreServices.SeaGameRankingEntity> GetAllSeaGameRanking();
        [OperationContract]
        FootballScoreServices.SeaGameRankingEntity GetSeaGameRankingById(long id);
        [OperationContract]
        WcfActionResponse AddNewsRanking(FootballScoreServices.SeaGameRankingEntity seaGameRanking);
        [OperationContract]
        WcfActionResponse UpdateNewsRanking(FootballScoreServices.SeaGameRankingEntity seaGameRanking);
        [OperationContract]
        WcfActionResponse DeleteNewsRanking(long id);

        #endregion

        #region FootballScoreDetail

        [OperationContract]
        WcfActionResponse InsertFootballScoreDetail(FootballScoreDetailEntity footballScore);
        [OperationContract]
        WcfActionResponse UpdateFootballScoreDetail(FootballScoreDetailEntity footballScore);
        [OperationContract]
        WcfActionResponse DeleteFootballScoreDetail(int footballScoreId);
        [OperationContract]
        FootballScoreDetailEntity GetFootballScoreDetailById(int footballScoreId);
        [OperationContract]
        FootballScoreDetailEntity[] SearchFootballScoreDetail(string keyword, int FootballScoreId, EnumFootballScoreDetailStatus status, int pageIndex, int pageSize, ref int totalRow);


        #endregion
    }
}
