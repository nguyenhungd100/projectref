﻿using ChannelVN.CMS.Common;
using ChannelVN.Author.Entity;
using ChannelVN.CMS.WcfApi.Common;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAuthorService" in both code and config file together.
    [ServiceContract]
    interface IAuthorService : IErrorMessageMapping
    {
        [OperationContract]
        List<AuthorEntity> SelectListAuthor();
        [OperationContract]
        AuthorEntity SelectAuthorById(int authorId);
        [OperationContract]
        WcfActionResponse InsertNewsAuthor(NewsAuthorEntity info);
        [OperationContract]
        WcfActionResponse UpdateNewsAuthor(NewsAuthorEntity info);
        //[OperationContract]
        //[WebGet(ResponseFormat = WebMessageFormat.Json)]
        //string SelectListAuthorJson();
    }
}
