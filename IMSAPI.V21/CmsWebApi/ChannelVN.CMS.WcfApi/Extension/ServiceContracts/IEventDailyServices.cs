﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.EventDaily.Entity;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEventDailyServices" in both code and config file together.
    [ServiceContract]
    public interface IEventDailyServices
    {
        [OperationContract]
        WcfActionResponse Insert(EventDailyEntity eventDailyEntity, ref int eventDailyId);

        [OperationContract]
        EventDailyEntity EventDailyGetById(int id);

        [OperationContract]
        List<EventDailyEntity> Search(string keyword);

        [OperationContract]
        WcfActionResponse Update(EventDailyEntity eventDailyEntity);

        [OperationContract]
        WcfActionResponse Delete(int id);
    }
}
