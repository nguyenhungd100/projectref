﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Common;
using ChannelVN.SurveyManager.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface ISurveyManagerServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse Insert(SurveyDetailEntity surveyDetailEntity, int zoneId, string zoneIdList);
        [OperationContract]
        WcfActionResponse Update(SurveyDetailEntity surveyDetailEntity, int zoneId, string zoneIdList);
        [OperationContract]
        SurveyDetailEntity GetInfo(int surveyId);
        [OperationContract]
        List<SurveyEntity> GetList(string keyword, int zoneId, EnumSurveyType type, int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse Delete(int surveyId);
        [OperationContract]
        WcfActionResponse DeleteQuestion(string listId);
        [OperationContract]
        WcfActionResponse DeleteAnswers(string listId);

    }
}
