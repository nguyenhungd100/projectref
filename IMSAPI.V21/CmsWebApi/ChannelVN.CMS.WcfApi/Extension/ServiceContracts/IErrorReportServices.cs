﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ErrorReport.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IErrorReportServices" in both code and config file together.
    [ServiceContract]
    public interface IErrorReportServices
    {
        [OperationContract]
        WcfActionResponse Insert(ErrorReportEntity entity, ref int id);

        [OperationContract]
        WcfActionResponse UpdateTestStatus(int id, int testStatus, string note);

        [OperationContract]
        WcfActionResponse UpdateProcessStatus(int id, int processStatus, string note, string processBy);

        [OperationContract]
        WcfActionResponse UpdatePriority(int id, EnumErrorReportPriority priority);

        [OperationContract]
        List<ErrorReportEntity> Search(string keyword, string nameSpace, EnumErrorReportType type, DateTime dateFrom,
            DateTime dateTo, EnumErrorReportTestStatus testStatus, EnumErrorReportProcessStatus processStatus,
            EnumErrorReportCommonStatus commonStatus, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        ErrorReportEntity GetById(int id);
    }
}
