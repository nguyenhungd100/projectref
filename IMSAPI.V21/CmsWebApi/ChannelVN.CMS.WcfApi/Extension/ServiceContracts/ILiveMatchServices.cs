﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.LiveMatch.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface ILiveMatchServices : IErrorMessageMapping
    {
        #region LiveMatch

        [OperationContract]
        List<LiveMatchEntity> SearchLiveMatch(string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        List<LiveMatchEntity> SearchLiveMatchForSuggestion(string keyword, int top);
        [OperationContract]
        LiveMatchEntity GetLiveMatchByLiveMatchId(int liveMatchId);
        [OperationContract]
        LiveMatchDetailEntity GetLiveMatchDetail(int liveMatchId);
        [OperationContract]
        WcfActionResponse InsertLiveMatch(LiveMatchEntity liveMatch);
        [OperationContract]
        WcfActionResponse InsertLiveMatchV2(LiveMatchEntity liveMatch,ref int Id);
        [OperationContract]
        WcfActionResponse UpdateLiveMatch(LiveMatchEntity liveMatch);
        [OperationContract]
        WcfActionResponse DeleteLiveMatch(int liveMatchId);

        #endregion

        #region LiveMatchTimeline

        [OperationContract]
        List<LiveMatchTimelineEntity> GetLiveMatchTimelineByLiveMatchId(int liveMatchId);
        [OperationContract]
        LiveMatchTimelineEntity GetLiveMatchTimelineById(int liveMatchTimelineId);
        [OperationContract]
        WcfActionResponse InsertLiveMatchTimeline(LiveMatchTimelineEntity liveMatchTimeline, ref int timelineId);
        [OperationContract]
        WcfActionResponse UpdateLiveMatchTimeline(LiveMatchTimelineEntity liveMatchTimeline);
        [OperationContract]
        WcfActionResponse DeleteLiveMatchTimeline(int liveMatchTimelineId);

        #endregion

        #region LiveMatchEvent

        [OperationContract]
        List<LiveMatchEventEntity> GetLiveMatchEventByLiveMatchId(int liveMatchId);
        [OperationContract]
        LiveMatchEventEntity GetLiveMatchEventById(int liveMatchEventId);
        [OperationContract]
        WcfActionResponse InsertLiveMatchEvent(LiveMatchEventEntity liveMatchEvent);
        [OperationContract]
        WcfActionResponse UpdateLiveMatchEvent(LiveMatchEventEntity liveMatchEvent);
        [OperationContract]
        WcfActionResponse DeleteLiveMatchEvent(int liveMatchEventId);

        #endregion

        #region LiveMatchPenalty

        [OperationContract]
        List<LiveMatchPenaltyEntity> GetLiveMatchPenaltyByLiveMatchId(int liveMatchId);
        [OperationContract]
        LiveMatchPenaltyEntity GetLiveMatchPenaltyById(int liveMatchPenaltyId);
        [OperationContract]
        WcfActionResponse InsertLiveMatchPenalty(LiveMatchPenaltyEntity liveMatchPenalty);
        [OperationContract]
        WcfActionResponse UpdateLiveMatchPenalty(LiveMatchPenaltyEntity liveMatchPenalty);
        [OperationContract]
        WcfActionResponse DeleteLiveMatchPenalty(int liveMatchPenaltyId);

        #endregion
    }
}
