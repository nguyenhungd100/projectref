﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Expert.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IExpertServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse ExpertInsert(ExpertEntity expertEntity, ref int expertId);

        [OperationContract]
        ExpertEntity ExpertGetById(int id);

        [OperationContract]
        WcfActionResponse ExpertUpdate(ExpertEntity expertEntity);

        [OperationContract]
        ExpertEntity[] ListExpertSearch(string keyword);

        [OperationContract]
        WcfActionResponse ExpertDelete(int id);

        [OperationContract]
        ExpertEntity ExpertGetByNewsId(int id);

        [OperationContract]
        WcfActionResponse UpdateExpertInNews(ExpertInNews expertEntity);

        [OperationContract]
        List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type);

        [OperationContract]
        WcfActionResponse InsertBoxExpertNewsEmbed(BoxExpertNewsEmbedEntity newsEmbedBox);

        [OperationContract]
        WcfActionResponse UpdateBoxExpertNewsEmbed(string listNewsId, int zoneId, int type);

        [OperationContract]
        WcfActionResponse DeleteBoxExpertNewsEmbed(long newsId, int zoneId, int type);

        [OperationContract]
        NewsInListEntity[] SearchExpertNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition,
                                                          DateTime distributedDateFrom, DateTime distributedDateTo,
                                                          int pageIndex, int pageSize, string excludeNewsIds,
                                                          int newsType, ref int totalRows);

        [OperationContract]
        List<ExpertInZone> ExpertGetByExpertId(int id);

        [OperationContract]
        WcfActionResponse UpdateExpertInZone(ExpertInZone expertEntity);

        [OperationContract]
        WcfActionResponse DeleteExpertZone(int expertId);

        [OperationContract]
        ExpertInNewsEntity[] ExpertInNews_GetByExpertId(int expertId, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        WcfActionResponse ExpertInNews_UpdateNews(int expertId, string deleteNewsId, string addNewsId);
    }
}
