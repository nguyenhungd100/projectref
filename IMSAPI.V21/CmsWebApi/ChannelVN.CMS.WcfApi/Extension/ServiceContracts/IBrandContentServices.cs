﻿using ChannelVN.BrandContent.Entity;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBrandContentServices" in both code and config file together.
    [ServiceContract]
    public interface IBrandContentServices
    {
        [OperationContract]
        WcfActionResponse BrandContent_Insert(BrandContentEntity BrandContent);

        [OperationContract]
        WcfActionResponse BrandContent_Update(BrandContentEntity BrandContent);

        [OperationContract]
        BrandContentEntity[] BrandContent_Search(string Keyword, int pageIndex, int pageSize, ref int TotalRow);

        [OperationContract]
        BrandContentEntity BrandContent_GetById(int BrandId);

        [OperationContract]
        WcfActionResponse BrandInNews_Insert(string brandIds, long NewsId);

        [OperationContract]
        WcfActionResponse BrandContent_Delete(int id);
        [OperationContract]
        BrandContentEntity[] BrandInNews_GetByNewsId(long NewsId);

    }
}
