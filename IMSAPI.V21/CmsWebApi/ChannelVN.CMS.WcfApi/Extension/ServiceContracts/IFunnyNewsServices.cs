﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.FunnyNews.Entity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IFunnyNewsServices : IErrorMessageMapping
    {
        #region FunnyNews

        [OperationContract]
        WcfActionResponse InsertFunnyNews(FunnyNewsEntity photo, string tagIdList);
        [OperationContract]
        WcfActionResponse UpdateFunnyNews(FunnyNewsEntity photo, string tagIdList);
        [OperationContract]
        WcfActionResponse UpdateFunnyNewsStatus(int id, EnumFunnyNewsStatus status);
        [OperationContract]
        WcfActionResponse UpdateFunnyNewsViewCount(int id, int viewCount);
        [OperationContract]
        WcfActionResponse DeleteFunnyNews(int photoId);
        [OperationContract]
        FunnyNewsEntity GetFunnyNews(int photoId);
        [OperationContract]
        FunnyNewsEntity[] SearchFunnyNews(string keyword, int zoneId, EnumFunnyNewsMediaType mediaType, DateTime fromDate, DateTime toDate, string createdBy, string author, EnumFunnyNewsStatus status, int isHot, int pageIndex, int pageSize, ref int totalRows);
        [OperationContract]
        FunnyNewsCountEntity[] CountFunnyNews(string username, int zoneId);

        #endregion
        #region FunnyNewsTag

        [OperationContract]
        FunnyNewsTagEntity[] GetFunnyNewsTagsByFunnyNewsId(int funnyNewsId);

        #endregion

        #region ZoneUseFunnyNews

        [OperationContract]
        List<ZoneUseFunnyNewsEntity> GetAllZoneUseForFunnyNews();

        #endregion
    }
}
