﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface ICrawlTopNewsServices : IErrorMessageMapping
    {
        [OperationContract]
        List<CrawlTopNewsAgentEntity> GetAllCrawlTopNewsAgent();

        [OperationContract]
        CrawlTopNewsEntity GetCrawlTopNewsById(int id);

        [OperationContract]
        List<CrawlTopNewsEntity> SearchCrawlTopNews(int newsAngentId, string keyword, int pageIndex, int pageSize, int category, DateTime from, DateTime to, ref int totalRow);

        [OperationContract]
        List<CrawlTopNewsCategoryEntity> GetCategoryByAgent(int id);
    }
}
