﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IExternalInterviewQuestionServices : IErrorMessageMapping
    {
        [OperationContract]
        InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId);

        [OperationContract]
        List<InterviewQuestionEntity> SearchInterviewQuestion(int interviewId, string keyword, int pageIndex,
                                                              int pageSize, ref int totalRow);

        [OperationContract]
        List<InterviewQuestionEntity> GetNotReceiveInterviewQuestion();
        [OperationContract]
        int GetInterviewQuestionCount(int interviewId);

        [OperationContract]
        WcfActionResponse ReceiveExternaQuestion(int interviewQuestionId);

        [OperationContract]
        WcfActionResponse ReceiveListInterviewQuestion(string listQuestionId);
        [OperationContract]
        WcfActionResponse DeleteExternaQuestion(int interviewQuestionId);

        [OperationContract]
        WcfActionResponse UpdateExternalQuestion(InterviewQuestionEntity interviewQuestion);

        [OperationContract]
        WcfActionResponse InsertExternalQuestion(InterviewQuestionEntity interviewQuestion);
    }
}
