﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ServiceContract]
    public interface IQuickNewsServices : IErrorMessageMapping
    {
        [OperationContract]
        QuickNewsEntity ExternalGetQuickNewsByQuickNewsId(long id);

        [OperationContract]
        QuickNewsDetailEntity ExternalGetQuickNewsDetailByQuickNewsId(long id);

        [OperationContract]
        WcfActionResponse ExternalReceiveQuickNews(long id, long newsId, string receivedBy);

        [OperationContract]
        WcfActionResponse ExternalDeleteQuickNews(long id, string deletedBy);

        [OperationContract]
        WcfActionResponse ExternalDeleteMultiQuickNews(string idList, string deletedBy);

        [OperationContract]
        List<QuickNewsEntity> ExternalSearchQuickNews(string keyword, int type, int status, int pageIndex,
                                                      int pageSize, ref int totalRow);

        [OperationContract]
        QuickNewsAuthorEntity ExternalGetQuickNewsAuthorById(long vietId);

        [OperationContract]
        QuickNewsAuthorEntity ExternalGetQuickNewsAuthorByEmail(string email);

        [OperationContract]
        List<QuickNewsAuthorEntity> ExternalSearchQuickNewsAuthor(string keyword, string email, int status,
                                                                  int pageIndex,
                                                                  int pageSize, ref int totalRow);

        [OperationContract]
        int ExternalCountQuickNews(int type, int status);

        [OperationContract]
        List<QuickNewsImagesEntity> ExternalGetQuickNewsImagesByQuickNewsId(long quickNewsId);

        [OperationContract]
        WcfActionResponse InsertQuickNews(QuickNewsEntity quickNews);

        [OperationContract]
        WcfActionResponse InsertQuickNewsAuthor(QuickNewsAuthorEntity quickNewsAuthor);

        [OperationContract]
        WcfActionResponse InsertQuickNewsImage(QuickNewsImagesEntity quickNewsImage);

        [OperationContract]
        QuickNewsEntity GetQuickNewsDetailByQuickNewsId(long id);

    }
}
