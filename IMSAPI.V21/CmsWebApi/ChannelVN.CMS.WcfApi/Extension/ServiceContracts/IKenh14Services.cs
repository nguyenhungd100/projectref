﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IKenh14Services" in both code and config file together.
    [ServiceContract]
    public interface IKenh14Services
    {
        [OperationContract]
        List<QuoteEntity> SearchQuote(string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        QuoteEntity GetQuoteById(long id);

        [OperationContract]
        WcfActionResponse UpdateQuote(QuoteEntity Quote);

        [OperationContract]
        WcfActionResponse InsertQuote(QuoteEntity Quote);

        [OperationContract]
        List<MediaAlbumEntity> SearchMediaAlbum(string keyword, int zoneId, int status, int pageIndex,
                                                int pageSize, ref int totalRow);

        [OperationContract]
        MediaAlbumForEditEntity GetMediaAlbumForEditByMediaAlbumId(int MediaAlbumId, int topPhoto);

        [OperationContract]
        WcfActionResponse CreateMediaAlbum(MediaAlbumForEditEntity MediaAlbumForEdit);

        [OperationContract]
        WcfActionResponse UpdateMediaAlbum(MediaAlbumForEditEntity MediaAlbumForEdit);

        [OperationContract]
        WcfActionResponse DeleteMediaAlbum(int MediaAlbumId);

        [OperationContract]
        WcfActionResponse DeleteMediaAlbumDetail(int id);

        [OperationContract]
        MediaAlbumDetailEntity GetMediaAlbumDetailForEditById(int mediaAlbumId);

        [OperationContract]
        WcfActionResponse CreateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit);

        [OperationContract]
        WcfActionResponse UpdateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit);

        [OperationContract]
        WcfActionResponse K14EmbedInsert(K14EmbedEntity k14EmbedEntity, ref int k14EmbedId);

        [OperationContract]
        WcfActionResponse K14EmbedUpdate(K14EmbedEntity k14EmbedEntity);

        [OperationContract]
        WcfActionResponse K14EmbedDelete(int id);

        [OperationContract]
        K14EmbedEntity K14EmbedGetById(int id);

        [OperationContract]
        K14EmbedEntity[] ListK14EmbedSearch(string keyword);

        [OperationContract]
        K14EmbedEntity[] K14EmbedGetAll();

        #region Sticker

        [OperationContract]
        StickerEntity[] SearchSticker(string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        StickerEntity GetStickerByStickerId(long stickerId);

        [OperationContract]
        WcfActionResponse InsertSticker(StickerEntity sticker);

        [OperationContract]
        WcfActionResponse UpdateSticker(StickerEntity sticker);

        [OperationContract]
        WcfActionResponse DeleteById(long stickerId);

        [OperationContract]
        WcfActionResponse AddNewsSticker(string newsIds, long stickerId);

        [OperationContract]
        List<StickerEntity> GetListStickerByNewsId(long newsId);

        #endregion
    }
}
