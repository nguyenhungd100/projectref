﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.InfoGraph.Entity;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IInfoGraphServices" in both code and config file together.
    [ServiceContract]
    public interface IInfoGraphServices : IErrorMessageMapping
    {

        #region Info Graph
        [OperationContract]
        List<InfoGraphEntity> ListInfoGraph(string keyword, string createdBy,int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        InfoGraphEntity SelectInfoGraph(int id);
        [OperationContract]
        WcfActionResponse InsertInfoGraph(InfoGraphEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse UpdateInfoGraph(InfoGraphEntity elm);
        [OperationContract]
        WcfActionResponse DeleteInfoGraph(int id);
        #endregion

        #region Info Graph Chart
       
        [OperationContract]
        InfoGraphChartEntity SelectInfoGraphChart(string name, int type);
        [OperationContract]
        WcfActionResponse InsertInfoGraphChart(InfoGraphChartEntity elm, ref int id);        
        #endregion

        #region Info Graph Folder
        [OperationContract]
        List<InfoGraphFolderEntity> ListInfoGraphFolder(string keyword, string createdBy, int visible, int pub,int type, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertInfoGraphFolder(InfoGraphFolderEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse UpdateInfoGraphFolder(InfoGraphFolderEntity elm);
        [OperationContract]
        WcfActionResponse DeleteInfoGraphFolder(int id);
        [OperationContract]
        WcfActionResponse InsertInfoGraphUsage(int folderId, string username, ref int id);
        [OperationContract]
        WcfActionResponse DeleteInfoGraphUsage(int folderId, string username);
        #endregion

        #region Info Graph Image

        [OperationContract]
        List<InfoGraphImageEntity> ListInfoGraphImage(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        InfoGraphImageEntity SelectInfoGraphImage(int id);
        [OperationContract]
        WcfActionResponse InsertInfoGraphImage(InfoGraphImageEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse UpdateInfoGraphImage(InfoGraphImageEntity elm);
        [OperationContract]
        WcfActionResponse DeleteInfoGraphImage(int id);
        #endregion

        #region Info Graph Svg
        [OperationContract]
        List<InfoGraphSvgEntity> ListInfoGraphSvg(string keyword, int folderId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        InfoGraphSvgEntity SelectInfoGraphSvg(int id);
        [OperationContract]
        List<InfoGraphSvgFolderEntity> SvgFolder(bool showAll, string channelName, string keyWord);
        [OperationContract]
        WcfActionResponse UpdateSvg(InfoGraphSvgEntity elm);
        [OperationContract]
        WcfActionResponse DeleteSvg(int id);
        [OperationContract]
        WcfActionResponse InsertSvgChannel(string channelName, int svgFolderId);
        [OperationContract]
        WcfActionResponse DeleteSvgChannel(string channelName);
        #endregion

        #region Info Graph Template
        [OperationContract]
        List<InfoGraphTemplateEntity> ListInfoGraphTemplate(string keyword, int folderId, EnumInfoGraphTemplateType type, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        InfoGraphTemplateEntity SelectInfoGraphTemplate(int id);
        [OperationContract]
        WcfActionResponse InsertInfoGraphTemplate(InfoGraphTemplateEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse UpdateInfoGraphTemplate(InfoGraphTemplateEntity elm);
        [OperationContract]
        WcfActionResponse DeleteInfoGraphTemplate(int id);
        [OperationContract]
        WcfActionResponse InsertTemplateChannel(InfoGraphTemplateChannelEntity elm);
        [OperationContract]
        WcfActionResponse UpdateTemplateChannel(InfoGraphTemplateChannelEntity elm);
        [OperationContract]
        List<InfoGraphTemplateChannelEntity> ListTemplateChannel(int status);
        [OperationContract]
        InfoGraphTemplateChannelEntity SelectTemplateChannel(int id);
        #endregion
    }
}
