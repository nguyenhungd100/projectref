﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.CMS.Common;
using System;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface IJobManagerServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertReport(JobReportEntity report, List<JobFileAttachmentEntity> jobFileAttachments);
        [OperationContract]
        WcfActionResponse UpdateNormalJob(JobEntity job, List<string> listProcessUser, List<string> listFollowUser, List<JobTargetEntity> jobTargets, List<JobFileAttachmentEntity> jobFileAttachments);
        [OperationContract]
        WcfActionResponse UpdateTargetJob(JobEntity job, List<string> listFollowUser, List<JobTargetEntity> jobTargets, List<JobFileAttachmentEntity> jobFileAttachments);
        [OperationContract]
        WcfActionResponse JobUpdateStatus(JobEntity job);
        [OperationContract]
        WcfActionResponse GetUnreadComment(int jobId, string userAssigned);
        [OperationContract]
        WcfActionResponse SetUnreadComment(int jobId,int jobReportId, string userAssigned, int status);
        [OperationContract]
        WcfActionResponse GetHighlightJob(int jobId, string userAssigned);
        [OperationContract]
        WcfActionResponse HighlightJob(int jobId, string userAssigned);
        [OperationContract]
        WcfActionResponse DeleteJob(int jobId);
        [OperationContract]
        JobEntity GetJobByJobId(int jobId);
        [OperationContract]
        JobDetailEntity GetJobDetailByJobId(int jobId);
        [OperationContract]
        JobDetailEntity GetJobDetailByJobReportId(int jobId, int? jobReportId = 0);
        [OperationContract]
        JobDetailEntity GetJobDetailAndDiscussionByJobId(int jobId, int topParentDiscussion, int topChildDiscussion);
        [OperationContract]
        List<JobReportEntity> ListReport(int jobId,int parentId, ref int totalRow);
        [OperationContract]
        List<JobEntity> SearchJob(string keyword, EnumJobType jobType, int jobParentId, string createdBy, string userAssigned, EnumJobStatus status, int zoneId, int isHightLight, int isUnreadComment, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<JobEntity> SearchJobWithDiscussion(string keyword, EnumJobType jobType, int jobParentId, string createdBy, string userAssigned, EnumJobStatus status, int zoneId, int isHightLight, int isUnreadComment, int topParentDiscussion, int topChildDiscussion, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        bool GetJobUnReadCommentByUserAssigned(int jobId, int jobReportId, string UserAssigned);
    }
}
