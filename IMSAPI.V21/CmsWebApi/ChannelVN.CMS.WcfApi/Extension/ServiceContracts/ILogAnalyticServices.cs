﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.LogAnalytics;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILogAnalyticServices" in both code and config file together.
    [ServiceContract]
    public interface ILogAnalyticServices
    {
        [OperationContract]
        WcfActionResponse UpdateLog(LogAnalyticsEntity log);
    }
}
