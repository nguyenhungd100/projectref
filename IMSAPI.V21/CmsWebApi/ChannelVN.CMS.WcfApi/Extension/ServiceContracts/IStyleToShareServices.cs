﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ServiceContract]
    public interface IStyleToShareServices : IErrorMessageMapping
    {
        #region Share Style

        [OperationContract]
        ShareStylesEntity GetShareStyleByShareStyleId(int styleId);
        [OperationContract]
        ShareStylesDetailEntity GetShareStyleDetailByShareStyleId(int styleId);
        [OperationContract]
        List<ShareStylesEntity> SearchShareStyle(string keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<ShareStylesDetailEntity> SearchShareStyleDetail(string keyword, int pageIndex, int pageSize,
                                                             ref int totalRow);
        [OperationContract]
        WcfActionResponse DeleteShareStyleByShareStyleId(int styleId);
        [OperationContract]
        WcfActionResponse ReceiveShareStyle(int styleId, long newsId);

        #endregion

        #region Style images

        [OperationContract]
        List<StyleImagesEntity> GetStyleImageByStyleId(int styleId, int top);

        #endregion
    }
}
