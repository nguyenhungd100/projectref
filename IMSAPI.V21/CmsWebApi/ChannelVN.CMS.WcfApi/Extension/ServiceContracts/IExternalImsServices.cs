﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IExternalImsServices" in both code and config file together.
    [ServiceContract]
    public interface IExternalImsServices
    {
        [OperationContract]
        List<ExternalNewsEntity> SearchNews(string keyword, string username, string zoneIds, DateTime fromDate,
                                            DateTime toDate, int sortOrder, int status, int type, int pageIndex,
                                            int pageSize, ref int totalRow);

        [OperationContract]
        ExternalNewsEntity GetExternalNewsById(long id);

        [OperationContract]
        ExternalNewsEntity GetExternalNewsByImsNewsId(long id);

        [OperationContract]
        List<ExternalNewsInZoneEntity> GetZoneByNewsId(long id);

        [OperationContract]
        List<ExternalNewsAuthorEntity> GetAuthorByNewsId(long id);

        [OperationContract]
        WcfActionResponse ReceiveExternalNews(ExternalNewsEntity ExternalNews);

        [OperationContract]
        WcfActionResponse PublishExternalNews(ExternalNewsEntity ExternalNews);

        [OperationContract]
        WcfActionResponse ReturnExternalNews(ExternalNewsEntity ExternalNews);

        [OperationContract]
        WcfActionResponse UnpublishExternalNews(ExternalNewsEntity ExternalNews);

        [OperationContract]
        WcfActionResponse UpdateImsNewsInfo(ExternalNewsEntity ExternalNews);

        [OperationContract]
        VideoEntity GetDetailVideo(int videoId);

        [OperationContract]
        List<VideoRoyaltyAuthorEntity> GetVideoRoyaltyAuthorByVideoId(long id);

        [OperationContract]
        List<ExternalNewsEntity> SearchNewsIms(string keyword, string zoneIds, DateTime fromDate, DateTime toDate, int type,
                                            int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        WcfActionResponse UpdateImsNewsBody(ExternalNewsEntity externalNews);

        [OperationContract]
        EmbedAlbumForEditEntity GetEmbedAlbumForEditByEmbedAlbumId(int embedAlbumId, int topPhoto);
    }
}
