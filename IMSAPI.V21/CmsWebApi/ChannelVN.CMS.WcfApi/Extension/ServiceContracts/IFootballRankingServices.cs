﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.FootballRanking.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IFootballRankingServices : IErrorMessageMapping
    {
        #region FootballRanking

        [OperationContract]
        WcfActionResponse InsertFootballRanking(FootballRankingEntity footballRanking);
        [OperationContract]
        WcfActionResponse UpdateFootballRanking(FootballRankingEntity footballRanking);
        [OperationContract]
        WcfActionResponse DeleteFootballRanking(int footballRankingId);
        [OperationContract]
        WcfActionResponse UpdateFootballRankingPriority(string listOfPriority);
        [OperationContract]
        FootballRankingEntity GetFootballRanking(int footballRankingId);
        [OperationContract]
        FootballRankingEntity[] SearchFootballRanking(string keyword, EnumFootballRankingStatus status, int pageIndex, int pageSize, ref int totalRow);


        #endregion

        #region FootballRankingDetail

        [OperationContract]
        WcfActionResponse InsertFootballRankingDetail(FootballRankingDetailEntity footballRanking);
        [OperationContract]
        WcfActionResponse UpdateFootballRankingDetail(FootballRankingDetailEntity footballRanking);
        [OperationContract]
        WcfActionResponse DeleteFootballRankingDetail(int footballRankingId);
        [OperationContract]
        FootballRankingDetailEntity GetFootballRankingDetailById(int footballRankingId);
        [OperationContract]
        FootballRankingDetailEntity[] SearchFootballRankingDetail(string keyword, int FootballRankingId, EnumFootballRankingDetailStatus status, int pageIndex, int pageSize, ref int totalRow);


        #endregion
    }
}
