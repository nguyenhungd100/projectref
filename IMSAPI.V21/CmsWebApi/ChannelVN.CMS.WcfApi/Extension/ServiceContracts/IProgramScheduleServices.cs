﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IProgramScheduleServices : IErrorMessageMapping
    {
        #region Program channel

        [OperationContract]
        WcfActionResponse InsertProgramChannel(ProgramChannelEntity programChannel, ref int newProgramChannelId);
        [OperationContract]
        WcfActionResponse UpdateProgramChannel(ProgramChannelEntity programChannel);
        [OperationContract]
        WcfActionResponse DeleteProgramChannel(int programChannelId);
        [OperationContract]
        WcfActionResponse MoveProgramChannelUp(int programChannelId);
        [OperationContract]
        WcfActionResponse MoveProgramChannelDown(int programChannelId);
        [OperationContract]
        List<ProgramChannelEntity> GetProgramChannelByStatus(EnumProgramChannelStatus status);
        [OperationContract]
        ProgramChannelEntity GetProgramChannelByProgramChannelId(int id);

        [OperationContract]
        List<ProgramChannelZoneVideoEntity> GetZoneVideo();
        #endregion


        #region Program schedule

        [OperationContract]
        WcfActionResponse InsertProgramSchedule(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId);
        [OperationContract]
        WcfActionResponse UpdateProgramSchedule(ProgramScheduleEntity programSchedule);
        [OperationContract]
        WcfActionResponse DeleteProgramSchedule(int programScheduleId);
        [OperationContract]
        List<ProgramScheduleEntity> SearchProgramSchedule(string keyword, string date, EnumProgramScheduleStatus status, int programChannelId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        ProgramScheduleEntity GetProgramScheduleByProgramScheduleId(int id);
        [OperationContract]
        WcfActionResponse CheckExistProgramSchedule(ProgramScheduleEntity programSchedule, ref int newProgramScheduleId);

        #endregion

        #region Program schedule detail

        [OperationContract]
        WcfActionResponse InsertProgramScheduleDetail(ProgramScheduleDetailEntity programScheduleDetail, ref int newProgramScheduleDetailId);
        [OperationContract]
        WcfActionResponse InsertListProgramScheduleDetail(List<ProgramScheduleDetailEntity> programScheduleDetails, string listDeleteId);
        [OperationContract]
        WcfActionResponse UpdateProgramScheduleDetail(ProgramScheduleDetailEntity programScheduleDetail);
        [OperationContract]
        WcfActionResponse DeleteProgramScheduleDetail(int programScheduleDetailId);
        [OperationContract]
        List<ProgramScheduleDetailEntity> GetProgramScheduleDetailByProgramScheduleId(int programScheduleId, EnumProgramScheduleDetailStatus status);

        [OperationContract]
        List<ProgramScheduleDetailEntity> ProgramScheduleDetailSearchExport(int programChannelId, DateTime fromDate,
            DateTime toDate, EnumProgramScheduleDetailStatus status);
        [OperationContract]
        List<ProgramScheduleDetailEntity> GetProgramScheduleDetailByScheduleDate(int programChannelId, DateTime scheduleDate, EnumProgramScheduleDetailStatus status);
        [OperationContract]
        ProgramScheduleDetailEntity GetProgramScheduleDetailByProgramScheduleDetailId(int id);

        #endregion
        [OperationContract]
        WcfActionResponse ImportProgramSchedule(List<ProgramChannelEntity> programChannel);
    }
}
