﻿using System.ServiceModel;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using ChannelVN.PageManager.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IPageManagerServices : IErrorMessageMapping
    {
        #region Page
        [OperationContract]
        List<PageEntity> GetPageByURL(string pageUrl);

        [OperationContract]
        bool PublishPageUseModule(int pageId, string configs, string lastModifiedBy);

        [OperationContract]
        bool PublishPageUseModuleByUrl(string pageUrl, string configs, string lastModifiedBy);
        #endregion

        #region PageModule
        [OperationContract]
        List<PageModuleEntity> GetPageModuleByPageId(int pageId);
        #endregion

        #region PageModuleConfig

        [OperationContract]
        List<PageModuleConfigEntity> GetPageModuleConfigByModuleId(int moduleId);

        #endregion

        #region PageUseModuleConfig
        [OperationContract]
        List<PageUseModuleConfigEntity> GetPageUseModuleConfigByModuleIdAndPageId(int pageId, int moduleId);

        [OperationContract]
        WcfActionResponse SavePageModule(int pageId, List<PageUseModuleDetailEntity> allModuleInPage);
        #endregion

        [OperationContract]
        string GetPagePublishedConfig(int pageId);
    }
}
