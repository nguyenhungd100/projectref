﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.TransferMarket.Bo;
using ChannelVN.TransferMarket.Entity;
using ChannelVN.TransferMarket.Entity.ErrorCode;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface ITransferMarketServices : IErrorMessageMapping
    {
        #region TransferMarket

        [OperationContract]
        List<TransferMarketEntity> SearchTransferMarket(string keyword, EnumTransferMarketStatus status, int pageIndex,
                                                        int pageSize, ref int totalRow);
        [OperationContract]
        TransferMarketEntity GetTransferMarketByTransferMarketId(long transferMarketId);
        [OperationContract]
        WcfActionResponse InsertTransferMarket(TransferMarketEntity transferMarket);
        [OperationContract]
        WcfActionResponse UpdateTransferMarket(TransferMarketEntity transferMarket);
        [OperationContract]
        WcfActionResponse DeleteTransferMarket(long transferMarketId);

        #endregion

        #region TransferMarketNews

        [OperationContract]
        List<TransferMarketNewsEntity> SearchTransferMarketNews(string keyword, long transferMarketId,
                                                                EnumTransferMarketNewsLabelType labelType, int mode,
                                                                EnumTransferMarketNewsStatus status, int pageIndex,
                                                                int pageSize, ref int totalRow);
        [OperationContract]
        TransferMarketNewsEntity GetTransferMarketNewsByTransferMarketNewsId(long transferMarketNewsId);
        [OperationContract]
        WcfActionResponse InsertTransferMarketNews(TransferMarketNewsEntity transferMarketNews);
        [OperationContract]
        WcfActionResponse UpdateTransferMarketNews(TransferMarketNewsEntity transferMarketNews);
        [OperationContract]
        WcfActionResponse DeleteTransferMarketNews(long transferMarketNewsId);
        [OperationContract]
        WcfActionResponse ApproveTransferMarketNews(long transferMarketNewsId);
        [OperationContract]
        WcfActionResponse UnpublishTransferMarketNews(long transferMarketNewsId);

        #endregion
    }
}
