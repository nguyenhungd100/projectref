﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Newsletter.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface INewsletterServices : IErrorMessageMapping
    {
        #region NewsletterRegistration
        [OperationContract]
        List<NewsletterRegistrationEntity> GetAllNewsletterRegistrationForThread();
        [OperationContract]
        List<NewsletterRegistrationEntity> GetAllNewsletterRegistrationForZone();
        [OperationContract]
        NewsletterRegistrationEntity GetNewsletterRegistrationById(int newsletterId);
        [OperationContract]
        List<NewsletterRegistrationEntity> NewsletterRegistrationSearch(string keyword, int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
         WcfActionResponse NewsletterRegistrationUpdateStatus(int newsletterId, int status);
        [OperationContract]
        WcfActionResponse NewsletterRegistrationDelete(int newsletterId);

        #endregion

        #region NewsletterMonitor

        [OperationContract]
        List<NewsletterMonitorEntity> GetUnprocessNewsletterMonitorItem();

        [OperationContract]
        WcfActionResponse UpdateProcessNewsletterMonitorItemState(string listOfProcessId);

        #endregion

        #region NewsLetter
        [OperationContract]
        List<NewsletterEntity> SelectData(string keyword, int pageIndex, int pageSize, ref int totalRows);
        [OperationContract]
        NewsletterEntity SelectDataById(int Id);
        [OperationContract]
        List<NewsletterDetailEntity> SelectNewsletterDetailById(int newsLetterId);
        [OperationContract]
        WcfActionResponse InsertNewsLetter(NewsletterEntity entity, List<NewsletterDetailEntity> entityDetail, ref int id);
        [OperationContract]
        WcfActionResponse UpdateNewsLetter(NewsletterEntity entity, List<NewsletterDetailEntity> entityDetail);
        [OperationContract]
        WcfActionResponse DeleteNewsLetter(int Id);
        [OperationContract]
        WcfActionResponse DeleteAllNewsLetterDetail(int newsLetterId);
        [OperationContract]
        WcfActionResponse DeleteNewsLetterDetail(int Id);
        [OperationContract]
        WcfActionResponse UpdatePriority(int newsLetterId, string listPriority);
        #endregion
    }
}
