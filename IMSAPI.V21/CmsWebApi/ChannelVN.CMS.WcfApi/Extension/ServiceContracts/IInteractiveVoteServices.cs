﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.InteractiveVote.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ServiceContract]
    public interface IInteractiveVoteServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertInteractiveVote(InteractiveVoteEntity quiz);

        [OperationContract]
        WcfActionResponse UpdateInteractiveVote(InteractiveVoteEntity quiz);

        [OperationContract]
        WcfActionResponse UpdateInteractiveVoteData(int id, string quizData);        

        [OperationContract]
        InteractiveVoteEntity GetInteractiveVoteById(int id);

        [OperationContract]
        List<InteractiveVoteEntity> SearchInteractiveVote(string keyword, EnumInteractiveVoteType type, string createdBy, int status, int siteId,
            int pageIndex, int pageSize, ref int totalRow);       

    }
}
