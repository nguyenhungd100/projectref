﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.CrawlerSource;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewsCrawlerV5Services" in both code and config file together.
    [ServiceContract]
    public interface INewsCrawlerV5Services
    {
        [OperationContract]
        WcfActionResponse InsertCrawlerSource(CrawlerSourceTopicWrapperEntity news);
        [OperationContract]
        List<CrawlerSourceEntity> GetSourceByTopic(string userName, int topicId);
        [OperationContract]
        List<CrawlerSourceEntity> GetSourceByUser(string userName);
        [OperationContract]
        List<CrawlerSourceTopicEntity> GetTopicByCrawlerSource(int crawlerSourceId);
    }
}
