﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.PartnerNews.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IPartnerNewsServices : IErrorMessageMapping
    {

        #region Partner
        [OperationContract]
        WcfActionResponse InsertPartner(PartnerEntity info);
        [OperationContract]
        WcfActionResponse UpdatePartner(PartnerEntity info);
        [OperationContract]
        WcfActionResponse DeletePartner(int id);
        [OperationContract]
        List<PartnerEntity> SelectAllPartner(int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<PartnerEntity> PartnerSelectListAllField();
        [OperationContract]
        PartnerEntity PartnerSelectDataById(int id);
        #endregion


        #region PartnerNews
        [OperationContract]
        WcfActionResponse InsertPartnerNews(PartnerNewsEntity info);
        [OperationContract]
        WcfActionResponse UpdatePartnerNews(PartnerNewsEntity info);
        [OperationContract]
        WcfActionResponse UpdateStatusPartnerNewsByNewsId(long newsid, int status);
        [OperationContract]
        WcfActionResponse DeletePartnerNews(long id);
        [OperationContract]
        List<PartnerNewsEntity> SelectAllPartnerNews(string keyword, int partnerId, EnumPartnerNewsStatus status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<PartnerNewsEntity> PartnerNewsSelectListAllField();
        [OperationContract]
        PartnerNewsEntity PartnerNewsSelectDataById(long id);
        #endregion
    }
}
