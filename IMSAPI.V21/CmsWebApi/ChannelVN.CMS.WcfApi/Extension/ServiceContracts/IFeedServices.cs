﻿using ChannelVN.Feed.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFeedServices" in both code and config file together.
    [ServiceContract]
    public interface IFeedServices
    {
        [OperationContract]
        bool Insert_FeedItem(FeedItemEntity feedItem, ref int Id);

        [OperationContract]
        List<FeedEntity> Feed_GetList();

        [OperationContract]
        bool Update_Status(int Id, FeedItemStatus status, int DownloadMediaId=-1);

        [OperationContract]
        bool Update_Status_ByMediaIds(string MediaIds);

        [OperationContract]
        List<FeedItemEntity> FeedItem_GetList();

        [OperationContract]
        List<FeedItemEntity> FeedItem_GetList_Download();

        [OperationContract]
        List<FeedItemEntity> FeedItem_GetList_ReDownload();


        #region FeedItemV2
        [OperationContract]
        bool Insert_FeedItemV2(FeedItemV2Entity feedItem, ref int Id);
        [OperationContract]
        bool FeedItemV2_UpdateByComplete(FeedItemV2Entity feedItem);
        [OperationContract]
        bool FeedItemV2_DownloadFinished(string MediaIds);
        [OperationContract]
        bool FeedItemV2_Downloading(string IdAndMediaIds);
        [OperationContract]
        List<FeedItemV2Entity> FeedItemV2_GetListCheckDownload();
        [OperationContract]
        List<FeedItemV2Entity> FeedItemV2_GetListDownloadFinished();
        [OperationContract]
        bool FeedItemV2_UpdateStatus(int Id, FeedItemStatus Status);
        [OperationContract]
        FeedItemV2Entity FeedItemV2_GetById(int Id);
        #endregion
    }
}
