﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.NewsCrawler.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ServiceContract]
    public interface INewsCrawlerServices : IErrorMessageMapping
    {
        #region Sites

        [OperationContract]
        SiteDetailEntity GetSiteDetailByUrl(string url);

        [OperationContract]
        SiteEntity[] SearchSite(int status);

        [OperationContract]
        WcfActionResponse UpdateSite(SiteEntity entity, ref int siteId);

        [OperationContract]
        WcfActionResponse UpdateSiteStatus(int siteId, int status);

        [OperationContract]
        WcfActionResponse DeleteSiteBySiteId(int siteId);

        [OperationContract]
        WcfActionResponse UpdateSiteDetail(SiteDetailEntity siteDetail);
        #endregion

        #region Zones
        [OperationContract]
        ZoneEntity[] SearchZone(int status, int siteId);

        [OperationContract]
        WcfActionResponse UpdateZone(ZoneEntity entity, ref int siteId);

        [OperationContract]
        WcfActionResponse UpdateZoneStatus(int zoneId, int status);
        #endregion

        #region News

        [OperationContract]
        NewsInListEntity[] SearchNews(int status, int siteId, int zoneId, string keyword, int pageIndex, int pageSize,
                                      ref int totalRow);

        [OperationContract]
        WcfActionResponse InsertNews(NewsEntity entity);

        [OperationContract]
        WcfActionResponse UpdateNews(NewsCrawler.Entity.NewsEntity entity);

        [OperationContract]
        WcfActionResponse UpdateReadMonitor(string accountName, int siteId, int zoneId, int channelId);

        [OperationContract]
        ChannelVN.NewsCrawler.Entity.NewsEntity GetNewsById(long id);

        [OperationContract]
        List<NewsCountEntity> NewsCounter(string accountName, int channelId);

        #endregion

        #region ErrorReport

        [OperationContract]
        ErrorReportInListEntity[] SearchErrorReport(int siteId, int zoneId, int pageIndex, int pageSize,
                                      ref int totalRow);

        [OperationContract]
        WcfActionResponse InsertErrorReport(ErrorReportEntity entity);

        [OperationContract]
        ErrorReportEntity GetErrorReportById(int id);

        #endregion

        #region NewsCrawlerConfig

        [OperationContract]
        List<NewsCrawlerConfigEntity> SearchNewsCrawlerConfig(string accountName, int siteId, int zoneId);

        [OperationContract]
        WcfActionResponse UpdateNewsCrawlerConfig(NewsCrawlerConfigEntity entity);

        [OperationContract]
        WcfActionResponse AddSite(SiteEntity site);

        [OperationContract]
        WcfActionResponse AddZone(NewsCrawlerConfigForEditEntity config, string accountName);

        [OperationContract]
        WcfActionResponse UpdateNewsCrawlerConfigDetail(SiteDetailEntity siteDetail);

        [OperationContract]
        WcfActionResponse DeleteNewsCrawlerConfig(int id);

        [OperationContract]
        WcfActionResponse DeleteNewsCrawlerConfigBySiteIdAndZoneId(int siteId, int zoneId);

        [OperationContract]
        WcfActionResponse DeleteNewsCrawlerConfigBySiteId(int siteId);

        [OperationContract]
        List<SiteDetailInListEntity> GetAllSiteDetailInMyFavorite();

        #endregion

        #region CrawlerSystem

        [OperationContract]
        List<NewsDetailRegexEntity> GetCrawlerSystemByUrl(string url);

        #endregion

        #region CafeBiz
        [OperationContract]
        List<NewsLinkCafeFEntity> SearchCafeBiz(string keyword, int pageIndex, int pageSize, ref int totalRow, int time = 0, string filter = "A", int siteId = -1, int categoryId = -1, string page = "", int typeId = 0);
        [OperationContract]
        NewsLinkDetailCafeFEntity GetByIdCafeBiz(long LinkId);
        [OperationContract]
        WcfActionResponse UpdateStatusCrawlerCafeBiz(int LinkId, string LinkStatus);
        #endregion
    }
}
