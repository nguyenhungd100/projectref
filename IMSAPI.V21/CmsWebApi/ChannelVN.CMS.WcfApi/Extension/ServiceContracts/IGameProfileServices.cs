﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.GameProfile.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface IGameProfileServices : IErrorMessageMapping
    {
        #region GameBrand

        [OperationContract]
        GameBrandEntity[] GetAllBrand();

        [OperationContract]
        GameBrandEntity[] GetListBrand(int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        GameBrandEntity[] SearchBrand(string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        GameBrandEntity GetBrandById(int id);

        [OperationContract]
        WcfActionResponse InsertBrand(GameBrandEntity gameBrandEntity);

        [OperationContract]
        WcfActionResponse UpdateBrand(GameBrandEntity gameBrandEntity);

        [OperationContract]
        WcfActionResponse DeleteBrand(int id);

        [OperationContract]
        WcfActionResponse UpdateStatusByID(long productId, byte status);

        #endregion

        #region GameCategory

        [OperationContract]
        GameCategoryEntity[] GetAllCategory();

        [OperationContract]
        GameCategoryEntity GetCategoryById(int id);

        [OperationContract]
        WcfActionResponse InsertCategory(GameCategoryEntity gameCategoryEntity);

        [OperationContract]
        WcfActionResponse UpdateCategory(GameCategoryEntity gameCategoryEntity);

        [OperationContract]
        WcfActionResponse DeleteCategory(int id);

        [OperationContract]
        WcfActionResponse UpdateCategoryStatusByID(int newsCatId, byte status);

        #endregion

        #region Game

        [OperationContract]
        GameViewForAllListEntity[] SearchGame(string keyword, int catId, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        GameEntity GetGameById(int gameid);

        [OperationContract]
        WcfActionResponse InsertGame(GameEntity gameGameEntity);

        [OperationContract]
        WcfActionResponse UpdateGame(GameEntity gameGameEntity);

        [OperationContract]
        WcfActionResponse DeleteGame(int id);

        [OperationContract]
        WcfActionResponse UpdateGameStatusByID(int gameId, byte status);

        [OperationContract]
        WcfActionResponse UpdateGameIsNewByID(int gameId, bool isNew);

        [OperationContract]
        WcfActionResponse UpdateGameIsHighLightByID(int gameId, bool isHighLight);

        #endregion

        #region GameMedia

        [OperationContract]
        GameMediaEntity[] GetListGameMediaByGameId(int gameId);

        [OperationContract]
        GameMediaEntity GetGameMediaById(int id);

        [OperationContract]
        int CountGameMediaInGame(int gameId, byte mediaType);

        [OperationContract]
        WcfActionResponse UpdateMultiGameMedia(string listName, string listDesc, string listNote, string listUrl,
                                               int mediaType, int gameId);

        [OperationContract]
        WcfActionResponse InsertGameMedia(GameMediaEntity gameMediaEntity);

        [OperationContract]
        WcfActionResponse UpdateGameMedia(GameMediaEntity gameMediaEntity);

        [OperationContract]
        WcfActionResponse DeleteGameMedia(int id);

        #endregion

        #region GameRate

        [OperationContract]
        GameRateEntity GetGameRateByGameId(int gameId);

        [OperationContract]
        WcfActionResponse UpdateGameRate(GameRateEntity gameRate);

        [OperationContract]
        List<GateGameRatiesEntity> GetListGameRatiesByDate(DateTime lastModifyDate, ref DateTime maxVoteDate);

        [OperationContract]
        WcfActionResponse SyncGateGameRaties(GateGameRatiesEntity gameRate);
        #endregion

        #region GameGiftCode

        [OperationContract]
        WcfActionResponse InsertGiftCode(long newsId, string giftCode, string tags);
        [OperationContract]
        GameGiftCodeEntity GetStartGiftCodeByNewsId(long newsId);

        #endregion
    }
}
