﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.ExternalVideoEmbed;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IExternalVideoServices : IErrorMessageMapping
    {
        [OperationContract]
        ExternalVideoEntity GetExternalVideoById(int id);

        [OperationContract]
        List<ExternalVideoEntity> GetUnprocessExternalVideo(string keyword, int zoneVideoId, int pageIndex, int pageSize,
                                                            ref int totalRow);
        [OperationContract]
        int GetExternalVideoCount();

        [OperationContract]
        WcfActionResponse RecieveExternalVideo(int videoId);

        [OperationContract]
        WcfActionResponse DeleteExternalVideo(int videoId);

        [OperationContract]
        WcfActionResponse Insert(ExternalVideoEmbedEntity videoEmbedEntity);

        [OperationContract]
        WcfActionResponse Delete(int zoneId, int type);

        [OperationContract]
        List<ExternalVideoEmbedEntity> GetListByZoneType(int zoneId, int type);
    }
}
