﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.BigStory.Bo;
using ChannelVN.BigStory.Entity;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IBigStoryServices
    {
        #region BigStory
        [OperationContract]
        List<BigStoryEntity> BigStory_Search(string keyword, string CreatedBy, BigStoryIsFocus IsFocus, BigStoryStatus status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        BigStoryEntity BigStory_GetById(int Id);

        [OperationContract]
        WcfActionResponse BigStory_Update(BigStoryEntity entity);

        [OperationContract]
        WcfActionResponse BigStory_UpdateAllItem(BigStoryEntity entity, List<BigStoryItemEntity> ListBigStoryItem);

        [OperationContract]
        WcfActionResponse BigStory_Insert(BigStoryEntity entity);

        [OperationContract]
        WcfActionResponse BigStory_InsertAllItem(BigStoryEntity entity, List<BigStoryItemEntity> ListBigStoryItem);

        [OperationContract]
        WcfActionResponse BigStory_Delete(int Id);

        [OperationContract]
        WcfActionResponse BigStory_UpdateIsFocus(int Id, bool IsFocus);

        [OperationContract]
        WcfActionResponse BigStory_UpdateStatus(int Id, BigStoryStatus status); 
        #endregion

        #region BigStoryItem
        [OperationContract]
        List<BigStoryItemEntity> BigStoryItem_GetByBigStoryId(int BigStoryId);

        [OperationContract]
        WcfActionResponse BigStoryItem_UpdateIsHighlight(int Id, bool IsHighlight);

        [OperationContract]
        WcfActionResponse BigStoryItem_UpdateStatus(int Id, BigStoryItemStatus status);

        [OperationContract]
        WcfActionResponse BigStoryItem_Delete(int Id);

        [OperationContract]
        WcfActionResponse BigStoryItem_Insert(BigStoryItemEntity entity);

        [OperationContract]
        WcfActionResponse BigStoryItem_Update(BigStoryItemEntity entity);

        [OperationContract]
        BigStoryItemEntity BigStoryItem_GetById(int Id);

        [OperationContract]
        WcfActionResponse BigStoryItem_InsertByList(List<BigStoryItemEntity> list);
        [OperationContract]
        WcfActionResponse BigStoryItem_UpdateByList(List<BigStoryItemEntity> list, int bigStoryId);
        #endregion

        #region BigStoryFocusItem
        [OperationContract]
        List<BigStoryFocusItemEntity> BigStoryFocusItem_GetByBigStoryId(int BigStoryId);                
        [OperationContract]
        WcfActionResponse BigStoryFocusItem_Insert(BigStoryFocusItemEntity entity);
        [OperationContract]
        WcfActionResponse BigStoryFocusItem_Update(BigStoryFocusItemEntity entity);

        [OperationContract]
        BigStoryFocusItemEntity BigStoryFocusItem_GetById(int Id);
        [OperationContract]
        WcfActionResponse BigStoryFocusItem_InsertByList(List<BigStoryFocusItemEntity> list);
        [OperationContract]
        WcfActionResponse BigStoryFocusItem_UpdateByList(List<BigStoryFocusItemEntity> list, int bigStoryId);
        [OperationContract]
        WcfActionResponse BigStoryFocusItem_Delete(int Id);
        #endregion
    }
}
