﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.LiveGameMatch.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface ILiveGameMatchServices : IErrorMessageMapping
    {
        #region live game
        [OperationContract]
        WcfActionResponse InsertLiveGame(LiveGameEntity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateLiveGame(LiveGameEntity info);
        [OperationContract]
        WcfActionResponse DeleteLiveGame(int id);
        [OperationContract]
        List<LiveGameEntity> ListLiveGameByPages(string search, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        LiveGameEntity GetLiveGameById(int Id);
        #endregion

        #region live game match
        [OperationContract]
        WcfActionResponse InsertLiveGameMatch(LiveGameMatchEntity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateLiveGameMatch(LiveGameMatchEntity info);
        [OperationContract]
        WcfActionResponse DeleteLiveGameMatch(long newsId);
        [OperationContract]
        LiveGameMatchEntity GetGameMatchById(int Id);
        [OperationContract]
        WcfActionResponse UpdatePublishDate(DateTime publishDate, long publishDateStamp, int gameMatchId);
        [OperationContract]
        LiveGameMatchEntity GetGameMatchByNewsId(long newsId);
        #endregion

        #region Game league
        [OperationContract]
        WcfActionResponse InsertLiveGameLeague(LiveGameLeagueEntity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateLiveGameLeague(LiveGameLeagueEntity info);
        [OperationContract]
        WcfActionResponse DeleteLiveGameLeague(int id);
        [OperationContract]
        List<LiveGameLeagueEntity> ListLiveGameLeagueByPages(string search, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        LiveGameLeagueEntity GetLiveGameLeagueById(int Id);
        #endregion

        #region Team game
        [OperationContract]
        WcfActionResponse InsertLiveTeamGame(LiveTeamGameEntity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateLiveTeamGame(LiveTeamGameEntity info);
        [OperationContract]
        WcfActionResponse DeleteLiveTeamGame(int id);
        [OperationContract]
        List<LiveTeamGameEntity> ListLiveTeamGameByPages(string search, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        LiveTeamGameEntity GetLiveTeamGameById(int Id);
        [OperationContract]
        List<LiveTeamGameEntity> ListLiveTeamGameByMultiId(int teamA, int teamB);
        #endregion
    }
}
