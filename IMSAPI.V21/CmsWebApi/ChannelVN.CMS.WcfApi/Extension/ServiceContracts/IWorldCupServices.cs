﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WorldCup.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface IWorldCupServices : IErrorMessageMapping
    {
        #region WCTeams

        [OperationContract]
        List<WCTeamsEntity> SearchTeams(string groupName);
        [OperationContract]
        WCTeamsEntity GetTeamById(int id);
        [OperationContract]
        WcfActionResponse InsertTeam(WCTeamsEntity team);
        [OperationContract]
        WcfActionResponse UpdateTeam(WCTeamsEntity team);
        [OperationContract]
        WcfActionResponse DeleteTeam(int id);

        #endregion

        #region WCTopPlayers

        [OperationContract]
        List<WCTopPlayersDetailEntity> SearchTopPlayer(string name);

        [OperationContract]
        WCTopPlayersEntity GetTopPlayerById(int id);

        [OperationContract]
        WcfActionResponse InsertTopPlayer(WCTopPlayersEntity player);

        [OperationContract]
        WcfActionResponse UpdateTopPlayer(WCTopPlayersEntity player);

        [OperationContract]
        WcfActionResponse DeleteTopPlayer(int id);

        #endregion
    }
}
