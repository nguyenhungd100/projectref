﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ServiceContract]
    public interface IGiftCodeServices : IErrorMessageMapping
    {
        [OperationContract]
        List<GiftCodeEntity> GetByNewsId(long newsId);
    }
}
