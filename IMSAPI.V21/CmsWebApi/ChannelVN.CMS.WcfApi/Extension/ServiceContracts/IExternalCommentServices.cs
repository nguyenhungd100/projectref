﻿using System.ServiceModel;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IExternalCommentServices : IErrorMessageMapping
    {
        #region Comment

        [OperationContract]
        CommentEntity[] Search(string objectTitle, EnumCommentObjectType objectType, string title, long parentId, CommentStatus status, long objectId, int pageIndex,
                               int pageSize, string zoneIds, ref int totalRow);

        [OperationContract]
        CommentEntity GetCommentById(long id);

        [OperationContract]
        WcfActionResponse ReceiveCommentById(long id, ref long commentPublishedId);

        [OperationContract]
        WcfActionResponse UpdateCommentById(CommentEntity comment);

        [OperationContract]
        WcfActionResponse DeleteCommentById(long commentId, string deletedBy);

        [OperationContract]
        CommentEntity[] StatisticsGetComment(EnumCommentObjectType objectType, CommentStatus status, DateTime fromDate, DateTime toDate);

        [OperationContract]
        WcfActionResponse RemoveCommentById(long id, string LastModifiedBy);

        [OperationContract]
        WcfActionResponse DeleteListComments(string commentIds, string LastModifiedBy);

        [OperationContract]
        NewsHasNewCommentExtEntity[] GetNewsHasNewCommentExt(string objectTitle, EnumCommentObjectType objectType, CommentStatus status, string zoneIds, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        CommentEntity[] SearchV2(string objectTitle, EnumCommentObjectType objectType, string content, long parentId, CommentStatus status, long objectId, int pageIndex, int pageSize, string zoneIds, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertComment(CommentEntity comment);

        [OperationContract]
        List<CommentEntity> SearchObjectByUser(long userId, int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<CommentEntity> SearchCommentByUserObject(long userId, int status, long objectId);

        [OperationContract]
        WcfActionResponse CommentUpdateMailForwarded(long id, string mailForwarded);
        [OperationContract]
        WcfActionResponse CommentUpdateMailReplied(long id, string mailReplied);
        #endregion

        #region CommentPublished

        [OperationContract]
        CommentPublishedEntity[] SearchCommentPublished(string objectTitle, EnumCommentPublishedObjectType objectType, string title,
            long parentId, CommentPublishedStatus status, string receivedBy, long objectId, int pageIndex,
                               int pageSize, DateTime publishDate, int TitleSearchMode, ref int totalRow);

        [OperationContract]
        CommentPublishedEntity GetCommentPublishedById(long id);

        [OperationContract]
        WcfActionResponse ApproveCommentPublished(long id);

        [OperationContract]
        WcfActionResponse RemoveCommentPublished(long id);

        [OperationContract]
        WcfActionResponse UnapproveCommentPublished(long id);

        [OperationContract]
        WcfActionResponse CommentPublishedAdminInsert(CommentPublishedEntity comment);

        [OperationContract]
        WcfActionResponse UpdateCommentPublishedById(CommentPublishedEntity commentPublished);

        [OperationContract]
        WcfActionResponse DeleteCommentPublishedById(long commentPublishedId, string deletedBy);

        [OperationContract]
        CommentPublishedEntity[] StatisticsGetCommentPublished(EnumCommentPublishedObjectType objectType, CommentPublishedStatus status, DateTime fromDate, DateTime toDate);

        [OperationContract]
        WcfActionResponse DeleteListCommentPublished(string commentIds, string LastModifiedBy);

        [OperationContract]
        WcfActionResponse UnApproveListCommentPublished(string commentIds, string LastModifiedBy);

        [OperationContract]
        WcfActionResponse ApproveListCommentPublished(string commentIds, string PublishBy);

        [OperationContract]
        WcfActionResponse ReApproveListCommentPublished(string commentIds, string PublishBy);

        [OperationContract]
        CommentPublishedEntity[] GetPublishedDateNewsByNewsId(string NewsIds);

        [OperationContract]
        List<CommentEntity> GetCommentsByNewsId(long newsId, long parentId);

        [OperationContract]
        List<CommentCounterEntity> GetCommentCounterByObjectId(string lstId, int objectType);

        [OperationContract]
        List<CommentCounterEntity> GetCommentCounterByObjectIdAndStatus(string lstId, int objectType, int status);

        [OperationContract]
        List<CommentCounterEntity> GetCommentCounterInternalByObjectId(string lstId, int objectType);

        [OperationContract]
        NewsHasNewCommentEntity[] GetNewsHasNewComment(string objectTitle, EnumCommentPublishedObjectType objectType,
            CommentPublishedStatus status, int pageIndex, int pageSize, ref int totalRow);


        [OperationContract]
        CommentPublishedEntity[] SearchCommentPublishedV2(string objectTitle, EnumCommentPublishedObjectType objectType, string content,
            long parentId, CommentPublishedStatus status, string receivedBy, long objectId, string zoneIds, int pageIndex, int pageSize,
            DateTime publishDate, ref int totalRow);

        [OperationContract]
        WcfActionResponse UpdateLikeCountComment(string value);

        [OperationContract]
        List<CommentPublishedCountEntity> GetCommentCount(DateTime fromDate, DateTime toDate, string zoneIds);
        [OperationContract]
        List<CommentPublishedCountByZoneEntity> GetCommentCountByZone(DateTime fromDate, DateTime toDate, string zoneIds);

        [OperationContract]
        CommentPublishedStatisticsEntity CommentPublished_Statistics(DateTime fromDate, DateTime toDate, string zoneIds);

        [OperationContract]
        WcfActionResponse CommentPublishedUpdateMailForwarded(long id, string mailForwarded);

        [OperationContract]
        WcfActionResponse CommentPublishedUpdateMailReplied(long id, string mailReplied);
        #endregion

        #region CommentRate

        [OperationContract]
        WcfActionResponse UpdateUnprocessCommentRate();

        [OperationContract]
        List<CommentRateEntity> GetUnprocessLikeComment(DateTime lastModifieddate);



        #endregion

        [OperationContract]
        WcfActionResponse InsertCommentLogAction(CommentLogEntity commentLog);

    }
}
