﻿using ChannelVN.CafeBiz.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICafeBizServices" in both code and config file together.
    [ServiceContract]
    interface ICafeBizServices : IErrorMessageMapping
    {
        #region BizDocField (Lĩnh vực văn bản pháp luật)
        [OperationContract]
        WcfActionResponse InsertBizDocField(BizDocFieldEntity info);
        [OperationContract]
        WcfActionResponse UpdateBizDocField(BizDocFieldEntity info);
        [OperationContract]
        WcfActionResponse DeleteBizDocField(int Id);
        [OperationContract]
        List<BizDocFieldEntity> SelectAllBizDocField(int PageIndex, int PageSize, ref int TotalRow);
        [OperationContract]
        List<BizDocFieldEntity> SelectListAllField();
        [OperationContract]
        BizDocFieldEntity SelectDataById(int id);
        #endregion
        #region BizDoc (Văn bản pháp luật)
        [OperationContract]
        WcfActionResponse InsertBizDoc(BizDocEntity info);
        [OperationContract]
        WcfActionResponse UpdateBizDoc(BizDocEntity info);
        [OperationContract]
        WcfActionResponse DeleteBizDoc(long Id);
        [OperationContract]
        List<BizDocEntity> SelectAllBizDoc(int PageIndex, int PageSize, ref int TotalRow, int fieldId = 0, int propId = 0);
        [OperationContract]
        BizDocEntity SelectBizBocById(long id);
        #endregion
        #region BizDocProperties
        [OperationContract]
        List<BizDocPropertiesEntity> SelectListAllProperties();
        #endregion
    }
}
