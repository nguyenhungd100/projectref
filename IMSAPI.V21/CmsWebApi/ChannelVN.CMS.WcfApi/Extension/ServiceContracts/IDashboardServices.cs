﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Dashboard.Entity;
using ChannelVN.Dashboard.Bo;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IDashboardServices : IErrorMessageMapping
    {
        #region Thongke
        [OperationContract]
        StatisticsBoxTongHopEntity Statistics_Boxtonghop(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName);

        [OperationContract]
        CountCommentEntity Statistics_BinhLuan(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName);

        [OperationContract]
        CountUserEntity Statistics_ThanhVien(Constants.loaiThongKe loaiThongKe, string UserName);

        [OperationContract]
        StatisticsProductionNewsSendEntity Statistics_BaiGuilen(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName);

        [OperationContract]
        StatisticsProductionNewsPublishEntity Statistics_BaiXuatBan(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName);

        [OperationContract]
        StatisticsProductionVideoSendEntity Statistics_VideoGuiLen(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName);

        [OperationContract]
        StatisticsProductionVideoPublishEntity Statistics_VideoXuatBan(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName);

        [OperationContract]
        List<ListPercent> Statistics_TiLeBaiTheoCategory(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName);

        [OperationContract]
        List<ListPercent> Statistics_TiLeBaiTheoChuyenmuc(Constants.loaiThongKe loaiThongKe, string UserName);

        [OperationContract]
        List<ListPercent> Statistics_TiLeVideoTheoChuyenmuc(Constants.loaiThongKe loaiThongKe, string UserName);

        [OperationContract]
        StatisticsView_VisitEntity Statistics_ViewVisitByTime(Constants.loaiThongKe loaiThongKe, string UserName, int ZoneId, Constants.Db Db);

        [OperationContract]
        StatisticsView_VisitEntityByZone Statistics_ViewVisitByZone(Constants.loaiThongKe loaiThongKe, string UserName, Constants.Db Db);

        [OperationContract]
        List<TopPVEntity> Statistics_TopPV(Constants.loaiThongKe loaiThongKe, int ZoneId, string UserName, int TopSize, string keyword);

        [OperationContract]
        List<ZoneParentEntity> ListZoneParent(string UserName);
        [OperationContract]
        List<ZoneEntity> ListZoneByUserName(string UserName);

        [OperationContract]
        List<ZoneEntity> ListZoneById(int ZoneParentId);

        [OperationContract]
        void XoaCacheStatisticsByUserName(string UserName);

        [OperationContract]
        void XoaCacheStatistics();

        [OperationContract]
        void XoaAllCache();
        #endregion
    }
}
