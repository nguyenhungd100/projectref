﻿using System;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IFeedBackNewsServices : IErrorMessageMapping
    {
        #region FeedBackNews

        [OperationContract]
        FeedBackNewsEntity GetFeedBackNewsById(long id);

        [OperationContract]
        List<FeedBackNewsWithSimpleFieldEntity> SearchFeedBackNews(string keyword, long parentId, EnumFeedBackNewsStatus status,
                                                    EnumFeedBackNewsFilterDateField filterDateField,
                                                    DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize,
                                                    ref int totalRow);
        [OperationContract]
        List<FeedBackNewsWithSimpleFieldEntity> SearchFeedBackNewsHasExternalNews(int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        WcfActionResponse DeleteFeedBackNewsById(long id);
        [OperationContract]
        WcfActionResponse InsertFeedBackNews(FeedBackNewsEntity feedBackNews);
        [OperationContract]
        WcfActionResponse UpdateFeedBackNews(FeedBackNewsEntity feedBackNews);
        [OperationContract]
        WcfActionResponse PublishFeedBack(long id, string publishedBy);
        [OperationContract]
        WcfActionResponse UnpublishFeedBack(long id);

        #endregion

        #region ExternalFeedBackNews

        [OperationContract]
        ExternalFeedBackNewsEntity GetExternalFeedBackNewsById(long id);

        [OperationContract]
        List<ExternalFeedBackNewsWithSimpleFieldEntity> SearchExternalFeedBackNews(string keyword, long parentId,
                                                                    DateTime dateFrom, DateTime dateTo, int pageIndex,
                                                                    int pageSize,
                                                                    ref int totalRow);

        [OperationContract]
        WcfActionResponse DeleteExternalFeedBackNewsById(long id);
        [OperationContract]
        WcfActionResponse ReceiveExternalFeedBackNews(long id);
        [OperationContract]
        List<ExFeedBackNewsEntity> SearchNews(string keyword, int status, int pageIndex, int pageSize,
                                                      ref int totalRow);
        [OperationContract]
        ExFeedBackNewsEntity GetNewsById(long id);
        [OperationContract]
        WcfActionResponse UpdateStatusIms(long newsId, int status, long feedbackId);

        #endregion
    }
}
