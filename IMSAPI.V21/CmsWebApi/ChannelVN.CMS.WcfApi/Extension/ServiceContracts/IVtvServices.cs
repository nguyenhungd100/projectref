﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.VTV.Entity;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IVtvServices" in both code and config file together.
    [ServiceContract]
    public interface IVtvServices : IErrorMessageMapping
    {
        #region CHANNEL
        [OperationContract]
        List<VtvChannelEntity> GetListChannel(string keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertChannel(VtvChannelEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse UpdateChannel(VtvChannelEntity elm);
        [OperationContract]
        VtvChannelEntity SelectChannel(int id);
        [OperationContract]
        WcfActionResponse DeleteChannel(int id);
        #endregion

        #region SCHEDULE
        [OperationContract]
        List<VtvScheduleEntity> GetListSchedule(string keyword, int channelId, DateTime filterDate, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertSchedule(VtvScheduleEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse UpdateSchedule(VtvScheduleEntity elm);
        [OperationContract]
        VtvScheduleEntity SelectSchedule(int id);
        [OperationContract]
        WcfActionResponse DeleteSchedule(int id);
        #endregion

        #region Program Schedule
        [OperationContract]
        WcfActionResponse InsertProgramSchedule(VTVProgramScheduleEntity elm, ref int id);
        #endregion

        #region Program Schedule Detail
        [OperationContract]
        WcfActionResponse InsertProgramScheduleDetail(List<VTVProgramScheduleDetailEntity> elm, ref int id);
        #endregion

        #region Schedule Register
        /// <summary>
        /// Lấy danh sách người dùng đăng ký nhận thông tin từ VTV (Lịch phát sóng, Tạp chí truyền hình)
        /// Dữ liệu được lấy từ DB ngoài (VTV_EXT)
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="registeredDate">Datetime</param>
        /// <param name="type">int</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int</param>
        /// <returns>List Entity</returns>
        [OperationContract]
        List<VTVScheduleRegisterEntity> ListScheduleRegister(string keyword, DateTime registeredDate,
                                                    int type, int status, int order, int pageIndex, int pageSize, ref int totalRow);
        /// <summary>
        /// Lấy thông tin người đăng ký nhận quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        [OperationContract]
        VTVScheduleRegisterEntity GetScheduleRegisterById(int Id);
        /// <summary>
        /// Cập nhật thông tin ScheduleRegister
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        [OperationContract]
        WcfActionResponse UpdateScheduleRegisterInfo(VTVScheduleRegisterEntity info);

        WcfActionResponse GetScheduleFilePath(int programChannelId, int week, int month, int year);

        //VTVScheduleRegisterLogEntity GetScheduleRegisterLogByEmail(int scheduleRegisterId, int week,
        //    int month, int year);

        //WcfActionResponse LogSendScheduleToCustomer(VTVScheduleRegisterLogEntity log);
        #endregion

        #region Schedule Register Log

        [OperationContract]
        List<VTVScheduleRegisterLogEntity> ListScheduleRegisterLog(int scheduleRegisterId, int pageIndex, int pageSize,
            ref int totalRow);

        [OperationContract]
        WcfActionResponse ScheduleRegisterLogInsert(VTVScheduleRegisterLogEntity entity, ref int id);
        #endregion

        #region VIDEO THREAD
        [OperationContract]
        List<VtvVideoThreadEntity> GetListVideoThread(string keyword, int isHot, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertVideoThread(VtvVideoThreadEntity elm, ref int id);
        [OperationContract]
        WcfActionResponse UpdateVideoThread(VtvVideoThreadEntity elm);
        [OperationContract]
        WcfActionResponse DeleteVideoThread(int id);
        [OperationContract]
        VtvVideoThreadEntity SelectVideoThread(int id);
        [OperationContract]
        WcfActionResponse IsHotVideoThread(int id);
        #endregion

        #region VIDEO THREAD ZONE
        [OperationContract]
        WcfActionResponse InsertVideoThreadZone(VtvVideoThreadZoneEntity elm);
        [OperationContract]
        VtvVideoThreadZoneEntity SelectVideoThreadZone(int videoThreadId, bool isPrimary);
        [OperationContract]
        WcfActionResponse DeleteVideoThreadZone(int videoThreadId);
        #endregion

        #region VIDEO THREAD VIDEO
        [OperationContract]
        List<VtvVideoThreadVideoEntity> GetListVideoThreadVideo(int videoThreadId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertVideoThreadVideo(VtvVideoThreadVideoEntity elm);
        [OperationContract]
        WcfActionResponse DeleteVideoThreadVideo(int videoThreadId, int videoId);
        [OperationContract]
        VtvVideoThreadVideoEntity ExistVideoThreadZone(int videoThreadId, int videoId);
        #endregion

        #region VIDEO THREAD TAG
        [OperationContract]
        List<VtvVideoThreadTagEntity> GetListVideoThreadTag(int videoThreadId);
        [OperationContract]
        WcfActionResponse InsertVideoThreadTag(VtvVideoThreadTagEntity elm);
        [OperationContract]
        WcfActionResponse DeleteVideoThreadTag(int videoThreadId);
        #endregion

        #region Gift Program
        /// <summary>
        /// Lấy toàn bộ danh sách chương trình quà tặng
        /// </summary>
        /// <param name="keyword">string</param>
        /// <param name="status">int</param>
        /// <param name="pageIndex">int</param>
        /// <param name="pageSize">int</param>
        /// <param name="totalRow">int output</param>
        /// <returns>list Entity</returns>
        [OperationContract]
        List<VTVGiftProgramEntity> ListGiftProgram(string keyword, int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<VTVGiftProgramEntity> ListAllGiftProgram();
        /// <summary>
        /// Lấy thông tin chi tiết chương trình quà tặng
        /// </summary>
        /// <param name="Id">int</param>
        /// <returns>Entity</returns>
        [OperationContract]
        VTVGiftProgramEntity GetGiftProgramById(int Id);
        /// <summary>
        /// Cập nhật thông tin chương trình quà tặng
        /// </summary>
        /// <param name="info">Entity</param>
        /// <returns>true/false</returns>
        [OperationContract]
        WcfActionResponse UpdateGiftProgramInfo(VTVGiftProgramEntity info);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse InsertGiftProgramInfo(VTVGiftProgramEntity info);
        /// <summary>
        /// Xóa thông tin chương trình quà tặng. Kiểm tra nếu là quyền thư ký (ArticleAdmin) thì mới cho phép xóa
        /// </summary>
        /// <param name="id">int</param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse DeleteGiftProgram(int Id);
        #endregion

        #region VTV Gift
        [OperationContract]
        List<VTVGiftEntity> ListGift(string keyword, int giftProgramId, int status, DateTime from, DateTime to,
                                                          int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<VTVGiftEntity> ListGiftAll();
        [OperationContract]
        VTVGiftEntity GetGiftById(int Id);
        [OperationContract]
        WcfActionResponse UpdateGiftInfo(VTVGiftEntity info);
        [OperationContract]
        WcfActionResponse InsertGiftInfo(VTVGiftEntity info);
        [OperationContract]
        WcfActionResponse DeleteGift(int id);
        #endregion

        #region VTV Gift Register
        [OperationContract]
        List<VTVGiftRegisterEntity> ListGiftRegister(string keyword, int giftId, int status, DateTime from, DateTime to, int order,
                                                          int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        VTVGiftRegisterEntity GetGiftRegisterById(int Id);
        [OperationContract]
        WcfActionResponse UpdateGiftRegisterInfo(VTVGiftRegisterEntity info);
        [OperationContract]
        WcfActionResponse DeleteGiftRegister(int id);
        #endregion

        #region Game Show
        [OperationContract]
        List<VTVLiveShowRegisterEntity> ListLiveShowRegister(string keyword, int status,
                                                             int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        WcfActionResponse UpdateLiveShowRegisterInfo(VTVLiveShowRegisterEntity info);

        [OperationContract]
        VTVLiveShowRegisterEntity GetLiveShowRegisterById(int Id);

        [OperationContract]
        WcfActionResponse UpdateStatusLiveShowRegister(VTVLiveShowRegisterEntity info);
        #endregion

        #region Video Record
        [OperationContract]
        WcfActionResponse UpdateStatusVideoRecord(VTVVideoRecordEntity info);

        [OperationContract]
        WcfActionResponse InsertVideoRecord(VTVVideoRecordEntity elm, ref int id);

        [OperationContract]
        List<VTVVideoRecordEntity> ListVideoRecord(string keyword, int programChannel, string accountName, int status,
                                                        DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        List<VTVProgramChannelEntity> ListChannel(int status);

        [OperationContract]
        WcfActionResponse UpdateVideoRecordStatusByChannel(int channelId, int status);

        [OperationContract]
        VTVVideoRecordEntity GetVideoRecordById(int id);

        #endregion
        [OperationContract]
        List<SeaGamesScheduleEntity> GetListSeaGamesSchedule(string keyword, int pageIndex, int pageSize,
                                                             ref int totalRow);

        [OperationContract]
        SeaGamesScheduleEntity GetSeaGamesScheduleById(int id);

        [OperationContract]
        WcfActionResponse UpdateSeaGamesSchedule(SeaGamesScheduleEntity elm);

        [OperationContract]
        WcfActionResponse DeleteSeaGamesSchedule(int id);

        [OperationContract]
        NewsPrRoyaltyEntity GetNewsPrRoyaltyByNewsId(long id);

        [OperationContract]
        WcfActionResponse UpdateNewsPrRoyalty(NewsPrRoyaltyEntity elm);

        [OperationContract]
        List<BoxCountDownChannelEntity> ListBoxCountDown(string keyword, int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<BoxCountDownChannelEntity> ListAllBoxCountDown();
        [OperationContract]
        BoxCountDownChannelEntity GetBoxCountDownById(int Id);
        [OperationContract]
        WcfActionResponse UpdateBoxCountDownInfo(BoxCountDownChannelEntity info);
        [OperationContract]
        WcfActionResponse InsertBoxCountDown(BoxCountDownChannelEntity info);
        [OperationContract]
        WcfActionResponse DeleteBoxCountDown(int Id);
    }
}
