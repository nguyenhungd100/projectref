﻿using ChannelVN.CMS.Common;
using ChannelVN.MagazineContent.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    
    [ServiceContract]
    public interface IMagazineContentServices
    {
        [OperationContract]
        List<MagazineContentEntity> MagazineContent_Search(string keyword, MagazineContentIsHot IsHot, MagazineContentStatus status, string UserName, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        MagazineContentEntity MagazineContent_GetById(int Id);

        [OperationContract]
        WcfActionResponse MagazineContent_Update(MagazineContentEntity entity);

        [OperationContract]
        WcfActionResponse MagazineContent_Insert(MagazineContentEntity entity);

        [OperationContract]
        WcfActionResponse MagazineContent_UpdateStatus(int Id, MagazineContentStatus status, string Username);

        [OperationContract]
        WcfActionResponse MagazineContent_UpdateIsHot(int Id, bool IsHot, string Username);

        [OperationContract]
        WcfActionResponse MagazineContent_Published(int Id, DateTime PublishedDate, string Username);

        [OperationContract]
        WcfActionResponse MagazineContent_Delete(int Id);

    }
}
