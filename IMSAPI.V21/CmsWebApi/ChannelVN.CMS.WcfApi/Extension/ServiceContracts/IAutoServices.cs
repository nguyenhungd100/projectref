﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.Auto.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface IAutoServices : IErrorMessageMapping
    {
        #region Auto

        [OperationContract]
        List<AutoManufacturerEntity> SearchAutoManufacturer(string keyword, EnumAutoManufacturerStatus status, int pageIndex,
                                                        int pageSize, ref int totalRow);
        [OperationContract]
        AutoManufacturerEntity GetAutoManufacturerByAutoManufacturerId(int id);
        [OperationContract]
        WcfActionResponse InsertAutoManufacturer(AutoManufacturerEntity autoManufacturer);
        [OperationContract]
        WcfActionResponse UpdateAutoManufacturer(AutoManufacturerEntity autoManufacturer);
        [OperationContract]
        WcfActionResponse UpdateStatusAutoManufacturer(int id, EnumAutoManufacturerStatus status);
        [OperationContract]
        WcfActionResponse DeleteById(long tagId);
        [OperationContract]
        AlbumInAutoManufacturerEntity GetAlbumInAutoManufacturerByAlbumId(int id);
        [OperationContract]
        WcfActionResponse UpdateAlbumInAutoManufacturer(AlbumInAutoManufacturerEntity albumInAutoManufacturerEntity);
        [OperationContract]
        CMS.Common.WcfActionResponse InsertAutoModel(Auto.Entity.AutoModelEntity autoModel);
        [OperationContract]
        CMS.Common.WcfActionResponse UpdateAutoModel(Auto.Entity.AutoModelEntity autoModel);
        [OperationContract]
        WcfActionResponse DeleteAutoModelById(long tagId);
        [OperationContract]
        AutoModelEntity GetAutoModelByAutoModelId(int id);
        [OperationContract]
        List<Auto.Entity.AutoModelEntity> SearchAutoModel(string keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        AutoModelEntity GetAutoModelByTagId(long id);
        [OperationContract]
        List<AutoManufacturerEntity> GetByParentId(int id);
        [OperationContract]
        List<AutoManufacturerEntity> SearchAutoManufacturerByParent(string keyword, EnumAutoManufacturerStatus status,
                                                                    int parentId, int pageIndex, int pageSize,
                                                                    ref int totalRow);

        #endregion

        [OperationContract]
        List<Auto.Entity.AutoTypeEntity> SearchAutoType(string keyword, int pageIndex, int pageSize,
                                                               ref int totalRow);

        [OperationContract]
        Auto.Entity.AutoTypeEntity GetAutoTypeByAutoTypeId(int id);

        [OperationContract]
        CMS.Common.WcfActionResponse InsertAutoType(Auto.Entity.AutoTypeEntity autoType);

        [OperationContract]
        CMS.Common.WcfActionResponse UpdateAutoType(Auto.Entity.AutoTypeEntity autoType);

        [OperationContract]
        WcfActionResponse DeleteAutoTypeById(long tagId);

        [OperationContract]
        CMS.Common.WcfActionResponse UpdateAvatarAutoModel(Auto.Entity.AutoModelEntity autoModel);

        [OperationContract]
        List<AutoModelEntity> SeachModelEmbed(int type);

        [OperationContract]
        CMS.Common.WcfActionResponse UpdateBoxAutoModel(int type, string lst);

        [OperationContract]
        CMS.Common.WcfActionResponse InsertAlbumInAutoModel(int autoModelId, int autoAlbumId);

        [OperationContract]
        AlbumInAutoManufacturerEntity GetAlbumAutoModelByModelId(int id);

        [OperationContract]
        AutoModelEntity GetAutoModelByName(string name);
    }
}
