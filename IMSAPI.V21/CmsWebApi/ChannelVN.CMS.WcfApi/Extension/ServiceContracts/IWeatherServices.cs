﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.Weather.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface IWeatherServices : IErrorMessageMapping
    {
        [OperationContract]
        WeatherEntity[] GetAllWeather();
        [OperationContract]
        WeatherEntity GetWeatherByDate(DateTime weatherDate);
        [OperationContract]
        WeatherEntity GetWeatherById(int id);
        [OperationContract]
        WcfActionResponse UpdateWeather(WeatherEntity info);
        [OperationContract]
        WcfActionResponse DeleteById(int id);

        [OperationContract]
        ExternalWeatherEntity[] ExternalGetAllWeather();
        [OperationContract]
        ExternalWeatherEntity ExternalGetByCodeId(int id);
    }
}
