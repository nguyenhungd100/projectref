﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChannelVN.LiveGameLeague.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface ILiveGameLeagueServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertLiveGameLeague(LiveGameLeagueEntity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateLiveGameLeague(LiveGameLeagueEntity info);
        [OperationContract]
        WcfActionResponse DeleteLiveGameLeague(int id);
        [OperationContract]
        List<LiveGameLeagueEntity> ListLiveGameLeagueByPages(string search, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        LiveGameLeagueEntity GetLiveGameLeagueById(int Id);
    }
}
