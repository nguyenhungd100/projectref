﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.MC.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMCServices" in both code and config file together.
    [ServiceContract]
    public interface IMCServices
    {
        [OperationContract]
        WcfActionResponse MCInsert(MCEntity expertEntity, ref int expertId);

        [OperationContract]
        MCEntity MCGetById(int id);

        [OperationContract]
        WcfActionResponse MCUpdate(MCEntity expertEntity);

        [OperationContract]
        MCEntity[] ListMCSearch(string keyword);

        [OperationContract]
        MCInNewsEntity[] MC_ListForEditNews(long NewsId);

        [OperationContract]
        WcfActionResponse MCDelete(int id);

        [OperationContract]
        MCEntity MCGetByNewsId(int id);

        [OperationContract]
        WcfActionResponse UpdateMCInNews(MCInNews expertEntity);

        [OperationContract]
        List<MCEntity> GetListMCEmbed(int zoneId, int type);

        [OperationContract]
        WcfActionResponse InsertBoxMCNewsEmbed(BoxMCEmbedEntity newsEmbedBox);

        [OperationContract]
        WcfActionResponse UpdateBoxMCNewsEmbed(string listNewsId, int zoneId, int type);

        [OperationContract]
        WcfActionResponse DeleteBoxMCNewsEmbed(int mcId, int zoneId, int type);

        [OperationContract]
        NewsInListEntity[] SearchMCNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition,
                                                          DateTime distributedDateFrom, DateTime distributedDateTo,
                                                          int pageIndex, int pageSize, string excludeNewsIds,
                                                          int newsType, ref int totalRows);

        [OperationContract]
        List<MCInPhoto> GetListPhotoByMcId(int McId, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        WcfActionResponse UpdatePhotoInMC(int mcId, string addPhotoId, string deletePhotoId);

        [OperationContract]
        WcfActionResponse UpdateNewsInTagNews(int mcId, string deleteNewsId, string addNewsId, int newsType);

        [OperationContract]
        WcfActionResponse MCInNews_Update(string mcIds, long NewsId);
    }
}
