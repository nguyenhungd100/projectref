﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Olympia.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IOlympiaServices" in both code and config file together.
    [ServiceContract]
    public interface IOlympiaServices : IErrorMessageMapping
    {
        #region SCORE
        [OperationContract]
        List<OlympiaScoreEntity> GetListScore(string Keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertScore(OlympiaScoreEntity elm, ref int Id);
        [OperationContract]
        WcfActionResponse UpdateScore(OlympiaScoreEntity elm);
        [OperationContract]
        OlympiaScoreEntity SelectScore(int Id);
        [OperationContract]
        WcfActionResponse DeleteScore(int Id);
        #endregion

        #region PLAYER
        [OperationContract]
        List<OlympiaPlayerEntity> GetListPlayer(string Keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<OlympiaPlayerEntity> GetListPlayerByCompetition(int CompetitionID);
        [OperationContract]
        List<OlympiaPlayerEntity> GetListPlayerByPlayer(int playerId);
        [OperationContract]
        WcfActionResponse InsertPlayer(OlympiaPlayerEntity elm, ref int Id);
        [OperationContract]
        WcfActionResponse UpdatePlayer(OlympiaPlayerEntity elm);
        [OperationContract]
        OlympiaPlayerEntity SelectPlayer(int Id);
        [OperationContract]
        WcfActionResponse DeletePlayer(int Id);
        #endregion

        #region COMPETITION
        [OperationContract]
        List<OlympiaCompetitionEntity> GetListCompetition(string Keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertCompetition(OlympiaCompetitionEntity elm, ref int Id);
        [OperationContract]
        WcfActionResponse InsertResult(OlympiaResultEntity elm, ref int Id);
        [OperationContract]
        WcfActionResponse UpdateCompetition(OlympiaCompetitionEntity elm);
        [OperationContract]
        OlympiaCompetitionEntity SelectCompetition(int Id);
        [OperationContract]
        WcfActionResponse DeleteCompetition(int Id);
        [OperationContract]
        WcfActionResponse DeleteResult(int competitionID);
        #endregion

        #region SCHEDULE
        [OperationContract]
        List<OlympiaScheduleEntity> GetListSchedule(string keyword, int ScheduleType, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertSchedule(OlympiaScheduleEntity elm, ref int Id);
        [OperationContract]
        WcfActionResponse UpdateSchedule(OlympiaScheduleEntity elm);
        [OperationContract]
        OlympiaScheduleEntity SelectSchedule(int Id);
        [OperationContract]
        WcfActionResponse DeleteSchedule(int Id);
        #endregion

        #region CALENDAR
        [OperationContract]
        List<OlympiaCalendarEntity> GetListCalendar(string keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertCalendar(OlympiaCalendarEntity elm, ref int Id);
        [OperationContract]
        WcfActionResponse UpdateCalendar(OlympiaCalendarEntity elm);
        [OperationContract]
        OlympiaCalendarEntity SelectCalendar(int Id);
        [OperationContract]
        WcfActionResponse DeleteCalendar(int Id);
        #endregion

        #region MEDIA

        [OperationContract]
        WcfActionResponse UpdateOlympiaMedia(OlympiaMediaEntity olympiaMedia);
        [OperationContract]
        WcfActionResponse UpdateOlympiaMediaStatus(int id, EnumOlympiaMediaStatus status);
        [OperationContract]
        OlympiaMediaEntity GetOlympiaMediaById(int id);
        [OperationContract]
        OlympiaMediaEntity[] SearchOlympiaMedia(string keyword, int year, int quarter, int month, int week,
                                                EnumOlympiaMediaType mediaType, EnumOlympiaMediaStatus status,
                                                int pageIndex, int pageSize, ref int totalRow);
        #endregion

        #region WINNER

        [OperationContract]
        List<OlympiaWinnerEntity> SearchOlympiaWinner(string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        OlympiaWinnerEntity GetOlympiaWinnerById(int id);

        [OperationContract]
        WcfActionResponse UpdateOlympiaWinner(OlympiaWinnerEntity olympiaWinner);

        [OperationContract]
        WcfActionResponse DeleteOlympiaWinnerById(int id);

        #endregion

        #region FAQ
        [OperationContract]
        List<OlympiaFaqEntity> ListFaq(string keyword,int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertFaq(OlympiaFaqEntity elm, ref Int64 id);
        [OperationContract]
        WcfActionResponse UpdateFaq(OlympiaFaqEntity elm);
        [OperationContract]
        OlympiaFaqEntity SelectFaq(Int64 id);
        [OperationContract]
        WcfActionResponse DeleteFaq(int id);
        #endregion
    }
}
