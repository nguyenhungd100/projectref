﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Quiz.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ServiceContract]
    public interface IQuizServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertQuiz(QuizEntity quiz);

        [OperationContract]
        WcfActionResponse UpdateQuiz(QuizEntity quiz);

        [OperationContract]
        WcfActionResponse UpdateQuizConfig(int id, string quizData, string quizConfig);

        [OperationContract]
        WcfActionResponse QuizPublish(int id, string quizData, string quizConfig);

        [OperationContract]
        QuizEntity GetQuizById(int id);

        [OperationContract]
        List<QuizEntity> SearchQuiz(string keyword, EnumQuizType type, string createdBy, int status, int siteId,
            int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        WcfActionResponse InsertQuizResult(QuizResultEntity quiz);

        [OperationContract]
        QuizResultEntity GetQuizResultByAccount(long id, long quizId, string email, string phone, string facebook,
           string google, string vietId);

        [OperationContract]
        List<QuizResultEntity> SearchQuizResult(long quizId, string phone, string facebook, string google, string vietId,
            int pageIndex, int pageSize, ref int totalRow);

    }
}
