﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Magazine.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IMagazineServices : IErrorMessageMapping
    {
        #region Magazine
        [OperationContract]
        MagazineEntity GetMagazineById(int Id);
        [OperationContract]
        WcfActionResponse InsertMagazine(MagazineEntity entity, ref int newId);
        [OperationContract]
        WcfActionResponse UpdateMagazine(MagazineEntity entity);
        [OperationContract]
        WcfActionResponse DeleteMagazine(int magazineId);
        [OperationContract]
        MagazineEntity[] MagazineSearch(string keyword, int type, EnumMagazineStatus status, int pageIndex, int pageSize, ref int totalRow);
        #endregion

        #region Magazine News
        [OperationContract]
        WcfActionResponse InsertMagazineNews(MagazineNewsEntity entity, ref int newId);
        [OperationContract]
        WcfActionResponse DeleteMagazineNews(int Id);
        [OperationContract]
        WcfActionResponse UpdateMagazineNews(int magazineId, string deleteNewsId, string addNewsId);
        [OperationContract]
        MagazineNewsEntity[] MagazineNewsSearch(string keyword);
        #endregion
    }
}
