﻿using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.DynamicLayout.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IDynamicLayoutServices : IErrorMessageMapping
    {
        #region Pages

        [OperationContract]
        PageEntity[] GetAllPages();
        [OperationContract]
        WcfActionResponse AddModuleIntoPlaceHolder(ModuleInPageEntity moduleInPage);
        [OperationContract]
        ModuleInPageEntity[] GetByPlaceHolderName(string placeHolderName, int pageId);
        [OperationContract]
        WcfActionResponse UpdateModuleInPagePriority(string listOfPriority);
        [OperationContract]
        WcfActionResponse UpdateModuleInPageSkinSrc(int moduleInPageId, string skinSrc);
        [OperationContract]
        PageWithFullModuleEntity GetPageDetailByPageId(int pageId);
        [OperationContract]
        PageWithFullModuleEntity GetPageDetailByPageSrc(string pageSrc);

        #endregion

        #region Modules

        [OperationContract]
        ModuleEntity[] GetAllModules();
        [OperationContract]
        ModuleConfigurationEntity[] GetByModuleInPageId(int moduleInPageId);
        [OperationContract]
        WcfActionResponse InsertModuleConfig(ModuleConfigurationEntity moduleConfigurationEntity, ref int moduleConfigId);
        [OperationContract]
        WcfActionResponse UpdateModuleConfig(ModuleConfigurationEntity moduleConfigurationEntity);
        [OperationContract]
        WcfActionResponse InsertModule(ModuleEntity module);
        [OperationContract]
        WcfActionResponse DeleteModuleInPage(int doduleInPageId);
        [OperationContract]
        ModuleInPageWithConfigEntity[] GetAllModuleWithConfigByPlaceHolder(string placeHolderName, int pageId);
        [OperationContract]
        ModuleInPageWithConfigEntity GetModuleWithConfigByModuleInPageId(int moduleInPageId);
        [OperationContract]
        FullModuleInPageWithConfigEntity GetFullModuleWithConfigByModuleInPageId(int moduleInPageId);

        #endregion
    }
}
