﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Dantri.Entity;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDantriServices" in both code and config file together.
    [ServiceContract]
    public interface IDantriServices
    {
        [OperationContract]
        List<CommentUserEntity> Search(string userName, int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse UpdateCommentUser(CommentUserEntity commentUser);
        [OperationContract]
        WcfActionResponse UpdateStatusCommentUser(CommentUserEntity commentUser);
        [OperationContract]
        CommentUserEntity GetById(long id);
        #region NewsRoyaltyInfo
        [OperationContract]
        NewsRoyaltyInfoEntity GetNewsRoyaltyInfoByNewsId(long id);
        [OperationContract]
        WcfActionResponse InsertNewsRoyaltyInfo(NewsRoyaltyInfoEntity NewsRoyaltyInfo);
        [OperationContract]
        WcfActionResponse UpdateNewsRoyaltyInfo(NewsRoyaltyInfoEntity NewsRoyaltyInfo);
        [OperationContract]
        WcfActionResponse DeleteNewsRoyaltyInfoByNewsId(long tagId);
        #endregion
        #region NewsRoyaltyAuthor
        [OperationContract]
        List<NewsRoyaltyAuthorEntity> GetNewsRoyaltyAuthorByNewsId(long id);
        [OperationContract]
        WcfActionResponse InsertNewsRoyaltyAuthor(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor);
        [OperationContract]
        WcfActionResponse UpdateNewsRoyaltyAuthor(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor);
        [OperationContract]
        WcfActionResponse DeleteNewsRoyaltyAuthorByNewsId(long tagId);
        [OperationContract]
        WcfActionResponse DeleteNewsRoyaltyAuthorById(long tagId);
        [OperationContract]
        WcfActionResponse DeleteNewsRoyaltyInfoById(long tagId);
        #endregion


        #region VideoRoyaltyInfo
        [OperationContract]
        VideoRoyaltyInfoEntity GetVideoRoyaltyInfoByVideoId(long id);
        [OperationContract]
        WcfActionResponse InsertVideoRoyaltyInfo(VideoRoyaltyInfoEntity VideoRoyaltyInfo);
        [OperationContract]
        WcfActionResponse UpdateVideoRoyaltyInfo(VideoRoyaltyInfoEntity VideoRoyaltyInfo);
        [OperationContract]
        WcfActionResponse DeleteVideoRoyaltyInfoByVideoId(long tagId);
        #endregion
        #region VideoRoyaltyAuthor
        [OperationContract]
        List<VideoRoyaltyAuthorEntity> GetVideoRoyaltyAuthorByVideoId(long id);
        [OperationContract]
        WcfActionResponse InsertVideoRoyaltyAuthor(VideoRoyaltyAuthorEntity VideoRoyaltyAuthor);
        [OperationContract]
        WcfActionResponse UpdateVideoRoyaltyAuthor(VideoRoyaltyAuthorEntity VideoRoyaltyAuthor);
        [OperationContract]
        WcfActionResponse DeleteVideoRoyaltyAuthorByVideoId(long tagId);
        [OperationContract]
        WcfActionResponse DeleteVideoRoyaltyAuthorById(long tagId);
        [OperationContract]
        WcfActionResponse DeleteVideoRoyaltyInfoById(long tagId);
        #endregion
    }
}
