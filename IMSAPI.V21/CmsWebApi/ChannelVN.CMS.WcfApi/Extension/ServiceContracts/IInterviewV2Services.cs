﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.InterviewV2.Entity;
using ChannelVN.CMS.Common;
using System;


namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface IInterviewV2Services : IErrorMessageMapping
    {
        #region Respondent services
        [OperationContract]
        WcfActionResponse InsertRespondent(RespondentEntity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateRespondent(RespondentEntity info);
        [OperationContract]
        WcfActionResponse DeleteRespondent(int respondentId);
        [OperationContract]
        List<RespondentEntity> ListRespondentByInterView(int interviewId);
        [OperationContract]
        List<RespondentEntity> ListRespondentByPagesize(int interviewId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        RespondentEntity GetRespondentById(int respondentId);
        #endregion
        #region Question services
        [OperationContract]
        List<QuestionEntity> ListQuestionRespondentInterView(int interviewId, int respondentId, EnumStatusQuestion status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        QuestionEntity GetQuestionById(int questionId);
        [OperationContract]
        WcfActionResponse Answer(string AnswerContent, EnumStatusQuestion status, int Id);
        [OperationContract]
        WcfActionResponse InsertQuestion(QuestionEntity questionInfo, ref int id);
        [OperationContract]
        WcfActionResponse UpdateQuestion(QuestionEntity questionInfo);
        [OperationContract]
        WcfActionResponse DeleteQuestion(int id);
        [OperationContract]
        List<QuestionEntity> ListQuestionByAnswerd(int interviewId, EnumStatusQuestion status,int orderBy, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse UpdatePriority(int interviewId, string listOfInterviewQuestionId);
        [OperationContract]
        WcfActionResponse InsertAnswer(string answerContent, int interviewId, EnumStatusQuestion status, ref int id);
        #endregion
        #region Interview
        [OperationContract]
        InterviewV2Entity SelectInterviewByNewsId(long newsid);
        [OperationContract]
        InterviewV2Entity SelectInterviewById(int interviewId);
        [OperationContract]
        WcfActionResponse InsertInterview(InterviewV2Entity info, ref int id);
        [OperationContract]
        WcfActionResponse UpdateStatusInterview(int id, EnumStatusInterview status);
        #endregion
        #region LogAction
        [OperationContract]
        WcfActionResponse InsertLog(int interviewId, string userName, string action, ref int id);
        [OperationContract]
        List<LogActionEntity> ListLog(int interviewId, int pageIndex, int pageSize, ref int totalRow);
        #endregion
    }
}
