﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.GroupQuestion;

namespace ChannelVN.CMS.WcfApi.Extension
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IQuestionAnswerServices" in both code and config file together.
    [ServiceContract]
    public interface IQuestionAnswerServices
    {
        [OperationContract]
        List<GroupQuestionEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<QuestionEntity> GetListQuestionByGroup(string keyword, int groupId, int pageIndex, int pageSize,
                                                    ref int totalRow);
        [OperationContract]
        List<QuestionEntity> GetListQuestionByGroupEvent(string keyword, int groupId, int eventId, int pageIndex, int pageSize,
                                                    ref int totalRow);
        [OperationContract]
        List<AnswerEntity> GetListAnswer(int questionId);

        [OperationContract]
        QuestionEntity GetQuestionById(int questionId);

        [OperationContract]
        WcfActionResponse UpdateQuestion(QuestionEntity news);

        [OperationContract]
        WcfActionResponse InsertAnswer(AnswerEntity news, ref int id);

        [OperationContract]
        WcfActionResponse InsertQuestion(QuestionEntity news, ref int id);

        [OperationContract]
        WcfActionResponse DeleteQuestion(int id);

        [OperationContract]
        List<EventQuestionEntity> SearchEvent(string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        WcfActionResponse InsertQuestionInEvent(int questionId, int eventId);

        [OperationContract]
        WcfActionResponse InsertEventQuestion(EventQuestionEntity news, ref int id);

        [OperationContract]
        WcfActionResponse InsertGroupQuestion(GroupQuestionEntity news, ref int id);

        [OperationContract]
        WcfActionResponse UpdateGroupQuestion(GroupQuestionEntity news);

        [OperationContract]
        WcfActionResponse UpdateEventQuestion(EventQuestionEntity news);
        [OperationContract]
        WcfActionResponse DeleteGroupQuestion(int id);
        [OperationContract]
        WcfActionResponse DeleteEventQuestion(int id);

        [OperationContract]
        GroupQuestionEntity GetGroupQuestionById(int questionId);

        [OperationContract]
        EventQuestionEntity GetEventQuestionById(int questionId);

        [OperationContract]
        WcfActionResponse DeleteAnswer(int id);

        [OperationContract]
        WcfActionResponse UpdateAnswer(AnswerEntity news);

        [OperationContract]
        List<QuestionEntity> GetListQuestionBefore(string keyword, int groupId, int eventId, int pageIndex, int pageSize,
                                                   ref int totalRow);
    }
}
