﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.TraBenh.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDiseaseServices" in both code and config file together.
    [ServiceContract]
    public interface IDiseaseServices
    {
        #region GroupDisease
        [OperationContract]
        List<GroupDiseaseEntity> GroupDiseaseGetList(string keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<GroupDiseaseEntity> GroupDiseaseGetAll();
        [OperationContract]
        WcfActionResponse UpdateGroupDisease(GroupDiseaseEntity info, ref int id);
        [OperationContract]
        GroupDiseaseEntity GetGroupDiseaseById(int Id);
        [OperationContract]
        WcfActionResponse DeleteGroupDiseaseById(int id);
        #endregion

        #region ExpertOnTheDisease
        [OperationContract]
        List<ExpertOnTheDiseaseEntity> ExpertOnTheDiseaseGetList(string keyword, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse UpdateExpertOnTheDisease(ExpertOnTheDiseaseEntity info, ref int id);
        [OperationContract]
        ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseById(int Id);
        [OperationContract]
        WcfActionResponse DeleteExpertOnTheDiseaseById(int id);
        [OperationContract]
        List<ExpertOnTheDiseaseEntity> GetAllListExpertOnTheDisease();
        [OperationContract]
        ExpertOnTheDiseaseEntity GetExpertOnTheDiseaseByNewsId(long id);
        #endregion

        #region Disease

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo DisaeseId. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="diseaseId"></param>
        /// <returns></returns>
        [OperationContract]
        DiseaseEntity DiseaseGetById(int diseaseId);
        /// <summary>
        /// NANIA
        /// 2015-05-18
        /// Gets danh sách bệnh theo danh sách IDs
        /// </summary>
        /// <param name="diseaseIdList"></param>
        /// <returns></returns>

        [OperationContract]
        List<DiseaseEntity> DiseaseGetListByIds(string diseaseIdList);

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets chi tiết thông tin bệnh theo [Name]. Đã bao gồm cả thông tin mở rộng từ DiseaseStaticInfo
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [OperationContract]
        DiseaseEntity DiseaseGetByName(string name);


        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Gets danh sách thông tin bệnh có lọc theo điều kiện
        /// </summary>
        /// <param name="keyword">Từ khóa: search theo tên bệnh</param>
        /// <param name="status">Trạng thái</param>
        /// <param name="type">Loại bệnh</param>
        /// <param name="mode">(Vị trí trên trang)</param>
        /// <param name="groupId">Nhóm bệnh</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow">CÓ OUTPUT</param>
        /// <returns></returns>
        [OperationContract]
        List<DiseaseEntity> DiseaseGetList(string keyword, int status, int type, int mode, int groupId,
            int pageIndex, int pageSize, ref int totalRow);

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Trả kết quả suggest theo từ khóa hoặc chữ cái đầu tiên của bệnh
        /// </summary>
        /// <param name="firstChar"></param>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        [OperationContract]
        List<DiseaseEntity> DiseaseSuggest(string firstChar, string keyword, int top);

        /// <summary>
        /// NANIA
        /// 2015-05-14
        /// Cập nhật thông tin bệnh, bao gồm cả thông tin mở rộng: DiseaseStaticInfo
        /// </summary>
        /// <param name="diseaseEntity"></param>
        /// <param name="newsRelationIds"></param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse UpdateDisease(DiseaseEntity diseaseEntity, string newsRelationIds);

        /// <summary>
        /// Get Name, Id thông tin bệnh gắn vào bài - ngocnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        [OperationContract]
        List<DiseaseEntity> DiseaseGetByNewsId(long newsId);

        /// <summary>
        /// Gets danh sách news trong Disease
        /// </summary>
        /// <param name="diseaseId"></param>
        /// <returns></returns>
        [OperationContract]
        List<NewsInDisease> DiseaseGetNewsDiseaseId(int diseaseId);

        /// <summary>
        /// Insert Bệnh vào tin theo list Id Bệnh
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="listDiseases"></param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse UpdateDiseaseInNews(long newsId, string listDiseases);

        /// <summary>
        /// Ngocnh
        /// 22/5/2015
        /// Cập nhật chuyên gia của bệnh vào tin
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="expertId"></param>
        /// <returns></returns>
        [OperationContract]
        WcfActionResponse UpdateExpertOnDiseaseInNews(long newsId, int expertId);

        #endregion
        
        #region Menu

        [OperationContract]
        List<MenuEntity> GetListMenuByParent(int parentId);
        [OperationContract]
        List<MenuEntity> GetAllOfMenu();
        [OperationContract]
        MenuEntity GetMenuDetails(int id);
        [OperationContract]
        int UpdateMenu(MenuEntity menuEntity);
        [OperationContract]
        WcfActionResponse DeleteMenu(int id);


        #endregion
    }
}
