﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.TheGuide.Entity;


namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    interface ITheGuideServices : IErrorMessageMapping
    {

        #region BreakingNews
        [OperationContract]
        List<BreakingNewsEntity> GetBreakingNewsSearch(string keyword, EnumBreakingNewsStatus status,
            EnumBreakingNewsType type, int zoneId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        BreakingNewsEntity GetBreakingNewsById(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsDelete(int id);
        [OperationContract]
        WcfActionResponse UpdateBreakingNews(BreakingNewsEntity breakingNewsEntity, string tagIdList);
        #endregion

        #region BreakingNewsLocation

        [OperationContract]
        WcfActionResponse UpdateBreakingNewsLocation(BreakingNewsLocationEntity BreakingNewsLocationEntity);
        [OperationContract]
        List<BreakingNewsLocationEntity> BreakingNewsLocationGetAll(string keyword, int status);
        [OperationContract]
        BreakingNewsLocationEntity BreakingNewsLocationGetById(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsLocationDelete(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsLocationMoveUp(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsLocationMoveDown(int id);
        #endregion

        #region BreakingNewsEvent
        [OperationContract]
        WcfActionResponse UpdateBreakingNewsEvent(BreakingNewsEntity breakingNewsEntity, BreakingNewsEventEntity breakingNewsEventEntity, string tagIdList);
        [OperationContract]
        BreakingNewsEventEntity GetBreakingNewsEventById(int id);
        [OperationContract]
        BreakingNewsEventEntity GetBreakingNewsEventByBreakingNewsId(int id);
        //=============================
        [OperationContract]
        WcfActionResponse UpdateBreakingNewsEventLocation(BreakingNewsEventLocationEntity breakingNewsEventLocationEntity);
        [OperationContract]
        List<BreakingNewsEventLocationEntity> BreakingNewsEventLocationGetAll(string keyword, int status, int breakingNewsLocationId);
        [OperationContract]
        BreakingNewsEventLocationEntity BreakingNewsEventLocationGetById(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsEventLocationDelete(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsEventLocationMoveUp(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsEventLocationMoveDown(int id);

        //=============================
        [OperationContract]
        WcfActionResponse UpdateBreakingNewsEventType(BreakingNewsEventTypeEntity breakingNewsEventTypeEntity);
        [OperationContract]
        List<BreakingNewsEventTypeEntity> BreakingNewsEventTypeGetAll(string keyword, int status);
        [OperationContract]
        BreakingNewsEventTypeEntity BreakingNewsEventTypeGetById(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsEventTypeDelete(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsEventTypeMoveUp(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsEventTypeMoveDown(int id);
        #endregion

        #region BreakingNewsPromotion
        [OperationContract]
        WcfActionResponse UpdateBreakingNewsPromotion(BreakingNewsEntity breakingNewsEntity, BreakingNewsPromotionEntity breakingNewsPromotionEntity, string tagIdList);

        [OperationContract]
        BreakingNewsPromotionEntity GetBreakingNewsPromotionById(int id);
        [OperationContract]
        BreakingNewsPromotionEntity GetBreakingNewsPromotionByBreakingNewsId(int id);

        //=============================
        [OperationContract]
        WcfActionResponse UpdateBreakingNewsPromotionBusinessLocation(BreakingNewsPromotionBusinessLocationEntity breakingNewsPromotionBusinessLocationEntity);
        [OperationContract]
        List<BreakingNewsPromotionBusinessLocationEntity> BreakingNewsPromotionBusinessLocationGetAll(string keyword, int status, int breakingNewsLocationId);
        [OperationContract]
        BreakingNewsPromotionBusinessLocationEntity BreakingNewsPromotionBusinessLocationGetById(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionBusinessLocationDelete(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionBusinessLocationMoveUp(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionBusinessLocationMoveDown(int id);
        //=============================
        [OperationContract]
        WcfActionResponse UpdateBreakingNewsPromotionLocation(BreakingNewsPromotionLocationEntity breakingNewsPromotionLocationEntity);
        [OperationContract]
        List<BreakingNewsPromotionLocationEntity> BreakingNewsPromotionLocationGetAll(string keyword, int status);
        [OperationContract]
        BreakingNewsPromotionLocationEntity BreakingNewsPromotionLocationGetById(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionLocationDelete(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionLocationMoveUp(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionLocationMoveDown(int id);
        //=============================
        [OperationContract]
        WcfActionResponse UpdateBreakingNewsPromotionType(BreakingNewsPromotionTypeEntity breakingNewsPromotionTypeEntity);
        [OperationContract]
        List<BreakingNewsPromotionTypeEntity> BreakingNewsPromotionTypeGetAll(string keyword, int status);
        [OperationContract]
        BreakingNewsPromotionTypeEntity BreakingNewsPromotionTypeGetById(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionTypeDelete(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionTypeMoveUp(int id);
        [OperationContract]
        WcfActionResponse BreakingNewsPromotionTypeMoveDown(int id);
        #endregion

        #region BreakingNewsTag
        [OperationContract]
        List<BreakingNewsTagWithTagInfoEntity> BreakingNewsTagGetByBreakingNewsId(int breakingNewsId);

        #endregion
    }
}
