﻿using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Emailer.Entity;

namespace ChannelVN.CMS.WcfApi.Extension.ServiceContracts
{
    [ServiceContract]
    public interface IEmailerServices : IErrorMessageMapping
    {
        #region Email

        [OperationContract]
        WcfActionResponse EmailerInsert(EmailEntity email);
        [OperationContract]
        WcfActionResponse EmailerUpdate(EmailEntity email);
        [OperationContract]
        EmailEntity EmailerGetById(string email);
        [OperationContract]
        EmailEntity[] EmailerGetList(string keyword, int status, int groupId, int pageIndex, int pageSize, ref int totalRow);

        #endregion

        #region Email Group

        [OperationContract]
        WcfActionResponse EmailerGroupInsert(EmailGroupEntity emailGroupEntity);
        [OperationContract]
        WcfActionResponse EmailerGroupUpdate(EmailGroupEntity emailGroupEntity);
        [OperationContract]
        EmailGroupEntity EmailerGroupGetById(int id);
        [OperationContract]
        EmailGroupEntity[] EmailerGroupGetList(string keyword, int status, int pageIndex, int pageSize, ref int totalRow);

        #endregion

        #region Email Queue

        [OperationContract]
        WcfActionResponse EmailerQueueInsert(EmailQueueEntity emailQueueEntity);
        [OperationContract]
        WcfActionResponse EmailerQueueUpdateStatus(long newsId);
        [OperationContract]
        WcfActionResponse EmailerQueueUpdateStatusById(int id, int status);
        [OperationContract]
        EmailQueueEntity[] EmailerQueueGetListTop(int top);

        #endregion
    }
}
