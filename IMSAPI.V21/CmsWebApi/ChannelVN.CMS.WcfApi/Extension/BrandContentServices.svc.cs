﻿using ChannelVN.BrandContent.Bo;
using ChannelVN.BrandContent.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System.ServiceModel.Activation;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BrandContentServices : IBrandContentServices
    {
        public WcfActionResponse BrandContent_Insert(BrandContentEntity BrandContent)
        {
            var errorCode = BrandContentBo.BrandContent_Insert(BrandContent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse BrandContent_Update(BrandContentEntity BrandContent)
        {
            var errorCode = BrandContentBo.BrandContent_Update(BrandContent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(BrandContent), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public BrandContentEntity[] BrandContent_Search(string Keyword, int pageIndex, int pageSize, ref int TotalRow)
        {
            return BrandContentBo.BrandContent_Search(Keyword, pageIndex, pageSize, ref TotalRow).ToArray();
        }
        public BrandContentEntity[] BrandInNews_GetByNewsId(long NewsId)
        {
            return BrandContentBo.BrandInNews_GetByNewsId(NewsId).ToArray();
        }
        public BrandContentEntity BrandContent_GetById(int BrandId)
        {
            return BrandContentBo.BrandContent_GetById(BrandId);
        }
        public WcfActionResponse BrandInNews_Insert(string brandIds, long NewsId)
        {
            var errorCode = BrandContentBo.BrandInNews_Insert(brandIds, NewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse BrandContent_Delete(int id)
        {
            var errorCode = BrandContentBo.BrandContent_Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

    }
}
