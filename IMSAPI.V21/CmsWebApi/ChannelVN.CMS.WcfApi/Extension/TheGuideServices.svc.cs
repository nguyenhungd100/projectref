﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.TheGuide.Bo;
using ChannelVN.TheGuide.Entity;
using ChannelVN.TheGuide.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TheGuideServices : ITheGuideServices
    {
        public List<BreakingNewsEntity> GetBreakingNewsSearch(string keyword, EnumBreakingNewsStatus status, EnumBreakingNewsType type, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            return BreakingNewsBo.Search(keyword, status, type, zoneId, pageIndex, pageSize, ref  totalRow);
        }
        public BreakingNewsEntity GetBreakingNewsById(int id)
        {
            return BreakingNewsBo.GetById(id);
        }
        public WcfActionResponse BreakingNewsDelete(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BreakingNewsBo.Delete(id);
                if (errorCode != ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode,
                        ErrorMapping.Current[errorCode]);
                }
                else
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateBreakingNews(BreakingNewsEntity breakingNewsEntity, string tagIdList)
        {
            try
            {
                if (breakingNewsEntity.Id > 0)
                {
                    BreakingNewsBo.Update(breakingNewsEntity, tagIdList);
                }
                else
                {
                    var id = 0;
                    BreakingNewsBo.Insert(breakingNewsEntity, tagIdList, ref id);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        #region BreakingNewsLocation
        public WcfActionResponse UpdateBreakingNewsLocation(BreakingNewsLocationEntity BreakingNewsLocationEntity)
        {
            try
            {
                if (BreakingNewsLocationEntity.Id > 0)
                {
                    BreakingNewsBo.BreakingNewsLocationUpdate(BreakingNewsLocationEntity);
                }
                else
                {
                    BreakingNewsBo.BreakingNewsLocationInsert(BreakingNewsLocationEntity);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public List<BreakingNewsLocationEntity> BreakingNewsLocationGetAll(string keyword, int status)
        {
            return BreakingNewsBo.BreakingNewsLocationGetAll(keyword, status);
        }
        public BreakingNewsLocationEntity BreakingNewsLocationGetById(int id)
        {
            return BreakingNewsBo.BreakingNewsLocationGetById(id);
        }
        public WcfActionResponse BreakingNewsLocationDelete(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BreakingNewsBo.BreakingNewsLocationDelete(id);
                if (errorCode != ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode,
                        ErrorMapping.Current[errorCode]);
                }
                else
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse BreakingNewsLocationMoveUp(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsBo.BreakingNewsLocationMoveUp(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse BreakingNewsLocationMoveDown(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsBo.BreakingNewsLocationMoveDown(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
        #region BreakingNewsEvent
        public WcfActionResponse UpdateBreakingNewsEvent(BreakingNewsEntity breakingNewsEntity, BreakingNewsEventEntity breakingNewsEventEntity, string tagIdList)
        {
            try
            {
                if (breakingNewsEntity.Id > 0)
                {
                    BreakingNewsBo.Update(breakingNewsEntity, tagIdList);
                    breakingNewsEventEntity.BreakingNewsId = breakingNewsEntity.Id;
                    if (breakingNewsEventEntity.Id > 0)
                    {
                        BreakingNewsEventBo.Update(breakingNewsEventEntity);    
                    }
                    else
                    {
                        BreakingNewsEventBo.Insert(breakingNewsEventEntity);
                    }
                }
                else
                {
                    var id = 0;
                    BreakingNewsBo.Insert(breakingNewsEntity, tagIdList, ref id);
                    breakingNewsEventEntity.BreakingNewsId = id;
                    BreakingNewsEventBo.Insert(breakingNewsEventEntity);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public BreakingNewsEventEntity GetBreakingNewsEventById(int id)
        {
            return BreakingNewsEventBo.GetById(id);
        }
        public BreakingNewsEventEntity GetBreakingNewsEventByBreakingNewsId(int id)
        {
            return BreakingNewsEventBo.GetByBreakingNewsId(id);
        }
        //=============================================
        public WcfActionResponse UpdateBreakingNewsEventLocation(BreakingNewsEventLocationEntity breakingNewsEventLocationEntity)
        {
            try
            {
                if (breakingNewsEventLocationEntity.Id > 0)
                {
                    BreakingNewsEventBo.BreakingNewsEventLocationUpdate(breakingNewsEventLocationEntity);
                }
                else
                {
                    BreakingNewsEventBo.BreakingNewsEventLocationInsert(breakingNewsEventLocationEntity);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public List<BreakingNewsEventLocationEntity> BreakingNewsEventLocationGetAll(string keyword, int status, int breakingNewsLocationId)
        {
            return BreakingNewsEventBo.BreakingNewsEventLocationGetAll(keyword, status, breakingNewsLocationId);
        }
        public BreakingNewsEventLocationEntity BreakingNewsEventLocationGetById(int id)
        {
            return BreakingNewsEventBo.BreakingNewsEventLocationGetById(id);
        }
        public WcfActionResponse BreakingNewsEventLocationDelete(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BreakingNewsEventBo.BreakingNewsEventLocationDelete(id);
                if (errorCode != ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode,
                        ErrorMapping.Current[errorCode]);
                }
                else
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse BreakingNewsEventLocationMoveUp(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsEventBo.BreakingNewsEventLocationMoveUp(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse BreakingNewsEventLocationMoveDown(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsEventBo.BreakingNewsEventLocationMoveDown(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        //=============================================
        public WcfActionResponse UpdateBreakingNewsEventType(BreakingNewsEventTypeEntity breakingNewsEventTypeEntity)
        {
            try
            {
                if (breakingNewsEventTypeEntity.Id > 0)
                {
                    BreakingNewsEventBo.BreakingNewsEventTypeUpdate(breakingNewsEventTypeEntity);
                }
                else
                {
                    BreakingNewsEventBo.BreakingNewsEventTypeInsert(breakingNewsEventTypeEntity);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public List<BreakingNewsEventTypeEntity> BreakingNewsEventTypeGetAll(string keyword, int status)
        {
            return BreakingNewsEventBo.BreakingNewsEventTypeGetAll(keyword, status);
        }
        public BreakingNewsEventTypeEntity BreakingNewsEventTypeGetById(int id)
        {
            return BreakingNewsEventBo.BreakingNewsEventTypeGetById(id);
        }
        public WcfActionResponse BreakingNewsEventTypeDelete(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BreakingNewsEventBo.BreakingNewsEventTypeDelete(id);
                if (errorCode != ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode,
                        ErrorMapping.Current[errorCode]);
                }
                else
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse BreakingNewsEventTypeMoveUp(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsEventBo.BreakingNewsEventTypeMoveUp(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse BreakingNewsEventTypeMoveDown(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsEventBo.BreakingNewsEventTypeMoveDown(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
        #region BreakingNewsPromotion
        public WcfActionResponse UpdateBreakingNewsPromotion(BreakingNewsEntity breakingNewsEntity, BreakingNewsPromotionEntity breakingNewsPromotionEntity, string tagIdList)
        {
            try
            {
                if (breakingNewsEntity.Id > 0)
                {
                    BreakingNewsBo.Update(breakingNewsEntity, tagIdList);
                    breakingNewsPromotionEntity.BreakingNewsId = breakingNewsEntity.Id;
                    if (breakingNewsPromotionEntity.Id>0)
                    {
                        BreakingNewsPromotionBo.Update(breakingNewsPromotionEntity);    
                    }
                    else
                    {
                        BreakingNewsPromotionBo.Insert(breakingNewsPromotionEntity);
                    }
                    
                }
                else
                {
                    var id = 0;
                    BreakingNewsBo.Insert(breakingNewsEntity, tagIdList, ref id);
                    breakingNewsPromotionEntity.BreakingNewsId = id;
                    BreakingNewsPromotionBo.Insert(breakingNewsPromotionEntity);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public BreakingNewsPromotionEntity GetBreakingNewsPromotionById(int id)
        {
            return BreakingNewsPromotionBo.GetById(id);
        }
        public BreakingNewsPromotionEntity GetBreakingNewsPromotionByBreakingNewsId(int id)
        {
            return BreakingNewsPromotionBo.GetByBreakingNewsId(id);
        }
        //=============================================
        public WcfActionResponse UpdateBreakingNewsPromotionBusinessLocation(BreakingNewsPromotionBusinessLocationEntity breakingNewsPromotionBusinessLocationEntity)
        {
            try
            {
                if (breakingNewsPromotionBusinessLocationEntity.Id > 0)
                {
                    BreakingNewsPromotionBo.BreakingNewsPromotionBusinessLocationUpdate(breakingNewsPromotionBusinessLocationEntity);
                }
                else
                {
                    BreakingNewsPromotionBo.BreakingNewsPromotionBusinessLocationInsert(breakingNewsPromotionBusinessLocationEntity);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public List<BreakingNewsPromotionBusinessLocationEntity> BreakingNewsPromotionBusinessLocationGetAll(string keyword, int status, int breakingNewsLocationId)
        {
            return BreakingNewsPromotionBo.BreakingNewsPromotionBusinessLocationGetAll(keyword, status, breakingNewsLocationId);
        }
        public BreakingNewsPromotionBusinessLocationEntity BreakingNewsPromotionBusinessLocationGetById(int id)
        {
            return BreakingNewsPromotionBo.BreakingNewsPromotionBusinessLocationGetById(id);
        }
        public WcfActionResponse BreakingNewsPromotionBusinessLocationDelete(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BreakingNewsPromotionBo.BreakingNewsPromotionBusinessLocationDelete(id);
                if (errorCode != ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode,
                        ErrorMapping.Current[errorCode]);
                }
                else
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse BreakingNewsPromotionBusinessLocationMoveUp(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsPromotionBo.BreakingNewsPromotionBusinessLocationMoveUp(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse BreakingNewsPromotionBusinessLocationMoveDown(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsPromotionBo.BreakingNewsPromotionBusinessLocationMoveDown(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        //=============================================
        public WcfActionResponse UpdateBreakingNewsPromotionLocation(BreakingNewsPromotionLocationEntity breakingNewsPromotionLocationEntity)
        {
            try
            {
                if (breakingNewsPromotionLocationEntity.Id > 0)
                {
                    BreakingNewsPromotionBo.BreakingNewsPromotionLocationUpdate(breakingNewsPromotionLocationEntity);
                }
                else
                {
                    BreakingNewsPromotionBo.BreakingNewsPromotionLocationInsert(breakingNewsPromotionLocationEntity);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public List<BreakingNewsPromotionLocationEntity> BreakingNewsPromotionLocationGetAll(string keyword, int status)
        {
            return BreakingNewsPromotionBo.BreakingNewsPromotionLocationGetAll(keyword, status);
        }
        public BreakingNewsPromotionLocationEntity BreakingNewsPromotionLocationGetById(int id)
        {
            return BreakingNewsPromotionBo.BreakingNewsPromotionLocationGetById(id);
        }
        public WcfActionResponse BreakingNewsPromotionLocationDelete(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BreakingNewsPromotionBo.BreakingNewsPromotionLocationDelete(id);
                if (errorCode != ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode,
                        ErrorMapping.Current[errorCode]);
                }
                else
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse BreakingNewsPromotionLocationMoveUp(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsPromotionBo.BreakingNewsPromotionLocationMoveUp(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse BreakingNewsPromotionLocationMoveDown(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsPromotionBo.BreakingNewsPromotionLocationMoveDown(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        //=============================================
        public WcfActionResponse UpdateBreakingNewsPromotionType(BreakingNewsPromotionTypeEntity breakingNewsPromotionTypeEntity)
        {
            try
            {
                if (breakingNewsPromotionTypeEntity.Id > 0)
                {
                    BreakingNewsPromotionBo.BreakingNewsPromotionTypeUpdate(breakingNewsPromotionTypeEntity);
                }
                else
                {
                    BreakingNewsPromotionBo.BreakingNewsPromotionTypeInsert(breakingNewsPromotionTypeEntity);
                }
                return WcfActionResponse.CreateSuccessResponse();
            }
            catch (Exception)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public List<BreakingNewsPromotionTypeEntity> BreakingNewsPromotionTypeGetAll(string keyword, int status)
        {
            return BreakingNewsPromotionBo.BreakingNewsPromotionTypeGetAll(keyword, status);
        }
        public BreakingNewsPromotionTypeEntity BreakingNewsPromotionTypeGetById(int id)
        {
            return BreakingNewsPromotionBo.BreakingNewsPromotionTypeGetById(id);
        }
        public WcfActionResponse BreakingNewsPromotionTypeDelete(int id)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BreakingNewsPromotionBo.BreakingNewsPromotionTypeDelete(id);
                if (errorCode != ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode,
                        ErrorMapping.Current[errorCode]);
                }
                else
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse BreakingNewsPromotionTypeMoveUp(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsPromotionBo.BreakingNewsPromotionTypeMoveUp(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse BreakingNewsPromotionTypeMoveDown(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();

            if (BreakingNewsPromotionBo.BreakingNewsPromotionTypeMoveDown(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = "";
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion
        #region BreakingNewsTag
        public List<BreakingNewsTagWithTagInfoEntity> BreakingNewsTagGetByBreakingNewsId(int breakingNewsId)
        {
            return BreakingNewsTagBo.GetBreakingNewsTagByBreakingNewsId(breakingNewsId);
        }
        #endregion
        public Common.ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
