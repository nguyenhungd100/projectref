using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class QuickNewsServices : IQuickNewsServices
    {

        public Common.ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ChannelVN.ExternalCms.Entity.ErrorCode.ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ChannelVN.ExternalCms.Entity.ErrorCode.ErrorMapping.Current[(ChannelVN.ExternalCms.Entity.ErrorCode.ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public ExternalCms.Entity.QuickNewsEntity ExternalGetQuickNewsByQuickNewsId(long id)
        {
            return QuickNewsBo.ExternalGetQuickNewsByQuickNewsId(id);
        }

        public ExternalCms.Entity.QuickNewsDetailEntity ExternalGetQuickNewsDetailByQuickNewsId(long id)
        {
            return QuickNewsBo.ExternalGetQuickNewsDetailByQuickNewsId(id);
        }

        public QuickNewsEntity GetQuickNewsDetailByQuickNewsId(long id)
        {
            return QuickNewsBo.GetQuickNewsDetailByQuickNewsId(id);
        }

        public WcfActionResponse ExternalReceiveQuickNews(long id, long newsId, string receivedBy)
        {
            var errorCode = QuickNewsBo.ExternalReceiveQuickNews(id, newsId, receivedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse ExternalDeleteQuickNews(long id, string deletedBy)
        {
            var errorCode = QuickNewsBo.ExternalDeleteQuickNews(id, deletedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse ExternalDeleteMultiQuickNews(string idList, string deletedBy)
        {
            var errorCode = QuickNewsBo.ExternalDeleteMultiQuickNews(idList, deletedBy);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<ExternalCms.Entity.QuickNewsEntity> ExternalSearchQuickNews(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuickNewsBo.ExternalSearchQuickNews(keyword, type, status, pageIndex, pageSize, ref totalRow);
        }
        public int ExternalCountQuickNews(int type, int status)
        {
            try
            {
                return QuickNewsBo.ExternalCountQuickNews(type, status);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public ExternalCms.Entity.QuickNewsAuthorEntity ExternalGetQuickNewsAuthorById(long vietId)
        {
            return QuickNewsBo.ExternalGetQuickNewsAuthorById(vietId);
        }

        public ExternalCms.Entity.QuickNewsAuthorEntity ExternalGetQuickNewsAuthorByEmail(string email)
        {
            return QuickNewsBo.ExternalGetQuickNewsAuthorByEmail(email);
        }

        public List<ExternalCms.Entity.QuickNewsAuthorEntity> ExternalSearchQuickNewsAuthor(string keyword, string email, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuickNewsBo.ExternalSearchQuickNewsAuthor(keyword, email, status, pageIndex, pageSize, ref totalRow);
        }

        public List<ExternalCms.Entity.QuickNewsImagesEntity> ExternalGetQuickNewsImagesByQuickNewsId(long quickNewsId)
        {
            return QuickNewsBo.ExternalGetQuickNewsImagesByQuickNewsId(quickNewsId);
        }

        public WcfActionResponse InsertQuickNews(ExternalCms.Entity.QuickNewsEntity quickNews)
        {
            var errorCode = QuickNewsBo.InsertQuickNews(quickNews);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InsertQuickNewsAuthor(ExternalCms.Entity.QuickNewsAuthorEntity quickNewsAuthor)
        {
            var errorCode = QuickNewsBo.InsertQuickNewsAuthor(quickNewsAuthor);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse InsertQuickNewsImage(ExternalCms.Entity.QuickNewsImagesEntity quickNewsImage)
        {
            var errorCode = QuickNewsBo.InsertQuickNewsImage(quickNewsImage);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
    }
}
