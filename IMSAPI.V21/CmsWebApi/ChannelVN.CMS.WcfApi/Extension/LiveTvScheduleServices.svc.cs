﻿
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.LiveTvSchedule.Bo;
using ChannelVN.LiveTvSchedule.Entity;
using ChannelVN.VTV.Bo;
using ChannelVN.LiveTvSchedule.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LiveTvScheduleServices : ILiveTvScheduleServices
    {
        #region CHANNEL
        public List<LiveTvScheduleEntity> GetList(string keyword, int scheduleType, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveTvScheduleBo.Search(keyword, scheduleType,  status, pageIndex, pageSize, ref totalRow);
        }

        public List<LiveTvScheduleEntity> GetLiveTvScheduleByScheduleDate(DateTime scheduleDate, int programChannelId)
        {
            return LiveTvScheduleBo.GetLiveTvScheduleByScheduleDate(scheduleDate, programChannelId);
        }
        public WcfActionResponse Insert(LiveTvScheduleEntity entity, ref int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTvScheduleBo.Insert(entity, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public WcfActionResponse Update(LiveTvScheduleEntity entity)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTvScheduleBo.Update(entity) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        public LiveTvScheduleEntity GetById(int id)
        {
            return LiveTvScheduleBo.GetById(id);
        }
        public WcfActionResponse Delete(int id)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (LiveTvScheduleBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
