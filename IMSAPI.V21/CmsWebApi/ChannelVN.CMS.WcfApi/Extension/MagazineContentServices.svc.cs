﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.MagazineContent.Bo;
using ChannelVN.MagazineContent.Entity;
using ChannelVN.MagazineContent.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MagazineContentServices : IMagazineContentServices
    {
        public List<MagazineContentEntity> MagazineContent_Search(string keyword, MagazineContentIsHot IsHot, MagazineContentStatus status, string UserName, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return MagazineContentBo.MagazineContentSearch(keyword, IsHot, status, UserName, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public MagazineContentEntity MagazineContent_GetById(int Id)
        {
            return MagazineContentBo.GetMagazineContentById(Id);
        }
        public WcfActionResponse MagazineContent_Update(MagazineContentEntity entity)
        {
            return MagazineContentBo.UpdateMagazineContent(entity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_Insert(MagazineContentEntity entity)
        {
            return MagazineContentBo.InsertMagazineContent(entity) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_UpdateStatus(int Id, MagazineContentStatus status, string Username)
        {
            return MagazineContentBo.MagazineContent_UpdateStatus(Id, status, Username) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_UpdateIsHot(int Id, bool IsHot, string Username)
        {
            return MagazineContentBo.MagazineContent_UpdateIsHot(Id, IsHot, Username) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_Published(int Id, DateTime PublishedDate, string Username)
        {
            return MagazineContentBo.MagazineContent_Published(Id, PublishedDate, Username) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public WcfActionResponse MagazineContent_Delete(int Id)
        {
            return MagazineContentBo.DeleteMagazineContent(Id) == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                               ErrorMapping.Current[
                                                                   ErrorMapping.ErrorCodes.BusinessError]);
        }
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
