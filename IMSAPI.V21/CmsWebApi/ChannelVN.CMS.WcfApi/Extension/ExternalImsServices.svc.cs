﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ExternalImsServices : IExternalImsServices
    {
        public List<ExternalNewsEntity> SearchNewsIms(string keyword, string zoneIds, DateTime fromDate, DateTime toDate, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return ExternalNewsBo.SearchNews(keyword, zoneIds, fromDate, toDate, type, pageIndex, pageSize, ref totalRow);
        }
        public List<ExternalNewsEntity> SearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return ExternalNewsBo.SearchNews(keyword, username, zoneIds, fromDate, toDate, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
        }
        public ExternalNewsEntity GetExternalNewsById(long id)
        {
            return ExternalNewsBo.GetExternalNewsById(id);
        }
        public ExternalNewsEntity GetExternalNewsByImsNewsId(long id)
        {
            return ExternalNewsBo.GetExternalNewsByImsNewsId(id);
        }

        public List<ExternalNewsInZoneEntity> GetZoneByNewsId(long id)
        {
            return ExternalNewsBo.GetZoneByNewsId(id);
        }

        public List<ExternalNewsAuthorEntity> GetAuthorByNewsId(long id)
        {
            return ExternalNewsBo.GetAuthorByNewsId(id);
        }

        public WcfActionResponse ReceiveExternalNews(ExternalNewsEntity ExternalNews)
        {
            var errorCode = ExternalNewsBo.ReceiveExternalNews(ExternalNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse PublishExternalNews(ExternalNewsEntity ExternalNews)
        {
            var errorCode = ExternalNewsBo.PublishExternalNews(ExternalNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ReturnExternalNews(ExternalNewsEntity ExternalNews)
        {
            var errorCode = ExternalNewsBo.ReturnExternalNews(ExternalNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UnpublishExternalNews(ExternalNewsEntity ExternalNews)
        {
            var errorCode = ExternalNewsBo.UnpublishExternalNews(ExternalNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateImsNewsInfo(ExternalNewsEntity ExternalNews)
        {
            try
            {
                var errorCode = ExternalNewsBo.UpdateImsNewsInfo(ExternalNews);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public VideoEntity GetDetailVideo(int videoId)
        {
            var zoneName = "";
            return VideoBo.GetById(videoId, ref zoneName);
        }
        public List<VideoRoyaltyAuthorEntity> GetVideoRoyaltyAuthorByVideoId(long id)
        {
            return VideoRoyaltyAuthorBo.GetVideoRoyaltyAuthorByVideoId(id);
        }
        public WcfActionResponse UpdateImsNewsBody(ExternalNewsEntity externalNews)
        {
            try
            {
                var errorCode = ExternalNewsBo.UpdateImsNewsBody(externalNews);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public EmbedAlbumForEditEntity GetEmbedAlbumForEditByEmbedAlbumId(int embedAlbumId, int topPhoto)
        {
            var embedAlbum = EmbedAlbumBo.GetEmbedAlbumForEditByEmbedAlbumId(embedAlbumId, topPhoto);
            return embedAlbum;
        }
    }
}
