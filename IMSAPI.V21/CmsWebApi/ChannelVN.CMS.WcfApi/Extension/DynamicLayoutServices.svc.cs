﻿using System;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.Extension.ServiceContracts;
using ChannelVN.DynamicLayout.BO;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Extension
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DynamicLayoutServices : IDynamicLayoutServices
    {
        #region Pages
        public PageEntity[] GetAllPages()
        {
            return PageBo.GetAll().ToArray();
        }

        public WcfActionResponse AddModuleIntoPlaceHolder(ModuleInPageEntity moduleInPage)
        {
            WcfActionResponse responseData;
            var moduleInPageid = 0;
            var errorCode = ModuleInPageBo.Insert(moduleInPage, ref moduleInPageid);
            if (errorCode == DynamicLayout.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
                responseData.Data = moduleInPageid.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, DynamicLayout.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
            }

            return responseData;
        }

        public ModuleInPageEntity[] GetByPlaceHolderName(string placeHolderName, int pageId)
        {
            return ModuleInPageBo.GetByPlaceHolderName(placeHolderName, pageId).ToArray();
        }


        public WcfActionResponse UpdateModuleInPagePriority(string listOfPriority)
        {
            var errorCode = ModuleInPageBo.UpdatePriority(listOfPriority);
            return errorCode == DynamicLayout.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                               DynamicLayout.Entity.ErrorCode.ErrorMapping.
                                                                   Current[errorCode]);
        }

        public WcfActionResponse UpdateModuleInPageSkinSrc(int moduleInPageId, string skinSrc)
        {
            var errorCode = ModuleInPageBo.UpdateSkinSrc(moduleInPageId, skinSrc);
            return errorCode == DynamicLayout.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                               DynamicLayout.Entity.ErrorCode.ErrorMapping.
                                                                   Current[errorCode]);
        }


        public PageWithFullModuleEntity GetPageDetailByPageId(int pageId)
        {
            return PageBo.GetDetailById(pageId);
        }
        public PageWithFullModuleEntity GetPageDetailByPageSrc(string pageSrc)
        {
            return PageBo.GetDetailByPageSrc(pageSrc);
        }
        #endregion

        #region Modules
        public ModuleEntity[] GetAllModules()
        {
            return ModuleBo.GetAll().ToArray();
        }

        public ModuleConfigurationEntity[] GetByModuleInPageId(int moduleInPageId)
        {
            return ModuleConfigurationBo.GetByModuleInPageID(moduleInPageId).ToArray();
        }

        public WcfActionResponse InsertModuleConfig(ModuleConfigurationEntity moduleConfigurationEntity, ref int moduleConfigId)
        {
            WcfActionResponse responseData;
            var errorCode = ModuleConfigurationBo.Insert(moduleConfigurationEntity, ref moduleConfigId);
            if (errorCode == DynamicLayout.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
                responseData.Data = moduleConfigId.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, DynamicLayout.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateModuleConfig(ModuleConfigurationEntity moduleConfigurationEntity)
        {
            var errorCode = ModuleConfigurationBo.Update(moduleConfigurationEntity);
            return errorCode == DynamicLayout.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                               DynamicLayout.Entity.ErrorCode.ErrorMapping.
                                                                   Current[errorCode]);
        }

        public WcfActionResponse InsertModule(ModuleEntity module)
        {
            WcfActionResponse responseData;
            var moduleId = 0;
            var errorCode = ModuleBo.Insert(module, ref moduleId);
            if (errorCode == DynamicLayout.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
                responseData.Data = moduleId.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, DynamicLayout.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteModuleInPage(int doduleInPageId)
        {
            WcfActionResponse responseData;
            var errorCode = ModuleInPageBo.DeleteById(doduleInPageId);
            if (errorCode == DynamicLayout.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
                responseData.Data = doduleInPageId.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, DynamicLayout.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public ModuleInPageWithConfigEntity[] GetAllModuleWithConfigByPlaceHolder(string placeHolderName, int pageId)
        {
            return ModuleInPageBo.GetAllModuleWithConfigByPlaceHolder(placeHolderName, pageId).ToArray();
        }
        public ModuleInPageWithConfigEntity GetModuleWithConfigByModuleInPageId(int moduleInPageId)
        {
            return ModuleInPageBo.GetModuleWithConfigByModuleInPageId(moduleInPageId);
        }
        public FullModuleInPageWithConfigEntity GetFullModuleWithConfigByModuleInPageId(int moduleInPageId)
        {
            return ModuleInPageBo.GetFullModuleWithConfigByModuleInPageId(moduleInPageId);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
