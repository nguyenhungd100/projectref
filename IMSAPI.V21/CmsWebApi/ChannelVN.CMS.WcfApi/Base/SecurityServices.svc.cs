﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BO.Base.AuthenticateLog;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SecurityServices : ISecurityServices
    {
        #region UserDataBo
        public WcfActionResponse ValidAccount(string username, string password)
        {
            try
            {

                var errorCode = UserDataBo.ValidAccount(username, password);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }
        public WcfActionResponse ValidAccountWithMd5EncryptPassword(string username, string password)
        {
            try
            {
                var errorCode = UserDataBo.ValidAccountWithMd5EncryptPassword(username, password);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }

        public WcfActionResponse ValidSmsCode(string username, string smsCode)
        {
            var errorCode = UserDataBo.ValidSmsCode(username, smsCode);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse ResetPassword(string encryptUserId, string newPassword)
        {
            var errorCode = UserDataBo.ResetPassword(encryptUserId, newPassword);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse ResetMyPassword(string accountName, string newPassword, string cfNewPassword, string oldPassword)
        {
            var errorCode = UserDataBo.ResetPassword(accountName, newPassword, cfNewPassword, oldPassword);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse AddNewUser(UserWithPermissionEntity userWithPermission)
        {
            var newUserId = 0;
            var encryptUserId = "";
            var errorCode = UserDataBo.AddNew(userWithPermission.User, ref newUserId, ref encryptUserId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse UpdateUser(UserWithPermissionEntity userWithPermission, string accountName)
        {
            var errorCode = UserDataBo.Update(userWithPermission.User, accountName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                UserDataBo.GrantListPermission(userWithPermission.User.Id, userWithPermission.UserPermissions);
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse UpdateUserProfile(UserEntity user, string accountName)
        {
            var errorCode = UserDataBo.UpdateProfile(user, accountName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse AddnewUserProfile(UserEntity user, ref int userId)
        {
            var encryptUserId = "";
            var errorCode = UserDataBo.AddNew(user, ref userId, ref encryptUserId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse UpdateAvatar(string accountName, string avatar)
        {
            var errorCode = UserDataBo.UpdateAvatar(accountName, avatar);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }
        public UserEntity GetUserByUserId(string encryptUserId)
        {
            return UserDataBo.GetById(encryptUserId);
        }

        public UserEntity GetUserByUsername(string username)
        {
            return UserDataBo.GetUserByUsername(username);
        }
        public WcfActionResponse UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            var errorCode = UserDataBo.UpdateOtpSecretKeyForUsername(username, otpSecretKey);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }
        public WcfActionResponse IsUserHasPermissionInZone(string username, EnumPermission permissionId, int zoneId)
        {
            var errorCode = UserDataBo.IsUserHasPermissionInZone(username, (int)permissionId, zoneId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse IsUserInGroupPermission(string username, GroupPermission groupPermissionId)
        {
            var errorCode = UserDataBo.IsUserInGroupPermission(username, (int)groupPermissionId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public List<UserPermissionEntity> GetPermisionByUsernameAndPermissionId(string username, EnumPermission permissionId)
        {
            return UserDataBo.GetListByUsernameAndPermissionId(username, (int)permissionId);
        }

        public List<UserPermissionEntity> GetPermisionByUsernameAndZoneId(string username, int zoneId)
        {
            return UserDataBo.GetPermisionByUsernameAndZoneId(username, zoneId);
        }

        public List<PermissionEntity> GetPermisionByUsername(string username)
        {
            return UserDataBo.GetPermissionByUsername(username);
        }


        public WcfActionResponse UpdateUserPermissionByUserId(string encryptUserId, UserPermissionEntity[] userPermission, bool isFullPermission, bool isFullZone, string accountName)
        {
            try {
                var userId = CryptonForId.DecryptIdToInt(encryptUserId);
                var user = UserDataBo.GetById(userId);
                user.IsFullPermission = isFullPermission;
                user.IsFullZone = isFullZone;
                var response = UserDataBo.Update(user, accountName);
                if (response == ErrorMapping.ErrorCodes.Success)
                {
                    if (!isFullPermission || !isFullZone)
                    {
                        var errorCode = UserDataBo.GrantListPermission(userId,
                                                                         new List<UserPermissionEntity>(userPermission));
                        if (errorCode == ErrorMapping.ErrorCodes.Success)
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        else
                        {
                            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                        }
                    }
                    else
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)response, ErrorMapping.Current[response]);
            }
            catch
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,"Error");
            }
        }
        public WcfActionResponse User_ChangeStatus(string userName, UserStatus status)
        {
            var errorCode = UserDataBo.ChangeStatus(userName, status);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        #endregion

        #region PermissionTemplate

        public List<PermissionTemplateEntity> PermissionTemplateGetAll()
        {
            return PermissionBo.PermissionTemplateGetAll();
        }

        //public WcfActionResponse PermissionTemplateUpdate(PermissionTemplateEntity template)
        //{
        //    try
        //    {
        //        var response = PermissionBo.PermissionTemplateUpdate(template);
        //        return (response == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
        //         WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        //        return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        //    }
        //}
        public WcfActionResponse UserPermission_Insert(string encryptUserId, int tempId,bool isFullPermission, bool isFullZone, string accountName)
        {
            try
            {
                var userId = CryptonForId.DecryptIdToInt(encryptUserId);
                var user = UserDataBo.GetById(userId);
                user.IsFullPermission = isFullPermission;
                user.IsFullZone = isFullZone;
                var response = UserDataBo.Update(user, accountName);
                if (response == ErrorMapping.ErrorCodes.Success)
                {
                    if (!isFullPermission || !isFullZone)
                    {
                        var errorCode = UserBo.UserPermission_Insert(userId, tempId);
                        if (errorCode == ErrorMapping.ErrorCodes.Success)
                        {
                            return WcfActionResponse.CreateSuccessResponse();
                        }
                        else
                        {
                            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                        }
                    }
                    else
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                }
                return WcfActionResponse.CreateErrorResponse((int)response, ErrorMapping.Current[response]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            }
        }
        public WcfActionResponse PermissionTemplateDelete(int templateId)
        {
            var response = PermissionBo.PermissionTemplateDelete(templateId);
            return (response == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() :
             WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]));
        }

        public List<PermissionTemplateDetailEntity> GetListPermissionDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            return PermissionBo.GetListPermissionDetailByTemplateId(templateId, isGetChildZone);
        }
        public WcfActionResponse PermissionTemplateDetailUpdate(int templateId, List<PermissionTemplateDetailEntity> permissionTemplates)
        {
            var response = PermissionBo.PermissionTemplateDetailUpdate(templateId, permissionTemplates);
            if (response == ErrorMapping.ErrorCodes.Success) return WcfActionResponse.CreateSuccessResponse();
            else return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]);
        }
        public WcfActionResponse RemoveAllPermissionTemplateDetailByPermissionTemplateId(int templateId)
        {
            var response = PermissionBo.RemoveAllPermissionTemplateDetailByPermissionTemplateId(templateId);
            if (response == ErrorMapping.ErrorCodes.Success) return WcfActionResponse.CreateSuccessResponse();
            else return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]);
        }
        public PermissionTemplateWithPermissionDetailEntity GetPermissionTemplateWithPermissionDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            return PermissionBo.GetPermissionTemplateWithPermissionDetailByTemplateId(templateId, isGetChildZone);
        }

        public PermissionTemplateEntity GetPermissionTemplateById(int templateId)
        {
            return PermissionBo.GetPermissionTemplateById(templateId);
        }
        public WcfActionResponse PermissionTemplateUpdate(int templateId, string templateName)
        {
            var response = PermissionBo.PermissionTemplateUpdate(templateId, templateName);
            if (response == ErrorMapping.ErrorCodes.Success) return WcfActionResponse.CreateSuccessResponse();
            else return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]);
        }
        public WcfActionResponse PermissionTemplateInsert(string templateName)
        {
            var response = PermissionBo.PermissionTemplateInsert(templateName);
            if (response == ErrorMapping.ErrorCodes.Success) return WcfActionResponse.CreateSuccessResponse();
            else return WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[response]);
        }
        #endregion

        #region UserBo

        public UserWithPermissionDetailEntity GetUserWithPermissionDetailByUserId(string encryptUserId, bool isGetChildZone)
        {
            return UserBo.GetUserWithPermissionDetailByUserId(encryptUserId, isGetChildZone);
        }
        public List<UserStandardEntity> SearchUser(string keyword, UserStatus status, UserSortExpression sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                UserStatus userStatus;
                if (!Enum.TryParse(status.ToString(), out userStatus))
                {
                    userStatus = UserStatus.Unknow;
                }
                UserSortExpression sortBy;
                if (!Enum.TryParse(sortOrder.ToString(), out sortBy))
                {
                    sortBy = UserSortExpression.CreateDateDesc;
                }
                return UserBo.Search(keyword, userStatus, sortBy, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<UserStandardEntity>();
            }
        }

        public List<UserStandardEntity> GetListReporterUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(EnumPermission.ArticleReporter, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }
        public List<UserStandardEntity> GetListEditorUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(EnumPermission.ArticleEditor, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }
        public List<UserStandardEntity> GetListSecretaryUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(EnumPermission.ArticleAdmin, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }

        public List<UserStandardEntity> GetUserListByPermissionListAndZoneList(string zoneIds,
                                                                               params EnumPermission[] permissionIds)
        {
            return UserBo.GetUserListByPermissionListAndZoneList(zoneIds, permissionIds);
        }
        public List<UserStandardEntity> GetListUserFullPermisstion()
        {
            return UserBo.GetListUserFullPermisstion();
        }
        public List<UserStandardEntity> GetUserWithFullPermissionAndZoneId(long newsId, string currentUsername)
        {
            return UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, currentUsername);
        }

        public string GetOtpSecretKeyByUsername(string username)
        {
            return UserBo.GetOtpSecretKeyByUsername(username);
        }
        #endregion

        #region UserPenName

        public WcfActionResponse EditPenName(UserPenNameEntity penName)
        {
            var errorCode = UserPenNameBo.EditPenName(penName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse DeletePenNameById(int id)
        {
            var errorCode = UserPenNameBo.DeletePenNameById(id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public List<UserPenNameEntity> SearchPenName(string email, string username, int pageIndex, int pageSize, ref int totalRow)
        {
            return UserPenNameBo.SearchPenName(email, username, pageIndex, pageSize, ref totalRow);
        }
        public UserPenNameEntity GetByVietId(long vietId)
        {
            return UserPenNameBo.GetByVietId(vietId);
        }
        #endregion

        #region Authenticate Log
        public WcfActionResponse AddnewAuthLog(AuthenticateLogEntity auth, ref long logId)
        {
            var errorCode = AuthenticateLogBo.Insert(auth, ref logId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
