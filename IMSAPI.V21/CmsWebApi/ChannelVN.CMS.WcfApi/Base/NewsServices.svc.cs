﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.NewsCacheMonitor;
using ChannelVN.CMS.BO.Base.NewsMobileStream;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.CafeF.BoxExpertNewsEmbed;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsCacheMonitor;
using ChannelVN.CMS.Entity.Base.NewsMobileStream;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeBiz.BoxExpertNewsEmbed;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Cached.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.BO.Base.NewsMaskOnline;
using ChannelVN.CMS.BO.Base.Statistic;
using ChannelVN.CMS.Entity.Base.Statistic;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        #region NewsExtension

        public NewsExtensionEntity GetNewsExtensionValue(long newsId, EnumNewsExtensionType type)
        {
            return NewsBo.GetNewsExtensionValue(newsId, type);
        }
        public int GetNewsExtensionMaxValue(long newsId, EnumNewsExtensionType type)
        {
            return NewsBo.GetNewsExtensionMaxValue(newsId, (int)type);
        }
        public WcfResponseData SetNewsExtensionValue(long newsId, EnumNewsExtensionType type, string value)
        {
            var errorCode = NewsBo.SetNewsExtensionValue(newsId, type, value);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfResponseData.CreateSuccessResponse();
            }
            return WcfResponseData.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfResponseData DeleteNewsExtensionByNewsId(long newsId)
        {
            var errorCode = NewsBo.DeleteNewsExtensionByNewsId(newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfResponseData.CreateSuccessResponse();
            }
            return WcfResponseData.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<NewsExtensionEntity> GetNewsExtensionByNewsId(long newsId)
        {
            return NewsBo.GetNewsExtensionByNewsId(newsId);
        }
        public List<NewsExtensionEntity> GetNewsExtensionByListNewsId(string listNewsId)
        {
            return NewsBo.GetNewsExtensionByListNewsId(listNewsId);
        }
        public List<NewsExtensionEntity> GetNewsExtensionByTypeAndVaue(EnumNewsExtensionType type, string value)
        {
            return NewsBo.GetNewsExtensionByTypeAndVaue(type, value);
        }

        #endregion

        #region For News

        #region Update & Insert

        public WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newEncryptNewsId = String.Empty;
                var newsId = 0L;
                var errorCode = NewsBo.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, ref newsId, ref newEncryptNewsId, new List<string>(authorList), newsChildOrder, sourceId, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteNews(long newsId, string userName)
        {
            try
            {
                var errorCode = NewsBo.DeleteNews(newsId, userName);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = NewsBo.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, isRebuildLink, ref newsStatus, new List<string>(authorList), newsChildOrder, sourceId, publishedContent, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateNewsAvatar(long newsId, string avatar, int position)
        {
            var newsStatus = 0;
            var errorCode = NewsBo.UpdateNewsAvatar(newsId, avatar, position);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateOnlyTitle(long newsId, string newsTitle, string currentUsername)
        {
            var errorCode = NewsBo.UpdateOnlyTitle(newsId, newsTitle, currentUsername);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateDetail(long newsId, string newsTitle, string sapo, string body, string currentUsername)
        {
            var errorCode = NewsBo.UpdateDetail(newsId, newsTitle, sapo, body, currentUsername);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            var errorCode = NewsBo.UpdateDetailSimple(newsId, newsTitle, avatar, avatar2, avatar3, avatar4, avatar5, accountName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region NewsPr

        public NewsPrEntity GetNewsPrByNewsId(long newsId)
        {
            return NewsPrBo.GetNewsPrByNewsId(newsId);
        }

        public NewsPrEntity GetNewsPrByContentId(long contentId)
        {
            return NewsPrBo.GetNewsPrByContentId(contentId);
        }

        public WcfActionResponse RecieveNewsPrIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId, ref long newsId)
        {
            //newsId = 0L;
            var errorCode = NewsPrBo.RecieveNewsPrIntoCms(newsInfo, newsPrInfo, listNewsRelationId, ref newsId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse RecieveFeedbackNewsIntoCms(NewsEntity newsInfo, ref long newsId)
        {
            var errorCode = NewsBo.RecieveFeedbackNewsIntoCms(newsInfo, ref newsId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateNewsPrInfo(NewsPrEntity newsPrInfo)
        {
            var errorCode = NewsPrBo.UpdateNewsPrInfo(newsPrInfo);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse RecieveNewsPrRetailIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId)
        {
            var newsId = 0L;
            var errorCode = NewsPrBo.RecieveNewsPrRetailIntoCms(newsInfo, newsPrInfo, listNewsRelationId, ref newsId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public NewsPrCurrentDateEntity[] GetListNewsPrCurrentDate(DateTime Date, bool UnPublish, int PageIndex, int PageSize, ref int TotalRows)
        {
            return NewsPrBo.GetListNewsPrCurrentDate(Date, UnPublish, PageIndex, PageSize, ref TotalRows).ToArray();
        }
        #endregion

        #region Status flow

        public WcfActionResponse Send(long newsId, string currentUsername, string receiver)
        {
            try
            {
                var errorCode = NewsBo.Send(newsId, currentUsername, receiver);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi gửi tin");
            }
        }
        public WcfActionResponse Receive(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.Receive(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi nhận tin");
            }
        }
        public WcfActionResponse Release(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.Release(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi nhả tin");
            }
        }
        public WcfActionResponse Publish(long newsId, int newsPositionTypeForHome, int newsPositionOrderForHome, int newsPositionTypeForList, int newsPositionOrderForList, string currentUsername, string publishedContent)
        {
            try
            {
                var newsUrl = "";
                var errorCode = NewsBo.Publish(newsId, newsPositionTypeForHome, newsPositionOrderForHome, newsPositionTypeForList, newsPositionOrderForList, currentUsername, ref newsUrl, publishedContent);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsUrl, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi xuất bản tin");
            }
        }
        public WcfActionResponse Unpublish(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.Unpublish(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi gỡ tin");
            }
        }
        public WcfActionResponse Return(long newsId, string currentUsername, string receiver, string note)
        {
            try
            {
                var errorCode = NewsBo.Return(newsId, currentUsername, receiver, note);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi trả lại tin");
            }
        }
        public WcfActionResponse ReturnToCooperator(long newsId, string currentUsername, string note)
        {
            try
            {
                var errorCode = NewsBo.ReturnToCooperator(newsId, currentUsername, note);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi trả lại tin");
            }
        }
        public WcfActionResponse GetBack(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.GetBack(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi rút lại tin");
            }
        }
        public WcfActionResponse ReceiveFromReturnStore(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.ReceiveFromReturnStore(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi nhận lại tin");
            }
        }
        public WcfActionResponse ReceiveFromCooperatorStore(long newsId, string currentUsername, string listZoneId)
        {
            try
            {
                var errorCode = NewsBo.ReceiveFromCooperatorStore(newsId, currentUsername, listZoneId);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi nhận lại tin từ Cộng tác viên");
            }
        }
        public WcfActionResponse MoveToTrash(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.MoveToTrash(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi xóa tin");
            }
        }
        public WcfActionResponse ReceiveFromTrash(long newsId, string currentUsername)
        {
            try
            {
                var errorCode = NewsBo.ReceiveFromTrash(newsId, currentUsername);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi khôi phục tin");
            }
        }
        public WcfActionResponse Forward(long newsId, string currentUsername, string receiver)
        {
            try
            {
                var errorCode = NewsBo.Forward(newsId, currentUsername, receiver);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    "Có lỗi khi chuyển tiếp tin");
            }
        }

        #endregion

        #region Search

        public NewsInListEntity[] SearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNews(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsTemp(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsTemp(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsByNewsId(long NewsId)
        {
            var news = NewsBo.SearchNewsByNewsId(NewsId);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsPublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsPublishedForSecretary(keyword, username, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsTemp_PublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsTemp_PublishedForSecretary(keyword, username, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }

        public NewsInListEntity[] SearchNewsPr(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsPr(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsByUsername(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsTemp_ByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsTemp_ByUsername(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsByType(string keyword, NewsType type, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsByType(keyword, type, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, int priority, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {
            var news = NewsBo.SearchNewsWhichPublished(zoneId, keyword, type, displayPosition, priority, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {
            var news = NewsBo.SearchNewsForNewsPosition(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListEntity[] SearchNewsWhichPublishedForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludePositionTypes, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {
            var news = NewsBo.SearchNewsWhichPublishedForNewsPosition(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, excludePositionTypes, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public NewsInListWithNotifyEntity[] SearchNewsWithNotification(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, int pageIndex, int pageSize, NewsStatus[] statusArray, ref int totalRow)
        {
            var news = NewsBo.SearchNewsWithNotification(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, pageIndex, pageSize, ref totalRow, statusArray);
            return news.ToArray();
        }
        public NewsInListWithNotifyEntity[] SearchNewsWithNotificationV2(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, NewsSortExpression sortOrder, int pageIndex, int pageSize, NewsStatus[] statusArray, ref int totalRow)
        {
            var news = NewsBo.SearchNewsWithNotificationV2(keyword, username, zoneId, fromDate, toDate, sortOrder, pageIndex, pageSize, ref totalRow, statusArray);
            return news.ToArray();
        }

        public NewsPublishForObjectBoxEntity[] SearchNewsForNewsRelation(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public NewsPublishForObjectBoxEntity[] SearchNewsForNewsRelation_ByNewsPublishTemp(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishTemp_ForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public NewsPublishForObjectBoxEntity[] SearchNewsForNewsTagRelation(int zoneId, int tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsForNewsTagRelation(zoneId, tagId, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public NewsPublishForSearchEntity[] SearchNewsPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, ref int totalRow)
        {
            return NewsBo.SearchNewsPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, pageIndex, pageSize, ref totalRow, excludeNewsIds).ToArray();
        }


        public NewsPublishForNewsRelationEntity[] GetNewsRelationByNewsId(long newsId)
        {
            return NewsBo.GetRelatedNewsByNewsId(newsId).ToArray();
        }

        public NewsPublishForNewsRelationEntity[] GetNewsRelationByTypeNewsId(long newsId, int type)
        {
            return NewsBo.GetRelatedNewsByTypeNewsId(newsId, type).ToArray();
        }

        public List<NewsWithAnalyticEntity> GetTopMostNewsInHour(int top, int zoneId)
        {
            return NewsBo.GetTopMostNewsInHour(top, zoneId);
        }

        public NewsInListEntity[] SearchNewsByStatus(string keyword, string userDoAction, string userForFilter,
                                              NewsFilterFieldForUsername userFieldForFilter,
                                              string zoneIds, DateTime fromDate, DateTime toDate, NewsType newsType,
                                              NewsStatus newsStatus, bool isGetTotalRow, int pageIndex, int pageSize,
                                              ref int totalRow)
        {
            NewsStatus status;
            if (!Enum.TryParse(newsStatus.ToString(), out status))
            {
                status = NewsStatus.Unknow;
            }
            NewsFilterFieldForUsername filterUsername;
            if (!Enum.TryParse(userFieldForFilter.ToString(), out filterUsername))
            {
                filterUsername = NewsFilterFieldForUsername.CreatedBy;
            }
            NewsType type;
            if (!Enum.TryParse(newsType.ToString(), out type))
            {
                type = NewsType.All;
            }

            return NewsBo.SearchNewsByStatus(keyword, userDoAction, userForFilter,
                                                                          filterUsername, zoneIds, fromDate, toDate,
                                                                          type, status, isGetTotalRow, pageIndex,
                                                                          pageSize, ref totalRow).ToArray();
        }

        public NewsEntity[] SearchNewsByTag(string tagIds, string tagNames, bool searchBytag, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByTag(tagIds, tagNames, searchBytag, status, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInTag(int zoneId, string keyword, long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInTag(zoneId, keyword, tagId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInMc(int zoneId, string keyword, int mcId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInMc(zoneId, keyword, mcId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInThread(int zoneId, string keyword, long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInThread(zoneId, keyword, threadId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInTopic(int zoneId, string keyword, long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInTopic(zoneId, keyword, topicId, pageIndex, pageSize, ref totalRow);
        }

        public NewsEntity[] SearchNewsByFocusKeyword(string keyword, int top)
        {
            return NewsBo.SearchNewsByFocusKeyword(keyword, top).ToArray();
        }

        public NewsEntity[] SearchNewsByTagId(long tagId)
        {
            return NewsBo.SearchNewsByTagId(tagId).ToArray();
        }
        public List<NewsEntity> SearchNewsByTagIdWithPaging(long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByTagIdWithPaging(tagId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsEntity> SearchNewsByMcIdWithPaging(int mcId, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByMcIdWithPaging(mcId, type, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsEntity> SearchNewsByThreadIdWithPaging(long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByThreadIdWithPaging(threadId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsEntity> SearchNewsByTopicIdWithPaging(long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByTopicIdWithPaging(topicId, pageIndex, pageSize, ref totalRow);
        }
        public List<NewsEntity> SearchHotNewsByTopicId(long topicId)
        {
            return NewsBo.SearchHotNewsByTopicId(topicId);
        }

        public NewsCounterEntity[] CounterNewsByStatus(string accountName, int role, string commonStatus)
        {
            return NewsBo.CounterNewsByStatus(accountName, role, commonStatus).ToArray();
        }

        public NewsCounterEntity[] GetNewsCounterByStatus(string accountName, int zoneId)
        {
            return NewsBo.GetNewsCounterByStatus(accountName, zoneId).ToArray();
        }

        public CategoryCounterEntity GetZoneCounter(int zoneId, DateTime fromDate, DateTime endDate)
        {
            return NewsBo.GetZoneCounter(zoneId, fromDate, endDate);
        }

        public NewsForExportEntity[] StatisticsExportNewsData(string zoneIds, string sourceIds, int orderBy, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.StatisticsExportNewsData(zoneIds, sourceIds, orderBy, fromDate, toDate).ToArray();
        }

        public NewsForExportEntity[] StatisticsExportHotNewsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.StatisticsExportHotNewsData(zoneIds, top, fromDate, toDate).ToArray();
        }

        public NewsForExportEntity[] StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.StatisticsExportNewsHasManyCommentsData(zoneIds, top, fromDate, toDate).ToArray();
        }

        public NewsForExportEntity[] StatisticsExportNewsHasManyCommentsDataV2(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            return NewsStatisticBo.StatisticsExportNewsHasManyCommentsData(zoneIds, top, fromDate, toDate).ToArray();
        }

        public NewsForExportEntity[] GetListNewsByDistributionDate(string zoneIds, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.GetListNewsByDistributionDate(zoneIds, fromDate, toDate).ToArray();
        }

        public NewsUnPublishEntity[] SearchNewsUnPublish(string keyword, int ZoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsUnPublish(keyword, ZoneId, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }

        public List<NewsDashBoardUserEntity> GetDataDashBoardUser(string username, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.GetDataDashBoardUser(username, fromDate, toDate);
        }
        public List<Int64> GetIds(string username, string zoneIds, int top)
        {
            return NewsBo.GetIds(username, zoneIds, top);
        }
        #endregion

        #region News notification

        public WcfActionResponse SetNotification(long newsId, string username, int newsStatus, bool isWarning, string message)
        {
            NewsStatus status;
            if (!Enum.TryParse(newsStatus.ToString(), out status))
            {
                status = NewsStatus.Unknow;
            }

            var errorCode = NewsBo.SetNotification(newsId, username, status, isWarning, message);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ReadNotification(long newsId, string username)
        {
            var errorCode = NewsBo.ReadNotification(newsId, username);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse SetLabelForNews(long newsId, string username, int labelId)
        {
            var errorCode = NewsBo.SetLabelForNews(newsId, username, labelId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public NewsNotificationEntity[] GetNewsNotificationByNewsId(string listNewsId, string username)
        {
            return NewsBo.GetNewsNotificationByNewsId(listNewsId, username).ToArray();
        }
        public NewsNotificationEntity[] GetUnReadNewsNotificationByUsername(string username, DateTime getFromDate, ref int unreadCount)
        {
            return NewsBo.GetUnReadNewsNotificationByUsername(username, getFromDate, ref unreadCount).ToArray();
        }

        #endregion

        #region Get

        public NewsDetailForEditEntity GetDetail(long newsId, string currentUsername)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsForEditByNewsId(newsId, currentUsername);
        }
        public NewsEntity GetNewsFullDetail(long newsId)
        {
            return NewsBo.GetNewsInfoById(newsId);
        }
        public int GetNewsTypeByNewsId(long id)
        {
            return NewsBo.GetNewsTypeByNewsId(id);
        }
        public NewsForValidateEntity GetNewsForValidateById(long newsId)
        {
            return NewsBo.GetNewsForValidateById(newsId);
        }

        public NewsEntity GetNewsDetailByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsByNewsId(newsId);
        }

        public NewsForOtherAPIEntity[] GetNewsForOtherAPIByNewsIds(string newsIds)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsForOtherApi(newsIds).ToArray();
        }

        public List<NewsWithAnalyticEntity> GetNewsByListNewsId(NewsStatus status, string listNewsId)
        {
            return NewsBo.GetNewsByListNewsId(status, listNewsId);
        }
        public List<NewsWithExpertEntity> GetAllNewsByListNewsId(string listNewsId)
        {
            return NewsBo.GetAllNewsByListNewsId(listNewsId);
        }
        public List<NewsForExportEntity> GetAllNewsForExportByListNewsId(string listNewsId)
        {
            return NewsBo.GetAllNewsForExportByListNewsId(listNewsId);
        }
        public List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId)
        {
            return NewsBo.GetNewsDistributionDateByListNewsId(listNewsId);
        }

        public NewsInfoForCachedEntity GetNewsInfoForCachedById(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsInfoForCachedById(newsId);
        }

        #region Get News By OriginalId

        public NewsInListEntity[] SearchNewsByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByOriginalId(zoneIds, fromDate, toDate, originalId, pageIndex, pageSize,
                                                    ref totalRow).ToArray();
        }
        public int GetViewCountByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId)
        {
            return NewsBo.GetViewCountByOriginalId(zoneIds, fromDate, toDate, originalId);
        }

        #endregion

        #endregion

        #region NewsMobileStream
        public List<NewsMobileStreamEntity> GetNewsMobileStreamList(int zoneId)
        {
            return BoFactory.GetInstance<NewsMobileStreamBo>().GetList(zoneId);
        }
        public WcfActionResponse SaveNewsMobileStream(UpdateNewsMobileStreamEntity[] newsMobileStream)
        {
            var errorCode = NewsMobileStreamBo.SaveNewsMobileStream(new List<UpdateNewsMobileStreamEntity>(newsMobileStream));
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        // Tướng Cương Add
        public WcfActionResponse NewsMobileStream_RemoveTopLessView(int topRemove, int numberOfNewsOnTimeLine, int newsPositionTypeIdOnMobileFocus)
        {
            var errorCode = NewsMobileStreamBo.NewsMobileStream_RemoveTopLessView(topRemove, numberOfNewsOnTimeLine, newsPositionTypeIdOnMobileFocus);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

        }
        #endregion
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage(string listOfFocusPositionOnLastestnews)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage(listOfFocusPositionOnLastestnews);
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestnews)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId, listOfFocusPositionOnLastestnews);
        }

        public List<NewsPositionEntity> GetNewsPositionByPositionType(int newsPositionType, int zoneId, string listOfOrder)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionByTypeAndZoneId(newsPositionType, zoneId, listOfOrder);
        }
        public List<NewsPositionEntity> GetScheduleByTypeAndOrder(int typeId, int order, int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetScheduleByTypeAndOrder(typeId, order, zoneId);
        }

        public NewsPositionEntity GetNewsPositionByNewsPositionId(int newsPositionId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetNewsPositionById(newsPositionId);
        }

        public WcfActionResponse SaveNewsPosition(NewsPositionEntity[] newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        {
            var errorCode =
                BoFactory.GetInstance<NewsPositionBo>().SaveNewsPosition(new List<NewsPositionEntity>(newsPositions),
                                                                         avatarIndex, checkNewsExists, usernameForAction,
                                                                         isSwap);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse SaveNewsPositionSchedule(int typeId, int zoneId, int order, string listNewsId, string listScheduleDate, string listRemovePosition, string usernameForAction)
        {
            var errorCode =
                BoFactory.GetInstance<NewsPositionBo>().SaveNewsPositionSchedule(typeId, zoneId, order, listNewsId, listScheduleDate, listRemovePosition, usernameForAction);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UnlockNewsPosition(int positionId)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UnlockNewsPosition(positionId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdatePositionOrder(int typeId, int zoneId, string listPositionId)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UpdatePositionOrder(typeId, zoneId, listPositionId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdatePositionOrderByPositionId(long positionId, int positionOrder)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UpdatePositionOrderByPositionId(positionId, positionOrder);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse LockNewsPosition(int positionId, int lockForZoneId, DateTime expiredLock)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().LockNewsPosition(positionId, lockForZoneId, expiredLock);
            return errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
        }

        public List<NewsPositionForMobileEntity> GetNewsPositionForMobile()
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForMobileByTypeAndZoneId((int)NewsPositionTypeForMobile.DefaultType, 0);
        }

        public WcfActionResponse SaveNewsPositionForMobile(NewsPositionForMobileEntity[] newsPositionsForMobile, int avatarIndex, bool checkNewsExists)
        {
            var errorCode =
                BoFactory.GetInstance<NewsPositionBo>().SaveNewsPositionForMobile(
                    new List<NewsPositionForMobileEntity>(newsPositionsForMobile), avatarIndex, checkNewsExists);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            // Log Activity
            //if (responseData.Success)
            //{
            //    var stopTrying = false;
            //    var time = DateTime.Now;
            //    var applicationId = ApplicationEntity.Id(ApplicationEntity.ApplicationId.CommentCheckError);
            //    while (!stopTrying)
            //    {
            //        try
            //        {
            //            ActivityBo.Insert(new ActivityEntity
            //            {
            //                ActionText = "",
            //                ApplicationId = applicationId,
            //                CreatedDate = DateTime.Now,
            //                SourceId = newsPositionsForMobile.CreatedBy,
            //                SourceName = checkNodeCommentEntity.CreatedBy,
            //                DestinationId = string.Empty,
            //                DestinationName = string.Empty,
            //                GroupTimeLine = Utility.SetPublishedDate(),
            //                Message =
            //                    string.Format(ActionTextCheckNode, checkNodeCommentEntity.CreatedBy, newsUrl, newsTitle, ownerNews, actionContent),
            //                OwnerId = checkNodeCommentEntity.CreatedBy,
            //                Type = 0
            //            });
            //            stopTrying = true;
            //        }
            //        catch (Exception ex)
            //        {
            //            if (DateTime.Now.Subtract(time).Milliseconds > 1000)
            //            {
            //                stopTrying = true;
            //                throw ex;
            //            }
            //        }
            //    }
            //}

            return responseData;
        }

        public WcfActionResponse LockPositionByType(long newsPositionId, int lockTypeId, int order, DateTime expiredLock)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().LockPositionByType(newsPositionId, lockTypeId, order, expiredLock);
            return errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse LockPositionByNewsId(long newsId, int zoneId, int lockTypeId, int order, DateTime expiredLock)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().LockPositionByNewsId(newsId, zoneId, lockTypeId, order, expiredLock);
            return errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UnlockPositionByType(int lockTypeId, int order)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UnlockPositionByType(lockTypeId, order);
            return errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region News history

        public List<NewsHistoryEntity> GetNewsHistoryByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsHistoryBo>().GetHistoryByNewsid(newsId);
        }

        #endregion

        #region NewsAuthor

        public WcfActionResponse NewsAuthorInsert(NewsAuthorEntity newsAuthorEntity, ref int newsAuthorId)
        {
            var errorCode = NewsAuthorBo.Insert(newsAuthorEntity, ref newsAuthorId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsAuthorId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsByAuthorInsert(NewsByAuthorEntity newsByAuthorEntity)
        {
            var errorCode = NewsAuthorBo.InsertToNewsByAuthor(newsByAuthorEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public NewsAuthorEntity NewsAuthorAutoCompleted(NewsAuthorEntity newsAuthorEntity)
        {
            return NewsAuthorBo.AutoCompleted(newsAuthorEntity);
        }

        public WcfActionResponse NewsAuthorUpdate(NewsAuthorEntity newsAuthorEntity)
        {
            var errorCode = NewsAuthorBo.Update(newsAuthorEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(newsAuthorEntity), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse NewsAuthorDelete(int id)
        {
            var errorCode = NewsAuthorBo.Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public NewsAuthorEntity NewsAuthorGetById(int id)
        {
            return NewsAuthorBo.GetById(id);
        }

        public NewsAuthorEntity[] ListNewsAuthorByUser(string username)
        {
            return NewsAuthorBo.GetListByUserName(username).ToArray();
        }

        public NewsAuthorEntity[] ListNewsAuthorSearch(string keyword)
        {
            return NewsAuthorBo.Search(keyword).ToArray();
        }

        public NewsAuthorEntity[] NewsAuthorGetAll()
        {
            return NewsAuthorBo.GetAll().ToArray();
        }

        public NewsByAuthorEntity[] ListNewsAuthorInNews(long newsId)
        {
            return NewsAuthorBo.GetListInNews(newsId).ToArray();
        }
        public NewsAuthorEntity GetByUserData(string userData, AuthorType authorType)
        {
            return NewsAuthorBo.GetByUserData(userData, (int)authorType);
        }
        #endregion

        #region News version
        public WcfActionResponse UpdateNewsVersion(NewsEntity news, string lastModifiedBy)
        {
            var errorCode = NewsBo.UpdateVersion(news, lastModifiedBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ReleaseNewsVersion(long newsId, string lastModifiedBy)
        {
            var errorCode = NewsBo.ReleaseVersion(newsId, lastModifiedBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse CreateVersionForExistsNews(long newsId, string createdBy, ref long newsVersionId)
        {
            var errorCode = NewsBo.CreateVersionForExistsNews(newsId, createdBy, ref newsVersionId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ReleaseFirstNewsVersion(long newsId, string userForUpdateAction)
        {
            var errorCode = NewsBo.ReleaseFirstVersion(newsId, userForUpdateAction);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse RemoveNewsVersion(long newsId, string lastModifiedBy)
        {
            var errorCode = NewsBo.RemoveEditingVersion(newsId, lastModifiedBy);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse AutoSaveNewsVersion(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername)
        {
            var errorCode = NewsBo.AutoSaveNewsVersion(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public NewsVersionWithSimpleFieldsEntity[] GetNewsVersionByNewsId(long newsId)
        {
            return NewsBo.GetVersionByNewsId(newsId).ToArray();
        }
        public NewsVersionWithSimpleFieldsEntity[] GetNewsVersionByNewsIdForUser(long newsId, string lastModifiedBy)
        {
            return NewsBo.GetListVersionByNewsId(newsId, lastModifiedBy).ToArray();
        }
        public NewsVersionEntity GetNewsVersionById(long id)
        {
            return NewsBo.GetById(id);
        }
        public NewsVersionEntity GetLastestNewsVersionByNewsId(long newsId, string lastModifiedBy)
        {
            return NewsBo.GetLastestVersionByNewsId(newsId, lastModifiedBy);
        }
        public NewsVersionEntity GetNewsVersionByNewsIdAndCreatedBy(long newsId, string createdBy)
        {
            return NewsBo.GetNewsVersionByNewsIdAndCreatedBy(newsId, createdBy);
        }
        #endregion

        #endregion

        #region For NewsConfig

        public WcfActionResponse SetNewsConfigValue(string configName, string configValue)
        {
            var errorCode = NewsConfigBo.SetValue(configName, configValue);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public NewsConfigEntity[] GetListConfigByGroupKey(string groupConfigKey)
        {
            return NewsConfigBo.GetListConfigByGroupKey(groupConfigKey).ToArray();
        }
        public NewsConfigEntity GetNewsConfigByConfigName(string configName)
        {
            return NewsConfigBo.GetByConfigName(configName);
        }

        public WcfActionResponse SetValueForStaticHtmlTemplate(string configName, string configLabel, string configValue, EnumStaticHtmlTemplateType configType)
        {
            var errorCode = NewsConfigBo.SetValueForStaticHtmlTemplate(configName, configLabel, configValue, configType);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public NewsConfigEntity[] GetListConfigForStaticHtmlTemplate(string groupConfigKey, EnumStaticHtmlTemplateType type)
        {
            return NewsConfigBo.GetListConfigForStaticHtmlTemplate(groupConfigKey, type).ToArray();
        }

        #endregion

        #region For News OS an News Crawler

        #region News OS

        public NewsOsEntity GetNewsOsDetail(long newsId)
        {
            return NewsOsBo.GetNewsByNewsId(newsId);
        }

        public ListNewsOsEntity[] GetListNewsOs(string keyword, string catId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsOsBo.GetListNews(keyword, catId, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public long ReceiverNewsOs(long newsId, string content, string avatar, string username)
        {
            return NewsOsBo.ReceiverNews(newsId, content, avatar, username);
        }

        public WcfActionResponse DeleteNewsOsById(long newsId)
        {
            var errorCode = NewsOsBo.DeleteById(newsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteNewsOsByListId(string listNewsId)
        {
            var errorCode = NewsOsBo.DeleteByListId(listNewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region News Crawler

        public NewsCrawlerEntity GetNewsCrawlerDetail(int newsId)
        {
            return NewsCrawlerBo.GetNewsByNewsId(newsId);
        }

        public NewsCrawlerEntity[] GetListNewsCrawler(int catId, int siteId, int hot, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsCrawlerBo.GetListNews(catId, siteId, hot, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public long ReceiverNewsCrawler(int newsId, string content, string avatar, string zoneId, string username)
        {
            return NewsCrawlerBo.ReceiverNews(newsId, content, avatar, zoneId, username);
        }

        public WcfActionResponse InsertNewsCrawlerSite(string siteLink, string siteName)
        {
            WcfActionResponse responseData;

            var errorCode = NewsCrawlerBo.InsertSite(siteLink, siteName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public WcfActionResponse InvisibleNewsCrawlerSite(int id)
        {
            WcfActionResponse responseData;

            var errorCode = NewsCrawlerBo.InvisibleSite(id);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteNewsCrawlerById(int newsId)
        {
            WcfActionResponse responseData;

            var errorCode = NewsCrawlerBo.DeleteById(newsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }
        public WcfActionResponse DeleteNewsCrawlerByListId(string listNewsId)
        {
            WcfActionResponse responseData;

            var errorCode = NewsCrawlerBo.DeleteByListId(listNewsId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public CategoryCrawlerEntity[] GetListCrawlerCategory()
        {
            return NewsCrawlerBo.GetListCategoryCrawler().ToArray();
        }
        public SiteCrawlerEntity[] GetListCrawlerSite()
        {
            return NewsCrawlerBo.GetListSiteCrawler().ToArray();
        }

        #endregion

        #endregion

        #region For NewsCachedMonitor

        public List<UpdateCachedEntity> GetUpdateCachedItemForNews(PageSetting pageSetting)
        {
            return BoFactory.GetInstance<NewsCachedMonitorBo>().GetUpdateCachedItemForNews(pageSetting);
        }

        public WcfActionResponse UpdateCachedProcessIdForNews(string listOfProcessId)
        {
            var errorCode = BoFactory.GetInstance<NewsCachedMonitorBo>().UpdateCachedProcessIdForNews(listOfProcessId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateCachedProcessIdForTag(string listOfProcessId)
        {
            var errorCode = BoFactory.GetInstance<NewsCachedMonitorBo>().UpdateCachedProcessIdForTag(listOfProcessId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region For Zone

        public ZoneEntity GetZoneById(int zoneId)
        {
            return ZoneBo.GetZoneById(zoneId);
        }

        public List<ZoneEntity> GetListZoneByParentId(int parentZoneId)
        {
            return ZoneBo.GetListZoneByParentId(parentZoneId);
        }

        public List<ZoneEntity> GetZoneByKeyword(string keyword)
        {
            return ZoneBo.GetZoneByKeyword(keyword);
        }

        public List<ZoneEntity> GetAllZone()
        {
            return ZoneBo.GetAllZone();
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneActiveWithTreeView(bool getParentZoneOnly)
        {
            return ZoneBo.GetAllZoneActiveWithTreeView(getParentZoneOnly);
        }
        public List<ZoneWithSimpleFieldEntity> GetAllZoneWithTreeView(bool getParentZoneOnly)
        {
            return ZoneBo.GetAllZoneWithTreeView(getParentZoneOnly);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneActivedWithTreeView(bool getParentZoneOnly)
        {
            return ZoneBo.GetAllZoneActivedWithTreeView(getParentZoneOnly);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeView(string username)
        {
            return ZoneBo.GetAllZoneByUserWithTreeView(username);
        }

        public List<ZonePhotoWithSimpleFieldEntity> GetAllZonePhotoWithTreeView()
        {
            return ZonePhotoBo.GetAllZonePhotoWithTreeView();
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneByUserAndPermissionWithTreeView(string username, int permissionId)
        {
            return ZoneBo.GetAllZoneByUserWithTreeViewAndPermission(username, permissionId);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneByUserAndListPermissionWithTreeView(string username, params int[] permissionIds)
        {
            return ZoneBo.GetAllZoneByUserWithTreeViewAndPermission(username, permissionIds);
        }

        public List<ZoneWithSimpleFieldEntity> GetAllZoneActivedByUserAndListPermissionWithTreeView(string username, params int[] permissionIds)
        {
            return ZoneBo.GetAllZoneActivedByUserWithTreeViewAndPermission(username, permissionIds);
        }

        public List<ZoneEntity> GetListZoneByUserId(int userId)
        {
            return ZoneBo.GetListZoneByUserId(userId);
        }

        public List<ZoneEntity> GetListZoneByUsername(string username)
        {
            return ZoneBo.GetListZoneByUsername(username);
        }

        public List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            return ZoneBo.GetZoneByNewsId(newsId);
        }

        public List<ZoneDefaultTagEntity> GetAllDefaultTagForZone()
        {
            return ZoneBo.GetAllDefaultTagForZone();
        }

        public WcfActionResponse Insert(ZoneEntity zoneEntity, string username)
        {
            var errorCode = ZoneBo.Insert(zoneEntity, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse Update(ZoneEntity zoneEntity, string username)
        {
            var errorCode = ZoneBo.Update(zoneEntity, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse MoveUp(int zoneId, string username)
        {
            var errorCode = ZoneBo.MoveUp(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse MoveDown(int zoneId, string username)
        {
            var errorCode = ZoneBo.MoveDown(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateInvisibled(int zoneId, string username)
        {
            var errorCode = ZoneBo.UpdateInvisibled(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateAllowComment(int zoneId, string username)
        {
            var errorCode = ZoneBo.UpdateAllowComment(zoneId, username);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public List<ZoneEntity> GetGroupNewsZoneByParentId(int zoneId)
        {
            return ZoneBo.GetGroupNewsZoneByParentId(zoneId);
        }

        #endregion

        #region For NewsUsePhoto
        public WcfActionResponse UpdatePhotoPublishedInNews(long newsId, string photoPublishedIds, string photoPublishedDescriptions)
        {
            var errorCode = BoFactory.GetInstance<NewsBo>().UpdatePhotoPublishedInNews(newsId, photoPublishedIds, photoPublishedDescriptions);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public NewsUsePhotoEntity[] GetAllNewsUsePhotoByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetAllNewsUsePhotoByNewsId(newsId).ToArray();
        }
        public int CountPhotoPublishedByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().CountPhotoPublishedByNewsId(newsId);
        }
        #endregion

        #region For NewsUseVideo

        public WcfActionResponse UpdateVideoInNews(long newsId, string videoIds, string videoDescriptions)
        {
            var errorCode = BoFactory.GetInstance<NewsBo>().UpdateVideoInNews(newsId, videoIds, videoDescriptions);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public NewsUseVideoEntity[] GetAllNewsUseVideoByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().GetAllNewsUseVideoByNewsId(newsId).ToArray();
        }
        public int CountVideoByNewsId(long newsId)
        {
            return BoFactory.GetInstance<NewsBo>().CountVideoByNewsId(newsId);
        }
        #endregion

        #region For NewsSource

        public WcfActionResponse NewsSourceInsert(NewsSourceEntity newsSourceEntity, ref int newsSourceId)
        {
            var errorCode = BoFactory.GetInstance<NewsSourceBo>().Insert(newsSourceEntity, ref newsSourceId);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse NewsSourceUpdate(NewsSourceEntity newsSourceEntity)
        {
            var errorCode = BoFactory.GetInstance<NewsSourceBo>().Update(newsSourceEntity);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse NewsSourceDelete(int id)
        {
            var errorCode = BoFactory.GetInstance<NewsSourceBo>().Delete(id);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public NewsSourceEntity NewsSourceGetById(int id)
        {
            return BoFactory.GetInstance<NewsSourceBo>().GetById(id);
        }

        public NewsSourceEntity[] NewsSourceGetAll(string keyword)
        {
            return BoFactory.GetInstance<NewsSourceBo>().GetAll(keyword).ToArray();
        }

        public NewsBySourceEntity[] ListNewsSourceInNews(long newsId)
        {
            return BoFactory.GetInstance<NewsSourceBo>().GetListInNews(newsId).ToArray();
        }

        #endregion

        #region For EmbedGroup
        public WcfActionResponse EmbedGroupInsert(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            try
            {
                var errorCode = EmbedGroupBo.Insert(embedGroup, listOfZoneId);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse EmbedGroupUpdate(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            try
            {
                var errorCode = EmbedGroupBo.Update(embedGroup, listOfZoneId);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse EmbedGroupUpdateSortOrder(string listOfEmbedGroupId)
        {
            try
            {
                var errorCode = EmbedGroupBo.UpdateSortOrder(listOfEmbedGroupId);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public EmbedGroupEntity[] EmbedGroupGetAll()
        {
            return EmbedGroupBo.GetAll().ToArray();
        }
        public EmbedGroupWithDetailEntity EmbedGroupGetById(int id)
        {
            return EmbedGroupBo.GetById(id);
        }
        public WcfActionResponse EmbedGroupDelete(int id)
        {
            try
            {
                var errorCode = EmbedGroupBo.Delete(id);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        #endregion

        #region For BoxBanner

        public WcfActionResponse InsertBoxBanner(BoxBannerEntity boxBanner, string zoneIdList)
        {
            try
            {
                var errorCode = BoxBannerBo.InsertBoxBanner(boxBanner, zoneIdList);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxBanner(BoxBannerEntity boxBanner, string zoneIdList)
        {
            try
            {
                var errorCode = BoxBannerBo.UpdateBoxBanner(boxBanner, zoneIdList);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateBoxBannerPriority(string listOfBoxBannerId)
        {
            try
            {
                var errorCode = BoxBannerBo.UpdateBoxBannerPriority(listOfBoxBannerId);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteBoxBanner(int boxBannerId)
        {
            try
            {
                var errorCode = BoxBannerBo.DeleteBoxBanner(boxBannerId);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoxBannerEntity GetBoxBannerById(int id)
        {
            return BoxBannerBo.GetBoxBannerById(id);
        }

        public List<BoxBannerEntity> GetListBoxBannerByZoneIdAndPosition(int zoneId, int position, EnumBoxBannerStatus status, EnumBoxBannerType type)
        {
            return BoxBannerBo.GetListBoxBannerByZoneIdAndPosition(zoneId, position, status, type);
        }
        public List<BoxBannerZoneEntity> GetListZoneByBoxBannerId(int boxBannerId)
        {
            return BoxBannerBo.GetListZoneByBoxBannerId(boxBannerId);
        }

        #endregion

        #region NewsLive -  Bài tường thuật

        public WcfActionResponse InsertNewsLive(NewsLiveEntity news, ref NewsLiveDetail returnNews)
        {
            var newsLiveId = 0L;
            var errorCode = NewsLiveBo.Insert(news, ref newsLiveId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(news.Id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            if (responseData.Success && newsLiveId > 0)
            {
                returnNews = NewsLiveBo.GetDetails(newsLiveId);
            }
            return responseData;
        }

        public WcfActionResponse UpdateNewsLive(NewsLiveEntity news)
        {
            var errorCode = NewsLiveBo.Update(news);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(news.Id.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteNewsLive(long id, string accountName)
        {
            var errorCode = NewsLiveBo.Delete(id, accountName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse AddNewsLiveAuthor(NewsLiveAuthorEntity newsLiveAuthorEntity)
        {
            var errorCode = NewsLiveBo.AddAuthor(newsLiveAuthorEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteNewsLiveAuthor(long newsId, int authorId)
        {
            var errorCode = NewsLiveBo.DeleteAuthor(newsId, authorId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse PublishNewsLive(long id, string accountName)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();
            var errorCode = NewsLiveBo.Publish(id, accountName);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse RollbackNewsLive(long id, string accountName)
        {
            var errorCode = NewsLiveBo.Rollback(id, accountName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse TrashNewsLive(long id, string accountName)
        {
            var errorCode = NewsLiveBo.Trash(id, accountName);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateContentNewsLive(long id, string content)
        {
            var errorCode = NewsLiveBo.UpdateContent(id, content);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateNewsLiveSettings(NewsLiveSettingsEntity newsLiveSettings)
        {
            var errorCode = NewsLiveBo.UpdateSettings(newsLiveSettings);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public NewsLiveForList GetAllNewsLive(long newsId, NewsLiveStatus status, int order)
        {
            return NewsLiveBo.GetAll(newsId, status, order);
        }

        public NewsLiveEntity[] GetPagingNewsLive(long newsId, NewsLiveStatus status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsLiveBo.GetPaging(newsId, status, order, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public NewsLiveDetail GetDetailNewsLive(long id)
        {
            return NewsLiveBo.GetDetails(id);
        }

        public NewsLiveAuthorEntity[] GetNewsLiveAuthorList(long newsLiveId)
        {
            return NewsLiveBo.GetAuthorList(newsLiveId).ToArray();
        }

        public NewsLiveSettingsEntity GetNewsLiveSettings(long newsId)
        {
            return NewsLiveBo.GetSettings(newsId);
        }

        #endregion

        #region NewsEmbedBox
        public NewsEmbedBoxListEntity[] GetListNewsEmbedBox(int type, int status)
        {
            return NewsEmbedBoxBo.GetListNewsEmbedBox(type, status).ToArray();
        }

        public WcfActionResponse InsertNewsEmbedBox(NewsEmbebBoxEntity newsEmbedBox)
        {
            var errorCode = NewsEmbedBoxBo.Insert(newsEmbedBox);
            return errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsEmbedBox.News_ID.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateNewsEmbedBox(string listNewsId, int type)
        {
            var errorCode = NewsEmbedBoxBo.Update(listNewsId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateStatusNewsEmbedBox(string listOfBoxNewsId, int type, int status)
        {
            var errorCode = NewsEmbedBoxBo.UpdateStatus(listOfBoxNewsId, type, status);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listOfBoxNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteNewsEmbedBox(long newsId, int type)
        {
            var errorCode = NewsEmbedBoxBo.Delete(newsId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region NewsEmbedBoxOnPage
        public NewsEmbedBoxOnPageListEntity[] GetListNewsEmbedBoxOnPage(int zoneId, int type)
        {
            return NewsEmbedBoxOnPageBo.GetListNewsEmbedBoxOnPage(zoneId, type).ToArray();
        }

        public WcfActionResponse InsertNewsEmbedBoxOnPage(NewsEmbedBoxOnPageEntity newsEmbedBox)
        {
            var errorCode = NewsEmbedBoxOnPageBo.Insert(newsEmbedBox);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsEmbedBox.NewsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateNewsEmbedBoxOnPage(string listNewsId, int zoneId, int type)
        {
            var errorCode = NewsEmbedBoxOnPageBo.Update(listNewsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteNewsEmbedBoxOnPage(long newsId, int zoneId, int type)
        {
            var errorCode = NewsEmbedBoxOnPageBo.Delete(newsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region NewsChild
        public WcfActionResponse UpdateNewsChildVersion(string userDoAction, long parentNewsId, int order, string body, ref NewsChildVersionEntity returnNewsChild)
        {
            returnNewsChild = NewsChildBo.UpdateNewsChildVersion(parentNewsId, order, body, userDoAction);
            return (returnNewsChild == null
                                    ? WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                                          ErrorMapping.Current[
                                                                              ErrorMapping.ErrorCodes.BusinessError])
                                    : WcfActionResponse.CreateSuccessResponse());
        }

        public WcfActionResponse UpdateChildVersion(NewsChildVersionEntity entity)
        {
            var errorCode = NewsChildBo.UpdateNewsChildVersion(entity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                               ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse RemoveNewsChildVersion(string userDoAction, long parentNewsId, int order)
        {
            var errorCode = NewsChildBo.RemoveNewsChild(parentNewsId, order, userDoAction);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                               ErrorMapping.Current[errorCode]);
        }

        public NewsChildVersionEntity GetNewsChildDetailVersion(string userDoAction, long parentNewsId, int order)
        {
            return NewsChildBo.GetByNewsIdAndOrder(parentNewsId, order, userDoAction);
        }

        /// <summary>
        /// lưu dữ liệu newsChild theo parentNewsid
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WcfActionResponse InsertNewsChild(NewsChildEntity entity)
        {
            var errorCode = NewsChildBo.InsertNewsChild(entity);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(entity.NewsParentId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        /// <summary>
        /// Cập nhật thông tin news Child
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateNewsChild(NewsChildEntity entity)
        {
            var errorCode = NewsChildBo.UpdateNewsChild(entity);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(entity.NewsParentId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNewsId"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteNewsChild(long parentNewsId, int order)
        {
            var errorCode = NewsChildBo.DeleteNewsChild(parentNewsId, order);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(parentNewsId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNewsId"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteNewsChildVersion(long versionId, int order)
        {
            var errorCode = NewsChildBo.DeleteNewsChildVersion(versionId, order);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(versionId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WcfActionResponse InsertNewsChildVersion(NewsChildVersionEntity entity)
        {
            var errorCode = NewsChildBo.InsertNewsChildVersion(entity);
            WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                               ? WcfActionResponse.CreateSuccessResponse(entity.NewsParentId.ToString(), "1")
                                               : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="isGetContent"></param>
        /// <returns></returns>
        public List<NewsChildEntity> GetNewsChildByNewsId(long newsId, bool isGetContent = false)
        {
            return NewsChildBo.GetNewsChildByNewsId(newsId, isGetContent);
        }
        public List<NewsChildVersionEntity> GetNewsChildVersionByNewsId(long newsId, bool isGetContent = false)
        {
            return NewsChildBo.GetNewsChildVersionByNewsId(newsId, isGetContent);
        }

        public NewsChildEntity GetByNewsAndOrderId(long parentNewsId, int order)
        {
            return NewsChildBo.GetByNewsAndOrderId(parentNewsId, order);
        }
        #endregion

        #region News Recommendation
        public NewsInListEntity[] RecommendByActiveUsers(int zoneId, int top)
        {
            //return NewsBo.RecommendByActiveUsers(zoneId, top).ToArray();
            return new NewsInListEntity[] { };
        }
        #endregion

        #region For Royalties

        /// <summary>
        /// Get dánh sách bài viết để chấm nhuận bút
        /// </summary>
        /// <param name="zoneId">Id chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu lấy tin</param>
        /// <param name="endDate">Ngày kết thúc lấy tin</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="order">Order</param>
        /// <param name="isGetTotal">Có get total row hay không</param>
        /// <returns></returns>
        public NewsForRoyaltiesEntity[] GetRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, bool isGetTotal, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                var data = NewsBo.GetRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
                totalRow = isGetTotal ? NewsBo.CountRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId) : data.Count;
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new NewsForRoyaltiesEntity[] { };
        }

        public NewsForRoyaltiesEntity[] GetBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, bool isGetTotal, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                var data = NewsBo.GetBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
                totalRow = isGetTotal ? NewsBo.CountBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId) : data.Count;
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new NewsForRoyaltiesEntity[] { };
        }

        public NewsForRoyaltiesEntity[] GetPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, bool isGetTotal, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                var data = NewsBo.GetPRRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
                totalRow = isGetTotal ? NewsBo.CountPRRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId) : data.Count;
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new NewsForRoyaltiesEntity[] { };
        }
        public List<NewsForRoyaltiesEntity> GetRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author, int pageIndex, int pageSize, int order, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            try
            {
                var data = NewsBo.GetRoyaltiesV2(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, ref totalRow,
                                                 order, NewsCategories, newsType, viewCountFrom, viewCountTo);
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return null;
        }
        public List<NewsForRoyaltiesEntity> GetPRRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author, int pageIndex, int pageSize, int order, ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            try
            {
                var data = NewsBo.GetRoyaltiesPRV2(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, ref totalRow,
                                                 order, NewsCategories, newsType, viewCountFrom, viewCountTo);
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return null;
        }


        /// <summary>
        /// Get tong tien nhuận bút
        /// </summary>
        /// <param name="zoneId">Id chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu lấy tin</param>
        /// <param name="endDate">Ngày kết thúc lấy tin</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả</param>
        public decimal GetTotalRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string newsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            return NewsBo.GetTotalPrice(zoneId, startDate, endDate, creator, author, newsCategories, newsType, viewCountFrom, viewCountTo, originalId);
        }

        /// <summary>
        /// Lưu lại giá tiền của bài viết
        /// </summary>
        /// <param name="newsId">Id bài viết</param>
        /// <param name="price">Giá tiền</param>
        /// <returns></returns>
        public WcfActionResponse SetPrice(long newsId, decimal price)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.SetPrice(newsId, price);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        /// <summary>
        /// Update tiền thưởng nhuận bút
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="bonusPrice"></param>
        /// <returns></returns>
        public WcfActionResponse SetBonusPrice(long newsId, decimal bonusPrice)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.SetBonusPrice(newsId, bonusPrice);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="isOnHome"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateIsOnHome(long newsId, bool isOnHome)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateIsOnHome(newsId, isOnHome);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="isFocus"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateIsFocus(long newsId, bool isFocus)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateIsFocus(newsId, isFocus);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayInSlide"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateDisplayInSlide(long id, int displayInSlide)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateDisplayInSlide(id, displayInSlide);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }

        public WcfActionResponse UpdateDisplayPosition(long id, int displayPosition, string accountName)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateDisplayPosition(id, displayPosition, accountName);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        public WcfActionResponse UpdatePriority(long id, int priority, string accountName)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdatePriority(id, priority, accountName);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }

        /// <summary>
        /// Lưu lại ghi chú khi chấm nhuận bút
        /// </summary>
        /// <param name="newsId">Id bài viết</param>
        /// <param name="note">Ghi chú</param>
        /// <returns></returns>
        public WcfActionResponse SetNoteRoyalties(long newsId, string note)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.SetNoteRoyalties(newsId, note);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }

        public WcfActionResponse SetNewsCategory(long newsId, int newsCategory)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.SetNewsCategory(newsId, newsCategory);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }
        #endregion

        #region NewsGroup

        public WcfActionResponse UpdateTopMostRead(int groupNewsId, int withinDays, int rootZoneId, int excludeLastDays)
        {
            var errorCode = NewsGroupBo.UpdateTopMostRead(groupNewsId, withinDays, rootZoneId, excludeLastDays);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostReadDaily(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            var errorCode = NewsGroupBo.UpdateTopMostReadDaily(groupNewsId, rootZoneId, excludeLastDays);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostReadHourly(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            var errorCode = NewsGroupBo.UpdateTopMostReadHourly(groupNewsId, rootZoneId, excludeLastDays);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostComment(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays)
        {
            var errorCode = NewsGroupBo.UpdateTopMostComment(groupNewsId, withinDays, rootZoneId, objectType, excludeLastDays);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public NewsGroupEntity NewsGroupGetById(int id)
        {
            try
            {
                return NewsGroupBo.NewsGroupGetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsGroupEntity();
            }
        }

        public WcfActionResponse UpdateTopMostReadByZone(int groupNewsId, int withinDays, int excludeLastDays, int zoneId, int itemCount)
        {
            var errorCode = NewsGroupBo.UpdateTopMostReadByZone(groupNewsId, withinDays, excludeLastDays, zoneId, itemCount);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostReadV2(int groupNewsId, int withinDays, int excludeLastDays, int itemCount)
        {
            var errorCode = NewsGroupBo.UpdateTopMostReadV2(groupNewsId, withinDays, excludeLastDays, itemCount);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostCommentByZone(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays, int topNews)
        {
            var errorCode = NewsGroupBo.UpdateTopMostCommentByZone(groupNewsId, withinDays, rootZoneId, objectType, excludeLastDays, topNews);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        public WcfActionResponse UpdateTopMostCommentV2(int groupNewsId, int withinDays, int objectType, int excludeLastDays, int topNews)
        {
            var errorCode = NewsGroupBo.UpdateTopMostCommentV2(groupNewsId, withinDays, objectType, excludeLastDays, topNews);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public List<NewsGroupDetailEntity> UpdateTopMostReadV3(int groupNewsId, int zoneId, int withinHours, int totalRow)
        {
            return NewsGroupBo.UpdateTopMostReadV3(groupNewsId, zoneId, withinHours, totalRow);
        }
        public WcfActionResponse UpdateTopMostLikeShare(int groupNewsId, int zoneId, string lstNewsId, string likeShareCountList)
        {
            var errorCode = NewsGroupBo.UpdateTopMostLikeShare(groupNewsId, zoneId, lstNewsId, likeShareCountList);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }
        public WcfActionResponse UpdateTopCommentCountInDay(int groupNewsId, string lstNewsId, string lstCommentCount)
        {
            var errorCode = NewsGroupBo.UpdateTopCommentCountInDay(groupNewsId, lstNewsId, lstCommentCount);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);

            return responseData;
        }

        #endregion

        #region NewsPositionSchedule

        public WcfActionResponse InsertNewsPositionSchedule(NewsPositionScheduleEntity newsPosition)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().InserNewsPositionSchedule(newsPosition);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public WcfActionResponse UpdateNewsPositionSchedule(NewsPositionScheduleEntity newsPosition)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().UpdateNewsPositionSchedule(newsPosition);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public WcfActionResponse DeleteNewsPositionSchedule(int newsPositionScheduleId)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().DeleteNewsPositionSchedule(newsPositionScheduleId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public NewsPositionScheduleEntity[] GetListNewsPositionScheduleGetByPositionTypeId(int positionTypeId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionScheduleGetByPositionTypeId(positionTypeId).ToArray();
        }
        public NewsPositionEntity[] GetListNewsPositionScheduleByPositionTypeIdAndOrder(int typeId, int order, int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionScheduleByPositionTypeIdAndOrder(typeId, order, zoneId).ToArray();
        }
        public WcfActionResponse AutoUpdateNewsPositionSchedule(int maxProcess)
        {
            var errorCode = BoFactory.GetInstance<NewsPositionBo>().AutoUpdateNewsPositionSchedule(maxProcess);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                         ErrorMapping.Current[errorCode]);
            return responseData;
        }

        #endregion

        #region NewsWaitRepublish

        public WcfActionResponse InsertNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newEncryptNewsId = String.Empty;
                var newsId = 0L;
                var errorCode = NewsBo.InsertNewsWaitRepublish(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, ref newsId, ref newEncryptNewsId, new List<string>(authorList), newsChildOrder, sourceId, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newEncryptNewsId, "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse ChangeNewsWaitRepublishStatusToProcessed(long newsId, string userForUpdateAction)
        {
            try
            {
                var errorCode = NewsBo.ChangeNewsWaitRepublishStatusToProcessed(newsId, userForUpdateAction);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = NewsBo.UpdateNewsWaitRepublish(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, isRebuildLink, ref newsStatus, new List<string>(authorList), newsChildOrder, sourceId, publishedContent, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public NewsInListEntity[] SearchNewsWaitRepublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsWaitRepublishStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows)
        {
            var news = NewsBo.SearchNewsWaitRepublish(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }

        public NewsDetailForEditEntity GetNewsByNewsWaitRepublishId(long newsId, string currentUsername)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsByNewsWaitRepublishId(newsId, currentUsername);
        }

        public int NewsWaitRepublishCountDown(string username, string zoneIds, NewsFilterFieldForUsername filterFieldForUsername, NewsType newsType)
        {
            return NewsBo.NewsWaitRepublishCountDown(username, zoneIds, filterFieldForUsername, newsType);
        }
        /// <summary>
        /// Lấy danh sách tin bài theo zoneId và theo ngày xuất bản
        /// </summary>
        /// <param name="zoneIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public List<NewsPublishEntity> GetNewsPublishByDate(int zoneIds, DateTime fromDate, DateTime toDate)
        {
            return NewsPublishBo.GetListDataByDate(zoneIds, fromDate, toDate);
        }

        public List<NewsPublishEntity> GetNewsPublishByHour(int hour, int zoneId)
        {
            return NewsPublishBo.GetLastestNewsByHour(hour, zoneId);
        }

        /// <summary>
        /// Lấy thông tin bài viết theo Id và zoneId
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public NewsPublishEntity GetByNewsIdAndZoneId(long newsId, int zoneId)
        {
            return NewsPublishBo.GetByNewsIdAndZoneId(newsId, zoneId);
        }
        #endregion

        #region NewsAnalytic

        public List<NewsAnalyticEntity> GetLastUpdateViewCountInNewsAnalytic(DateTime lastUpdateViewCount)
        {
            return NewsAnalyticBo.GetLastUpdateViewCount(lastUpdateViewCount);
        }

        public List<NewsAnalyticEntity> GetTopLastestNewsInNewsAnalytic(int top)
        {
            return NewsAnalyticBo.GetTopLastestNews(top);
        }

        public List<NewsAnalyticV3Entity> GetTopHighestViewCountByZone(int zone, int hour, int top)
        {
            return NewsAnalyticBo.GetTopHighestViewCountByZone(zone, hour, top);
        }

        public List<NewsAnalyticV3Entity> GetTopHighestCommentCountByZone(int zone, int hour, int top)
        {
            return NewsAnalyticBo.GetTopHighestCommentCountByZone(zone, hour, top);
        }
        public List<NewsPublishEntity> GetListNewsByListNewsId(string listNewsId)
        {
            return NewsPublishBo.GetListNewsByListNewsId(listNewsId);
        }

        #endregion

        #region NewsAnalyticV2

        public List<NewsAnalyticV2Entity> GetLastUpdateViewCount(long lastUpdateViewCountId)
        {
            return NewsAnalyticV2Bo.GetLastUpdateViewCount(lastUpdateViewCountId);
        }

        public List<NewsAnalyticV2Entity> GetAllViewFromDateToNow(DateTime fromDate, int maxRecord)
        {
            return NewsAnalyticV2Bo.GetAllViewFromDateToNow(fromDate, maxRecord);
        }

        public List<NewsAnalyticV2Entity> GetTopMostReadInHour(int top, int zoneId)
        {
            return NewsAnalyticV2Bo.GetTopMostReadInHour(top, zoneId);
        }

        #endregion

        #region NewsAnalyticAdtech

        public WcfActionResponse ResetAdtechViewCount()
        {
            try
            {
                var errorCode = NewsAnalyticAdtechBo.ResetAdtechViewCount();
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                     ? WcfActionResponse.CreateSuccessResponse()
                                                     : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                             ErrorMapping.Current[
                                                                                                 errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateAdtechViewCount(long newsId, int viewCount)
        {
            try
            {
                var errorCode = NewsAnalyticAdtechBo.UpdateAdtechViewCount(newsId, viewCount);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                     ? WcfActionResponse.CreateSuccessResponse()
                                                     : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                             ErrorMapping.Current[
                                                                                                 errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse BatchUpdateAdtechViewCount(string listNewsId, string listViewCount)
        {
            try
            {
                var errorCode = NewsAnalyticAdtechBo.BatchUpdateAdtechViewCount(listNewsId, listViewCount);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                     ? WcfActionResponse.CreateSuccessResponse()
                                                     : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                             ErrorMapping.Current[
                                                                                                 errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<NewsWithAnalyticEntity> GetTopMostReadInHourByAdtechViewCount(int top)
        {
            return NewsAnalyticAdtechBo.GetTopMostReadInHourByAdtechViewCount(top);
        }
        public List<NewsAnalyticEntity> GetLastUpdateAdtechViewCount(DateTime lastUpdateViewCount)
        {
            return NewsAnalyticAdtechBo.GetLastUpdateAdtechViewCount(lastUpdateViewCount);
        }
        public List<NewsAnalyticEntity> GetTopLastestAdtechViewCount(int top)
        {
            return NewsAnalyticAdtechBo.GetTopLastestAdtechViewCount(top);
        }

        #endregion

        #region NewsLogExternalAction

        public List<NewsLogExternalActionEntity> GetExternalActionLogByNewsIdAndActionType(long newsId,
                                                                          EnumNewsLogExternalAction actionType)
        {
            return NewsLogExternalActionBo.GetActionLogByNewsIdAndActionType(newsId, actionType);
        }

        public WcfActionResponse InsertExternalActionLog(NewsLogExternalActionEntity newsLogExternalAction)
        {
            try
            {
                var errorCode = NewsLogExternalActionBo.InsertActionLog(newsLogExternalAction);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region For NewsMaskOnline
        public List<NewsMaskOnlineEntity> ListNewsMaskOnline(string keyword, DateTime fromDate, DateTime toDate,
                                                  EnumMaskSourceId SourceID, EnumMaskStatus status, int pageIndex, int pageSize, int Mode = -1)
        {
            return NewsMaskOnlineBo.ListNewsMaskOnline(keyword, fromDate, toDate, SourceID, (int)status, pageIndex, pageSize, Mode);
        }
        #endregion

        #region for update like count Fb
        public WcfActionResponse UpdateLikeCount(long newsId, int likeCount)
        {
            var errorCode = NewsBo.UpdateLikeCount(newsId, likeCount);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<NewsEntity> SearchNewsByDateRange(DateTime startDate, DateTime endDate, int status)
        {
            return NewsBo.SearchNewsByDateRange(startDate, endDate, status);
        }
        #endregion

        public NewsPublishEntity GetNewsPublishByNewsId(long newsId)
        {
            //var oldEntity = NewsPublishBo.GetNewsPublishByNewsId(newsId);
            //var miEntity = new NewsPublishMiPresssEntity()
            //                   {
            //                       AdStore = oldEntity.AdStore,
            //                       AdStoreUrl = oldEntity.AdStoreUrl,
            //                       Author = oldEntity.Author,
            //                       Avatar = oldEntity.Avatar,
            //                       Avatar2 = oldEntity.Avatar2,
            //                       Avatar3 = oldEntity.Avatar3,
            //                       Avatar4 = oldEntity.Avatar4,
            //                       Avatar5 = oldEntity.Avatar5,
            //                       AvatarCustom = oldEntity.AvatarCustom,
            //                       AvatarDesc = oldEntity.AvatarDesc,
            //                       DisplayInSlide = oldEntity.DisplayInSlide,
            //                       DisplayPosition = oldEntity.DisplayPosition,
            //                       DisplayStyle = oldEntity.DisplayStyle,
            //                       DistributionDate = (oldEntity.DistributionDate - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalSeconds.ToString(),
            //                       EncryptId = oldEntity.EncryptId,
            //                       ExpiredDate = (oldEntity.ExpiredDate - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalSeconds.ToString(),
            //                       Id = oldEntity.Id,
            //                       InitSapo = oldEntity.InitSapo,
            //                       InterviewId = oldEntity.InterviewId,
            //                       IsBreakingNews = oldEntity.IsBreakingNews,
            //                       IsFocus = oldEntity.IsFocus,
            //                       IsPr = oldEntity.IsPr,
            //                       LocationType = oldEntity.LocationType,
            //                       NewsId = oldEntity.NewsId,
            //                       NewsRelation = oldEntity.NewsRelation,
            //                       OriginalId = oldEntity.OriginalId,
            //                       OriginalUrl = oldEntity.OriginalUrl,
            //                       PublishedDate = oldEntity.PublishedDate,
            //                       RollingNewsId = oldEntity.RollingNewsId,
            //                       Sapo = oldEntity.Sapo,
            //                       Source = oldEntity.Source,
            //                       SubTitle = oldEntity.SubTitle,
            //                       TagItem = oldEntity.TagItem,
            //                       TagSubTitleId = oldEntity.TagSubTitleId,
            //                       ThreadId = oldEntity.ThreadId,
            //                       Title = oldEntity.Title,
            //                       Type = oldEntity.Type,
            //                       Url = oldEntity.Url,
            //                       ZoneId = oldEntity.ZoneId
            //                   };
            //return miEntity;
            return NewsPublishBo.GetNewsPublishByNewsId(newsId);
        }

        public NewsContentEntity GetNewsContentByNewsId(long newsId)
        {
            return NewsContentBo.GetNewsContentByNewsId(newsId);
        }

        public List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            return NewsInZoneBo.GetNewsInZoneByNewsId(newsId);
        }


        public WcfActionResponse InsertNewsInZone(NewsInZoneEntity newsInZone)
        {
            try
            {
                var errorCode = NewsInZoneBo.InsertNewsInZone(newsInZone);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {

                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse() { Success = false };
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public NewsEntity GetNewsByTitle(string title)
        {
            return BoFactory.GetInstance<NewsBo>().GetNewsByTitle(title);
        }

        public int CountNewsBySource(int sourceId, DateTime fromDate, DateTime toDate)
        {
            return NewsBo.CountNewsBySource(sourceId, fromDate, toDate);
        }

        public WcfActionResponse UpdatePhotoVideoCount(long newsId, int photoCount, int videoCount)
        {
            try
            {
                var errorCode = NewsBo.UpdatePhotoVideoCount(newsId, photoCount, videoCount);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {

                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }

        public WcfActionResponse UpdateNewsRelationJson(long id, List<NewsPublishForNewsRelationEntity> newsRelation)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            ErrorMapping.ErrorCodes code = NewsBo.UpdateNewsRelationJson(id, newsRelation);
            if (code == ErrorMapping.ErrorCodes.Success) responseData.Success = true;
            else responseData.Success = false;
            return responseData;
        }

        public List<NewsInListEntity> SearchNewsPublishedByKeyword(string keyword)
        {
            var news = new List<NewsInListEntity>();
            try
            {
                news = NewsBo.SearchNewsPublishedByKeyword(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return news;
        }
        #region Full log
        public List<NewsFullLogEntity> GetNewsFullLog(string keyword)
        {
            var news = new List<NewsFullLogEntity>();
            try
            {
                news = NewsBo.GetNewsFullLog(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return news;
        }
        #endregion

        #region News Top Hot
        public WcfActionResponse NewsHot_Insert(NewsHotEntity entity)
        {
            var errorCode = NewsBo.NewsHot_Insert(entity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_Insert2(NewsHotEntity entity)
        {
            var errorCode = NewsBo.NewsHot_Insert2(entity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_Delete(string NewsIds, int type)
        {
            var errorCode = NewsBo.NewsHot_Delete(NewsIds, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_CheckExists()
        {
            var errorCode = NewsBo.NewsHot_CheckExists();
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_UpdateType()
        {
            var errorCode = NewsBo.NewsHot_UpdateType();
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_Lock(int newsHotId)
        {
            var errorCode = NewsBo.NewsHot_Lock(newsHotId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse NewsHot_Unlock(int newsHotId)
        {
            var errorCode = NewsBo.NewsHot_Unlock(newsHotId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public NewsHotEntity NewsHot_ChangeNewsInLockPosition(int newsHotId, long newsId)
        {
            return NewsBo.NewsHot_ChangeNewsInLockPosition(newsHotId, newsId);
        }
        public long NewsHot_UnpublishOrBomdNews(long newsId)
        {
            return NewsBo.NewsHot_UnpublishOrBomdNews(newsId);
        }
        public List<NewsHotEntity> NewsHot_GetByType(int type)
        {
            return NewsBo.NewsHot_GetByType(type);
        }
        public List<NewsHotEntity> NewsHot_AllForRedis()
        {
            return NewsBo.NewsHot_GetAllForRedis();
        }
        #endregion

        #region Seoer Update News
        public WcfActionResponse Seoer_Update(SeoerNewsEntity news)
        {
            try
            {
                var errorCode = NewsBo.Seoer_Update(news);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }


        #endregion

        #region ViewPlus

        public List<NewsViewPlusListEntity> ViewPlus_GetList(int ZoneId, DateTime DateFrom, DateTime DateTo, int PageIndex, int PageSize, int bidType, ref int TotalRow)
        {
            var news = new List<NewsViewPlusListEntity>();
            try
            {
                news = NewsPrBo.ViewPlus_GetList(ZoneId, DateFrom, DateTo, PageIndex, PageSize, bidType, ref TotalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return news;
        }
        public WcfActionResponse ViewPlus_Insert(ViewPlusEntity viewPlus)
        {
            try
            {
                var errorCode = NewsPrBo.ViewPlus_Insert(viewPlus);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public WcfActionResponse ViewPlus_Delete(string Ids)
        {
            try
            {
                var errorCode = NewsPrBo.ViewPlus_Delete(Ids);
                return errorCode == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }

        #endregion
    }
}
