﻿using System;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Base.Vote;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class VoteServices : IVoteServices
    {
        #region Vote

        public WcfActionResponse Insert(VoteEntity voteEntity, int zoneId, string zoneListId)
        {
            var voteId = 0;
            if (VoteBo.Insert(voteEntity, zoneId, zoneListId, ref voteId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(voteId.ToString(), ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse Update(VoteEntity voteEntity, int zoneId, string zoneListId)
        {
            if (VoteBo.Update(voteEntity, zoneId, zoneListId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse Delete(int id)
        {
            if (VoteBo.Delete(id) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public VoteDetailEntity GetInfo(int id)
        {
            return VoteBo.GetInfo(id);
        }

        public VoteEntity[] GetList(string keyword, int zoneId, int pageIndex, int pageSize, bool isGetTotal, ref int totalRow)
        {
            return VoteBo.GetList(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow).ToArray();
        }

        public WcfActionResponse InsertAnswers(VoteAnswersEntity voteAnswersEntity)
        {
            var voteAnswersId = 0;

            if (VoteBo.InsertAnswers(voteAnswersEntity, ref voteAnswersId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(voteAnswersId.ToString(CultureInfo.InvariantCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse InsertListAnswers(VoteAnswersEntity[] lstVoteAnswers)
        {
            var errorCode = new ErrorMapping.ErrorCodes();
            var voteAnswersId = 0;
            foreach (var voteAnswersEntity in lstVoteAnswers)
            {
                errorCode = VoteBo.InsertAnswers(voteAnswersEntity, ref voteAnswersId);
            }
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateAnswers(VoteAnswersEntity voteAnswersEntity)
        {
            var errorCode = VoteBo.UpdateAnswers(voteAnswersEntity);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdatePriorityAnswers(string listAnswersId)
        {
            var errorCode = VoteBo.UpdatePriorityAnswers(listAnswersId);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateListAnswers(VoteAnswersEntity[] lstVoteAnswers)
        {
            var errorCode = new ErrorMapping.ErrorCodes();
            foreach (var voteAnswersEntity in lstVoteAnswers)
            {
                errorCode = VoteBo.UpdateAnswers(voteAnswersEntity);
            }
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteAnswers(int id)
        {
            var errorCode = VoteBo.DeleteAnswers(id);
            var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                   ? WcfActionResponse.CreateSuccessResponse()
                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                           ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public VoteAnswersEntity[] GetListAnswersByVoteId(int voteId)
        {
            var voteAnswers = VoteBo.GetListAnswersByVoteId(voteId);
            try
            {
                var extVoteAnswers = ChannelVN.ExternalCms.Bo.VoteBo.GetListAnswersByVoteId(voteId);
                for (var i = 0; i < voteAnswers.Count; i++)
                {
                    var ext = extVoteAnswers.Find(it => it.VoteItemId == voteAnswers[i].Id);
                    if (ext != null)
                    {
                        voteAnswers[i].VoteRate = ext.VoteRate;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return voteAnswers.ToArray();
            //return VoteBo.GetListAnswersByVoteId(voteId).ToArray();
        }

        public VoteAnswersEntity GetAnswersDetail(int voteAnswersId)
        {
            return VoteBo.GetAnswersDetail(voteAnswersId);
        }

        #endregion

        #region VoteYesNo

        #region VoteYesNoGroup

        public List<VoteYesNoGroupEntity> GetAllVoteYesNoGroup()
        {
            return VoteYesNoBo.GetAllVoteYesNoGroup();
        }
        public VoteYesNoGroupEntity GetVoteYesNoGroupByVoteYesNoGroupId(int voteYesNoGroupId)
        {
            return VoteYesNoBo.GetVoteYesNoGroupByVoteYesNoGroupId(voteYesNoGroupId);
        }

        public WcfActionResponse UpdateVoteYesNoGroupPriority(string sortedListIds)
        {
            var errorCode = VoteYesNoBo.UpdateVoteYesNoGroupPriority(sortedListIds);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertVoteYesNoGroup(VoteYesNoGroupEntity voteYesNoGroup)
        {
            var voteYesNoId = 0;
            var errorCode = VoteYesNoBo.InsertVoteYesNoGroup(voteYesNoGroup, ref voteYesNoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(voteYesNoId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateVoteYesNoGroup(VoteYesNoGroupEntity voteYesNoGroup)
        {
            var errorCode = VoteYesNoBo.UpdateVoteYesNoGroup(voteYesNoGroup);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteVoteYesNoGroup(int voteYesNoGroupId)
        {
            var errorCode = VoteYesNoBo.DeleteVoteYesNoGroup(voteYesNoGroupId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region VoteYesNo

        public List<VoteYesNoEntity> SearchVoteYesNo(int voteYesNoGroupId, string keyword, int isFocus, EnumVoteYesNoStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VoteYesNoBo.SearchVoteYesNo(voteYesNoGroupId, keyword, isFocus, status, pageIndex, pageSize, ref totalRow);
        }
        public VoteYesNoEntity GetVoteYesNoByVoteYesNoId(int voteYesNoId)
        {
            return VoteYesNoBo.GetVoteYesNoByVoteYesNoId(voteYesNoId);
        }

        public WcfActionResponse UpdateVoteYesNoPriority(string sortedListIds)
        {
            var errorCode = VoteYesNoBo.UpdateVoteYesNoPriority(sortedListIds);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertVoteYesNo(VoteYesNoEntity voteYesNo)
        {
            int voteYesNoId = 0;
            var errorCode = VoteYesNoBo.InsertVoteYesNo(voteYesNo, ref voteYesNoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(voteYesNoId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateVoteYesNo(VoteYesNoEntity voteYesNo)
        {
            var errorCode = VoteYesNoBo.UpdateVoteYesNo(voteYesNo);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteVoteYesNo(int voteYesNoId)
        {
            var errorCode = VoteYesNoBo.DeleteVoteYesNo(voteYesNoId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }


        public WcfActionResponse InsertVoteInNews(int voteId, long newsId)
        {
            if (VoteBo.InsertVoteInNews(voteId, newsId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(voteId.ToString(), ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }
    }
}
