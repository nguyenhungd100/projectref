﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.NewsRoyalties;
using ChannelVN.CMS.BO.Base.Royalties;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsRoyaltiesServices : INewsRoyaltiesServices
    {
        #region RoyaltiesRule

        #region Get

        public List<NewsRoyaltiesRuleEntity> GetListRoleLevel()
        {
            return NewsRoyaltiesRuleBo.GetListRoleLevel();
        }

        public List<NewsRoyaltiesCategoryEntity> GetAllNewsRoyaltiesCategory()
        {
            return NewsRoyaltiesCategoryBo.GetAllNewsRoyaltiesCategory();
        }

        public NewsRoyaltiesRuleEntity GetValueByRoleLevelCategoryId(int roleId, int levelId, int categoryId)
        {
            return NewsRoyaltiesRuleBo.GetValueByRoleLevelCategoryId(roleId, levelId, categoryId);
        }

        public List<NewsRoyaltiesMediaEntity> GetAllNewsRoyaltiesMedia()
        {
            return NewsRoyaltiesMediaBo.GetAllNewsRoyaltiesMedia();
        }

        public NewsRoyaltiesRuleEntity GetValueByRoleLevelMediaId(int roleId, int levelId, int mediaId)
        {
            return NewsRoyaltiesRuleBo.GetValueByRoleLevelMediaId(roleId, levelId, mediaId);
        }

        public List<NewsRoyaltiesRuleEntity> GetAllRule()
        {
            return NewsRoyaltiesRuleBo.GetAll();
        }

        public List<NewsRoyaltiesEntity> GetRoyaltiesByNewsId(long newsId)
        {
            return NewsRoyaltiesBo.GetByNewsId(newsId);
        }

        public NewsRoyaltiesEntity GetRoyaltiesById(long id)
        {
            return NewsRoyaltiesBo.GetById(id);
        }

        #endregion

        public WcfActionResponse UpdateListRoyalties(long newsId, List<NewsRoyaltiesEntity> listRoyalties)
        {
            var errorCode = NewsRoyaltiesBo.UpdateListRoyalties(newsId, listRoyalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteRoyalties(int royaltiesId)
        {
            var errorCode = NewsRoyaltiesBo.DeleteRoyalties(royaltiesId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<NewsRoyaltiesEntity> SearchRoyalties(string userName, int royaltiesCategoryId, int royaltiesRoleId,
                                                  string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate,
                                                  int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsRoyaltiesBo.SearchRoyalties(userName, royaltiesCategoryId, royaltiesRoleId,
                                                    keyword,
                                                    royaltiesRate, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public WcfActionResponse UpdateRoyaltiesValue(NewsRoyaltiesEntity listRoyalties)
        {
            var errorCode = NewsRoyaltiesBo.UpdateRoyaltiesValue(listRoyalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateRoyaltiesMediaNumber(NewsRoyaltiesEntity listRoyalties)
        {
            var errorCode = NewsRoyaltiesBo.UpdateRoyaltiesMediaNumber(listRoyalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateRoyaltiesLevel(NewsRoyaltiesEntity listRoyalties)
        {
            var errorCode = NewsRoyaltiesBo.UpdateRoyaltiesLevelId(listRoyalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateValueByRoleLevelCategoryId(NewsRoyaltiesRuleEntity royalties)
        {
            var errorCode = NewsRoyaltiesRuleBo.UpdateValueByRoleLevelCategoryId(royalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateValueByRoleLevelMediaId(NewsRoyaltiesRuleEntity royalties)
        {
            var errorCode = NewsRoyaltiesRuleBo.UpdateValueByRoleLevelMediaId(royalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<NewsRoyaltiesLevelEntity> GetAllNewsRoyaltiesLevel()
        {
            return NewsRoyaltiesLevelBo.GetAllNewsRoyaltiesLevel();
        }

        public List<NewsRoyaltiesRoleEntity> GetAllNewsRoyaltiesRole()
        {
            return NewsRoyaltiesRoleBo.GetAllNewsRoyaltiesRole();
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
