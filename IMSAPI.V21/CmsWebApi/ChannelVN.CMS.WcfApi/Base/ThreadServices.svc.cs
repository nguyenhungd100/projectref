﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Thread;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ThreadServices : IThreadServices
    {
        #region Get
        public ThreadDetailEntity GetThreadByThreadId(long threadId)
        {
            return BoFactory.GetInstance<ThreadBo>().GetThreadByThreadId(threadId);
        }
        public List<ThreadEntity> GetThreadByNewsId(long newsId)
        {
            return BoFactory.GetInstance<ThreadBo>().GetThreadByNewsId(newsId);
        }
        public ThreadEntity[] SearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoFactory.GetInstance<ThreadBo>().SearchThread(keyword, zoneId, orderBy, isHotThread, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestion(int top, int zoneId, string keyword, int orderBy)
        {
            return BoFactory.GetInstance<ThreadBo>().SearchThreadForSuggestion(top, zoneId, keyword, (int)orderBy);
        }
        #endregion

        #region Update
        public WcfActionResponse Insert(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, ref long newThreadId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().Insert(thread, zoneId, zoneIdList, relationThread, ref newThreadId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(thread));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse Update(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().Update(thread, zoneId, zoneIdList, relationThread);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(thread));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse DeleteById(long threadId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().DeleteById(threadId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateThreadHot(long threadId, bool isHotThread)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadHot(threadId, isHotThread);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateThreadNews(long threadId, string deleteNewsId, string addNewsId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadNews(threadId, deleteNewsId, addNewsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateThreadNewsRelated(long newsId, string relatedThreads)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadNewsRelated(newsId, relatedThreads);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        #endregion

        #region BoxThreadEmbed
        public BoxThreadEmbedEntity[] GetListThreadEmbed(int zoneId, int type)
        {
            return BoFactory.GetInstance<BoxThreadEmbedBo>().GetListThreadEmbed(zoneId, type).ToArray();
        }

        public WcfActionResponse InsertThreadEmbed(BoxThreadEmbedEntity threadEmbedBox)
        {
            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Insert(threadEmbedBox);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(string.Empty, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateThreadEmbed(string listThreadId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Update(listThreadId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(listThreadId, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteThreadEmbed(long threadId, int zoneId, int type)
        {
            WcfActionResponse responseData;
            var newEncryptThreadId = string.Empty;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Delete(threadId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(threadId.ToString(), "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
