﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    public interface ISecurityServices : IErrorMessageMapping
    {
        #region Policy

        [OperationContract]
        WcfActionResponse ValidAccount(string username, string password);
        [OperationContract]
        WcfActionResponse ValidAccountWithMd5EncryptPassword(string username, string password);
        [OperationContract]
        WcfActionResponse ValidSmsCode(string username, string smsCode);
        [OperationContract]
        WcfActionResponse ResetPassword(string encryptUserId, string newPassword);
        [OperationContract]
        WcfActionResponse ResetMyPassword(string accountName, string newPassword, string cfNewPassword, string oldPassword);

        #endregion

        #region Permission

        [OperationContract]
        WcfActionResponse IsUserHasPermissionInZone(string username, EnumPermission permissionId, int zoneId);
        [OperationContract]
        WcfActionResponse IsUserInGroupPermission(string username, GroupPermission groupPermissionId);
        [OperationContract]
        WcfActionResponse UpdateUserPermissionByUserId(string encryptUserId, UserPermissionEntity[] userPermission, bool isFullPermission, bool isFullZone, string accountName);

        [OperationContract]
        List<UserPermissionEntity> GetPermisionByUsernameAndPermissionId(string username, EnumPermission permissionId);
        [OperationContract]
        List<UserPermissionEntity> GetPermisionByUsernameAndZoneId(string username, int zoneId);
        [OperationContract]
        List<PermissionEntity> GetPermisionByUsername(string username);

        #endregion

        #region PermissionTemplate
        [OperationContract]
        List<PermissionTemplateEntity> PermissionTemplateGetAll();

        [OperationContract]
        WcfActionResponse PermissionTemplateDelete(int templateId);

        [OperationContract]
        List<PermissionTemplateDetailEntity> GetListPermissionDetailByTemplateId(int templateId, bool isGetChildZone);

        [OperationContract]
        WcfActionResponse PermissionTemplateDetailUpdate(int templateId,
                                                                List<PermissionTemplateDetailEntity>
                                                                    permissionTemplates);

        [OperationContract]
        WcfActionResponse RemoveAllPermissionTemplateDetailByPermissionTemplateId(int templateId);

        [OperationContract]
        PermissionTemplateWithPermissionDetailEntity GetPermissionTemplateWithPermissionDetailByTemplateId(int templateId, bool isGetChildZone);

        #endregion

        #region User

        [OperationContract]
        WcfActionResponse AddNewUser(UserWithPermissionEntity userWithPermission);
        [OperationContract]
        WcfActionResponse UpdateUser(UserWithPermissionEntity userWithPermission, string accountName);
        [OperationContract]
        WcfActionResponse UpdateUserProfile(UserEntity user, string accountName);
        [OperationContract]
        WcfActionResponse AddnewUserProfile(UserEntity user, ref int userId);
        [OperationContract]
        WcfActionResponse UpdateAvatar(string accountName, string avatar);

        [OperationContract]
        UserEntity GetUserByUserId(string encryptUserId);
        [OperationContract]
        UserEntity GetUserByUsername(string username);
        [OperationContract]
        UserWithPermissionDetailEntity GetUserWithPermissionDetailByUserId(string encryptUserId, bool isGetChildZone);
        [OperationContract]
        List<UserStandardEntity> SearchUser(string keyword, UserStatus status, UserSortExpression sortOrder, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<UserStandardEntity> GetListReporterUser(long newsId, string currentUsername);
        [OperationContract]
        List<UserStandardEntity> GetListEditorUser(long newsId, string currentUsername);
        [OperationContract]
        List<UserStandardEntity> GetListSecretaryUser(long newsId, string currentUsername);

        [OperationContract]
        List<UserStandardEntity> GetUserListByPermissionListAndZoneList(string zoneIds,
                                                                        params EnumPermission[] permissionIds);
        [OperationContract]
        List<UserStandardEntity> GetListUserFullPermisstion();

        [OperationContract]
        List<UserStandardEntity> GetUserWithFullPermissionAndZoneId(long newsId, string currentUsername);

        [OperationContract]
        string GetOtpSecretKeyByUsername(string username);

        [OperationContract]
        WcfActionResponse UpdateOtpSecretKeyForUsername(string username, string otpSecretKey);

        [OperationContract]
        WcfActionResponse UserPermission_Insert(string encryptUserId, int tempId, bool isFullPermission, bool isFullZone, string accountName);

        [OperationContract]
        WcfActionResponse PermissionTemplateInsert(string templateName);

        [OperationContract]
        WcfActionResponse PermissionTemplateUpdate(int templateId, string templateName);

        [OperationContract]
        PermissionTemplateEntity GetPermissionTemplateById(int templateId);

        [OperationContract]
        WcfActionResponse User_ChangeStatus(string userName, UserStatus status);
        #endregion

        #region UserPenName

        [OperationContract]
        WcfActionResponse EditPenName(UserPenNameEntity penName);

        [OperationContract]
        WcfActionResponse DeletePenNameById(int id);

        [OperationContract]
        List<UserPenNameEntity> SearchPenName(string email, string username, int pageIndex, int pageSize,
                                              ref int totalRow);

        [OperationContract]
        UserPenNameEntity GetByVietId(long vietId);

        #endregion

        #region Authenticate Log
        [OperationContract]
        WcfActionResponse AddnewAuthLog(AuthenticateLogEntity auth, ref long logId);
        #endregion
    }
}
