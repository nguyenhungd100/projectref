﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewsRoyaltiesServices" in both code and config file together.
    [ServiceContract]
    public interface INewsRoyaltiesServices
    {
        #region RoyaltiesRule

        #region Get

        [OperationContract]
        List<NewsRoyaltiesRuleEntity> GetListRoleLevel();

        [OperationContract]
        List<NewsRoyaltiesCategoryEntity> GetAllNewsRoyaltiesCategory();

        [OperationContract]
        NewsRoyaltiesRuleEntity GetValueByRoleLevelCategoryId(int roleId, int levelId, int categoryId);

        [OperationContract]
        List<NewsRoyaltiesMediaEntity> GetAllNewsRoyaltiesMedia();

        [OperationContract]
        NewsRoyaltiesRuleEntity GetValueByRoleLevelMediaId(int roleId, int levelId, int mediaId);

        [OperationContract]
        List<NewsRoyaltiesRuleEntity> GetAllRule();

        [OperationContract]
        List<NewsRoyaltiesEntity> GetRoyaltiesByNewsId(long newsId);

        [OperationContract]
        WcfActionResponse UpdateListRoyalties(long newsId, List<NewsRoyaltiesEntity> listRoyalties);

        [OperationContract]
        WcfActionResponse DeleteRoyalties(int royaltiesId);

        [OperationContract]
        List<NewsRoyaltiesEntity> SearchRoyalties(string userName, int royaltiesCategoryId, int royaltiesRoleId,
                                                  string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate,
                                                  int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        WcfActionResponse UpdateRoyaltiesValue(NewsRoyaltiesEntity listRoyalties);

        [OperationContract]
        WcfActionResponse UpdateRoyaltiesMediaNumber(NewsRoyaltiesEntity listRoyalties);

        [OperationContract]
        WcfActionResponse UpdateRoyaltiesLevel(NewsRoyaltiesEntity listRoyalties);

        [OperationContract]
        WcfActionResponse UpdateValueByRoleLevelCategoryId(NewsRoyaltiesRuleEntity royalties);

        [OperationContract]
        WcfActionResponse UpdateValueByRoleLevelMediaId(NewsRoyaltiesRuleEntity royalties);

        [OperationContract]
        List<NewsRoyaltiesRoleEntity> GetAllNewsRoyaltiesRole();

        [OperationContract]
        List<NewsRoyaltiesLevelEntity> GetAllNewsRoyaltiesLevel();

        [OperationContract]
        NewsRoyaltiesEntity GetRoyaltiesById(long id);

        #endregion

        #endregion
    }
}
