﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    public interface IThreadServices : IErrorMessageMapping
    {
        #region Get

        [OperationContract]
        ThreadDetailEntity GetThreadByThreadId(long threadId);

        [OperationContract]
        ThreadEntity[] SearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestion(int top, int zoneId, string keyword, int orderBy);

        [OperationContract]
        List<ThreadEntity> GetThreadByNewsId(long newsId);

        #endregion

        #region Update

        [OperationContract]
        WcfActionResponse Insert(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, ref long newThreadId);

        [OperationContract]
        WcfActionResponse Update(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread);

        [OperationContract]
        WcfActionResponse DeleteById(long threadId);

        [OperationContract]
        WcfActionResponse UpdateThreadHot(long threadId, bool isHotThread);

        [OperationContract]
        WcfActionResponse UpdateThreadNews(long threadId, string deleteNewsId, string addNewsId);

        [OperationContract]
        WcfActionResponse UpdateThreadNewsRelated(long newsId, string relatedThreads);

        #endregion

        #region BoxThreadEmbed

        [OperationContract]
        BoxThreadEmbedEntity[] GetListThreadEmbed(int zoneId, int type);

        [OperationContract]
        WcfActionResponse InsertThreadEmbed(BoxThreadEmbedEntity threadEmbedBox);

        [OperationContract]
        WcfActionResponse UpdateThreadEmbed(string listThreadId, int zoneId, int type);

        [OperationContract]
        WcfActionResponse DeleteThreadEmbed(long threadId, int zoneId, int type);

        #endregion
    }
}
