﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    interface IRoyaltiesServices : IErrorMessageMapping
    {
        #region RoyaltiesCategory

        #region Get

        [OperationContract]
        RoyaltiesCategoryEntity GetRoyaltiesCategoryById(int id);

        [OperationContract]
        List<RoyaltiesCategoryEntity> GetAllRoyaltiesCategory();

        [OperationContract]
        List<RoyaltiesCategoryEntity> GetByRoyaltiesCategoryType(int zoneId, EnumRoyaltiesCategoryType type);

        #endregion

        #region Set

        [OperationContract]
        WcfActionResponse InsertRoyaltiesCategory(RoyaltiesCategoryEntity royaltiesCategory);

        [OperationContract]
        WcfActionResponse UpdateRoyaltiesCategory(RoyaltiesCategoryEntity royaltiesCategory);

        [OperationContract]
        WcfActionResponse DeleteRoyaltiesCategory(int royaltiesCategoryId);

        [OperationContract]
        WcfActionResponse UpdateRoyaltiesCategoryForClipAndPhoto(int zoneId);

        #endregion

        #endregion

        #region RoyaltiesRole

        #region Get

        [OperationContract]
        RoyaltiesRoleEntity GetRoyaltiesRoleById(int id);

        [OperationContract]
        List<RoyaltiesRoleEntity> GetAllRoyaltiesRole();

        #endregion

        #region Set

        [OperationContract]
        WcfActionResponse InsertRoyaltiesRole(RoyaltiesRoleEntity royaltiesRole);

        [OperationContract]
        WcfActionResponse UpdateRoyaltiesRole(RoyaltiesRoleEntity royaltiesRole);

        [OperationContract]
        WcfActionResponse DeleteRoyaltiesRole(int royaltiesRoleId);

        #endregion

        #endregion

        #region RoyaltiesMember

        #region Get

        [OperationContract]
        RoyaltiesMemberEntity GetRoyaltiesMemberById(int id);

        [OperationContract]
        List<RoyaltiesMemberEntity> SearchRoyaltiesMember(int royaltiesRoleId, string keyword, EnumRoyaltiesMemberStatus status, int pageIndex, int pageSize, ref int totalRow);

        #endregion

        #region Set

        [OperationContract]
        WcfActionResponse InsertRoyaltiesMember(RoyaltiesMemberEntity royaltiesMember);

        [OperationContract]
        WcfActionResponse UpdateRoyaltiesMember(RoyaltiesMemberEntity royaltiesMember);

        [OperationContract]
        WcfActionResponse DeleteRoyaltiesMember(int royaltiesMemberId);

        #endregion

        #endregion

        #region RoyaltiesRule

        #region Get

        [OperationContract]
        List<RoyaltiesRuleEntity> GetRoyaltiesRuleByZoneId(int zoneId);

        [OperationContract]
        RoyaltiesRuleEntity GetByRoyaltiesCategoryIdAndRoyaltiesRate(bool isManager,
                                                                     int royaltiesCategoryId,
                                                                     int royaltiesRate);

        #endregion

        #region Set

        [OperationContract]
        WcfActionResponse UpdateRoyaltiesRuleValues(List<RoyaltiesRuleEntity> listRoyaltiesRule);

        #endregion

        #endregion

        #region Royalties

        #region Get

        [OperationContract]
        RoyaltiesEntity GetRoyaltiesById(long id);

        [OperationContract]
        List<RoyaltiesEntity> SearchRoyalties(EnumRoyaltiesCategoryType type, int zoneId, int royaltiesMemberId, int royaltiesCategoryId,
                                              int royaltiesRoleId, string keyword, int royaltiesRate,
                                              DateTime fromDate, DateTime toDate, int pageIndex, int pageSize,
                                              ref int totalRow);

        [OperationContract]
        List<RoyaltiesGroupByUserEntity> SearchRoyaltiesGroupByUser(EnumRoyaltiesCategoryType type,
                                                                    int zoneId, int royaltiesMemberId,
                                                                    int royaltiesCategoryId,
                                                                    int royaltiesRoleId, string keyword,
                                                                    int royaltiesRate, DateTime fromDate,
                                                                    DateTime toDate);

        [OperationContract]
        List<RoyaltiesHistoryEntity> GetRoyaltiesHistoryByRoyaltiesId(long royaltiesId);

        [OperationContract]
        List<RoyaltiesEntity> GetRoyaltiesByNewsId(long newsId);

        #endregion

        #region Set

        [OperationContract]
        WcfActionResponse InsertRoyalties(RoyaltiesEntity royalties);

        [OperationContract]
        WcfActionResponse UpdateRoyalties(RoyaltiesEntity royalties);

        [OperationContract]
        WcfActionResponse DeleteRoyalties(int royaltiesId);

        [OperationContract]
        WcfActionResponse UpdateListRoyalties(long newsId, List<RoyaltiesEntity> listRoyalties);

        #endregion

        #endregion

        #region Quota
        [OperationContract]
        WcfActionResponse UpdateRoyaltiesQuota(string lstId, string lstPost, string lstView);

        [OperationContract]
        List<RoyaltiesQuotaEntity> SearchUser(string keyword, UserStatus status, UserSortExpression sortOrder,
                                              int pageIndex, int pageSize, ref int totalRow);

        #endregion
    }
}
