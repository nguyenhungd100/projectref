﻿using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.WcfApi.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    public interface IVoteServices : IErrorMessageMapping
    {
        #region Vote

        [OperationContract]
        WcfActionResponse Insert(VoteEntity voteEntity, int zoneId, string zoneListId);
        [OperationContract]
        WcfActionResponse Update(VoteEntity voteEntity, int zoneId, string zoneListId);
        [OperationContract]
        WcfActionResponse Delete(int id);
        [OperationContract]
        VoteDetailEntity GetInfo(int id);
        [OperationContract]
        VoteEntity[] GetList(string keyword, int zoneId, int pageIndex, int pageSize, bool isGetTotal, ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertAnswers(VoteAnswersEntity voteAnswersEntity);
        [OperationContract]
        WcfActionResponse InsertListAnswers(VoteAnswersEntity[] lstVoteAnswers);
        [OperationContract]
        WcfActionResponse UpdateAnswers(VoteAnswersEntity voteAnswersEntity);
        [OperationContract]
        WcfActionResponse UpdatePriorityAnswers(string listAnswersId);
        [OperationContract]
        WcfActionResponse UpdateListAnswers(VoteAnswersEntity[] lstVoteAnswers);
        [OperationContract]
        WcfActionResponse DeleteAnswers(int id);
        [OperationContract]
        VoteAnswersEntity[] GetListAnswersByVoteId(int voteId);
        [OperationContract]
        VoteAnswersEntity GetAnswersDetail(int voteAnswersId);
        [OperationContract]
        WcfActionResponse InsertVoteInNews(int voteId, long newsId);
        #endregion

        #region VoteYesNo

        #region VoteYesNoGroup

        [OperationContract]
        List<VoteYesNoGroupEntity> GetAllVoteYesNoGroup();
        [OperationContract]
        VoteYesNoGroupEntity GetVoteYesNoGroupByVoteYesNoGroupId(int voteYesNoGroupId);
        [OperationContract]
        WcfActionResponse UpdateVoteYesNoGroupPriority(string sortedListIds);
        [OperationContract]
        WcfActionResponse InsertVoteYesNoGroup(VoteYesNoGroupEntity voteYesNoGroup);
        [OperationContract]
        WcfActionResponse UpdateVoteYesNoGroup(VoteYesNoGroupEntity voteYesNoGroup);
        [OperationContract]
        WcfActionResponse DeleteVoteYesNoGroup(int voteYesNoGroupId);

        #endregion

        #region VoteYesNo

        [OperationContract]
        List<VoteYesNoEntity> SearchVoteYesNo(int voteYesNoGroupId, string keyword, int isFocus, EnumVoteYesNoStatus status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        VoteYesNoEntity GetVoteYesNoByVoteYesNoId(int voteYesNoId);
        [OperationContract]
        WcfActionResponse UpdateVoteYesNoPriority(string sortedListIds);
        [OperationContract]
        WcfActionResponse InsertVoteYesNo(VoteYesNoEntity voteYesNo);
        [OperationContract]
        WcfActionResponse UpdateVoteYesNo(VoteYesNoEntity voteYesNo);
        [OperationContract]
        WcfActionResponse DeleteVoteYesNo(int voteYesNoId);

        #endregion

        #endregion
    }
}
