﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsCacheMonitor;
using ChannelVN.CMS.Entity.Base.NewsMobileStream;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.Cached.Entity;
using ChannelVN.CMS.Entity.Base.Statistic;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    interface INewsServices : IErrorMessageMapping
    {
        #region NewsExtension

        [OperationContract]
        NewsExtensionEntity GetNewsExtensionValue(long newsId, EnumNewsExtensionType type);

        [OperationContract]
        int GetNewsExtensionMaxValue(long newsId, EnumNewsExtensionType type);

        [OperationContract]
        WcfResponseData SetNewsExtensionValue(long newsId, EnumNewsExtensionType type, string value);

        [OperationContract]
        WcfResponseData DeleteNewsExtensionByNewsId(long newsId);

        [OperationContract]
        List<NewsExtensionEntity> GetNewsExtensionByNewsId(long newsId);

        [OperationContract]
        List<NewsExtensionEntity> GetNewsExtensionByListNewsId(string listNewsId);

        [OperationContract]
        List<NewsExtensionEntity> GetNewsExtensionByTypeAndVaue(EnumNewsExtensionType type, string value);

        #endregion

        #region For News

        #region Update & Insert
        [OperationContract]
        WcfActionResponse DeleteNews(long newsId, string userName);
        [OperationContract]
        WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions);
        [OperationContract]
        WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions);
        [OperationContract]
        WcfActionResponse UpdateNewsAvatar(long newsId, string avatar, int position);
        [OperationContract]
        WcfActionResponse UpdateOnlyTitle(long newsId, string newsTitle, string currentUsername);
        [OperationContract]
        WcfActionResponse UpdateDetail(long newsId, string newsTitle, string sapo, string body, string currentUsername);
        [OperationContract]
        WcfActionResponse UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2,
            string avatar3, string avatar4, string avatar5, string accountName);

        #endregion

        #region NewsPr

        [OperationContract]
        NewsPrEntity GetNewsPrByNewsId(long newsId);

        [OperationContract]
        NewsPrEntity GetNewsPrByContentId(long contentId);

        [OperationContract]
        WcfActionResponse RecieveNewsPrIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId, ref long newsId);

        [OperationContract]
        WcfActionResponse UpdateNewsPrInfo(NewsPrEntity newsPrInfo);

        [OperationContract]
        WcfActionResponse RecieveNewsPrRetailIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId);

        [OperationContract]
        NewsPrCurrentDateEntity[] GetListNewsPrCurrentDate(DateTime Date, bool UnPublish, int PageIndex, int PageSize, ref int TotalRows);

        #endregion

        #region Status flow

        [OperationContract]
        WcfActionResponse Send(long newsId, string currentUsername, string receiver);
        [OperationContract]
        WcfActionResponse Receive(long newsId, string currentUsername);
        [OperationContract]
        WcfActionResponse Release(long newsId, string currentUsername);
        [OperationContract]
        WcfActionResponse Publish(long newsId, int newsPositionTypeForHome, int newsPositionOrderForHome, int newsPositionTypeForList, int newsPositionOrderForList, string currentUsername, string publishedContent);
        [OperationContract]
        WcfActionResponse Unpublish(long newsId, string currentUsername);
        [OperationContract]
        WcfActionResponse Return(long newsId, string currentUsername, string receiver, string note);
        [OperationContract]
        WcfActionResponse ReturnToCooperator(long newsId, string currentUsername, string note);
        [OperationContract]
        WcfActionResponse GetBack(long newsId, string currentUsername);
        [OperationContract]
        WcfActionResponse ReceiveFromReturnStore(long newsId, string currentUsername);
        [OperationContract]
        WcfActionResponse MoveToTrash(long newsId, string currentUsername);
        [OperationContract]
        WcfActionResponse ReceiveFromTrash(long newsId, string currentUsername);
        [OperationContract]
        WcfActionResponse Forward(long newsId, string currentUsername, string receiver);
        [OperationContract]
        WcfActionResponse ReceiveFromCooperatorStore(long newsId, string currentUsername, string listZoneId);

        #endregion

        #region Search

        [OperationContract]
        NewsInListEntity[] SearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows);
        [OperationContract]
        NewsInListEntity[] SearchNewsTemp(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        NewsInListEntity[] SearchNewsByNewsId(long NewsId);

        [OperationContract]
        NewsInListEntity[] SearchNewsPublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows);
        [OperationContract]
        NewsInListEntity[] SearchNewsTemp_PublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows);
        [OperationContract]
        NewsInListEntity[] SearchNewsPr(string keyword, string username, string zoneIds, DateTime fromDate,
                                        DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername,
                                        NewsSortExpression sortOrder, NewsStatus newsStatus, NewsType newsType,
                                        int pageIndex, int pageSize, ref int totalRows);
        [OperationContract]
        NewsInListEntity[] SearchNewsByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        NewsInListEntity[] SearchNewsTemp_ByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        NewsInListEntity[] SearchNewsByType(string keyword, NewsType type, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        NewsInListEntity[] SearchNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, int priority, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows);

        [OperationContract]
        NewsInListEntity[] SearchNewsForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows);

        [OperationContract]
        NewsInListEntity[] SearchNewsWhichPublishedForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludePositionTypes, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows);

        [OperationContract]
        NewsInListWithNotifyEntity[] SearchNewsWithNotification(string keyword, string username,
                                                                string zoneIds, DateTime fromDate, DateTime toDate,
                                                                NewsFilterFieldForUsername filterFieldForUsername,
                                                                NewsSortExpression sortOrder, int pageIndex,
                                                                int pageSize, NewsStatus[] statusArray, ref int totalRow);

        [OperationContract]
        NewsInListWithNotifyEntity[] SearchNewsWithNotificationV2(string keyword, string username, int zoneId,
                                                                  DateTime fromDate, DateTime toDate,
                                                                  NewsSortExpression sortOrder, int pageIndex,
                                                                  int pageSize, NewsStatus[] statusArray,
                                                                  ref int totalRow);

        [OperationContract]
        NewsPublishForObjectBoxEntity[] SearchNewsForNewsRelation(int zoneId, string keyword, int pageIndex,
                                                                  int pageSize, ref int totalRow);
        [OperationContract]
        NewsPublishForObjectBoxEntity[] SearchNewsForNewsRelation_ByNewsPublishTemp(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        NewsPublishForSearchEntity[] SearchNewsPublished(int zoneId, string keyword, int type, int displayPosition,
                                                         DateTime distributedDateFrom, DateTime distributedDateTo,
                                                         int pageIndex, int pageSize, string excludeNewsIds,
                                                         ref int totalRow);

        [OperationContract]
        NewsPublishForNewsRelationEntity[] GetNewsRelationByNewsId(long newsId);

        [OperationContract]
        NewsPublishForNewsRelationEntity[] GetNewsRelationByTypeNewsId(long newsId, int type);

        [OperationContract]
        List<NewsWithAnalyticEntity> GetTopMostNewsInHour(int top, int zoneId);

        [OperationContract]
        NewsInListEntity[] SearchNewsByStatus(string keyword, string userDoAction, string userForFilter,
                                              NewsFilterFieldForUsername userFieldForFilter,
                                              string zoneIds, DateTime fromDate, DateTime toDate, NewsType newsType,
                                              NewsStatus newsStatus, bool isGetTotalRow, int pageIndex, int pageSize,
                                              ref int totalRow);

        [OperationContract]
        NewsEntity[] SearchNewsByTag(string tagIds, string tagNames, bool searchBytag, int status, int pageIndex,
                                     int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsInListEntity> SearchNewsPublishExcludeNewsInTag(int zoneId, string keyword, long tagId, int pageIndex,
                                                                 int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsInListEntity> SearchNewsPublishExcludeNewsInMc(int zoneId, string keyword, int mcId, int pageIndex,
                                                                int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsInListEntity> SearchNewsPublishExcludeNewsInThread(int zoneId, string keyword, long threadId,
                                                                    int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsInListEntity> SearchNewsPublishExcludeNewsInTopic(int zoneId, string keyword, long topicId,
                                                                   int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        NewsEntity[] SearchNewsByFocusKeyword(string keyword, int top);
        [OperationContract]
        NewsEntity[] SearchNewsByTagId(long tagId);

        [OperationContract]
        List<NewsEntity> SearchNewsByTagIdWithPaging(long tagId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsEntity> SearchNewsByMcIdWithPaging(int mcId, int type, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsEntity> SearchNewsByThreadIdWithPaging(long threadId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsEntity> SearchNewsByTopicIdWithPaging(long topicId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsEntity> SearchHotNewsByTopicId(long topicId);
        [OperationContract]
        NewsCounterEntity[] CounterNewsByStatus(string accountName, int role, string commonStatus);
        [OperationContract]
        NewsCounterEntity[] GetNewsCounterByStatus(string accountName, int zoneId);

        [OperationContract]
        NewsForExportEntity[] StatisticsExportNewsData(string zoneIds, string sourceIds, int orderBy, DateTime fromDate, DateTime toDate);
        [OperationContract]
        NewsForExportEntity[] StatisticsExportHotNewsData(string zoneIds, int top, DateTime fromDate, DateTime toDate);

        [OperationContract]
        NewsForExportEntity[] StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate);

        [OperationContract]
        NewsForExportEntity[] StatisticsExportNewsHasManyCommentsDataV2(string zoneIds, int top, DateTime fromDate,
                                                                        DateTime toDate);

        [OperationContract]
        NewsForExportEntity[] GetListNewsByDistributionDate(string zoneIds, DateTime fromDate, DateTime toDate);

        [OperationContract]
        NewsUnPublishEntity[] SearchNewsUnPublish(string keyword, int ZoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        List<NewsDashBoardUserEntity> GetDataDashBoardUser(string username, DateTime fromDate, DateTime toDate);

        [OperationContract]
        List<Int64> GetIds(string username, string zoneIds, int top);
        #endregion

        #region News notification

        [OperationContract]
        WcfActionResponse SetNotification(long newsId, string username, int newsStatus, bool isWarning, string message);
        [OperationContract]
        WcfActionResponse ReadNotification(long newsId, string username);
        [OperationContract]
        WcfActionResponse SetLabelForNews(long newsId, string username, int labelId);
        [OperationContract]
        NewsNotificationEntity[] GetNewsNotificationByNewsId(string listNewsId, string username);
        [OperationContract]
        NewsNotificationEntity[] GetUnReadNewsNotificationByUsername(string username, DateTime getFromDate, ref int unreadCount);

        #endregion

        #region Get

        [OperationContract]
        NewsDetailForEditEntity GetDetail(long newsId, string currentUsername);

        [OperationContract]
        NewsEntity GetNewsDetailByNewsId(long newsId);

        [OperationContract]
        int GetNewsTypeByNewsId(long id);
        [OperationContract]
        NewsForOtherAPIEntity[] GetNewsForOtherAPIByNewsIds(string newsIds);

        [OperationContract]
        List<NewsWithAnalyticEntity> GetNewsByListNewsId(NewsStatus status, string listNewsId);

        [OperationContract]
        List<NewsWithExpertEntity> GetAllNewsByListNewsId(string listNewsId);

        [OperationContract]
        List<NewsForExportEntity> GetAllNewsForExportByListNewsId(string listNewsId);

        [OperationContract]
        List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId);

        [OperationContract]
        NewsForValidateEntity GetNewsForValidateById(long newsId);

        [OperationContract]
        NewsInfoForCachedEntity GetNewsInfoForCachedById(long newsId);

        [OperationContract]
        NewsEntity GetNewsFullDetail(long newsId);

        #region Get News By OriginalId

        [OperationContract]
        NewsInListEntity[] SearchNewsByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId,
                                                  int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        int GetViewCountByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId);

        #endregion

        #endregion

        #region NewsMobileStream

        [OperationContract]
        List<NewsMobileStreamEntity> GetNewsMobileStreamList(int zoneId);

        [OperationContract]
        WcfActionResponse SaveNewsMobileStream(UpdateNewsMobileStreamEntity[] newsMobileStream);

        [OperationContract]
        WcfActionResponse NewsMobileStream_RemoveTopLessView(int topRemove, int numberOfNewsOnTimeLine,
            int newsPositionTypeIdOnMobileFocus);
        #endregion

        #region News position

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage(string listOfFocusPositionOnLastestnews);

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestnews);

        [OperationContract]
        List<NewsPositionEntity> GetNewsPositionByPositionType(int newsPositionType, int zoneId, string listOfOrder);

        [OperationContract]
        List<NewsPositionEntity> GetScheduleByTypeAndOrder(int typeId, int order, int zoneId);

        [OperationContract]
        NewsPositionEntity GetNewsPositionByNewsPositionId(int newsPositionId);

        [OperationContract]
        WcfActionResponse SaveNewsPosition(NewsPositionEntity[] newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap);

        [OperationContract]
        WcfActionResponse SaveNewsPositionSchedule(int typeId, int zoneId, int order, string listNewsId,
                                                   string listScheduleDate, string listRemovePosition, string usernameForAction);

        [OperationContract]
        WcfActionResponse UnlockNewsPosition(int positionId);

        [OperationContract]
        WcfActionResponse UpdatePositionOrder(int typeId, int zoneId, string listPositionId);

        [OperationContract]
        WcfActionResponse UpdatePositionOrderByPositionId(long positionId, int positionOrder);

        [OperationContract]
        WcfActionResponse LockNewsPosition(int positionId, int lockForZoneId, DateTime expiredLock);

        [OperationContract]
        List<NewsPositionForMobileEntity> GetNewsPositionForMobile();

        [OperationContract]
        WcfActionResponse SaveNewsPositionForMobile(NewsPositionForMobileEntity[] newsPositionsForMobile, int avatarIndex, bool checkNewsExists);

        [OperationContract]
        WcfActionResponse LockPositionByType(long newsPositionId, int lockTypeId, int order, DateTime expiredLock);

        [OperationContract]
        WcfActionResponse LockPositionByNewsId(long newsId, int zoneId, int lockTypeId, int order, DateTime expiredLock);

        [OperationContract]
        WcfActionResponse UnlockPositionByType(int lockTypeId, int order);
        #endregion

        #region News history

        [OperationContract]
        List<NewsHistoryEntity> GetNewsHistoryByNewsId(long newsId);

        #endregion

        #region NewsAuthor

        [OperationContract]
        WcfActionResponse NewsAuthorInsert(NewsAuthorEntity newsAuthorEntity, ref int newsAuthorId);
        [OperationContract]
        WcfActionResponse NewsByAuthorInsert(NewsByAuthorEntity newsByAuthorEntity);
        [OperationContract]
        NewsAuthorEntity NewsAuthorAutoCompleted(NewsAuthorEntity newsAuthorEntity);
        [OperationContract]
        WcfActionResponse NewsAuthorUpdate(NewsAuthorEntity newsAuthorEntity);
        [OperationContract]
        WcfActionResponse NewsAuthorDelete(int id);
        [OperationContract]
        NewsAuthorEntity NewsAuthorGetById(int id);
        [OperationContract]
        NewsAuthorEntity[] ListNewsAuthorByUser(string username);
        [OperationContract]
        NewsAuthorEntity[] ListNewsAuthorSearch(string keyword);
        [OperationContract]
        NewsAuthorEntity[] NewsAuthorGetAll();
        [OperationContract]
        NewsByAuthorEntity[] ListNewsAuthorInNews(long newsId);
        [OperationContract]
        NewsAuthorEntity GetByUserData(string userData, AuthorType authorType);
        #endregion

        #region News version

        [OperationContract]
        WcfActionResponse UpdateNewsVersion(NewsEntity news, string lastModifiedBy);
        [OperationContract]
        WcfActionResponse ReleaseNewsVersion(long newsId, string lastModifiedBy);
        [OperationContract]
        WcfActionResponse CreateVersionForExistsNews(long newsId, string createdBy, ref long newsVersionId);
        [OperationContract]
        WcfActionResponse ReleaseFirstNewsVersion(long newsId, string userForUpdateAction);
        [OperationContract]
        WcfActionResponse RemoveNewsVersion(long newsId, string lastModifiedBy);
        [OperationContract]
        WcfActionResponse AutoSaveNewsVersion(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername);
        [OperationContract]
        NewsVersionWithSimpleFieldsEntity[] GetNewsVersionByNewsId(long newsId);
        [OperationContract]
        NewsVersionWithSimpleFieldsEntity[] GetNewsVersionByNewsIdForUser(long newsId, string lastModifiedBy);
        [OperationContract]
        NewsVersionEntity GetNewsVersionById(long id);
        [OperationContract]
        NewsVersionEntity GetLastestNewsVersionByNewsId(long newsId, string lastModifiedBy);
        [OperationContract]
        NewsVersionEntity GetNewsVersionByNewsIdAndCreatedBy(long newsId, string createdBy);

        #endregion

        [OperationContract]
        NewsPublishEntity GetNewsPublishByNewsId(long newsId);

        [OperationContract]
        NewsContentEntity GetNewsContentByNewsId(long newsId);

        [OperationContract]
        List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId);

        [OperationContract]
        WcfActionResponse InsertNewsInZone(NewsInZoneEntity newsInZone);

        [OperationContract]
        NewsPublishForObjectBoxEntity[] SearchNewsForNewsTagRelation(int zoneId, int tagId, int pageIndex, int pageSize,
                                                                     ref int totalRow);

        #endregion

        #region For NewsConfig

        [OperationContract]
        WcfActionResponse SetNewsConfigValue(string configName, string configValue);

        [OperationContract]
        NewsConfigEntity[] GetListConfigByGroupKey(string groupConfigKey);

        [OperationContract]
        NewsConfigEntity GetNewsConfigByConfigName(string configName);

        [OperationContract]
        WcfActionResponse SetValueForStaticHtmlTemplate(string configName, string configLabel, string configValue, EnumStaticHtmlTemplateType configType);

        [OperationContract]
        NewsConfigEntity[] GetListConfigForStaticHtmlTemplate(string groupConfigKey, EnumStaticHtmlTemplateType type);

        #endregion

        #region For News OS an News Crawler

        #region News OS

        [OperationContract]
        NewsOsEntity GetNewsOsDetail(long newsId);
        [OperationContract]
        ListNewsOsEntity[] GetListNewsOs(string keyword, string catId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        long ReceiverNewsOs(long newsId, string content, string avatar, string username);
        [OperationContract]
        WcfActionResponse DeleteNewsOsById(long newsId);
        [OperationContract]
        WcfActionResponse DeleteNewsOsByListId(string listNewsId);

        #endregion

        #region News Crawler

        [OperationContract]
        NewsCrawlerEntity GetNewsCrawlerDetail(int newsId);
        [OperationContract]
        NewsCrawlerEntity[] GetListNewsCrawler(int catId, int siteId, int hot, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        long ReceiverNewsCrawler(int newsId, string content, string avatar, string zoneId, string username);
        [OperationContract]
        WcfActionResponse InsertNewsCrawlerSite(string siteLink, string siteName);
        [OperationContract]
        WcfActionResponse InvisibleNewsCrawlerSite(int id);
        [OperationContract]
        WcfActionResponse DeleteNewsCrawlerById(int newsId);
        [OperationContract]
        WcfActionResponse DeleteNewsCrawlerByListId(string listNewsId);
        [OperationContract]
        CategoryCrawlerEntity[] GetListCrawlerCategory();
        [OperationContract]
        SiteCrawlerEntity[] GetListCrawlerSite();

        #endregion

        #endregion

        #region For NewsCachedMonitor

        [OperationContract]
        List<UpdateCachedEntity> GetUpdateCachedItemForNews(PageSetting pageSetting);
        [OperationContract]
        WcfActionResponse UpdateCachedProcessIdForNews(string listOfProcessId);
        [OperationContract]
        WcfActionResponse UpdateCachedProcessIdForTag(string listOfProcessId);

        #endregion

        #region For Zone

        [OperationContract]
        ZoneEntity GetZoneById(int zoneId);
        [OperationContract]
        List<ZoneEntity> GetListZoneByParentId(int parentZoneId);
        [OperationContract]
        List<ZoneEntity> GetZoneByKeyword(string keyword);
        [OperationContract]
        List<ZoneEntity> GetAllZone();
        [OperationContract]
        List<ZoneWithSimpleFieldEntity> GetAllZoneActiveWithTreeView(bool getParentZoneOnly);
        [OperationContract]
        List<ZoneWithSimpleFieldEntity> GetAllZoneWithTreeView(bool getParentZoneOnly);
        [OperationContract]
        List<ZoneWithSimpleFieldEntity> GetAllZoneActivedWithTreeView(bool getParentZoneOnly);
        [OperationContract]
        List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeView(string username);
        [OperationContract]
        List<ZoneWithSimpleFieldEntity> GetAllZoneByUserAndPermissionWithTreeView(string username, int permissionId);
        [OperationContract]
        List<ZoneWithSimpleFieldEntity> GetAllZoneByUserAndListPermissionWithTreeView(string username, params int[] permissionIds);
        [OperationContract]
        List<ZoneWithSimpleFieldEntity> GetAllZoneActivedByUserAndListPermissionWithTreeView(string username, params int[] permissionIds);
        [OperationContract]
        List<ZoneEntity> GetListZoneByUserId(int userId);
        [OperationContract]
        List<ZoneEntity> GetListZoneByUsername(string username);
        [OperationContract]
        List<ZoneDefaultTagEntity> GetAllDefaultTagForZone();
        [OperationContract]
        WcfActionResponse Insert(ZoneEntity zoneEntity, string username);
        [OperationContract]
        WcfActionResponse Update(ZoneEntity zoneEntity, string username);
        [OperationContract]
        WcfActionResponse MoveUp(int zoneId, string username);
        [OperationContract]
        WcfActionResponse MoveDown(int zoneId, string username);
        [OperationContract]
        WcfActionResponse UpdateInvisibled(int zoneId, string username);
        [OperationContract]
        WcfActionResponse UpdateAllowComment(int zoneId, string username);
        [OperationContract]
        List<ZoneEntity> GetZoneByNewsId(long newsId);
        [OperationContract]
        List<ZonePhotoWithSimpleFieldEntity> GetAllZonePhotoWithTreeView();
        #endregion

        #region For NewsUsePhoto

        [OperationContract]
        WcfActionResponse UpdatePhotoPublishedInNews(long newsId, string photoPublishedIds, string photoPublishedDescriptions);
        [OperationContract]
        NewsUsePhotoEntity[] GetAllNewsUsePhotoByNewsId(long newsId);

        [OperationContract]
        int CountPhotoPublishedByNewsId(long newsId);

        #endregion

        #region ForNewsUseVideo

        [OperationContract]
        WcfActionResponse UpdateVideoInNews(long newsId, string videoIds, string videoDescriptions);

        [OperationContract]
        NewsUseVideoEntity[] GetAllNewsUseVideoByNewsId(long newsId);

        [OperationContract]
        int CountVideoByNewsId(long newsId);
        #endregion

        #region For NewsSource

        [OperationContract]
        WcfActionResponse NewsSourceInsert(NewsSourceEntity newsSourceEntity, ref int newsSourceId);

        [OperationContract]
        WcfActionResponse NewsSourceUpdate(NewsSourceEntity newsSourceEntity);

        [OperationContract]
        WcfActionResponse NewsSourceDelete(int id);

        [OperationContract]
        NewsSourceEntity NewsSourceGetById(int id);

        [OperationContract]
        NewsSourceEntity[] NewsSourceGetAll(string keyword);

        [OperationContract]
        NewsBySourceEntity[] ListNewsSourceInNews(long newsId);

        #endregion

        #region For EmbedGroup

        [OperationContract]
        WcfActionResponse EmbedGroupInsert(EmbedGroupEntity embedGroup, string listOfZoneId);
        [OperationContract]
        WcfActionResponse EmbedGroupUpdate(EmbedGroupEntity embedGroup, string listOfZoneId);
        [OperationContract]
        WcfActionResponse EmbedGroupUpdateSortOrder(string listOfEmbedGroupId);
        [OperationContract]
        EmbedGroupEntity[] EmbedGroupGetAll();
        [OperationContract]
        EmbedGroupWithDetailEntity EmbedGroupGetById(int id);
        [OperationContract]
        WcfActionResponse EmbedGroupDelete(int id);

        #endregion

        #region For BoxBanner

        [OperationContract]
        WcfActionResponse InsertBoxBanner(BoxBannerEntity boxBanner, string zoneIdList);

        [OperationContract]
        WcfActionResponse UpdateBoxBanner(BoxBannerEntity boxBanner, string zoneIdList);

        [OperationContract]
        WcfActionResponse UpdateBoxBannerPriority(string listOfBoxBannerId);

        [OperationContract]
        WcfActionResponse DeleteBoxBanner(int boxBannerId);

        [OperationContract]
        BoxBannerEntity GetBoxBannerById(int id);

        [OperationContract]
        List<BoxBannerEntity> GetListBoxBannerByZoneIdAndPosition(int zoneId, int position, EnumBoxBannerStatus status, EnumBoxBannerType type);
        [OperationContract]
        List<BoxBannerZoneEntity> GetListZoneByBoxBannerId(int boxBannerId);

        #endregion

        #region NewsLive -  Bài tường thuật

        [OperationContract]
        WcfActionResponse InsertNewsLive(NewsLiveEntity news, ref NewsLiveDetail returnNews);
        [OperationContract]
        WcfActionResponse UpdateNewsLive(NewsLiveEntity news);
        [OperationContract]
        WcfActionResponse DeleteNewsLive(long id, string accountName);
        [OperationContract]
        WcfActionResponse AddNewsLiveAuthor(NewsLiveAuthorEntity newsLiveAuthorEntity);
        [OperationContract]
        WcfActionResponse DeleteNewsLiveAuthor(long newsId, int authorId);
        [OperationContract]
        WcfActionResponse PublishNewsLive(long id, string accountName);
        [OperationContract]
        WcfActionResponse RollbackNewsLive(long id, string accountName);
        [OperationContract]
        WcfActionResponse TrashNewsLive(long id, string accountName);
        [OperationContract]
        WcfActionResponse UpdateContentNewsLive(long id, string content);
        [OperationContract]
        WcfActionResponse UpdateNewsLiveSettings(NewsLiveSettingsEntity newsLiveSettings);
        [OperationContract]
        NewsLiveForList GetAllNewsLive(long newsId, NewsLiveStatus status, int order);
        [OperationContract]
        NewsLiveEntity[] GetPagingNewsLive(long newsId, NewsLiveStatus status, int order, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        NewsLiveDetail GetDetailNewsLive(long id);
        [OperationContract]
        NewsLiveAuthorEntity[] GetNewsLiveAuthorList(long newsLiveId);
        [OperationContract]
        NewsLiveSettingsEntity GetNewsLiveSettings(long newsId);

        #endregion

        #region NewsEmbedBox

        [OperationContract]
        NewsEmbedBoxListEntity[] GetListNewsEmbedBox(int type, int status);
        [OperationContract]
        WcfActionResponse InsertNewsEmbedBox(NewsEmbebBoxEntity newsEmbedBox);
        [OperationContract]
        WcfActionResponse UpdateNewsEmbedBox(string listNewsId, int type);
        [OperationContract]
        WcfActionResponse UpdateStatusNewsEmbedBox(string listOfBoxNewsId, int type, int status);
        [OperationContract]
        WcfActionResponse DeleteNewsEmbedBox(long newsId, int type);

        #endregion

        #region NewsEmbedBoxOnPage

        [OperationContract]
        NewsEmbedBoxOnPageListEntity[] GetListNewsEmbedBoxOnPage(int zoneId, int type);
        [OperationContract]
        WcfActionResponse InsertNewsEmbedBoxOnPage(NewsEmbedBoxOnPageEntity newsEmbedBox);
        [OperationContract]
        WcfActionResponse UpdateNewsEmbedBoxOnPage(string listNewsId, int zoneId, int type);
        [OperationContract]
        WcfActionResponse DeleteNewsEmbedBoxOnPage(long newsId, int zoneId, int type);

        #endregion

        #region NewsChild

        [OperationContract]
        WcfActionResponse UpdateNewsChildVersion(string userDoAction, long parentNewsId, int order, string body, ref NewsChildVersionEntity returnNewsChild);
        [OperationContract]
        WcfActionResponse UpdateChildVersion(NewsChildVersionEntity entity);
        [OperationContract]
        WcfActionResponse RemoveNewsChildVersion(string userDoAction, long parentNewsId, int order);
        [OperationContract]
        NewsChildVersionEntity GetNewsChildDetailVersion(string userDoAction, long parentNewsId, int order);
        [OperationContract]
        WcfActionResponse InsertNewsChild(NewsChildEntity entity);
        [OperationContract]
        WcfActionResponse UpdateNewsChild(NewsChildEntity entity);
        [OperationContract]
        WcfActionResponse InsertNewsChildVersion(NewsChildVersionEntity entity);
        [OperationContract]
        List<NewsChildEntity> GetNewsChildByNewsId(long newsId, bool isGetContent = false);
        [OperationContract]
        List<NewsChildVersionEntity> GetNewsChildVersionByNewsId(long newsId, bool isGetContent = false);
        [OperationContract]
        NewsChildEntity GetByNewsAndOrderId(long parentNewsId, int order);
        [OperationContract]
        WcfActionResponse DeleteNewsChild(long parentNewsId, int order);
        [OperationContract]
        WcfActionResponse DeleteNewsChildVersion(long versionId, int order);
        #endregion

        #region News Recommendation

        [OperationContract]
        NewsInListEntity[] RecommendByActiveUsers(int zoneId, int top);

        #endregion

        #region For Royalties

        [OperationContract]
        NewsForRoyaltiesEntity[] GetRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                              string author, int pageIndex, int pageSize, int order, bool isGetTotal,
                                              ref int totalRow, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0);

        [OperationContract]
        NewsForRoyaltiesEntity[] GetBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate,
                                                        string creator, string author, int pageIndex, int pageSize,
                                                        int order, bool isGetTotal, ref int totalRow,
                                                        string NewsCategories, NewsType newsType, int viewCountFrom = 0,
                                                        int viewCountTo = 0, int originalId = 0);
        [OperationContract]
        NewsForRoyaltiesEntity[] GetPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                                string author, int pageIndex, int pageSize, int order, bool isGetTotal,
                                                ref int totalRow, string NewsCategories, NewsType newsType,
                                                int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0);
        [OperationContract]
        List<NewsForRoyaltiesEntity> GetRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author,
                                                    int pageIndex, int pageSize, int order, ref int totalRow,
                                                    string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0);

        [OperationContract]
        List<NewsForRoyaltiesEntity> GetPRRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate,
                                                      string creator, int author, int pageIndex, int pageSize, int order,
                                                      ref int totalRow, string NewsCategories, NewsType newsType,
                                                      int viewCountFrom = 0, int viewCountTo = 0);
        [OperationContract]
        decimal GetTotalRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string newsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0);

        [OperationContract]
        WcfActionResponse SetPrice(long newsId, decimal price);

        [OperationContract]
        WcfActionResponse SetNewsCategory(long newsId, int newsCategory);

        [OperationContract]
        WcfActionResponse SetBonusPrice(long newsId, decimal bonusPrice);

        [OperationContract]
        WcfActionResponse UpdateIsOnHome(long newsId, bool isOnHome);

        [OperationContract]
        WcfActionResponse UpdateIsFocus(long newsId, bool isFocus);

        [OperationContract]
        WcfActionResponse UpdateDisplayInSlide(long newsId, int displayInSlide);

        [OperationContract]
        WcfActionResponse UpdateDisplayPosition(long id, int displayPosition, string accountName);

        [OperationContract]
        WcfActionResponse UpdatePriority(long id, int priority, string accountName);

        [OperationContract]
        WcfActionResponse SetNoteRoyalties(long newsId, string note);

        #endregion

        #region NewsGroup

        [OperationContract]
        WcfActionResponse UpdateTopMostRead(int groupNewsId, int withinDays, int rootZoneId, int excludeLastDays);

        [OperationContract]
        WcfActionResponse UpdateTopMostReadDaily(int groupNewsId, int rootZoneId, int excludeLastDays);

        [OperationContract]
        WcfActionResponse UpdateTopMostReadHourly(int groupNewsId, int rootZoneId, int excludeLastDays);

        [OperationContract]
        WcfActionResponse UpdateTopMostComment(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays);

        [OperationContract]
        NewsGroupEntity NewsGroupGetById(int id);

        [OperationContract]
        List<ZoneEntity> GetGroupNewsZoneByParentId(int zoneId);

        [OperationContract]
        WcfActionResponse UpdateTopMostReadByZone(int groupNewsId, int withinDays, int excludeLastDays,
                                                  int zoneId, int itemCount);

        [OperationContract]
        WcfActionResponse UpdateTopMostReadV2(int groupNewsId, int withinDays, int excludeLastDays, int itemCount);

        [OperationContract]
        WcfActionResponse UpdateTopMostCommentByZone(int groupNewsId, int withinDays, int rootZoneId, int objectType,
                                                     int excludeLastDays, int topNews);

        [OperationContract]
        WcfActionResponse UpdateTopMostCommentV2(int groupNewsId, int withinDays, int objectType, int excludeLastDays,
                                                 int topNews);

        [OperationContract]
        List<NewsGroupDetailEntity> UpdateTopMostReadV3(int groupNewsId, int zoneId, int withinHours, int totalRow);

        [OperationContract]
        WcfActionResponse UpdateTopMostLikeShare(int groupNewsId, int zoneId, string lstNewsId, string likeShareCountList);

        [OperationContract]
        WcfActionResponse UpdateTopCommentCountInDay(int groupNewsId, string lstNewsId, string lstCommentCount);
        #endregion

        #region NewsPositionSchedule
        [OperationContract]
        WcfActionResponse InsertNewsPositionSchedule(NewsPositionScheduleEntity newsPosition);
        [OperationContract]
        WcfActionResponse UpdateNewsPositionSchedule(NewsPositionScheduleEntity newsPosition);
        [OperationContract]
        WcfActionResponse DeleteNewsPositionSchedule(int newsPositionScheduleId);
        [OperationContract]
        NewsPositionScheduleEntity[] GetListNewsPositionScheduleGetByPositionTypeId(int positionTypeId);

        [OperationContract]
        NewsPositionEntity[] GetListNewsPositionScheduleByPositionTypeIdAndOrder(int typeId, int order, int zoneId);
        [OperationContract]
        WcfActionResponse AutoUpdateNewsPositionSchedule(int maxProcess);

        #endregion

        #region NewsWaitRepublish
        [OperationContract]
        WcfActionResponse InsertNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions);

        [OperationContract]
        WcfActionResponse ChangeNewsWaitRepublishStatusToProcessed(long newsId, string userForUpdateAction);

        [OperationContract]
        WcfActionResponse UpdateNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions);

        [OperationContract]
        NewsInListEntity[] SearchNewsWaitRepublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, NewsFilterFieldForUsername filterFieldForUsername, NewsSortExpression sortOrder, NewsWaitRepublishStatus newsStatus, NewsType newsType, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        NewsDetailForEditEntity GetNewsByNewsWaitRepublishId(long newsId, string currentUsername);

        [OperationContract]
        int NewsWaitRepublishCountDown(string username, string zoneIds, NewsFilterFieldForUsername filterFieldForUsername, NewsType newsType);

        [OperationContract]
        List<NewsPublishEntity> GetNewsPublishByDate(int zoneIds, DateTime fromDate, DateTime toDate);
        [OperationContract]
        NewsPublishEntity GetByNewsIdAndZoneId(long newsId, int zoneId);
        #endregion

        #region NewsAnalytic

        [OperationContract]
        List<NewsAnalyticEntity> GetLastUpdateViewCountInNewsAnalytic(DateTime lastUpdateViewCount);

        [OperationContract]
        List<NewsAnalyticEntity> GetTopLastestNewsInNewsAnalytic(int top);

        [OperationContract]
        List<NewsAnalyticV3Entity> GetTopHighestViewCountByZone(int zone, int hour, int top);

        [OperationContract]
        List<NewsAnalyticV3Entity> GetTopHighestCommentCountByZone(int zone, int hour, int top);

        [OperationContract]
        List<NewsPublishEntity> GetListNewsByListNewsId(string listNewsId);

        #endregion

        #region NewsAnalyticV2

        [OperationContract]
        List<NewsAnalyticV2Entity> GetLastUpdateViewCount(long lastUpdateViewCount);

        [OperationContract]
        List<NewsAnalyticV2Entity> GetAllViewFromDateToNow(DateTime fromDate, int maxRecord);

        [OperationContract]
        List<NewsAnalyticV2Entity> GetTopMostReadInHour(int top, int zoneId);

        #endregion

        #region NewsAnalyticAdtech

        [OperationContract]
        WcfActionResponse ResetAdtechViewCount();

        [OperationContract]
        WcfActionResponse UpdateAdtechViewCount(long newsId, int viewCount);

        [OperationContract]
        WcfActionResponse BatchUpdateAdtechViewCount(string listNewsId, string listViewCount);

        [OperationContract]
        List<NewsWithAnalyticEntity> GetTopMostReadInHourByAdtechViewCount(int top);

        [OperationContract]
        List<NewsAnalyticEntity> GetLastUpdateAdtechViewCount(DateTime lastUpdateViewCount);

        [OperationContract]
        List<NewsAnalyticEntity> GetTopLastestAdtechViewCount(int top);

        #endregion

        #region NewsLogExternalAction

        [OperationContract]
        List<NewsLogExternalActionEntity> GetExternalActionLogByNewsIdAndActionType(long newsId,
                                                                              EnumNewsLogExternalAction actionType);

        [OperationContract]
        WcfActionResponse InsertExternalActionLog(NewsLogExternalActionEntity newsLogExternalAction);

        #endregion

        #region For NewsMaskOnline
        [OperationContract]
        List<NewsMaskOnlineEntity> ListNewsMaskOnline(string keyword, DateTime fromDate, DateTime toDate,
                                                  EnumMaskSourceId SourceID, EnumMaskStatus status, int pageIndex, int pageSize, int Mode = -1);
        #endregion

        #region Update likecount Fb
        [OperationContract]
        List<NewsEntity> SearchNewsByDateRange(DateTime startDate, DateTime endDate, int status);
        [OperationContract]
        WcfActionResponse UpdateLikeCount(long newsId, int likeCount);
        #endregion

        [OperationContract]
        NewsEntity GetNewsByTitle(string title);

        [OperationContract]
        int CountNewsBySource(int sourceId, DateTime fromDate, DateTime toDate);

        [OperationContract]
        WcfActionResponse UpdatePhotoVideoCount(long newsId, int photoCount, int videoCount);

        [OperationContract]
        WcfActionResponse UpdateNewsRelationJson(long id, List<NewsPublishForNewsRelationEntity> newsRelation);

        [OperationContract]
        List<NewsPublishEntity> GetNewsPublishByHour(int hour, int zoneId);

        [OperationContract]
        CategoryCounterEntity GetZoneCounter(int zoneId, DateTime fromDate, DateTime endDate);

        [OperationContract]
        List<NewsInListEntity> SearchNewsPublishedByKeyword(string keyword);
        [OperationContract]
        List<NewsHotEntity> NewsHot_AllForRedis();

        #region News Top Hot
        [OperationContract]
        WcfActionResponse NewsHot_Insert(NewsHotEntity entity);

        [OperationContract]
        WcfActionResponse NewsHot_Insert2(NewsHotEntity entity);

        [OperationContract]
        WcfActionResponse NewsHot_Delete(string NewsIds, int type);

        [OperationContract]
        WcfActionResponse NewsHot_Lock(int newsHotId);

        [OperationContract]
        WcfActionResponse NewsHot_Unlock(int newsHotId);

        [OperationContract]
        NewsHotEntity NewsHot_ChangeNewsInLockPosition(int newsHotId, long newsId);

        [OperationContract]
        long NewsHot_UnpublishOrBomdNews(long newsId);

        [OperationContract]
        WcfActionResponse NewsHot_CheckExists();

        [OperationContract]
        WcfActionResponse NewsHot_UpdateType();

        [OperationContract]
        List<NewsHotEntity> NewsHot_GetByType(int type);
        #endregion

        #region Seoer Update News
        [OperationContract]
        WcfActionResponse Seoer_Update(SeoerNewsEntity news);
        #endregion
        #region News full log

        [OperationContract]
        List<NewsFullLogEntity> GetNewsFullLog(string keyword);
        #endregion

        #region ViewPlus
        [OperationContract]
        List<NewsViewPlusListEntity> ViewPlus_GetList(int ZoneId, DateTime DateFrom, DateTime DateTo, int PageIndex, int PageSize, int bidType, ref int TotalRow);

        [OperationContract]
        WcfActionResponse ViewPlus_Delete(string Ids);

        [OperationContract]
        WcfActionResponse ViewPlus_Insert(ViewPlusEntity viewPlus);
        #endregion
        [OperationContract]
        WcfActionResponse RecieveFeedbackNewsIntoCms(NewsEntity newsInfo, ref long newsId);
    }
}
