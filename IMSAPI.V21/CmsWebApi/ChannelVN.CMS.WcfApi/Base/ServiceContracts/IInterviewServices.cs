﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    public interface IInterviewServices : IErrorMessageMapping
    {
        #region Interview

        #region Get

        [OperationContract]
        InterviewDetailEntity GetInterviewForEdit(int interviewId);
        [OperationContract]
        InterviewEntity GetInterviewByInterviewId(int interviewId);
        [OperationContract]
        InterviewWithSimpleFieldEntity GetInterviewShortInfoByInterviewId(int interviewId);
        [OperationContract]
        List<InterviewWithSimpleFieldEntity> SearchInterview(string keyword, int isFocus, int isActived, int status, DateTime startDateFrom, DateTime startDateTo, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<InterviewForSuggestionEntity> SearchInterviewForSuggestion(int top, string keyword, int isFocus, int isActived);
        [OperationContract]
        InterviewPublishedEntity GetInterviewPublishedByInterviewId(int interviewId);

        #endregion

        #region Update

        [OperationContract]
        WcfActionResponse InsertInterview(InterviewEntity interview, ref int interviewId);
        [OperationContract]
        WcfActionResponse UpdateInterview(InterviewEntity interview);
        [OperationContract]
        WcfActionResponse DeleteInterviewByInterviewId(int interviewId);
        [OperationContract]
        WcfActionResponse UpdateInterviewFocus(int interviewId, bool isFocus);
        [OperationContract]
        WcfActionResponse UpdateInterviewActive(int interviewId, bool isActived);
        [OperationContract]
        WcfActionResponse PublishInterviewContent(int interviewId);
        [OperationContract]
        WcfActionResponse RefreshRollingNewsEvent(int interviewId);

        #endregion

        #endregion

        #region Interview channel

        #region Get

        [OperationContract]
        InterviewChannelEntity GetInterviewChannelByInterviewChannelId(int interviewChannelId);
        [OperationContract]
        List<InterviewChannelEntity> GetInterviewChannelDetailByInterviewId(int interviewId, string username);
        [OperationContract]
        List<InterviewChannelEntity> GetInterviewChannelByInterviewIdAndUsername(int interviewId, string username);
        [OperationContract]
        bool CheckChannelRoleForUser(int interviewId, string username, EnumInterviewChannelRole channelRole);
        [OperationContract]
        List<InterviewChannelEntity> GetInterviewChannelProcess(int interviewChannelId);
        #endregion

        #region Update
        [OperationContract]
        WcfActionResponse InsertInterviewChannel(InterviewChannelEntity interviewChannel, ref int interviewChannelId);
        [OperationContract]
        WcfActionResponse UpdateInterviewChannel(InterviewChannelEntity interviewChannel);
        [OperationContract]
        WcfActionResponse RemoveInterviewChannel(int interviewChannelId);
        #endregion

        #endregion

        #region Interview question

        #region Get

        [OperationContract]
        InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId);

        [OperationContract]
        List<InterviewQuestionWithSimpleFieldEntity> SearchInterviewQuestion(int interviewId, string keyword, int type,
                                                                             string username,
                                                                             EnumInterviewQuestionFieldFilterForUsername
                                                                                 fieldFilterForUsername,
                                                                             EnumInterviewQuestionStatus status,
                                                                             bool invertStatus,
                                                                             EnumInterviewQuestionOrder orderBy,
                                                                             int pageIndex,
                                                                             int pageSize, ref int totalRow);

        [OperationContract]
        List<InterviewQuestionEntity> GetAllPublishedQuestionByInterviewId(int interviewId);

        #endregion

        #region Working flow

        [OperationContract]
        WcfActionResponse ReceivedInterviewQuestion(int interviewQuestionId);
        [OperationContract]
        WcfActionResponse DistributeInterviewQuestion(int interviewQuestionId, int interviewChannelId);
        [OperationContract]
        WcfActionResponse ForwardInterviewQuestion(int interviewQuestionId, int interviewChannelId);
        [OperationContract]
        WcfActionResponse SendAnswerForInterviewQuestion(int interviewQuestionId, string answerContent);
        [OperationContract]
        WcfActionResponse ReturnInterviewQuestion(int interviewQuestionId);
        [OperationContract]
        WcfActionResponse SendInterviewQuestionWaitForPublish(int interviewQuestionId);
        [OperationContract]
        WcfActionResponse PublishInterviewQuestion(int interviewQuestionId);
        [OperationContract]
        WcfActionResponse UnpublishInterviewQuestion(int interviewQuestionId);

        #endregion

        #region Update

        [OperationContract]
        WcfActionResponse InsertInterviewQuestion(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId);

        [OperationContract]
        WcfActionResponse InsertExternalInterviewQuestion(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId);

        [OperationContract]
        WcfActionResponse InsertExtensionInterviewQuestionData(int interviewId, string data,
                                                               ref int interviewQuestionId);

        [OperationContract]
        WcfActionResponse UpdateInterviewQuestion(InterviewQuestionEntity interviewQuestion);

        [OperationContract]
        WcfActionResponse UpdateInterviewQuestionPriority(int interviewId, string listOfInterviewQuestionId);

        [OperationContract]
        WcfActionResponse DeleteInterviewQuestion(int interviewQuestionId);

        #endregion

        #endregion

        #region Interview channel process

        #region Update

        [OperationContract]
        WcfActionResponse UpdateInterviewChannelProcess(int interviewChannelId, string listProcessInterviewChannelId);

        #endregion

        #endregion

        #region InterviewV3

        #region GET
        [OperationContract]
        InterviewV3Entity InterviewV3GetById(int Id);
        #endregion

        #region UPDATE
        [OperationContract]
        WcfActionResponse InterviewV3Insert(InterviewV3Entity interviewV3Entity, out int interviewId);
        [OperationContract]
        WcfActionResponse InterviewV3Update(InterviewV3Entity interviewV3Entity);
        [OperationContract]
        WcfActionResponse InterviewV3UpdateStatus(int id, int status);
        #endregion

        #endregion

        #region InterviewV3Guests
        #region GET
        [OperationContract]
        InterviewV3GuestsEntity InterviewV3GuestsGetById(int id);
        [OperationContract]
        InterviewV3GuestsEntity InterviewV3GuestsGetInterviewIdAndUserName(int interviewId, string UserName);
        [OperationContract]
        List<InterviewV3GuestsEntity> InterviewV3GuestsGetByInterviewId(int interviewId, bool status);
        #endregion

        #region UPDATE
        [OperationContract]
        WcfActionResponse InterviewV3GuestsInsert(InterviewV3GuestsEntity interviewV3Guests, out int guestId);
        [OperationContract]
        WcfActionResponse InterviewV3GuestsUpdateInfo(InterviewV3GuestsEntity interviewV3Guests);

        [OperationContract]
        WcfActionResponse InterviewV3GuestsUpdateStatus(int guestId,bool status);

        [OperationContract]
        WcfActionResponse InterviewV3GuestsDelete(int guestId, int interviewId);
        #endregion
        #endregion

        #region InterviewV3Question
        #region GET
        [OperationContract]
        List<InterviewV3QuestionEntity> SearchInterviewV3Question(int interviewId, string keyword,
                                                                             EnumInterviewV3QuestionStatus status,                                                                            
                                                                             EnumInterviewV3QuestionOrder orderBy,
                                                                             int pageIndex,
                                                                             int pageSize, ref int totalRow);
        [OperationContract]
        List<InterviewV3QuestionEntity> InterviewV3QuestionGetListByInterviewId(int interviewId);
        [OperationContract]
        List<InterviewV3QuestionDisplayListEntity> InterviewV3QuestionSimpleListByInterviewId(int interviewId);
        [OperationContract]
        List<InterviewV3QuestionEntity> InterviewV3QuestionGetListByStatus(int interviewId, EnumInterviewV3QuestionStatus status, EnumInterviewV3QuestionOrder orderBy);

        [OperationContract]
        InterviewV3QuestionEntity InterviewV3QuestionGetById(int id);
        [OperationContract]
        WcfActionResponse InsertExtensionInterviewV3QuestionData(int interviewId, string data,
                                                               ref int interviewQuestionId);
        [OperationContract]
        WcfActionResponse UpdateInterviewV3QuestionPriority(int interviewId, string listOfInterviewQuestionId);
        #endregion

        #region UPDATE
        [OperationContract]
        WcfActionResponse InterviewV3QuestionUpdateQuestionContent(int questionId, string questionContent, string updateBy);
        [OperationContract]
        WcfActionResponse InterviewV3QuestionUpdateStatus(int questionId, EnumInterviewV3QuestionStatus status);
        [OperationContract]
        WcfActionResponse InterviewV3QuestionUpdatePriority(int questionId, int priority);
        [OperationContract]
        WcfActionResponse InterviewV3QuestionUpdate(InterviewV3QuestionEntity interviewV3QuestionEntity);
        [OperationContract]
        WcfActionResponse InterviewV3QuestionInsert(InterviewV3QuestionEntity interviewV3QuestionEntity, out int questionId);
        [OperationContract]
        WcfActionResponse InterviewV3QuestionReceiveQuestion(int questionId, string processUser, EnumInterviewV3QuestionStatus status);
        [OperationContract]
        WcfActionResponse DeleteInterviewV3Question(int questionId);
        #endregion
        #endregion

        #region InterviewV3Answers
        #region GET
        [OperationContract]
        List<InterviewV3AnswersCustomEntity> InterviewV3AnswersGetCustomListByQuestionId(int questionId);
        [OperationContract]
        InterviewV3AnswersEntity InterviewV3AnswersGetById(int id);
        [OperationContract]
        List<InterviewV3AnswersEntity> InterviewV3AnswersGetListByQuestionId(int questionId);
        #endregion

        #region UPDATE
        [OperationContract]
        WcfActionResponse InterviewV3AnswersInsert(InterviewV3AnswersEntity interviewV3AnswersEntity, out int answerId);
        [OperationContract]
        WcfActionResponse InterviewV3AnswersUpdate(InterviewV3AnswersEntity interviewV3AnswersEntity);
        [OperationContract]
        WcfActionResponse InterviewV3AnswersUpdateAnswerContent(int answerId, string content, string updateBy);
        [OperationContract]
        WcfActionResponse InterviewV3AnswersUpdateAnswerStatus(int answerId, bool status);
        [OperationContract]
        WcfActionResponse InterviewV3AnswersDelete(int answerId);
        #endregion
        #endregion
    }
}
