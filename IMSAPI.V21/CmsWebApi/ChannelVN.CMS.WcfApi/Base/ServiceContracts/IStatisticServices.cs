﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.WcfApi.Common;
using StatisticEntity;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    public interface IStatisticServices : IErrorMessageMapping
    {
        #region  StatisticV3
        [OperationContract]
        List<StatisticV3ViewCountEntity> ListPageViewAll(Int64 fromDate, Int64 toDate);
        [OperationContract]
        List<StatisticV3CategoryEntity> ListCategoryViewCount(string cateId, Int64 fromDate, Int64 toDate);
        [OperationContract]
        List<StatisticV3ViewCountEntity> ListPageViewAllByChannel(ChannelId channel, Int64 fromDate, Int64 toDate);
        [OperationContract]
        List<StatisticV3CategoryEntity> ListCategoryViewCountByChannel(ChannelId channel, string cateId, Int64 fromDate,
                                                              Int64 toDate);
        #endregion

        #region Thống kê lượng truy cập

        [OperationContract]
        StatisticDomainEntity[] GetListStatisticDomainDateNow();
        [OperationContract]
        StatisticDomainEntity[] GetListStatisticDomainByDate(string fromDate, string toDate);
        [OperationContract]
        StatisticEntity.StatisticEntity[] GetListStatisticAllCateDateNow();
        [OperationContract]
        StatisticEntity.StatisticEntity[] GetListStatisticCateByDate(string cateId, string fromDate, string toDate);
        [OperationContract]
        List<StatisticV3ViewCountEntity> ListPageViewAllMobileByChannel(ChannelId channel, Int64 fromDate, Int64 toDate);
        [OperationContract]
        List<StatisticV3CategoryEntity> ListCategoryViewCountMobileByChannel(ChannelId channel, string cateId,
                                                                             Int64 fromDate, Int64 toDate);
        [OperationContract]
        List<StatisticV3CategoryEntity> ListCategoryViewCountSummaryByChannel(ChannelId channel, string cateId,
                                                                              Int64 fromDate, Int64 toDate);

        #endregion

        #region Thống kê sản lượng bài viết

        [OperationContract]
        NewsStatisticEntity[] GetStatisticStatusNewsByAccount(string account, int status, string startDate, string endDate);
        [OperationContract]
        NewsStatisticEntity[] GetStatisticStatusNewsByAccountAdmin(string createdBy, int status, string startDate, string endDate);
        [OperationContract]
        NewsStatisticTotalProduction[] GetStatisticNewsTotalProduction(string zoneId, string startDate, string endDate);
        [OperationContract]
        NewsStatisticTotalProduction[] GetStatisticNewsTotalProductionUser(string zoneId, string startDate, string endDate, string userName);

        [OperationContract]
        ViewCountByUserEntity[] GetUserViewCount(string startDate, string endDate);

        [OperationContract]
        NewsStatisticSourceEntity[] GetStatisticNewsBySourceId(int sourceId, string startDate, string endDate);

        #endregion

        [OperationContract]
        NewsStatisticTotalProduction[] GetStatisticNewsComment(string zoneId, DateTime startDate, DateTime endDate);

        [OperationContract]
        List<StatisticV3TagVisitEntity> ListTagViewCountByChannel(StatisticTagChannel channel, Int64 fromDate,
                                                                  Int64 toDate);

        [OperationContract]
        WcfActionResponse UpdateZoneViewByDay(PageViewByZoneEntity updateObject);
        [OperationContract]
        WcfActionResponse UpdateAllSiteViewByDay(PageViewAllSiteEntity updateObject);
        [OperationContract]
        WcfActionResponse GetLastUpdateZoneViewRun();
        [OperationContract]
        WcfActionResponse PageViewAllSiteGetData(DateTime dateFrom, DateTime dateTo,string channelName);
        [OperationContract]
        WcfActionResponse PageViewByZoneGetData(DateTime dateFrom, DateTime dateTo,string zoneIds, string channelName);
        [OperationContract]
        ChannelEntity GetChannelByName(string channelName);
    }
}
