﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    public interface ITagServices : IErrorMessageMapping
    {
        #region Tag

        [OperationContract]
        TagEntity[] SearchAllByKeyword(string keyword, int zoneId, bool isThread);
        [OperationContract]
        TagScoreEntity[] ListTagScore(long TagId);
        [OperationContract]
        TagEntity[] SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, EnumSearchTagOrder orderBy,
                              int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false);

        [OperationContract]
        TagEntity[] Tag_SearchInBoxEmbed(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        TagEntity[] Tag_SearchInBoxEmbed_Approved(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        TagWithSimpleFieldEntity[] SearchTagForSuggestion(int top, int zoneId, string keyword, int isHot, EnumSearchTagOrder orderBy, bool getTagHasNewsOnly);
        [OperationContract]
        TagEntityDetail GetTagByTagId(long tagId);
        [OperationContract]
        TagEntity[] GetTagByListOfTagId(string listOfTagId);
        [OperationContract]
        TagEntity GetTagByTagName(string name, bool getTagHasNewsOnly = false);
        [OperationContract]
        TagEntity[] GetTagByParentTagId(long parentTagId);
        [OperationContract]
        WcfActionResponse InsertTag(TagEntity tag, int zoneId, string zoneIdList);
        [OperationContract]
        WcfActionResponse Update_RelationTag(long tagId, string tagRelations);
        [OperationContract]
        WcfActionResponse InsertTagScore(TagScoreEntity tagscore);
        [OperationContract]
        WcfActionResponse DeleteTagScore(long TagId);
        [OperationContract]
        long InsertNewTag(TagEntity tag, ref long id);
        [OperationContract]
        WcfActionResponse UpdateTagList(string[] tagNames, int zoneId, string zoneIdList);
        [OperationContract]
        WcfActionResponse Update(TagEntity tag, int zoneId, string zoneIdList);
        [OperationContract]
        WcfActionResponse DeleteById(long tagId);
        [OperationContract]
        WcfActionResponse UpdatePriority(long tagId, long priority);
        [OperationContract]
        WcfActionResponse UpdateTagHot(long tagId, bool isHotTag);
        [OperationContract]
        WcfActionResponse AddNews(string newsIds, long tagId, int tagMode);
        [OperationContract]
        WcfActionResponse UpdateNewsInTagNews(long tagId, string deleteNewsId, string addNewsId, int tagMode);
        [OperationContract]
        List<TagWithSimpleFieldEntity> GetTagByListOfTagName(List<string> listTagName);
        [OperationContract]
        List<TagWithSimpleFieldEntity> GetCleanTagByTagWordCount(int wordCount);
        [OperationContract]
        List<TagWithSimpleFieldEntity> GetCleanTagStartByKeyword(string startByKeyword, int minWordCount,
                                                                 int maxWordCount);
        [OperationContract]
        GateGameTagEntity GetGateGameTagByTagId(long tagId);

        [OperationContract]
        WcfActionResponse InsertGateGameTag(GateGameTagEntity gateGameTagEntity);
        [OperationContract]
        List<TagEntity> GetListTagByNewsId(long newsId);
        [OperationContract]
        List<TagWithSimpleFieldEntity> GetListTagAndNewsInfoByNewsId(long newsId, ref int newsStatus, ref string newsUrl, ref int zoneId);
        [OperationContract]
        WcfActionResponse DeleteTagNewsById(string tagName, long newsId);
        [OperationContract]
        WcfActionResponse UpdateTagNews(long newsId, string tagName);
        [OperationContract]
        WcfActionResponse UpdateViewCountByUrl(int viewCount, string url);
        [OperationContract]
        WcfActionResponse UpdateListViewCountByUrl(string value);

        #endregion

        #region AutoTag

        [OperationContract]
        TagAutoEntity[] GetListAutoTag();

        [OperationContract]
        WcfActionResponse UpdateAutoTagIsProcess(long newsId);

        [OperationContract]
        WcfActionResponse UpdateTagAutoByNewsId(long newsId, string tagIdList);
        [OperationContract]
        WcfActionResponse UpdateTagAutoForNews(long newsId, string tagAutoIdList);

        #endregion

        #region BoxTagEmbeb

        [OperationContract]
        BoxTagEmbedEntity[] GetListTagEmbed(int zoneId, int type);
        [OperationContract]
        WcfActionResponse UpdateTagEmbed(string listTagId, int zoneId, int type);

        [OperationContract]
        WcfActionResponse UpdateTagEmbedExternal(string listTagId, int zoneId, int type, string listTitle,
                                                 string listUrl);

        #endregion
        [OperationContract]
        List<QueueUpdateTagChannelEntity> GetUnProcess(int channelId);
        [OperationContract]
        WcfActionResponse UpdateIsProcess(long newsId);
        [OperationContract]
        WcfActionResponse RecieveFromCloud(TagEntity tag, int zoneId, string zoneIdList, ref long newTagId);
        [OperationContract]
        PhotoTagV2Entity GetPhotoTagByTagName(string name);
        [OperationContract]
        List<PhotoTagV2Entity> SearchTagPhotoByName(string name);
        [OperationContract]
        WcfActionResponse InsertPhotoTag(PhotoTagV2Entity tag);
    }
}
