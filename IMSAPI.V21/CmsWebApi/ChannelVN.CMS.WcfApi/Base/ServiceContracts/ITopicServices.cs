﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Topic;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITopicServices" in both code and config file together.
    [ServiceContract]
    public interface ITopicServices
    {
        #region Get

        [OperationContract]
        TopicDetailEntity GetTopicByTopicId(long TopicId);

        [OperationContract]
        TopicEntity[] SearchTopic(string keyword, int zoneId, int orderBy, int isHotTopic, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        List<TopicEntity> GetTopicByNewsId(long newsId);

        #endregion

        #region Update

        [OperationContract]
        WcfActionResponse Insert(TopicEntity Topic, int zoneId, ref long newTopicId);

        [OperationContract]
        WcfActionResponse Update(TopicEntity Topic, int zoneId);

        [OperationContract]
        WcfActionResponse DeleteById(long TopicId);

        [OperationContract]
        WcfActionResponse UpdateTopicHot(long TopicId, bool isHotTopic);

        [OperationContract]
        WcfActionResponse UpdateTopicNews(long TopicId, string deleteNewsId, string addNewsId);

        [OperationContract]
        WcfActionResponse UpdateTopicNewsRelated(long newsId, string relatedTopics);

        [OperationContract]
        WcfActionResponse UpdateTopicNewsHot(long topicId, string addNewsAvatar, string addNewsId);

        #endregion


    }
}
