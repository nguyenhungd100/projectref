﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.ServiceModel;
using ChannelVN.CMS.BO.Base.FileUpload;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.FileUpload;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.Base.PhotoGroup;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    interface IMediaServices : IErrorMessageMapping
    {
        #region Videos

        #region Video playlists

        #region Updates

        [OperationContract]
        WcfActionResponse SaveVideoPlaylist(PlaylistEntity playlistEntity);
        [OperationContract]
        WcfActionResponse RemoveVideoOutOfPlaylist(int playlistId, string videoIdList);
        [OperationContract]
        WcfActionResponse AddVideoIntoPlayList(int videoId, int playlistId, int priority);
        [OperationContract]
        WcfActionResponse AddVideoListIntoPlaylist(string videoIdList, string timeToPlayList, int playlistId, bool deletOldVideo);
        [OperationContract]
        WcfActionResponse DeleteVideoPlaylistById(int playlistId);
        [OperationContract]
        WcfActionResponse SendVideoFromYoutube(int id);
        [OperationContract]
        WcfActionResponse DeleteVideoPlaylistByIdList(string playlistIdList);
        [OperationContract]
        WcfActionResponse PublishPlaylist(int playlistId);
        [OperationContract]
        WcfActionResponse UnpublishPlaylist(int playlistId);
        [OperationContract]
        WcfActionResponse ChangeMode(int playlistId, EnumPlayListMode mode);
        [OperationContract]
        WcfActionResponse ExportPlaylistToProgramSchedule(int playlistId, int programChannelId,
                                                          string programScheduleNameFormat);

        #endregion

        #region Gets

        [OperationContract]
        PlaylistEntity GetVideoPlaylistById(int playlistId);
        [OperationContract]
        PlaylistEntity[] GetListVideoPlaylistByVideoId(int videoId);
        [OperationContract]
        PlaylistEntity[] GetListVideoPlaylistPaging(int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow);
        [OperationContract]
        PlaylistEntity[] GetMyPlaylist(string username, int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow);
        [OperationContract]
        PlaylistEntity[] SearchPlayList(string username, int zoneVideoId, EnumPlayListStatus status, string keyword, EnumPlayListMode mode, EnumPlayListSort sortOder, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        PlaylistCounterEntity[] GetPlaylistCounter();

        #endregion

        #endregion

        #region Video data

        #region Updates
        [OperationContract]
        WcfActionResponse UpdateConvertedMode(string listVideoId);
        [OperationContract]
        WcfActionResponse SaveVideo(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList);

        [OperationContract]
        WcfActionResponse SaveVideoV3(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList);
        [OperationContract]
        WcfActionResponse SaveVideoV4(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList);
        [OperationContract]
        WcfActionResponse SaveVideoNameAndDescription(VideoEntity videoEntity);
        [OperationContract]
        WcfActionResponse SaveVideoName(VideoEntity videoEntity);
        [OperationContract]
        WcfActionResponse SaveVideoV2(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList);
        [OperationContract]
        WcfActionResponse UpdateVideoMode(int mode, int id);
        [OperationContract]
        WcfActionResponse UpdateVideoViews(string keyVideo, int views);
        [OperationContract]
        WcfActionResponse UpdateListVideoViews(string listKeyVideo, string listViewVideo);
        [OperationContract]
        WcfActionResponse UpdateVideoAvatar(string avatar, int id);
        [OperationContract]
        WcfActionResponse AddVideoFileToVideo(VideoEntity videoEntity);
        [OperationContract]
        WcfActionResponse UpdateNewsIdForListVideoId(string idList, long newsId);
        [OperationContract]
        WcfActionResponse DeleteVideoById(int videoId);
        [OperationContract]
        WcfActionResponse SendVideo(int videoId);
        [OperationContract]
        WcfActionResponse ReturnVideo(int videoId);
        [OperationContract]
        WcfActionResponse PublishVideo(int videoId);
        [OperationContract]
        WcfActionResponse PublishVideoInNews(int videoId, DateTime distributionDate);
        [OperationContract]
        WcfActionResponse UnpublishVideo(int videoId);
        [OperationContract]
        WcfActionResponse DeleteVideoByIdList(string videoIdList);
        [OperationContract]
        WcfActionResponse ChangeVideoStatus(int videoId);
        [OperationContract]
        WcfActionResponse ChangeVideoStatusByList(string videoIdList);
        [OperationContract]
        WcfActionResponse SyncNewsAndVideo(string videoIdList, long newsId);

        [OperationContract]
        WcfActionResponse SyncNewsAndVideoWithStatus(string videoIdList, long newsId, EnumVideoStatus status,
                                                     int excludeStatus);
        [OperationContract]
        WcfActionResponse UpdateKeyVideoByFileName(string keyVideo, string fileName);
        #endregion

        #region Gets

        [OperationContract]
        List<VideoKeyEntity> GetListVideoWaitConvert(int top);

        [OperationContract]
        VideoEntity GetVideoByFileName(string fileName);
        [OperationContract]
        VideoEntity GetDetailVideo(int videoId);
        [OperationContract]
        VideoEntity GetDetailVideoByVideoExternalId(string videoExternalId);
        [OperationContract]
        VideoEntity GetDetailVideoByKeyvideo(string keyvideo);
        [OperationContract]
        VideoEntity GetDetailVideoV2(int videoId);
        [OperationContract]
        VideoDetailForEdit GetDetailVideoForEdit(int videoId);
        [OperationContract]
        VideoDetailForEdit GetDetailVideoForEditV2(int videoId);
        [OperationContract]
        VideoEntity[] GetListMyVideo(string username, int pageIndex, int pageSize, string keyword, int status, int sortOrder, int cateId, int playlistId, ref int totalRow);
        [OperationContract]
        VideoEntity[] GetListVideoByIdList(string videoIdList);
        [OperationContract]
        VideoEntity[] GetListVideoByCate(int pageIndex, int pageSize, int zoneId, int playlistId, int status, string keyword, int sortOrder, ref int totalRow);
        [OperationContract]
        VideoEntity[] GetListVideoInPlaylist(int pageIndex, int pageSize, int playlistId, int status, string keyword, int sortOder, ref int totalRow);
        [OperationContract]
        VideoEntity[] GetListAllVideoInPlaylist(int playlistId);
        [OperationContract]
        VideoEntity[] GetVideoInPlaylist(int playlistId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        VideoEntity[] SearchVideo(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<VideoDistributionEntity> SearchDistribution(string keyword, DateTime viewDate, int mode, string zoneIds,
                                                         int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        VideoEntity[] SearchVideoV2(string username, int videoFolderId, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        VideoEntity[] SearchVideoAd(string username, int zoneVideoId, int playlistId, string keyword,
                                    EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate,
                                    int mode, int pageIndex, int pageSize, bool allowAd, ref int totalRow);
        [OperationContract]
        VideoExportEntity[] SearchExportVideo(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        VideoEntity[] SearchVideoWithExcludeZones(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, string excludeZones, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        VideoInNewsEntity[] GetVideoInNewsList(int videoId);
        [OperationContract]
        VideoInfoForSync GetVideoInfoForSynchonize(int videoId);
        [OperationContract]
        VideoCounterEntity[] GetVideoCounter();
        [OperationContract]
        string[] GetLastPublishedForUpdateView(DateTime fromDate);
        [OperationContract]
        VideoExportEntity[] GetRoyalties(DateTime startDate, DateTime endDate, int pageIndex, int pageSize,
                                         ref int totalRow);
        [OperationContract]
        VideoExportEntity[] GetRoyaltiesByZone(DateTime startDate, DateTime endDate, int pageIndex, int pageSize,
                                               ref int totalRow, string zones);
        [OperationContract]
        List<VideoSharingEntity> GetListSharing(string keyword, string zoneIds, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        VideoEntity[] GetListVideoByHour(string zoneVideoIds, int Hour);
        #endregion

        #region ZoneVideo

        [OperationContract]
        ZoneVideoEntity[] GetVideoZoneListByParentId(int parentId, EnumZoneVideoStatus status);
        [OperationContract]
        ZoneVideoEntity[] GetVideoZoneListAsTreeview(int parentId, EnumZoneVideoStatus status, string prefix);
        [OperationContract]
        ZoneVideoEntity[] GetVideoZoneListRelation(int videoId);
        [OperationContract]
        ZoneVideoEntity GetVideoZoneByVideoZoneId(int videoZoneId);
        [OperationContract]
        ZoneVideoDetailEntity GetVideoZoneDetailByVideoZoneId(int videoZoneId);
        [OperationContract]
        WcfActionResponse InsertVideoZone(ZoneVideoEntity zoneVideo, string listVideoTagId, ref int zoneVideoId);
        [OperationContract]
        WcfActionResponse UpdateVideoZone(ZoneVideoEntity zoneVideo, string listVideoTagId);
        [OperationContract]
        WcfActionResponse MoveVideoZoneUp(int zoneVideoId);
        [OperationContract]
        WcfActionResponse MoveVideoZoneDown(int zoneVideoId);
        [OperationContract]
        WcfActionResponse DeleteVideoZone(int zoneVideoId);
        [OperationContract]
        WcfActionResponse UpdateAvatarZoneVideo(string zoneVideoAvatars);
        [OperationContract]
        WcfActionResponse GetZoneVideoIdByNewsZoneId(string newsZoneId, ref int zoneVideoId);

        #endregion

        #region VideoFromYoutube

        [OperationContract]
        WcfActionResponse InsertVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id);
        [OperationContract]
        WcfActionResponse UpdateVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList);
        [OperationContract]
        WcfActionResponse SaveVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id);
        [OperationContract]
        WcfActionResponse UpdateVideoFromYoutubeStatus(int id, string avatar, string filePath, EnumVideoFromYoutubeStatus status,
                                                       string htmlCode, string keyVideo, string pname, string fileName,
                                                       string duration, string size, int capacity);
        [OperationContract]
        WcfActionResponse DeleteVideoFromYoutube(int id);
        [OperationContract]
        WcfActionResponse PublishVideoFromYoutube(int id);
        [OperationContract]
        VideoFromYoutubeEntity GetVideoFromYoutubeById(int id);
        [OperationContract]
        VideoFromYoutubeForEditEntity GetVideoFromYoutubeForEdit(int id);
        [OperationContract]
        VideoFromYoutubeEntity[] GetListVideoFromYoutube(string accountName, int status, int order, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<VideoFromYoutubeDetailEntity> SearchVideoFromYoutube(string username, string keyword,
                                                                  EnumVideoFromYoutubeStatus status, int zoneVideoId,
                                                                  int playlistId, DateTime fromDate, DateTime toDate,
                                                                  int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<VideoFromYoutubeEntity> GetUploadingVideoFromYoutube();

        #endregion

        #region BoxVideoEmbed
        [OperationContract]
        BoxVideoEmbedEntity[] GetListVideoEmbed(int zoneId, int type);
        [OperationContract]
        WcfActionResponse InsertVideoEmbed(BoxVideoEmbedEntity videoEmbedBox);
        [OperationContract]
        WcfActionResponse UpdateVideoEmbed(string listVideoId, int zoneId, int type);
        [OperationContract]
        WcfActionResponse UpdateVideoEmbed_Epl(List<BoxVideoEmbedEplEntity> listEmbed);

        [OperationContract]
        WcfActionResponse DeleteVideoEmbed(long videoId, int zoneId, int type);

        #endregion

        #region VideoEPL
        [OperationContract]
        VideoEPLEntity[] VideoEPLByZoneId(int zoneId, DateTime dateFrom, DateTime dateTo);

        [OperationContract]
        VideoEPLZoneToZoneEntity[] VideoEPLZoneAndZone(int zoneId1, int zoneId2, DateTime dateFrom, DateTime dateTo);
        #endregion

        #region BoxVideoEmbedByNewsZone
        [OperationContract]
        BoxVideoEmbedByNewsZoneEntity[] GetListVideoEmbedByNewsZone(int zoneId, int type);
        [OperationContract]
        WcfActionResponse InsertVideoEmbedByNewsZone(BoxVideoEmbedByNewsZoneEntity videoEmbedBox);
        [OperationContract]
        WcfActionResponse UpdateVideoEmbedByNewsZone(string listVideoId, int zoneId, int type);
        [OperationContract]
        WcfActionResponse DeleteVideoEmbedByNewsZone(long videoId, int zoneId, int type);

        #endregion

        #endregion

        #region Video Thread
        [OperationContract]
        List<VideoThreadEntity> SearchVideoThread(string keyword, bool isHotThread, EnumVideoThreadStatus status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        VideoThreadDetailEntity GetVideoThreadById(int id);
        [OperationContract]
        WcfActionResponse InsertVideoThread(VideoThreadEntity videoThread, string videoIdList, ref int videoThreadId);
        [OperationContract]
        WcfActionResponse UpdateVideoThread(VideoThreadEntity videoThread, string videoIdList);
        [OperationContract]
        WcfActionResponse DeleteVideoThread(int videoThreadId);
        [OperationContract]
        WcfActionResponse UpdateVideoInVideoThread(int videoThreadId, string videoIdList);
        #endregion

        #region Video tag

        [OperationContract]
        List<VideoTagEntity> SearchVideoTag(string keyword, int parentId, EnumVideoTagMode tagMode, EnumVideoTagStatus status, EnumVideoTagSort orderBy, bool getTagHasVideoOnly, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<VideoTagEntity> GetVideoTagByZoneVideoId(int zoneVideoId);
        [OperationContract]
        VideoTagEntity GetVideoTagById(int id);
        [OperationContract]
        WcfActionResponse InsertVideoTag(VideoTagEntity videoTag, ref int videoTagId);
        [OperationContract]
        WcfActionResponse UpdateVideoTagMode(int videoTagId, bool isHotTag);
        [OperationContract]
        WcfActionResponse UpdateVideoTag(VideoTagEntity videoTag);
        [OperationContract]
        WcfActionResponse UpdateVideoTagWithTagName(string videoTagName, ref int videoTagId);
        [OperationContract]
        VideoTagEntity GetVideoTagByName(string name);

        #endregion


        #region Video Folder

        [OperationContract]
        WcfActionResponse InsertVideoFolder(VideoFolderEntity videoFolder);
        [OperationContract]
        WcfActionResponse UpdateVideoFolder(VideoFolderEntity videoFolder);
        [OperationContract]
        WcfActionResponse DeleteVideoFolder(int videoFolderId);
        [OperationContract]
        VideoFolderEntity GetVideoFolderByVideoFolderId(int videoFolderId);
        [OperationContract]
        VideoFolderEntity[] GetVideoFolderByParentId(int parentId, string createdBy);
        [OperationContract]
        VideoFolderEntity[] VideoFolderSearch(int parentId, string name, string createdBy, int status);

        [OperationContract]
        List<VideoFolderEntity> GetVideoFolderWithTreeView(string createBy);

        #endregion

        #endregion

        #region Photos

        #region Photo

        [OperationContract]
        WcfActionResponse InsertPhoto(PhotoEntity photo, string tagIdList);
        [OperationContract]
        WcfActionResponse InsertPhotoV2(PhotoEntity photo, string tagIdList);
        [OperationContract]
        WcfActionResponse UpdatePhoto(PhotoEntity photo, string tagIdList);
        [OperationContract]
        WcfActionResponse UpdatePhotoV2(PhotoEntity photo, string tagIdList);
        [OperationContract]
        WcfActionResponse DeletePhoto(long photoId);
        [OperationContract]
        PhotoPublishedEntity[] PublishPhoto(string listPhotoId, int albumId, long newsId, int zoneId, string distributedBy);
        [OperationContract]
        PhotoEntity GetPhotoByPhotoId(long photoId);
        [OperationContract]
        PhotoEntity GetPhotoByPhotoIdV2(long photoId);
        [OperationContract]
        PhotoEntity[] SearchPhoto(string keyword, int photoLabelId, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        PhotoEntity[] SearchPhotoV2(string keyword, int photoFolderId, int photoLabelId, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        PhotoEntity GetPhotoByImageUrl(string imageUrl);
        #endregion

        #region Photo published

        [OperationContract]
        WcfActionResponse UpdatePhotoPublished(PhotoPublishedEntity photoPublished);
        [OperationContract]
        WcfActionResponse UpdatePhotoPublishedAlbumId(PhotoPublishedUpdateEntity photoPublished);
        [OperationContract]
        WcfActionResponse UpdatePhotoPublishedInUsed(string listPhotoPublishedIdInUsed, long newsId, int albumId, string createdBy);
        [OperationContract]
        WcfActionResponse UpdatePhotoPublishedPriority(string listPhotoPublishedId);
        [OperationContract]
        PhotoPublishedEntity GetPhotoPublishedByPhotoPublishedId(long photoPublishedId);
        [OperationContract]
        PhotoPublishedEntity[] GetPhotoPublishedByPhotoId(long photoId);
        [OperationContract]
        WcfActionResponse DeleteListPhotoPublished(string listPhotoPublishedId);
        [OperationContract]
        PhotoPublishedEntity[] SearchPhotoPublishedInAlbum(string keyword, int albumId, int pageIndex, int pageSize, ref int totalRow);

        #endregion

        #region Album

        [OperationContract]
        WcfActionResponse InsertAlbum(AlbumEntity album, string tagIdList);
        [OperationContract]
        WcfActionResponse UpdateAlbum(AlbumEntity album, string tagIdList);
        [OperationContract]
        WcfActionResponse DeleteAlbum(int albumId);
        [OperationContract]
        AlbumPublishedEntity[] PublishAlbum(string listAlbumId, long newsId, int zoneId, string distributedBy);
        [OperationContract]
        AlbumEntity GetAlbumByAlbumId(int albumId);
        [OperationContract]
        AlbumEntity[] SearchAlbum(string keyword, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow);

        #endregion

        #region Album published

        [OperationContract]
        WcfActionResponse UpdateAlbumPublished(AlbumPublishedEntity albumPublished);
        [OperationContract]
        WcfActionResponse UpdateAlbumPublishedInUsed(string listAlbumPublishedIdInUsed, long newsId, string createdBy);
        [OperationContract]
        AlbumPublishedEntity GetAlbumPublishedByAlbumPublishedId(long albumPublishedId);
        [OperationContract]
        AlbumPublishedEntity[] GetAlbumPublishedByAlbumId(long albumId);

        #endregion

        #region Photo label

        [OperationContract]
        WcfActionResponse InsertPhotoLabel(PhotoLabelEntity photoLabel);
        [OperationContract]
        WcfActionResponse UpdatePhotoLabel(PhotoLabelEntity photoLabel);
        [OperationContract]
        WcfActionResponse DeletePhotoLabel(int photoLabelId);
        [OperationContract]
        PhotoLabelEntity GetPhotoLabelByPhotoLabelId(int photoLabelId);
        [OperationContract]
        PhotoLabelEntity[] GetPhotoLabelByParentLabelId(int parentLabelId, string createdBy);

        #endregion

        #region Photo Folder

        [OperationContract]
        WcfActionResponse InsertPhotoFolder(PhotoFolderEntity photoFolder);
        [OperationContract]
        WcfActionResponse UpdatePhotoFolder(PhotoFolderEntity photoFolder);
        [OperationContract]
        WcfActionResponse DeletePhotoFolder(int photoFolderId);
        [OperationContract]
        PhotoFolderEntity GetPhotoFolderByPhotoFolderId(int photoFolderId);
        [OperationContract]
        PhotoFolderEntity[] GetPhotoFolderByParentId(int parentId, string createdBy);
        [OperationContract]
        PhotoFolderEntity[] PhotoFolderSearch(int parentId, string name, string createdBy, int status);

        [OperationContract]
        List<PhotoFolderEntity> GetFolderWithTreeView(string createBy);

        #endregion

        #endregion

        #region PhotoGroupDetail

        [OperationContract]
        WcfActionResponse InsertPhotoGroupDetail(PhotoGroupDetailEntity photo);
        [OperationContract]
        WcfActionResponse UpdatePhotoGroupDetail(PhotoGroupDetailEntity photo);
        [OperationContract]
        WcfActionResponse UpdatePhotoGroupDetailStatus(int id, EnumPhotoGroupDetailStatus status);
        [OperationContract]
        WcfActionResponse DeletePhotoGroupDetail(int photoId);
        [OperationContract]
        PhotoGroupDetailEntity GetPhotoGroupDetail(int photoId);
        [OperationContract]
        PhotoGroupDetailEntity[] SearchPhotoGroupDetail(string keyword, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, EnumPhotoGroupDetailStatus status, int isHot, int photoGroupId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        PhotoGroupDetailCountEntity[] CountPhotoGroupDetail(string username);
        [OperationContract]
        PhotoGroupEntity[] GetAllPhotoGroup(ref int totalRow);
        [OperationContract]
        WcfActionResponse InsertPhotoGroupDetailInPhotoTag(PhotoGroupDetailInPhotoTagEntity photo);
        [OperationContract]
        WcfActionResponse DeletePhotoGroupDetailInPhotoTag(int photoId);

        #endregion

        #region EmbedAlbum

        [OperationContract]
        List<EmbedAlbumEntity> SearchEmbedAlbum(string keyword, int type, int zoneId, int status, int pageIndex,
                                                int pageSize, ref int totalRow);

        [OperationContract]
        EmbedAlbumForEditEntity GetEmbedAlbumForEditByEmbedAlbumId(int embedAlbumId, int topPhoto);

        [OperationContract]
        WcfActionResponse CreateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit);

        [OperationContract]
        WcfActionResponse CreateEmbedAlbumReturnId(EmbedAlbumForEditEntity embedAlbumForEdit, ref int id);

        [OperationContract]
        WcfActionResponse UpdateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit);

        [OperationContract]
        WcfActionResponse DeleteEmbedAlbum(int embedAlbumId);

        #endregion

        #region FileUpload

        [OperationContract]
        WcfActionResponse InsertFileUpload(FileUploadEntity fileUpload);

        [OperationContract]
        WcfActionResponse UpdateFileUpload(FileUploadEntity fileUpload);

        [OperationContract]
        WcfActionResponse DeleteFileUpload(int fileUploadId);

        [OperationContract]
        FileUploadEntity GetFileUpload(int fileUploadId);

        [OperationContract]
        FileUploadEntity[] SearchFileUpload(string keyword, int zoneId, string ext, string uploadedBy, EnumFileUploadStatus status,
                                            int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        FileUploadExtEntity[] GetAllExt();

        #endregion
        [OperationContract]
        VideoEntity[] SearchVideoFromExternalChannel(string username, int zoneVideoId, int playlistId, string keyword,
                                                     EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate,
                                                     DateTime toDate, int mode, int pageIndex, int pageSize,
                                                     ref int totalRow);
        [OperationContract]
        ZoneVideoEntity[] GetVideoZoneListAsTreeviewFromExternalChannel(int parentId, EnumZoneVideoStatus status,
                                                                        string prefix);

        [OperationContract]
        WcfActionResponse UpdateVideoInNews(string videoInNews, long newsId);

        [OperationContract]
        VideoEntity[] SearchVideoExcludeBomb(string username, int zoneVideoId, int playlistId, string keyword,
                                             EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate,
                                             DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow);


        [OperationContract]
        List<PhotoGroupDetailInZonePhotoEntity> GetZonePhotoByPhotoId(int id);

        #region VideoExtension
        [OperationContract]
        VideoExtensionEntity GetVideoExtensionValue(long videoId, EnumVideoExtensionType type);
        [OperationContract]
        int GetVideoExtensionMaxValue(long videoId, EnumVideoExtensionType type);
        [OperationContract]
        WcfResponseData SetVideoExtensionValue(long videoId, EnumVideoExtensionType type, string value);
        [OperationContract]
        WcfResponseData DeleteVideoExtensionByVideoId(long videoId);
        [OperationContract]
        List<VideoExtensionEntity> GetVideoExtensionByVideoId(long videoId);
        [OperationContract]
        List<VideoExtensionEntity> GetVideoExtensionByListVideoId(string listVideoId);
        [OperationContract]
        List<VideoExtensionEntity> GetVideoExtensionByTypeAndVaue(EnumVideoExtensionType type, string value);

        #endregion

        [OperationContract]
        BoxVideoEmbedEntity[] GetExternalListVideoEmbed(int zoneId, int type);

        [OperationContract]
        VideoEntity GetDetailExternalVideo(int videoId);

        #region BoxProgramEmbed

        [OperationContract]
        ZoneVideoEntity[] SearchProgram(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        BoxVideoProgramEmbedEntity[] GetListProgramEmbed(int zoneId, int type);

        [OperationContract]
        WcfActionResponse UpdateProgramEmbed(string listVideoId, int zoneId, int type);

        #endregion

        #region Video Distribution
        [OperationContract]
        VideoEntity[] SearchVideoDistribution(string zoneVideoIds, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        VideoEntity GetDetailVideoDistribution(int videoId);
        #endregion


        [OperationContract]
        List<ZoneVideoEntity> GetActiveZoneInDay(string zoneIds, DateTime viewDate, int mode);
        [OperationContract]
        WcfActionResponse SaveVideoWithoutPermission(VideoEntity videoEntity, string tagIds, string zoneIdList,
                                                      string playlistIdList);
        [OperationContract]
        List<VideoModeNotifyEntity> GetNotifyZoneInDay(DateTime viewDate);
        [OperationContract]
        List<VideoDistributionEntity> SearchDistributionV2(string keyword, DateTime viewDate, int mode, string zoneIds,
                                                           int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<ZoneVideoEntity> GetActiveZoneInDayV2(string zoneIds, DateTime viewDate, int mode);
        [OperationContract]
        WcfActionResponse SaveVideoPlaylistV2(PlaylistEntity playlistEntity);
        [OperationContract]
        List<PlaylistWithZoneEntity> GetLastDaysHavePlaylistWithZone(DateTime fromDate, DateTime toDate, int group,
                                                                     int team, int mode);
        [OperationContract]
        List<PlaylistInZoneEntity> GetZoneByPlaylist(int playlistId);
        [OperationContract]
        PlaylistGroupEntity[] GetPlaylistGroupAsTreeview(int parentId, string prefix);
        [OperationContract]
        PlaylistGroupEntity GetPlaylistGroupByPlaylistId(int playlistId);
        [OperationContract]
        WcfActionResponse InsertPlaylistInVideoGroup(PlaylistInPlaylistGroupEntity playlistGroup);
        [OperationContract]
        List<PlaylistGroupEntity> GetPlaylistGroupByParentId(int playlistId);
        [OperationContract]
        List<VideoEntity> SearchByPlaylistGroup(int group, int zone);
        [OperationContract]
        List<PlaylistWithZoneEntity> GetTopDayHaveVideo(int top);
        [OperationContract]
        VideoEntity[] GetListAllVideoInPlaylistByMode(int playlistId, int mode);
        [OperationContract]
        VideoEntity[] SearchVideoForList(string username, int zoneVideoId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        List<EPLPlayListEntity> EPLPlayListGetByUser(string userName);

        [OperationContract]
        WcfActionResponse InsertVideoToEPLPlaylist(string userName, int videoId);

        [OperationContract]
        WcfActionResponse DeleteVideoFromEPLPlaylist(int userId, int videoId);

        #region Video EPL
        [OperationContract]
        WcfActionResponse Video_Insert_By_Feed(VideoEntity videoEntity, string tagIds, int MapPlayListTime, string zoneList);

        [OperationContract]
        WcfActionResponse Video_Insesert_ZoneRecut(int videoId);
        [OperationContract]
        WcfActionResponse Video_UpdateZonePrimary(int videoId, int ZoneId);

        [OperationContract]
        WcfActionResponse Video_InsertZone(int videoId, string ZoneIds);

        [OperationContract]
        WcfActionResponse Playlist_GetIdByZoneId(int zoneId1, int zoneId2, DateTime DateFrom, DateTime DateTo, int VideoId, ref int PlayListId);
        [OperationContract]
        List<ZoneVideoEntity> ZoneVideo_GetAll();

        [OperationContract]
        List<EPLPlayListEntity> EPLPlayListGetAll();

        [OperationContract]
        WcfActionResponse Playlist_Update_DistributionDate(string playlistIds);

        [OperationContract]
        PlaylistGroupEntity PlaylistGroup_Current(DateTime fromDate, DateTime toDate);
        #endregion
    }
}
