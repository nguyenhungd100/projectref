﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.Base.ServiceContracts
{
    [ServiceContract]
    public interface IRollingNewsServices : IErrorMessageMapping
    {
        #region RollingNews

        [OperationContract]
        List<RollingNewsEntity> SearchRollingNews(string keyword, EnumRollingNewsStatus status, int pageIndex, int pageSize,
                                                  ref int totalRow);
        [OperationContract]
        List<RollingNewsForSuggestionEntity> SearchRollingNewsForSuggestion(string keyword,
                                                                            EnumRollingNewsStatus status,
                                                                            int top);
        [OperationContract]
        RollingNewsEntity GetRollingNewsByRollingNewsId(int rollingNewsId);
        [OperationContract]
        WcfActionResponse InsertRollingNews(RollingNewsEntity rollingNews);

        [OperationContract]
        WcfActionResponse InsertRollingNewsV2(RollingNewsEntity rollingNews, ref int Id);

        [OperationContract]
        WcfActionResponse UpdateRollingNews(RollingNewsEntity rollingNews);
        [OperationContract]
        WcfActionResponse DeleteRollingNews(int rollingNewsId);
        [OperationContract]
        WcfActionResponse ChangeStatusRollingNews(int rollingNewsId, EnumRollingNewsStatus status);

        #endregion

        #region RollingNewsAuthor

        [OperationContract]
        List<RollingNewsAuthorEntity> GetRollingNewsAuthorByRollingNewsId(int rollingNewsId);
        [OperationContract]
        RollingNewsAuthorEntity GetRollingNewsAuthorByRollingNewsAuthorId(int rollingNewsAuthorId);
        [OperationContract]
        WcfActionResponse InsertRollingNewsAuthor(RollingNewsAuthorEntity rollingNewsAuthor);
        [OperationContract]
        WcfActionResponse UpdateRollingNewsAuthor(RollingNewsAuthorEntity rollingNewsAuthor);
        [OperationContract]
        WcfActionResponse DeleteRollingNewsAuthor(int rollingNewsAuthorId);

        #endregion

        #region RollingNewsEvent

        [OperationContract]
        List<RollingNewsEventEntity> GetRollingNewsEventByRollingNewsId(int rollingNewsId, EnumRollingNewsEventType eventType, EnumRollingNewsEventStatus status);
        [OperationContract]
        RollingNewsEventEntity GetRollingNewsEventByRollingNewsEventId(int rollingNewsEventId);
        [OperationContract]
        WcfActionResponse InsertRollingNewsEvent(RollingNewsEventEntity rollingNewsEvent);
        [OperationContract]
        WcfActionResponse UpdateRollingNewsEvent(RollingNewsEventEntity rollingNewsEvent);

        [OperationContract]
        WcfActionResponse UpdateRollingNewsEventV2(List<RollingNewsEventEntity> rollingNewsEvent, int rollingNewsId);

        [OperationContract]
        WcfActionResponse DeleteRollingNewsEvent(int rollingNewsEventId);
        [OperationContract]
        WcfActionResponse PublishRollingNewsEvent(int rollingNewsEventId);
        [OperationContract]
        WcfActionResponse UnpublishRollingNewsEvent(int rollingNewsEventId);
        [OperationContract]
        WcfActionResponse UpdateIsFocusForRollingNewsEvent(int rollingNewsEventId, bool isFocus);
        [OperationContract]
        WcfActionResponse RefreshRollingNewsEvent(int rollingNewsEventId);

        #endregion
    }
}
