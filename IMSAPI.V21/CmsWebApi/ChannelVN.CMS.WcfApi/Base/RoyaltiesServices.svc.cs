﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.Royalties;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RoyaltiesServices : IRoyaltiesServices
    {
        #region RoyaltiesCategory

        #region Get

        public RoyaltiesCategoryEntity GetRoyaltiesCategoryById(int id)
        {
            return RoyaltiesCategoryBo.GetRoyaltiesCategoryById(id);
        }

        public List<RoyaltiesCategoryEntity> GetAllRoyaltiesCategory()
        {
            return RoyaltiesCategoryBo.GetAllRoyaltiesCategory();
        }

        public List<RoyaltiesCategoryEntity> GetByRoyaltiesCategoryType(int zoneId, EnumRoyaltiesCategoryType type)
        {
            return RoyaltiesCategoryBo.GetByRoyaltiesCategoryType(zoneId, type);
        }

        #endregion

        #region Set

        public WcfActionResponse InsertRoyaltiesCategory(RoyaltiesCategoryEntity royaltiesCategory)
        {
            var errorCode = RoyaltiesCategoryBo.InsertRoyaltiesCategory(royaltiesCategory);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int) errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateRoyaltiesCategory(RoyaltiesCategoryEntity royaltiesCategory)
        {
            var errorCode = RoyaltiesCategoryBo.UpdateRoyaltiesCategory(royaltiesCategory);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteRoyaltiesCategory(int royaltiesCategoryId)
        {
            var errorCode = RoyaltiesCategoryBo.DeleteRoyaltiesCategory(royaltiesCategoryId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateRoyaltiesCategoryForClipAndPhoto(int zoneId)
        {
            var errorCode = RoyaltiesCategoryBo.UpdateRoyaltiesCategoryForClipAndPhoto(zoneId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]); 
        }

        #endregion

        #endregion

        #region RoyaltiesRole

        #region Get

        public RoyaltiesRoleEntity GetRoyaltiesRoleById(int id)
        {
            return RoyaltiesRoleBo.GetRoyaltiesRoleById(id);
        }

        public List<RoyaltiesRoleEntity> GetAllRoyaltiesRole()
        {
            return RoyaltiesRoleBo.GetAllRoyaltiesRole();
        }

        #endregion

        #region Set

        public WcfActionResponse InsertRoyaltiesRole(RoyaltiesRoleEntity royaltiesRole)
        {
            var errorCode = RoyaltiesRoleBo.InsertRoyaltiesRole(royaltiesRole);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateRoyaltiesRole(RoyaltiesRoleEntity royaltiesRole)
        {
            var errorCode = RoyaltiesRoleBo.UpdateRoyaltiesRole(royaltiesRole);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteRoyaltiesRole(int royaltiesRoleId)
        {
            var errorCode = RoyaltiesRoleBo.DeleteRoyaltiesRole(royaltiesRoleId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #endregion

        #region RoyaltiesMember

        #region Get

        public RoyaltiesMemberEntity GetRoyaltiesMemberById(int id)
        {
            return RoyaltiesMemberBo.GetRoyaltiesMemberById(id);
        }

        public List<RoyaltiesMemberEntity> SearchRoyaltiesMember(int royaltiesRoleId, string keyword, EnumRoyaltiesMemberStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return RoyaltiesMemberBo.SearchRoyaltiesMember(royaltiesRoleId, keyword, status, pageIndex, pageSize, ref totalRow);
        }

        #endregion

        #region Set

        public WcfActionResponse InsertRoyaltiesMember(RoyaltiesMemberEntity royaltiesMember)
        {
            var errorCode = RoyaltiesMemberBo.InsertRoyaltiesMember(royaltiesMember);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateRoyaltiesMember(RoyaltiesMemberEntity royaltiesMember)
        {
            var errorCode = RoyaltiesMemberBo.UpdateRoyaltiesMember(royaltiesMember);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteRoyaltiesMember(int royaltiesMemberId)
        {
            var errorCode = RoyaltiesMemberBo.DeleteRoyaltiesMember(royaltiesMemberId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #endregion

        #region RoyaltiesRule

        #region Get

        public List<RoyaltiesRuleEntity> GetRoyaltiesRuleByZoneId(int zoneId)
        {
            return RoyaltiesRuleBo.GetRoyaltiesRuleByZoneId(zoneId);
        }

        public RoyaltiesRuleEntity GetByRoyaltiesCategoryIdAndRoyaltiesRate(bool isManager,
                                                                                   int royaltiesCategoryId,
                                                                                   int royaltiesRate)
        {
            return RoyaltiesRuleBo.GetByRoyaltiesCategoryIdAndRoyaltiesRate(isManager,
                                                                            royaltiesCategoryId,
                                                                            royaltiesRate);
        }

        #endregion

        #region Set

        public WcfActionResponse UpdateRoyaltiesRuleValues(List<RoyaltiesRuleEntity> listRoyaltiesRule)
        {
            var errorCode = RoyaltiesRuleBo.UpdateRoyaltiesRuleValues(listRoyaltiesRule);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #endregion

        #region Royalties

        #region Get

        public RoyaltiesEntity GetRoyaltiesById(long id)
        {
            return RoyaltiesBo.GetRoyaltiesById(id);
        }

        public List<RoyaltiesEntity> SearchRoyalties(EnumRoyaltiesCategoryType type, int zoneId, int royaltiesMemberId, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return RoyaltiesBo.SearchRoyalties(type, zoneId, royaltiesMemberId, royaltiesCategoryId, royaltiesRoleId, keyword,
                                       royaltiesRate, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public List<RoyaltiesGroupByUserEntity> SearchRoyaltiesGroupByUser(EnumRoyaltiesCategoryType type,
                                                                           int zoneId, int royaltiesMemberId,
                                                                           int royaltiesCategoryId,
                                                                           int royaltiesRoleId, string keyword,
                                                                           int royaltiesRate, DateTime fromDate,
                                                                           DateTime toDate)
        {
            return RoyaltiesBo.SearchRoyaltiesGroupByUser(type, zoneId, royaltiesMemberId, royaltiesCategoryId,
                                                          royaltiesRoleId, keyword,
                                                          royaltiesRate, fromDate, toDate);
        }

        public List<RoyaltiesHistoryEntity> GetRoyaltiesHistoryByRoyaltiesId(long royaltiesId)
        {
            return RoyaltiesBo.GetRoyaltiesHistoryByRoyaltiesId(royaltiesId);
        }

        public List<RoyaltiesEntity> GetRoyaltiesByNewsId(long newsId)
        {
            return RoyaltiesBo.GetRoyaltiesByNewsId(newsId);
        }

        #endregion

        #region Set

        public WcfActionResponse InsertRoyalties(RoyaltiesEntity royalties)
        {
            var errorCode = RoyaltiesBo.InsertRoyalties(royalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateRoyalties(RoyaltiesEntity royalties)
        {
            var errorCode = RoyaltiesBo.UpdateRoyalties(royalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteRoyalties(int royaltiesId)
        {
            var errorCode = RoyaltiesBo.DeleteRoyalties(royaltiesId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateListRoyalties(long newsId, List<RoyaltiesEntity> listRoyalties)
        {
            var errorCode = RoyaltiesBo.UpdateListRoyalties(newsId, listRoyalties);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #endregion

        public WcfActionResponse UpdateRoyaltiesQuota(string lstId, string lstPost, string lstView)
        {
            var errorCode = RoyaltiesQuotaBo.UpdateQuota(lstId, lstPost, lstView);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public List<RoyaltiesQuotaEntity> SearchUser(string keyword, UserStatus status, UserSortExpression sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                UserStatus userStatus;
                if (!Enum.TryParse(status.ToString(), out userStatus))
                {
                    userStatus = UserStatus.Unknow;
                }
                UserSortExpression sortBy;
                if (!Enum.TryParse(sortOrder.ToString(), out sortBy))
                {
                    sortBy = UserSortExpression.CreateDateDesc;
                }
                return RoyaltiesQuotaBo.Search(keyword, userStatus, sortBy, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<RoyaltiesQuotaEntity>();
            }
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
