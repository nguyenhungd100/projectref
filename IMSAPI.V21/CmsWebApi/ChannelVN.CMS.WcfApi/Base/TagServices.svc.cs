﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.Photo;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TagServices : ITagServices
    {
        #region Tag
        public TagEntity[] SearchAllByKeyword(string keyword, int zoneId, bool isThread)
        {
            return BoFactory.GetInstance<TagBo>().SearchAllByKeyword(keyword, zoneId, isThread).ToArray();
        }
        public TagScoreEntity[] ListTagScore(long TagId)
        {
            return BoFactory.GetInstance<TagScoreBo>().ListTagScore(TagId).ToArray();
        }
        public TagEntity[] SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, EnumSearchTagOrder orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            return BoFactory.GetInstance<TagBo>().SearchTag(keyword, parentTagId, isThread, zoneId, type, orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly).ToArray();
        }
        public TagEntity[] Tag_SearchInBoxEmbed(string keyword,int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            return TagBo.Tag_SearchInBoxEmbed(keyword, isHotTag, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public TagEntity[] Tag_SearchInBoxEmbed_Approved(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            return TagBo.Tag_SearchInBoxEmbed_Approved(keyword, isHotTag, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public virtual TagWithSimpleFieldEntity[] SearchTagForSuggestion(int top, int zoneId, string keyword, int isHot, EnumSearchTagOrder orderBy, bool getTagHasNewsOnly)
        {
            return BoFactory.GetInstance<TagBo>().SearchTagForSuggestion(top, zoneId, keyword, isHot, orderBy, getTagHasNewsOnly).ToArray();
        }

        public TagEntityDetail GetTagByTagId(long tagId)
        {
            return BoFactory.GetInstance<TagBo>().GetTagByTagId(tagId);
        }
        public TagEntity[] GetTagByListOfTagId(string listOfTagId)
        {
            return BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(listOfTagId).ToArray();
        }
        public TagEntity[] GetTagByParentTagId(long parentTagId)
        {
            return BoFactory.GetInstance<TagBo>().GetTagByParentTagId(parentTagId).ToArray();
        }
        public TagEntity GetTagByTagName(string name, bool getTagHasNewsOnly = false)
        {
            return BoFactory.GetInstance<TagBo>().GetTagByTagName(name, getTagHasNewsOnly);
        }
        public WcfActionResponse DeleteTagScore(long TagId)
        {
            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<TagScoreBo>().DeleteTagScore(TagId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(TagId), "1");
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }
        public WcfActionResponse InsertTagScore(TagScoreEntity tagscore)
        {
            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<TagScoreBo>().InsertTagScore(tagscore);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tagscore), "1");
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }
        public WcfActionResponse InsertTag(TagEntity tag, int zoneId, string zoneIdList)
        {
            WcfActionResponse responseData;
            long newTagId = 0;
            var errorCode = BoFactory.GetInstance<TagBo>().InsertTag(tag, zoneId, zoneIdList, ref newTagId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                tag.Id = newTagId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
            }
            else
            {
                if (newTagId > 0)
                {
                    tag.Id = newTagId;
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            return responseData;
        }
        public long InsertNewTag(TagEntity tag, ref long newTagId)
        {
            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<TagBo>().InsertTag(tag, ref newTagId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                tag.Id = newTagId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
            }
            else
            {
                if (newTagId > 0)
                {
                    tag.Id = newTagId;
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            return newTagId;
        }
        public WcfActionResponse UpdateTagList(string[] tagNames, int zoneId, string zoneIdList)
        {
            WcfActionResponse responseData = WcfActionResponse.CreateSuccessResponse();
            try
            {
                var tags = BoFactory.GetInstance<TagBo>().UpdateTagList(tagNames, zoneId, zoneIdList);
                responseData.Data = tags;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public WcfActionResponse Update_RelationTag(long tagId, string tagRelations)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<TagBo>().Update_RelationTag(tagId, tagRelations);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse Update(TagEntity tag, int zoneId, string zoneIdList)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<TagBo>().Update(tag, zoneId, zoneIdList);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateViewCountByUrl(int viewCount, string url)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<TagBo>().UpdateViewCountByUrl(viewCount, url);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateListViewCountByUrl(string value)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<TagBo>().UpdateListViewCountByUrl(value);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse DeleteById(long tagId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<TagBo>().DeleteById(tagId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdatePriority(long tagId, long priority)
        {
            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<TagBo>().UpdatePriority(tagId, priority);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateTagHot(long tagId, bool isHotTag)
        {
            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<TagBo>().UpdateTagHot(tagId, isHotTag);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }
        public WcfActionResponse AddNews(string newsIds, long tagId, int tagMode)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = BoFactory.GetInstance<TagBo>().AddNews(newsIds, tagId, tagMode);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public WcfActionResponse UpdateNewsInTagNews(long tagId, string deleteNewsId, string addNewsId, int tagMode)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = BoFactory.GetInstance<TagBo>().UpdateNewsInTagNews(tagId, deleteNewsId, addNewsId,
                                                                                   tagMode);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public List<TagWithSimpleFieldEntity> GetTagByListOfTagName(List<string> listTagName)
        {
            return BoFactory.GetInstance<TagBo>().GetTagByListOfTagName(listTagName);
        }
        public List<TagWithSimpleFieldEntity> GetCleanTagByTagWordCount(int wordCount)
        {
            return BoFactory.GetInstance<TagBo>().GetCleanTagByTagWordCount(wordCount);
        }
        public List<TagWithSimpleFieldEntity> GetCleanTagStartByKeyword(string startByKeyword, int minWordCount, int maxWordCount)
        {
            return BoFactory.GetInstance<TagBo>().GetCleanTagStartByKeyword(startByKeyword, minWordCount, maxWordCount);
        }
        public GateGameTagEntity GetGateGameTagByTagId(long tagId)
        {
            return BoFactory.GetInstance<GateGameTagBo>().GetGateGameTagByTagId(tagId);
        }
        public WcfActionResponse InsertGateGameTag(GateGameTagEntity gateGameTagEntity)
        {
            WcfActionResponse responseData;
            int gameTagId = 0;
            var errorCode = BoFactory.GetInstance<GateGameTagBo>().Insert(gateGameTagEntity, ref gameTagId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                gateGameTagEntity.GateGameId = gameTagId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(gateGameTagEntity), "1");
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }
        /// <summary>
        /// Lấy dạm sách tag theo bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public List<TagEntity> GetListTagByNewsId(long newsId)
        {
            return BoFactory.GetInstance<TagBo>().GetListTagByNewsId(newsId);
        }
        /// <summary>
        /// Lấy danh sách tag theo bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public List<TagWithSimpleFieldEntity> GetListTagAndNewsInfoByNewsId(long newsId, ref int newsStatus, ref string newsUrl, ref int zoneId)
        {
            return BoFactory.GetInstance<TagBo>().GetListTagAndNewsInfoByNewsId(newsId, ref newsStatus, ref newsUrl, ref zoneId);
        }
        /// <summary>
        /// Xóa tag liên quan tới bài viết
        /// </summary>
        /// <param name="tagId"></param>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteTagNewsById(string tagName, long newsId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<TagBo>().DeleteTagNewsById(tagName, newsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        /// <summary>
        /// Cập nhật lại tag cho bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="strTagName"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateTagNews(long newsId, string strTagName)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<TagBo>().UpdateTagNews(newsId, strTagName);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        #endregion

        #region AutoTag
        public TagAutoEntity[] GetListAutoTag()
        {
            return BoFactory.GetInstance<TagAutoBo>().GetAutoTagList().ToArray();
        }
        public WcfActionResponse UpdateAutoTagIsProcess(long newsId)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = BoFactory.GetInstance<TagAutoBo>().UpdateAutoTagIsProcess(newsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }

        public WcfActionResponse UpdateTagAutoByNewsId(long newsId, string tagIdList)
        {
            WcfActionResponse responseData;
            var errorCode = NewsBo.UpdateTagAutoByNewsId(newsId, tagIdList);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                responseData = WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateTagAutoForNews(long newsId, string tagAutoIdList)
        {
            var errorCode = NewsBo.UpdateTagAutoForNews(newsId, tagAutoIdList);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region Box Tag Embed

        public BoxTagEmbedEntity[] GetListTagEmbed(int zoneId, int type)
        {
            return BoFactory.GetInstance<BoxTagEmbedBo>().GetListTagEmbed(zoneId, type).ToArray();
        }
        public WcfActionResponse UpdateTagEmbed(string listTagId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<BoxTagEmbedBo>().Update(listTagId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(listTagId, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public WcfActionResponse UpdateTagEmbedExternal(string listTagId, int zoneId, int type, string listTitle, string listUrl)
        {

            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<BoxTagEmbedBo>().UpdateExternalTag(listTagId, zoneId, type, listTitle, listUrl);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(listTagId, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public List<QueueUpdateTagChannelEntity> GetUnProcess(int channelId)
        {
            return BoFactory.GetInstance<TagCloudBo>().GetUnProcess(channelId);
        }
        public WcfActionResponse UpdateIsProcess(long newsId)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = BoFactory.GetInstance<TagCloudBo>().UpdateIsProcess(newsId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }
        public WcfActionResponse RecieveFromCloud(TagEntity tag, int zoneId, string zoneIdList, ref long newTagId)
        {
            try
            {
                WcfActionResponse responseData;
                var errorCode = BoFactory.GetInstance<TagBo>().RecieveFromCloud(tag, zoneId, zoneIdList, ref newTagId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new WcfActionResponse { Success = false };
        }

        #region PhotoTag V2
        public PhotoTagV2Entity GetPhotoTagByTagName(string name)
        {
            return PhotoBo.GetTagByTagName(name);
        }

        public List<PhotoTagV2Entity> SearchTagPhotoByName(string name)
        {
            return PhotoBo.SearchTag(name);
        }

        public WcfActionResponse InsertPhotoTag(PhotoTagV2Entity tag)
        {
            WcfActionResponse responseData;
            int newTagId = 0;
            var errorCode = PhotoBo.InsertPhotoTag(tag, ref newTagId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                tag.Id = newTagId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
            }
            else
            {
                if (newTagId > 0)
                {
                    tag.Id = newTagId;
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            return responseData;
        } 
        #endregion
    }
}
