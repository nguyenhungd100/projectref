﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.RollingNews;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RollingNewsServices : IRollingNewsServices
    {
        #region RollingNews

        public List<RollingNewsEntity> SearchRollingNews(string keyword, EnumRollingNewsStatus status, int pageIndex, int pageSize,
                                                     ref int totalRow)
        {
            return RollingNewsBo.SearchRollingNews(keyword, status, pageIndex, pageSize,
                                                   ref totalRow);
        }

        public List<RollingNewsForSuggestionEntity> SearchRollingNewsForSuggestion(string keyword,
                                                                                          EnumRollingNewsStatus status,
                                                                                          int top)
        {
            return RollingNewsBo.SearchRollingNewsForSuggestion(keyword, status, top);
        }

        public RollingNewsEntity GetRollingNewsByRollingNewsId(int rollingNewsId)
        {
            return RollingNewsBo.GetRollingNewsByRollingNewsId(rollingNewsId);
        }
        public WcfActionResponse InsertRollingNews(RollingNewsEntity rollingNews)
        {
            var errorCode = RollingNewsBo.InsertRollingNews(rollingNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse InsertRollingNewsV2(RollingNewsEntity rollingNews, ref int Id)
        {
            var errorCode = RollingNewsBo.InsertRollingNewsV2(rollingNews, ref Id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateRollingNews(RollingNewsEntity rollingNews)
        {
            var errorCode = RollingNewsBo.UpdateRollingNews(rollingNews);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteRollingNews(int rollingNewsId)
        {
            var errorCode = RollingNewsBo.DeleteRollingNews(rollingNewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse ChangeStatusRollingNews(int rollingNewsId, EnumRollingNewsStatus status)
        {
            var errorCode = RollingNewsBo.ChangeStatusRollingNews(rollingNewsId, status);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region RollingNewsAuthor

        public List<RollingNewsAuthorEntity> GetRollingNewsAuthorByRollingNewsId(int rollingNewsId)
        {
            return RollingNewsBo.GetRollingNewsAuthorByRollingNewsId(rollingNewsId);
        }
        public RollingNewsAuthorEntity GetRollingNewsAuthorByRollingNewsAuthorId(int rollingNewsAuthorId)
        {
            return RollingNewsBo.GetRollingNewsAuthorByRollingNewsAuthorId(rollingNewsAuthorId);
        }
        public WcfActionResponse InsertRollingNewsAuthor(RollingNewsAuthorEntity rollingNewsAuthor)
        {
            var errorCode = RollingNewsBo.InsertRollingNewsAuthor(rollingNewsAuthor);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateRollingNewsAuthor(RollingNewsAuthorEntity rollingNewsAuthor)
        {
            var errorCode = RollingNewsBo.UpdateRollingNewsAuthor(rollingNewsAuthor);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteRollingNewsAuthor(int rollingNewsAuthorId)
        {
            var errorCode = RollingNewsBo.DeleteRollingNewsAuthor(rollingNewsAuthorId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region RollingNewsEvent

        public List<RollingNewsEventEntity> GetRollingNewsEventByRollingNewsId(int rollingNewsId, EnumRollingNewsEventType eventType, EnumRollingNewsEventStatus status)
        {
            return RollingNewsBo.GetRollingNewsEventByRollingNewsId(rollingNewsId, eventType, status);
        }
        public RollingNewsEventEntity GetRollingNewsEventByRollingNewsEventId(int rollingNewsEventId)
        {
            return RollingNewsBo.GetRollingNewsEventByRollingNewsEventId(rollingNewsEventId);
        }
        
        public WcfActionResponse InsertRollingNewsEvent(RollingNewsEventEntity rollingNewsEvent)
        {
            var errorCode = RollingNewsBo.InsertRollingNewsEvent(rollingNewsEvent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateRollingNewsEvent(RollingNewsEventEntity rollingNewsEvent)
        {
            var errorCode = RollingNewsBo.UpdateRollingNewsEvent(rollingNewsEvent);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateRollingNewsEventV2(List<RollingNewsEventEntity> rollingNewsEvent, int rollingNewsId)
        {
            var errorCode = RollingNewsBo.UpdateRollingNewsEventV2(rollingNewsEvent, rollingNewsId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteRollingNewsEvent(int rollingNewsEventId)
        {
            var errorCode = RollingNewsBo.DeleteRollingNewsEvent(rollingNewsEventId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse PublishRollingNewsEvent(int rollingNewsEventId)
        {
            var errorCode = RollingNewsBo.PublishRollingNewsEvent(rollingNewsEventId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UnpublishRollingNewsEvent(int rollingNewsEventId)
        {
            var errorCode = RollingNewsBo.UnpublishRollingNewsEvent(rollingNewsEventId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateIsFocusForRollingNewsEvent(int rollingNewsEventId, bool isFocus)
        {
            var errorCode = RollingNewsBo.UpdateIsFocusForRollingNewsEvent(rollingNewsEventId, isFocus);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse RefreshRollingNewsEvent(int rollingNewsEventId)
        {
            var errorCode = RollingNewsBo.UpdateRollingNewsEventIntoRedis(rollingNewsEventId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
