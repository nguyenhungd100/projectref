﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Base.FileUpload;
using ChannelVN.CMS.BO.Base.Photo;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.Base.Video;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.FileUpload;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.ProgramSchedule.Bo;
using ChannelVN.ProgramSchedule.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.Base.PhotoGroup;
using ChannelVN.CMS.BO.Base.PhotoGroup;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MediaServices : IMediaServices
    {
        #region Videos

        #region Video playlists

        #region Updates

        public WcfActionResponse SaveVideoPlaylist(PlaylistEntity playlistEntity)
        {
            var playlistId = 0;
            if (PlaylistBo.Save(playlistEntity, ref playlistId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(playlistId.ToString(CultureInfo.CurrentCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse SaveVideoPlaylistV2(PlaylistEntity playlistEntity)
        {
            var playlistId = 0;
            if (PlaylistBo.SaveV2(playlistEntity, ref playlistId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(playlistId.ToString(CultureInfo.CurrentCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse RemoveVideoOutOfPlaylist(int playlistId, string videoIdList)
        {
            if (PlaylistBo.RemoveVideoOutOfPlaylist(playlistId, videoIdList) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(playlistId.ToString(CultureInfo.CurrentCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse AddVideoIntoPlayList(int videoId, int playlistId, int priority)
        {
            if (PlaylistBo.AddVideo(videoId, playlistId, priority) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse AddVideoListIntoPlaylist(string videoIdList, string timeToPlayList, int playlistId, bool deletOldVideo)
        {
            if (PlaylistBo.AddVideoList(videoIdList, timeToPlayList, playlistId, deletOldVideo) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
        }

        public WcfActionResponse DeleteVideoPlaylistById(int playlistId)
        {
            if (PlaylistBo.DeleteById(playlistId) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse(playlistId.ToString(CultureInfo.CurrentCulture),
                                                               ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse DeleteVideoPlaylistByIdList(string playlistIdList)
        {
            if (PlaylistBo.DeleteByIdList(playlistIdList) == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                         ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError]);
        }

        public WcfActionResponse PublishPlaylist(int playlistId)
        {
            try
            {
                if (PlaylistBo.PublishPlaylist(playlistId, WcfMessageHeader.Current.ClientUsername) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UnpublishPlaylist(int playlistId)
        {
            try
            {
                if (PlaylistBo.UnpublishPlaylist(playlistId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse ChangeMode(int playlistId, EnumPlayListMode mode)
        {
            try
            {
                if (PlaylistBo.ChangeMode(playlistId, mode) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse ExportPlaylistToProgramSchedule(int playlistId, int programChannelId, string programScheduleNameFormat)
        {
            var playlist = PlaylistBo.GetById(playlistId);
            if (playlist != null)
            {
                var scheduleDate = new DateTime(playlist.DistributionDate.Year, playlist.DistributionDate.Month,
                                                playlist.DistributionDate.Day, 0, 0, 0);
                var schedules = ProgramScheduleBo.GetByProgramScheduleDate(programChannelId, scheduleDate);
                if (schedules != null && schedules.Count > 0)
                {
                    ProgramScheduleBo.DeleteProgramSchedule(schedules[0].Id);
                }
                #region Insert Program schedule
                var currentAccount = WcfMessageHeader.Current.ClientUsername;
                var programSchedule = new ProgramScheduleEntity
                {
                    ProgramChannelId = programChannelId,
                    ScheduleName =
                        string.Format(programScheduleNameFormat,
                                      scheduleDate.ToString("dd-MM-yyyy")),
                    ScheduleDate = scheduleDate,
                    CreatedBy = currentAccount,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = currentAccount,
                    LastModifiedDate = DateTime.Now,
                    PlayListId = playlist.Id,
                    Status = (int)EnumProgramScheduleStatus.Actived,
                    ScheduleAvatar = ""
                };
                var programScheduleId = 0;
                ProgramScheduleBo.InsertProgramSchedule(programSchedule, ref programScheduleId);
                if (programScheduleId > 0)
                {
                    var videoInPlaylist = VideoBo.GetAllVideoInPlaylist(playlist.Id);
                    foreach (var video in videoInPlaylist)
                    {
                        var programScheduleDetailId = 0;
                        #region Insert Program schedule detail
                        ProgramScheduleBo.InsertProgramScheduleDetail(new ProgramScheduleDetailEntity
                        {
                            ProgramScheduleId = programScheduleId,
                            Title = video.Name,
                            Description = video.Description,
                            Avatar = video.Avatar,
                            VideoId = video.Id,
                            ScheduleTime =
                                new DateTime(scheduleDate.Year,
                                             scheduleDate.Month,
                                             scheduleDate.Day,
                                             video.
                                                 DistributionDate.
                                                 Hour,
                                             video.
                                                 DistributionDate.
                                                 Minute,
                                             video.
                                                 DistributionDate.
                                                 Second),
                            Status =
                                (int)
                                EnumProgramScheduleDetailStatus.
                                    Actived,
                            CreatedBy = currentAccount,
                            CreatedDate = DateTime.Now,
                            LastModifiedBy = currentAccount,
                            LastModifiedDate = DateTime.Now,
                            PublishedBy = currentAccount,
                            PublishedDate = DateTime.Now
                        }, ref programScheduleDetailId);
                        #endregion
                    }
                }
                #endregion
            }
            return WcfActionResponse.CreateSuccessResponse();
        }

        #endregion

        #region Gets

        public PlaylistEntity GetVideoPlaylistById(int playlistId)
        {
            return PlaylistBo.GetById(playlistId);
        }

        public PlaylistEntity[] GetListVideoPlaylistByVideoId(int videoId)
        {
            return PlaylistBo.GetListByVideoId(videoId).ToArray();
        }

        public PlaylistEntity[] GetListVideoPlaylistPaging(int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            return PlaylistBo.GetListPaging(pageIndex, pageSize, status, keyword, sortOder, ref totalRow).ToArray();
        }

        public PlaylistEntity[] GetMyPlaylist(string username, int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            return PlaylistBo.GetMyPlaylist(username, pageIndex, pageSize, status, keyword, sortOder, ref totalRow).ToArray();
        }

        public PlaylistEntity[] SearchPlayList(string username, int zoneVideoId, EnumPlayListStatus status, string keyword, EnumPlayListMode mode, EnumPlayListSort sortOder, int pageIndex, int pageSize, ref int totalRow)
        {
            return PlaylistBo.SearchPlayList(username, zoneVideoId, status, keyword, mode, sortOder, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public PlaylistCounterEntity[] GetPlaylistCounter()
        {
            return PlaylistBo.GetPlaylistCounter().ToArray();
        }

        public List<PlaylistWithZoneEntity> GetLastDaysHavePlaylistWithZone(DateTime fromDate, DateTime toDate, int group, int team, int mode)
        {
            return PlaylistBo.GetLastDaysHavePlaylistWithZone(fromDate, toDate, group, team, mode);
        }

        public List<PlaylistWithZoneEntity> GetTopDayHaveVideo(int top)
        {
            return PlaylistBo.GetTopDayHaveVideo(top);
        }

        public List<PlaylistInZoneEntity> GetZoneByPlaylist(int playlistId)
        {
            return PlaylistBo.GetZoneByPlaylist(playlistId);
        }

        #endregion

        #endregion

        #region Video data

        #region Updates

        public WcfActionResponse UpdateConvertedMode(string listVideoId)
        {
            try
            {
                var result = VideoBo.UpdateConvertedMode(listVideoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideo(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.Save(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideoV4(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveV4(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideoV3(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveV3(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }


        public WcfActionResponse SaveVideoName(VideoEntity videoEntity)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveName(videoEntity);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideoNameAndDescription(VideoEntity videoEntity)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveNameAndDescription(videoEntity);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SaveVideoV2(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveV2(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateVideoMode(int mode, int id)
        {
            try
            {
                var result = VideoBo.UpdateMode(mode, id, WcfMessageHeader.Current.ClientUsername);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(id.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateVideoAvatar(string avatar, int id)
        {
            try
            {
                var result = VideoBo.UpdateAvatar(avatar, id, WcfMessageHeader.Current.ClientUsername);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(id.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateVideoViews(string keyVideo, int views)
        {
            try
            {
                var result = VideoBo.UpdateViews(keyVideo, views);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateListVideoViews(string listKeyVideo, string listViewVideo)
        {
            try
            {
                var result = VideoBo.UpdateListVideoViews(listKeyVideo, listViewVideo);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse AddVideoFileToVideo(VideoEntity videoEntity)
        {
            try
            {
                var videoId = 0;
                if (VideoBo.AddVideoFileToVideo(videoEntity, ref videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateNewsIdForListVideoId(string idList, long newsId)
        {
            try
            {
                if (VideoBo.UpdateNewsIdForList(idList, newsId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoById(int videoId)
        {
            try
            {
                var result = VideoBo.DeleteById(videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SendVideo(int videoId)
        {
            try
            {
                var result = VideoBo.SendVideo(videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse ReturnVideo(int videoId)
        {
            try
            {
                var result = VideoBo.ReturnVideo(videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse PublishVideo(int videoId)
        {
            try
            {
                var result = VideoBo.PublishVideo(videoId, WcfMessageHeader.Current.ClientUsername);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse PublishVideoInNews(int videoId, DateTime distributionDate)
        {
            try
            {
                var result = VideoBo.PublishVideoInNews(videoId, WcfMessageHeader.Current.ClientUsername, distributionDate);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UnpublishVideo(int videoId)
        {
            try
            {
                var result = VideoBo.UnpublishVideo(videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoByIdList(string videoIdList)
        {
            try
            {
                if (VideoBo.DeleteByIdList(videoIdList) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse ChangeVideoStatus(int videoId)
        {
            try
            {
                if (VideoBo.ChangeStatus(videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse ChangeVideoStatusByList(string videoIdList)
        {
            try
            {
                if (VideoBo.ChangeStatusByList(videoIdList) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SyncNewsAndVideo(string videoIdList, long newsId)
        {
            try
            {
                if (VideoBo.SyncNewsAndVideo(videoIdList, newsId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse SyncNewsAndVideoWithStatus(string videoIdList, long newsId, EnumVideoStatus status, int excludeStatus)
        {
            try
            {
                if (VideoBo.SyncNewsAndVideoWithStatus(videoIdList, newsId, status, excludeStatus) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        /// <summary>
        /// Tướng Cương: Update KeyVideo By FileName VTVCD24
        /// </summary>
        /// <param name="keyVideo">khi bên transcode callback về thì update vào Keyvideo khi đấy mới được dùng</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateKeyVideoByFileName(string keyVideo, string fileName)
        {
            try
            {
                var result = VideoBo.UpdateKeyVideoByFileName(keyVideo, fileName);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(keyVideo.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion

        #region Gets

        public List<VideoKeyEntity> GetListVideoWaitConvert(int top)
        {
            return VideoBo.GetListVideoWaitConvert(top);
        }

        public VideoEntity GetDetailVideo(int videoId)
        {
            var zoneName = "";
            return VideoBo.GetById(videoId, ref zoneName);
        }
        public VideoEntity GetDetailVideoByVideoExternalId(string videoExternalId)
        {
            var zoneName = "";
            return VideoBo.GetByVideoExternalId(videoExternalId, ref zoneName);
        }
        public VideoEntity GetDetailVideoV2(int videoId)
        {
            var zoneName = "";
            return VideoBo.GetByIdV2(videoId, ref zoneName);
        }

        public VideoEntity GetVideoByFileName(string fileName)
        {
            return VideoBo.GetByFileName(fileName);
        }
        public VideoEntity GetDetailVideoByKeyvideo(string keyvideo)
        {
            var zoneName = "";
            return VideoBo.GetByKeyvideo(keyvideo, ref zoneName);
        }
        public VideoDetailForEdit GetDetailVideoForEdit(int videoId)
        {
            try
            {
                var zoneName = string.Empty;
                var objectForSerialize = new VideoDetailForEdit();
                var videoRelationList = new List<VideoEntity>();

                var videoEntity = VideoBo.GetById(videoId, ref zoneName);
                // Add VideoEntity Serialized
                objectForSerialize.VideoInfo = videoEntity;
                if (videoEntity != null)
                    if (!string.IsNullOrEmpty(videoEntity.VideoRelation))
                        videoRelationList = VideoBo.GetListByIdList(videoEntity.VideoRelation);

                var zoneRelateList = ZoneVideoBo.GetListZoneRelation(videoId);
                var tagList = VideoBo.GetVideoTagListByVideo(videoId);
                var playlistList = PlaylistBo.GetListByVideoId(videoId);
                objectForSerialize.VideoRelation = videoRelationList;
                objectForSerialize.TagList = tagList;
                objectForSerialize.ZoneVideoList = zoneRelateList;
                objectForSerialize.Playlist = playlistList;
                objectForSerialize.ShareLink = BoConstants.VideoShareLink;
                try
                {
                    if (WcfMessageHeader.Current.Namespace == "VTV")
                    {
                        objectForSerialize.VideoExtension = VideoBo.GetVideoExtensionByVideoId(videoId);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                return objectForSerialize;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return null;
        }
        public VideoDetailForEdit GetDetailVideoForEditV2(int videoId)
        {
            try
            {
                var zoneName = string.Empty;
                var objectForSerialize = new VideoDetailForEdit();
                var videoRelationList = new List<VideoEntity>();

                var videoEntity = VideoBo.GetByIdV2(videoId, ref zoneName);
                // Add VideoEntity Serialized
                objectForSerialize.VideoInfo = videoEntity;
                if (videoEntity != null)
                    if (!string.IsNullOrEmpty(videoEntity.VideoRelation))
                        videoRelationList = VideoBo.GetListByIdList(videoEntity.VideoRelation);

                var zoneRelateList = ZoneVideoBo.GetListZoneRelation(videoId);
                var tagList = VideoBo.GetVideoTagListByVideo(videoId);
                var playlistList = PlaylistBo.GetListByVideoId(videoId);
                objectForSerialize.VideoRelation = videoRelationList;
                objectForSerialize.TagList = tagList;
                objectForSerialize.ZoneVideoList = zoneRelateList;
                objectForSerialize.Playlist = playlistList;
                objectForSerialize.ShareLink = BoConstants.VideoShareLink;
                try
                {
                    if (WcfMessageHeader.Current.Namespace == "VTV")
                    {
                        objectForSerialize.VideoExtension = VideoBo.GetVideoExtensionByVideoId(videoId);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
                return objectForSerialize;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return null;
        }

        public VideoEntity[] GetListMyVideo(string username, int pageIndex, int pageSize, string keyword, int status, int sortOrder, int cateId, int playlistId, ref int totalRow)
        {
            return VideoBo.GetListMyVideo(username, pageIndex, pageSize, keyword, status, sortOrder, cateId, playlistId,
                                               ref totalRow).ToArray();
        }

        public VideoEntity[] GetListVideoByIdList(string videoIdList)
        {
            return VideoBo.GetListByIdList(videoIdList).ToArray();
        }

        public VideoEntity[] GetListVideoByCate(int pageIndex, int pageSize, int zoneId, int playlistId, int status, string keyword, int sortOrder, ref int totalRow)
        {
            return
                VideoBo.GetListPagingByZone(pageIndex, pageSize, zoneId, playlistId, status, keyword, sortOrder,
                                            ref totalRow).ToArray();
        }

        public VideoEntity[] GetListVideoInPlaylist(int pageIndex, int pageSize, int playlistId, int status, string keyword, int sortOder, ref int totalRow)
        {
            return VideoBo.GetListPagingInPlaylist(pageIndex, pageSize, playlistId, status, keyword, sortOder, ref totalRow).ToArray();
        }

        public VideoEntity[] GetListAllVideoInPlaylist(int playlistId)
        {
            return VideoBo.GetAllVideoInPlaylist(playlistId).ToArray();
        }

        public VideoEntity[] GetListAllVideoInPlaylistByMode(int playlistId, int mode)
        {
            return VideoBo.GetAllVideoInPlaylistByMode(playlistId, mode).ToArray();
        }

        public VideoEntity[] GetVideoInPlaylist(int playlistId, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.GetVideoInPlaylist(playlistId, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public VideoEntity[] SearchVideo(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.Search(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoEntity[] SearchVideoForList(string username, int zoneVideoId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchForList(username, zoneVideoId, keyword, (int)status, (int)order, fromDate, toDate,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public List<VideoDistributionEntity> SearchDistribution(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchDistribution(keyword, viewDate, mode, zoneIds, pageIndex, pageSize, ref totalRow);
        }
        public List<VideoDistributionEntity> SearchDistributionV2(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchDistributionV2(keyword, viewDate, mode, zoneIds, pageIndex, pageSize, ref totalRow);
        }
        public VideoEntity[] SearchVideoV2(string username, int videoFolderId, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchV2(username, videoFolderId, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoEntity[] SearchVideoAd(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, bool allowAd, ref int totalRow)
        {
            return
                VideoBo.SearchAd(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, allowAd, ref totalRow).ToArray();
        }
        public VideoEntity[] SearchVideoExcludeBomb(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchExcludeBomb(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoExportEntity[] SearchExportVideo(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchExport(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoEntity[] SearchVideoWithExcludeZones(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, string excludeZones, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchWithExcludeZones(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode, excludeZones,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }

        public VideoInNewsEntity[] GetVideoInNewsList(int videoId)
        {
            return VideoBo.GetVideoInNewsList(videoId).ToArray();
        }

        public VideoInfoForSync GetVideoInfoForSynchonize(int videoId)
        {
            var zoneName = string.Empty;
            var video = VideoBo.GetById(videoId, ref zoneName);
            if (null == video)
            {
                return null;
            }
            var tags = string.Empty;
            var zoneIds = string.Empty;

            #region Gets TagList
            var tagListInVideo = new List<VideoTagEntity>();
            try
            {
                tagListInVideo = VideoBo.GetVideoTagListByVideo(video.Id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            #endregion

            #region Gets ZoneList
            try
            {
                var zoneList = ZoneVideoBo.GetListZoneRelation(video.Id);
                if (zoneList.Count > 0)
                {
                    zoneIds = zoneList.Aggregate(tags, (current, zone) => current + (", " + zone.Id));
                    if (zoneIds != "") zoneIds = zoneIds.Remove(0, 2);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            #endregion

            return new VideoInfoForSync
            {
                Video = video,
                TagList = tagListInVideo,
                ZoneIds = zoneIds
            };
        }

        public VideoCounterEntity[] GetVideoCounter()
        {
            return VideoBo.GetVideoCounter().ToArray();
        }

        public string[] GetLastPublishedForUpdateView(DateTime fromDate)
        {
            return VideoBo.GetLastPublishedForUpdateView(fromDate).ToArray();
        }

        public VideoExportEntity[] GetRoyalties(DateTime startDate, DateTime endDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = VideoBo.GetRoyalties(startDate, endDate, pageIndex, pageSize);
                totalRow = VideoBo.CountRoyalties(startDate, endDate);
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new VideoExportEntity[] { };
        }

        public VideoExportEntity[] GetRoyaltiesByZone(DateTime startDate, DateTime endDate, int pageIndex, int pageSize, ref int totalRow, string zones)
        {
            try
            {
                var data = VideoBo.GetRoyaltiesByZone(startDate, endDate, pageIndex, pageSize, zones);
                totalRow = VideoBo.CountRoyaltiesByZone(startDate, endDate, zones);
                return data.ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new VideoExportEntity[] { };
        }

        public List<VideoSharingEntity> GetListSharing(string keyword, string zoneIds, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.GetListSharing(keyword, zoneIds, dateFrom, dateTo, pageIndex, pageSize, ref totalRow);
        }
        public VideoEntity[] GetListVideoByHour(string zoneVideoIds, int Hour)
        {
            return VideoBo.GetListVideoByHour(zoneVideoIds, Hour).ToArray();
        }
        #endregion

        #region ZoneVideo

        public ZoneVideoEntity[] GetVideoZoneListByParentId(int parentId, EnumZoneVideoStatus status)
        {
            return ZoneVideoBo.GetListByParentId(parentId, (int)status).ToArray();
        }

        public ZoneVideoEntity[] GetVideoZoneListAsTreeview(int parentId, EnumZoneVideoStatus status, string prefix)
        {
            var zoneList = ZoneVideoBo.GetListByParentId(parentId, (int)status);
            return ZoneVideoBo.BindAllOfZoneVideoToTreeviewFullDepth(zoneList, prefix).ToArray();
        }

        public PlaylistGroupEntity[] GetPlaylistGroupAsTreeview(int parentId, string prefix)
        {
            var zoneList = VideoBo.GetPlaylistGroupByParentId(parentId);
            return VideoBo.BindAllOfPlaylistGroupToTreeviewFullDepth(zoneList, prefix).ToArray();
        }

        public ZoneVideoEntity[] GetVideoZoneListRelation(int videoId)
        {
            return ZoneVideoBo.GetListZoneRelation(videoId).ToArray();
        }

        public ZoneVideoEntity GetVideoZoneByVideoZoneId(int videoZoneId)
        {
            return ZoneVideoBo.GetZoneVideoByZoneVideoId(videoZoneId);
        }

        public ZoneVideoDetailEntity GetVideoZoneDetailByVideoZoneId(int videoZoneId)
        {
            return ZoneVideoBo.GetZoneVideoDetailByZoneVideoId(videoZoneId);
        }


        public WcfActionResponse InsertVideoZone(ZoneVideoEntity zoneVideo, string listVideoTagId, ref int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.InsertVideoZone(zoneVideo, listVideoTagId, ref zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(zoneVideoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateVideoZone(ZoneVideoEntity zoneVideo, string listVideoTagId)
        {
            try
            {
                if (ZoneVideoBo.UpdateVideoZone(zoneVideo, listVideoTagId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse MoveVideoZoneUp(int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.MoveVideoZoneUp(zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse MoveVideoZoneDown(int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.MoveVideoZoneDown(zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoZone(int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.DeleteVideoZone(zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse UpdateAvatarZoneVideo(string zoneVideoAvatars)
        {
            try
            {
                if (ZoneVideoBo.UpdateAvatarZoneVideo(zoneVideoAvatars) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }


        public WcfActionResponse GetZoneVideoIdByNewsZoneId(string newsZoneId, ref int zoneVideoId)
        {
            try
            {
                if (ZoneVideoBo.GetZoneVideoIdByNewsZoneId(newsZoneId, ref zoneVideoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(zoneVideoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.VideoNotAvailable,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.VideoNotAvailable]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        #endregion

        #region VideoFromYoutube

        public WcfActionResponse InsertVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id)
        {
            try
            {
                var statusUpdate = VideoBo.InsertVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList, ref id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate, ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }

        public WcfActionResponse UpdateVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList)
        {
            try
            {
                var statusUpdate = VideoBo.UpdateVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate, ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }

        public WcfActionResponse SaveVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id)
        {
            try
            {
                var statusUpdate = VideoBo.SaveVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList, ref id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate, ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message);
            }
        }

        public WcfActionResponse UpdateVideoFromYoutubeStatus(int id, string avatar, string filePath, EnumVideoFromYoutubeStatus status, string htmlCode, string keyVideo, string pname, string fileName, string duration, string size, int capacity)
        {
            try
            {
                var statusUpdate = VideoBo.UpdateVideoFromYoutubeStatus(id, avatar, filePath, status, htmlCode, keyVideo, pname,
                                                                        fileName, duration, size, capacity);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate, ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("MediaServices.UpdateVideoFromYoutubeStatus() => {0}", ex));
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoFromYoutube(int id)
        {
            try
            {
                var statusUpdate = VideoBo.DeleteVideoFromYoutube(id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                                 ErrorMapping.Current[
                                                                     ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse SendVideoFromYoutube(int id)
        {
            try
            {
                var statusUpdate = VideoBo.SendVideoFromYoutube(id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                                 ErrorMapping.Current[
                                                                     ErrorMapping.ErrorCodes.BusinessError]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse PublishVideoFromYoutube(int id)
        {
            try
            {
                var statusUpdate = VideoBo.PublishVideoFromYoutube(id);
                if (statusUpdate == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)statusUpdate,
                                                                 ErrorMapping.Current[statusUpdate]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public VideoFromYoutubeEntity GetVideoFromYoutubeById(int id)
        {
            try
            {
                return VideoBo.GetVideoFromYoutubeById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public VideoFromYoutubeForEditEntity GetVideoFromYoutubeForEdit(int id)
        {
            try
            {
                return VideoBo.GetVideoFromYoutubeForEdit(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public VideoFromYoutubeEntity[] GetListVideoFromYoutube(string accountName, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.GetListVideoFromYoutube(accountName, status, order, pageIndex, pageSize, ref totalRow).ToArray();
        }

        public List<VideoFromYoutubeDetailEntity> SearchVideoFromYoutube(string username, string keyword, EnumVideoFromYoutubeStatus status, int zoneVideoId, int playlistId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.SearchVideoFromYoutube(username, keyword, status, zoneVideoId, playlistId, fromDate,
                                                  toDate, pageIndex, pageSize, ref totalRow);
        }

        public List<VideoFromYoutubeEntity> GetUploadingVideoFromYoutube()
        {
            try
            {
                return VideoBo.GetUploadingVideoFromYoutube();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("MediaServices.GetUploadingVideoFromYoutube() => {0}", ex));
                return new List<VideoFromYoutubeEntity>();
            }
        }

        #endregion

        #region BoxVideoEmbed
        public BoxVideoEmbedEntity[] GetListVideoEmbed(int zoneId, int type)
        {
            return BoxVideoEmbedBo.GetListVideoEmbed(zoneId, type).ToArray();
        }

        public WcfActionResponse InsertVideoEmbed(BoxVideoEmbedEntity videoEmbedBox)
        {

            WcfActionResponse responseData;

            var errorCode = BoxVideoEmbedBo.Insert(videoEmbedBox);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;

        }

        public WcfActionResponse UpdateVideoEmbed(string listVideoId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedBo.Update(listVideoId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }
        public WcfActionResponse UpdateVideoEmbed_Epl(List<BoxVideoEmbedEplEntity> listEmbed)
        {

            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedBo.UpdateEpl(listEmbed);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteVideoEmbed(long videoId, int zoneId, int type)
        {

            WcfActionResponse responseData;

            var newEncryptVideoId = String.Empty;
            var errorCode = BoxVideoEmbedBo.Delete(videoId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;

        }
        #endregion

        #region BoxVideoEmbedByNewsZone
        public BoxVideoEmbedByNewsZoneEntity[] GetListVideoEmbedByNewsZone(int zoneId, int type)
        {
            return BoxVideoEmbedByNewsZoneBo.GetListVideoEmbed(zoneId, type).ToArray();
        }

        public WcfActionResponse InsertVideoEmbedByNewsZone(BoxVideoEmbedByNewsZoneEntity videoEmbedBox)
        {

            WcfActionResponse responseData;

            var errorCode = BoxVideoEmbedByNewsZoneBo.Insert(videoEmbedBox);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;

        }

        public WcfActionResponse UpdateVideoEmbedByNewsZone(string listVideoId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = BoxVideoEmbedByNewsZoneBo.Update(listVideoId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteVideoEmbedByNewsZone(long videoId, int zoneId, int type)
        {

            WcfActionResponse responseData;

            var newEncryptVideoId = String.Empty;
            var errorCode = BoxVideoEmbedByNewsZoneBo.Delete(videoId, zoneId, type);
            responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

            return responseData;

        }
        #endregion

        #region BoxProgramEmbed

        public ZoneVideoEntity[] SearchProgram(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoxVideoProgramEmbedBo.SearchProgramEmbed(zoneId, keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public BoxVideoProgramEmbedEntity[] GetListProgramEmbed(int zoneId, int type)
        {
            return BoxVideoProgramEmbedBo.GetListProgramEmbed(zoneId, type).ToArray();
        }

        public WcfActionResponse UpdateProgramEmbed(string listVideoId, int zoneId, int type)
        {

            WcfActionResponse responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError, ErrorMapping.Current[ErrorMapping.ErrorCodes.BusinessError]);
            try
            {
                var errorCode = BoxVideoProgramEmbedBo.Update(listVideoId, zoneId, type);
                responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse() : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return responseData;
        }
        #endregion

        #endregion

        #region Video Thread
        public List<VideoThreadEntity> SearchVideoThread(string keyword, bool isHotThread, EnumVideoThreadStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoThreadBo.SearchVideoThread(keyword, isHotThread, (int)status, pageIndex, pageSize, ref totalRow);
        }

        public VideoThreadDetailEntity GetVideoThreadById(int id)
        {
            return VideoThreadBo.GetVideoThreadById(id);
        }

        public WcfActionResponse InsertVideoThread(VideoThreadEntity videoThread, string videoIdList, ref int videoThreadId)
        {
            var result = VideoThreadBo.InsertVideoThread(videoThread, videoIdList, ref videoThreadId);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(videoThreadId.ToString(CultureInfo.InvariantCulture),
                                                                 ErrorMapping.Current[ErrorMapping.ErrorCodes.Success])
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse UpdateVideoThread(VideoThreadEntity videoThread, string videoIdList)
        {
            var result = VideoThreadBo.UpdateVideoThread(videoThread, videoIdList);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse DeleteVideoThread(int videoThreadId)
        {
            var result = VideoThreadBo.DeleteVideoThread(videoThreadId);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse UpdateVideoInVideoThread(int videoThreadId, string videoIdList)
        {
            var result = VideoThreadBo.UpdateVideoInVideoThread(videoThreadId, videoIdList);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }
        #endregion

        #region Video tag

        public List<VideoTagEntity> SearchVideoTag(string keyword, int parentId, EnumVideoTagMode tagMode, EnumVideoTagStatus status, EnumVideoTagSort orderBy, bool getTagHasVideoOnly, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoTagBo.SearchVideoTag(keyword, parentId, tagMode, status, orderBy, getTagHasVideoOnly, pageIndex,
                                     pageSize, ref totalRow);
        }
        public List<VideoTagEntity> GetVideoTagByZoneVideoId(int zoneVideoId)
        {
            return VideoTagBo.GetVideoTagByZoneVideoId(zoneVideoId);
        }
        public VideoTagEntity GetVideoTagById(int id)
        {
            try
            {
                return VideoTagBo.GetVideoTagById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public VideoTagEntity GetVideoTagByName(string name)
        {
            try
            {
                return VideoTagBo.GetVideoTagByName(name);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return null;
        }

        public WcfActionResponse InsertVideoTag(VideoTagEntity videoTag, ref int videoTagId)
        {
            var result = VideoTagBo.InsertVideoTag(videoTag, ref videoTagId);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }
        public WcfActionResponse UpdateVideoTagMode(int videoTagId, bool isHotTag)
        {
            var result = VideoTagBo.UpdateVideoTagMode(videoTagId, isHotTag);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse UpdateVideoTag(VideoTagEntity videoTag)
        {
            var result = VideoTagBo.UpdateVideoTag(videoTag);
            return result == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
        }

        public WcfActionResponse UpdateVideoTagWithTagName(string videoTagName, ref int videoTagId)
        {
            try
            {
                var result = VideoTagBo.UpdateVideoTag(videoTagName, ref videoTagId);
                return result == ErrorMapping.ErrorCodes.Success
                           ? WcfActionResponse.CreateSuccessResponse()
                           : WcfActionResponse.CreateErrorResponse((int)result, ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("MediaServices.UpdateVideoTagWithTagName() => {0}", ex));
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        #endregion


        #region Video Folder

        public WcfActionResponse InsertVideoFolder(VideoFolderEntity videoFolder)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoFolderId = 0;

            if (VideoFolderBo.InsertVideoFolder(videoFolder, ref videoFolderId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolderId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateVideoFolder(VideoFolderEntity videoFolder)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (VideoFolderBo.UpdateVideoFolder(videoFolder) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolder.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteVideoFolder(int videoFolderId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (VideoFolderBo.DeleteVideoFolder(videoFolderId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolderId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public VideoFolderEntity GetVideoFolderByVideoFolderId(int videoFolderId)
        {
            return VideoFolderBo.GetVideoFolderByVideoFolderId(videoFolderId);
        }

        public VideoFolderEntity[] GetVideoFolderByParentId(int parentId, string createdBy)
        {
            return VideoFolderBo.GetVideoFolderByParentId(parentId, createdBy).ToArray();
        }

        public VideoFolderEntity[] VideoFolderSearch(int parentId, string name, string createdBy, int status)
        {
            return VideoFolderBo.VideoFolderSearch(parentId, name, createdBy, status).ToArray();
        }

        public List<VideoFolderEntity> GetVideoFolderWithTreeView(string createBy)
        {
            return VideoFolderBo.GetAllFolderWithTreeView(createBy);
        }

        #endregion

        #region VideoEPL
        public VideoEPLEntity[] VideoEPLByZoneId(int zoneId, DateTime dateFrom, DateTime dateTo)
        {
            return VideoEPLBo.VideoEPLByZone(zoneId, dateFrom, dateTo).ToArray();
        }
        public VideoEPLZoneToZoneEntity[] VideoEPLZoneAndZone(int zoneId1, int zoneId2, DateTime dateFrom, DateTime dateTo)
        {
            return VideoEPLBo.VideoEPLZoneAndZone(zoneId1, zoneId2, dateFrom, dateTo).ToArray();
        }
        #endregion

        #endregion

        #region Photos

        #region Photo

        public WcfActionResponse InsertPhoto(PhotoEntity video, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoId = 0L;

            if (PhotoBo.InsertPhoto(video, tagIdList, ref videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse InsertPhotoV2(PhotoEntity video, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoId = 0L;

            if (PhotoBo.InsertPhotoV2(video, tagIdList, ref videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhoto(PhotoEntity video, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhoto(video, tagIdList) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = video.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdatePhotoV2(PhotoEntity video, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoV2(video, tagIdList) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = video.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeletePhoto(long videoId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.DeletePhoto(videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoPublishedEntity[] PublishPhoto(string listPhotoId, int albumId, long newsId, int zoneId, string distributedBy)
        {
            return PhotoBo.PublishPhoto(listPhotoId, albumId, newsId, zoneId, distributedBy).ToArray();
        }

        public PhotoEntity GetPhotoByPhotoId(long videoId)
        {
            return PhotoBo.GetPhotoByPhotoId(videoId);
        }
        public PhotoEntity GetPhotoByPhotoIdV2(long videoId)
        {
            return PhotoBo.GetPhotoByPhotoIdV2(videoId);
        }

        public PhotoEntity GetPhotoByImageUrl(string imageUrl)
        {
            return PhotoBo.GetPhotoByImageUrl(imageUrl);
        }

        public PhotoEntity[] SearchPhoto(string keyword, int videoLabelId, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                PhotoBo.SearchPhoto(keyword, videoLabelId, zoneId, fromDate, toDate, createdBy, pageIndex, pageSize,
                                    ref totalRow).ToArray();
        }
        public PhotoEntity[] SearchPhotoV2(string keyword, int videoFolderId, int videoLabelId, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                PhotoBo.SearchPhotoV2(keyword, videoFolderId, videoLabelId, zoneId, fromDate, toDate, createdBy, pageIndex, pageSize,
                                    ref totalRow).ToArray();
        }

        #endregion

        #region Photo published

        public WcfActionResponse UpdatePhotoPublished(PhotoPublishedEntity videoPublished)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoPublished(videoPublished) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoPublished.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoPublishedAlbumId(PhotoPublishedUpdateEntity videoPublished)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoPublishedAlbumid(videoPublished) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoPublished.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoPublishedInUsed(string listPhotoPublishedIdInUsed, long newsId, int albumId, string createdBy)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoPublishedInUsed(listPhotoPublishedIdInUsed, newsId, albumId, createdBy) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        //public WcfActionResponse UpdatePhotoPublishedInUsedWhenInsertNews(string listPhotoPublishedIdInUsed, long newsId, string createdBy)
        //{
        //    if (!SecretKey.IsValid(secretKey)) return WcfActionResponse.CreateErrorResponseForInvalidRequest();
        //    var responseData = WcfActionResponse.CreateSuccessResponse();

        //    if (PhotoBo.UpdatePhotoPublishedInUsedWhenInsertNews(listPhotoPublishedIdInUsed, newsId, createdBy) == ErrorMapping.ErrorCodes.Success)
        //    {
        //        responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
        //        responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
        //    }
        //    else
        //    {
        //        responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
        //        responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
        //    }
        //    return responseData;
        //}

        public WcfActionResponse UpdatePhotoPublishedPriority(string listPhotoPublishedId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.UpdatePhotoPublishedPriority(listPhotoPublishedId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoPublishedEntity GetPhotoPublishedByPhotoPublishedId(long videoPublishedId)
        {
            return PhotoBo.GetPhotoPublishedByPhotoPublishedId(videoPublishedId);
        }

        public PhotoPublishedEntity[] GetPhotoPublishedByPhotoId(long videoId)
        {
            return PhotoBo.GetPhotoPublishedByPhotoId(videoId).ToArray();
        }

        public WcfActionResponse DeleteListPhotoPublished(string listPhotoPublishedId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoBo.DeletePhotoPublished(listPhotoPublishedId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoPublishedEntity[] SearchPhotoPublishedInAlbum(string keyword, int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            return PhotoBo.SearchInAlbum(keyword, albumId, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion

        #region Album

        public WcfActionResponse InsertAlbum(AlbumEntity album, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();
            var albumId = 0;

            if (AlbumBo.InsertAlbum(album, tagIdList, ref albumId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = albumId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateAlbum(AlbumEntity album, string tagIdList)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (AlbumBo.UpdateAlbum(album, tagIdList) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = album.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteAlbum(int albumId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (AlbumBo.DeleteAlbum(albumId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = albumId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public AlbumPublishedEntity[] PublishAlbum(string listAlbumId, long newsId, int zoneId, string distributedBy)
        {
            return AlbumBo.PublishAlbum(listAlbumId, newsId, zoneId, distributedBy).ToArray();
        }

        public AlbumEntity GetAlbumByAlbumId(int albumId)
        {
            return AlbumBo.GetAlbumByAlbumId(albumId);
        }

        public AlbumEntity[] SearchAlbum(string keyword, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            return AlbumBo.SearchAlbum(keyword, zoneId, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow).ToArray();
        }

        #endregion

        #region Album published

        public WcfActionResponse UpdateAlbumPublished(AlbumPublishedEntity albumPublished)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (AlbumBo.UpdateAlbumPublished(albumPublished) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = albumPublished.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateAlbumPublishedInUsed(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (AlbumBo.UpdateAlbumPublishedInUsed(listAlbumPublishedIdInUsed, newsId, createdBy) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        //public WcfActionResponse UpdateAlbumPublishedInUsedWhenInsertNews(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        //{
        //    if (!SecretKey.IsValid(secretKey)) return WcfActionResponse.CreateErrorResponseForInvalidRequest();
        //    var responseData = WcfActionResponse.CreateSuccessResponse();

        //    if (AlbumBo.UpdateAlbumPublishedInUsedWhenInsertNews(listAlbumPublishedIdInUsed, newsId, createdBy) == ErrorMapping.ErrorCodes.Success)
        //    {
        //        responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
        //        responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
        //    }
        //    else
        //    {
        //        responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
        //        responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
        //    }
        //    return responseData;
        //}

        public AlbumPublishedEntity GetAlbumPublishedByAlbumPublishedId(long albumPublishedId)
        {
            return AlbumBo.GetAlbumPublishedByAlbumPublishedId(albumPublishedId);
        }

        public AlbumPublishedEntity[] GetAlbumPublishedByAlbumId(long albumId)
        {
            return AlbumBo.GetAlbumPublishedByAlbumId(albumId).ToArray();
        }

        #endregion

        #region Photo label

        public WcfActionResponse InsertPhotoLabel(PhotoLabelEntity videoLabel)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoLabelId = 0;

            if (PhotoLabelBo.InsertPhotoLabel(videoLabel, ref videoLabelId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoLabelId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoLabel(PhotoLabelEntity videoLabel)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoLabelBo.UpdatePhotoLabel(videoLabel) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoLabel.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeletePhotoLabel(int videoLabelId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoLabelBo.DeletePhotoLabel(videoLabelId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoLabelId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoLabelEntity GetPhotoLabelByPhotoLabelId(int videoLabelId)
        {
            return PhotoLabelBo.GetPhotoLabelByPhotoLabelId(videoLabelId);
        }

        public PhotoLabelEntity[] GetPhotoLabelByParentLabelId(int parentLabelId, string createdBy)
        {
            return PhotoLabelBo.GetPhotoLabelByParentLabelId(parentLabelId, createdBy).ToArray();
        }

        #endregion


        #region Photo Folder

        public WcfActionResponse InsertPhotoFolder(PhotoFolderEntity videoFolder)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoFolderId = 0;

            if (PhotoFolderBo.InsertPhotoFolder(videoFolder, ref videoFolderId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolderId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoFolder(PhotoFolderEntity videoFolder)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoFolderBo.UpdatePhotoFolder(videoFolder) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolder.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeletePhotoFolder(int videoFolderId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoFolderBo.DeletePhotoFolder(videoFolderId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoFolderId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoFolderEntity GetPhotoFolderByPhotoFolderId(int videoFolderId)
        {
            return PhotoFolderBo.GetPhotoFolderByPhotoFolderId(videoFolderId);
        }

        public PhotoFolderEntity[] GetPhotoFolderByParentId(int parentId, string createdBy)
        {
            return PhotoFolderBo.GetPhotoFolderByParentId(parentId, createdBy).ToArray();
        }

        public PhotoFolderEntity[] PhotoFolderSearch(int parentId, string name, string createdBy, int status)
        {
            return PhotoFolderBo.PhotoFolderSearch(parentId, name, createdBy, status).ToArray();
        }

        public List<PhotoFolderEntity> GetFolderWithTreeView(string createBy)
        {
            return PhotoFolderBo.GetAllFolderWithTreeView(createBy);
        }

        #endregion

        #endregion

        #region PhotoGroup
        public WcfActionResponse InsertPhotoGroupDetailInPhotoTag(PhotoGroupDetailInPhotoTagEntity video)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoId = 0;

            if (PhotoGroupBo.InsertPhotoGroupDetailInPhotoTag(video) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse InsertPhotoGroupDetail(PhotoGroupDetailEntity video)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var videoId = 0;

            if (PhotoGroupBo.InsertPhotoGroupDetail(video, ref videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdatePhotoGroupDetail(PhotoGroupDetailEntity video)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoGroupBo.UpdatePhotoGroupDetail(video) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = video.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }
        public WcfActionResponse UpdatePhotoGroupDetailStatus(int id, EnumPhotoGroupDetailStatus status)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoGroupBo.UpdatePhotoGroupDetailStatus(id, status) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeletePhotoGroupDetail(int videoId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoGroupBo.DeletePhotoGroupDetail(videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeletePhotoGroupDetailInPhotoTag(int videoId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (PhotoGroupBo.DeletePhotoGroupDetailInPhotoTag(videoId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = videoId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public PhotoGroupDetailEntity GetPhotoGroupDetail(int videoId)
        {
            return PhotoGroupBo.GetPhotoGroupDetailById(videoId);
        }

        public List<PhotoGroupDetailInZonePhotoEntity> GetZonePhotoByPhotoId(int id)
        {
            return ZonePhotoBo.GetZonePhotoByPhotoId(id);
        }

        public PhotoGroupDetailEntity[] SearchPhotoGroupDetail(string keyword, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, EnumPhotoGroupDetailStatus status, int isHot, int videoGroupId, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                PhotoGroupBo.SearchPhotoGroupDetail(keyword, zoneId, fromDate, toDate, createdBy, status, isHot, videoGroupId, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public PhotoGroupDetailCountEntity[] CountPhotoGroupDetail(string username)
        {
            var count = PhotoGroupBo.CountPhotoGroupDetail(username);
            if (count != null) return count.ToArray();
            return
                new PhotoGroupDetailCountEntity[0];
        }

        public PhotoGroupEntity[] GetAllPhotoGroup(ref int totalRow)
        {
            return
                PhotoGroupBo.GetAllPhotoGroup(ref totalRow).ToArray();
        }

        #endregion

        #region EmbedAlbum

        public List<EmbedAlbumEntity> SearchEmbedAlbum(string keyword, int type, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return EmbedAlbumBo.SearchEmbedAlbum(keyword, type, zoneId, status, pageIndex, pageSize, ref totalRow);
        }
        public EmbedAlbumForEditEntity GetEmbedAlbumForEditByEmbedAlbumId(int embedAlbumId, int topPhoto)
        {
            return EmbedAlbumBo.GetEmbedAlbumForEditByEmbedAlbumId(embedAlbumId, topPhoto);
        }

        public WcfActionResponse CreateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit)
        {
            var errorCode = EmbedAlbumBo.CreateEmbedAlbum(embedAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse CreateEmbedAlbumReturnId(EmbedAlbumForEditEntity embedAlbumForEdit, ref int id)
        {
            var errorCode = EmbedAlbumBo.CreateEmbedAlbumReturnId(embedAlbumForEdit, ref id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse UpdateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit)
        {
            var errorCode = EmbedAlbumBo.UpdateEmbedAlbum(embedAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse DeleteEmbedAlbum(int embedAlbumId)
        {
            var errorCode = EmbedAlbumBo.DeleteEmbedAlbum(embedAlbumId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region FileUpload

        public WcfActionResponse InsertFileUpload(FileUploadEntity fileUpload)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            var fileUploadId = 0;

            if (FileUploadBo.InsertFileUpload(fileUpload, ref fileUploadId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = fileUploadId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse UpdateFileUpload(FileUploadEntity fileUpload)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FileUploadBo.UpdateFileUpload(fileUpload) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = fileUpload.Id.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public WcfActionResponse DeleteFileUpload(int fileUploadId)
        {
            var responseData = WcfActionResponse.CreateSuccessResponse();

            if (FileUploadBo.DeleteFileUpload(fileUploadId) == ErrorMapping.ErrorCodes.Success)
            {
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                responseData.Data = fileUploadId.ToString(CultureInfo.InvariantCulture);
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                responseData.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                responseData.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return responseData;
        }

        public FileUploadEntity GetFileUpload(int fileUploadId)
        {
            return FileUploadBo.GetFileUploadById(fileUploadId);
        }

        public FileUploadEntity[] SearchFileUpload(string keyword, int zoneId, string ext, string uploadedBy, EnumFileUploadStatus status, int pageIndex, int pageSize, ref int totalRows)
        {
            return
                FileUploadBo.SearchFileUpload(keyword, zoneId, ext, uploadedBy, status, pageIndex, pageSize, ref totalRows).ToArray();
        }


        public FileUploadExtEntity[] GetAllExt()
        {
            return
                 FileUploadBo.GetAllExt().ToArray();
        }

        #endregion

        #region VideoExtension

        public VideoExtensionEntity GetVideoExtensionValue(long videoId, EnumVideoExtensionType type)
        {
            return VideoBo.GetVideoExtensionValue(videoId, type);
        }
        public int GetVideoExtensionMaxValue(long videoId, EnumVideoExtensionType type)
        {
            return VideoBo.GetVideoExtensionMaxValue(videoId, (int)type);
        }
        public WcfResponseData SetVideoExtensionValue(long videoId, EnumVideoExtensionType type, string value)
        {
            var errorCode = VideoBo.SetVideoExtensionValue(videoId, type, value);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfResponseData.CreateSuccessResponse();
            }
            return WcfResponseData.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfResponseData DeleteVideoExtensionByVideoId(long videoId)
        {
            var errorCode = VideoBo.DeleteVideoExtensionByVideoId(videoId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfResponseData.CreateSuccessResponse();
            }
            return WcfResponseData.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<VideoExtensionEntity> GetVideoExtensionByVideoId(long videoId)
        {
            return VideoBo.GetVideoExtensionByVideoId(videoId);
        }
        public List<VideoExtensionEntity> GetVideoExtensionByListVideoId(string listVideoId)
        {
            return VideoBo.GetVideoExtensionByListVideoId(listVideoId);
        }
        public List<VideoExtensionEntity> GetVideoExtensionByTypeAndVaue(EnumVideoExtensionType type, string value)
        {
            return VideoBo.GetVideoExtensionByTypeAndVaue(type, value);
        }

        #endregion

        public VideoEntity[] SearchVideoFromExternalChannel(string username, int zoneVideoId, int playlistId, string keyword, EnumVideoStatus status, EnumVideoSortOrder order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            return
                VideoBo.SearchExternalVideo(username, zoneVideoId, playlistId, keyword, (int)status, (int)order, fromDate, toDate, mode,
                               pageIndex, pageSize, ref totalRow).ToArray();
        }

        public ZoneVideoEntity[] GetVideoZoneListAsTreeviewFromExternalChannel(int parentId, EnumZoneVideoStatus status, string prefix)
        {
            var zoneList = ZoneVideoBo.GetListByParentIdExternalCms(parentId, (int)status);
            return ZoneVideoBo.BuildTree(zoneList, prefix).ToArray();
        }

        public WcfActionResponse UpdateVideoInNews(string videoInNews, long newsId)
        {
            try
            {
                var result = VideoInNewsBo.UpdateVideoInNews(videoInNews, newsId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(result.ToString(),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoxVideoEmbedEntity[] GetExternalListVideoEmbed(int zoneId, int type)
        {
            return VideoBo.GetExternalListVideoEmbed(zoneId, type).ToArray();
        }

        public string GetErrorMessage(ErrorMapping.ErrorCodes error)
        {
            return ErrorMapping.Current[error];
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        public VideoEntity GetDetailExternalVideo(int videoId)
        {
            var zoneName = "";
            return VideoBo.GetById(videoId, ref zoneName);
        }

        #region Video Distribution
        public VideoEntity[] SearchVideoDistribution(string zoneVideoIds, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoBo.SearchVideoDistribution(zoneVideoIds, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public VideoEntity GetDetailVideoDistribution(int videoId)
        {
            var zoneName = "";
            return VideoBo.VideoDistributionGetById(videoId, ref zoneName);
        }
        #endregion


        public List<ZoneVideoEntity> GetActiveZoneInDay(string zoneIds, DateTime viewDate, int mode)
        {
            return ZoneVideoBo.GetActiveZoneInDay(zoneIds, viewDate, mode);
        }

        public List<ZoneVideoEntity> GetActiveZoneInDayV2(string zoneIds, DateTime viewDate, int mode)
        {
            return ZoneVideoBo.GetActiveZoneInDayV2(zoneIds, viewDate, mode);
        }

        public WcfActionResponse SaveVideoWithoutPermission(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.SaveWithoutPermission(videoEntity, tagIds, zoneIdList, playlistIdList, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(result.ToString(),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<VideoModeNotifyEntity> GetNotifyZoneInDay(DateTime viewDate)
        {
            return ZoneVideoBo.GetNotifyZoneInDay(viewDate);
        }

        public PlaylistGroupEntity GetPlaylistGroupByPlaylistId(int playlistId)
        {
            return VideoBo.GetPlaylistGroupByPlaylistId(playlistId);
        }

        public WcfActionResponse InsertPlaylistInVideoGroup(PlaylistInPlaylistGroupEntity playlistGroup)
        {
            try
            {
                if (VideoBo.InsertPlaylistInVideoGroup(playlistGroup) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<PlaylistGroupEntity> GetPlaylistGroupByParentId(int playlistId)
        {
            return VideoBo.GetPlaylistGroupByParentId(playlistId);
        }

        public List<VideoEntity> SearchByPlaylistGroup(int group, int zone)
        {
            return
                VideoBo.SearchByPlaylistGroup(group, zone);
        }
        public WcfActionResponse Playlist_Update_DistributionDate(string playlistIds)
        {
            try
            {
                if (PlaylistBo.Playlist_Update_DistributionDate(playlistIds) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public PlaylistGroupEntity PlaylistGroup_Current(DateTime fromDate, DateTime toDate)
        {
            return PlaylistBo.PlaylistGroup_Current(fromDate, toDate);
        }
        #region Video EPL
        public WcfActionResponse Video_Insert_By_Feed(VideoEntity videoEntity, string tagIds, int MapPlayListTime, string zoneList)
        {
            try
            {
                var videoId = 0;
                var result = VideoBo.Insert_By_Feed(videoEntity, tagIds, zoneList, MapPlayListTime, ref videoId);
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse(videoId.ToString(CultureInfo.CurrentCulture),
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                else if (videoId <= 0)
                    return WcfActionResponse.CreateSuccessResponse("", ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                return WcfActionResponse.CreateErrorResponse((int)result,
                                                             ErrorMapping.Current[result]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Video_Insesert_ZoneRecut(int videoId)
        {
            try
            {
                if (VideoBo.Video_Insesert_ZoneRecut(videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Video_UpdateZonePrimary(int videoId, int ZoneId)
        {
            try
            {
                if (VideoBo.Video_UpdateZonePrimary(videoId, ZoneId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Video_InsertZone(int videoId, string ZoneIds)
        {
            try
            {
                if (VideoBo.Video_InsertZone(videoId, ZoneIds) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public WcfActionResponse Playlist_GetIdByZoneId(int zoneId1, int zoneId2, DateTime DateFrom, DateTime DateTo, int VideoId, ref int PlayListId)
        {
            try
            {
                if (VideoBo.Playlist_GetIdByZoneId(zoneId1, zoneId2, DateFrom, DateTo, VideoId, ref PlayListId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public List<ZoneVideoEntity> ZoneVideo_GetAll()
        {
            return VideoBo.ZoneVideo_GetAll();
        }
        public List<EPLPlayListEntity> EPLPlayListGetAll()
        {
            return VideoEPLBo.EPLPlayListGetAll();
        }

        public List<EPLPlayListEntity> EPLPlayListGetByUser(string userName)
        {
            return VideoEPLBo.EPLPlayListGetByUser(userName);
        }

        public WcfActionResponse InsertVideoToEPLPlaylist(string userName, int videoId)
        {
            try
            {
                if (VideoEPLBo.Insert(userName, videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteVideoFromEPLPlaylist(int userId, int videoId)
        {
            try
            {
                if (VideoEPLBo.Delete(userId, videoId) == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse("",
                                                                   ErrorMapping.Current[ErrorMapping.ErrorCodes.Success]);
                }
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UnknowError,
                                                             ErrorMapping.Current[
                                                                 ErrorMapping.ErrorCodes.UnknowError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        #endregion
    }
}
