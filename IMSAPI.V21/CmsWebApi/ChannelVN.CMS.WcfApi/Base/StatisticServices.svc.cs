﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Base.ServiceContracts;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;
using LogCMS.DAL;
using StatisticEntity;
using ChannelVN.CMS.BO.Base.Statistic;

namespace ChannelVN.CMS.WcfApi.Base
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class StatisticServices : IStatisticServices
    {
        #region  StatisticV3
        public List<StatisticV3ViewCountEntity> ListPageViewAll(Int64 fromDate, Int64 toDate)
        {
            return NewsStatisticV3Bo.ListPageViewAll(fromDate, toDate);
        }
        public List<StatisticV3CategoryEntity> ListCategoryViewCount(string cateId, Int64 fromDate, Int64 toDate)
        {
            return NewsStatisticV3Bo.ListCategoryViewCount(cateId, fromDate, toDate);
        }
        public List<StatisticV3ViewCountEntity> ListPageViewAllByChannel(ChannelId channel, Int64 fromDate, Int64 toDate)
        {
            return NewsStatisticV3Bo.ListPageViewAll(channel, fromDate, toDate);
        }

        public List<StatisticV3CategoryEntity> ListCategoryViewCountByChannel(ChannelId channel, string cateId, Int64 fromDate, Int64 toDate)
        {
            return NewsStatisticV3Bo.ListCategoryViewCount(channel, cateId, fromDate, toDate);
        }

        public List<StatisticV3CategoryEntity> ListCategoryViewCountSummaryByChannel(ChannelId channel, string cateId, Int64 fromDate, Int64 toDate)
        {
            return NewsStatisticV3Bo.ListCategoryViewCountSummary(channel, cateId, fromDate, toDate);
        }

        public List<StatisticV3ViewCountEntity> ListPageViewAllMobileByChannel(ChannelId channel, Int64 fromDate, Int64 toDate)
        {
            return NewsStatisticV3Bo.ListPageViewAllMobile(channel, fromDate, toDate);
        }

        public List<StatisticV3CategoryEntity> ListCategoryViewCountMobileByChannel(ChannelId channel, string cateId, Int64 fromDate, Int64 toDate)
        {
            return NewsStatisticV3Bo.ListCategoryViewCountMobile(channel, cateId, fromDate, toDate);
        }

        public List<StatisticV3TagVisitEntity> ListTagViewCountByChannel(StatisticTagChannel channel, Int64 fromDate, Int64 toDate)
        {
            return TagBo.GetTagVisitByDay(Enum.GetName(typeof(StatisticTagChannel), channel), fromDate, toDate);
        }

        #endregion

        #region Thống kê lượng truy cập

        private static MainDB db;
        private StoredProcedures storedProcedures = new StoredProcedures(db);



        public StatisticDomainEntity[] GetListStatisticDomainDateNow()
        {
            try
            {
                return storedProcedures.GetListStatisticDomainDateNow().ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new StatisticDomainEntity[] { };
            }
        }



        public StatisticDomainEntity[] GetListStatisticDomainByDate(string fromDate, string toDate)
        {
            try
            {
                return storedProcedures.GetListStatisticDomainByDate(fromDate, toDate).ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new StatisticDomainEntity[] { };
            }
        }


        public StatisticEntity.StatisticEntity[] GetListStatisticAllCateDateNow()
        {
            try
            {
                return storedProcedures.GetListStatisticAllCateDateNow().ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new StatisticEntity.StatisticEntity[] { };
            }
        }


        public StatisticEntity.StatisticEntity[] GetListStatisticCateByDate(string cateId, string fromDate, string toDate)
        {
            try
            {
                return storedProcedures.GetListStatisticCateByDate(cateId, fromDate, toDate).ToArray();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new StatisticEntity.StatisticEntity[] { };
            }
        }
        #endregion

        #region Thống kê sản lượng bài viết

        public NewsStatisticEntity[] GetStatisticStatusNewsByAccount(string account, int status, string startDate, string endDate)
        {
            return
                NewsStatisticBo.GetStatisticStatusNewsByAccount(account, status,
                                                                Convert.ToDateTime(startDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")),
                                                                Convert.ToDateTime(endDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")))
                    .ToArray();
        }

        public NewsStatisticEntity[] GetStatisticStatusNewsByAccountAdmin(string createdBy, int status, string startDate, string endDate)
        {
            return
                NewsStatisticBo.GetStatisticStatusNewsByAccountAdmin(createdBy, status,
                                                                     Convert.ToDateTime(startDate,
                                                                                        CultureInfo.GetCultureInfo(
                                                                                            "en-gb")),
                                                                     Convert.ToDateTime(endDate,
                                                                                        CultureInfo.GetCultureInfo(
                                                                                            "en-gb"))).ToArray();
        }


        public NewsStatisticTotalProduction[] GetStatisticNewsTotalProduction(string zoneId, string startDate, string endDate)
        {
            return
                NewsStatisticBo.GetStatisticNewsTotalProduction(zoneId,
                                                                Convert.ToDateTime(startDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")),
                                                                Convert.ToDateTime(endDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")))
                    .ToArray();
        }
        public ViewCountByUserEntity[] GetUserViewCount(string startDate, string endDate)
        {
            return
                NewsStatisticBo.GetUserViewCount(Convert.ToDateTime(startDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")),
                                                                Convert.ToDateTime(endDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")))
                    .ToArray();
        }

        public NewsStatisticTotalProduction[] GetStatisticNewsTotalProductionUser(string zoneId, string startDate, string endDate, string userName)
        {
            return
                NewsStatisticBo.GetStatisticNewsTotalProductionUser(zoneId,
                                                                Convert.ToDateTime(startDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")),
                                                                Convert.ToDateTime(endDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")), userName)
                    .ToArray();
        }

        public NewsStatisticSourceEntity[] GetStatisticNewsBySourceId(int sourceId, string startDate, string endDate)
        {
            return
                NewsStatisticBo.GetStatisticNewsBySourceId(sourceId,
                                                                Convert.ToDateTime(startDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")),
                                                                Convert.ToDateTime(endDate,
                                                                                   CultureInfo.GetCultureInfo("en-gb")))
                    .ToArray();
        }

        #endregion
        
        public NewsStatisticTotalProduction[] GetStatisticNewsComment(string zoneId, DateTime startDate, DateTime endDate)
        {
            return
                NewsStatisticBo.GetStatisticNewsComment(zoneId, startDate, endDate)
                    .ToArray();
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }

        #region PageView Process
        public WcfActionResponse UpdateZoneViewByDay(PageViewByZoneEntity updateObject)
        {
            WcfActionResponse objResult = new WcfActionResponse();
            try
            {
                objResult.Success = PageViewStatisticBo.PageViewByZone_UpdateByDay(updateObject);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                objResult.Success = false;
            }
            return objResult;
        }

        public WcfActionResponse GetLastUpdateZoneViewRun()
        {
            WcfActionResponse objResult = new WcfActionResponse();
            try
            {
                objResult.Data = PageViewStatisticBo.GetLastestRun().ToString("dd/MM/yyyy hh:mm:ss");
                return objResult;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                objResult.Success = false;
            }
            return objResult;
        }

        public WcfActionResponse UpdateAllSiteViewByDay(PageViewAllSiteEntity updateObject)
        {
            WcfActionResponse objResult = new WcfActionResponse();
            try
            {
                objResult.Success = PageViewStatisticBo.PageViewAllSite_UpdateByDay(updateObject);
                return objResult;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                objResult.Success = false;
            }
            return objResult;
        }

        public WcfActionResponse PageViewAllSiteGetData(DateTime dateFrom,DateTime dateTo, string channelName)
        {
            WcfActionResponse objResult = new WcfActionResponse();
            try
            {
                ChannelEntity objChannel = PageViewStatisticBo.GetChannel_ByName(channelName);
                objResult.Data = NewtonJson.Serialize(PageViewStatisticBo.PageViewAllSite_GetData(dateFrom, dateTo, objChannel.ChannelID));
                objResult.Success = true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                objResult.Success = false;
            }
            return objResult;
        }

        public WcfActionResponse PageViewByZoneGetData(DateTime dateFrom, DateTime dateTo,string zoneIds, string channelName)
        {
            WcfActionResponse objResult = new WcfActionResponse();
            try
            {
                ChannelEntity objChannel = PageViewStatisticBo.GetChannel_ByName(channelName);
                objResult.Data = NewtonJson.Serialize(PageViewStatisticBo.PageViewByZone_GetData(dateFrom, dateTo,zoneIds, objChannel.ChannelID));
                objResult.Success = true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                objResult.Success = false;
            }
            return objResult;
        }

        public ChannelEntity GetChannelByName(string channelName)
        {
            ChannelEntity objResult = new ChannelEntity();
            try
            {
                objResult = PageViewStatisticBo.GetChannel_ByName(channelName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return objResult;
        }
        #endregion
    }
}
