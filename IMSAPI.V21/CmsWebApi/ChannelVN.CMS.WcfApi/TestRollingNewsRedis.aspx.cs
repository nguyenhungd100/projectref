﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChannelVN.CMS.BO.Base.RollingNews;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.RedisClientHelper;

namespace ChannelVN.CMS.WcfApi
{
    public partial class TestRollingNewsRedis : System.Web.UI.Page
    {
        public class RollingNewsEventInfo
        {
            public int Id { get; set; }
            public string EventContent { get; set; }
            public DateTime EventTime { get; set; }
            public int EventType { get; set; }
            public bool IsFocus { get; set; }
            public string EventNote { get; set; }
            public string MobileContent { get; set; }
            public string ShortContent { get; set; }
            public string Images { get; set; }
            public int ImageCount { get; set; }
        }

        private int DbNumber
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["db"])) return -1;
                return Utility.ConvertToInt(Request.QueryString["db"]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadData();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void LoadData()
        {
            var rollingNewsId = Request.QueryString["rollingNewsId"];
            var allDataKey = string.Format("CmsRollingNewsData[{0}]", rollingNewsId);
            var focusDataKey = string.Format("CmsRollingNewsHotData[{0}]", rollingNewsId);
            var statusDataKey = string.Format("CmsRollingNewsStatus[{0}]", rollingNewsId);

            var redisClient = new RedisStackClient(DbNumber);
            var data = redisClient.Get<List<RollingNewsEventInfo>>(allDataKey);
            grvAllEvent.DataSource = data;
            grvAllEvent.DataBind();

            var focus = redisClient.Get<List<RollingNewsEventInfo>>(focusDataKey);
            grvHotEvent.DataSource = focus;
            grvHotEvent.DataBind();

            ltrStatus.Text = redisClient.Get<int>(statusDataKey).ToString();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                var rollingNewsId = Utility.ConvertToInt(Request.QueryString["rollingNewsId"]);

                if (rollingNewsId > 0)
                {
                    RollingNewsBo.UpdateRollingNewsEventIntoRedis(rollingNewsId);

                    LoadData();
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var rollingNewsId = Request.QueryString["rollingNewsId"];
                var allDataKey = string.Format("CmsRollingNewsData[{0}]", rollingNewsId);
                var focusDataKey = string.Format("CmsRollingNewsHotData[{0}]", rollingNewsId);
                var statusDataKey = string.Format("CmsRollingNewsStatus[{0}]", rollingNewsId);

                var redisClient = new RedisStackClient(DbNumber);
                redisClient.Remove(allDataKey);
                redisClient.Remove(focusDataKey);
                redisClient.Remove(statusDataKey);
            }
            catch (Exception ex)
            {
            }
        }
    }
}