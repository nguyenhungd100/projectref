﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;

namespace ChannelVN.CMS.WcfApi.Common
{
    [DataContract]
    public class ErrorMessageEntity
    {
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
    public static class EnumHelper
    {
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            var knownTypes = new List<Type>
                                 {
                                     typeof (Entity.ErrorCode.ErrorMapping.ErrorCodes),
                                     typeof (DynamicLayout.Entity.ErrorCode.ErrorMapping.ErrorCodes),
                                     typeof (SEO.Entity.ErrorCode.ErrorMapping.ErrorCodes),
                                     typeof (Emailer.Entity.ErrorCode.ErrorMapping.ErrorCodes),
                                     typeof (SocialNetwork.Entity.ErrorCode.ErrorMapping.ErrorCodes)
                                 };
            return knownTypes;
        }
    } 
}