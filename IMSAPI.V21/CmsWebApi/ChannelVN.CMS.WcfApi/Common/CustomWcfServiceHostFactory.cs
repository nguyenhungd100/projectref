﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace ChannelVN.CMS.WcfApi.Common
{
    public class CustomWcfServiceHostFactory : WebServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            var host = new CustomWcfServiceHost(serviceType, baseAddresses);

            return host;
        }
    }
}