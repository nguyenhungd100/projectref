﻿using System.ServiceModel;

namespace ChannelVN.CMS.WcfApi.Common
{
    [ServiceContract]
    public interface IErrorMessageMapping
    {
        [OperationContract]
        ErrorMessageEntity[] GetErrorMessage();
    }
}
