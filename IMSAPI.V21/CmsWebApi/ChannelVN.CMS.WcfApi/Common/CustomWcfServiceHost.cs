﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;

namespace ChannelVN.CMS.WcfApi.Common
{
    public class CustomWcfServiceHost : WebServiceHost
    {
        public CustomWcfServiceHost()
        {
        }

        public CustomWcfServiceHost(object singletonInstance, params Uri[] baseAddresses)
            : base(singletonInstance, baseAddresses)
        {
        }

        public CustomWcfServiceHost(Type serviceType, params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
        }



        protected override void OnOpening()
        {
            base.OnOpening();

            foreach (var baseAddress in BaseAddresses)
            {
                var found = false;
                foreach (var se in Description.Endpoints)
                    if (se.Address.Uri == baseAddress)
                    {
                        found = true;
                        ((WebHttpBinding)se.Binding).ReaderQuotas.MaxStringContentLength = Int32.MaxValue;//here you set it and also set it below
                    }
                if (!found)
                {
                    if (ImplementedContracts.Count > 1)
                        throw new InvalidOperationException("Service '" + Description.ServiceType.Name + "' implements multiple ServiceContract types, and no endpoints are defined in the configuration file. WebServiceHost can set up default endpoints, but only if the service implements only a single ServiceContract. Either change the service to only implement a single ServiceContract, or else define endpoints for the service explicitly in the configuration file. When more than one contract is implemented, must add base address endpoint manually");
                    var enumerator = ImplementedContracts.Values.GetEnumerator();
                    enumerator.MoveNext();
                    var contractType = enumerator.Current.ContractType;
                    var binding = new WebHttpBinding
                                      {
                                          ReaderQuotas =
                                              {
                                                  MaxStringContentLength = Int32.MaxValue,
                                                  MaxArrayLength = Int32.MaxValue,
                                                  MaxBytesPerRead = Int32.MaxValue,
                                                  MaxDepth = Int32.MaxValue,
                                                  MaxNameTableCharCount = Int32.MaxValue
                                              },
                                          MaxBufferPoolSize = Int32.MaxValue,
                                          MaxBufferSize = Int32.MaxValue,
                                          MaxReceivedMessageSize = Int32.MaxValue
                                      };
                    AddServiceEndpoint(contractType, binding, baseAddress);
                }

            }

            foreach (var se in Description.Endpoints)
                if (se.Behaviors.Find<WebHttpBehavior>() == null)
                    se.Behaviors.Add(new WebHttpBehavior());

            // disable help page.
            var serviceDebugBehavior = Description.Behaviors.Find<ServiceDebugBehavior>();
            if (serviceDebugBehavior != null)
            {
                serviceDebugBehavior.HttpHelpPageEnabled = false;
                serviceDebugBehavior.HttpsHelpPageEnabled = false;
            }
        }
    }
}