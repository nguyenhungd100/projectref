﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestCommentCount.aspx.cs" Inherits="ChannelVN.CMS.WcfApi.TestCommentCount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <br />
        Channel:
        <asp:DropDownList ID="ddlChannel" runat="server">
            <asp:ListItem Value="1">SohaNews</asp:ListItem>
            <asp:ListItem Value="2">GenK</asp:ListItem>
            <asp:ListItem Value="3">TimDiemThi</asp:ListItem>
            <asp:ListItem Value="4">AutoPro</asp:ListItem>
            <asp:ListItem Value="5">SucKhoeDoiSong</asp:ListItem>
            <asp:ListItem Value="6">NguoiLaoDong</asp:ListItem>
            <asp:ListItem Value="7">GameK</asp:ListItem>
            <asp:ListItem Value="8">Kenh14</asp:ListItem>
            <asp:ListItem Value="9">Afamily</asp:ListItem>
            <asp:ListItem Value="10">VnEconomy</asp:ListItem>
            <asp:ListItem Value="11">GiaDinhNet</asp:ListItem>
            <asp:ListItem Value="12">CafeF</asp:ListItem>
            <asp:ListItem Value="13">CafeBiz</asp:ListItem>
            <asp:ListItem Value="14">DanTri</asp:ListItem>
        </asp:DropDownList>
        <br />
        Comment type:
        <asp:DropDownList ID="ddlCommentType" runat="server">
            <asp:ListItem Value="1">News</asp:ListItem>
            <asp:ListItem Value="2">Video</asp:ListItem>
            <asp:ListItem Value="3">Vote</asp:ListItem>
        </asp:DropDownList>
        <br />
        NewsId:
        <asp:TextBox ID="txtNewsId" runat="server"></asp:TextBox>
        <asp:Button ID="btnSetCommentCount" runat="server" Text="Set comment count" OnClick="btnSetCommentCount_Click" />
        <asp:Label ID="lblSetFromDb" runat="server" Text=""></asp:Label>
        <br />
        <asp:Button ID="btnGetFromRedis" runat="server" Text="Get comment count" OnClick="btnGetFromRedis_Click" />
        <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
        <br />
        Set Comment count From Date <asp:TextBox ID="txtSetCommentCountFromDate" runat="server"></asp:TextBox>
        To Date<asp:TextBox ID="txtSetCommentCountToDate" runat="server"></asp:TextBox>
        <asp:Button ID="btnSetCommentCountFromDateToDate" runat="server" Text="Set comment count from date To date" OnClick="btnSetCommentCountFromDateToDate_Click" />
    </form>
</body>
</html>
