﻿using System.ServiceModel;
using ChannelVN.CMS.Entity.External.Kenh14.HotNewsConfig;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.External.Kenh14.ServiceContracts
{
    [ServiceContract]
    public interface IHotNewsConfigServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse UpdateHotNewsConfig(HotNewsConfigEntity hotNewsConfigEntity);
        [OperationContract]
        WcfActionResponse DeleteHotNewsConfig(int zoneId);
        [OperationContract]
        List<HotNewsConfigEntity> GetListHotNewsConfig();
    }
}
