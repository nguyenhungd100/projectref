﻿using System.ServiceModel;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.Kenh14.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.External.Kenh14.ServiceContracts
{
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions);
        [OperationContract]
        WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions);
        [OperationContract]
        List<NewsWithAnalyticEntity> GetNewsByListNewsId(string listNewsId);
        [OperationContract]
        WcfActionResponse UpdateSpecialNewsRelation(long newsId, int zoneId, string newsRelationSpecialId);
        #region News

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage(string listOfFocusPositionOnLastestNews);

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestNews);

        [OperationContract]
        WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar, string url, string sapo);

        [OperationContract]
        WcfActionResponse SetNewsPostitionInList(int currentType, int newType,
                                                 int position, long newsId, string accountName);

        #endregion
    }
}
