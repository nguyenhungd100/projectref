﻿using System.ServiceModel;
using ChannelVN.CMS.Entity.External.Kenh14.BoxTVShow;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.External.Kenh14.ServiceContracts
{
    [ServiceContract]
    public interface IBoxTVShowServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse UpdateBoxTVShow(BoxTVShowEntity boxTVShowEntity);
        [OperationContract]
        WcfActionResponse DeleteBoxTVShow(int id);
        [OperationContract]
        BoxTVShowEntity GetByIdBoxTVShow(int id);
        [OperationContract]
        List<BoxTVShowEntity> GetListBoxTVShow();
    }
}
