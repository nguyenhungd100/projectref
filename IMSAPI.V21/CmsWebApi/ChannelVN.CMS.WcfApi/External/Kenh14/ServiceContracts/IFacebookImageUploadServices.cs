﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.Kenh14.FacebookImageUpload;

namespace ChannelVN.CMS.WcfApi.External.Kenh14
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFacebookImageUploadServices" in both code and config file together.
    [ServiceContract]
    public interface IFacebookImageUploadServices
    {
        [OperationContract]
        List<FacebookImageUploadEntity> GetByNewsId(long newsId, int pageIndex, int pageSize, int status, ref int totalRow);
        [OperationContract]
        WcfActionResponse UpdateStatus(int id, int status);
    }
}
