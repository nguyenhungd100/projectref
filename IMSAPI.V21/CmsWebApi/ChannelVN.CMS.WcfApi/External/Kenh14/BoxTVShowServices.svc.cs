﻿
using System;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.External.Kenh14.BoxTVShow;
using ChannelVN.CMS.Entity.External.Kenh14.BoxTVShow;
using ChannelVN.CMS.WcfApi.External.Kenh14.ServiceContracts;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.External.Kenh14
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BoxTVShowServices : IBoxTVShowServices
    {
        public WcfActionResponse UpdateBoxTVShow(BoxTVShowEntity boxTVShowEntity)
        {
            try
            {
                var errorCode = BoxTVShowBo.UpdateBoxTVShow(boxTVShowEntity);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(boxTVShowEntity.Id.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse DeleteBoxTVShow(int id)
        {
            try
            {
                var errorCode = BoxTVShowBo.DeleteBoxTVShow(id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(id.ToString(), "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public BoxTVShowEntity GetByIdBoxTVShow(int id)
        {
            return BoxTVShowBo.GetByIdBoxTVShow(id);
        }

        public List<BoxTVShowEntity> GetListBoxTVShow()
        {
            return BoxTVShowBo.GetListBoxTVShow();
        }
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
