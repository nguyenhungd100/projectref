﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.External.Kenh14.FacebookImageUpload;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.Kenh14.FacebookImageUpload;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.External.Kenh14
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FacebookImageUploadServices : IFacebookImageUploadServices
    {
        public List<FacebookImageUploadEntity> GetByNewsId(long newsId, int pageIndex, int pageSize,int status, ref int totalRow)
        {
            List<FacebookImageUploadEntity> returnValue = FacebookImageUploadBo.GetByNewsId(newsId, pageIndex, pageSize,status, ref totalRow);
            return returnValue;
        }

        public WcfActionResponse UpdateStatus(int id, int status)
        {
            try
            {
                var errorCode = FacebookImageUploadBo.UpdateStatus(id,status);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse("", "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
    }
}
