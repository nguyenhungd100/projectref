﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.Photo;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.CafeF;
using ChannelVN.CMS.BO.External.CafeF.BoxExpertNewsEmbed;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.External.CafeBiz.BoxExpertNewsEmbed;
using ChannelVN.CMS.Entity.External.CafeF.BusinessNewsCrawler;
using ChannelVN.CMS.Entity.External.CafeF.Expert;
using ChannelVN.CMS.WcfApi.External.CafeF.ServiceContracts;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.ErrorCode;
using NewsAuthorBo = ChannelVN.CMS.BO.External.CafeF.NewsAuthorBo;

namespace ChannelVN.CMS.WcfApi.External.CafeF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CafeFServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CafeFServices.svc or CafeFServices.svc.cs at the Solution Explorer and start debugging.
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CafeFServices : ICafeFServices
    {
        public WcfActionResponse ExpertInsert(ExpertEntity expertEntity, ref int expertId)
        {
            var errorCode = NewsAuthorBo.Insert(expertEntity, ref expertId);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(expertId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public ExpertEntity ExpertGetById(int id)
        {
            return NewsAuthorBo.GetById(id);
        }

        public WcfActionResponse ExpertUpdate(ExpertEntity expertEntity)
        {
            var errorCode = NewsAuthorBo.Update(expertEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(expertEntity), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public ExpertEntity[] ListExpertSearch(string keyword)
        {
            return NewsAuthorBo.Search(keyword).ToArray();
        }

        public WcfActionResponse ExpertDelete(int id)
        {
            var errorCode = NewsAuthorBo.Delete(id);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public ExpertEntity ExpertGetByNewsId(int id)
        {
            return NewsAuthorBo.GetExpertByNewsId(id);
        }

        public WcfActionResponse UpdateExpertInNews(ExpertInNews expertEntity)
        {
            var errorCode = NewsAuthorBo.InsertExpertIntoNews(expertEntity);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public NewsDetailForEditEntity GetDetail(long newsId, string currentUsername)
        {
            return NewsAuthorBo.GetNewsForEditByNewsId(newsId, currentUsername);
        }

        #region BoxExpertNewsEmbed
        public List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type)
        {
            return BoxExpertNewsEmbedBo.GetListBoxExpertNewsEmbed(zoneId, type);
        }

        public WcfActionResponse InsertBoxExpertNewsEmbed(BoxExpertNewsEmbedEntity newsEmbedBox)
        {
            var errorCode = BoxExpertNewsEmbedBo.Insert(newsEmbedBox);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsEmbedBox.NewsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateBoxExpertNewsEmbed(string listNewsId, int zoneId, int type)
        {
            var errorCode = BoxExpertNewsEmbedBo.Update(listNewsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteBoxExpertNewsEmbed(long newsId, int zoneId, int type)
        {
            var errorCode = BoxExpertNewsEmbedBo.Delete(newsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion
        public NewsInListEntity[] SearchExpertNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, string excludeNewsIds, int newsType, ref int totalRows)
        {
            var news = NewsAuthorBo.SearchExpertNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize, ref totalRows);
            return news.ToArray();
        }
        public WcfActionResponse UpdateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit)
        {
            var errorCode = BO.External.CafeF.EmbedAlbumb.EmbedAlbumBo.UpdateEmbedAlbum(embedAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public WcfActionResponse CreateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit)
        {
            var errorCode = BO.External.CafeF.EmbedAlbumb.EmbedAlbumBo.CreateEmbedAlbum(embedAlbumForEdit);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public List<FeedBackToExpertArticleEntity> FeedBackToExpertArticleSearch(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return FeedBackToExpertArticleBo.Search(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FeedBackToExpertArticleEntity>();
            }
        }
        public WcfActionResponse FeedBackToExpertArticleUpdateStatus(int id, int status)
        {
            var errorCode = FeedBackToExpertArticleBo.UpdateStatus(id, status);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        public FeedBackToExpertArticleEntity GetFeedBackToExpertArticleById(int id)
        {
            return FeedBackToExpertArticleBo.GetById(id);
        }
        public List<BusinessNewsCrawlerSourceEntity> GetAllBusinessCrawlerSource()
        {
            return BusinessNewsBo.GetAll();
        }

        public List<BusinessNewsCrawlerDetailEntity> SearchBusinessNews(int source, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return BusinessNewsBo.SearchNews(source, status, pageIndex, pageSize, ref totalRow);
        }
        public BusinessNewsCrawlerDetailEntity GetBusinessNewsCrawlerById(long id)
        {
            return BusinessNewsBo.GetById(id);
        }
        public WcfActionResponse BusinessNewsCrawlUpdateStatus(long id, int status)
        {
            var errorCode = BusinessNewsBo.UpdateStatus(id, status);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse()
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
    }
}
