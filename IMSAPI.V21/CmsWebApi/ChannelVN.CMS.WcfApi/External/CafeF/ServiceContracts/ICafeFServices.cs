﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.External.CafeBiz.BoxExpertNewsEmbed;
using ChannelVN.CMS.Entity.External.CafeF.BusinessNewsCrawler;
using ChannelVN.CMS.Entity.External.CafeF.Expert;

namespace ChannelVN.CMS.WcfApi.External.CafeF.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICafeFServices" in both code and config file together.
    [ServiceContract]
    public interface ICafeFServices
    {
        [OperationContract]
        WcfActionResponse ExpertInsert(ExpertEntity expertEntity, ref int expertId);
        [OperationContract]
        ExpertEntity ExpertGetById(int id);
        [OperationContract]
        WcfActionResponse ExpertUpdate(ExpertEntity expertEntity);
        [OperationContract]
        ExpertEntity[] ListExpertSearch(string keyword);
        [OperationContract]
        WcfActionResponse ExpertDelete(int id);
        [OperationContract]
        ExpertEntity ExpertGetByNewsId(int id);
        [OperationContract]
        WcfActionResponse UpdateExpertInNews(ExpertInNews expertEntity);
        [OperationContract]
        NewsDetailForEditEntity GetDetail(long newsId, string currentUsername);
        #region BoxExpertNewsEmbed

        [OperationContract]
        List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type);

        [OperationContract]
        WcfActionResponse InsertBoxExpertNewsEmbed(BoxExpertNewsEmbedEntity newsEmbedBox);

        [OperationContract]
        WcfActionResponse UpdateBoxExpertNewsEmbed(string listNewsId, int zoneId, int type);

        [OperationContract]
        WcfActionResponse DeleteBoxExpertNewsEmbed(long newsId, int zoneId, int type);

        #endregion

        [OperationContract]
        NewsInListEntity[] SearchExpertNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition,
                                                          DateTime distributedDateFrom, DateTime distributedDateTo,
                                                          int pageIndex, int pageSize, string excludeNewsIds,
                                                          int newsType, ref int totalRows);

        [OperationContract]
        WcfActionResponse UpdateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit);
        [OperationContract]
        WcfActionResponse CreateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit);
        [OperationContract]
        List<FeedBackToExpertArticleEntity> FeedBackToExpertArticleSearch(string keyword, int status, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        WcfActionResponse FeedBackToExpertArticleUpdateStatus(int id, int status);
        [OperationContract]
        FeedBackToExpertArticleEntity GetFeedBackToExpertArticleById(int id);
        [OperationContract]
        List<BusinessNewsCrawlerSourceEntity> GetAllBusinessCrawlerSource();
        [OperationContract]
        List<BusinessNewsCrawlerDetailEntity> SearchBusinessNews(int source, int status, int pageIndex, int pageSize,
                 ref int totalRow);
        [OperationContract]
        BusinessNewsCrawlerDetailEntity GetBusinessNewsCrawlerById(long id);
        [OperationContract]
        WcfActionResponse BusinessNewsCrawlUpdateStatus(long id, int status);
    }
}
