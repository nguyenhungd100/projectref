﻿using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.CafeBiz.BoxExpertNewsEmbed;
using ChannelVN.CMS.Entity.External.CafeF.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Entity.External.CafeF.NewsGallery;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.External.CafeF.ServiceContracts
{
    [ServiceContract]
    interface INewsServices : IErrorMessageMapping
    {
        #region News position

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId);

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage();

        #endregion

        [OperationContract]
        WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar,
                                           string url);

        #region News Gallery
        [OperationContract]
        WcfActionResponse SaveNewsGallery(NewsGalleryEntity info);
        [OperationContract]
        WcfActionResponse SaveListNewsGallery(List<NewsGalleryEntity> lstInfo);
        [OperationContract]
        NewsGalleryEntity SelectNewsGalleryById(int id);
        [OperationContract]
        WcfActionResponse UpdateListNewsGallery(NewsGalleryEntity info);
        [OperationContract]
        WcfActionResponse DeleteNewsGallery(int id);
        [OperationContract]
        List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId);
        [OperationContract]
        List<NewsGalleryEntity> ListGalleryByNewsId(long newsId, int pageIndex, int pageSize, ref int totalRows);
        #endregion

        #region BoxExpertNewsEmbed

        [OperationContract]
        List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type);

        [OperationContract]
        WcfActionResponse InsertBoxExpertNewsEmbed(BoxExpertNewsEmbedEntity newsEmbedBox);

        [OperationContract]
        WcfActionResponse UpdateBoxExpertNewsEmbed(string listNewsId, int zoneId, int type);

        [OperationContract]
        WcfActionResponse DeleteBoxExpertNewsEmbed(long newsId, int zoneId, int type);

        #endregion
    }
}
