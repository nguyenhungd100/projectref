﻿using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.CafeF.BoxExpertNewsEmbed;
using ChannelVN.CMS.BO.External.CafeF.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeBiz.BoxExpertNewsEmbed;
using ChannelVN.CMS.Entity.External.CafeF.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.CafeF.ServiceContracts;
using ChannelVN.WcfExtensions;
using System;
using ChannelVN.CMS.Entity.External.CafeF.NewsGallery;
using ChannelVN.CMS.BO.External.CafeF.NewsGallery;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.External.CafeF
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage()
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId);
        }
        public WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar, string url)
        {
            var errorCode = NewsPositionBo.SaveLinkPosition(type, position, zoneId, title, avatar, url);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion
        #region News Gallery
        public WcfActionResponse SaveNewsGallery(NewsGalleryEntity info)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (NewsGalleryBo.SaveNewsGallery(info, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Lấy thông tin bài slide ảnh theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public NewsGalleryEntity SelectNewsGalleryById(int id)
        {
            return NewsGalleryBo.SelectNewsGalleryById(id);
        }
        /// <summary>
        /// Save một danh sách bài slide ảnh
        /// </summary>
        /// <param name="lstInfo"></param>
        /// <returns></returns>
        public WcfActionResponse SaveListNewsGallery(List<NewsGalleryEntity> lstInfo)
        {
            int id = 0;
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (NewsGalleryBo.SaveListNewsGallery(lstInfo, ref id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = id.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Cập nhật thông tin bài slide ảnh
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public WcfActionResponse UpdateListNewsGallery(NewsGalleryEntity info)
        {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (NewsGalleryBo.UpdateNewsGallery(info) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.Data = info.SlideId.ToString();
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Xóa tin bài slide theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WcfActionResponse DeleteNewsGallery(int id) {
            var actionResponse = WcfActionResponse.CreateSuccessResponse();
            if (NewsGalleryBo.DeleteNewsGallery(id) == ErrorMapping.ErrorCodes.Success)
            {
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.Success];
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                actionResponse.ErrorCode = (int)ErrorMapping.ErrorCodes.UnknowError;
                actionResponse.Message = ErrorMapping.Current[ErrorMapping.ErrorCodes.UnknowError];
            }
            return actionResponse;
        }
        /// <summary>
        /// Lấy danh sách tin slide theo newsId
        /// </summary>
        /// <param name="newsId">long</param>
        /// <returns>list data</returns>
        public List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId)
        {
            return new List<NewsGalleryEntity>(NewsGalleryBo.SelectNewsGalleryByNewsId(newsId));
        }

        public List<NewsGalleryEntity> ListGalleryByNewsId(long newsId, int pageIndex, int pageSize, ref int totalRows) { 
            return new List<NewsGalleryEntity>(NewsGalleryBo.SelectNewsGalleryByNewsId(newsId,pageIndex,pageSize,ref totalRows));
        }
        #endregion

        #region BoxExpertNewsEmbed
        public List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type)
        {
            return BoxExpertNewsEmbedBo.GetListBoxExpertNewsEmbed(zoneId, type);
        }

        public WcfActionResponse InsertBoxExpertNewsEmbed(BoxExpertNewsEmbedEntity newsEmbedBox)
        {
            var errorCode = BoxExpertNewsEmbedBo.Insert(newsEmbedBox);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsEmbedBox.NewsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse UpdateBoxExpertNewsEmbed(string listNewsId, int zoneId, int type)
        {
            var errorCode = BoxExpertNewsEmbedBo.Update(listNewsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(listNewsId, "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse DeleteBoxExpertNewsEmbed(long newsId, int zoneId, int type)
        {
            var errorCode = BoxExpertNewsEmbedBo.Delete(newsId, zoneId, type);
            return errorCode == ErrorMapping.ErrorCodes.Success
                       ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "")
                       : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
