﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.External.TheGuide.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.TheGuide.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.BO.Common;

namespace ChannelVN.CMS.WcfApi.External.TheGuide
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage()
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
        }


        public WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar, string url, string sapo)
        {
            var errorCode = NewsPositionBo.SaveLinkPosition(type, position, zoneId, title, avatar, url, sapo);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion
        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
