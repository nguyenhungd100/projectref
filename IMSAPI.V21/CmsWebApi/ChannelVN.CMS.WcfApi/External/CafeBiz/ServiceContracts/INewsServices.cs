﻿using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.CafeBiz.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Entity.External.CafeBiz.NewsGallery;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.External.CafeBiz.ServiceContracts
{
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping   
    {
        #region News position

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId);

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage();

        #endregion

        [OperationContract]
        WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar,
                                           string url);

        #region News Gallery
        [OperationContract]
        WcfActionResponse SaveNewsGallery(NewsGalleryEntity info);
        [OperationContract]
        WcfActionResponse SaveListNewsGallery(List<NewsGalleryEntity> lstInfo);
        [OperationContract]
        NewsGalleryEntity SelectNewsGalleryById(int id);
        [OperationContract]
        WcfActionResponse UpdateListNewsGallery(NewsGalleryEntity info);
        [OperationContract]
        WcfActionResponse DeleteNewsGallery(int id);
        [OperationContract]
        List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId);
        [OperationContract]
        List<NewsGalleryEntity> ListGalleryByNewsId(long newsId, int pageIndex, int pageSize, ref int totalRows);
        #endregion
    }
}
