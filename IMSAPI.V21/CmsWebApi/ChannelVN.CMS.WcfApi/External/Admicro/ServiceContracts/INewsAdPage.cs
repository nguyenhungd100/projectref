﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.ForExternalApplication.Entity;
using EnumAdPageForChannel = ChannelVN.ForExternalApplication.Entity.ChannelMappingForExternalApplication;

namespace ChannelVN.CMS.WcfApi.External.Admicro.ServiceContracts
{
    [ServiceContract]
    public interface INewsAdPage
    {
        [OperationContract]
        NewsForAdStoreEntity GetNewsById(EnumAdPageForChannel adPageForChannel, string secretKey, long newsId);

        [OperationContract]
        WcfActionResponse UpdateAdPageMode(EnumAdPageForChannel adPageForChannel, string secretKey, long newsId, EnumAdPageMode adPageMode);

        [OperationContract]
        List<NewsForAdStoreEntity> SearchAdPage(EnumAdPageForChannel adPageForChannel, string secretKey, string keyword, int zoneId, DateTime distributionDateFrom, DateTime distributionDateTo, int pageIndex, int pageSize, ref int totalRow);
    }
}
