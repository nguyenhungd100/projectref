﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.External.Admicro.ServiceContracts;
using ChannelVN.ForExternalApplication.Bo;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.ForExternalApplication.Entity.ErrorCode;
using EnumAdPageForChannel = ChannelVN.ForExternalApplication.Entity.ChannelMappingForExternalApplication;

namespace ChannelVN.CMS.WcfApi.External.Admicro
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsAdPage : INewsAdPage
    {
        public NewsForAdStoreEntity GetNewsById(EnumAdPageForChannel adPageForChannel, string secretKey, long newsId)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretKeyForExternalRequest"]))
            {
                return null;
            }
            return ForAdmicroBo.GetNewsForAdStoreById(adPageForChannel, newsId);
        }
        public WcfActionResponse UpdateAdPageMode(EnumAdPageForChannel adPageForChannel, string secretKey, long newsId, EnumAdPageMode adPageMode)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretKeyForExternalRequest"]))
            {
                return null;
            }
            try
            {
                var errorCode = ForAdmicroBo.UpdateAdStoreMode(adPageForChannel, newsId, adPageMode);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                       ? WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(ForAdmicroBo.GetNewsForAdStoreById(adPageForChannel, newsId)), "")
                                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                               ErrorMapping.Current[
                                                                                   errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        public List<NewsForAdStoreEntity> SearchAdPage(EnumAdPageForChannel adPageForChannel, string secretKey, string keyword, int zoneId, DateTime distributionDateFrom,
                                                          DateTime distributionDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretKeyForExternalRequest"]))
            {
                return null;
            }
            return ForAdmicroBo.SearchNewsForAdStore(adPageForChannel, keyword, zoneId, distributionDateFrom, distributionDateTo,
                                                      pageIndex, pageSize, ref totalRow);
        }
    }
}
