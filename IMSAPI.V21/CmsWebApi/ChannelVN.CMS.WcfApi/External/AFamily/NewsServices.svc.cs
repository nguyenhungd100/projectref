﻿using ChannelVN.AFamily.Bo;
using ChannelVN.AFamily.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.External.AFamily
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        public ErrorMessageEntity[] GetErrorMessage()
        {
            return new ErrorMessageEntity[] { };
        }

        public WcfActionResponse UpdateParentNewsId(long newsId, long id)
        {
            try
            {
                var errorCode = NewsBo.UpdateUpdateParentNewsId(newsId, id);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
    }
}
