﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.External.AFamily
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewsServices" in both code and config file together.
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse UpdateParentNewsId(long newsId, long id);
    }
}
