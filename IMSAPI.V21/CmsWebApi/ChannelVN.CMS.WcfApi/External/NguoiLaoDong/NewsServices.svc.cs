﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.NguoiLaoDong.News;
using ChannelVN.CMS.BO.External.NguoiLaoDong.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.NguoiLaoDong.ServiceContracts;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.External.NguoiLaoDong
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage()
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId);
        }
        public bool CheckNewsDistributionDateForLastestNews(long newsId, ref DateTime minValidDate)
        {
            return BoFactory.GetInstance<NewsPositionBo>().CheckNewsDistributionDateForLastestNews(newsId, ref minValidDate);
        }
        public bool RemoveNewsDisplayInSlide(string newsIds)
        {
            return BoFactory.GetInstance<NewsPositionBo>().RemoveNewsDisplayInSlide(newsIds);
        }

        #endregion

        public WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string newsRelationSpecialIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newEncryptNewsId = String.Empty;
                var newsId = 0L;
                var errorCode = NewsBo.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, newsRelationSpecialIdList, currentUsername, ref newsId, ref newEncryptNewsId, new List<string>(authorList), newsChildOrder, sourceId, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string newsRelationSpecialIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = NewsBo.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, newsRelationSpecialIdList, currentUsername, isRebuildLink, ref newsStatus, new List<string>(authorList), newsChildOrder, sourceId, publishedContent, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }


        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
