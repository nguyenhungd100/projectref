﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.NewsPosition;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.NewsWaitEntity;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.External.NguoiLaoDong.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewsServices" in both code and config file together.
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        #region News position

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage();

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId);

        [OperationContract]
        bool CheckNewsDistributionDateForLastestNews(long newsId, ref DateTime minValidDate);

        [OperationContract]
        bool RemoveNewsDisplayInSlide(string newsIds);

        #endregion

        [OperationContract]
        WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                     string tagIdListForSubtitle, string newsRelationIdList,
                                     string newsRelationSpecialIdList, string currentUsername, string[] authorList,
                                     int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions);

        [OperationContract]
        WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string newsRelationSpecialIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions);

    }
}
