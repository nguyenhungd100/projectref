﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Entity.External.Interactive.Security;

namespace ChannelVN.CMS.WcfApi.External.Interactive.ServiceContracts
{
    [ServiceContract]
    public interface ISecurityServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse UpdateUserPermissionByUserId(string encryptUserId, ChannelVN.CMS.Entity.Base.Security.UserPermissionEntity[] userPermission, bool isFullPermission, bool isFullZone, string accountName);

        #region User

        [OperationContract]
        WcfActionResponse AddNewUser(UserWithPermissionEntity userWithPermission);
        [OperationContract]
        WcfActionResponse UpdateUser(UserWithPermissionEntity userWithPermission, string accountName);
        [OperationContract]
        WcfActionResponse UpdateUserProfile(UserEntity user, string accountName);
        [OperationContract]
        WcfActionResponse AddnewUserProfile(UserEntity user, ref int userId);
        [OperationContract]
        WcfActionResponse UpdateAvatar(string accountName, string avatar);

        [OperationContract]
        WcfActionResponse ChangeStatus(int userId, UserStatus status);

        [OperationContract]
        UserEntity GetUserByUserId(string encryptUserId);
        [OperationContract]
        UserEntity GetUserByUsername(string username);

        [OperationContract]
        UserEntity GetUserByActiveCode(string activeCode);

        [OperationContract]
        UserWithPermissionDetailEntity GetUserWithPermissionDetailByUserId(string encryptUserId, bool isGetChildZone);
        [OperationContract]
        List<UserStandardEntity> SearchUser(string keyword, UserStatus status, UserSortExpression sortOrder, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<UserStandardEntity> GetListReporterUser(long newsId, string currentUsername);
        [OperationContract]
        List<UserStandardEntity> GetListEditorUser(long newsId, string currentUsername);
        [OperationContract]
        List<UserStandardEntity> GetListSecretaryUser(long newsId, string currentUsername);

        [OperationContract]
        List<UserStandardEntity> GetUserListByPermissionListAndZoneList(string zoneIds,
                                                                        params ChannelVN.CMS.Entity.Base.Security.EnumPermission[] permissionIds);
        [OperationContract]
        List<UserStandardEntity> GetUserWithFullPermissionAndZoneId(long newsId, string currentUsername);

        [OperationContract]
        string GetOtpSecretKeyByUsername(string username);

        [OperationContract]
        WcfActionResponse UpdateOtpSecretKeyForUsername(string username, string otpSecretKey);

        [OperationContract]
        WcfActionResponse UpdateActiveCodeForUser(string username, string activeCode);

        [OperationContract]
        WcfActionResponse ValidAccount(string username, string password, int siteId);

        #endregion
    }
}
