using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.External.Interactive.Account;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.Interactive.Security;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.Interactive.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.External.Interactive
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SecurityServices : ISecurityServices
    {
        #region Policy
        public WcfActionResponse UpdateUserPermissionByUserId(string encryptUserId, ChannelVN.CMS.Entity.Base.Security.UserPermissionEntity[] userPermission, bool isFullPermission, bool isFullZone, string accountName)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);
            var user = UserBo.GetById(userId);
            user.IsFullPermission = isFullPermission;
            user.IsFullZone = isFullZone;
            var response = UserBo.Update(user, accountName);
            if (response == ErrorMapping.ErrorCodes.Success)
            {
                if (!isFullPermission || !isFullZone)
                {
                    var errorCode = ChannelVN.CMS.BO.Base.Security.PermissionBo.GrantListPermission(userId,
                                                                     new List<ChannelVN.CMS.Entity.Base.Security.UserPermissionEntity>(userPermission));
                    if (errorCode == ErrorMapping.ErrorCodes.Success)
                    {
                        return WcfActionResponse.CreateSuccessResponse();
                    }
                    else
                    {
                        return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                    }
                }
                else
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
            }
            return WcfActionResponse.CreateErrorResponse((int)response, ErrorMapping.Current[response]);
        }

        #endregion

        #region User
        public WcfActionResponse AddNewUser(UserWithPermissionEntity userWithPermission)
        {
            var newUserId = 0;
            var encryptUserId = "";
            var errorCode = UserBo.AddNew(userWithPermission.User, ref newUserId, ref encryptUserId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse UpdateUser(UserWithPermissionEntity userWithPermission, string accountName)
        {
            var errorCode = UserBo.Update(userWithPermission.User, accountName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                ChannelVN.CMS.BO.Base.Security.PermissionBo.GrantListPermission(userWithPermission.User.Id, userWithPermission.UserPermissions);
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse UpdateUserProfile(UserEntity user, string accountName)
        {
            var errorCode = UserBo.UpdateProfile(user, accountName);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse AddnewUserProfile(UserEntity user, ref int userId)
        {
            var encryptUserId = "";
            var errorCode = UserBo.AddNew(user, ref userId, ref encryptUserId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }

        public WcfActionResponse UpdateAvatar(string accountName, string avatar)
        {
            var errorCode = UserBo.UpdateAvatar(accountName, avatar);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }


        public WcfActionResponse ChangeStatus(int userId, UserStatus status)
        {
            var errorCode = UserBo.ChangeStatus(userId, status);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }



        public UserEntity GetUserByUserId(string encryptUserId)
        {
            return UserBo.GetById(encryptUserId);
        }

        public UserEntity GetUserByUsername(string username)
        {
            return UserBo.GetUserByUsername(username);
        }
        public UserEntity GetUserByActiveCode(string activeCode)
        {
            return UserBo.GetUserByActiveCode(activeCode);
        }

        public UserWithPermissionDetailEntity GetUserWithPermissionDetailByUserId(string encryptUserId, bool isGetChildZone)
        {
            return UserBo.GetUserWithPermissionDetailByUserId(encryptUserId, isGetChildZone);
        }


        public List<UserStandardEntity> SearchUser(string keyword, UserStatus status, UserSortExpression sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                UserStatus userStatus;
                if (!Enum.TryParse(status.ToString(), out userStatus))
                {
                    userStatus = UserStatus.Unknow;
                }
                UserSortExpression sortBy;
                if (!Enum.TryParse(sortOrder.ToString(), out sortBy))
                {
                    sortBy = UserSortExpression.CreateDateDesc;
                }
                return UserBo.Search(keyword, userStatus, sortBy, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<UserStandardEntity>();
            }
        }

        public List<UserStandardEntity> GetListReporterUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(ChannelVN.CMS.Entity.Base.Security.EnumPermission.ArticleReporter, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }
        public List<UserStandardEntity> GetListEditorUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(ChannelVN.CMS.Entity.Base.Security.EnumPermission.ArticleEditor, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }
        public List<UserStandardEntity> GetListSecretaryUser(long newsId, string currentUsername)
        {
            var users = UserBo.GetUserWhoCanManageThisNews(ChannelVN.CMS.Entity.Base.Security.EnumPermission.ArticleAdmin, newsId);
            foreach (var user in users.Where(user => user.UserName == currentUsername))
            {
                users.Remove(user);
                break;
            }
            return users;
        }

        public List<UserStandardEntity> GetUserListByPermissionListAndZoneList(string zoneIds,
                                                                               params ChannelVN.CMS.Entity.Base.Security.EnumPermission[] permissionIds)
        {
            return UserBo.GetUserListByPermissionListAndZoneList(zoneIds, permissionIds);
        }
        public List<UserStandardEntity> GetUserWithFullPermissionAndZoneId(long newsId, string currentUsername)
        {
            return UserBo.GetUserWithFullPermissionAndZoneId(ChannelVN.CMS.Entity.Base.Security.EnumPermission.ArticleAdmin, newsId, currentUsername);
        }

        public string GetOtpSecretKeyByUsername(string username)
        {
            return UserBo.GetOtpSecretKeyByUsername(username);
        }

        public WcfActionResponse UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            var errorCode = UserBo.UpdateOtpSecretKeyForUsername(username, otpSecretKey);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }
        public WcfActionResponse UpdateActiveCodeForUser(string username, string activeCode)
        {
            var errorCode = UserBo.UpdateActiveCodeForUser(username, activeCode);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            else
            {
                return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
        }
        #endregion

        public WcfActionResponse ValidAccount(string username, string password, int siteId)
        {
            try
            {

                var errorCode = UserBo.ValidAccount(username, password, siteId);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    return WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
        }


        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
