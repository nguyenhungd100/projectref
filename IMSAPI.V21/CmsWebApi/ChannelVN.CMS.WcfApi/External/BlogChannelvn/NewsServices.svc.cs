﻿using System;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.BlogChannelvn.NewsPosition;
using ChannelVN.CMS.Entity.External.BlogChannelvn.NewsPosition;
using ChannelVN.CMS.WcfApi.External.BlogChannelvn.ServiceContracts;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using System.Collections.Generic;

namespace ChannelVN.CMS.WcfApi.External.BlogChannelvn
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage(string listOfFocusPositionOnLastestNews)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage(listOfFocusPositionOnLastestNews);
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestNews)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId, listOfFocusPositionOnLastestNews);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
