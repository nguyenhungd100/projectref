﻿using System.ServiceModel;
using ChannelVN.CMS.Entity.External.BlogChannelvn.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.External.BlogChannelvn.ServiceContracts
{
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        #region News
        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage(string listOfFocusPositionOnLastestNews);

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestNews);
        #endregion
    }
}
