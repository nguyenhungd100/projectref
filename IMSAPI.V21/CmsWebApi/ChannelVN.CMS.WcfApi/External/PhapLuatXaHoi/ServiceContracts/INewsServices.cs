﻿using System.ServiceModel;
using ChannelVN.CMS.Entity.External.PhapLuatXaHoi.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.External.PhapLuatXaHoi.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewsServices" in both code and config file together.
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        #region News position

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage();

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId);

        #endregion
    }
}
