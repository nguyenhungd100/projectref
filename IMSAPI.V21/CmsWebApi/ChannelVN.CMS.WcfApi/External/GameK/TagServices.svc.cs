﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.GameK.Tag;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.GameK.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.WcfExtensions;
using System.ServiceModel.Activation;

namespace ChannelVN.CMS.WcfApi.External.GameK
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TagServices : ITagServices
    {
        public WcfActionResponse InsertTag(TagEntity tag, int zoneId, string zoneIdList)
        {
            WcfActionResponse responseData;
            long newTagId = 0;
            var errorCode = BoFactory.GetInstance<TagBo>().InsertTag(tag, zoneId, zoneIdList, ref newTagId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                tag.Id = newTagId;
                responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag), "1");
            }
            else
            {
                responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
            }
            return responseData;
        }

        public WcfActionResponse Update(TagEntity tag, int zoneId, string zoneIdList)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<TagBo>().Update(tag, zoneId, zoneIdList);
                if (errorCode == ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(tag));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
