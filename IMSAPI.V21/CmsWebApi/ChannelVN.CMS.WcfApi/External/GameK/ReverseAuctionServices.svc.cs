﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.AuthenticateLog;
using ChannelVN.CMS.BO.External.GameK;
using ChannelVN.CMS.BO.External.GameK.Auction;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.GameK.Auction;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.External.GameK
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ReverseAuctionServices : IReverseAuctionServices
    {
        public List<ReverseAuctionEntity> Search(string keyword,
                                                            int status,
                                                            long newsId,
                                                            int pageIndex,
                                                            int pageSize,
                                                            ref int totalRow)
        {
            var auctionList = ReverseAuctionBo.Search(keyword, status, newsId, pageIndex, pageSize, ref totalRow);

            return auctionList;
        }

        public ReverseAuctionEntity GetAuctionById(long id)
        {
            return ReverseAuctionBo.GetById(id);
        }

        public WcfActionResponse AddNew(ReverseAuctionEntity auction, ref int newAuctionId)
        {
            var errorCode = ReverseAuctionBo.AddNew(auction, ref newAuctionId);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public WcfActionResponse Update(ReverseAuctionEntity auction)
        {
            var errorCode = ReverseAuctionBo.Update(auction);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        public ErrorMessageEntity[] GetErrorMessage()
        {
            return new ErrorMessageEntity[] { };
        }
    }
}
