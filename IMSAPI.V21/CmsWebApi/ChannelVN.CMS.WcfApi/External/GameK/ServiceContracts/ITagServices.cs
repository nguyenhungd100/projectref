﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.WcfApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.External.GameK.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITagServices" in both code and config file together.
    [ServiceContract]
    public interface ITagServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse InsertTag(TagEntity tag, int zoneId, string zoneIdList);
        [OperationContract]
        WcfActionResponse Update(TagEntity tag, int zoneId, string zoneIdList);
    }
}
