﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.External.GameK.Auction;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.External.GameK
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReverseAuctionServices" in both code and config file together.
    [ServiceContract]
    public interface IReverseAuctionServices : IErrorMessageMapping
    {
        [OperationContract]
        List<ReverseAuctionEntity> Search(string keyword,
                                          int status,
                                          long newsId,
                                          int pageIndex,
                                          int pageSize,
                                          ref int totalRow);

        [OperationContract]
        ReverseAuctionEntity GetAuctionById(long id);

        [OperationContract]
        WcfActionResponse AddNew(ReverseAuctionEntity auction, ref int newAuctionId);

        [OperationContract]
        WcfActionResponse Update(ReverseAuctionEntity auction);
    }
}
