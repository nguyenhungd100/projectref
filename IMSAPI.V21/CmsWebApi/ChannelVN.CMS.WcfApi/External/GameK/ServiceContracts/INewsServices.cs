﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Entity.External.GameK.NewsPosition;

namespace ChannelVN.CMS.WcfApi.External.GameK.ServiceContracts
{
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        #region News
        [OperationContract]
        WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                     string tagIdListForSubtitle, string newsRelationIdList,
                                     string newsRelationSpecialIdList, string currentUsername, string[] authorList,
                                     int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions);

        [OperationContract]
        WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string newsRelationSpecialIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions);


        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage(string listOfFocusPositionOnLastestNews);

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestNews);
        #endregion
    }
}
