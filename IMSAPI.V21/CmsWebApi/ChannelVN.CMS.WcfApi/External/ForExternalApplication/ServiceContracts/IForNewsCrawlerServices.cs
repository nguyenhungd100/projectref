﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.ForExternalApplication.Entity;

namespace ChannelVN.CMS.WcfApi.External.ForExternalApplication.ServiceContracts
{
    [ServiceContract]
    public interface IForNewsCrawlerServices
    {
        [OperationContract]
        WcfActionResponse CrawlAndPublish(string secretKey, ChannelMappingForExternalApplication channelMapping, NewsCrawlerResultEntity newsCrawlerResult, ref long newsId, ref string newsUrl);
        [OperationContract]
        List<ZoneForCrawlerEntity> GetAllZoneForCrawler(string secretKey, ChannelMappingForExternalApplication channelMapping);
    }
}
