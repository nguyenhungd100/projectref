﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.ForExternalApplication.Entity;

namespace ChannelVN.CMS.WcfApi.External.ForExternalApplication.ServiceContracts
{
    [ServiceContract]
    public interface IForExternalSite
    {
        [OperationContract]
        List<NewsPositionEntity> GetNewsPositionByTypeAndZoneId(ChannelMappingForExternalApplication channel, string secretKey, int positionTypeId, int zoneId);
    }
}
