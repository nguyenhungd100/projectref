﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.External.ForExternalApplication.ServiceContracts;
using ChannelVN.ForExternalApplication.Bo;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.ForExternalApplication.Entity.ErrorCode;

namespace ChannelVN.CMS.WcfApi.External.ForExternalApplication
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ForNewsCrawlerServices : IForNewsCrawlerServices
    {
        public WcfActionResponse CrawlAndPublish(string secretKey, ChannelMappingForExternalApplication channelMapping, NewsCrawlerResultEntity newsCrawlerResult, ref long newsId, ref string newsUrl)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretKeyForExternalRequest"]))
            {
                return null;
            }
            try
            {
                var errorCode = ForNewsCrawler.CrawlAndPublish(channelMapping, newsCrawlerResult, ref newsId, ref newsUrl);
                var responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                       ? WcfActionResponse.CreateSuccessResponse()
                                       : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                               ErrorMapping.Current[
                                                                                   errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public List<ZoneForCrawlerEntity> GetAllZoneForCrawler(string secretKey, ChannelMappingForExternalApplication channelMapping)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretKeyForExternalRequest"]))
            {
                return null;
            }
            return ForNewsCrawler.GetAllZoneForCrawler(channelMapping);
        }
    }
}
