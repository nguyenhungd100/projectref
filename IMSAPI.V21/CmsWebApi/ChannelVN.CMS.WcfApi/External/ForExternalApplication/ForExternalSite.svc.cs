﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.WcfApi.External.ForExternalApplication.ServiceContracts;
using ChannelVN.ForExternalApplication.Entity;

namespace ChannelVN.CMS.WcfApi.External.ForExternalApplication
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ForExternalSite : IForExternalSite
    {
        public List<NewsPositionEntity> GetNewsPositionByTypeAndZoneId(ChannelMappingForExternalApplication channel, string secretKey, int positionTypeId, int zoneId)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretKeyForExternalRequest"]))
            {
                return null;
            }
            try
            {
                ChannelMapping.SetChannelNamespace(channel);
                return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionByTypeAndZoneId(positionTypeId, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsPositionEntity>();
            }
        }
    }
}
