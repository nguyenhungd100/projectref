﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.TimDiemThi.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.TimDiemThi.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.TimDiemThi.ServiceContracts;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.External.TimDiemThi
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage()
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId);
        }
        public bool CheckNewsDistributionDateForLastestNews(long newsId, ref DateTime minValidDate)
        {
            return BoFactory.GetInstance<NewsPositionBo>().CheckNewsDistributionDateForLastestNews(newsId, ref minValidDate);
        }
        public bool RemoveNewsDisplayInSlide(string newsIds)
        {
            return BoFactory.GetInstance<NewsPositionBo>().RemoveNewsDisplayInSlide(newsIds);
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
