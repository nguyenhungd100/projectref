﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.TimDiemThi.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.External.TimDiemThi.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewsServices" in both code and config file together.
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        #region News position

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage();

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId);

        [OperationContract]
        bool CheckNewsDistributionDateForLastestNews(long newsId, ref DateTime minValidDate);

        [OperationContract]
        bool RemoveNewsDisplayInSlide(string newsIds);

        #endregion
    }
}
