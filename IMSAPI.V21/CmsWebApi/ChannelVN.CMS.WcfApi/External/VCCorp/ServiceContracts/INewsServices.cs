﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.Entity.External.VCCorp.NewsPosition;

namespace ChannelVN.CMS.WcfApi.External.VCCorp
{
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        [OperationContract]
        WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar,
                                           string url);


        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage();

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId);
    }
}
