﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.SucKhoeDoiSong.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.SucKhoeDoiSong.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.SucKhoeDoiSong.ServiceContracts;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.External.SucKhoeDoiSong
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage()
        {
            try
            {

                return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new NewsPositionForHomePageEntity();
            }
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId)
        {
            try
            {

                return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new NewsPositionForListPageEntity();
            }
        }

        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
