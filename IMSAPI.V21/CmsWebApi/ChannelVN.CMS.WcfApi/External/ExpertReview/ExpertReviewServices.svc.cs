﻿using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Thread;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.ServiceContracts.ExpertReview;
using ChannelVN.ExpertReviewExternal.Bo;
using ChannelVN.ExpertReviewExternal.Entity;
using ChannelVN.ExpertReviewExternal.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace ChannelVN.CMS.WcfApi.External.ExpertReview
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ExpertReviewServices : IExpertReviewServices
    {
        #region SEO
        #region Zone
        public List<ZoneEntity> GetAllZone(string channelMapping, string secretKey)
        {
            string s = Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]);
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return ZoneExpertBo.GetAllZone(channelMapping);
        }
        public List<ZoneWithSimpleFieldEntity> GetAllZoneActiveWithTreeView(string channelMapping, string secretKey)
        {
            string s = Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]);
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return ZoneExpertBo.GetAllZoneActiveWithTreeView(channelMapping);
        }
        #endregion
        
        #region Tag
        public List<TagExpertEntity> SearchTag(string channelMapping, string secretKey, string keyword, int zoneId, int type, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return TagExpertBo.SearchTag(channelMapping, keyword, zoneId, type, isHotTag, pageIndex, pageSize, ref totalRow);
        }
        public List<TagExpertEntity> GetListTagNew(string channelMapping, string secretKey, int zoneId,int pageIndex, int pageSize, ref int totalRow)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return new List<TagExpertEntity>();
            }
            return TagExpertBo.GetListTagNew(channelMapping, zoneId, pageIndex, pageSize, ref totalRow);
        }
        public TagDetailEntity GetTagByTagId(string channelMapping, string secretKey, long tagId)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return TagExpertBo.GetTagByTagId(channelMapping, tagId);
        }
        public TagEntity GetTagByTagName(string channelMapping, string secretKey, string name, bool getTagHasNewsOnly = false)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return TagExpertBo.GetTagByTagName(channelMapping, name, getTagHasNewsOnly);
        }

        public WcfActionResponse Tag_UpdateStatus(string channelMapping, string secretKey, long tagId, EnumTagStatus tagStatus)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.InvalidRequest,
                                                           ErrorMapping.Current[
                                                               ErrorMapping.ErrorCodes.InvalidRequest]);
            }
            return TagExpertBo.Tag_UpdateStatus(channelMapping, tagId, tagStatus) 
                  ? WcfActionResponse.CreateSuccessResponse()
                  : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                          ErrorMapping.Current[
                                                              ErrorMapping.ErrorCodes.BusinessError]);
        }
        public List<ThreadEntity> SearchThread(string channelMapping, string secretKey, string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return TagExpertBo.SearchThread(channelMapping, keyword, zoneId, orderBy, isHotThread, pageIndex, pageSize, ref totalRow);
        }
        #endregion

        #region News
        public List<ExpertNewsInListEntity> SearchNews(string channelMapping, string secretKey, string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return NewsExpertBo.SearchNews(channelMapping,keyword, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows);
        }
        public List<ExpertNewsInListEntity> SearchNewsPublishedByGoogleKeyword(string channelMapping, string secretKey, string keyword, int pageSize, int pageIndex, ref int TotalRows)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return NewsExpertBo.SearchNewsPublishedByKeyword(channelMapping, keyword, pageSize, pageIndex, ref TotalRows);
        }
        public NewsDetailEntity GetNewsByNewsId(string channelMapping, string secretKey, long newsId)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return NewsExpertBo.NewsGetById(channelMapping, newsId);
        }
        public WcfActionResponse UpdateNews(string channelMapping, string secretKey, NewsExpertEntity news)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return  WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.InvalidRequest,
                                                           ErrorMapping.Current[
                                                               ErrorMapping.ErrorCodes.InvalidRequest]);
            }
            try
            {
                return NewsExpertBo.News_Expert_Update(channelMapping, news) == ErrorMapping.ErrorCodes.Success
                    ? WcfActionResponse.CreateSuccessResponse()
                    : WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.BusinessError,
                                                            ErrorMapping.Current[
                                                                ErrorMapping.ErrorCodes.BusinessError]);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }
        #endregion
        public NewsEntity[] SearchNewsByFocusKeyword(string channelMapping, string secretKey, string keyword, int top)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return NewsExpertBo.SearchNewsByFocusKeyword(channelMapping,keyword, top).ToArray();
        }
        public NewsPublishForObjectBoxEntity[] SearchNewsForNewsRelation(string channelMapping, string secretKey, int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return NewsExpertBo.SearchNewsForNewsRelation(channelMapping,zoneId, keyword, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public List<UserStandardEntity> ListAllUser(string channelMapping, string secretKey)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return UserExpertBo.ListAllUser(channelMapping);
        }
        #endregion

        #region Thread services
        #region Get
        public ThreadDetailEntity Thread_GetByThreadId(string channelMapping, string secretKey, long threadId)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return null;
            }
            return ThreadExpertBo.GetTagByTagId(channelMapping, threadId);
        }
        public List<ThreadEntity> Thread_SearchThread(string channelMapping, string secretKey, string keyword, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            if (secretKey != Utility.CreateMD5Checksum(ConfigurationManager.AppSettings["SecretExpertExternalRequest"]))
            {
                return new List<ThreadEntity>();
            }
            return ThreadExpertBo.SearchThread(channelMapping, keyword, zoneId, pageIndex, pageSize, ref totalRow);
        }

        public ThreadDetailEntity GetThreadByThreadId(long threadId)
        {
            return BoFactory.GetInstance<ThreadBo>().GetThreadByThreadId(threadId);
        }
        public List<ThreadEntity> GetThreadByNewsId(long newsId)
        {
            return BoFactory.GetInstance<ThreadBo>().GetThreadByNewsId(newsId);
        }
        public ThreadEntity[] ThreadServiceSearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoFactory.GetInstance<ThreadBo>().SearchThread(keyword, zoneId, orderBy, isHotThread, pageIndex, pageSize, ref totalRow).ToArray();
        }
        public List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestion(int top, int zoneId, string keyword, int orderBy)
        {
            return BoFactory.GetInstance<ThreadBo>().SearchThreadForSuggestion(top, zoneId, keyword, (int)orderBy);
        }
        #endregion

        #region Update
        public WcfActionResponse Insert(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, ref long newThreadId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().Insert(thread, zoneId, zoneIdList, relationThread, ref newThreadId);
                if (errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(thread));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse Update(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().Update(thread, zoneId, zoneIdList, relationThread);
                if (errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(thread));
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse DeleteById(long threadId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().DeleteById(threadId);
                if (errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateThreadHot(long threadId, bool isHotThread)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadHot(threadId, isHotThread);
                if (errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }
        public WcfActionResponse UpdateThreadNews(long threadId, string deleteNewsId, string addNewsId)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadNews(threadId, deleteNewsId, addNewsId);
                if (errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                                                                     ErrorMapping.Current[
                                                                         ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        public WcfActionResponse UpdateThreadNewsRelated(long newsId, string relatedThreads)
        {
            WcfActionResponse responseData;
            try
            {
                var errorCode = BoFactory.GetInstance<ThreadBo>().UpdateThreadNewsRelated(newsId, relatedThreads);
                if (errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success)
                {
                    responseData = WcfActionResponse.CreateSuccessResponse();
                }
                else
                {
                    responseData = WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                responseData = WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,
                    ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]);
            }
            return responseData;
        }

        #endregion

        #region BoxThreadEmbed
        public BoxThreadEmbedEntity[] GetListThreadEmbed(int zoneId, int type)
        {
            return BoFactory.GetInstance<BoxThreadEmbedBo>().GetListThreadEmbed(zoneId, type).ToArray();
        }

        public WcfActionResponse InsertThreadEmbed(BoxThreadEmbedEntity threadEmbedBox)
        {
            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Insert(threadEmbedBox);
            responseData = errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(string.Empty, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse UpdateThreadEmbed(string listThreadId, int zoneId, int type)
        {

            WcfActionResponse responseData;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Update(listThreadId, zoneId, type);
            responseData = errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(listThreadId, "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
            return responseData;
        }

        public WcfActionResponse DeleteThreadEmbed(long threadId, int zoneId, int type)
        {
            WcfActionResponse responseData;
            var newEncryptThreadId = string.Empty;
            var errorCode = BoFactory.GetInstance<BoxThreadEmbedBo>().Delete(threadId, zoneId, type);
            responseData = errorCode == ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(threadId.ToString(), "1") : WcfActionResponse.CreateErrorResponse((int)errorCode, ChannelVN.CMS.Entity.ErrorCode.ErrorMapping.Current[errorCode]);
            return responseData;
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
        #endregion
    }
}
