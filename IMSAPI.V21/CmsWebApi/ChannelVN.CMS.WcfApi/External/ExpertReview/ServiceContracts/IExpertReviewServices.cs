﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.ExpertReviewExternal.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ChannelVN.CMS.WcfApi.External.ServiceContracts.ExpertReview
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IExpertReviewServices" in both code and config file together.
    [ServiceContract]
    public interface IExpertReviewServices
    {
        #region  SEO
        #region Zone
        [OperationContract]
        List<ZoneEntity> GetAllZone(string channelMapping, string secretKey);

        [OperationContract]
        List<ZoneWithSimpleFieldEntity> GetAllZoneActiveWithTreeView(string channelMapping, string secretKey);
        #endregion

        #region Tag
        [OperationContract]
        List<TagExpertEntity> SearchTag(string channelMapping, string secretKey, string keyword, int zoneId, int type, int isHotTag, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        TagDetailEntity GetTagByTagId(string channelMapping, string secretKey, long tagId);
        [OperationContract]
        TagEntity GetTagByTagName(string channelMapping, string secretKey, string name, bool getTagHasNewsOnly = false);

        [OperationContract]
        List<ThreadEntity> SearchThread(string channelMapping, string secretKey, string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        List<TagExpertEntity> GetListTagNew(string channelMapping, string secretKey, int zoneId, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        WcfActionResponse Tag_UpdateStatus(string channelMapping, string secretKey, long tagId, EnumTagStatus tagStatus);
        #endregion

        #region News
        [OperationContract]
        WcfActionResponse UpdateNews(string channelMapping, string secretKey, NewsExpertEntity news);

        [OperationContract]
        List<ExpertNewsInListEntity> SearchNews(string channelMapping, string secretKey, string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows);

        [OperationContract]
        NewsDetailEntity GetNewsByNewsId(string channelMapping, string secretKey, long newsId);

        [OperationContract]
        NewsEntity[] SearchNewsByFocusKeyword(string channelMapping, string secretKey, string keyword, int top);

        [OperationContract]
        NewsPublishForObjectBoxEntity[] SearchNewsForNewsRelation(string channelMapping, string secretKey, int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        List<ExpertNewsInListEntity> SearchNewsPublishedByGoogleKeyword(string channelMapping, string secretKey, string keyword, int pageSize, int pageIndex, ref int TotalRows);
        #endregion
        #endregion

        #region Thread service
        [OperationContract]
        List<ThreadEntity> Thread_SearchThread(string channelMapping, string secretKey, string keyword, int zoneId, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        ThreadDetailEntity Thread_GetByThreadId(string channelMapping,string secretKey, long threadId);

        [OperationContract]
        List<UserStandardEntity> ListAllUser(string channelMapping, string secretKey);

        #region Get

        [OperationContract]
        ThreadDetailEntity GetThreadByThreadId(long threadId);

        [OperationContract]
        ThreadEntity[] ThreadServiceSearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow);

        [OperationContract]
        List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestion(int top, int zoneId, string keyword, int orderBy);

        [OperationContract]
        List<ThreadEntity> GetThreadByNewsId(long newsId);

        #endregion

        #region Update

        [OperationContract]
        WcfActionResponse Insert(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, ref long newThreadId);

        [OperationContract]
        WcfActionResponse Update(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread);

        [OperationContract]
        WcfActionResponse DeleteById(long threadId);

        [OperationContract]
        WcfActionResponse UpdateThreadHot(long threadId, bool isHotThread);

        [OperationContract]
        WcfActionResponse UpdateThreadNews(long threadId, string deleteNewsId, string addNewsId);

        [OperationContract]
        WcfActionResponse UpdateThreadNewsRelated(long newsId, string relatedThreads);

        #endregion

        #region BoxThreadEmbed

        [OperationContract]
        BoxThreadEmbedEntity[] GetListThreadEmbed(int zoneId, int type);

        [OperationContract]
        WcfActionResponse InsertThreadEmbed(BoxThreadEmbedEntity threadEmbedBox);

        [OperationContract]
        WcfActionResponse UpdateThreadEmbed(string listThreadId, int zoneId, int type);

        [OperationContract]
        WcfActionResponse DeleteThreadEmbed(long threadId, int zoneId, int type);

        #endregion
        #endregion
    }
}
