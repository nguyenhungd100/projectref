﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.GiaDinhNet.News;
using ChannelVN.CMS.BO.External.GiaDinhNet.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.GiaDinhNet.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.GiaDinhNet.ServiceContracts;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.WcfApi.External.GiaDinhNet
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices
    {
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage()
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId);
        }

        public WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar, string url, string sapo, NewsType newsType)
        {
            var errorCode = NewsPositionBo.SaveLinkPosition(type, position, zoneId, title, avatar, url, sapo, newsType);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }

        #endregion

        #region Magazine News
        public List<NewsEntity> SearchNewsByMagazineIdWithPaging(int magazineId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsByMagazineIdWithPaging(magazineId, pageIndex, pageSize, ref totalRow);
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInMagazine(int zoneId, string keyword, int magazineId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsBo.SearchNewsPublishExcludeNewsInMagazine(zoneId, keyword, magazineId, pageIndex, pageSize, ref totalRow);
        }
        #endregion

        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
