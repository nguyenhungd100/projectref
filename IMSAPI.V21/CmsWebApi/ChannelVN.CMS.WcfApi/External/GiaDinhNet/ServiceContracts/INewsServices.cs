﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.GiaDinhNet.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.External.GiaDinhNet.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewsServices" in both code and config file together.
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        #region News position

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage();

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId);

        [OperationContract]
        WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar,
                                           string url, string sapo, NewsType newsType);
        #endregion

        #region Magazine News
        [OperationContract]
        List<NewsEntity> SearchNewsByMagazineIdWithPaging(int magazineId, int pageIndex, int pageSize, ref int totalRow);
        [OperationContract]
        List<NewsInListEntity> SearchNewsPublishExcludeNewsInMagazine(int zoneId, string keyword, int magazineId,
                                                                    int pageIndex, int pageSize, ref int totalRow);
        #endregion
    }
}
