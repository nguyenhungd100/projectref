﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.External.SohaNews.News;
using ChannelVN.CMS.BO.External.SohaNews.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.SohaNews.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;
using ChannelVN.CMS.WcfApi.External.SohaNews.ServiceContracts;
using ChannelVN.WcfExtensions;
using System;

namespace ChannelVN.CMS.WcfApi.External.SohaNews
{
    [ExtensionInspector]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class NewsServices : INewsServices, IErrorMessageMapping
    {
        #region News position

        public NewsPositionForHomePageEntity GetNewsPositionForHomePage()
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
        }

        public NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId)
        {
            return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForListPage(zoneId);
        }
        public WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar, string url)
        {
            var errorCode = NewsPositionBo.SaveLinkPosition(type, position, zoneId, title, avatar, url);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return WcfActionResponse.CreateSuccessResponse();
            }
            return WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);
        }
        #endregion

        #region News position for zone sport
        //public NewPositionForZoneSportWithLastestNews GetNewsPositionForZoneSportByPositionType(int newsPositionType, int zoneId, string listOfOrder)
        //{
        //    return BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForZoneSportByTypeAndZoneId(newsPositionType, zoneId, listOfOrder);
        //}

        //public WcfActionResponse SaveNewsPositionForZoneSport(NewsPositionForZoneSportEntity[] newsPositions, int avatarIndex, bool checkNewsExists)
        //{
        //    var errorCode =
        //        BoFactory.GetInstance<NewsPositionBo>().SaveNewsPositionForZoneSport(
        //            new List<NewsPositionForZoneSportEntity>(newsPositions), avatarIndex, checkNewsExists);
        //    return errorCode == ErrorMapping.ErrorCodes.Success
        //               ? WcfActionResponse.CreateSuccessResponse()
        //               : WcfActionResponse.CreateErrorResponse((int) errorCode,
        //                                                       ErrorMapping.Current[errorCode]);
        //}
        #endregion

        public WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newEncryptNewsId = String.Empty;
                var newsId = 0L;
                var errorCode = NewsBo.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, ref newsId, ref newEncryptNewsId, new List<string>(authorList), newsChildOrder, sourceId, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success ? WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "") : WcfActionResponse.CreateErrorResponse((int)errorCode, ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }

        public WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var newsStatus = 0;
                var errorCode = NewsBo.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername, isRebuildLink, ref newsStatus, new List<string>(authorList), newsChildOrder, sourceId, publishedContent, newsExtensions);
                WcfActionResponse responseData = errorCode == ErrorMapping.ErrorCodes.Success
                                                   ? WcfActionResponse.CreateSuccessResponse(news.Id + ";" + newsStatus, "")
                                                   : WcfActionResponse.CreateErrorResponse((int)errorCode,
                                                                                         ErrorMapping.Current[errorCode]);

                return responseData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return WcfActionResponse.CreateErrorResponse(ex);
            }
        }


        public ErrorMessageEntity[] GetErrorMessage()
        {
            var errorCodes = Enum.GetValues(typeof(ErrorMapping.ErrorCodes));
            return (from object errorCode in errorCodes
                    select new ErrorMessageEntity
                    {
                        Code = (int)errorCode,
                        Message = ErrorMapping.Current[(ErrorMapping.ErrorCodes)errorCode]
                    }).ToArray();
        }
    }
}
