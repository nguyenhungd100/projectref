﻿using System.Collections.Generic;
using System.ServiceModel;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.SohaNews.NewsPosition;
using ChannelVN.CMS.WcfApi.Common;

namespace ChannelVN.CMS.WcfApi.External.SohaNews.ServiceContracts
{
    [ServiceContract]
    public interface INewsServices : IErrorMessageMapping
    {
        #region News position

        [OperationContract]
        NewsPositionForListPageEntity GetNewsPositionForListPage(int zoneId);

        [OperationContract]
        NewsPositionForHomePageEntity GetNewsPositionForHomePage();

        #endregion

        #region News position for zone sport
        //[OperationContract]
        //NewPositionForZoneSportWithLastestNews GetNewsPositionForZoneSportByPositionType(int newsPositionType, int zoneId, string listOfOrder);

        //[OperationContract]
        //WcfActionResponse SaveNewsPositionForZoneSport(NewsPositionForZoneSportEntity[] newsPositions, int avatarIndex, bool checkNewsExists);
        #endregion

        [OperationContract]
        WcfActionResponse InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, string[] authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions);
        [OperationContract]
        WcfActionResponse UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername, bool isRebuildLink, string[] authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions);
        

        [OperationContract]
        WcfActionResponse SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar,
                                           string url);
    }
}
