﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.ExternalCms.Bo;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ForExternalApplication.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.BO.Base.News;
using System.Threading;
using ChannelVN.CMS.Common.RedisClientHelper;

namespace ChannelVN.CMS.WcfApi
{
    public partial class TestCommentCount : System.Web.UI.Page
    {
        private const string RedisKeyForCountCommentByNewsId = "CmsCommentCountByNewsId[{0}]";
        private const string RedisKeyForCountCommentByVideoId = "CmsCommentCountByVideoId[{0}]";
        private const string RedisKeyForCountCommentByVoteId = "CmsCommentCountByVoteId[{0}]";

        protected void Page_Load(object sender, EventArgs e)
        {
            lblResult.Text = "";
            lblSetFromDb.Text = "";
        }

        protected void btnSetCommentCount_Click(object sender, EventArgs e)
        {
            var commentType = EnumCommentPublishedObjectType.ForNews;
            var key = string.Format(RedisKeyForCountCommentByNewsId, txtNewsId.Text);
            switch (Utility.ConvertToInt(ddlCommentType.SelectedValue))
            {
                case (int)EnumCommentPublishedObjectType.ForVideo:
                    commentType = EnumCommentPublishedObjectType.ForVideo;
                    key = string.Format(RedisKeyForCountCommentByVideoId, txtNewsId.Text);
                    break;
                case (int)EnumCommentPublishedObjectType.ForVote:
                    commentType = EnumCommentPublishedObjectType.ForVote;
                    key = string.Format(RedisKeyForCountCommentByVoteId, txtNewsId.Text);
                    break;
            }
            ChannelMappingForExternalApplication channel;
            if (!Enum.TryParse(ddlChannel.SelectedValue, out channel))
            {
                channel = ChannelMappingForExternalApplication.AutoPro;
            }
            ChannelMapping.SetChannelNamespace(channel);
            var count = CommentPublishedBo.GetPublishedCommentCountByNewsId(commentType,
                Utility.ConvertToLong(txtNewsId.Text));
            lblSetFromDb.Text = "CountFromDb=" + count;

            if (count > 0)
            {
                var dbNumber = Utility.ConvertToInt(Request.QueryString["db"]);
                var redisClient = new RedisStackClient(dbNumber);
                redisClient.Remove(key);
                redisClient.Add(key, count, DateTime.Now.AddMonths(1).TimeOfDay);
            }
        }

        protected void btnGetFromRedis_Click(object sender, EventArgs e)
        {
            var key = string.Format(RedisKeyForCountCommentByNewsId, txtNewsId.Text);
            switch (Utility.ConvertToInt(ddlCommentType.SelectedValue))
            {
                case (int)EnumCommentPublishedObjectType.ForVideo:
                    key = string.Format(RedisKeyForCountCommentByVideoId, txtNewsId.Text);
                    break;
                case (int)EnumCommentPublishedObjectType.ForVote:
                    key = string.Format(RedisKeyForCountCommentByVoteId, txtNewsId.Text);
                    break;
            }
            var dbNumber = Utility.ConvertToInt(Request.QueryString["db"]);
            var redisClient = new RedisStackClient(dbNumber);
            var count = redisClient.Get<CommentPublishedBo.CommentCountEntity>(key);
            lblResult.Text = "Count=" + NewtonJson.Serialize(count);
        }

        protected void btnSetCommentCountFromDateToDate_Click(object sender, EventArgs e)
        {
            var imsNamespace = Utility.ConvertToInt(ddlChannel.SelectedValue);

            var fromDate = Utility.ConvertToDateTime(txtSetCommentCountFromDate.Text);
            var toDate = Utility.ConvertToDateTime(txtSetCommentCountToDate.Text);

            var listUpdated = "";
            if (imsNamespace > 0 && fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
            {
                ChannelMappingForExternalApplication channelNamespace;
                if (!Enum.TryParse(imsNamespace.ToString(), out channelNamespace))
                {
                    channelNamespace = ChannelMappingForExternalApplication.DanTri;
                }

                var commentType = EnumCommentPublishedObjectType.ForNews;
                switch (Utility.ConvertToInt(ddlCommentType.SelectedValue))
                {
                    case (int)EnumCommentPublishedObjectType.ForVideo:
                        commentType = EnumCommentPublishedObjectType.ForVideo;
                        break;
                    case (int)EnumCommentPublishedObjectType.ForVote:
                        commentType = EnumCommentPublishedObjectType.ForVote;
                        break;
                }

                ChannelMapping.SetChannelNamespace(channelNamespace);
                var newsList = NewsPublishBo.GetListDataByDate(0, fromDate, toDate);

                foreach (var newsPublish in newsList)
                {
                    var key = string.Format(RedisKeyForCountCommentByNewsId, newsPublish.NewsId);
                    switch (commentType)
                    {
                        case EnumCommentPublishedObjectType.ForVideo:
                            key = string.Format(RedisKeyForCountCommentByVideoId, newsPublish.NewsId);
                            break;
                        case EnumCommentPublishedObjectType.ForVote:
                            key = string.Format(RedisKeyForCountCommentByVoteId, newsPublish.NewsId);
                            break;
                    }
                    var dbNumber = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));
                    CommentPublishedBo.UpdateCommentCountIntoRedis(commentType, newsPublish.NewsId);
                    var redisClient = new RedisStackClient(dbNumber);
                    listUpdated += "<p>"+ newsPublish.NewsId + " => "+NewtonJson.Serialize(redisClient.Get(key))+ "</p>";
                    //Thread.Sleep(100);
                }
            }
            lblResult.Text = string.IsNullOrEmpty(listUpdated) ? "Empty updated" : listUpdated;
        }
    }
}