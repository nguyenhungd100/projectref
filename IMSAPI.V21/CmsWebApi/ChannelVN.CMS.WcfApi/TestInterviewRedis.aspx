﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestInterviewRedis.aspx.cs" Inherits="ChannelVN.CMS.WcfApi.TestInterviewRedis" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
        <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" />
        <div>Status: <asp:Literal runat="server" ID="ltrStatus"></asp:Literal></div>
        <div>
            <h1>All event:</h1>
            <div>
                <asp:GridView ID="grvAllEvent" runat="server"></asp:GridView>
            </div>
        </div>
        <div><asp:Literal runat="server" ID="ltrData"></asp:Literal></div>
    </form>
</body>
</html>
