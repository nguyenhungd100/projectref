﻿-- select all duplicate tag
select o.Name, oc.dupeCount, o.id
from Tag o
inner join (
    SELECT Name, COUNT(*) AS dupeCount
    FROM Tag
    GROUP BY Name
    HAVING COUNT(*) > 1
) oc on o.Name = oc.Name
GO



select TagId, count(0) 
from TagNews 
where TagId in (857,858)
group by TagId
GO


-- process duplicate tag
CREATE PROCEDURE [dbo].[TagCloud_ProcessDuplicateTags]
 @TagIdA VARCHAR(1000), 
 @TagIdB INT
AS
BEGIN
 BEGIN TRANSACTION
  DECLARE @Tbl TABLE (TagId bigint, id int identity(1,1))
  INSERT @tbl(TagId) 
  SELECT CAST(part as bigint) from dbo.SplitString(@TagIdA, ',')
  
  DECLARE @Index int, @Total int, @TagId Bigint
  SET @Index = 1
  SET @Total = (SELECT COUNT(0) FROM @Tbl)
  WHILE @Index <= @Total
  BEGIN
   SET @TagId = (SELECT TagId FROM @Tbl WHERE id = @Index)
   
   UPDATE TagNews SET TagId = @TagIdB WHERE TagId = @TagId and not exists(select 1 from TagNews as T1 where T1.TagId = @TagIdB AND T1.NewsId=TagNews.NewsId)

   DELETE FROM TagNews WHERE TagId = @TagId
   
   DELETE Tag WHERE Id = @TagId
   
   -- Increment
   SET @Index = @Index + 1;
  END

 IF @@ERROR <> 0
  ROLLBACK TRANSACTION
 ELSE
  COMMIT TRANSACTION
END
GO