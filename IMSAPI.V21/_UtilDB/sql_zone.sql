USE [IMS_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_GetMaxId]    Script Date: 09/13/2017 14:39:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCedure [dbo].[CMS_Zone_GetMaxId]
AS
SELECT ISNULL(MAX(Id),0) as 'MaxId' FROM Zone
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_Insert]    Script Date: 09/13/2017 14:39:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_Zone_Insert]
	@Id int,
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(255),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@Domain nvarchar(300),
	@AllowComment bit = 1
AS
	BEGIN TRANSACTION CMS_Zone_Insert
	
	BEGIN TRY
		IF @Name IS NOT NULL AND @Name <> ''
		BEGIN
			INSERT INTO Zone (
				Id,
				Name, 
				Description, 
				ShortURL, 
				SortOrder, 
				ParentId, 
				Invisibled, 
				Status,
				CreatedDate,
				ModifiedDate,
				Domain,
				AllowComment)
			VALUES (
				@Id,
				@Name,
				@Description,
				@ShortURL,
				@SortOrder,
				@ParentId,
				@Invisibled,
				@Status,
				GETDATE(),
				GETDATE(),
				@Domain,
				@AllowComment
			)
			COMMIT TRANSACTION CMS_Zone_Insert
		END
		ELSE BEGIN
			RAISERROR ('Invalid Zone Name', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Insert
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Insert
	END CATCH
GO
