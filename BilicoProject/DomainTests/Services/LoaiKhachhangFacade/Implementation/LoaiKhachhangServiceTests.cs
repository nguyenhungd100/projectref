﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.LoaiKhachhangFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.LoaiKhachhangFacade;

namespace Domain.LoaiKhachhang.Tests
{
    [TestClass()]
    public class LoaiKhachhangServiceTests
    {
        LoaiKhachhangService citySvc = new LoaiKhachhangService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new LoaiKhachhangModel()
            {
                TenLoai = "Khách hàng Vàng",
                Thutu = 1
            });
            res = citySvc.Add(new LoaiKhachhangModel()
            {
                TenLoai = "Khách hàng Bạc",
                Thutu = 2
            });
            res = citySvc.Add(new LoaiKhachhangModel()
            {
                TenLoai = "Khách hàng Đồng",
                Thutu = 3
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}