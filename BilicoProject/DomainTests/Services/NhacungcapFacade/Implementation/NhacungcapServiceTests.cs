﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.NhacungcapFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.NhacungcapFacade;

namespace Domain.Nhacungcap.Tests
{
    [TestClass()]
    public class NhacungcapServiceTests
    {
        NhacungcapService citySvc = new NhacungcapService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new NhacungcapModel()
            {
                TenNhacungcap = "Công ty cổ phần ABC",
                Hoatdong = true,
                Thutu = 1
            });
            res = citySvc.Add(new NhacungcapModel()
            {
                TenNhacungcap = "Công ty cổ phần DEF",
                Hoatdong = true,
                Thutu = 2
            });
            res = citySvc.Add(new NhacungcapModel()
            {
                TenNhacungcap = "Công ty cổ phần KMN",
                Hoatdong = true,
                Thutu = 3
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}