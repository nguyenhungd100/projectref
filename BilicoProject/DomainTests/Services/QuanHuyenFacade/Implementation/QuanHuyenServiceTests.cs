﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.QuanHuyenFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.QuanHuyenFacade;

namespace Domain.QuanHuyen.Tests
{
    [TestClass()]
    public class QuanHuyenServiceTests
    {
        QuanHuyenService citySvc = new QuanHuyenService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new QuanHuyenModel()
            {
                Description = "TP Da Nang",
                DisplayOrder = 1,
                IsActive = true,
                Title = "Đà Nẵng 123"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}