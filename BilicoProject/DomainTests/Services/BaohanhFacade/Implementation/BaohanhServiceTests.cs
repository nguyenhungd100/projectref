﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.BaohanhFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.BaohanhFacade;

namespace Domain.Baohanh.Tests
{
    [TestClass()]
    public class BaohanhServiceTests
    {
        BaohanhService citySvc = new BaohanhService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new BaohanhModel()
            {
                SanphamId = 1,
                HopdongId = 2,
                GhichuHonghoc = "",
                ThoigianDukienHoanthanh = DateTime.Now,
                KythuatXulyId = 2,
                Ngaytao = DateTime.Now,
                NguoitaoId = 1
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}