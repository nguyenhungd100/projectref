﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.PhongbanFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.PhongbanFacade;

namespace Domain.Phongban.Tests
{
    [TestClass()]
    public class PhongbanServiceTests
    {
        PhongbanService citySvc = new PhongbanService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new PhongbanModel()
            {
                TenPhongban = "BỘ PHẬN MARKETING",
                Thutu = 1
            });
            res = citySvc.Add(new PhongbanModel()
            {
                TenPhongban = "BỘ PHẬN SALE",
                Thutu = 2
            });
            res = citySvc.Add(new PhongbanModel()
            {
                TenPhongban = "BỘ PHẬN KỸ THUẬT",
                Thutu = 3
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}