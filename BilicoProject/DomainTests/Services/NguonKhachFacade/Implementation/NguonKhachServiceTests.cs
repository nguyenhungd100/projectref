﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.NguonKhachFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.NguonKhachFacade;

namespace Domain.NguonKhach.Tests
{
    [TestClass()]
    public class NguonKhachServiceTests
    {
        NguonKhachService citySvc = new NguonKhachService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new NguonKhachModel()
            {
                TenNguon = "Facebook",
                Hoatdong = true,
            });
            res = citySvc.Add(new NguonKhachModel()
            {
                TenNguon = "Hotline",
                Hoatdong = true,
            });
            res = citySvc.Add(new NguonKhachModel()
            {
                TenNguon = "Website",
                Hoatdong = true,
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}