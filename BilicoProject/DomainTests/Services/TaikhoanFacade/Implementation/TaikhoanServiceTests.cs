﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.TaikhoanFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.TaikhoanFacade;

namespace Domain.Taikhoan.Tests
{
    [TestClass()]
    public class TaikhoanServiceTests
    {
        TaikhoanService citySvc = new TaikhoanService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new TaikhoanModel()
            {
                Email = "admin@bilico.vn" ,
                Matkhau = "123" ,
                PhongbanId = 1,
                Hovaten = "ADMIN" ,
                Mobile = "01234567" ,
                Diachi = "Address" ,
                Ngaytao = DateTime.Now ,
                Tinhtrang = true ,
                Roles = ";1;"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdAsyncTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SingleAsyncTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}