﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.HangsanxuatFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.HangsanxuatFacade;

namespace Domain.Hangsanxuat.Tests
{
    [TestClass()]
    public class HangsanxuatServiceTests
    {
        HangsanxuatService citySvc = new HangsanxuatService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new HangsanxuatModel()
            {
                TenHangsanxuat = "Honda",
                Hoatdong = true,
                Thutu = 1
            });
            res = citySvc.Add(new HangsanxuatModel()
            {
                TenHangsanxuat = "Mazda",
                Hoatdong = true,
                Thutu = 2
            });
            res = citySvc.Add(new HangsanxuatModel()
            {
                TenHangsanxuat = "Kia",
                Hoatdong = true,
                Thutu = 3
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}