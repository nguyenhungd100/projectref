﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.NhomsanphamFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.NhomsanphamFacade;

namespace Domain.Nhomsanpham.Tests
{
    [TestClass()]
    public class NhomsanphamServiceTests
    {
        NhomsanphamService citySvc = new NhomsanphamService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new NhomsanphamModel()
            {
                TenNhomsanpham = "NHÓM 1",
                Hoatdong = true,
                Thutu = 1
            });
            res = citySvc.Add(new NhomsanphamModel()
            {
                TenNhomsanpham = "NHÓM 2",
                Hoatdong = true,
                Thutu = 2
            });
            res = citySvc.Add(new NhomsanphamModel()
            {
                TenNhomsanpham = "NHÓM 3",
                Hoatdong = true,
                Thutu = 3
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}