﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.ThanhphoFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.ThanhphoFacade;

namespace Domain.Thanhpho.Tests
{
    [TestClass()]
    public class ThanhphoServiceTests
    {
        ThanhphoService citySvc = new ThanhphoService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new ThanhphoModel()
            {
                Description = "TP Da Nang",
                DisplayOrder = 1,
                IsActive = true,
                Title = "Đà Nẵng 123"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}