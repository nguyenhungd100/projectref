﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.RoleFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.RoleFacade;

namespace Domain.Role.Tests
{
    [TestClass()]
    public class RoleServiceTests
    {
        RoleService citySvc = new RoleService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new RoleModel()
            {
                Description = "Quản trị hệ thống",
                Title = "Quản trị hệ thống",
                RoleName = "Administrators"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}