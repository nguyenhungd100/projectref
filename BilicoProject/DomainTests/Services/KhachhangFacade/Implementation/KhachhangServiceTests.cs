﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.KhachhangFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Khachhang.Tests
{
    [TestClass()]
    public class KhachhangServiceTests
    {
        KhachhangService citySvc = new KhachhangService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(null);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTest2()
        {
            var res = citySvc.Add(new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {

            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTest3()
        {
            var res = citySvc.Add(new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {
                Email = "abc@gmail.com",

            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTest4()
        {
            var res = citySvc.Add(new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {
                Hoten = "Hoang Van Binh",
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTest5()
        {
            var res = citySvc.Add(new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {
                Hoten = "Hoang Van Binh",
                Mobile = "091231233"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddTest6()
        {
            var res = citySvc.Add(new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {
                Hoten = "Hoang Van Binh",
                Mobile = "091231236",
                Nguoitao = 1
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }



        [TestMethod()]
        public void AddSanphamQuantamTest()
        {
            var res = citySvc.AddSanphamQuantam(1, new int[] { 1 });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }



        [TestMethod()]
        public void AddTuvanTest()
        {
            var res = citySvc.AddTuvan(null);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTuvanTest2()
        {
            var res = citySvc.AddTuvan(new BILICO.IMS.Domain.Services.KhachhangFacade.TuvanModel());
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTuvanTest3()
        {
            var res = citySvc.AddTuvan(new BILICO.IMS.Domain.Services.KhachhangFacade.TuvanModel()
            {
                SanphamTuvanId = 1
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTuvanTest4()
        {
            var res = citySvc.AddTuvan(new BILICO.IMS.Domain.Services.KhachhangFacade.TuvanModel()
            {
                SanphamTuvanId = 1,
                NoidungTuvan = "Tư vấn"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTuvanTest41()
        {
            var res = citySvc.AddTuvan(new BILICO.IMS.Domain.Services.KhachhangFacade.TuvanModel()
            {
                SanphamTuvanId = 1,
                NoidungTuvan = "Tư vấn",
                Nguoitao = 1,
                KhachhangId = 1,
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTuvanTest5()
        {
            var res = citySvc.AddTuvan(new BILICO.IMS.Domain.Services.KhachhangFacade.TuvanModel()
            {
                SanphamTuvanId = 1,
                NoidungTuvan = "Tư vấn",
                Nguoitao = 1,
                KhachhangId = 1,
                PhanXuly = true
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddTuvanTest6()
        {
            var res = citySvc.AddTuvan(new BILICO.IMS.Domain.Services.KhachhangFacade.TuvanModel()
            {
                SanphamTuvanId = 1,
                NoidungTuvan = "Tư vấn",
                Nguoitao = 1,
                KhachhangId = 1,
                PhanXuly = true,
                NguoiXulyId = 1,
                NoidungGiaoviec = "tu van tiep",
                HanXuly = DateTime.Now.AddDays(1)
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }



        [TestMethod()]
        public void DeleteTest()
        {
            var res = citySvc.Delete(new int[] { 1 });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTuvanTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetBaogiaDetailTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetBaogiasTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetSanphamQuantamsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetTuvansTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ImportTest()
        {
            var res = citySvc.Import(new List<BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel>(){ new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {
                Hoten = "Hoang Van Binh",
                Mobile = "091231237",
                Nguoitao = 1
            },
            new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {
                Hoten = "Hoang Van Binh",
                Mobile = "091231238",
                Nguoitao = 1
            },
            new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {
                Hoten = "Hoang Van Binh",
                Mobile = "091231239",
                Nguoitao = 1
            }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void Move2SaleTest()
        {
            //var res = citySvc.Move2Sale(2, new int[] { 2 });
            //res.Wait();
            //Assert.IsTrue(res.Result == true);
        }

        [TestMethod()]
        public void RemoveSanphamQuantamTest()
        {
            var res = citySvc.RemoveSanphamQuantam(new int[] { 1 });
            res.Wait();
            Assert.IsTrue(res.Result.Result == true);
        }




        [TestMethod()]
        public void SaveBaogiaTest()
        {
            var res = citySvc.SaveBaogia(null);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveBaogiaTest2()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel());
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveBaogiaTest3()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel()
            {
                Baogia = new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaModel()
                {

                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveBaogiaTest4()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel()
            {
                Baogia = new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaModel()
                {
                    NoidungBaogia = "Báo giá lần 1"
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveBaogiaTest5()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel()
            {
                Baogia = new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaModel()
                {
                    NoidungBaogia = "Báo giá lần 1"
                },

                Sanphams = new List<BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel(){
                        SanphamId = 1,
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveBaogiaTest6()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel()
            {
                Baogia = new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaModel()
                {
                    NoidungBaogia = "Báo giá lần 1"
                },

                Sanphams = new List<BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel(){
                        SanphamId = 1,
                        Soluong = 2,
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveBaogiaTest7()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel()
            {
                Baogia = new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaModel()
                {
                    NoidungBaogia = "Báo giá lần 1",
                    KhachhangId = 1,
                    Nguoitao = 1
                },

                Sanphams = new List<BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel(){
                        SanphamId = 1,
                        Soluong = 2,
                        Gia = 300000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void SaveBaogiaTest8()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel()
            {
                Baogia = new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaModel()
                {
                    NoidungBaogia = "Báo giá lần 1",
                    KhachhangId = 1,
                    Nguoitao = 1
                },

                Sanphams = new List<BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel(){
                        SanphamId = 1,
                        Soluong = 2,
                        Gia = 300000
                    }
                },

                Dichvus = new List<BILICO.IMS.Domain.Services.KhachhangFacade.DichvuBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.DichvuBaogiaModel()
                    {
                        Dichvu = "Lắp đặt",
                        Gia = 1100000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveBaogiaTest9()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel()
            {
                Baogia = new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaModel()
                {
                    NoidungBaogia = "Báo giá lần 5",
                    KhachhangId = 1,
                    Nguoitao = 1
                },

                Sanphams = new List<BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel(){
                        SanphamId = 1,
                        Soluong = 2,
                        Gia = 300000
                    },
                    new BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel(){
                        SanphamId = 2,
                        Soluong = 1,
                        Gia = 100000
                    }
                },

                Dichvus = new List<BILICO.IMS.Domain.Services.KhachhangFacade.DichvuBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.DichvuBaogiaModel()
                    {
                        Dichvu = "Lắp đặt",
                        Soluong = 1,
                        Gia = 1100000
                    },
                    new BILICO.IMS.Domain.Services.KhachhangFacade.DichvuBaogiaModel()
                    {
                        Dichvu = "Thiết kế",
                        Soluong = 1,
                        Gia = 5100000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void SaveBaogiaTest10()
        {
            var res = citySvc.SaveBaogia(new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaDetailModel()
            {
                Baogia = new BILICO.IMS.Domain.Services.KhachhangFacade.BaogiaModel()
                {
                    BaogiaId = 5,
                    NoidungBaogia = "Báo giá lần 5.1",
                    KhachhangId = 1,
                    Nguoicapnhat = 1
                },

                Sanphams = new List<BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel(){
                        Id = 5,
                        SanphamId = 1,
                        Soluong = 1,
                        Gia = 150000
                    },
                    new BILICO.IMS.Domain.Services.KhachhangFacade.SanphamBaogiaModel(){
                        SanphamId = 3,
                        Soluong = 2,
                        Gia = 200000
                    }
                },

                Dichvus = new List<BILICO.IMS.Domain.Services.KhachhangFacade.DichvuBaogiaModel>()
                {
                    new BILICO.IMS.Domain.Services.KhachhangFacade.DichvuBaogiaModel()
                    {
                        Id = 2,
                        Dichvu = "Nhân công lắp đặt",
                        Soluong = 1,
                        Gia = 1200000
                    },
                    new BILICO.IMS.Domain.Services.KhachhangFacade.DichvuBaogiaModel()
                    {
                        Dichvu = "Thiết kế bản vẽ",
                        Soluong = 1,
                        Gia = 600000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }




        [TestMethod()]
        public void SaveNhucauTest()
        {
            var res = citySvc.SaveNhucau(0, null);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveNhucauTest2()
        {
            var res = citySvc.SaveNhucau(1, null);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveNhucauTest3()
        {
            var res = citySvc.SaveNhucau(1, new int[0]);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveNhucauTest4()
        {
            var res = citySvc.SaveNhucau(1, new int[] { 1 });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }




        [TestMethod()]
        public void UpdateTest()
        {
            var res = citySvc.Update(new BILICO.IMS.Domain.Services.KhachhangFacade.KhachhangModel()
            {
                KhachhangId = 2,
                Hoten = "Le Minh Tu",
                Mobile = "091231233"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateTuvanTest()
        {
            var res = citySvc.UpdateTuvan(new BILICO.IMS.Domain.Services.KhachhangFacade.TuvanModel()
            {
                Id = 3,
                SanphamTuvanId = 1,
                NoidungTuvan = "Tư vấn lần 3",
                Nguoitao = 1,
                KhachhangId = 1,
                PhanXuly = true,
                NguoiXulyId = 1,
                NoidungGiaoviec = "tu van tiep",
                HanXuly = DateTime.Now.AddDays(1)
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }
    }
}