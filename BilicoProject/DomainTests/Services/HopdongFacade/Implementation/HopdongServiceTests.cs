﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.HopdongFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.HopdongFacade;

namespace Domain.Hopdong.Tests
{
    [TestClass()]
    public class HopdongServiceTests
    {
        HopdongService citySvc = new HopdongService();

        [TestMethod()]
        public void AddDanhgiaNoiboTest()
        {
            var res = citySvc.AddDanhgiaNoibo(null);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddDanhgiaNoiboTest2()
        {
            var res = citySvc.AddDanhgiaNoibo(new HopdongDanhgiaNoiboModel());
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddDanhgiaNoiboTest3()
        {
            var res = citySvc.AddDanhgiaNoibo(new HopdongDanhgiaNoiboModel()
            {
                HopdongId = 1
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddDanhgiaNoiboTest4()
        {
            var res = citySvc.AddDanhgiaNoibo(new HopdongDanhgiaNoiboModel()
            {
                HopdongId = 1,
                KythuatId = 1
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddDanhgiaNoiboTest5()
        {
            var res = citySvc.AddDanhgiaNoibo(new HopdongDanhgiaNoiboModel()
            {
                HopdongId = 1,
                KythuatId = 1,
                Chatluong = 4,
                Chiphi = 6,
                PhanhoiKH = 5,
                Tiendo = 8,
                Noidung = "Trung bình"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddDukienchiTest()
        {
            var res = citySvc.AddDukienchi(null);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }


        [TestMethod()]
        public void AddDukienchiTest2()
        {
            var res = citySvc.AddDukienchi(new HopdongDukienchiModel());
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddDukienchiTest3()
        {
            var res = citySvc.AddDukienchi(new HopdongDukienchiModel()
            {
                HopdongId = 1
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddDukienchiTest4()
        {
            var res = citySvc.AddDukienchi(new HopdongDukienchiModel()
            {
                HopdongId = 1,
                Sotien = 250000
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void AddDukienchiTest5()
        {
            var res = citySvc.AddDukienchi(new HopdongDukienchiModel()
            {
                HopdongId = 1,
                Sotien = 250000,
                Lydochi = "Mua vật liệu"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddDukienhangcanthietTest()
        {
            var res = citySvc.AddDukienhangcanthiet(new HopdongDukienhangcanthietModel()
            {
                HopdongId = 1,
                Chungloai = "Vách ngăn",
                TenMathang = "Vách ngăn xốp",
                Thoigiancan = new DateTime(2018, 2, 22),
                Tylechot = 60
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddDukienthuTest()
        {
            var res = citySvc.AddDukienthu(new HopdongDukienthuModel()
            {
                HopdongId = 1,
                Sotien = 250000,
                Noidungthu = "Thu tạm ứng lần 1 của khách",
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddGiaoviecTest()
        {
            var res = citySvc.AddGiaoviec(new HopdongGiaoviecModel()
            {
                HopdongId = 1,
                KehoachId = 1,
                NoidungCongviec = "Lắp kim phun",
                Batdau = DateTime.Now,
                Ketthuc = DateTime.Now.AddDays(4),
                NguoithuchienId = 1
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddKehoachTest()
        {
            var res = citySvc.AddKehoach(new HopdongKehoachModel()
            {
                HopdongId = 1,
                NoidungThuchien = "Lắp đặt tủ điện",
                Batdau = DateTime.Now,
                Ketthuc = DateTime.Now.AddDays(4),
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddNhansuTest()
        {
            var res = citySvc.AddNhansu(new HopdongNhansuModel()
            {
                HopdongId = 1,
                UserId = 1,
                Vaitro = "Công nhân"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddNhatkythicongTest()
        {
            var res = citySvc.AddNhatkythicong(new HopdongNhatkythicongModel()
            {
                HopdongId = 1,
                KehoachId = 1,
                Noidungbaocao = "dựng sà gồ",
                LoaiCongviec = LoaiBaocaoCongviec.Thoi_gian_cho_vat_tu,
                NguoibaocaoId = 1,
                AnhDinhkem = new byte[0]
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddPhanhoiTest()
        {
            var res = citySvc.AddPhanhoi(new HopdongPhanhoiModel()
            {
                HopdongId = 1,
                Nhanxet = "Làm tốt",
                KythuatId = 1,
                Ketqua = true,
                Ngaythicong = DateTime.Now
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddQuyettoanTest()
        {
            var res = citySvc.AddQuyettoan(new HopdongQuyettoanModel()
            {
                HopdongId = 1,
                Ghichu = "Quyết toán lần 2",
                NguoiQuyettoanId = 1,
                NguoiDuyetQuyettoanId = 1,
                NgayQuyettoan = DateTime.Now,
                Chiphis = new List<ChiphiQuyettoanModel>()
                {
                    new ChiphiQuyettoanModel()
                    {
                        LoaiChiphi = LoaiChiphi.Nha_nghi,
                        Sotien = 500000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddSucoTest()
        {
            var res = citySvc.AddSuco(new HopdongSucoModel()
            {
                HopdongId = 1,
                AnhHientruong = new byte[1],
                LoaiSuco = LoaiSuco.Do_khach_hang,
                //Suco = "Gãy đường ống thoát nước",
                ThoidiemXayra = DateTime.Now.Subtract(TimeSpan.FromDays(3)),

                NguoithuchienId = 1,
                PhuonganKhacphuc = "Sửa chữa",
                ThoidiemKhacphuc = DateTime.Now,
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddTailieuDinhkemTest()
        {
            var res = citySvc.AddTailieuDinhkem(new HopdongTailieuDinhkemModel()
            {
                HopdongId = 1,
                KehoachId = 1,
                NguoitaoId = 1,
                AnhCongtrinhs = new List<HopdongAnhCongtrinhModel>()
                {
                    new HopdongAnhCongtrinhModel()
                    {
                        //AnhCongtrinh = new byte[4]
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddTamungTest()
        {
            var res = citySvc.AddTamung(new HopdongTamungModel()
            {
                HopdongId = 1,
                Ghichu = "Tạm ứng lần 2",
                Ngaytamung = DateTime.Now,
                NguoichoTamungId = 1,
                NguoitamungId = 1,
                Sotien = 300000
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddThanhtoanTest()
        {
            var res = citySvc.AddThanhtoan(new HopdongThanhtoanModel()
            {
                HopdongId = 1,
                Sotien = 300000,
                NoidungThanhtoan = "Thanh toán abc",
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void AddVattuTest()
        {
            var res = citySvc.AddVattu(new HopdongVattuModel()
            {
                HopdongId = 1,
                Ghichu = "Mua thêm",
                Ngaygiao = DateTime.Now,
                NguoigiaoId = 1,
                NguoinhanId = 1,
                Vattu = "Kìm dụng cụ"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterDanhgiaNoiboTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterDukienchiTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterDukienhangcanthietTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterDukienthuTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterGiaoviecTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterKehoachTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterNhansuTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterNhatkythicongTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterPhanhoiTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterQuyettoanTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterSucoTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterTailieuDinhkemTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterTamungTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterThanhtoanTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FilterVattuTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetHopdongDetailTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetHopdongsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveDanhgiaNoiboTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveDukienchiTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveDukienhangcanthietTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveDukienthuTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveGiaoviecTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveKehoachTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveNhansuTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveNhatkythicongTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemovePhanhoiTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveQuyettoanTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveSucoTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveTailieuDinhkemTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveTamungTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveThanhtoanTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void RemoveVattuTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SaveHopdongTest()
        {
            var res = citySvc.SaveHopdong(null);
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveHopdongTest2()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel());
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveHopdongTest3()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel()
            {
                Hopdong = new HopdongModel()
                {

                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveHopdongTest4()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel()
            {
                Hopdong = new HopdongModel()
                {
                    SoHD = "HD123123123"
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveHopdongTest5()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel()
            {
                Hopdong = new HopdongModel()
                {
                    SoHD = "HD123123123",
                    KhachhangId = 1,
                },

                Sanphams = new List<SanphamHopdongModel>()
                {
                    new SanphamHopdongModel(){
                        SanphamId = 1,
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveHopdongTest6()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel()
            {
                Hopdong = new HopdongModel()
                {
                    SoHD = "HD123123123",
                    KhachhangId = 1,
                },

                Sanphams = new List<SanphamHopdongModel>()
                {
                    new SanphamHopdongModel(){
                        SanphamId = 1,
                        Soluong = 2,
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveHopdongTest7()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel()
            {
                Hopdong = new HopdongModel()
                {
                    SoHD = "HD0123123123",
                    KhachhangId = 1,
                    Nguoitao = 1
                },

                Sanphams = new List<SanphamHopdongModel>()
                {
                    new SanphamHopdongModel(){
                        SanphamId = 1,
                        Soluong = 2,
                        Gia = 300000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void SaveHopdongTest8()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel()
            {
                Hopdong = new HopdongModel()
                {
                    SoHD = "HD12123123",
                    Ghichu = "Báo giá lần 1",
                    KhachhangId = 1,
                    Nguoitao = 1
                },

                Sanphams = new List<SanphamHopdongModel>()
                {
                    new SanphamHopdongModel(){
                        SanphamId = 1,
                        Soluong = 2,
                        Gia = 300000
                    }
                },

                Dichvus = new List<DichvuHopdongModel>()
                {
                    new DichvuHopdongModel()
                    {
                        Dichvu = "Lắp đặt",
                        Gia = 1100000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == false);
        }

        [TestMethod()]
        public void SaveHopdongTest9()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel()
            {
                Hopdong = new HopdongModel()
                {
                    SoHD = "HD12123123",
                    Ghichu = "Báo giá lần 5",
                    KhachhangId = 1,
                    Nguoitao = 1
                },

                Sanphams = new List<SanphamHopdongModel>()
                {
                    new SanphamHopdongModel(){
                        SanphamId = 1,
                        Soluong = 2,
                        Gia = 300000
                    },
                    new SanphamHopdongModel(){
                        SanphamId = 2,
                        Soluong = 1,
                        Gia = 100000
                    }
                },

                Dichvus = new List<DichvuHopdongModel>()
                {
                    new DichvuHopdongModel()
                    {
                        Dichvu = "Lắp đặt",
                        Soluong = 1,
                        Gia = 1100000
                    },
                    new DichvuHopdongModel()
                    {
                        Dichvu = "Thiết kế",
                        Soluong = 1,
                        Gia = 5100000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void SaveHopdongTest10()
        {
            var res = citySvc.SaveHopdong(new HopdongDetailModel()
            {
                Hopdong = new HopdongModel()
                {
                    HopdongId = 5,
                    SoHD = "HD234234234",
                    Ghichu = "Báo giá lần 5.1",
                    KhachhangId = 1,
                    Nguoicapnhat = 1
                },

                Sanphams = new List<SanphamHopdongModel>()
                {
                    new SanphamHopdongModel(){
                        Id = 8,
                        SanphamId = 1,
                        Soluong = 1,
                        Gia = 150000
                    },
                    new SanphamHopdongModel(){
                        SanphamId = 3,
                        Soluong = 2,
                        Gia = 200000
                    }
                },

                Dichvus = new List<DichvuHopdongModel>()
                {
                    new DichvuHopdongModel()
                    {
                        Id = 1,
                        Dichvu = "Nhân công lắp đặt",
                        Soluong = 1,
                        Gia = 1200000
                    },
                    new DichvuHopdongModel()
                    {
                        Dichvu = "Thiết kế bản vẽ",
                        Soluong = 1,
                        Gia = 600000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateDanhgiaNoiboTest()
        {
            var res = citySvc.UpdateDanhgiaNoibo(new HopdongDanhgiaNoiboModel()
            {
                Id = 1,
                HopdongId = 1,
                KythuatId = 1,
                Chatluong = 9,
                Chiphi = 8,
                PhanhoiKH = 8,
                Tiendo = 10,
                Noidung = "Khá tốt"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateDukienchiTest()
        {
            var res = citySvc.UpdateDukienchi(new HopdongDukienchiModel()
            {
                Id = 1,
                HopdongId = 1,
                Sotien = 150000,
                Lydochi = "Mua phế liệu"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateDukienhangcanthietTest()
        {
            var res = citySvc.UpdateDukienhangcanthiet(new HopdongDukienhangcanthietModel()
            {
                Id = 1,
                HopdongId = 1,
                Chungloai = "Vách ngăn 123",
                TenMathang = "Vách ngăn xốp 123",
                Thoigiancan = new DateTime(2018, 2, 22),
                Tylechot = 60
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateDukienthuTest()
        {
            var res = citySvc.UpdateDukienthu(new HopdongDukienthuModel()
            {
                Id = 1,
                HopdongId = 1,
                Sotien = 250000,
                Noidungthu = "Thu tạm ứng lần 1 của khách",
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateGiaoviecTest()
        {
            var res = citySvc.UpdateGiaoviec(new HopdongGiaoviecModel()
            {
                Id = 1,
                HopdongId = 1,
                KehoachId = 1,
                NoidungCongviec = "Lắp kim phun 123",
                Batdau = DateTime.Now,
                Ketthuc = DateTime.Now.AddDays(4),
                NguoithuchienId = 1
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateKehoachTest()
        {
            var res = citySvc.UpdateKehoach(new HopdongKehoachModel()
            {
                Id = 1,
                HopdongId = 1,
                NoidungThuchien = "Lắp đặt tủ điện 123",
                Batdau = DateTime.Now,
                Ketthuc = DateTime.Now.AddDays(4),
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateNhansuTest()
        {
            var res = citySvc.UpdateNhansu(new HopdongNhansuModel()
            {
                Id = 1,
                HopdongId = 1,
                UserId = 1,
                Vaitro = "Giám sát"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateNhatkythicongTest()
        {
            var res = citySvc.UpdateNhatkythicong(new HopdongNhatkythicongModel()
            {
                Id = 1,
                HopdongId = 1,
                KehoachId = 1,
                Noidungbaocao = "dựng sà gồ 123",
                LoaiCongviec = LoaiBaocaoCongviec.Di_tinh,
                NguoibaocaoId = 1,
                AnhDinhkem = new byte[0],
                Batdau = DateTime.Now,
                Ketthuc = DateTime.Now.AddDays(2)
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdatePhanhoiTest()
        {
            var res = citySvc.UpdatePhanhoi(new HopdongPhanhoiModel()
            {
                Id = 1,
                HopdongId = 1,
                Nhanxet = "Làm tốt lắm",
                KythuatId = 1,
                Ketqua = true,
                Ngaythicong = DateTime.Now
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateQuyettoanTest()
        {
            var res = citySvc.UpdateQuyettoan(new HopdongQuyettoanModel()
            {
                Id = 1,
                HopdongId = 1,
                Ghichu = "Quyết toán lần 2.1",
                NguoiQuyettoanId = 1,
                NguoiDuyetQuyettoanId = 1,
                NgayQuyettoan = DateTime.Now,
                Chiphis = new List<ChiphiQuyettoanModel>()
                {
                    new ChiphiQuyettoanModel()
                    {
                        Id = 1,
                        LoaiChiphi = LoaiChiphi.Di_lai,
                        Sotien = 200000
                    },
                    new ChiphiQuyettoanModel()
                    {
                        LoaiChiphi = LoaiChiphi.An_uong,
                        Sotien = 300000
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateSucoTest()
        {
            var res = citySvc.UpdateSuco(new HopdongSucoModel()
            {
                Id = 1,
                HopdongId = 1,
                AnhHientruong = new byte[1],
                LoaiSuco = LoaiSuco.Do_ky_thuat,
                //Suco = "Gãy đường ống thoát nước 1.2",
                ThoidiemXayra = DateTime.Now.Subtract(TimeSpan.FromDays(3)),

                NguoithuchienId = 1,
                PhuonganKhacphuc = "Sửa chữa lại",
                ThoidiemKhacphuc = DateTime.Now,
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateTailieuDinhkemTest()
        {
            var res = citySvc.UpdateTailieuDinhkem(new HopdongTailieuDinhkemModel()
            {
                Id = 1,
                HopdongId = 1,
                KehoachId = 1,
                NguoitaoId = 1,
                AnhCongtrinhs = new List<HopdongAnhCongtrinhModel>()
                {
                    new HopdongAnhCongtrinhModel()
                    {
                        Id = 1,
                        //AnhCongtrinh = new byte[0]
                    }
                }
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateTamungTest()
        {
            var res = citySvc.UpdateTamung(new HopdongTamungModel()
            {
                Id = 1,
                HopdongId = 1,
                Ghichu = "Tạm ứng lần 2.1",
                Ngaytamung = DateTime.Now,
                NguoichoTamungId = 1,
                NguoitamungId = 1,
                Sotien = 400000
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateThanhtoanTest()
        {
            var res = citySvc.UpdateThanhtoan(new HopdongThanhtoanModel()
            {
                Id = 1,
                HopdongId = 1,
                Sotien = 300000,
                NoidungThanhtoan = "Thanh toán 123",
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void UpdateVattuTest()
        {
            var res = citySvc.UpdateVattu(new HopdongVattuModel()
            {
                Id = 1,
                HopdongId = 1,
                Ghichu = "Mua thêm vật tư",
                Ngaygiao = DateTime.Now,
                NguoigiaoId = 1,
                NguoinhanId = 1,
                Vattu = "Kìm dụng cụ 123"
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }
    }
}