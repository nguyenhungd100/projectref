﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BILICO.IMS.Domain.Services.NhucaugiaFacade.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Services.NhucaugiaFacade;

namespace Domain.Nhucaugia.Tests
{
    [TestClass()]
    public class NhucaugiaServiceTests
    {
        NhucaugiaService citySvc = new NhucaugiaService();

        [TestMethod()]
        public void AddTest()
        {
            var res = citySvc.Add(new NhucaugiaModel()
            {
                TenNhucau = "Dealer 1",
                Hoatdong = true
            });
            res = citySvc.Add(new NhucaugiaModel()
            {
                TenNhucau = "Dealer 2",
                Hoatdong = true
            });
            res = citySvc.Add(new NhucaugiaModel()
            {
                TenNhucau = "Dealer 3",
                Hoatdong = true
            });
            res.Wait();
            Console.WriteLine(res.Result.Error);
            Assert.IsTrue(res.Result.Result == true);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ListTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void UpdateTest()
        {
            Assert.Fail();
        }
    }
}