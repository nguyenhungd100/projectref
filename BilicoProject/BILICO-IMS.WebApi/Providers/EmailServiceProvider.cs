﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BILICO.IMS.WebApi.Providers
{
    public class EmailServiceProvider
    {
        private string _alias;
        private string _authenticateUser;
        private string _authenticatePass;

        public EmailServiceProvider()
        {
            //var config = (new SMSConfigService()).Single(c => true == true);
            //if (config == null)
            //    throw new Exception("Chưa cấu hình dịch vụ SMS");

            //_alias = config.SMSServer;
            //_authenticateUser = config.Username;
            //_authenticatePass = config.Password;
        }

        public EmailServiceProvider(string alias, string authenticateUser, string authenticatePass)
        {
            _alias = alias;
            _authenticateUser = authenticateUser;
            _authenticatePass = authenticatePass;
        }

        public string Send(string emailReceiver, string title, string body)
        {
            if (String.IsNullOrEmpty(_alias) || String.IsNullOrEmpty(_authenticateUser) || String.IsNullOrEmpty(_authenticatePass))
            {
                throw new Exception("Chưa cấu hình dịch vụ Email");
            }

            //if (distributor == null || String.IsNullOrEmpty(distributor.Mobile))
            //{
            //    return "Khách hàng không có số điện thoại";
            //}

            //message = TemplateConfig.ReplaceSigned(message, distributor.HoTen, khachhang.Gioitinh, khachhang.Ngaysinh, khachhang.Diachi, khachhang.VGCOIN, khachhang.Mobile);

            //message = StringUtil.Encode(message, EncodeSeperator.Space, false, false);

            //var res = (new SMSBrandNameService.VMGAPISoapClient()).BulkSendSms(khachhang.Mobile, _alias, message, sendTime, _authenticateUser, _authenticatePass);

            //if (res.error_code != 0)
            //    return res.error_detail;

            return string.Empty;
        }
    }
}