﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BILICO.IMS.WebApi.Helpers
{
    public class FileResult : IHttpActionResult
    {
        private readonly string _filePath;
        private readonly byte[] _contents;
        private readonly string _contentType;

        public FileResult(string filePath, string contentType = null)
        {
            if (filePath == null) throw new ArgumentNullException("filePath");

            _filePath = filePath;
            _contentType = contentType;
        }
        public FileResult(byte[] contents, string contentType)
        {
            if (contents == null) throw new ArgumentNullException("contents");

            _contents = contents;
            _contentType = contentType;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response;

            if (_filePath != null)
            {
                response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(File.OpenRead(_filePath))
                };
            }
            else
            {
                var mem = new MemoryStream(_contents);
                response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(mem)
                };
            }

            var contentType = _contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(_filePath));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);

            return Task.FromResult(response);
        }
    }
}