﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using IdentityServer3.AccessTokenValidation;
using log4net;
using BILICO.IMS.WebApi.Helpers;
using System.Web.Http.ExceptionHandling;
using Newtonsoft.Json;
using System.Web.Http.Cors;
using System.Net;
using System.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Web.Helpers;
using BILICO.IMS.Framework.Configuration;
using BILICO.IMS.WebApi.Authorization;

[assembly: OwinStartup(typeof(BILICO.IMS.WebApi.Startup))]

namespace BILICO.IMS.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AntiForgeryConfig.UniqueClaimTypeIdentifier = "sub";
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = ApiConfiguration.AuthenticateServer + "/oauth2",
                RequiredScopes = new[] { "bil.ims.Api" }
            });

            //Set authorization manager for api
            app.UseResourceAuthorization(new AuthorizationManager());

            // web api configuration
            var config = new HttpConfiguration();

            config.Filters.Add(new GlobalExceptionHandler());

            app.UseWebApi(config);

            //config route
            config.MapHttpAttributeRoutes();

            // Swagger
            SwaggerConfig.Register(config);

            // Allow cross-domain requests
            var cors = new EnableCorsAttribute("*", "*", "GET, POST, PUT, DELETE, OPTIONS");
            config.EnableCors(cors);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
        }
    }
}
