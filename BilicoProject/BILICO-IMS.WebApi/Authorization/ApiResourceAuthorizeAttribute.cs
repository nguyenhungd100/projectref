﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Thinktecture.IdentityModel.WebApi;
using BILICO.IMS.Domain.Services.RoleFacade;

namespace BILICO.IMS.WebApi.Authorization
{
    /// <summary>
    /// Attribute for authorize user access
    /// </summary>
    public class ApiAuthorize : ResourceAuthorizeAttribute
    {
        public ApiAuthorize() : base()
        {

        }

        public ApiAuthorize(PermissionCode permission) : base(permission.ToString())
        {

        }
    }
}