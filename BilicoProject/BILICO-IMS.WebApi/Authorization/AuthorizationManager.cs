﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Thinktecture.IdentityModel.Owin.ResourceAuthorization;
using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Domain.Services.RoleFacade.Implementation;
using BILICO.IMS.Domain.Services.TaikhoanFacade;
using BILICO.IMS.Domain.Services.TaikhoanFacade.Implementation;
using BILICO.IMS.WebApi.Helpers;

namespace BILICO.IMS.WebApi.Authorization
{
    /// <summary>
    /// Authorize user when access api
    /// </summary>
    public class AuthorizationManager : ResourceAuthorizationManager
    {
        private IRoleService _roleService = new RoleService();
        private ITaikhoanService _taikhoanService = new TaikhoanService();

        public override Task<bool> CheckAccessAsync(ResourceAuthorizationContext context)
        {
            if (!context.Principal.Identity.IsAuthenticated)
                throw new HttpResponseException(HttpStatusCode.Unauthorized);

            if (!IsValidToken(context))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            if (!context.Action.Any(c => c.Type == "name"))
                return Ok();

            return HasPermissionDoThisAction(context, context.Action.First(c => c.Type == "name").Value);
        }

        private bool IsValidToken(ResourceAuthorizationContext context)
        {
            var timeStamp = context.Principal.Claims.FirstOrDefault(c => c.Type == "user_modified_time_stamp");
            var userId = context.Principal.Identity.GetUserId();
            var user = _taikhoanService.GetById(userId);
            if (user == null)
            {
                return false;
            }
            if (user.UserModifiedTimeStamp.ToString() == timeStamp.Value)
            {
                return true;
            }
            return false;
        }

        private async Task<bool> HasPermissionDoThisAction(ResourceAuthorizationContext context, string action)
        {
            var roles = context.Principal.Claims.FirstOrDefault(c => c.Type == "role");
            var permissions = context.Principal.Claims.FirstOrDefault(c => c.Type == "permissions");

            var permission = (PermissionCode)Enum.Parse(typeof(PermissionCode), action);
            var result = permissions != null && !String.IsNullOrEmpty(permissions.Value) ? permissions.Value.Contains($";{permission.GetHashCode()};") : false;

            if (result == false)
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            else
                return await Eval(result);
        }
    }
}