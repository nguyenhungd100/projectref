﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BILICO.IMS.WebApi.Modules
{
    public class ResponseResultBase<T>
    {
        public bool result { set; get; }

        public string message { set; get; }

        public T data { set; get; }

        public int error_code { set; get; }
    }
}