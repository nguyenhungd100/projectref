
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.ReportFacade;
using BILICO.IMS.Domain.Services.ReportFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;

namespace BILICO.IMS.WebApi.Modules.Reports.V1.Controllers
{
    /// <summary>
    /// API làm việc với Report
    /// </summary>

    [Route("v1/baocao")]
    public class BaocaoController : ApiController
    {
        IReportService _baocaoService = new ReportService();

        /// <summary>
        /// Báo cáo thống kê theo khách hàng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Report</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/baocao/report_customer")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<KhachhangStatisticModel>))]
        public async Task<IHttpActionResult> ReportCustomer(CustomerFilterModel filter)
        {
            var res = await _baocaoService.ReportCustomer(filter);

            return Json(new ResponseResultBase<KhachhangStatisticModel>() { result = true, data = res });
        }

        /// <summary>
        /// Báo cáo thống kê theo lợi nhuận
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/baocao/report_total_profit")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ProfitStatisticModel>))]
        public async Task<IHttpActionResult> ReportTotalProfitAsync(ProfitFilterModel filter)
        {
            var res = await _baocaoService.ReportTotalProfitAsync(filter);

            return Json(new ResponseResultBase<ProfitStatisticModel>() { result = true, data = res });
        }

        /// <summary>
        /// Báo cáo thi công
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/baocao/report_construction")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<List<ConstructionStatisticModel>>))]
        public async Task<IHttpActionResult> ReportConstruction(ConstructionFilterModel filter)
        {
            var res = await _baocaoService.ReportConstruction(filter);

            return Json(new ResponseResultBase<List<ConstructionStatisticModel>>() { result = true, data = res });
        }

        /// <summary>
        /// Báo cáo chi phí
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/baocao/report_cost")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<List<CostStatisticModel>>))]
        public async Task<IHttpActionResult> ReportCost(CostFilterModel filter)
        {
            var res = await _baocaoService.ReportCost(filter);

            return Json(new ResponseResultBase<List<CostStatisticModel>>() { result = true, data = res });
        }

        /// <summary>
        /// Báo cáo khu vực
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/baocao/report_region")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<List<RegionStatisticModel>>))]
        public async Task<IHttpActionResult> ReportRegion(RegionFilterModel filter)
        {
            var res = await _baocaoService.ReportRegion(filter);

            return Json(new ResponseResultBase<List<RegionStatisticModel>>() { result = true, data = res });
        }


        /// <summary>
        /// Báo cáo tiến độ
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/baocao/report_progress")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ProjectStatisticModel>))]
        public async Task<IHttpActionResult> ReportProgress(ProgressFilterModel filter)
        {
            var res = await _baocaoService.ReportProgress(filter);

            return Json(new ResponseResultBase<ProjectStatisticModel>() { result = true, data = res });
        }


        /// <summary>
        /// Báo cáo chi tiết lợi nhuận
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/baocao/report_profit_detail")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<List<ProfitDetailStatisticModel>>))]
        public async Task<IHttpActionResult> ReportProfitDetail(ProfitFilterModel filter)
        {
            var res = await _baocaoService.ReportProfitDetail(filter);

            return Json(new ResponseResultBase<List<ProfitDetailStatisticModel>>() { result = true, data = res });
        }
    }
}

