
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.HopdongFacade;
using BILICO.IMS.Domain.Services.HopdongFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;
using System.Configuration;

namespace BILICO.IMS.WebApi.Modules.Hopdongs.V1.Controllers
{
    /// <summary>
    /// API làm việc với Hopdong
    /// </summary>

    [Route("v1/hopdong")]
    public class HopdongController : ApiController
    {
        IHopdongService _hopdongService = new HopdongService();

        /// <summary>
        /// Lấy danh sách Hopdong trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hopdong</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_hop_dongs")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongModel>>))]
        public async Task<IHttpActionResult> GetHopdongs(FilterModel filter)
        {
            var res = await _hopdongService.GetHopdongs(filter);

            return Json(new ResponseResultBase<ListPager<HopdongModel>>() { result = true, data = new ListPager<HopdongModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Xem tiến độ hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/xem_tien_do")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<TiendoHopdongModel>))]
        public async Task<IHttpActionResult> Xemtiendo(TiendoFilterModel filter)
        {
            var res = await _hopdongService.Xemtiendo(filter);

            return Json(new ResponseResultBase<TiendoHopdongModel>() { result = true, data = res });
        }

        /// <summary>
        /// Hoàn thành hợp đồng
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_HOP_DONG)]
        [Route("v1/hopdong/hoan_thanh")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> HoanthanhHopdong(int hopdongId)
        {
            var res = await _hopdongService.HoanthanhHopdongAsync(hopdongId);

            return Json(new ResponseResultBase<bool>() { result = res.Result });
        }

        /// <summary>
        /// Xóa Hopdong
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] ids)
        {
            var res = await _hopdongService.Delete(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hopdong theo Id
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<HopdongDetailModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hopdongId)
        {
            var res = await _hopdongService.GetHopdongDetail(hopdongId);

            return Json(new ResponseResultBase<HopdongDetailModel>() { result = true, data = res });
        }

        /// <summary>
        /// Lấy báo cáo lợi nhuận của hợp đồng
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<HopdongBaocaoLoinhuanModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/bao_cao_loi_nhuan")]
        [HttpGet]
        public async Task<IHttpActionResult> BaocaoLoinhuanAsync(int hopdongId)
        {
            var res = await _hopdongService.BaocaoLoinhuanAsync(hopdongId);

            return Json(new ResponseResultBase<HopdongBaocaoLoinhuanModel>() { result = true, data = res });
        }

        /// <summary>
        /// Lấy thông tin kế hoạch theo Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<HopdongKehoachModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/get_ke_hoach_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetKehoachById(int Id)
        {
            var res = await _hopdongService.GetKehoachByIdAsync(Id);

            return Json(new ResponseResultBase<HopdongKehoachModel>() { result = true, data = res });
        }

        /// <summary>
        /// Lấy danh sách sản phẩm đặt mua của hợp đồng
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<List<SanphamHopdongModel>>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/get_san_pham_by_hop_dong_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSanphamByHopdongIdAsync(int hopdongId)
        {
            var res = await _hopdongService.GetSanphamByHopdongIdAsync(hopdongId);

            return Json(new ResponseResultBase<List<SanphamHopdongModel>>() { result = true, data = res });
        }



        /// <summary>
        /// Tìm sản phẩm theo mã đã đặt mua của hợp đồng
        /// </summary>
        /// <param name="ma_san_pham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<List<SanphamHopdongModel>>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/search_sanpham_datmua")]
        [HttpGet]
        public async Task<IHttpActionResult> SearchSanphamDatmuaAsync(string ma_san_pham)
        {
            var res = await _hopdongService.SearchSanphamDatmuaAsync(ma_san_pham);

            return Json(new ResponseResultBase<List<SanphamHopdongModel>>() { result = true, data = res });
        }

        /// <summary>
        /// Tạo hoặc cập nhật hợp đồng
        /// </summary>
        /// <param name="hopdong"></param>
        /// <returns></returns>
        ///  /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_HOP_DONG)]
        [Route("v1/hopdong/save_hop_dong")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> SaveHopdong(HopdongDetailModel hopdong)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();
            if (hopdong.Hopdong.HopdongId <= 0)
                hopdong.Hopdong.Nguoitao = userId;
            else
                hopdong.Hopdong.Nguoicapnhat = userId;

            var res = await _hopdongService.SaveHopdong(hopdong);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        #region ke hoach
        /// <summary>
        /// Lấy danh sách kế hoạch trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_kehoachs")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongKehoachModel>>))]
        public async Task<IHttpActionResult> FilterKehoach(FilterHopdongBase filter)
        {
            
            var res = await _hopdongService.FilterKehoach(filter);

            return Json(new ResponseResultBase<ListPager<HopdongKehoachModel>>() { result = true, data = new ListPager<HopdongKehoachModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm kế hoạch mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_kehoach")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddKehoach(HopdongKehoachModel kehoach)
        {           
            var userId = RequestContext.Principal.Identity.GetUserId();

            kehoach.NguoitaoId = userId;

            var res = await _hopdongService.AddKehoach(kehoach);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật kế hoạch
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_kehoach")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateKehoach(HopdongKehoachModel kehoach)
        {
            var res = await _hopdongService.UpdateKehoach(kehoach);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }


        /// <summary>
        /// Chuyển hợp đồng cho kỹ thuật triển khai
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_HOP_DONG)]
        [Route("v1/hopdong/move_2_tech")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> Move2TechAsync(int contractId)
        {
            var res = await _hopdongService.Move2TechAsync(contractId);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa kế hoạch trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_kehoach")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveKehoach(int[] ids)
        {
            var res = await _hopdongService.RemoveKehoach(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region nhan su
        /// <summary>
        /// Lấy danh sách nhân sự trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_nhansus")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongNhansuModel>>))]
        public async Task<IHttpActionResult> FilterNhansu(FilterHopdongBase filter)
        {
            var res = await _hopdongService.FilterNhansu(filter);

            return Json(new ResponseResultBase<ListPager<HopdongNhansuModel>>() { result = true, data = new ListPager<HopdongNhansuModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="nhansu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_nhansu")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddNhansu(HopdongNhansuModel nhansu)
        {
            var res = await _hopdongService.AddNhansu(nhansu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="nhansu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_nhansu")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateNhansu(HopdongNhansuModel nhansu)
        {
            var res = await _hopdongService.UpdateNhansu(nhansu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa nhân sự trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_nhansu")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveNhansu(int[] ids)
        {
            var res = await _hopdongService.RemoveNhansu(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region giao viec
        /// <summary>
        /// Lấy danh sách giao việc trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_giaoviecs")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongGiaoviecModel>>))]
        public async Task<IHttpActionResult> FilterGiaoviec(FilterHopdongBase filter)
        {
            var res = await _hopdongService.FilterGiaoviec(filter);

            return Json(new ResponseResultBase<ListPager<HopdongGiaoviecModel>>() { result = true, data = new ListPager<HopdongGiaoviecModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm giao việc mới
        /// </summary>
        /// <param name="giaoviec"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_giaoviec")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddGiaoviec(HopdongGiaoviecModel giaoviec)
        {
            var res = await _hopdongService.AddGiaoviec(giaoviec);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật giao việc
        /// </summary>
        /// <param name="giaoviec"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_giaoviec")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateGiaoviec(HopdongGiaoviecModel giaoviec)
        {
            var res = await _hopdongService.UpdateGiaoviec(giaoviec);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa giao việc trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_giaoviec")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveGiaoviec(int[] ids)
        {
            var res = await _hopdongService.RemoveGiaoviec(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region nhat ky thi cong
        /// <summary>
        /// Lấy danh sách nhật ký thi công trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_nhat_ky_thi_congs")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongNhatkythicongModel>>))]
        public async Task<IHttpActionResult> FilterNhatkythicong(FilterNhatkythicongModel filter)
        {
            var res = await _hopdongService.FilterNhatkythicong(filter);

            return Json(new ResponseResultBase<ListPager<HopdongNhatkythicongModel>>() { result = true, data = new ListPager<HopdongNhatkythicongModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm nhật ký thi công mới
        /// </summary>
        /// <param name="nhat_ky_thi_cong"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_nhat_ky_thi_cong")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddNhatkythicong(HopdongNhatkythicongModel nhat_ky_thi_cong)
        {
            var res = await _hopdongService.AddNhatkythicong(nhat_ky_thi_cong);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật nhật ký thi công
        /// </summary>
        /// <param name="nhat_ky_thi_cong"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_nhat_ky_thi_cong")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateNhatkythicong(HopdongNhatkythicongModel nhat_ky_thi_cong)
        {
            var res = await _hopdongService.UpdateNhatkythicong(nhat_ky_thi_cong);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa nhật ký thi công trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_nhat_ky_thi_cong")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveNhatkythicong(int[] ids)
        {
            var res = await _hopdongService.RemoveNhatkythicong(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region thanh toan
        /// <summary>
        /// Lấy danh sách thanh toán trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_thanhtoans")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongThanhtoanModel>>))]
        public async Task<IHttpActionResult> FilterThanhtoan(FilterHopdongBase filter)
        {
            var res = await _hopdongService.FilterThanhtoan(filter);

            return Json(new ResponseResultBase<ListPager<HopdongThanhtoanModel>>() { result = true, data = new ListPager<HopdongThanhtoanModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm thanh toán mới
        /// </summary>
        /// <param name="thanhtoan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_thanhtoan")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddThanhtoan(HopdongThanhtoanModel thanhtoan)
        {
            var res = await _hopdongService.AddThanhtoan(thanhtoan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thanh toán
        /// </summary>
        /// <param name="thanhtoan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_thanhtoan")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateThanhtoan(HopdongThanhtoanModel thanhtoan)
        {
            var res = await _hopdongService.UpdateThanhtoan(thanhtoan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa thanh toán trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_thanhtoan")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveThanhtoan(int[] ids)
        {
            var res = await _hopdongService.RemoveThanhtoan(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region du kien thu
        /// <summary>
        /// Lấy danh sách dự kiến thu trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_dukienthus")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongDukienthuModel>>))]
        public async Task<IHttpActionResult> FilterDukienthu(FilterHopdongBase filter)
        {
            var res = await _hopdongService.FilterDukienthu(filter);

            return Json(new ResponseResultBase<ListPager<HopdongDukienthuModel>>() { result = true, data = new ListPager<HopdongDukienthuModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm dự kiến thu mới
        /// </summary>
        /// <param name="dukienthu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_dukienthu")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddDukienthu(HopdongDukienthuModel dukienthu)
        {
            var res = await _hopdongService.AddDukienthu(dukienthu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật dự kiến thu
        /// </summary>
        /// <param name="dukienthu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_dukienthu")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateDukienthu(HopdongDukienthuModel dukienthu)
        {
            var res = await _hopdongService.UpdateDukienthu(dukienthu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa dự kiến thu trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_dukienthu")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveDukienthu(int[] ids)
        {
            var res = await _hopdongService.RemoveDukienthu(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region du kien chi
        /// <summary>
        /// Lấy danh sách dự kiến chi trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_dukienchis")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongDukienchiModel>>))]
        public async Task<IHttpActionResult> FilterDukienchi(FilterHopdongBase filter)
        {
            var res = await _hopdongService.FilterDukienchi(filter);

            return Json(new ResponseResultBase<ListPager<HopdongDukienchiModel>>() { result = true, data = new ListPager<HopdongDukienchiModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm dự kiến chi mới
        /// </summary>
        /// <param name="dukienchi"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_dukienchi")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddDukienchi(HopdongDukienchiModel dukienchi)
        {
            var res = await _hopdongService.AddDukienchi(dukienchi);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật dự kiến chi
        /// </summary>
        /// <param name="dukienchi"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_dukienchi")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateDukienchi(HopdongDukienchiModel dukienchi)
        {
            var res = await _hopdongService.UpdateDukienchi(dukienchi);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa dự kiến chi trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_dukienchi")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveDukienchi(int[] ids)
        {
            var res = await _hopdongService.RemoveDukienchi(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region du kien hang can thiet
        /// <summary>
        /// Lấy danh sách dự kiến hàng cần thiết trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_dukienhang_canthiets")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongDukienhangcanthietModel>>))]
        public async Task<IHttpActionResult> FilterDukienhangcanthiet(FilterHopdongBase filter)
        {
            var res = await _hopdongService.FilterDukienhangcanthiet(filter);

            return Json(new ResponseResultBase<ListPager<HopdongDukienhangcanthietModel>>() { result = true, data = new ListPager<HopdongDukienhangcanthietModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm dự kiến hàng cần thiết mới
        /// </summary>
        /// <param name="dukienhang_canthiet"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_dukienhang_canthiet")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddDukienhangcanthiet(HopdongDukienhangcanthietModel dukienhang_canthiet)
        {
            var res = await _hopdongService.AddDukienhangcanthiet(dukienhang_canthiet);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật dự kiến hàng cần thiết
        /// </summary>
        /// <param name="dukienhang_canthiet"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_dukienhang_canthiet")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateDukienhangcanthiet(HopdongDukienhangcanthietModel dukienhang_canthiet)
        {
            var res = await _hopdongService.UpdateDukienhangcanthiet(dukienhang_canthiet);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa dự kiến hàng cần thiết trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_dukienhang_canthiet")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveDukienhangcanthiet(int[] ids)
        {
            var res = await _hopdongService.RemoveDukienhangcanthiet(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region vat tu
        /// <summary>
        /// Lấy danh sách vật tư trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_vattus")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongVattuModel>>))]
        public async Task<IHttpActionResult> FilterVattu(FilterHopdongBase filter)
        {
            var res = await _hopdongService.FilterVattu(filter);

            return Json(new ResponseResultBase<ListPager<HopdongVattuModel>>() { result = true, data = new ListPager<HopdongVattuModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm vật tư mới
        /// </summary>
        /// <param name="vattu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_vattu")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddVattu(HopdongVattuModel vattu)
        {
            var res = await _hopdongService.AddVattu(vattu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật vật tư
        /// </summary>
        /// <param name="vattu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_vattu")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateVattu(HopdongVattuModel vattu)
        {
            var res = await _hopdongService.UpdateVattu(vattu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa vật tư trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_vattu")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveVattu(int[] ids)
        {
            var res = await _hopdongService.RemoveVattu(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region su co
        /// <summary>
        /// Lấy danh sách sự cố trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_sucos")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongSucoModel>>))]
        public async Task<IHttpActionResult> FilterSuco(FilterSucoModel filter)
        {
            var res = await _hopdongService.FilterSuco(filter);

            return Json(new ResponseResultBase<ListPager<HopdongSucoModel>>() { result = true, data = new ListPager<HopdongSucoModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm sự cố mới
        /// </summary>
        /// <param name="suco"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_suco")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddSuco(HopdongSucoModel suco)
        {
            var res = await _hopdongService.AddSuco(suco);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật sự cố
        /// </summary>
        /// <param name="suco"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_suco")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateSuco(HopdongSucoModel suco)
        {
            var res = await _hopdongService.UpdateSuco(suco);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa sự cố trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_suco")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveSuco(int[] ids)
        {
            var res = await _hopdongService.RemoveSuco(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region tam ung


        /// <summary>
        /// Lấy danh sách tạm ứng chậm quyết toán theo hợp đồng
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/get_tamung_cham_quyet_toans")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongTamungModel>>))]
        public async Task<IHttpActionResult> GetTamungChamquyettoansAsync(int hopdongId)
        {
            var res = await _hopdongService.GetTamungChamquyettoansAsync(hopdongId);

            return Json(new ResponseResultBase<List<HopdongTamungModel>>() { result = true, data = res });
        }

        /// <summary>
        /// Lấy danh sách tạm ứng trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_tamungs")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongTamungModel>>))]
        public async Task<IHttpActionResult> FilterTamung(FilterTamungModel filter)
        {
            var res = await _hopdongService.FilterTamung(filter);

            return Json(new ResponseResultBase<ListPager<HopdongTamungModel>>() { result = true, data = new ListPager<HopdongTamungModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy tạm ứng theo người quyết toán
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <param name="nguoiQuyettoanId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/get_tamung_by_nguoi_quyettoanId")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<HopdongTamungModel>))]
        public async Task<IHttpActionResult> GetTamungByNguoiQuyettoanId(int hopdongId, int nguoiQuyettoanId)
        {
            var res = await _hopdongService.GetTamungByNguoiQuyettoanIdAsync(hopdongId, nguoiQuyettoanId);

            return Json(new ResponseResultBase<HopdongTamungModel>() { result = true, data = res });
        }

        /// <summary>
        /// Thêm tạm ứng mới
        /// </summary>
        /// <param name="tamung"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_tamung")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddTamung(HopdongTamungModel tamung)
        {
            var res = await _hopdongService.AddTamung(tamung);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật tạm ứng
        /// </summary>
        /// <param name="tamung"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_tamung")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateTamung(HopdongTamungModel tamung)
        {
            var res = await _hopdongService.UpdateTamung(tamung);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa tạm ứng trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_tamung")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveTamung(int[] ids)
        {
            var res = await _hopdongService.RemoveTamung(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region quyet toan
        /// <summary>
        /// Lấy danh sách quyết toán trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_quyettoans")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongQuyettoanModel>>))]
        public async Task<IHttpActionResult> FilterQuyettoan(FilterQuyettoanModel filter)
        {
            var res = await _hopdongService.FilterQuyettoan(filter);

            return Json(new ResponseResultBase<ListPager<HopdongQuyettoanModel>>() { result = true, data = new ListPager<HopdongQuyettoanModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm quyết toán mới
        /// </summary>
        /// <param name="quyettoan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_quyettoan")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddQuyettoan(HopdongQuyettoanModel quyettoan)
        {
            var res = await _hopdongService.AddQuyettoan(quyettoan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật quyết toán
        /// </summary>
        /// <param name="quyettoan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_quyettoan")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateQuyettoan(HopdongQuyettoanModel quyettoan)
        {
            var res = await _hopdongService.UpdateQuyettoan(quyettoan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa quyết toán trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_quyettoan")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveQuyettoan(int[] ids)
        {
            var res = await _hopdongService.RemoveQuyettoan(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region tai lieu dinh kem
        /// <summary>
        /// Lấy danh sách tài liệu đính kèm trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_tailieu_dinhkems")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongTailieuDinhkemModel>>))]
        public async Task<IHttpActionResult> FilterTailieuDinhkem(FilterTailieuDinhkemModel filter)
        {
            var res = await _hopdongService.FilterTailieuDinhkem(filter);

            res.ForEach(c =>
            {
                c.AnhCongtrinhs.ForEach(k =>
                {
                    k.AnhCongtrinh = ConfigurationManager.AppSettings["ApiServer"] + k.AnhCongtrinh;
                });
            });

            return Json(new ResponseResultBase<ListPager<HopdongTailieuDinhkemModel>>() { result = true, data = new ListPager<HopdongTailieuDinhkemModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm tài liệu đính kèm mới
        /// </summary>
        /// <param name="tailieu_dinhkem"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_HOP_DONG)]
        [Route("v1/hopdong/add_tailieu_dinhkem")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddTailieuDinhkem(HopdongTailieuDinhkemModel tailieu_dinhkem)
        {
            if (!String.IsNullOrEmpty(tailieu_dinhkem.FileKey))
            {
                var path = Path.Combine(HttpContext.Current.Server.MapPath("/Upload/Temp"), tailieu_dinhkem.FileKey);

                var files = Directory.GetFiles(path);

                if (files != null && files.Length > 0)
                {
                    var savePath = Path.Combine(HttpContext.Current.Server.MapPath("/Upload/Data"), tailieu_dinhkem.FileKey);

                    if (!Directory.Exists(savePath))
                    {
                        Directory.CreateDirectory(savePath);
                    }

                    tailieu_dinhkem.AnhCongtrinhs = new List<HopdongAnhCongtrinhModel>();
                    foreach (var file in files)
                    {
                        File.Copy(file, Path.Combine(savePath, Path.GetFileName(file)));
                        tailieu_dinhkem.AnhCongtrinhs.Add(new HopdongAnhCongtrinhModel()
                        {
                            AnhCongtrinh = "/Upload/Data/" + tailieu_dinhkem.FileKey + "/" + Path.GetFileName(file)
                        });
                    }//foreach
                }
            }

            var res = await _hopdongService.AddTailieuDinhkem(tailieu_dinhkem);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật tài liệu đính kèm
        /// </summary>
        /// <param name="tailieu_dinhkem"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_HOP_DONG)]
        [Route("v1/hopdong/update_tailieu_dinhkem")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateTailieuDinhkem(HopdongTailieuDinhkemModel tailieu_dinhkem)
        {
            if (!String.IsNullOrEmpty(tailieu_dinhkem.FileKey))
            {
                var path = Path.Combine(HttpContext.Current.Server.MapPath("/Upload/Temp"), tailieu_dinhkem.FileKey);

                var files = Directory.GetFiles(path);

                if (files != null && files.Length > 0)
                {
                    var savePath = Path.Combine(HttpContext.Current.Server.MapPath("/Upload/Data"), tailieu_dinhkem.FileKey);

                    tailieu_dinhkem.AnhCongtrinhs = new List<HopdongAnhCongtrinhModel>();
                    foreach (var file in files)
                    {
                        File.Copy(file, Path.Combine(savePath, Path.GetFileName(file)));
                        tailieu_dinhkem.AnhCongtrinhs.Add(new HopdongAnhCongtrinhModel()
                        {
                            AnhCongtrinh = "/Upload/Data/" + tailieu_dinhkem.FileKey + "/" + Path.GetFileName(file)
                        });
                    }//foreach
                }
            }

            var res = await _hopdongService.UpdateTailieuDinhkem(tailieu_dinhkem);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa tài liệu đính kèm trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_tailieu_dinhkem")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveTailieuDinhkem(int[] ids)
        {
            var res = await _hopdongService.RemoveTailieuDinhkem(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region phan hoi
        /// <summary>
        /// Lấy danh sách phản hồi trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_phanhois")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongPhanhoiModel>>))]
        public async Task<IHttpActionResult> FilterPhanhoi(FilterPhanhoiModel filter)
        {
            var res = await _hopdongService.FilterPhanhoi(filter);

            return Json(new ResponseResultBase<ListPager<HopdongPhanhoiModel>>() { result = true, data = new ListPager<HopdongPhanhoiModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm phản hồi mới
        /// </summary>
        /// <param name="phanhoi"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_phanhoi")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddPhanhoi(HopdongPhanhoiModel phanhoi)
        {
            var res = await _hopdongService.AddPhanhoi(phanhoi);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật phản hồi
        /// </summary>
        /// <param name="phanhoi"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_phanhoi")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdatePhanhoi(HopdongPhanhoiModel phanhoi)
        {
            var res = await _hopdongService.UpdatePhanhoi(phanhoi);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa phản hồi trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_phanhoi")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemovePhanhoi(int[] ids)
        {
            var res = await _hopdongService.RemovePhanhoi(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion

        #region danh gia noi bo
        /// <summary>
        /// Lấy danh sách đánh giá nội bộ trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HOP_DONG)]
        [Route("v1/hopdong/list_danhgia_noibos")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<HopdongDanhgiaNoiboModel>>))]
        public async Task<IHttpActionResult> FilterDanhgiaNoibo(FilterDanhgiaNoiboModel filter)
        {
            var res = await _hopdongService.FilterDanhgiaNoibo(filter);

            return Json(new ResponseResultBase<ListPager<HopdongDanhgiaNoiboModel>>() { result = true, data = new ListPager<HopdongDanhgiaNoiboModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm đánh giá nội bộ mới
        /// </summary>
        /// <param name="danhgia_noibo"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/add_danhgia_noibo")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> AddDanhgiaNoibo(HopdongDanhgiaNoiboModel danhgia_noibo)
        {
            var res = await _hopdongService.AddDanhgiaNoibo(danhgia_noibo);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật đánh giá nội bộ
        /// </summary>
        /// <param name="danhgia_noibo"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TRIEN_KHAI_HOP_DONG)]
        [Route("v1/hopdong/update_danhgia_noibo")]
        [HttpPut]
        [ResponseType(typeof(ResponseResultBase<bool>))]
        public async Task<IHttpActionResult> UpdateDanhgiaNoibo(HopdongDanhgiaNoiboModel danhgia_noibo)
        {
            var res = await _hopdongService.UpdateDanhgiaNoibo(danhgia_noibo);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa đánh giá nội bộ trong hợp đồng
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HOP_DONG)]
        [Route("v1/hopdong/remove_danhgia_noibo")]
        [HttpDelete]
        public async Task<IHttpActionResult> RemoveDanhgiaNoibo(int[] ids)
        {
            var res = await _hopdongService.RemoveDanhgiaNoibo(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }
        #endregion
    }
}

