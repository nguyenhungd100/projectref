
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Hopdong_DukienchiFacade;
using BILICO.IMS.Domain.Services.Hopdong_DukienchiFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Hopdong_Dukienchis.V1.Controllers
{
	/// <summary>
    /// API làm việc với Hopdong_Dukienchi
    /// </summary>

    [Route("v1/hopdong_dukienchi")]
    public class Hopdong_DukienchiController : ApiController
    {
        IHopdong_DukienchiService _hopdong_dukienchiService = new Hopdong_DukienchiService();
		
        /// <summary>
        /// Lấy danh sách Hopdong_Dukienchi trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hopdong_Dukienchi</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/hopdong_dukienchi/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Hopdong_DukienchiModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _hopdong_dukienchiService.List(filter);

            return Json(new ResponseResultBase<ListPager<Hopdong_DukienchiModel>>() { result = true, data = new ListPager<Hopdong_DukienchiModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Hopdong_Dukienchi mới
        /// </summary>
        /// <param name="hopdong_dukienchi"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienchi/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Hopdong_DukienchiModel hopdong_dukienchi)
        {
            var res = await _hopdong_dukienchiService.Add(hopdong_dukienchi);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Hopdong_Dukienchi
        /// </summary>
        /// <param name="hopdong_dukienchi"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienchi/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Hopdong_DukienchiModel hopdong_dukienchi)
        {
            var res = await _hopdong_dukienchiService.Update(hopdong_dukienchi);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Hopdong_Dukienchi
        /// </summary>
        /// <param name="hopdong_dukienchiId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienchi/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _hopdong_dukienchiService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hopdong_Dukienchi theo Id
        /// </summary>
        /// <param name="hopdong_dukienchiId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Hopdong_DukienchiModel>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienchi/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hopdong_dukienchiId)
        {
            var res = await _hopdong_dukienchiService.GetById(hopdong_dukienchiId);

            return Json(new ResponseResultBase<Hopdong_DukienchiModel>() { result = true, data = res });
        }
    }
}

