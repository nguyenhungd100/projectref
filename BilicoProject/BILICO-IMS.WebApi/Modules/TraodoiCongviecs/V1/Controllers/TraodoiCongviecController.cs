
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.TraodoiCongviecFacade;
using BILICO.IMS.Domain.Services.TraodoiCongviecFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;

namespace BILICO.IMS.WebApi.Modules.TraodoiCongviecs.V1.Controllers
{
    /// <summary>
    /// API làm việc với TraodoiCongviec
    /// </summary>

    [Route("v1/traodoicongviec")]
    public class TraodoiCongviecController : ApiController
    {
        ITraodoiCongviecService _traodoicongviecService = new TraodoiCongviecService();

        /// <summary>
        /// Lấy danh sách TraodoiCongviec trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý TraodoiCongviec</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/traodoicongviec/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<TraodoiCongviecModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _traodoicongviecService.List(filter);

            return Json(new ResponseResultBase<ListPager<TraodoiCongviecModel>>() { result = true, data = new ListPager<TraodoiCongviecModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm TraodoiCongviec mới
        /// </summary>
        /// <param name="traodoicongviec"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/traodoicongviec/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(TraodoiCongviecModel traodoicongviec)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            traodoicongviec.NguoitaoId = userId;

            var res = await _traodoicongviecService.Add(traodoicongviec);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin TraodoiCongviec
        /// </summary>
        /// <param name="traodoicongviec"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/traodoicongviec/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(TraodoiCongviecModel traodoicongviec)
        {
            var res = await _traodoicongviecService.Update(traodoicongviec);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa TraodoiCongviec
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/traodoicongviec/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _traodoicongviecService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin TraodoiCongviec theo Id
        /// </summary>
        /// <param name="traodoicongviecId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<TraodoiCongviecModel>))]
        [ApiAuthorize]
        [Route("v1/traodoicongviec/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int traodoicongviecId)
        {
            var res = await _traodoicongviecService.GetById(traodoicongviecId);

            return Json(new ResponseResultBase<TraodoiCongviecModel>() { result = true, data = res });
        }
    }
}

