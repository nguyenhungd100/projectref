
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.NhacungcapFacade;
using BILICO.IMS.Domain.Services.NhacungcapFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Nhacungcaps.V1.Controllers
{
	/// <summary>
    /// API làm việc với Nhacungcap
    /// </summary>

    [Route("v1/nhacungcap")]
    public class NhacungcapController : ApiController
    {
        INhacungcapService _nhacungcapService = new NhacungcapService();

        /// <summary>
        /// Lấy danh sách Nhacungcap trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Nhacungcap</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_NHA_CUNG_CAP)]
        [Route("v1/nhacungcap/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<NhacungcapModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _nhacungcapService.List(filter);

            return Json(new ResponseResultBase<ListPager<NhacungcapModel>>() { result = true, data = new ListPager<NhacungcapModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Nhacungcap mới
        /// </summary>
        /// <param name="nhacungcap"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_NHA_CUNG_CAP)]
        [Route("v1/nhacungcap/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(NhacungcapModel nhacungcap)
        {
            var res = await _nhacungcapService.Add(nhacungcap);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Nhacungcap
        /// </summary>
        /// <param name="nhacungcap"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_NHA_CUNG_CAP)]
        [Route("v1/nhacungcap/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(NhacungcapModel nhacungcap)
        {
            var res = await _nhacungcapService.Update(nhacungcap);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Nhacungcap
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_NHA_CUNG_CAP)]
        [Route("v1/nhacungcap/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _nhacungcapService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Nhacungcap theo Id
        /// </summary>
        /// <param name="nhacungcapId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<NhacungcapModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_NHA_CUNG_CAP)]
        [Route("v1/nhacungcap/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int nhacungcapId)
        {
            var res = await _nhacungcapService.GetById(nhacungcapId);

            return Json(new ResponseResultBase<NhacungcapModel>() { result = true, data = res });
        }
    }
}

