﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.WebApi.Modules
{
    public class ListPager<T>
    {
        public List<T> list { set; get; }

        public PagingInfo pager { set; get; }
    }

    //public class ResponseResultBase<T>
    //{
    //    public string message { set; get; }
    //    public bool result { set; get; }
    //    public int error_code { set; get; }
    //    public T data { set; get; }
    //}
}