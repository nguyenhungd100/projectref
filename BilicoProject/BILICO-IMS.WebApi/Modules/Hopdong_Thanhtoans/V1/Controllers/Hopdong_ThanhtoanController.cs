
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Hopdong_ThanhtoanFacade;
using BILICO.IMS.Domain.Services.Hopdong_ThanhtoanFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Hopdong_Thanhtoans.V1.Controllers
{
	/// <summary>
    /// API làm việc với Hopdong_Thanhtoan
    /// </summary>

    [Route("v1/hopdong_thanhtoan")]
    public class Hopdong_ThanhtoanController : ApiController
    {
        IHopdong_ThanhtoanService _hopdong_thanhtoanService = new Hopdong_ThanhtoanService();
		
        /// <summary>
        /// Lấy danh sách Hopdong_Thanhtoan trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hopdong_Thanhtoan</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/hopdong_thanhtoan/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Hopdong_ThanhtoanModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _hopdong_thanhtoanService.List(filter);

            return Json(new ResponseResultBase<ListPager<Hopdong_ThanhtoanModel>>() { result = true, data = new ListPager<Hopdong_ThanhtoanModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Hopdong_Thanhtoan mới
        /// </summary>
        /// <param name="hopdong_thanhtoan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_thanhtoan/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Hopdong_ThanhtoanModel hopdong_thanhtoan)
        {
            var res = await _hopdong_thanhtoanService.Add(hopdong_thanhtoan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Hopdong_Thanhtoan
        /// </summary>
        /// <param name="hopdong_thanhtoan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_thanhtoan/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Hopdong_ThanhtoanModel hopdong_thanhtoan)
        {
            var res = await _hopdong_thanhtoanService.Update(hopdong_thanhtoan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Hopdong_Thanhtoan
        /// </summary>
        /// <param name="hopdong_thanhtoanId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_thanhtoan/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _hopdong_thanhtoanService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hopdong_Thanhtoan theo Id
        /// </summary>
        /// <param name="hopdong_thanhtoanId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Hopdong_ThanhtoanModel>))]
        [ApiAuthorize]
        [Route("v1/hopdong_thanhtoan/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hopdong_thanhtoanId)
        {
            var res = await _hopdong_thanhtoanService.GetById(hopdong_thanhtoanId);

            return Json(new ResponseResultBase<Hopdong_ThanhtoanModel>() { result = true, data = res });
        }
    }
}

