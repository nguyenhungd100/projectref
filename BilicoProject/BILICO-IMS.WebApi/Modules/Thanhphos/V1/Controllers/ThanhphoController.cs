
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.ThanhphoFacade;
using BILICO.IMS.Domain.Services.ThanhphoFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Thanhphos.V1.Controllers
{
	/// <summary>
    /// API làm việc với Thanhpho
    /// </summary>

    [Route("v1/thanhpho")]
    public class ThanhphoController : ApiController
    {
        IThanhphoService _thanhphoService = new ThanhphoService();

        /// <summary>
        /// Lấy danh sách Thanhpho trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Thanhpho</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_THANH_PHO)]
        [Route("v1/thanhpho/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ThanhphoModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _thanhphoService.List(filter);

            return Json(new ResponseResultBase<ListPager<ThanhphoModel>>() { result = true, data = new ListPager<ThanhphoModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Thanhpho mới
        /// </summary>
        /// <param name="thanhpho"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_THANH_PHO)]
        [Route("v1/thanhpho/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(ThanhphoModel thanhpho)
        {
            var res = await _thanhphoService.Add(thanhpho);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Thanhpho
        /// </summary>
        /// <param name="thanhpho"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_THANH_PHO)]
        [Route("v1/thanhpho/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(ThanhphoModel thanhpho)
        {
            var res = await _thanhphoService.Update(thanhpho);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Thanhpho
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_THANH_PHO)]
        [Route("v1/thanhpho/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _thanhphoService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Thanhpho theo Id
        /// </summary>
        /// <param name="thanhphoId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<ThanhphoModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_THANH_PHO)]
        [Route("v1/thanhpho/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int thanhphoId)
        {
            var res = await _thanhphoService.GetById(thanhphoId);

            return Json(new ResponseResultBase<ThanhphoModel>() { result = true, data = res });
        }
    }
}

