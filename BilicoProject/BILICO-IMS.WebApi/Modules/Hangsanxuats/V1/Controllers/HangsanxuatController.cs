
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.HangsanxuatFacade;
using BILICO.IMS.Domain.Services.HangsanxuatFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Hangsanxuats.V1.Controllers
{
	/// <summary>
    /// API làm việc với Hangsanxuat
    /// </summary>

    [Route("v1/hangsanxuat")]
    public class HangsanxuatController : ApiController
    {
        IHangsanxuatService _hangsanxuatService = new HangsanxuatService();

        /// <summary>
        /// Lấy danh sách Hangsanxuat trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hangsanxuat</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HANG_SAN_XUAT)]
        [Route("v1/hangsanxuat/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<HangsanxuatModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _hangsanxuatService.List(filter);

            return Json(new ResponseResultBase<ListPager<HangsanxuatModel>>() { result = true, data = new ListPager<HangsanxuatModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Hangsanxuat mới
        /// </summary>
        /// <param name="hangsanxuat"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_HANG_SAN_XUAT)]
        [Route("v1/hangsanxuat/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(HangsanxuatModel hangsanxuat)
        {
            var res = await _hangsanxuatService.Add(hangsanxuat);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Hangsanxuat
        /// </summary>
        /// <param name="hangsanxuat"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_HANG_SAN_XUAT)]
        [Route("v1/hangsanxuat/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(HangsanxuatModel hangsanxuat)
        {
            var res = await _hangsanxuatService.Update(hangsanxuat);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Hangsanxuat
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_HANG_SAN_XUAT)]
        [Route("v1/hangsanxuat/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _hangsanxuatService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hangsanxuat theo Id
        /// </summary>
        /// <param name="hangsanxuatId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<HangsanxuatModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_HANG_SAN_XUAT)]
        [Route("v1/hangsanxuat/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hangsanxuatId)
        {
            var res = await _hangsanxuatService.GetById(hangsanxuatId);

            return Json(new ResponseResultBase<HangsanxuatModel>() { result = true, data = res });
        }
    }
}

