
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Khachhang_NhucaugiaFacade;
using BILICO.IMS.Domain.Services.Khachhang_NhucaugiaFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Khachhang_Nhucaugias.V1.Controllers
{
	/// <summary>
    /// API làm việc với Khachhang_Nhucaugia
    /// </summary>

    [Route("v1/khachhang_nhucaugia")]
    public class Khachhang_NhucaugiaController : ApiController
    {
        IKhachhang_NhucaugiaService _khachhang_nhucaugiaService = new Khachhang_NhucaugiaService();
		
        /// <summary>
        /// Lấy danh sách Khachhang_Nhucaugia trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Khachhang_Nhucaugia</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/khachhang_nhucaugia/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Khachhang_NhucaugiaModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _khachhang_nhucaugiaService.List(filter);

            return Json(new ResponseResultBase<ListPager<Khachhang_NhucaugiaModel>>() { result = true, data = new ListPager<Khachhang_NhucaugiaModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Khachhang_Nhucaugia mới
        /// </summary>
        /// <param name="khachhang_nhucaugia"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_nhucaugia/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Khachhang_NhucaugiaModel khachhang_nhucaugia)
        {
            var res = await _khachhang_nhucaugiaService.Add(khachhang_nhucaugia);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Khachhang_Nhucaugia
        /// </summary>
        /// <param name="khachhang_nhucaugia"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_nhucaugia/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Khachhang_NhucaugiaModel khachhang_nhucaugia)
        {
            var res = await _khachhang_nhucaugiaService.Update(khachhang_nhucaugia);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Khachhang_Nhucaugia
        /// </summary>
        /// <param name="khachhang_nhucaugiaId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_nhucaugia/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _khachhang_nhucaugiaService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Khachhang_Nhucaugia theo Id
        /// </summary>
        /// <param name="khachhang_nhucaugiaId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Khachhang_NhucaugiaModel>))]
        [ApiAuthorize]
        [Route("v1/khachhang_nhucaugia/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int khachhang_nhucaugiaId)
        {
            var res = await _khachhang_nhucaugiaService.GetById(khachhang_nhucaugiaId);

            return Json(new ResponseResultBase<Khachhang_NhucaugiaModel>() { result = true, data = res });
        }
    }
}

