
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Khachhang_Baogia_ChitietDichvuFacade;
using BILICO.IMS.Domain.Services.Khachhang_Baogia_ChitietDichvuFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Khachhang_Baogia_ChitietDichvus.V1.Controllers
{
	/// <summary>
    /// API làm việc với Khachhang_Baogia_ChitietDichvu
    /// </summary>

    [Route("v1/khachhang_baogia_chitietdichvu")]
    public class Khachhang_Baogia_ChitietDichvuController : ApiController
    {
        IKhachhang_Baogia_ChitietDichvuService _khachhang_baogia_chitietdichvuService = new Khachhang_Baogia_ChitietDichvuService();
		
        /// <summary>
        /// Lấy danh sách Khachhang_Baogia_ChitietDichvu trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Khachhang_Baogia_ChitietDichvu</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietdichvu/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Khachhang_Baogia_ChitietDichvuModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _khachhang_baogia_chitietdichvuService.List(filter);

            return Json(new ResponseResultBase<ListPager<Khachhang_Baogia_ChitietDichvuModel>>() { result = true, data = new ListPager<Khachhang_Baogia_ChitietDichvuModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Khachhang_Baogia_ChitietDichvu mới
        /// </summary>
        /// <param name="khachhang_baogia_chitietdichvu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietdichvu/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Khachhang_Baogia_ChitietDichvuModel khachhang_baogia_chitietdichvu)
        {
            var res = await _khachhang_baogia_chitietdichvuService.Add(khachhang_baogia_chitietdichvu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Khachhang_Baogia_ChitietDichvu
        /// </summary>
        /// <param name="khachhang_baogia_chitietdichvu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietdichvu/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Khachhang_Baogia_ChitietDichvuModel khachhang_baogia_chitietdichvu)
        {
            var res = await _khachhang_baogia_chitietdichvuService.Update(khachhang_baogia_chitietdichvu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Khachhang_Baogia_ChitietDichvu
        /// </summary>
        /// <param name="khachhang_baogia_chitietdichvuId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietdichvu/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _khachhang_baogia_chitietdichvuService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Khachhang_Baogia_ChitietDichvu theo Id
        /// </summary>
        /// <param name="khachhang_baogia_chitietdichvuId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Khachhang_Baogia_ChitietDichvuModel>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietdichvu/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int khachhang_baogia_chitietdichvuId)
        {
            var res = await _khachhang_baogia_chitietdichvuService.GetById(khachhang_baogia_chitietdichvuId);

            return Json(new ResponseResultBase<Khachhang_Baogia_ChitietDichvuModel>() { result = true, data = res });
        }
    }
}

