﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Thinktecture.IdentityModel.WebApi;
using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Domain.Services.RoleFacade.Implementation;
using BILICO.IMS.Domain.Services.ConfigurationFacade;
using BILICO.IMS.Domain.Services.ConfigurationFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;

namespace BILICO.IMS.WebApi.Modules.Configurations.V1.Controllers
{
    /// <summary>
    /// API làm việc với cấu hình hệ thống
    /// </summary>
    [Route("v1/config")]
    public class ConfigurationController : ApiController
    {
        IConfigurationService _configurationService = new ConfigurationService();
        IRoleService _roleService = new RoleService();

        /// <summary>
        /// Lấy tất cấu hình hệ thống
        /// </summary>
        /// <returns></returns>
        /// <remarks>API quản lý cấu hình hệ thống</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<ConfigurationModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAU_HINH_HE_THONG)]
        [Route("v1/config/get_config")]
        [HttpGet]
        public async Task<IHttpActionResult> GetConfig()
        {
            var res = await _configurationService.GetConfig();

            return Json(new ResponseResultBase<ConfigurationModel>() { result = true, data = res });
        }


        /// <summary>
        /// Cập nhật cấu hình hệ thống
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/config/save_config")]
        [HttpPost]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAU_HINH_HE_THONG)]
        public async Task<IHttpActionResult> SaveConfig(ConfigurationModel configuration)
        {
            var res = await _configurationService.Save(configuration);

            return Json(new ResponseResultBase<bool> { result = res.Result, message = res.Error });
        }
    }
}