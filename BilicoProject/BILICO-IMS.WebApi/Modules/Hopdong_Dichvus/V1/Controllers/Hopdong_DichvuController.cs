
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Hopdong_DichvuFacade;
using BILICO.IMS.Domain.Services.Hopdong_DichvuFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Hopdong_Dichvus.V1.Controllers
{
	/// <summary>
    /// API làm việc với Hopdong_Dichvu
    /// </summary>

    [Route("v1/hopdong_dichvu")]
    public class Hopdong_DichvuController : ApiController
    {
        IHopdong_DichvuService _hopdong_dichvuService = new Hopdong_DichvuService();
		
        /// <summary>
        /// Lấy danh sách Hopdong_Dichvu trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hopdong_Dichvu</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/hopdong_dichvu/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Hopdong_DichvuModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _hopdong_dichvuService.List(filter);

            return Json(new ResponseResultBase<ListPager<Hopdong_DichvuModel>>() { result = true, data = new ListPager<Hopdong_DichvuModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Hopdong_Dichvu mới
        /// </summary>
        /// <param name="hopdong_dichvu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dichvu/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Hopdong_DichvuModel hopdong_dichvu)
        {
            var res = await _hopdong_dichvuService.Add(hopdong_dichvu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Hopdong_Dichvu
        /// </summary>
        /// <param name="hopdong_dichvu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dichvu/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Hopdong_DichvuModel hopdong_dichvu)
        {
            var res = await _hopdong_dichvuService.Update(hopdong_dichvu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Hopdong_Dichvu
        /// </summary>
        /// <param name="hopdong_dichvuId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dichvu/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _hopdong_dichvuService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hopdong_Dichvu theo Id
        /// </summary>
        /// <param name="hopdong_dichvuId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Hopdong_DichvuModel>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dichvu/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hopdong_dichvuId)
        {
            var res = await _hopdong_dichvuService.GetById(hopdong_dichvuId);

            return Json(new ResponseResultBase<Hopdong_DichvuModel>() { result = true, data = res });
        }
    }
}

