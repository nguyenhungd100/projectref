
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.CongviecFacade;
using BILICO.IMS.Domain.Services.CongviecFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;

namespace BILICO.IMS.WebApi.Modules.Congviecs.V1.Controllers
{
    /// <summary>
    /// API làm việc với Congviec
    /// </summary>

    [Route("v1/congviec")]
    public class CongviecController : ApiController
    {
        ICongviecService _congviecService = new CongviecService();

        /// <summary>
        /// Lấy danh sách Congviec trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Congviec</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/congviec/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<CongviecModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var res = await _congviecService.List(userId, filter);

            return Json(new ResponseResultBase<ListPager<CongviecModel>>() { result = true, data = new ListPager<CongviecModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Congviec mới
        /// </summary>
        /// <param name="congviec"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/congviec/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(CongviecModel congviec)
        {
            if (congviec.NguoithuchienId <= 0)
            {
                congviec.NguoithuchienId = RequestContext.Principal.Identity.GetUserId();
            }

            var userId = RequestContext.Principal.Identity.GetUserId();

            congviec.NguoitaoId = userId;

            var res = await _congviecService.Add(congviec);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Congviec
        /// </summary>
        /// <param name="congviec"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/congviec/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(CongviecModel congviec)
        {
            if (congviec.NguoithuchienId <= 0)
            {
                congviec.NguoithuchienId = RequestContext.Principal.Identity.GetUserId();
            }

            var res = await _congviecService.Update(congviec);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Congviec
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/congviec/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _congviecService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Congviec theo Id
        /// </summary>
        /// <param name="congviecId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<CongviecModel>))]
        [ApiAuthorize]
        [Route("v1/congviec/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int congviecId)
        {
            var res = await _congviecService.GetById(congviecId);

            return Json(new ResponseResultBase<CongviecModel>() { result = true, data = res });
        }
    }
}

