
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.DungcuFacade;
using BILICO.IMS.Domain.Services.DungcuFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Dungcus.V1.Controllers
{
    /// <summary>
    /// API làm việc với Dungcu
    /// </summary>

    [Route("v1/dungcu")]
    public class DungcuController : ApiController
    {
        IDungcuService _dungcuService = new DungcuService();

        /// <summary>
        /// Lấy danh sách Dungcu trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Dungcu</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_DUNG_CU)]
        [Route("v1/dungcu/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<DungcuModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _dungcuService.List(filter);

            return Json(new ResponseResultBase<ListPager<DungcuModel>>() { result = true, data = new ListPager<DungcuModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Dungcu mới
        /// </summary>
        /// <param name="dungcu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_DUNG_CU)]
        [Route("v1/dungcu/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(DungcuModel dungcu)
        {
            var res = await _dungcuService.Add(dungcu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Dungcu
        /// </summary>
        /// <param name="dungcu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_DUNG_CU)]
        [Route("v1/dungcu/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(DungcuModel dungcu)
        {
            var res = await _dungcuService.Update(dungcu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Dungcu
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_DUNG_CU)]
        [Route("v1/dungcu/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _dungcuService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Dungcu theo Id
        /// </summary>
        /// <param name="dungcuId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<DungcuModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_DUNG_CU)]
        [Route("v1/dungcu/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int dungcuId)
        {
            var res = await _dungcuService.GetById(dungcuId);

            return Json(new ResponseResultBase<DungcuModel>() { result = true, data = res });
        }
    }
}

