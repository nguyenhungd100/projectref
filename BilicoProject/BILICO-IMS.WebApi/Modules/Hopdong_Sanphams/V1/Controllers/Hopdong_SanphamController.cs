
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Hopdong_SanphamFacade;
using BILICO.IMS.Domain.Services.Hopdong_SanphamFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Hopdong_Sanphams.V1.Controllers
{
	/// <summary>
    /// API làm việc với Hopdong_Sanpham
    /// </summary>

    [Route("v1/hopdong_sanpham")]
    public class Hopdong_SanphamController : ApiController
    {
        IHopdong_SanphamService _hopdong_sanphamService = new Hopdong_SanphamService();
		
        /// <summary>
        /// Lấy danh sách Hopdong_Sanpham trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hopdong_Sanpham</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/hopdong_sanpham/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Hopdong_SanphamModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _hopdong_sanphamService.List(filter);

            return Json(new ResponseResultBase<ListPager<Hopdong_SanphamModel>>() { result = true, data = new ListPager<Hopdong_SanphamModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Hopdong_Sanpham mới
        /// </summary>
        /// <param name="hopdong_sanpham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_sanpham/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Hopdong_SanphamModel hopdong_sanpham)
        {
            var res = await _hopdong_sanphamService.Add(hopdong_sanpham);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Hopdong_Sanpham
        /// </summary>
        /// <param name="hopdong_sanpham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_sanpham/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Hopdong_SanphamModel hopdong_sanpham)
        {
            var res = await _hopdong_sanphamService.Update(hopdong_sanpham);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Hopdong_Sanpham
        /// </summary>
        /// <param name="hopdong_sanphamId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_sanpham/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _hopdong_sanphamService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hopdong_Sanpham theo Id
        /// </summary>
        /// <param name="hopdong_sanphamId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Hopdong_SanphamModel>))]
        [ApiAuthorize]
        [Route("v1/hopdong_sanpham/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hopdong_sanphamId)
        {
            var res = await _hopdong_sanphamService.GetById(hopdong_sanphamId);

            return Json(new ResponseResultBase<Hopdong_SanphamModel>() { result = true, data = res });
        }
    }
}

