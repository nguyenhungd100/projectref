
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.QuanHuyenFacade;
using BILICO.IMS.Domain.Services.QuanHuyenFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.QuanHuyens.V1.Controllers
{
	/// <summary>
    /// API làm việc với QuanHuyen
    /// </summary>

    [Route("v1/quanhuyen")]
    public class QuanHuyenController : ApiController
    {
        IQuanHuyenService _quanhuyenService = new QuanHuyenService();

        /// <summary>
        /// Lấy danh sách QuanHuyen trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý QuanHuyen</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_QUAN_HUYEN)]
        [Route("v1/quanhuyen/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<QuanHuyenModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _quanhuyenService.List(filter);

            return Json(new ResponseResultBase<ListPager<QuanHuyenModel>>() { result = true, data = new ListPager<QuanHuyenModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm QuanHuyen mới
        /// </summary>
        /// <param name="quanhuyen"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_QUAN_HUYEN)]
        [Route("v1/quanhuyen/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(QuanHuyenModel quanhuyen)
        {
            var res = await _quanhuyenService.Add(quanhuyen);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin QuanHuyen
        /// </summary>
        /// <param name="quanhuyen"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_QUAN_HUYEN)]
        [Route("v1/quanhuyen/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(QuanHuyenModel quanhuyen)
        {
            var res = await _quanhuyenService.Update(quanhuyen);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa QuanHuyen
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_QUAN_HUYEN)]
        [Route("v1/quanhuyen/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _quanhuyenService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin QuanHuyen theo Id
        /// </summary>
        /// <param name="quanhuyenId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<QuanHuyenModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_QUAN_HUYEN)]
        [Route("v1/quanhuyen/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int quanhuyenId)
        {
            var res = await _quanhuyenService.GetById(quanhuyenId);

            return Json(new ResponseResultBase<QuanHuyenModel>() { result = true, data = res });
        }
    }
}

