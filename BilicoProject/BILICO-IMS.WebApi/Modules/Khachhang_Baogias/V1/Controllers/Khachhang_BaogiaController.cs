
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Khachhang_BaogiaFacade;
using BILICO.IMS.Domain.Services.Khachhang_BaogiaFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Khachhang_Baogias.V1.Controllers
{
	/// <summary>
    /// API làm việc với Khachhang_Baogia
    /// </summary>

    [Route("v1/khachhang_baogia")]
    public class Khachhang_BaogiaController : ApiController
    {
        IKhachhang_BaogiaService _khachhang_baogiaService = new Khachhang_BaogiaService();
		
        /// <summary>
        /// Lấy danh sách Khachhang_Baogia trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Khachhang_Baogia</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/khachhang_baogia/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Khachhang_BaogiaModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _khachhang_baogiaService.List(filter);

            return Json(new ResponseResultBase<ListPager<Khachhang_BaogiaModel>>() { result = true, data = new ListPager<Khachhang_BaogiaModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Khachhang_Baogia mới
        /// </summary>
        /// <param name="khachhang_baogia"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Khachhang_BaogiaModel khachhang_baogia)
        {
            var res = await _khachhang_baogiaService.Add(khachhang_baogia);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Khachhang_Baogia
        /// </summary>
        /// <param name="khachhang_baogia"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Khachhang_BaogiaModel khachhang_baogia)
        {
            var res = await _khachhang_baogiaService.Update(khachhang_baogia);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Khachhang_Baogia
        /// </summary>
        /// <param name="khachhang_baogiaId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _khachhang_baogiaService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Khachhang_Baogia theo Id
        /// </summary>
        /// <param name="khachhang_baogiaId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Khachhang_BaogiaModel>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int khachhang_baogiaId)
        {
            var res = await _khachhang_baogiaService.GetById(khachhang_baogiaId);

            return Json(new ResponseResultBase<Khachhang_BaogiaModel>() { result = true, data = res });
        }
    }
}

