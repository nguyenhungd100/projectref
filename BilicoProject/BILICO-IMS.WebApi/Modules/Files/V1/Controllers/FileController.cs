using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System;
using System.IO;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Files.V1.Controllers
{
    /// <summary>
    /// API làm việc với file
    /// </summary>

    [Route("v1/file")]
    public class FileController : ApiController
    {
        /// <summary>
        /// Upload file vào thư mục temp và trả về file key
        /// </summary>
        /// <returns></returns>
        /// <remarks>API upload file</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/file/upload")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Guid>))]
        public IHttpActionResult Upload()
        {
            //handle upload
            var Request = HttpContext.Current.Request;

            if (Request.Files.Count > 0)
            {
                var temp = Guid.NewGuid();

                var path = Path.Combine(HttpContext.Current.Server.MapPath("/Upload/Temp"), temp.ToString());

                Directory.CreateDirectory(path);

                //Request.Files[0].SaveAs(Path.Combine(path, Request.Files[0].FileName));
                for (var idx = 0; idx < Request.Files.Count; idx++)
                {
                    Request.Files[idx].SaveAs(Path.Combine(path, Request.Files[idx].FileName));
                }

                return Json(new ResponseResultBase<Guid>() { result = true, data = temp });
            }//if

            return Json(new ResponseResultBase<Guid>() { result = true, data = Guid.Empty });
        }

        ///// <summary>
        ///// Upload file vào thư mục temp và trả về file key
        ///// </summary>
        ///// <returns></returns>
        ///// <remarks>API upload file</remarks>
        ///// <response code="200">Success</response>
        ///// <response code="400">Bad request</response>
        ///// <response code="500">Internal Server Error</response>
        //[ApiAuthorize]
        //[Route("v1/file/upload_multi")]
        //[HttpPost]
        //[ResponseType(typeof(ResponseResultBase<Guid>))]
        //public IHttpActionResult UploadMulti()
        //{
        //    //handle upload
        //    var Request = HttpContext.Current.Request;

        //    if (Request.Files.Count > 0)
        //    {
        //        var temp = Guid.NewGuid();

        //        var path = Path.Combine(HttpContext.Current.Server.MapPath("/Upload/Temp"), temp.ToString());

        //        Directory.CreateDirectory(path);

        //        foreach (HttpPostedFile file in Request.Files)
        //        {
        //            file.SaveAs(Path.Combine(path, file.FileName));
        //        }

        //        return Json(new ResponseResultBase<Guid>() { result = true, data = temp });
        //    }//if

        //    return Json(new ResponseResultBase<Guid>() { result = true, data = Guid.Empty });
        //}
    }
}

