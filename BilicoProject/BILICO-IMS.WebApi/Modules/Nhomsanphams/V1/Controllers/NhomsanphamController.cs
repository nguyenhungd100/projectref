
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.NhomsanphamFacade;
using BILICO.IMS.Domain.Services.NhomsanphamFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Nhomsanphams.V1.Controllers
{
	/// <summary>
    /// API làm việc với Nhomsanpham
    /// </summary>

    [Route("v1/nhomsanpham")]
    public class NhomsanphamController : ApiController
    {
        INhomsanphamService _nhomsanphamService = new NhomsanphamService();

        /// <summary>
        /// Lấy danh sách Nhomsanpham trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Nhomsanpham</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_NHOM_SAN_PHAM)]
        [Route("v1/nhomsanpham/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<NhomsanphamModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _nhomsanphamService.List(filter);

            return Json(new ResponseResultBase<ListPager<NhomsanphamModel>>() { result = true, data = new ListPager<NhomsanphamModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Nhomsanpham mới
        /// </summary>
        /// <param name="nhomsanpham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_NHOM_SAN_PHAM)]
        [Route("v1/nhomsanpham/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(NhomsanphamModel nhomsanpham)
        {
            var res = await _nhomsanphamService.Add(nhomsanpham);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Nhomsanpham
        /// </summary>
        /// <param name="nhomsanpham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_NHOM_SAN_PHAM)]
        [Route("v1/nhomsanpham/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(NhomsanphamModel nhomsanpham)
        {
            var res = await _nhomsanphamService.Update(nhomsanpham);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Nhomsanpham
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_NHOM_SAN_PHAM)]
        [Route("v1/nhomsanpham/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _nhomsanphamService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Nhomsanpham theo Id
        /// </summary>
        /// <param name="nhomsanphamId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<NhomsanphamModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_NHOM_SAN_PHAM)]
        [Route("v1/nhomsanpham/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int nhomsanphamId)
        {
            var res = await _nhomsanphamService.GetById(nhomsanphamId);

            return Json(new ResponseResultBase<NhomsanphamModel>() { result = true, data = res });
        }
    }
}

