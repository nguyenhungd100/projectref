
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Khachhang_TuvanFacade;
using BILICO.IMS.Domain.Services.Khachhang_TuvanFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Khachhang_Tuvans.V1.Controllers
{
	/// <summary>
    /// API làm việc với Khachhang_Tuvan
    /// </summary>

    [Route("v1/khachhang_tuvan")]
    public class Khachhang_TuvanController : ApiController
    {
        IKhachhang_TuvanService _khachhang_tuvanService = new Khachhang_TuvanService();
		
        /// <summary>
        /// Lấy danh sách Khachhang_Tuvan trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Khachhang_Tuvan</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/khachhang_tuvan/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Khachhang_TuvanModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _khachhang_tuvanService.List(filter);

            return Json(new ResponseResultBase<ListPager<Khachhang_TuvanModel>>() { result = true, data = new ListPager<Khachhang_TuvanModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Khachhang_Tuvan mới
        /// </summary>
        /// <param name="khachhang_tuvan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_tuvan/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Khachhang_TuvanModel khachhang_tuvan)
        {
            var res = await _khachhang_tuvanService.Add(khachhang_tuvan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Khachhang_Tuvan
        /// </summary>
        /// <param name="khachhang_tuvan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_tuvan/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Khachhang_TuvanModel khachhang_tuvan)
        {
            var res = await _khachhang_tuvanService.Update(khachhang_tuvan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Khachhang_Tuvan
        /// </summary>
        /// <param name="khachhang_tuvanId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_tuvan/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _khachhang_tuvanService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Khachhang_Tuvan theo Id
        /// </summary>
        /// <param name="khachhang_tuvanId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Khachhang_TuvanModel>))]
        [ApiAuthorize]
        [Route("v1/khachhang_tuvan/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int khachhang_tuvanId)
        {
            var res = await _khachhang_tuvanService.GetById(khachhang_tuvanId);

            return Json(new ResponseResultBase<Khachhang_TuvanModel>() { result = true, data = res });
        }
    }
}

