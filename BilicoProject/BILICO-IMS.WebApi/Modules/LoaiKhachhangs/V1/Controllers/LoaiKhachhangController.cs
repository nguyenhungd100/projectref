
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.LoaiKhachhangFacade;
using BILICO.IMS.Domain.Services.LoaiKhachhangFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.LoaiKhachhangs.V1.Controllers
{
    /// <summary>
    /// API làm việc với LoaiKhachhang
    /// </summary>

    [Route("v1/loaikhachhang")]
    public class LoaiKhachhangController : ApiController
    {
        ILoaiKhachhangService _loaikhachhangService = new LoaiKhachhangService();

        /// <summary>
        /// Lấy danh sách LoaiKhachhang trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý LoaiKhachhang</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_LOAI_KHACH_HANG)]
        [Route("v1/loaikhachhang/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<LoaiKhachhangModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _loaikhachhangService.List(filter);

            return Json(new ResponseResultBase<ListPager<LoaiKhachhangModel>>() { result = true, data = new ListPager<LoaiKhachhangModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm LoaiKhachhang mới
        /// </summary>
        /// <param name="loaikhachhang"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_LOAI_KHACH_HANG)]
        [Route("v1/loaikhachhang/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(LoaiKhachhangModel loaikhachhang)
        {
            var res = await _loaikhachhangService.Add(loaikhachhang);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin LoaiKhachhang
        /// </summary>
        /// <param name="loaikhachhang"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_LOAI_KHACH_HANG)]
        [Route("v1/loaikhachhang/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(LoaiKhachhangModel loaikhachhang)
        {
            var res = await _loaikhachhangService.Update(loaikhachhang);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa LoaiKhachhang
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_LOAI_KHACH_HANG)]
        [Route("v1/loaikhachhang/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _loaikhachhangService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin LoaiKhachhang theo Id
        /// </summary>
        /// <param name="loaikhachhangId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<LoaiKhachhangModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_LOAI_KHACH_HANG)]
        [Route("v1/loaikhachhang/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int loaikhachhangId)
        {
            var res = await _loaikhachhangService.GetById(loaikhachhangId);

            return Json(new ResponseResultBase<LoaiKhachhangModel>() { result = true, data = res });
        }
    }
}

