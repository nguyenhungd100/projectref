
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.SanphamFacade;
using BILICO.IMS.Domain.Services.SanphamFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;
using BILICO.IMS.Domain.Services;
using OfficeOpenXml;
using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Services.NhomsanphamFacade;
using BILICO.IMS.Domain.Services.NhomsanphamFacade.Implementation;
using BILICO.IMS.Domain.Services.HangsanxuatFacade;
using BILICO.IMS.Domain.Services.HangsanxuatFacade.Implementation;
using BILICO.IMS.Domain.Services.NhacungcapFacade;
using BILICO.IMS.Domain.Services.NhacungcapFacade.Implementation;
using BILICO.IMS.Domain.Services.Sanpham_GiaFacade;
using BILICO.IMS.Domain.Services.Sanpham_GiaFacade.Implementation;
using BILICO.IMS.Domain.Services.NhucaugiaFacade;
using BILICO.IMS.Domain.Services.NhucaugiaFacade.Implementation;
using log4net;
using OfficeOpenXml.Style;

namespace BILICO.IMS.WebApi.Modules.Sanphams.V1.Controllers
{
    /// <summary>
    /// API làm việc với Sanpham
    /// </summary>

    [Route("v1/sanpham")]
    public class SanphamController : ApiController
    {
        ISanphamService _sanphamService = new SanphamService();
        INhomsanphamService _nhomSanphamService = new NhomsanphamService();
        IHangsanxuatService _hangSanxuatService = new HangsanxuatService();
        INhacungcapService _nhacungcapService = new NhacungcapService();
        INhucaugiaService _nhucauGiaService = new NhucaugiaService();

        /// <summary>
        /// Lấy danh sách Sanpham trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Sanpham</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_SAN_PHAM)]
        [Route("v1/sanpham/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<SanphamModel>))]
        public async Task<IHttpActionResult> List(Domain.Services.SanphamFacade.FilterModel filter)
        {
            var res = await _sanphamService.List(filter);

            HttpContext.Current.Application["SanphamListModel"] = res;

            return Json(new ResponseResultBase<ListPager<SanphamModel>>() { result = true, data = new ListPager<SanphamModel> { list = res, pager = filter.Paging } });
        }


        private int GetColumnIndex(ExcelWorksheet ws, string columnName)
        {
            var start = ws.Dimension.Start;
            var end = ws.Dimension.End;
            for (int col = start.Column; col <= end.Column; col++)
            {
                var column = ws.Cells[start.Row, col].Text.Trim();
                if (column.ToLower() == columnName.ToLower())
                    return col;
            }

            return -1;
        }

        /// <summary>
        /// Import file excel điểm bán vào hệ thống
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/sanpham/import")]
        [ApiAuthorize(PermissionCode.THEM_SAN_PHAM)]
        [HttpPost]
        public async Task<IHttpActionResult> Import(string fileKey)
        {
            if (String.IsNullOrEmpty(fileKey))
            {
                return Json(new ResponseResultBase<bool>() { result = false, message = "File import không tồn tại" });
            }
            else
            {
                var path = Path.Combine(HttpContext.Current.Server.MapPath("/Upload/Temp"), fileKey);

                var files = Directory.GetFiles(path);

                if (files != null && files.Length > 0)
                {
                    var result = Import(new FileInfo(files[0]));

                    return Json(new ResponseResultBase<bool>() { result = result.Result, message = result.Error });
                }
                else
                {
                    return Json(new ResponseResultBase<bool>() { result = false, message = "File import không tồn tại" });
                }
            }
        }


        /// <summary>
        /// Import file excel điểm bán vào hệ thống
        /// </summary>
        /// <returns></returns>
        private ActionResult Import(FileInfo fileInfo)
        {
            //handle upload
            if (fileInfo != null)
            {
                using (ExcelPackage pck = new ExcelPackage(fileInfo))
                {
                    var ws = pck.Workbook.Worksheets[1];
                    var start = ws.Dimension.Start;
                    var end = ws.Dimension.End;

                    var maCol = GetColumnIndex(ws, "Mã Sản phẩm");
                    var tenSPCol = GetColumnIndex(ws, "Tên Sản phẩm");
                    var motaCol = GetColumnIndex(ws, "Mô tả");
                    var nhomSPCol = GetColumnIndex(ws, "Nhóm SP");
                    var nccCol = GetColumnIndex(ws, "Nhà cung cấp");
                    var hsxCol = GetColumnIndex(ws, "Hãng sản xuất");
                    var giaNhapCol = GetColumnIndex(ws, "Giá đầu vào");
                    var dealer1Col = GetColumnIndex(ws, "Dealer 1");
                    var dealer2Col = GetColumnIndex(ws, "Dealer 2");
                    var giaDacbietCol = GetColumnIndex(ws, "Giá đặc biệt");
                    var giaBanleCol = GetColumnIndex(ws, "Bán lẻ");
                    var duanCol = GetColumnIndex(ws, "Dự án");

                    if (maCol < 0 || tenSPCol < 0 || motaCol < 0 || nhomSPCol < 0 || nccCol < 0 || hsxCol < 0 || giaNhapCol < 0 || dealer1Col < 0 || dealer2Col < 0 || giaDacbietCol < 0 || giaBanleCol < 0 || duanCol < 0)
                        return new ActionResult() { Result = false, Error = "File không hợp lệ" };

                    var results = new List<ActionResult>();
                    for (int row = start.Row + 1; row <= end.Row; row++)
                    {
                        var sanpham = new SanphamModel();

                        var maSP = ws.Cells[row, maCol].Text.Trim();
                        var tenSP = ws.Cells[row, tenSPCol].Text.Trim();
                        var mota = ws.Cells[row, motaCol].Text.Trim();
                        var nhomSP = ws.Cells[row, nhomSPCol].Text.Trim();
                        var ncc = ws.Cells[row, nccCol].Text.Trim();
                        var hsx = ws.Cells[row, hsxCol].Text.Trim();
                        var giaNhap = ws.Cells[row, giaNhapCol].Text.Trim();
                        var dealer1 = ws.Cells[row, dealer1Col].Text.Trim();
                        var dealer2 = ws.Cells[row, dealer2Col].Text.Trim();
                        var giaDacbiet = ws.Cells[row, giaDacbietCol].Text.Trim();
                        var giaBanle = ws.Cells[row, giaBanleCol].Text.Trim();
                        var duan = ws.Cells[row, duanCol].Text.Trim();

                        sanpham.MaSanpham = maSP;
                        sanpham.TenSanpham = tenSP;
                        sanpham.Mota = mota;
                        sanpham.Giadauvao = TypeSafeConverter.ToDecimal(giaNhap);
                        sanpham.NguoitaoId = RequestContext.Principal.Identity.GetUserId();
                        sanpham.Ngaytao = DateTime.Now;

                        if (String.IsNullOrEmpty(maSP))
                            continue;

                        if (!String.IsNullOrEmpty(nhomSP))
                        {
                            var entity = _nhomSanphamService.Single(c => c.TenNhomsanpham.Contains(nhomSP));
                            if (entity != null)
                            {
                                sanpham.NhomsanphamId = entity.Id;
                            }
                        }
                        if (!String.IsNullOrEmpty(ncc))
                        {
                            var entity = _nhacungcapService.Single(c => c.TenNhacungcap.Contains(ncc));
                            if (entity != null)
                            {
                                sanpham.NhacungcapId = entity.Id;
                            }
                        }
                        if (!String.IsNullOrEmpty(hsx))
                        {
                            var entity = _hangSanxuatService.Single(c => c.TenHangsanxuat.Contains(hsx));
                            if (entity != null)
                            {
                                sanpham.HangsanxuatId = entity.Id;
                            }
                        }

                        sanpham.Giabans = new List<GiabanSanphamModel>();
                        if (!String.IsNullOrEmpty(dealer1))
                        {
                            var entity = _nhucauGiaService.Single(c => c.TenNhucau.Contains("Dealer 1"));
                            if (entity != null)
                            {
                                sanpham.Giabans.Add(new GiabanSanphamModel()
                                {
                                    NhucaugiaId = entity.NhucauId,
                                    Gia = TypeSafeConverter.ToDecimal(dealer1)
                                });
                            }
                        }
                        if (!String.IsNullOrEmpty(dealer2))
                        {
                            var entity = _nhucauGiaService.Single(c => c.TenNhucau.Contains("Dealer 2"));
                            if (entity != null)
                            {
                                sanpham.Giabans.Add(new GiabanSanphamModel()
                                {
                                    NhucaugiaId = entity.NhucauId,
                                    Gia = TypeSafeConverter.ToDecimal(dealer2)
                                });
                            }
                        }
                        if (!String.IsNullOrEmpty(giaDacbiet))
                        {
                            var entity = _nhucauGiaService.Single(c => c.TenNhucau.Contains("Giá đặc biệt"));
                            if (entity != null)
                            {
                                sanpham.Giabans.Add(new GiabanSanphamModel()
                                {
                                    NhucaugiaId = entity.NhucauId,
                                    Gia = TypeSafeConverter.ToDecimal(giaDacbiet)
                                });
                            }
                        }
                        if (!String.IsNullOrEmpty(giaBanle))
                        {
                            var entity = _nhucauGiaService.Single(c => c.TenNhucau.Contains("Bán lẻ"));
                            if (entity != null)
                            {
                                sanpham.Giabans.Add(new GiabanSanphamModel()
                                {
                                    NhucaugiaId = entity.NhucauId,
                                    Gia = TypeSafeConverter.ToDecimal(giaBanle)
                                });
                            }
                        }
                        if (!String.IsNullOrEmpty(duan))
                        {
                            var entity = _nhucauGiaService.Single(c => c.TenNhucau.Contains("Dự án"));
                            if (entity != null)
                            {
                                sanpham.Giabans.Add(new GiabanSanphamModel()
                                {
                                    NhucaugiaId = entity.NhucauId,
                                    Gia = TypeSafeConverter.ToDecimal(duan)
                                });
                            }
                        }

                        var res = _sanphamService.Add(sanpham);
                        results.Add(res);
                    }

                    //log fail
                    var logger = LogManager.GetLogger(typeof(SanphamController));
                    logger.Error(String.Join("|", results.Select(c => c.Error)));

                    return new ActionResult() { Result = true, Error = "Nạp thành công: " + results.Count(c => c.Result == true) + " - Nạp thất bại: " + results.Count(c => c.Result == false) };
                }//using
            }//if

            return new ActionResult() { Result = true };
        }

        /// <summary>
        /// Xuất danh sách sản phẩm ra file excel
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/sanpham/export")]
        [HttpGet]
        public async Task<IHttpActionResult> Export()
        {
            if (HttpContext.Current.Application["SanphamListModel"] == null)
            {
                return Json(new ResponseResultBase<bool> { message = "Chưa có số liệu cần xuất. Đề nghị lọc trước", result = false });
            }

            var listRes = (List<SanphamModel>)HttpContext.Current.Application["SanphamListModel"];

            using (ExcelPackage pck = new ExcelPackage(new System.IO.FileInfo(HttpContext.Current.Server.MapPath("~/Templates/Product-Report.xlsx"))))
            {
                var ws = pck.Workbook.Worksheets[1];

                for (var j = 0; j < listRes.Count; j++)
                {
                    var i = j;

                    ws.Cells["A" + (i + 2)].Value = (j + 1);
                    ws.Cells["A" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["A" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["B" + (i + 2)].Value = listRes[j].MaSanpham;
                    ws.Cells["B" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["B" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["C" + (i + 2)].Value = listRes[j].TenSanpham;
                    ws.Cells["C" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["C" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["D" + (i + 2)].Value = listRes[j].Mota;
                    ws.Cells["D" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["D" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (listRes[j].NhomsanphamId != null)
                    {
                        var nhomSP = await _nhomSanphamService.GetById(listRes[j].NhomsanphamId.Value);
                        ws.Cells["E" + (i + 2)].Value = nhomSP != null ? nhomSP.TenNhomsanpham : "";
                        ws.Cells["E" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells["E" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    }

                    if (listRes[j].NhacungcapId != null)
                    {
                        var ncc = await _nhacungcapService.GetById(listRes[j].NhacungcapId.Value);
                        ws.Cells["F" + (i + 2)].Value = ncc != null ? ncc.TenNhacungcap : "";
                        ws.Cells["F" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells["F" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    }

                    if (listRes[j].HangsanxuatId != null)
                    {
                        var hsx = await _hangSanxuatService.GetById(listRes[j].HangsanxuatId.Value);
                        ws.Cells["G" + (i + 2)].Value = hsx != null ? hsx.TenHangsanxuat : "";
                        ws.Cells["G" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells["G" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    }

                    ws.Cells["H" + (i + 2)].Value = listRes[j].Giadauvao;
                    ws.Cells["H" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["H" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    var nhucaugia = listRes[j].Giabans.FirstOrDefault(c => c.Nhucau == "Dealer 1");
                    if (nhucaugia != null)
                    {
                        ws.Cells["I" + (i + 2)].Value = nhucaugia.Gia;
                        ws.Cells["I" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells["I" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    }

                    nhucaugia = listRes[j].Giabans.FirstOrDefault(c => c.Nhucau == "Dealer 2");
                    if (nhucaugia != null)
                    {
                        ws.Cells["J" + (i + 2)].Value = nhucaugia.Gia;
                        ws.Cells["J" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells["J" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    }

                    nhucaugia = listRes[j].Giabans.FirstOrDefault(c => c.Nhucau == "Giá đặc biệt");
                    if (nhucaugia != null)
                    {
                        ws.Cells["K" + (i + 2)].Value = nhucaugia.Gia;
                        ws.Cells["K" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells["K" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    }

                    nhucaugia = listRes[j].Giabans.FirstOrDefault(c => c.Nhucau == "Bán lẻ");
                    if (nhucaugia != null)
                    {
                        ws.Cells["L" + (i + 2)].Value = nhucaugia.Gia;
                        ws.Cells["L" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells["L" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    }

                    nhucaugia = listRes[j].Giabans.FirstOrDefault(c => c.Nhucau == "Dự án");
                    if (nhucaugia != null)
                    {
                        ws.Cells["M" + (i + 2)].Value = nhucaugia.Gia;
                        ws.Cells["M" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells["M" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    }
                }

                ws.Cells["A1:M" + (1 + listRes.Count)].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);

                //pck.SaveAs(Response.OutputStream);
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", $"attachment;  filename=ProductList_Report_{ DateTime.Now }.xlsx");

                return new FileResult(pck.GetAsByteArray(), HttpContext.Current.Response.ContentType);
            }
        }

        /// <summary>
        /// Thêm Sanpham mới
        /// </summary>
        /// <param name="sanpham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_SAN_PHAM)]
        [Route("v1/sanpham/add")]
        [HttpPost]
        public IHttpActionResult Add(SanphamModel sanpham)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            sanpham.NguoitaoId = userId;

            var res = _sanphamService.Add(sanpham);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Sanpham
        /// </summary>
        /// <param name="sanpham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_SAN_PHAM)]
        [Route("v1/sanpham/update")]
        [HttpPut]
        public IHttpActionResult Update(SanphamModel sanpham)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            sanpham.NguoiCapnhatId = userId;

            var res = _sanphamService.Update(sanpham);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Sanpham
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_SAN_PHAM)]
        [Route("v1/sanpham/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _sanphamService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Sanpham theo Id
        /// </summary>
        /// <param name="sanphamId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<SanphamModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_SAN_PHAM)]
        [Route("v1/sanpham/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int sanphamId)
        {
            var res = await _sanphamService.GetById(sanphamId);

            return Json(new ResponseResultBase<SanphamModel>() { result = true, data = res });
        }
    }
}

