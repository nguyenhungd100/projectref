
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Domain.Services.RoleFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Roles.V1.Controllers
{
    /// <summary>
    /// API làm việc với Role
    /// </summary>

    [Route("v1/role")]
    public class RoleController : ApiController
    {
        IRoleService _roleService = new RoleService();

        /// <summary>
        /// Lấy danh sách Role trong hệ thống theo bộ lọc
        /// </summary>
        /// <returns></returns>
        /// <remarks>API quản lý Role</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_VAI_TRO)]
        [Route("v1/role/list")]
        [HttpGet]
        [ResponseType(typeof(ResponseResultBase<List<RoleModel>>))]
        public async Task<IHttpActionResult> List()
        {
            var res = await _roleService.List();

            return Json(new ResponseResultBase<List<RoleModel>>() { result = true, data = res });
        }

        /// <summary>
        /// Thêm Role mới
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_VAI_TRO)]
        [Route("v1/role/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(RoleModel role)
        {
            var res = await _roleService.Add(role);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Role
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_VAI_TRO)]
        [Route("v1/role/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(RoleModel role)
        {
            var res = await _roleService.Update(role);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Role
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_VAI_TRO)]
        [Route("v1/role/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _roleService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Role theo Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<RoleModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_VAI_TRO)]
        [Route("v1/role/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int roleId)
        {
            var res = await _roleService.GetById(roleId);

            return Json(new ResponseResultBase<RoleModel>() { result = true, data = res });
        }

        /// <summary>
        /// Lấy danh sách quyền của hệ thống
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<List<GroupPermission>>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_VAI_TRO)]
        [Route("v1/role/list_permissions")]
        [HttpGet]
        public IHttpActionResult ListPermissions()
        {
            var res = _roleService.ListPermissions();

            return Json(new ResponseResultBase<List<GroupPermission>>() { result = true, data = res });
        }
    }
}

