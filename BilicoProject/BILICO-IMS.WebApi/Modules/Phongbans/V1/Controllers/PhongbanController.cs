
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.PhongbanFacade;
using BILICO.IMS.Domain.Services.PhongbanFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Phongbans.V1.Controllers
{
	/// <summary>
    /// API làm việc với Phongban
    /// </summary>

    [Route("v1/phongban")]
    public class PhongbanController : ApiController
    {
        IPhongbanService _phongbanService = new PhongbanService();

        /// <summary>
        /// Lấy danh sách Phongban trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Phongban</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_PHONG_BAN)]
        [Route("v1/phongban/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<PhongbanModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _phongbanService.List(filter);

            return Json(new ResponseResultBase<ListPager<PhongbanModel>>() { result = true, data = new ListPager<PhongbanModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Phongban mới
        /// </summary>
        /// <param name="phongban"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_PHONG_BAN)]
        [Route("v1/phongban/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(PhongbanModel phongban)
        {
            var res = await _phongbanService.Add(phongban);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Phongban
        /// </summary>
        /// <param name="phongban"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_PHONG_BAN)]
        [Route("v1/phongban/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(PhongbanModel phongban)
        {
            var res = await _phongbanService.Update(phongban);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Phongban
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_PHONG_BAN)]
        [Route("v1/phongban/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _phongbanService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Phongban theo Id
        /// </summary>
        /// <param name="phongbanId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<PhongbanModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_PHONG_BAN)]
        [Route("v1/phongban/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int phongbanId)
        {
            var res = await _phongbanService.GetById(phongbanId);

            return Json(new ResponseResultBase<PhongbanModel>() { result = true, data = res });
        }
    }
}

