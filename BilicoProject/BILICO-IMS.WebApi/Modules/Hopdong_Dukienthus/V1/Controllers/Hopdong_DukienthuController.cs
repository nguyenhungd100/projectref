
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Hopdong_DukienthuFacade;
using BILICO.IMS.Domain.Services.Hopdong_DukienthuFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Hopdong_Dukienthus.V1.Controllers
{
	/// <summary>
    /// API làm việc với Hopdong_Dukienthu
    /// </summary>

    [Route("v1/hopdong_dukienthu")]
    public class Hopdong_DukienthuController : ApiController
    {
        IHopdong_DukienthuService _hopdong_dukienthuService = new Hopdong_DukienthuService();
		
        /// <summary>
        /// Lấy danh sách Hopdong_Dukienthu trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hopdong_Dukienthu</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/hopdong_dukienthu/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Hopdong_DukienthuModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _hopdong_dukienthuService.List(filter);

            return Json(new ResponseResultBase<ListPager<Hopdong_DukienthuModel>>() { result = true, data = new ListPager<Hopdong_DukienthuModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Hopdong_Dukienthu mới
        /// </summary>
        /// <param name="hopdong_dukienthu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienthu/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Hopdong_DukienthuModel hopdong_dukienthu)
        {
            var res = await _hopdong_dukienthuService.Add(hopdong_dukienthu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Hopdong_Dukienthu
        /// </summary>
        /// <param name="hopdong_dukienthu"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienthu/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Hopdong_DukienthuModel hopdong_dukienthu)
        {
            var res = await _hopdong_dukienthuService.Update(hopdong_dukienthu);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Hopdong_Dukienthu
        /// </summary>
        /// <param name="hopdong_dukienthuId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienthu/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _hopdong_dukienthuService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hopdong_Dukienthu theo Id
        /// </summary>
        /// <param name="hopdong_dukienthuId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Hopdong_DukienthuModel>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienthu/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hopdong_dukienthuId)
        {
            var res = await _hopdong_dukienthuService.GetById(hopdong_dukienthuId);

            return Json(new ResponseResultBase<Hopdong_DukienthuModel>() { result = true, data = res });
        }
    }
}

