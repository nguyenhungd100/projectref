
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Khachhang_Baogia_ChitietSanphamFacade;
using BILICO.IMS.Domain.Services.Khachhang_Baogia_ChitietSanphamFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Khachhang_Baogia_ChitietSanphams.V1.Controllers
{
	/// <summary>
    /// API làm việc với Khachhang_Baogia_ChitietSanpham
    /// </summary>

    [Route("v1/khachhang_baogia_chitietsanpham")]
    public class Khachhang_Baogia_ChitietSanphamController : ApiController
    {
        IKhachhang_Baogia_ChitietSanphamService _khachhang_baogia_chitietsanphamService = new Khachhang_Baogia_ChitietSanphamService();
		
        /// <summary>
        /// Lấy danh sách Khachhang_Baogia_ChitietSanpham trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Khachhang_Baogia_ChitietSanpham</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietsanpham/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Khachhang_Baogia_ChitietSanphamModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _khachhang_baogia_chitietsanphamService.List(filter);

            return Json(new ResponseResultBase<ListPager<Khachhang_Baogia_ChitietSanphamModel>>() { result = true, data = new ListPager<Khachhang_Baogia_ChitietSanphamModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Khachhang_Baogia_ChitietSanpham mới
        /// </summary>
        /// <param name="khachhang_baogia_chitietsanpham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietsanpham/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Khachhang_Baogia_ChitietSanphamModel khachhang_baogia_chitietsanpham)
        {
            var res = await _khachhang_baogia_chitietsanphamService.Add(khachhang_baogia_chitietsanpham);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Khachhang_Baogia_ChitietSanpham
        /// </summary>
        /// <param name="khachhang_baogia_chitietsanpham"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietsanpham/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Khachhang_Baogia_ChitietSanphamModel khachhang_baogia_chitietsanpham)
        {
            var res = await _khachhang_baogia_chitietsanphamService.Update(khachhang_baogia_chitietsanpham);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Khachhang_Baogia_ChitietSanpham
        /// </summary>
        /// <param name="khachhang_baogia_chitietsanphamId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietsanpham/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _khachhang_baogia_chitietsanphamService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Khachhang_Baogia_ChitietSanpham theo Id
        /// </summary>
        /// <param name="khachhang_baogia_chitietsanphamId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Khachhang_Baogia_ChitietSanphamModel>))]
        [ApiAuthorize]
        [Route("v1/khachhang_baogia_chitietsanpham/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int khachhang_baogia_chitietsanphamId)
        {
            var res = await _khachhang_baogia_chitietsanphamService.GetById(khachhang_baogia_chitietsanphamId);

            return Json(new ResponseResultBase<Khachhang_Baogia_ChitietSanphamModel>() { result = true, data = res });
        }
    }
}

