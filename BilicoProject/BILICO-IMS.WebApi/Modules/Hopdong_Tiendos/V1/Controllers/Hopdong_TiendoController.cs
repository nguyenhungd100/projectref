
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Hopdong_TiendoFacade;
using BILICO.IMS.Domain.Services.Hopdong_TiendoFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Hopdong_Tiendos.V1.Controllers
{
	/// <summary>
    /// API làm việc với Hopdong_Tiendo
    /// </summary>

    [Route("v1/hopdong_tiendo")]
    public class Hopdong_TiendoController : ApiController
    {
        IHopdong_TiendoService _hopdong_tiendoService = new Hopdong_TiendoService();
		
        /// <summary>
        /// Lấy danh sách Hopdong_Tiendo trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hopdong_Tiendo</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/hopdong_tiendo/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Hopdong_TiendoModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _hopdong_tiendoService.List(filter);

            return Json(new ResponseResultBase<ListPager<Hopdong_TiendoModel>>() { result = true, data = new ListPager<Hopdong_TiendoModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Hopdong_Tiendo mới
        /// </summary>
        /// <param name="hopdong_tiendo"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_tiendo/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Hopdong_TiendoModel hopdong_tiendo)
        {
            var res = await _hopdong_tiendoService.Add(hopdong_tiendo);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Hopdong_Tiendo
        /// </summary>
        /// <param name="hopdong_tiendo"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_tiendo/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Hopdong_TiendoModel hopdong_tiendo)
        {
            var res = await _hopdong_tiendoService.Update(hopdong_tiendo);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Hopdong_Tiendo
        /// </summary>
        /// <param name="hopdong_tiendoId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_tiendo/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _hopdong_tiendoService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hopdong_Tiendo theo Id
        /// </summary>
        /// <param name="hopdong_tiendoId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Hopdong_TiendoModel>))]
        [ApiAuthorize]
        [Route("v1/hopdong_tiendo/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hopdong_tiendoId)
        {
            var res = await _hopdong_tiendoService.GetById(hopdong_tiendoId);

            return Json(new ResponseResultBase<Hopdong_TiendoModel>() { result = true, data = res });
        }
    }
}

