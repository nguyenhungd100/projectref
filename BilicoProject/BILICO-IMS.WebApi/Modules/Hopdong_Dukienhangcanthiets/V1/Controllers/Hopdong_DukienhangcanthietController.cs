
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Hopdong_DukienhangcanthietFacade;
using BILICO.IMS.Domain.Services.Hopdong_DukienhangcanthietFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Hopdong_Dukienhangcanthiets.V1.Controllers
{
	/// <summary>
    /// API làm việc với Hopdong_Dukienhangcanthiet
    /// </summary>

    [Route("v1/hopdong_dukienhangcanthiet")]
    public class Hopdong_DukienhangcanthietController : ApiController
    {
        IHopdong_DukienhangcanthietService _hopdong_dukienhangcanthietService = new Hopdong_DukienhangcanthietService();
		
        /// <summary>
        /// Lấy danh sách Hopdong_Dukienhangcanthiet trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Hopdong_Dukienhangcanthiet</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/hopdong_dukienhangcanthiet/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Hopdong_DukienhangcanthietModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _hopdong_dukienhangcanthietService.List(filter);

            return Json(new ResponseResultBase<ListPager<Hopdong_DukienhangcanthietModel>>() { result = true, data = new ListPager<Hopdong_DukienhangcanthietModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Hopdong_Dukienhangcanthiet mới
        /// </summary>
        /// <param name="hopdong_dukienhangcanthiet"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienhangcanthiet/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Hopdong_DukienhangcanthietModel hopdong_dukienhangcanthiet)
        {
            var res = await _hopdong_dukienhangcanthietService.Add(hopdong_dukienhangcanthiet);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Hopdong_Dukienhangcanthiet
        /// </summary>
        /// <param name="hopdong_dukienhangcanthiet"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienhangcanthiet/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Hopdong_DukienhangcanthietModel hopdong_dukienhangcanthiet)
        {
            var res = await _hopdong_dukienhangcanthietService.Update(hopdong_dukienhangcanthiet);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Hopdong_Dukienhangcanthiet
        /// </summary>
        /// <param name="hopdong_dukienhangcanthietId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienhangcanthiet/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _hopdong_dukienhangcanthietService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Hopdong_Dukienhangcanthiet theo Id
        /// </summary>
        /// <param name="hopdong_dukienhangcanthietId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Hopdong_DukienhangcanthietModel>))]
        [ApiAuthorize]
        [Route("v1/hopdong_dukienhangcanthiet/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int hopdong_dukienhangcanthietId)
        {
            var res = await _hopdong_dukienhangcanthietService.GetById(hopdong_dukienhangcanthietId);

            return Json(new ResponseResultBase<Hopdong_DukienhangcanthietModel>() { result = true, data = res });
        }
    }
}

