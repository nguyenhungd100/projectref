
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.Khachhang_SanphamQuantamFacade;
using BILICO.IMS.Domain.Services.Khachhang_SanphamQuantamFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Khachhang_SanphamQuantams.V1.Controllers
{
	/// <summary>
    /// API làm việc với Khachhang_SanphamQuantam
    /// </summary>

    [Route("v1/khachhang_sanphamquantam")]
    public class Khachhang_SanphamQuantamController : ApiController
    {
        IKhachhang_SanphamQuantamService _khachhang_sanphamquantamService = new Khachhang_SanphamQuantamService();
		
        /// <summary>
        /// Lấy danh sách Khachhang_SanphamQuantam trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Khachhang_SanphamQuantam</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/khachhang_sanphamquantam/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<Khachhang_SanphamQuantamModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _khachhang_sanphamquantamService.List(filter);

            return Json(new ResponseResultBase<ListPager<Khachhang_SanphamQuantamModel>>() { result = true, data = new ListPager<Khachhang_SanphamQuantamModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Khachhang_SanphamQuantam mới
        /// </summary>
        /// <param name="khachhang_sanphamquantam"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_sanphamquantam/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(Khachhang_SanphamQuantamModel khachhang_sanphamquantam)
        {
            var res = await _khachhang_sanphamquantamService.Add(khachhang_sanphamquantam);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Khachhang_SanphamQuantam
        /// </summary>
        /// <param name="khachhang_sanphamquantam"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_sanphamquantam/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(Khachhang_SanphamQuantamModel khachhang_sanphamquantam)
        {
            var res = await _khachhang_sanphamquantamService.Update(khachhang_sanphamquantam);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Khachhang_SanphamQuantam
        /// </summary>
        /// <param name="khachhang_sanphamquantamId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang_sanphamquantam/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _khachhang_sanphamquantamService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Khachhang_SanphamQuantam theo Id
        /// </summary>
        /// <param name="khachhang_sanphamquantamId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<Khachhang_SanphamQuantamModel>))]
        [ApiAuthorize]
        [Route("v1/khachhang_sanphamquantam/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int khachhang_sanphamquantamId)
        {
            var res = await _khachhang_sanphamquantamService.GetById(khachhang_sanphamquantamId);

            return Json(new ResponseResultBase<Khachhang_SanphamQuantamModel>() { result = true, data = res });
        }
    }
}

