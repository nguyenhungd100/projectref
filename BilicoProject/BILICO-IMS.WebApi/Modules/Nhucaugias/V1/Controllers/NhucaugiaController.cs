
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.NhucaugiaFacade;
using BILICO.IMS.Domain.Services.NhucaugiaFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.Nhucaugias.V1.Controllers
{
	/// <summary>
    /// API làm việc với Nhucaugia
    /// </summary>

    [Route("v1/nhucaugia")]
    public class NhucaugiaController : ApiController
    {
        INhucaugiaService _nhucaugiaService = new NhucaugiaService();

        /// <summary>
        /// Lấy danh sách Nhucaugia trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Nhucaugia</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_NHU_CAU_GIA)]
        [Route("v1/nhucaugia/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<NhucaugiaModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _nhucaugiaService.List(filter);

            return Json(new ResponseResultBase<ListPager<NhucaugiaModel>>() { result = true, data = new ListPager<NhucaugiaModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Nhucaugia mới
        /// </summary>
        /// <param name="nhucaugia"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_NHU_CAU_GIA)]
        [Route("v1/nhucaugia/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(NhucaugiaModel nhucaugia)
        {
            var res = await _nhucaugiaService.Add(nhucaugia);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Nhucaugia
        /// </summary>
        /// <param name="nhucaugia"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_NHU_CAU_GIA)]
        [Route("v1/nhucaugia/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(NhucaugiaModel nhucaugia)
        {
            var res = await _nhucaugiaService.Update(nhucaugia);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Nhucaugia
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_NHU_CAU_GIA)]
        [Route("v1/nhucaugia/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _nhucaugiaService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Nhucaugia theo Id
        /// </summary>
        /// <param name="nhucaugiaId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<NhucaugiaModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_NHU_CAU_GIA)]
        [Route("v1/nhucaugia/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int nhucaugiaId)
        {
            var res = await _nhucaugiaService.GetById(nhucaugiaId);

            return Json(new ResponseResultBase<NhucaugiaModel>() { result = true, data = res });
        }
    }
}

