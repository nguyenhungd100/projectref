﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Thinktecture.IdentityModel.WebApi;
using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Domain.Services.RoleFacade.Implementation;
using BILICO.IMS.Domain.Services.NotificationFacade;
using BILICO.IMS.Domain.Services.NotificationFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;
using BILICO.IMS.WebApi.Modules;

namespace THUANTHANH.PMS.WebApi.Modules.Notifications.V1.Controllers
{
    /// <summary>
    /// API làm việc với tin nhắn
    /// </summary>
    [Route("v1/notification")]
    public class NotificationController : ApiController
    {
        INotificationService _notificationService = new NotificationService();
        IRoleService _roleService = new RoleService();

        /// <summary>
        /// Lấy tất danh sách tin nhắn trong hệ thống
        /// </summary>
        /// <returns></returns>
        /// <remarks>API quản lý tin nhắn của hệ thống</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<ListPager<NotificationModel>>))]
        [ApiAuthorize]
        [Route("v1/notification/get_my_notifications")]
        [HttpPost]
        public async Task<IHttpActionResult> GetMyNotifications(BILICO.IMS.Domain.Services.NotificationFacade.FilterModel filter)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var res = await _notificationService.GetMyNotifications(userId, filter);

            return Json(new ResponseResultBase<ListPager<NotificationModel>>() { result = true, data = new ListPager<NotificationModel> { list = res, pager = filter.Paging } });
        }


        /// <summary>
        /// Xóa tin nhắn
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/notification/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] ids)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var res = await _notificationService.Delete(userId, ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xem tin nhắn
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/notification/preview")]
        [HttpPost]
        [ApiAuthorize]
        public async Task<IHttpActionResult> Preview(int[] ids)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var res = await _notificationService.Preview(userId, ids);

            return Json(new ResponseResultBase<bool> { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Nhận tin nhắn
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/notification/receive")]
        [HttpPost]
        [ApiAuthorize]
        public async Task<IHttpActionResult> Receive(int[] ids)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var res = await _notificationService.Receive(userId, ids);

            return Json(new ResponseResultBase<bool> { result = res.Result, message = res.Error });
        }
    }
}