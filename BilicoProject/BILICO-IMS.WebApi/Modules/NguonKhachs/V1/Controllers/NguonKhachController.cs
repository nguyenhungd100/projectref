
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.NguonKhachFacade;
using BILICO.IMS.Domain.Services.NguonKhachFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;

namespace BILICO.IMS.WebApi.Modules.NguonKhachs.V1.Controllers
{
	/// <summary>
    /// API làm việc với NguonKhach
    /// </summary>

    [Route("v1/nguonkhach")]
    public class NguonKhachController : ApiController
    {
        INguonKhachService _nguonkhachService = new NguonKhachService();

        /// <summary>
        /// Lấy danh sách NguonKhach trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý NguonKhach</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_NGUON_KHACH_HANG)]
        [Route("v1/nguonkhach/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<NguonKhachModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _nguonkhachService.List(filter);

            return Json(new ResponseResultBase<ListPager<NguonKhachModel>>() { result = true, data = new ListPager<NguonKhachModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm NguonKhach mới
        /// </summary>
        /// <param name="nguonkhach"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_NGUON_KHACH_HANG)]
        [Route("v1/nguonkhach/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(NguonKhachModel nguonkhach)
        {
            var res = await _nguonkhachService.Add(nguonkhach);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin NguonKhach
        /// </summary>
        /// <param name="nguonkhach"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_NGUON_KHACH_HANG)]
        [Route("v1/nguonkhach/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(NguonKhachModel nguonkhach)
        {
            var res = await _nguonkhachService.Update(nguonkhach);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa NguonKhach
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_NGUON_KHACH_HANG)]
        [Route("v1/nguonkhach/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _nguonkhachService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin NguonKhach theo Id
        /// </summary>
        /// <param name="nguonkhachId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<NguonKhachModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_NGUON_KHACH_HANG)]
        [Route("v1/nguonkhach/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int nguonkhachId)
        {
            var res = await _nguonkhachService.GetById(nguonkhachId);

            return Json(new ResponseResultBase<NguonKhachModel>() { result = true, data = res });
        }
    }
}

