﻿namespace BILICO.IMS.WebApi.Modules.Khachhangs.V1.Controllers
{
    public class EmailModel
    {
        public int BaogiaId { set; get; }

        public string EmailReceiver { set; get; }

        public  string Note { set; get; }
    }
}