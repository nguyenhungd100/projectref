
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.KhachhangFacade;
using BILICO.IMS.Domain.Services.KhachhangFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;
using BILICO.IMS.Domain.Services;
using OfficeOpenXml;
using BILICO.IMS.Framework.Utils;
using OfficeOpenXml.Style;
using BILICO.IMS.Domain.Services.TaikhoanFacade.Implementation;
using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Domain.Services.TaikhoanFacade;
using BILICO.IMS.WebApi.Providers;
using System.Text;

namespace BILICO.IMS.WebApi.Modules.Khachhangs.V1.Controllers
{
    /// <summary>
    /// API làm việc với Khachhang
    /// </summary>

    [Route("v1/khachhang")]
    public class KhachhangController : ApiController
    {
        IKhachhangService _khachhangService = new KhachhangService();
        ITaikhoanService _taikhoanService = new TaikhoanService();

        /// <summary>
        /// Lấy danh sách Khachhang trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Khachhang</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<KhachhangModel>>))]
        public async Task<IHttpActionResult> List(Domain.Services.KhachhangFacade.FilterModel filter)
        {
            var res = await _khachhangService.List(filter);

            HttpContext.Current.Application["KhachhangModels"] = res;

            return Json(new ResponseResultBase<ListPager<KhachhangModel>>() { result = true, data = new ListPager<KhachhangModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy danh sách khách hàng mới trong hệ thống
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/list_new")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<KhachhangModel>>))]
        public async Task<IHttpActionResult> ListNew(Domain.Services.KhachhangFacade.FilterModel filter)
        {
            filter.NguoitaoId = RequestContext.Principal.Identity.GetUserId();
            filter.Status = CustomerStatus.New;

            var res = await _khachhangService.List(filter);

            HttpContext.Current.Application["KhachhangModels"] = res;

            return Json(new ResponseResultBase<ListPager<KhachhangModel>>() { result = true, data = new ListPager<KhachhangModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy danh sách khách hàng đã chuyển sale (Mới nhận đối với sale manager)
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/list_send2sale")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<KhachhangModel>>))]
        public async Task<IHttpActionResult> ListSend2Sale(Domain.Services.KhachhangFacade.FilterModel filter)
        {
            filter.NguoitaoId = RequestContext.Principal.Identity.GetUserId();

            filter.Status = CustomerStatus.Move2Sale;

            var res = await _khachhangService.List(filter);

            HttpContext.Current.Application["KhachhangModels"] = res;

            return Json(new ResponseResultBase<ListPager<KhachhangModel>>() { result = true, data = new ListPager<KhachhangModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy danh sách khách hàng đã chuyển sale của sale manager
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/list_send2_subsale")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<KhachhangModel>>))]
        public async Task<IHttpActionResult> ListSend2SubSale(Domain.Services.KhachhangFacade.FilterModel filter)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();
            filter.NguoitaoId = userId;
            filter.Status = CustomerStatus.Move2SubSale;
            var user = (new TaikhoanService()).GetById(userId);
            if (user.Roles.Contains($";{SystemRoles.SalesManagement.GetHashCode()};"))
            {
                filter.KinhdoanhNhanId = userId;
            }
            else
            {
                filter.SubSalerId = userId;
            }

            var res = await _khachhangService.List(filter);

            HttpContext.Current.Application["KhachhangModels"] = res;

            return Json(new ResponseResultBase<ListPager<KhachhangModel>>() { result = true, data = new ListPager<KhachhangModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy danh sách khách hàng đã chuyển đổi
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/list_converted")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<KhachhangModel>>))]
        public async Task<IHttpActionResult> ListConverted(Domain.Services.KhachhangFacade.FilterModel filter)
        {
            filter.NguoitaoId = RequestContext.Principal.Identity.GetUserId();
            filter.KinhdoanhNhanId = RequestContext.Principal.Identity.GetUserId();

            filter.Status = CustomerStatus.Converted;

            var res = await _khachhangService.List(filter);

            HttpContext.Current.Application["KhachhangModels"] = res;

            return Json(new ResponseResultBase<ListPager<KhachhangModel>>() { result = true, data = new ListPager<KhachhangModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy danh sách khách hàng không chuyển đổi
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/list_not_convertion")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<KhachhangModel>>))]
        public async Task<IHttpActionResult> ListNotConvertion(Domain.Services.KhachhangFacade.FilterModel filter)
        {
            filter.NguoitaoId = RequestContext.Principal.Identity.GetUserId();
            filter.KinhdoanhNhanId = RequestContext.Principal.Identity.GetUserId();

            filter.Status = CustomerStatus.NotConversion;

            var res = await _khachhangService.List(filter);

            HttpContext.Current.Application["KhachhangModels"] = res;

            return Json(new ResponseResultBase<ListPager<KhachhangModel>>() { result = true, data = new ListPager<KhachhangModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy danh sách khách hàng sale đã nhận
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/list_received")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<ListPager<KhachhangModel>>))]
        public async Task<IHttpActionResult> ListReceived(Domain.Services.KhachhangFacade.FilterModel filter)
        {
            filter.Status = CustomerStatus.Received;

            filter.KinhdoanhNhanId = RequestContext.Principal.Identity.GetUserId();

            var res = await _khachhangService.List(filter);

            HttpContext.Current.Application["KhachhangModels"] = res;

            return Json(new ResponseResultBase<ListPager<KhachhangModel>>() { result = true, data = new ListPager<KhachhangModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Khachhang mới
        /// </summary>
        /// <param name="khachhang"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_KHACH_HANG)]
        [Route("v1/khachhang/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(KhachhangModel khachhang)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            khachhang.Nguoitao = userId;

            var user = _taikhoanService.GetById(userId);

            if (user != null)
            {
                if (user.Roles != null && user.Roles.Contains($";{SystemRoles.Salers};"))
                {
                    khachhang.SubSalerId = userId;
                }

                if (user.Roles != null && user.Roles.Contains($";{SystemRoles.SalesManagement};"))
                {
                    khachhang.SalerId = userId;
                }
            }

            var res = await _khachhangService.Add(khachhang);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Gửi email cho danh sách điểm bán
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize]
        [Route("v1/khachhang/send_email")]
        [HttpPost]
        public async Task<IHttpActionResult> SendEmail(EmailModel email)
        {
            if (email == null || String.IsNullOrEmpty(email.EmailReceiver) || email.BaogiaId <= 0)
                return Json(new ResponseResultBase<bool>() { result = false, message = "Yêu cầu gửi email không hợp lệ", data = false });

            var baogia = await _khachhangService.GetBaogiaDetail(email.BaogiaId);

            var khachhang = await _khachhangService.GetById(baogia.Baogia.KhachhangId);

            var sb = new StringBuilder();

            sb.Append($"Kính gửi, {khachhang.Hoten}");

            sb.Append(baogia.Baogia.NoidungBaogia);

            sb.AppendLine();

            sb.Append("Báo giá sản phẩm:");

            sb.AppendLine();

            baogia.Sanphams.ForEach(c =>
            {
                sb.Append(" - " + c.TenSanpham + " x " + c.Soluong + " = " + c.Thanhtien);

                sb.AppendLine();
            });

            sb.Append("Báo giá dịch vụ:");

            sb.AppendLine();

            baogia.Dichvus.ForEach(c =>
            {
                sb.Append(" - " + c.Dichvu + " x " + c.Soluong + " = " + c.Thanhtien);

                sb.AppendLine();
            });

            var content = sb.ToString();

            var userId = RequestContext.Principal.Identity.GetUserId();

            var sender = await _taikhoanService.GetByIdAsync(userId);

            if (sender != null)
                (new EmailServiceProvider()).Send(sender.Email, "Báo giá: " + baogia.Baogia.NoidungBaogia, content);

            return Json(new ResponseResultBase<bool>() { result = true });
        }

        /// <summary>
        /// Cập nhật thông tin Khachhang
        /// </summary>
        /// <param name="khachhang"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_KHACH_HANG)]
        [Route("v1/khachhang/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(KhachhangModel khachhang)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var user = _taikhoanService.GetById(userId);

            if (user != null)
            {
                if (user.Roles != null && user.Roles.Contains($";{SystemRoles.Salers};"))
                {
                    khachhang.SubSalerId = userId;
                }

                if (user.Roles != null && user.Roles.Contains($";{SystemRoles.SalesManagement};"))
                {
                    khachhang.SalerId = userId;
                }
            }

            khachhang.Nguoicapnhat = userId;

            var res = await _khachhangService.Update(khachhang);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Khachhang
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_KHACH_HANG)]
        [Route("v1/khachhang/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _khachhangService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Khachhang theo Id
        /// </summary>
        /// <param name="khachhangId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<KhachhangModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int khachhangId)
        {
            var res = await _khachhangService.GetById(khachhangId);

            return Json(new ResponseResultBase<KhachhangModel>() { result = true, data = res });
        }

        /// <summary>
        /// Chuyển d/s khách hàng cho saler manager
        /// </summary>
        /// <param name="salerId"></param>
        /// <param name="customerIds"></param>
        /// <param name="nhandebietIds"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_KHACH_HANG)]
        [Route("v1/khachhang/move_2_sale")]
        [HttpPost]
        public async Task<IHttpActionResult> Move2Sale(Move2SaleModel move2Sale)
        {
            var res = await _khachhangService.Move2Sale(move2Sale);

            return Json(new ResponseResultBase<bool>() { result = res });
        }

        /// <summary>
        /// Chuyển d/s khách hàng cho saler
        /// </summary>
        /// <param name="subSalerId"></param>
        /// <param name="customerIds"></param>
        /// <param name="nhandebietIds"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_KHACH_HANG)]
        [Route("v1/khachhang/move_2_sub_sale")]
        [HttpPost]
        public async Task<IHttpActionResult> Move2SubSale(Move2SubSaleModel move2SubSaleModel)
        {
            var res = await _khachhangService.Move2SubSale(move2SubSaleModel);

            return Json(new ResponseResultBase<bool>() { result = res });
        }

        /// <summary>
        /// Trả khách hàng cho marketing
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_KHACH_HANG)]
        [Route("v1/khachhang/return_2_marketing")]
        [HttpPost]
        public async Task<IHttpActionResult> Return2Marketing(int customerId)
        {
            var res = await _khachhangService.Return2Marketing(customerId);

            return Json(new ResponseResultBase<bool>() { result = res });
        }

        /// <summary>
        /// Xuất danh sách khách hàng ra file excel
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/khachhang/export")]
        [HttpGet]
        public IHttpActionResult Export()
        {
            if (HttpContext.Current.Application["KhachhangModels"] == null)
            {
                return Json(new ResponseResultBase<bool> { message = "Chưa có số liệu cần xuất. Đề nghị lọc trước", result = false });
            }

            var listRes = (List<KhachhangModel>)HttpContext.Current.Application["KhachhangModels"];

            using (ExcelPackage pck = new ExcelPackage(new System.IO.FileInfo(HttpContext.Current.Server.MapPath("~/Templates/Customer-Report.xlsx"))))
            {
                var ws = pck.Workbook.Worksheets[1];

                for (var j = 0; j < listRes.Count; j++)
                {
                    var i = j;

                    ws.Cells["A" + (i + 2)].Value = (j + 1);
                    ws.Cells["A" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["A" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["B" + (i + 2)].Value = listRes[j].Hoten;
                    ws.Cells["B" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["B" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["C" + (i + 2)].Value = listRes[j].Gioitinh != null ? (listRes[j].Gioitinh == true ? "Nam" : "Nữ") : "";
                    ws.Cells["C" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["C" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["D" + (i + 2)].Value = listRes[j].Ngaysinh != null ? listRes[j].Ngaysinh.Value.ToString("dd/MM/yyyy") : "";
                    ws.Cells["D" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["D" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["E" + (i + 2)].Value = listRes[j].Email;
                    ws.Cells["E" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["E" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["F" + (i + 2)].Value = listRes[j].Mobile;
                    ws.Cells["F" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["F" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    ws.Cells["G" + (i + 2)].Value = listRes[j].Diachi;
                    ws.Cells["G" + (i + 2)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells["G" + (i + 2)].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                }

                ws.Cells["A1:G" + (1 + listRes.Count)].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);

                //pck.SaveAs(Response.OutputStream);
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", $"attachment;  filename=Customer_Report_{ DateTime.Now }.xlsx");

                return new FileResult(pck.GetAsByteArray(), HttpContext.Current.Response.ContentType);
            }
        }

        /// <summary>
        /// Nạp d/s khách hàng từ file
        /// </summary>
        /// <param name="fileKey"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_KHACH_HANG)]
        [Route("v1/khachhang/import")]
        [HttpPost]
        public async Task<IHttpActionResult> Import(string fileKey)
        {
            if (String.IsNullOrEmpty(fileKey))
            {
                return Json(new ResponseResultBase<bool>() { result = false, message = "File import không tồn tại" });
            }
            else
            {
                var path = Path.Combine(HttpContext.Current.Server.MapPath("/Upload/Temp"), fileKey);

                var files = Directory.GetFiles(path);

                if (files != null && files.Length > 0)
                {
                    var result = await Import(new FileInfo(files[0]));

                    return Json(new ResponseResultBase<bool>() { result = result.Result, message = result.Error });
                }
                else
                {
                    return Json(new ResponseResultBase<bool>() { result = false, message = "File import không tồn tại" });
                }
            }
        }

        private int GetColumnIndex(ExcelWorksheet ws, string columnName)
        {
            var start = ws.Dimension.Start;
            var end = ws.Dimension.End;
            for (int col = start.Column; col <= end.Column; col++)
            {
                var column = ws.Cells[start.Row, col].Text.Trim();
                if (column.ToLower() == columnName.ToLower())
                    return col;
            }

            return -1;
        }

        /// <summary>
        /// Import file excel nhân viên vào hệ thống
        /// </summary>
        /// <returns></returns>
        private async Task<ActionResult> Import(FileInfo fileInfo)
        {
            //handle upload
            var Request = HttpContext.Current.Request;
            if (fileInfo != null)
            {
                using (ExcelPackage pck = new ExcelPackage(fileInfo))
                {
                    var ws = pck.Workbook.Worksheets[1];
                    var start = ws.Dimension.Start;
                    var end = ws.Dimension.End;
                    var fullNameCol = GetColumnIndex(ws, "Họ tên");
                    var genderCol = GetColumnIndex(ws, "Giới tính");
                    var birthdayCol = GetColumnIndex(ws, "Ngày sinh");
                    var emailCol = GetColumnIndex(ws, "Email");
                    var mobileCol = GetColumnIndex(ws, "Mobile");
                    var addressCol = GetColumnIndex(ws, "Địa chỉ");

                    if (fullNameCol < 0 || genderCol < 0 || birthdayCol < 0 || emailCol < 0 || mobileCol < 0 || addressCol < 0)
                        return new ActionResult() { Result = false, Error = "File import không hợp lệ" };

                    var results = new List<ActionResult>();
                    for (int row = start.Row + 1; row <= end.Row; row++)
                    {
                        var khachhang = new KhachhangModel();
                        var fullName = ws.Cells[row, fullNameCol].Text.Trim();
                        var gender = ws.Cells[row, genderCol].Text.Trim();
                        var birdthDay = ws.Cells[row, birthdayCol].Text.Trim();
                        var email = ws.Cells[row, emailCol].Text.Trim();
                        var mobile = ws.Cells[row, mobileCol].Text.Trim();
                        var address = ws.Cells[row, addressCol].Text.Trim();

                        khachhang.Nguoitao = RequestContext.Principal.Identity.GetUserId();
                        khachhang.Hoten = fullName;
                        khachhang.Gioitinh = gender == "Nam" ? true : false;
                        khachhang.Ngaysinh = !String.IsNullOrEmpty(birdthDay) ? TypeSafeConverter.ToDateTime(birdthDay) : default(DateTime?);
                        khachhang.Email = email;
                        khachhang.Mobile = mobile;
                        khachhang.Diachi = address;

                        var res = await _khachhangService.Add(khachhang);
                        results.Add(res);
                    }

                    return new ActionResult() { Result = true, Error = "Nạp thành công: " + results.Count(c => c.Result == true) + " - Nạp thất bại: " + results.Count(c => c.Result == false) };
                }//using
            }//if

            return new ActionResult() { Result = true };
        }

        /// <summary>
        /// Cập nhật nhu cầu của khách hàng
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="nhucauIds"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_KHACH_HANG)]
        [Route("v1/khachhang/save_nhu_cau")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveNhucau(int customerId, int[] nhucauIds)
        {
            var res = await _khachhangService.SaveNhucau(customerId, nhucauIds);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Thêm sản phẩm cho khách hàng
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="sanphamIds"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_KHACH_HANG)]
        [Route("v1/khachhang/add_sanpham_quantam")]
        [HttpPost]
        public async Task<IHttpActionResult> AddSanphamQuantam(int customerId, int[] sanphamIds)
        {
            var res = await _khachhangService.AddSanphamQuantam(customerId, sanphamIds);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Loại bỏ sản phẩm quan tâm
        /// </summary>
        /// <param name="sanphamQuantamIds"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_KHACH_HANG)]
        [Route("v1/khachhang/remove_sanpham_quantam")]
        [HttpPost]
        public async Task<IHttpActionResult> RemoveSanphamQuantam(int[] sanphamQuantamIds)
        {
            var res = await _khachhangService.RemoveSanphamQuantam(sanphamQuantamIds);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy d/s sản phẩm quan tâm theo customerId
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<List<SanphamQuantamModel>>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/get_sanpham_quantams")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSanphamQuantams(int customerId)
        {
            var res = await _khachhangService.GetSanphamQuantams(customerId);

            return Json(new ResponseResultBase<List<SanphamQuantamModel>>() { result = true, data = res });
        }

        /// <summary>
        /// Thêm tư vấn mới cho khách hàng
        /// </summary>
        /// <param name="tuvan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TU_VAN_KHACH_HANG)]
        [Route("v1/khachhang/add_tu_van")]
        [HttpPost]
        public async Task<IHttpActionResult> AddTuvan(TuvanModel tuvan)
        {
            tuvan.Nguoitao = RequestContext.Principal.Identity.GetUserId();

            var res = await _khachhangService.AddTuvan(tuvan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật tư vấn
        /// </summary>
        /// <param name="tuvan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TU_VAN_KHACH_HANG)]
        [Route("v1/khachhang/update_tu_van")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateTuvan(TuvanModel tuvan)
        {
            var res = await _khachhangService.UpdateTuvan(tuvan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa tư vấn
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_KHACH_HANG)]
        [Route("v1/khachhang/delete_tu_van")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteTuvan(int[] ids)
        {
            var res = await _khachhangService.DeleteTuvan(ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy d/s tư vấn của khách hàng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<ListPager<TuvanModel>>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TU_VAN_KHACH_HANG)]
        [Route("v1/khachhang/get_tu_vans")]
        [HttpPost]
        public async Task<IHttpActionResult> GetTuvans(TuvanFilterModel filter)
        {
            var res = await _khachhangService.GetTuvans(filter);

            return Json(new ResponseResultBase<ListPager<TuvanModel>>() { result = true, data = new ListPager<TuvanModel>() { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy d/s báo giá
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<ListPager<BaogiaModel>>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TU_VAN_KHACH_HANG)]
        [Route("v1/khachhang/get_bao_gias")]
        [HttpPost]
        public async Task<IHttpActionResult> GetBaogias(TuvanFilterModel filter)
        {
            var res = await _khachhangService.GetBaogias(filter);

            return Json(new ResponseResultBase<ListPager<BaogiaModel>>() { result = true, data = new ListPager<BaogiaModel>() { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Lấy chi tiết thông tin báo giá
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<BaogiaDetailModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TU_VAN_KHACH_HANG)]
        [Route("v1/khachhang/get_bao_gia_detail")]
        [HttpGet]
        public async Task<IHttpActionResult> GetBaogiaDetail(int id)
        {
            var res = await _khachhangService.GetBaogiaDetail(id);

            return Json(new ResponseResultBase<BaogiaDetailModel>() { result = true, data = res });
        }

        /// <summary>
        /// Lấy nhu cấu giá của khách hàng
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<List<KhachNhucaugiaModel>>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_KHACH_HANG)]
        [Route("v1/khachhang/get_nhu_cau_gias")]
        [HttpGet]
        public async Task<IHttpActionResult> GetNhucauGias(int customerId)
        {
            var res = await _khachhangService.GetNhucauGias(customerId);

            return Json(new ResponseResultBase<List<KhachNhucaugiaModel>>() { result = true, data = res });
        }

        /// <summary>
        /// Lưu báo giá
        /// </summary>
        /// <param name="baogia"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.TU_VAN_KHACH_HANG)]
        [Route("v1/khachhang/save_bao_gia")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveBaogia(BaogiaDetailModel baogia)
        {
            baogia.Baogia.Nguoitao = RequestContext.Principal.Identity.GetUserId();

            baogia.Baogia.Nguoicapnhat = RequestContext.Principal.Identity.GetUserId();

            var res = await _khachhangService.SaveBaogia(baogia);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

     


    }
}

