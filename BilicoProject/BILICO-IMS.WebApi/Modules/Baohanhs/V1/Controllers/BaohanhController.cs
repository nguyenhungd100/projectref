
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

using Thinktecture.IdentityModel.WebApi;
using Newtonsoft.Json;

using BILICO.IMS.Domain.Services.BaohanhFacade;
using BILICO.IMS.Domain.Services.BaohanhFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;

namespace BILICO.IMS.WebApi.Modules.Baohanhs.V1.Controllers
{
	/// <summary>
    /// API làm việc với Baohanh
    /// </summary>

    [Route("v1/baohanh")]
    public class BaohanhController : ApiController
    {
        IBaohanhService _baohanhService = new BaohanhService();
		
        /// <summary>
        /// Lấy danh sách Baohanh trong hệ thống theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <remarks>API quản lý Baohanh</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_PHIEU_BAO_HANH)]
        [Route("v1/baohanh/list")]
        [HttpPost]
        [ResponseType(typeof(ResponseResultBase<BaohanhModel>))]
        public async Task<IHttpActionResult> List(FilterModel filter)
        {
            var res = await _baohanhService.List(filter);

            return Json(new ResponseResultBase<ListPager<BaohanhModel>>() { result = true, data = new ListPager<BaohanhModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm Baohanh mới
        /// </summary>
        /// <param name="baohanh"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_PHIEU_BAO_HANH)]
        [Route("v1/baohanh/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(BaohanhModel baohanh)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            baohanh.NguoitaoId = userId;

            var res = await _baohanhService.Add(baohanh);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin Baohanh
        /// </summary>
        /// <param name="baohanh"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_PHIEU_BAO_HANH)]
        [Route("v1/baohanh/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(BaohanhModel baohanh)
        {
            var res = await _baohanhService.Update(baohanh);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa Baohanh
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_PHIEU_BAO_HANH)]
        [Route("v1/baohanh/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] Ids)
        {
            var res = await _baohanhService.Delete(Ids);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin Baohanh theo Id
        /// </summary>
        /// <param name="baohanhId"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<BaohanhModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_PHIEU_BAO_HANH)]
        [Route("v1/baohanh/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int baohanhId)
        {
            var res = await _baohanhService.GetById(baohanhId);

            return Json(new ResponseResultBase<BaohanhModel>() { result = true, data = res });
        }
    }
}

