﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Thinktecture.IdentityModel.WebApi;
using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Domain.Services.RoleFacade.Implementation;
using BILICO.IMS.Domain.Services.TaikhoanFacade;
using BILICO.IMS.Domain.Services.TaikhoanFacade.Implementation;
using BILICO.IMS.WebApi.Authorization;
using BILICO.IMS.WebApi.Helpers;

namespace BILICO.IMS.WebApi.Modules.Taikhoans.V1.Controllers
{
    /// <summary>
    /// API làm việc với nhân viên
    /// </summary>
    [Route("v1/taikhoan")]
    public class TaikhoanController : ApiController
    {
        ITaikhoanService _taikhoanService = new TaikhoanService();
        IRoleService _roleService = new RoleService();

        /// <summary>
        /// Lấy tất cả danh sách nhân viên trong hệ thống
        /// </summary>
        /// <returns></returns>
        /// <remarks>API quản lý nhân viên, tài khoản hệ thống</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<ListPager<TaikhoanModel>>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_TAI_KHOAN)]
        [Route("v1/taikhoan/list")]
        [HttpPost]
        public async Task<IHttpActionResult> List(BILICO.IMS.Domain.Services.TaikhoanFacade.FilterModel filter)
        {
            var res = await _taikhoanService.List(filter);

            return Json(new ResponseResultBase<ListPager<TaikhoanModel>>() { result = true, data = new ListPager<TaikhoanModel> { list = res, pager = filter.Paging } });
        }

        /// <summary>
        /// Thêm nhân viên mới
        /// </summary>
        /// <param name="taikhoan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.THEM_TAI_KHOAN)]
        [Route("v1/taikhoan/add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add(TaikhoanModel taikhoan)
        {
            var res = await _taikhoanService.Add(taikhoan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Cập nhật thông tin nhân viên
        /// </summary>
        /// <param name="taikhoan"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_TAI_KHOAN)]
        [Route("v1/taikhoan/update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update(TaikhoanModel taikhoan)
        {
            var res = await _taikhoanService.Update(taikhoan);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Xóa nhân viên
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XOA_TAI_KHOAN)]
        [Route("v1/taikhoan/delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int[] id)
        {
            var res = await _taikhoanService.Delete(id);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Khóa hoặc mở khóa tài khoản
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_TAI_KHOAN)]
        [Route("v1/taikhoan/toggle_lock")]
        [HttpPost]
        public async Task<IHttpActionResult> ToggleLock(int Id)
        {
            var res = await _taikhoanService.ToggleLock(Id);

            return Json(new ResponseResultBase<bool>() { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Lấy thông tin nhân viên theo Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ResponseType(typeof(ResponseResultBase<TaikhoanModel>))]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.XEM_TAI_KHOAN)]
        [Route("v1/taikhoan/get_by_id")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int Id)
        {
            var res = await _taikhoanService.GetByIdAsync(Id);
            return Json(new ResponseResultBase<TaikhoanModel>() { result = true, data = res });
        }

        /// <summary>
        /// Lấy thông tin của tài khoản đang đăng nhập
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/taikhoan/get_my_account")]
        [HttpGet]
        [ResponseType(typeof(ResponseResultBase<TaikhoanModel>))]
        public async Task<IHttpActionResult> GetMyAccount()
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var user = await _taikhoanService.GetByIdAsync(userId);

            return Json(new ResponseResultBase<TaikhoanModel> { result = true, data = user });
        }

        /// <summary>
        /// Lấy danh sách vai trò của user đăng nhập
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [ApiAuthorize]
        [Route("v1/taikhoan/get_my_roles")]
        [HttpGet]
        [ResponseType(typeof(ResponseResultBase<List<RoleModel>>))]
        public async Task<IHttpActionResult> GetMyRoles()
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var user = await _taikhoanService.GetByIdAsync(userId);

            var roleIds = new List<int>();
            var roles = new List<RoleModel>();

            if (!String.IsNullOrEmpty(user.Roles))
            {
                roleIds = user.Roles.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(c => Convert.ToInt32(c)).ToList();

                roles = await _roleService.ListAsync(c => roleIds.Contains(c.RoleId));
            }

            return Json(new ResponseResultBase<List<RoleModel>> { result = true, data = roles });
        }

        /// <summary>
        /// Cập nhật tài khoản user đang đăng nhập
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/taikhoan/save_my_account")]
        [HttpPost]
        [ApiAuthorize]
        public async Task<IHttpActionResult> SaveMyAccount(TaikhoanModel model)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            if (model.TaikhoanId != userId)
            {
                return Json(new { result = false, message = "Không được cập nhật tài khoản khác" });
            }

            var res = await _taikhoanService.Update(model, false);

            return Json(new ResponseResultBase<bool> { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Đổi mật khẩu của user đang đăng nhập
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/taikhoan/change_password")]
        [HttpPost]
        [ApiAuthorize]
        public async Task<IHttpActionResult> ChangePassword(string oldPassword, string newPassword)
        {
            var userId = RequestContext.Principal.Identity.GetUserId();

            var res = await _taikhoanService.ChangePassword(userId, oldPassword, newPassword);

            return Json(new ResponseResultBase<bool> { result = res.Result, message = res.Error });
        }

        /// <summary>
        /// Reset mật khẩu của user
        /// </summary>
        /// <param name="taikhoanId"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        [ResponseType(typeof(ResponseResultBase<bool>))]
        [Route("v1/taikhoan/reset_password")]
        [HttpPost]
        [ApiAuthorize(Domain.Services.RoleFacade.PermissionCode.CAP_NHAT_TAI_KHOAN)]
        public async Task<IHttpActionResult> ResetPassword(int taikhoanId, string newPassword)
        {
            var res = await _taikhoanService.ResetPassword(taikhoanId, newPassword);

            return Json(new ResponseResultBase<bool> { result = res.Result, message = res.Error });
        }
    }
}