﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web;

namespace BILICO.IMS.Framework.Utils
{
    public enum MessageType
    {
        Success,
        Fail
    }

    public class Tool
    {
        public static void Message(string message)
        {
            if (HttpContext.Current == null)
                throw new FrameworkException("FrameworkException does not support this operator when context hasn't been initialized");

            ((Page)HttpContext.Current.Handler).RegisterClientScriptBlock("Validate", "<script language='javascript'>alert('" + message + "');</script>");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="strMessage"></param>
        /// <param name="url"></param>
        public static void ChangeOtherScreen(string message, string url)
        {
            if (HttpContext.Current == null)
                throw new FrameworkException("FrameworkException does not support this operator when context hasn't been initialized");

            ((Page)HttpContext.Current.Handler).RegisterClientScriptBlock("message", "<script language = 'javascript'>alert('" + message + "');document.location.href = '" + url + "';</script>");

        }

        /// <summary>
        /// Hiển thị thông báo friendly, tự động đóng thông báo sau 4 giây.
        /// References: Sử dụng thư viện jquery + jquery_notification
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public static void CoolMessage(string message, MessageType type)
        {
            if (HttpContext.Current == null)
                throw new FrameworkException("FrameworkException does not support this operator when context hasn't been initialized");

            ((Page)HttpContext.Current.Handler).ClientScript.RegisterClientScriptBlock(typeof(Page), "message", "$(document).ready(function(){showAutoCloseMessage('" + (type == MessageType.Success ? "success" : "error") + "','" + message + "');});", true);
        }
    }
}
