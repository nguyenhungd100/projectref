﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;

namespace BILICO.IMS.Framework.Utils
{
    public class Reporter
    {
        private static Reporter _current;
        private IEnumerable _dataSource;
        private string _templatePath;

        protected Reporter(IEnumerable dataSource, string templatePath)
        {
            _dataSource = dataSource;
            _templatePath = templatePath;
        }

        /// <summary>
        /// Create new instance of Reporter
        /// </summary>
        /// <param name="dataSource">data store</param>
        /// <param name="templatePath">template file control</param>
        /// <returns></returns>
        public static Reporter GetInstance(IEnumerable dataSource, string templatePath)
        {
            if (dataSource == null || String.IsNullOrEmpty(templatePath))
            {
                throw new ArgumentNullException();
            }

            if (!File.Exists(HttpContext.Current.Server.MapPath(templatePath)))
            {
                throw new FileNotFoundException();
            }

            _current = new Reporter(dataSource, templatePath);

            return _current;
        }

        /// <summary>
        /// Generate preview report
        /// </summary>
        /// <returns></returns>
        public string GetPreviewReport()
        {
            Repeater repeater = new Repeater();
            repeater.Page = new Page();

            repeater.ItemTemplate = repeater.Page.LoadTemplate(_templatePath);
            repeater.DataSource = _dataSource;
            repeater.DataBind();

            StringBuilder output = new StringBuilder();
            HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(output));
            repeater.RenderControl(writer);
            return output.ToString();
        }
    }
}
