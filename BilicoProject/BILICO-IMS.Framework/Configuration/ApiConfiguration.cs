﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Framework.Configuration
{
    public static class ApiConfiguration
    {
        private static string _apiServer;
        private static string _authServer;

        /// <summary>
        /// Return api server config path. http://api.sample.com
        /// </summary>
        public static string ApiServer
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["ApiServer"]))
                    return _apiServer;

                return ConfigurationManager.AppSettings["ApiServer"];
            }
        }

        /// <summary>
        /// Return authenticate server config path. http://login.sample.com
        /// </summary>
        public static string AuthenticateServer
        {
            get
            {
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["AuthenticateServer"]))
                    return _authServer;

                return ConfigurationManager.AppSettings["AuthenticateServer"];
            }
        }

        public static void Init(string apiServer, string authServer)
        {
            _apiServer = apiServer;
            _authServer = authServer;
        }
    }
}
