﻿using log4net;
using Microsoft.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace BILICO.IMS.Authentication.Helpers
{
    public class GlobalExceptionMiddleware : OwinMiddleware
    {
        public GlobalExceptionMiddleware(OwinMiddleware next)
            : base(next)
        { }

        public override async Task Invoke(IOwinContext context)
        {
            try
            {
                await Next.Invoke(context);
            }
            catch (Exception ex)
            {
                var logger = LogManager.GetLogger(typeof(GlobalExceptionMiddleware));
                logger.Fatal(ex);
            }
        }
    }

    public class GlobalExceptionHandler : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            //log global exception
            var logger = LogManager.GetLogger(typeof(GlobalExceptionHandler));
            if (context.Exception.InnerException != null)
                logger.Fatal(context.Exception.InnerException.ToString());
            else
                logger.Fatal(context.Exception.ToString());

            if (ConfigurationManager.AppSettings["CustomErrorMode"] == "Off")
            {
                throw context.Exception;
            }

            var statusCode = (context.Exception.InnerException is HttpResponseException ? (HttpStatusCode)((HttpResponseException)context.Exception.InnerException).Response.StatusCode : HttpStatusCode.InternalServerError);

            var error_msg = statusCode == HttpStatusCode.Unauthorized ? "Unauthorized" : (statusCode == HttpStatusCode.Forbidden ? "Forbidden" : "Internal Server Error");
            var error_code = statusCode == HttpStatusCode.Unauthorized ? 401 : (statusCode == HttpStatusCode.Forbidden ? 403 : 500);

            context.Response = new HttpResponseMessage
            {
                StatusCode = statusCode,
                ReasonPhrase = error_msg,
                Content = new StringContent(JsonConvert.SerializeObject(new { result = false, message = error_msg, error_code = error_code }), UTF8Encoding.UTF8, "application/json")
            };
        }
    }
}