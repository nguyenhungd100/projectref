﻿using log4net;
using Microsoft.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace BILICO.IMS.Authentication.IdentityServer
{
    public class GlobalExceptionHandler : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            //log global exception
            var logger = LogManager.GetLogger(typeof(GlobalExceptionHandler));
            if (context.Exception.InnerException != null)
                logger.Fatal(context.Exception.InnerException.ToString());
            else
                logger.Fatal(context.Exception.ToString());

            if (ConfigurationManager.AppSettings["CustomErrorMode"] == "Off")
            {
                throw context.Exception;
            }

            var statusCode = (context.Exception.InnerException is HttpResponseException ? (HttpStatusCode)((HttpResponseException)context.Exception.InnerException).Response.StatusCode : HttpStatusCode.InternalServerError);

            var error_msg = statusCode == HttpStatusCode.Unauthorized ? "Unauthorized" : (statusCode == HttpStatusCode.Forbidden ? "Forbidden" : "Internal Server Error");
            var error_code = statusCode == HttpStatusCode.Unauthorized ? 401 : (statusCode == HttpStatusCode.Forbidden ? 403 : 500);

            context.Response = new HttpResponseMessage
            {
                StatusCode = statusCode,
                ReasonPhrase = error_msg,
                Content = new StringContent(JsonConvert.SerializeObject(new { result = false, message = error_msg, error_code = error_code }), UTF8Encoding.UTF8, "application/json")
            };
        }
    }

    //public class GlobalExceptionMiddleware : OwinMiddleware
    //{
    //    public GlobalExceptionMiddleware(OwinMiddleware next) : base(next)
    //    { }

    //    public override async Task Invoke(IOwinContext ctx)
    //    {
    //        try
    //        {
    //            //1. Unauthorized 401 (OK)
    //            //2. Not found 404
    //            //3. OK 200
    //            //4. Internal Exception 500
    //            //5. Forbidden 403
    //            //6. Method Not Allowed 405

    //            if (ctx.Response.StatusCode != 200)
    //            {
    //                IOwinContext context = new OwinContext(ctx.Environment);

    //                // Buffer the response
    //                var stream = context.Response.Body;
    //                var buffer = new MemoryStream();
    //                context.Response.Body = buffer;

    //                await Next.Invoke(ctx);

    //                buffer = new MemoryStream();
    //                string json = JsonConvert.SerializeObject(new { success = "false", message = ctx.Response.ReasonPhrase });

    //                byte[] jsonBytes = System.Text.Encoding.UTF8.GetBytes(json);
    //                buffer.Write(jsonBytes, 0, jsonBytes.Length);
    //                buffer.Seek(0, SeekOrigin.Begin);

    //                // is flushed out to the client application.
    //                buffer.Seek(0, SeekOrigin.Begin);
    //                await buffer.CopyToAsync(stream);
    //            }
    //            else
    //            {
    //                await Next.Invoke(ctx);
    //            }               
    //        }
    //        catch (Exception ex)
    //        {
    //            //display json fail response when exception is not handled

    //            IOwinContext context = new OwinContext(ctx.Environment);

    //            // Buffer the response
    //            var stream = context.Response.Body;
    //            var buffer = new MemoryStream();
    //            context.Response.Body = buffer;

    //            await Next.Invoke(ctx);

    //            buffer = new MemoryStream();
    //            string json = JsonConvert.SerializeObject(new { success = "false", message = ctx.Response.ReasonPhrase });

    //            byte[] jsonBytes = System.Text.Encoding.UTF8.GetBytes(json);
    //            buffer.Write(jsonBytes, 0, jsonBytes.Length);
    //            buffer.Seek(0, SeekOrigin.Begin);

    //            // is flushed out to the client application.
    //            buffer.Seek(0, SeekOrigin.Begin);
    //            await buffer.CopyToAsync(stream);

    //            var logger = LogManager.GetLogger(typeof(GlobalExceptionMiddleware));
    //            logger.Fatal(ex.ToString());
    //        }
    //    }
    //}

    //public class PassthroughExceptionHandler : IExceptionHandler
    //{
    //    public Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
    //    {
    //        // don't just throw the exception; that will ruin the stack trace
    //        var info = ExceptionDispatchInfo.Capture(context.Exception);

    //        info.Throw();

    //        //context.Result = new JsonErrorResult();
    //        return Task.FromResult(0);
    //    }
    //}
}