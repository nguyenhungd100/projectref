﻿using IdentityServer3.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IdentityServer3.Core.Models;
using System.Threading.Tasks;
using IdentityServer3.Core.Services.InMemory;
using IdentityServer3.Core;
using IdentityServer3.Core.Services.Default;
using System.Security.Claims;
using BILICO.IMS.Domain.Services.TaikhoanFacade;
using BILICO.IMS.Domain.Services.TaikhoanFacade.Implementation;
using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Domain.Services.RoleFacade.Implementation;
using BILICO.IMS.Authentication.Helpers;

namespace BILICO.IMS.Authentication.IdentityServer
{
    public class UserService : UserServiceBase
    {
        private ITaikhoanService _userService = new TaikhoanService();

        public async override Task AuthenticateExternalAsync(ExternalAuthenticationContext context)
        {
            var nameClaim = context.ExternalIdentity.Claims.Single(x => x.Type == "username");
            var userName = nameClaim.Value;

            var user = await _userService.SingleAsync(c => c.Email == userName && c.Tinhtrang == true);
            var inmemoryUser = user.ToInMemoryUser();
            context.AuthenticateResult = GetAuthenticateResult(inmemoryUser);
        }

        public async override Task AuthenticateLocalAsync(LocalAuthenticationContext context)
        {
            var password = EncryptUtil.EncryptMD5(context.Password);
            var user = await _userService.SingleAsync(c => c.Email == context.UserName && c.Tinhtrang == true);
            if (user != null)
            {
                if (user.Matkhau != password)
                {
                    context.AuthenticateResult = new AuthenticateResult("Email or password is not valid");
                    return;
                }

                var inmemoryUser = user.ToInMemoryUser();
                var authenticateResult = GetAuthenticateResult(inmemoryUser);
                context.AuthenticateResult = authenticateResult;
            }
            else
            {
                context.AuthenticateResult = new AuthenticateResult("Login fail");
            }
        }

        private static AuthenticateResult GetAuthenticateResult(InMemoryUser user)
        {
            var authenticateResult = new AuthenticateResult(user.Subject,
                user.Username,
                user.Claims,
                Constants.BuiltInIdentityProvider,
                Constants.AuthenticationMethods.Password);

            return authenticateResult;
        }

        public async override Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var userId = TypeSafeConverter.ToInt32(context.Subject.FindFirst(Constants.ClaimTypes.Subject).Value);
            var user = await _userService.GetByIdAsync(userId);
            if (user != null)
            {
                var inmemoryUser = user.ToInMemoryUser();
                context.IssuedClaims = inmemoryUser.Claims;
            }
        }

        public override Task IsActiveAsync(IsActiveContext context)
        {
            context.IsActive = true;
            return Task.FromResult(0);
        }

        public override Task PostAuthenticateAsync(PostAuthenticationContext context)
        {
            return base.PostAuthenticateAsync(context);
        }

        public override Task PreAuthenticateAsync(PreAuthenticationContext context)
        {
            return base.PreAuthenticateAsync(context);
        }

        public override Task SignOutAsync(SignOutContext context)
        {
            return base.SignOutAsync(context);
        }
    }
}