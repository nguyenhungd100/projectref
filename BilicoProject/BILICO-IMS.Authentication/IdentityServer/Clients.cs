﻿using IdentityServer3.Core;
using IdentityServer3.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BILICO.IMS.Authentication.IdentityServer
{
    public static class Clients
    {
        public static IEnumerable<Client> Get()
        {
            var clientUris = ConfigurationManager.AppSettings["ClientAppRedirectUri"];
            var uris = clientUris.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            return new[]
            {
                new Client
                {
                    Enabled = true,
                    ClientName = "Web Application",
                    ClientId = "mvc",
                    Flow = Flows.Implicit,

                    RedirectUris = new List<string>
                    {
                        "http://app.sample.com"
                    },

                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://app.sample.com"
                    },

                    AllowedScopes = new List<string>
                    {
                        "openid",
                        "profile",
                        "roles",
                        "sampleApi"
                    }
                },
                new Client
                {
                    ClientName = "Web Application (service communication)",
                    ClientId = "mvc_service",
                    Flow = Flows.ClientCredentials,

                    ClientSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = new List<string>
                    {
                        "sampleApi"
                    }
                },
                new Client
                {
                    ClientName = "WPF Application (service communication)",
                    ClientId = "desktopApp",
                    Flow = Flows.ResourceOwner,

                    ClientSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = new List<string>
                    {
                        "openid",
                        "profile",
                        "write",
                        "offline_access",
                        "hldpmsApi"
                    }
                },
                new Client
                {
                    Enabled = true,
                    ClientName = "Client script application (service communication)",
                    ClientId = "ro.client",
                    Flow = Flows.ResourceOwner,
                    RedirectUris = new List<string>(uris),
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = new List<string>
                    {
                       "openid",
                       "profile",
                       "offline_access",
                       "bil.ims.Api"
                    },
                    AllowAccessTokensViaBrowser = true,
                    AllowedCorsOrigins = new  List<string>(uris),
                    AccessTokenType = AccessTokenType.Jwt,
                    IdentityTokenLifetime = 3000,
                    AccessTokenLifetime = 3600*24*30,
                    AuthorizationCodeLifetime = 300
                }
            };
        }
    }
}