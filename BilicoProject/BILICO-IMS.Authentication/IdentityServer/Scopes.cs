﻿using IdentityServer3.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BILICO.IMS.Authentication.IdentityServer
{
    public static class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            var scopes = new List<Scope>
            {
                new Scope
                {
                    Enabled = true,
                    DisplayName = "User Roles",
                    Description = "Roles of user",
                    Name = "roles",
                    Type = ScopeType.Identity,
                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim("role")
                    }
                },
                new Scope
                {
                    Enabled = true,
                    DisplayName = "BILICO API",
                    Name = "bil.ims.Api",
                    Description = "Access to a BILICO IMS API",
                    Type = ScopeType.Resource,

                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim("role"),
                        new ScopeClaim("permissions")
                    }
                }
            };

            scopes.Add(StandardScopes.OfflineAccess);
            scopes.AddRange(StandardScopes.All);

            return scopes;
        }
    }
}