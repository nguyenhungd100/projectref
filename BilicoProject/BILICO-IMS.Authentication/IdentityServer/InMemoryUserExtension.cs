﻿using IdentityServer3.Core;
using IdentityServer3.Core.Services.InMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using BILICO.IMS.Authentication.Helpers;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Domain.Services.RoleFacade;
using BILICO.IMS.Domain.Services.RoleFacade.Implementation;
using BILICO.IMS.Domain.Services.TaikhoanFacade;

namespace BILICO.IMS.Authentication.IdentityServer
{
    public static class UserExtension
    {
        public static InMemoryUser ToInMemoryUser(this TaikhoanModel user)
        {
            string result = string.Empty;

            if (!String.IsNullOrEmpty(user.Roles))
            {
                IRoleService _roleService = new RoleService();
                var ids = user.Roles.ParseIds();
                var roles = _roleService.List(c => ids.Contains(c.RoleId));
                var permissions = new List<int>();
                foreach (var role in roles)
                {
                    if (!String.IsNullOrEmpty(role.Permissions))
                    {
                        ids = role.Permissions.ParseIds();
                        permissions.AddRange(ids);
                    }
                }
                var res = permissions.ToArray().RemoveDuplicates();
                result = string.Join(";", res);
                if (!String.IsNullOrEmpty(result))
                    result = ";" + result + ";";
            }

            return new InMemoryUser
            {
                Username = user.Email,
                Password = user.Matkhau,
                Subject = user.TaikhoanId.ToString(),

                Claims = new[]
                {
                    new Claim(Constants.ClaimTypes.GivenName, user.Hovaten),
                    new Claim(Constants.ClaimTypes.Role, !String.IsNullOrEmpty(user.Roles)?user.Roles:""),
                    new Claim("permissions", result),
                    new Claim("user_modified_time_stamp", user.UserModifiedTimeStamp.ToString())
                }
            };
        }
    }
}