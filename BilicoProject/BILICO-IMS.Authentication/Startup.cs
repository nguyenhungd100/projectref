﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using IdentityServer3.Core.Configuration;
using System.Security.Cryptography.X509Certificates;
using BILICO.IMS.Authentication.IdentityServer;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services;
using System.Web.Helpers;
using System.IdentityModel.Tokens;
using System.Collections.Generic;
using IdentityServer3.Core;
using System.Web.Http;
using Serilog;

[assembly: OwinStartup(typeof(BILICO.IMS.Authentication.Startup))]

namespace BILICO.IMS.Authentication
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AntiForgeryConfig.UniqueClaimTypeIdentifier = Constants.ClaimTypes.Subject;
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            var factory = new IdentityServerServiceFactory()
                            .UseInMemoryClients(Clients.Get())
                            //.UseInMemoryUsers(UserService.Get())
                            .UseInMemoryScopes(Scopes.Get());

            factory.UserService = new Registration<IUserService>(new UserService());

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.Map("/oauth2", idsrvApp =>
            {
                // web api configuration
                var config = new HttpConfiguration();
                config.Filters.Add(new GlobalExceptionHandler());
                idsrvApp.UseWebApi(config);

                idsrvApp.UseIdentityServer(new IdentityServerOptions
                {
                    SiteName = "Identity Server",
                    SigningCertificate = LoadCertificate(),
                    Factory = factory,
                    
                    RequireSsl = false,
                    AuthenticationOptions = new IdentityServer3.Core.Configuration.AuthenticationOptions
                    {
                        EnablePostSignOutAutoRedirect = true,
                    }
                });
            });

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Trace()
                .CreateLogger();
        }

        X509Certificate2 LoadCertificate()
        {
            return new X509Certificate2(
                string.Format(@"{0}\bin\identityServer\idsrv3test.pfx", AppDomain.CurrentDomain.BaseDirectory), "idsrv3test");
        }
    }
}
