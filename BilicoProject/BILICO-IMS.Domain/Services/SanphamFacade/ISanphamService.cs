
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.SanphamFacade
{
    public interface ISanphamService
    {
        Task<SanphamModel> GetById(int sanphamId);

        ActionResult Add(SanphamModel sanpham);

        ActionResult Update(SanphamModel sanpham);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<SanphamModel>> List(FilterModel filter);
    }
}


