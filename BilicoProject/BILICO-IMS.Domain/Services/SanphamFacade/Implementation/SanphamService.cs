
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;


namespace BILICO.IMS.Domain.Services.SanphamFacade.Implementation
{
    public class SanphamService : ISanphamService
    {
        IEntityRepository<Sanpham, DomainContext> _sanphamRepository = new EntityRepository<Sanpham, DomainContext>();
        IEntityRepository<Sanpham_Gia, DomainContext> _giaSanphamRepository = new EntityRepository<Sanpham_Gia, DomainContext>();
        IEntityRepository<Nhucaugia, DomainContext> _nhucaugiaRepository = new EntityRepository<Nhucaugia, DomainContext>();

        public ActionResult Add(SanphamModel sanpham)
        {
            //validate
            if (sanpham == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Sanpham không để null"
                };

            if (String.IsNullOrEmpty(sanpham.MaSanpham))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Mã sản phẩm không hợp lệ"
                };

            if (sanpham.Giadauvao <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Giá đầu vào không hợp lệ"
                };

            if (sanpham.Giabans == null || sanpham.Giabans.Count == 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có giá bán theo nhu cầu"
                };

            if (sanpham.Giabans.Any(c => c.Gia <= 0))
                return new ActionResult() { Result = false, Error = "Giá theo nhu cầu phải lớn hơn 0" };

            foreach (var giaban in sanpham.Giabans)
            {
                var nhucau = _nhucaugiaRepository.GetById(giaban.NhucaugiaId);
                if (nhucau == null)
                    return new ActionResult() { Result = false, Error = "Không tồn tại nhu cầu này" };
            }

            var exist = _sanphamRepository.Single(c => c.MaSanpham == sanpham.MaSanpham);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Mã sản phẩm đã tồn tại"
                };
            }

            //add to storage
            sanpham.Ngaytao = DateTime.Now;
            var entity = sanpham.CloneToModel<SanphamModel, Sanpham>();
            var res = _sanphamRepository.Insert(entity);
            if (res)
            {
                sanpham.Giabans.ForEach(c =>
                {
                    c.SanphamId = entity.SanphamId;
                });
                var entities = sanpham.Giabans.CloneToListModels<GiabanSanphamModel, Sanpham_Gia>();
                res = _giaSanphamRepository.InsertMany(entities);
            }
            return new ActionResult() { Result = res, Error = res == false ? "Internal error" : "" };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _giaSanphamRepository.DeleteManyAsync(c => Ids.Contains(c.SanphamId));
            res = await _sanphamRepository.DeleteManyAsync(c => Ids.Contains(c.SanphamId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<SanphamModel> GetById(int sanphamId)
        {
            var entity = await _sanphamRepository.GetByIdAsync(sanphamId);
            var model = entity.CloneToModel<Sanpham, SanphamModel>();

            using (var db = new DomainContext())
            {
                var q = from g in db.Sanpham_Gias
                        join n in db.Nhucaugias on g.NhucaugiaId equals n.NhucauId
                        where g.SanphamId == model.SanphamId
                        select new GiabanSanphamModel()
                        {
                            Gia = g.Gia,
                            Id = g.Id,
                            Nhucau = n.TenNhucau,
                            NhucaugiaId = g.NhucaugiaId,
                            SanphamId = g.SanphamId
                        };
                model.Giabans = q.ToList();
            }//using

            return model;
        }

        public async Task<List<SanphamModel>> List(FilterModel filter)
        {
            var res = await _sanphamRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenSanpham.Contains(filter.Search) || c.Mota.Contains(filter.Search) || c.MaSanpham.Contains(filter.Search) : true), c => c.SanphamId, filter.Paging);

            var models = res.CloneToListModels<Sanpham, SanphamModel>();

            models.ForEach(c =>
            {
                //var giabans = _giaSanphamRepository.Fetch(k => k.SanphamId == c.SanphamId);
                //c.Giabans = giabans.CloneToListModels<Sanpham_Gia, GiabanSanphamModel>();
                using (var db = new DomainContext())
                {
                    var q = from g in db.Sanpham_Gias
                            join n in db.Nhucaugias on g.NhucaugiaId equals n.NhucauId
                            where g.SanphamId == c.SanphamId
                            select new GiabanSanphamModel()
                            {
                                Gia = g.Gia,
                                Id = g.Id,
                                Nhucau = n.TenNhucau,
                                NhucaugiaId = g.NhucaugiaId,
                                SanphamId = g.SanphamId
                            };
                    c.Giabans = q.ToList();
                }
            });

            return models;
        }

        public ActionResult Update(SanphamModel sanpham)
        {
            //validate
            if (sanpham == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "sanpham không để null"
                };

            if (sanpham.Giadauvao <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Giá đầu vào không hợp lệ"
                };

            if (String.IsNullOrEmpty(sanpham.MaSanpham))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Mã sản phẩm không hợp lệ"
                };

            if (sanpham.Giabans == null || sanpham.Giabans.Count == 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có giá bán theo nhu cầu"
                };

            if (sanpham.Giabans.Any(c => c.Gia <= 0))
                return new ActionResult() { Result = false, Error = "Giá theo nhu cầu phải lớn hơn 0" };

            foreach (var giaban in sanpham.Giabans)
            {
                var nhucau = _nhucaugiaRepository.GetById(giaban.NhucaugiaId);
                if (nhucau == null)
                    return new ActionResult() { Result = false, Error = "Không tồn tại nhu cầu này" };
            }

            var exist = _sanphamRepository.GetById(sanpham.SanphamId);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Sản phẩm không tồn tại"
                };

            exist = _sanphamRepository.Single(c => (c.MaSanpham == sanpham.MaSanpham) && c.SanphamId != sanpham.SanphamId);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Mã sản phẩm đã tồn tại"
                };
            }

            //update to storage
            sanpham.Ngaycapnhat = DateTime.Now;
            var entity = sanpham.CloneToModel<SanphamModel, Sanpham>();
            var res = _sanphamRepository.Update(entity);

            if (res)
            {
                sanpham.Giabans.ForEach(c =>
                {
                    c.SanphamId = entity.SanphamId;
                });
                var entities = sanpham.Giabans.CloneToListModels<GiabanSanphamModel, Sanpham_Gia>();

                //add new data
                res = _giaSanphamRepository.InsertMany(entities.Where(c => c.Id == 0));

                //remove old data
                var oldIds = sanpham.Giabans.Where(c => c.Id > 0).Select(c => c.Id);
                var removes = _giaSanphamRepository.DeleteMany(c => c.SanphamId == sanpham.SanphamId && !oldIds.Contains(c.Id));

                //Update old data
                res = _giaSanphamRepository.UpdateMany(entities.Where(c => c.Id > 0));
            }

            return new ActionResult() { Result = res, Error = res == false ? "Internal error" : "" };
        }
    }
}

