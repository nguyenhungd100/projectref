
using System;
using System.Collections.Generic;

namespace BILICO.IMS.Domain.Services.SanphamFacade
{
    public class SanphamModel
    {
        public Int32 SanphamId { set; get; }
        public String AnhSanpham { set; get; }
        public Nullable<Int32> NhacungcapId { set; get; }
        public Nullable<Int32> HangsanxuatId { set; get; }
        public Nullable<Int32> NhomsanphamId { set; get; }
        public String MaSanpham { set; get; }
        public String TenSanpham { set; get; }
        public String Mota { set; get; }
        public Decimal Giadauvao { set; get; }

        public DateTime Ngaytao { set; get; }
        public int NguoitaoId { set; get; }
        public DateTime? Ngaycapnhat { set; get; }
        public int? NguoiCapnhatId { set; get; }

        //Nhu cau gia
        public List<GiabanSanphamModel> Giabans { set; get; }
    }

    public class GiabanSanphamModel
    {
        public int Id { set; get; }
        public int SanphamId { set; get; }
        public int NhucaugiaId { set; get; }
        public decimal Gia { set; get; }

        //not map
        public string Nhucau { set; get; }
    }
}



