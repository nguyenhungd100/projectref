﻿namespace BILICO.IMS.Domain.Services.RoleFacade
{
    public class RoleModel
    {
        public int RoleId { set; get; }
        public string RoleName { set; get; }
        public string Description { set; get; }
        public string Title { set; get; }
        public string Permissions { set; get; }
    }
}