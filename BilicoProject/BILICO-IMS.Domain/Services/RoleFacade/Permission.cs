﻿using System.Collections.Generic;

namespace BILICO.IMS.Domain.Services.RoleFacade
{
    public class Permission
    {
        public PermissionCode Code { set; get; }

        public string Name { set; get; }

        public string HASH_CODE
        {
            get
            {
                return Code.ToString();
            }
        }

        public int GroupId { set; get; }
    }

    public class GroupPermission
    {
        public int GroupId { set; get; }
        public string Name { set; get; }
        public List<Permission> Permissions { set; get; }
    }
}