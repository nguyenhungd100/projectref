﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using BILICO.IMS.Framework.Utils;

namespace BILICO.IMS.Domain.Services.RoleFacade.Implementation
{
    public class RoleService : IRoleService
    {
        IEntityRepository<Role, DomainContext> _roleRepository = new EntityRepository<Role, DomainContext>();

        public async Task<ActionResult> Add(RoleModel role)
        {
            //validate
            if (role == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Role không để null"
                };

            var exist = await _roleRepository.SingleAsync(c => c.RoleName == role.RoleName);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "RoleName đã tồn tại"
                };
            }

            //add to storage
            var entity = role.CloneToModel<RoleModel, Role>();
            var res = await _roleRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int roleId)
        {
            var roleIds = Enum.GetValues(typeof(SystemRoles)).Cast<int>().ToArray();
            if (roleIds.Any(c => c == roleId))
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Không được xóa các role của hệ thống"
                };
            }

            var res = await _roleRepository.DeleteManyAsync(c => c.RoleId == roleId);

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var roleIds = Enum.GetValues(typeof(SystemRoles)).Cast<int>().ToArray();
            if (Ids.Any(c => roleIds.Contains(c)))
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Không được xóa các role của hệ thống"
                };
            }

            var res = await _roleRepository.DeleteManyAsync(c => Ids.Contains(c.RoleId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<RoleModel> GetById(int roleId)
        {
            var res = await _roleRepository.GetByIdAsync(roleId);
            return res.CloneToModel<Role, RoleModel>();
        }

        public async Task<bool> HasPermission(int[] roleIds, PermissionCode permission)
        {
            var code = $";{permission.GetHashCode()};";
            //using (var db = _roleRepository.GetDbContext())
            //{
            //    var roles = await db.Roles.Where(c => roleIds.Contains(c.RoleId)).ToListAsync();
            //    if (roles.Any(c => !String.IsNullOrEmpty(c.Permissions) && c.Permissions.Contains(code)))
            //    {
            //        return true;
            //    }
            //}
            var roles = await _roleRepository.FetchAsync(c => roleIds.Contains(c.RoleId));
            if (roles.Any(c => !String.IsNullOrEmpty(c.Permissions) && c.Permissions.Contains(code)))
            {
                return true;
            }
            return false;
        }

        public async Task<List<RoleModel>> List()
        {
            var res = await _roleRepository.FetchAsync();
            return res.CloneToListModels<Role, RoleModel>();
        }

        public async Task<List<RoleModel>> ListAsync(Expression<Func<Role, bool>> query)
        {
            var res = await _roleRepository.FetchAsync(query);
            return res.CloneToListModels<Role, RoleModel>();
        }

        public List<RoleModel> List(Expression<Func<Role, bool>> query)
        {
            using (var db = _roleRepository.GetDbContext())
            {
                var res = db.Roles.Where(query).ToList();
                return res.CloneToListModels<Role, RoleModel>();
            }
        }

        public List<GroupPermission> ListPermissions()
        {
            var groups = new List<GroupPermission>();
            #region Group
            groups.Add(new GroupPermission()
            {
                GroupId = 1,
                Name = "QUẢN LÝ KHÁCH HÀNG",
            });
            groups.Add(new GroupPermission()
            {
                GroupId = 2,
                Name = "QUẢN LÝ HỢP ĐỒNG",
            });
            groups.Add(new GroupPermission()
            {
                GroupId = 3,
                Name = "QUẢN LÝ PHIẾU BẢO HÀNH",
            });
            groups.Add(new GroupPermission()
            {
                GroupId = 4,
                Name = "QUẢN LÝ DỤNG CỤ",
            });
            groups.Add(new GroupPermission()
            {
                GroupId = 5,
                Name = "QUẢN LÝ DANH MỤC",
            });
            groups.Add(new GroupPermission()
            {
                GroupId = 6,
                Name = "HỆ THỐNG",
            });
            #endregion

            var permissions = new List<Permission>();

            #region Khach hang
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_KHACH_HANG,
                Name = "Thêm mới khách hàng",
                GroupId = 1
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_KHACH_HANG,
                Name = "Cập nhật khách hàng",
                GroupId = 1
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_KHACH_HANG,
                Name = "Xóa khách hàng",
                GroupId = 1
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_KHACH_HANG,
                Name = "Xem khách hàng",
                GroupId = 1
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.TU_VAN_KHACH_HANG,
                Name = "Tư vấn khách hàng",
                GroupId = 1
            });
            #endregion
            //////////////////////////////////////////////////////////////
            #region Hop dong
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_HOP_DONG,
                Name = "Thêm hợp đồng",
                GroupId = 2
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_HOP_DONG,
                Name = "Cập nhật hợp đồng",
                GroupId = 2
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_HOP_DONG,
                Name = "Xóa hợp đồng",
                GroupId = 2
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_HOP_DONG,
                Name = "Xem hợp đồng",
                GroupId = 2
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.TRIEN_KHAI_HOP_DONG,
                Name = "Triển khai hợp đồng",
                GroupId = 2
            });
            #endregion
            //////////////////////////////////////////////////////////////
            #region Bao hanh
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_PHIEU_BAO_HANH,
                Name = "Thêm phiếu bảo hành",
                GroupId = 3
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_PHIEU_BAO_HANH,
                Name = "Cập nhật phiếu bảo hành",
                GroupId = 3
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_PHIEU_BAO_HANH,
                Name = "Xóa phiếu bảo hành",
                GroupId = 3
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_PHIEU_BAO_HANH,
                Name = "Xem phiếu bảo hành",
                GroupId = 3
            });
            #endregion
            //////////////////////////////////////////////////////////////
            #region Dung cu
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_DUNG_CU,
                Name = "Thêm dụng cụ",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_DUNG_CU,
                Name = "Cập nhật dụng cụ",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_DUNG_CU,
                Name = "Xóa dụng cụ",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_DUNG_CU,
                Name = "Xem dụng cụ",
                GroupId = 5
            });
            #endregion

            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAU_HINH_HE_THONG,
                Name = "Cấu hình hệ thống",
                GroupId = 6
            });

            #region Danh mục Hãng sản xuất
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_HANG_SAN_XUAT,
                Name = "Thêm hãng sản xuất",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_HANG_SAN_XUAT,
                Name = "Cập nhật hãng sản xuất",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_HANG_SAN_XUAT,
                Name = "Xóa hãng sản xuất",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_HANG_SAN_XUAT,
                Name = "Xem hãng sản xuất",
                GroupId = 5
            });
            #endregion

            #region Danh mục loại khách hàng
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_LOAI_KHACH_HANG,
                Name = "Thêm loại khách hàng",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_LOAI_KHACH_HANG,
                Name = "Cập nhật loại khách hàng",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_LOAI_KHACH_HANG,
                Name = "Xóa loại khách hàng",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_LOAI_KHACH_HANG,
                Name = "Xem loại khách hàng",
                GroupId = 5
            });
            #endregion

            #region Danh mục nguồn khách hàng
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_NGUON_KHACH_HANG,
                Name = "Thêm nguồn khách hàng",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_NGUON_KHACH_HANG,
                Name = "Cập nhật nguồn khách hàng",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_NGUON_KHACH_HANG,
                Name = "Xóa nguồn khách hàng",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_NGUON_KHACH_HANG,
                Name = "Xem nguồn khách hàng",
                GroupId = 5
            });
            #endregion

            #region Danh mục nhà cung cấp
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_NHA_CUNG_CAP,
                Name = "Thêm nhà cung cấp",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_NHA_CUNG_CAP,
                Name = "Cập nhật nhà cung cấp",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_NHA_CUNG_CAP,
                Name = "Xóa nhà cung cấp",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_NHA_CUNG_CAP,
                Name = "Xem nhà cung cấp",
                GroupId = 5
            });
            #endregion

            #region Danh mục nhóm sản phẩm
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_NHOM_SAN_PHAM,
                Name = "Thêm nhóm sản phẩm",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_NHOM_SAN_PHAM,
                Name = "Cập nhật nhóm sản phẩm",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_NHOM_SAN_PHAM,
                Name = "Xóa nhóm sản phẩm",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_NHOM_SAN_PHAM,
                Name = "Xem nhóm sản phẩm",
                GroupId = 5
            });
            #endregion

            #region Danh mục nhu cầu giá
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_NHU_CAU_GIA,
                Name = "Thêm nhu cầu giá",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_NHU_CAU_GIA,
                Name = "Cập nhật nhu cầu giá",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_NHU_CAU_GIA,
                Name = "Xóa nhu cầu giá",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_NHU_CAU_GIA,
                Name = "Xem nhu cầu giá",
                GroupId = 5
            });
            #endregion

            #region Danh mục quận huyện
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_QUAN_HUYEN,
                Name = "Thêm quận huyện",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_QUAN_HUYEN,
                Name = "Cập nhật quận huyện",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_QUAN_HUYEN,
                Name = "Xóa quận huyện",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_QUAN_HUYEN,
                Name = "Xem quận huyện",
                GroupId = 5
            });
            #endregion

            #region Danh mục Phòng ban
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_PHONG_BAN,
                Name = "Thêm phòng ban",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_PHONG_BAN,
                Name = "Cập nhật phòng ban",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_PHONG_BAN,
                Name = "Xóa phòng ban",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_PHONG_BAN,
                Name = "Xem phòng ban",
                GroupId = 5
            });
            #endregion

            #region Danh muc THANH pho
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_THANH_PHO,
                Name = "Thêm thành phố",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_THANH_PHO,
                Name = "Cập nhật thành phố",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_THANH_PHO,
                Name = "Xóa thành phố",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_THANH_PHO,
                Name = "Xem thành phố",
                GroupId = 5
            });
            #endregion

            #region Danh muc sản phẩm
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_SAN_PHAM,
                Name = "Thêm sản phẩm",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_SAN_PHAM,
                Name = "Cập nhật sản phẩm",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_SAN_PHAM,
                Name = "Xóa sản phẩm",
                GroupId = 5
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_SAN_PHAM,
                Name = "Xem sản phẩm",
                GroupId = 5
            });
            #endregion

            //////////////////////////////////////////////////////////////
            #region Vai tro
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_VAI_TRO,
                Name = "Thêm vai trò",
                GroupId = 6
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_VAI_TRO,
                Name = "Cập nhật vai trò",
                GroupId = 6
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_VAI_TRO,
                Name = "Xóa vai trò",
                GroupId = 6
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_VAI_TRO,
                Name = "Xem vai trò",
                GroupId = 6
            });
            #endregion

            #region Tai khoan
            permissions.Add(new Permission()
            {
                Code = PermissionCode.THEM_TAI_KHOAN,
                Name = "Thêm tài khoản",
                GroupId = 6
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.CAP_NHAT_TAI_KHOAN,
                Name = "Cập nhật tài khoản",
                GroupId = 6
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XOA_TAI_KHOAN,
                Name = "Xóa tài khoản",
                GroupId = 6
            });
            permissions.Add(new Permission()
            {
                Code = PermissionCode.XEM_TAI_KHOAN,
                Name = "Xem tài khoản",
                GroupId = 6
            });
            #endregion

            foreach (var group in groups)
            {
                var groupPermissions = permissions.Where(c => c.GroupId == group.GroupId);
                group.Permissions = new List<Permission>();
                group.Permissions.AddRange(groupPermissions);
            }

            return groups;
        }

        public async Task<ActionResult> Update(RoleModel staff)
        {
            //validate
            if (staff == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Role không để null"
                };

            var exist = await _roleRepository.SingleAsync(c => c.RoleName == staff.RoleName && c.RoleId != staff.RoleId);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "RoleName đã tồn tại"
                };
            }
            exist = await _roleRepository.GetByIdAsync(staff.RoleId);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Role khong tồn tại"
                };

            //update old password to storage and not change
            var entity = staff.CloneToModel<RoleModel, Role>();
            var res = await _roleRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}
