﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.RoleFacade
{
    /// <summary>
    /// Các vai trò hệ thống không được phép xóa
    /// </summary>
    public enum SystemRoles
    {
        /// <summary>
        /// Quản trị hệ thống
        /// </summary>
        Administrators = 1011,

        /// <summary>
        /// Nhân viên marketing
        /// </summary>
        Marketers = 1012,

        /// <summary>
        /// Nhân viên kinh doanh
        /// </summary>
        Salers = 1013,

        /// <summary>
        /// Nhân viên kỹ thuật
        /// </summary>
        Technician = 1014,

        /// <summary>
        /// Quản lý kinh doanh
        /// </summary>
        SalesManagement = 1015
    }
}
