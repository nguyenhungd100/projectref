﻿namespace BILICO.IMS.Domain.Services.RoleFacade
{
    public enum PermissionCode
    {
        THEM_KHACH_HANG = 1,
        CAP_NHAT_KHACH_HANG = 2,
        XOA_KHACH_HANG = 3,
        XEM_KHACH_HANG = 4,
        TU_VAN_KHACH_HANG = 22,

        THEM_PHIEU_BAO_HANH = 5,
        CAP_NHAT_PHIEU_BAO_HANH = 6,
        XOA_PHIEU_BAO_HANH = 7,
        XEM_PHIEU_BAO_HANH = 8,

        THEM_DUNG_CU = 9,
        CAP_NHAT_DUNG_CU = 10,
        XOA_DUNG_CU = 11,
        XEM_DUNG_CU = 12,

        THEM_HANG_SAN_XUAT = 13,
        CAP_NHAT_HANG_SAN_XUAT = 14,
        XOA_HANG_SAN_XUAT = 15,
        XEM_HANG_SAN_XUAT = 16,

        THEM_HOP_DONG = 17,
        CAP_NHAT_HOP_DONG = 18,
        XOA_HOP_DONG = 19,
        XEM_HOP_DONG = 20,
        TRIEN_KHAI_HOP_DONG = 21,

        THEM_LOAI_KHACH_HANG = 23,
        CAP_NHAT_LOAI_KHACH_HANG = 24,
        XOA_LOAI_KHACH_HANG = 25,
        XEM_LOAI_KHACH_HANG = 26,

        THEM_NGUON_KHACH_HANG = 27,
        CAP_NHAT_NGUON_KHACH_HANG = 28,
        XOA_NGUON_KHACH_HANG = 29,
        XEM_NGUON_KHACH_HANG = 30,

        THEM_NHA_CUNG_CAP = 31,
        CAP_NHAT_NHA_CUNG_CAP = 32,
        XOA_NHA_CUNG_CAP = 33,
        XEM_NHA_CUNG_CAP = 34,

        THEM_NHOM_SAN_PHAM = 35,
        CAP_NHAT_NHOM_SAN_PHAM = 36,
        XOA_NHOM_SAN_PHAM = 37,
        XEM_NHOM_SAN_PHAM = 38,

        THEM_NHU_CAU_GIA = 39,
        CAP_NHAT_NHU_CAU_GIA = 40,
        XOA_NHU_CAU_GIA = 41,
        XEM_NHU_CAU_GIA = 42,

        THEM_PHONG_BAN = 43,
        CAP_NHAT_PHONG_BAN = 44,
        XOA_PHONG_BAN = 45,
        XEM_PHONG_BAN = 46,

        THEM_QUAN_HUYEN = 47,
        CAP_NHAT_QUAN_HUYEN = 48,
        XOA_QUAN_HUYEN = 49,
        XEM_QUAN_HUYEN = 50,

        THEM_THANH_PHO = 51,
        CAP_NHAT_THANH_PHO = 52,
        XOA_THANH_PHO = 53,
        XEM_THANH_PHO = 54,

        THEM_VAI_TRO = 55,
        CAP_NHAT_VAI_TRO = 56,
        XOA_VAI_TRO = 57,
        XEM_VAI_TRO = 58,

        THEM_SAN_PHAM = 59,
        CAP_NHAT_SAN_PHAM = 60,
        XOA_SAN_PHAM = 61,
        XEM_SAN_PHAM = 62,

        THEM_TAI_KHOAN = 63,
        CAP_NHAT_TAI_KHOAN = 64,
        XOA_TAI_KHOAN = 65,
        XEM_TAI_KHOAN = 66,

        CAU_HINH_HE_THONG = 67,
    }
}