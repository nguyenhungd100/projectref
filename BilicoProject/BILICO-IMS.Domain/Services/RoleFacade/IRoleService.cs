﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Data.Entity;

namespace BILICO.IMS.Domain.Services.RoleFacade
{
    public interface IRoleService
    {
        Task<ActionResult> Add(RoleModel role);

        Task<ActionResult> Update(RoleModel role);

        Task<ActionResult> Delete(int roleId);

        Task<ActionResult> Delete(int[] roleIds);

        Task<RoleModel> GetById(int roleId);

        Task<List<RoleModel>> List();

        Task<List<RoleModel>> ListAsync(Expression<Func<Role, bool>> query);

        List<RoleModel> List(Expression<Func<Role, bool>> query);

        Task<bool> HasPermission(int[] roleIds, PermissionCode permission);

        List<GroupPermission> ListPermissions();
    }
}
