
using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.NhacungcapFacade 		
{
	public class FilterModel
	{
		public string Search { set; get; }

        public PagingInfo Paging { set; get; }
	}
}


