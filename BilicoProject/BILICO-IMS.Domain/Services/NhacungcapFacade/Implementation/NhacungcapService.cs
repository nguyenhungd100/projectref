
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Linq.Expressions;

namespace BILICO.IMS.Domain.Services.NhacungcapFacade.Implementation
{
    public class NhacungcapService : INhacungcapService
    {
        IEntityRepository<Nhacungcap, DomainContext> _nhacungcapRepository = new EntityRepository<Nhacungcap, DomainContext>();

        public async Task<ActionResult> Add(NhacungcapModel nhacungcap)
        {
            //validate
            if (nhacungcap == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhacungcap không để null"
                };

            //add to storage
            var entity = nhacungcap.CloneToModel<NhacungcapModel, Nhacungcap>();
            var res = await _nhacungcapRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _nhacungcapRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<NhacungcapModel> GetById(int nhacungcapId)
        {
            var entity = await _nhacungcapRepository.GetByIdAsync(nhacungcapId);
            return entity.CloneToModel<Nhacungcap, NhacungcapModel>();
        }

        public async Task<List<NhacungcapModel>> List(FilterModel filter)
        {
            var res = await _nhacungcapRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenNhacungcap.Contains(filter.Search) : true), c => c.Id, filter.Paging);

            return res.CloneToListModels<Nhacungcap, NhacungcapModel>();
        }

        public async Task<NhacungcapModel> SingleAsync(Expression<Func<Nhacungcap, bool>> exp)
        {
            var entity = await _nhacungcapRepository.SingleAsync(exp);
            return entity.CloneToModel<Nhacungcap, NhacungcapModel>();
        }

        public NhacungcapModel Single(Expression<Func<Nhacungcap, bool>> exp)
        {
            var entity = _nhacungcapRepository.Single(exp);
            return entity.CloneToModel<Nhacungcap, NhacungcapModel>();
        }

        public async Task<ActionResult> Update(NhacungcapModel nhacungcap)
        {
            //validate
            if (nhacungcap == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nhacungcap không để null"
                };

            var exist = await _nhacungcapRepository.GetByIdAsync(nhacungcap.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nhacungcap không tồn tại"
                };

            //update to storage
            var entity = nhacungcap.CloneToModel<NhacungcapModel, Nhacungcap>();
            var res = await _nhacungcapRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

