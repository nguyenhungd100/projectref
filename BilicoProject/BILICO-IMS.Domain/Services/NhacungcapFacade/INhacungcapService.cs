
using BILICO.IMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.NhacungcapFacade
{
    public interface INhacungcapService
    {
        Task<NhacungcapModel> GetById(int nhacungcapId);

        Task<ActionResult> Add(NhacungcapModel nhacungcap);

        Task<ActionResult> Update(NhacungcapModel nhacungcap);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<NhacungcapModel>> List(FilterModel filter);

        Task<NhacungcapModel> SingleAsync(Expression<Func<Nhacungcap, bool>> exp);

        NhacungcapModel Single(Expression<Func<Nhacungcap, bool>> exp);
    }
}


