﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using BILICO.IMS.Framework.Utils;

namespace BILICO.IMS.Domain.Services.ConfigurationFacade.Implementation
{
    public class ConfigurationService : IConfigurationService
    {
        IEntityRepository<Configuration, DomainContext> _configRepository = new EntityRepository<Configuration, DomainContext>();

        public async Task<ConfigurationModel> GetConfig()
        {
            var entity = await _configRepository.SingleAsync(c => true == true);
            return entity.CloneToModel<Configuration, ConfigurationModel>();
        }

        public async Task<ActionResult> Save(ConfigurationModel config)
        {
            if (config == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Config không được phép null"
                };

            var entity = config.CloneToModel<ConfigurationModel, Configuration>();

            if (entity.Id == 0)
            {
                var res = await _configRepository.AddAsync(entity);

                return new ActionResult()
                {
                    Result = (res != null ? true : false)
                };
            }
            else
            {
                var res = await _configRepository.UpdateAsync(entity);

                return new ActionResult()
                {
                    Result = res
                };
            }
        }
    }
}
