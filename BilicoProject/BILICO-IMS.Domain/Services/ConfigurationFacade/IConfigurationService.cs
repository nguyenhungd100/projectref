﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.ConfigurationFacade
{
    public interface IConfigurationService
    {
        Task<ActionResult> Save(ConfigurationModel config);

        Task<ConfigurationModel> GetConfig();
    }
}
