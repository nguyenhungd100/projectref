
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;	
using BILICO.IMS.Domain.Data;	
using BILICO.IMS.Domain.Data.Entity;	
using BILICO.IMS.Framework.Data;	


namespace BILICO.IMS.Domain.Services.LoaiKhachhangFacade.Implementation
{
    public class LoaiKhachhangService : ILoaiKhachhangService
    {
        IEntityRepository<LoaiKhachhang, DomainContext> _loaikhachhangRepository = new EntityRepository<LoaiKhachhang, DomainContext>();

        public async Task<ActionResult> Add(LoaiKhachhangModel loaikhachhang)
        {
            //validate
            if (loaikhachhang == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "LoaiKhachhang không để null"
                };

            //add to storage
            var entity = loaikhachhang.CloneToModel<LoaiKhachhangModel, LoaiKhachhang>();
            var res = await _loaikhachhangRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _loaikhachhangRepository.DeleteManyAsync(c => Ids.Contains(c.LoaiKhachhangId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<LoaiKhachhangModel> GetById(int loaikhachhangId)
        {
            var entity = await _loaikhachhangRepository.GetByIdAsync(loaikhachhangId);
            return entity.CloneToModel<LoaiKhachhang, LoaiKhachhangModel>();
        }

        public async Task<List<LoaiKhachhangModel>> List(FilterModel filter)
        {
            var res = await _loaikhachhangRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenLoai.Contains(filter.Search) : true), c => c.LoaiKhachhangId, filter.Paging);

            return res.CloneToListModels<LoaiKhachhang, LoaiKhachhangModel>();
        }

        public async Task<ActionResult> Update(LoaiKhachhangModel loaikhachhang)
        {
            //validate
            if (loaikhachhang == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "loaikhachhang không để null"
                };
				            
            var exist = await _loaikhachhangRepository.GetByIdAsync(loaikhachhang.LoaiKhachhangId);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "loaikhachhang không tồn tại"
                };

            //update to storage
            var entity = loaikhachhang.CloneToModel<LoaiKhachhangModel, LoaiKhachhang>();
            var res = await _loaikhachhangRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

