
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.LoaiKhachhangFacade 		
{
	public interface ILoaiKhachhangService
	{
		Task<LoaiKhachhangModel> GetById(int loaikhachhangId);

        Task<ActionResult> Add(LoaiKhachhangModel loaikhachhang);

        Task<ActionResult> Update(LoaiKhachhangModel loaikhachhang);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<LoaiKhachhangModel>> List(FilterModel filter);
	}
}


