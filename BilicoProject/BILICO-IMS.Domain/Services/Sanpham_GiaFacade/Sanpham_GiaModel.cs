
using System;
namespace BILICO.IMS.Domain.Services.Sanpham_GiaFacade 		
{
	public class Sanpham_GiaModel
	{
			public Int32 Id { set; get; }
			public Int32 SanphamId { set; get; }
			public Int32 LoaiKhachhangId { set; get; }
			public Decimal Gia { set; get; }
	}
}



