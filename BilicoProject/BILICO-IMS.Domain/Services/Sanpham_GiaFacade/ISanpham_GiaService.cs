
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.Sanpham_GiaFacade 		
{
	public interface ISanpham_GiaService
	{
		Task<Sanpham_GiaModel> GetById(int sanpham_giaId);

        Task<ActionResult> Add(Sanpham_GiaModel sanpham_gia);

        Task<ActionResult> Update(Sanpham_GiaModel sanpham_gia);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<Sanpham_GiaModel>> List(FilterModel filter);
	}
}


