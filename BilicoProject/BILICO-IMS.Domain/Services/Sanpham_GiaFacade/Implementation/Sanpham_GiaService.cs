
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;	
using BILICO.IMS.Domain.Data;	
using BILICO.IMS.Domain.Data.Entity;	
using BILICO.IMS.Framework.Data;	


namespace BILICO.IMS.Domain.Services.Sanpham_GiaFacade.Implementation
{
    public class Sanpham_GiaService : ISanpham_GiaService
    {
        IEntityRepository<Sanpham_Gia, DomainContext> _sanpham_giaRepository = new EntityRepository<Sanpham_Gia, DomainContext>();

        public async Task<ActionResult> Add(Sanpham_GiaModel sanpham_gia)
        {
            //validate
            if (sanpham_gia == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Sanpham_Gia không để null"
                };

            //add to storage
            var entity = sanpham_gia.CloneToModel<Sanpham_GiaModel, Sanpham_Gia>();
            var res = await _sanpham_giaRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _sanpham_giaRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<Sanpham_GiaModel> GetById(int sanpham_giaId)
        {
            var entity = await _sanpham_giaRepository.GetByIdAsync(sanpham_giaId);
            return entity.CloneToModel<Sanpham_Gia, Sanpham_GiaModel>();
        }

        public async Task<List<Sanpham_GiaModel>> List(FilterModel filter)
        {
            var res = await _sanpham_giaRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? true : true), c => c.Id, filter.Paging);

            return res.CloneToListModels<Sanpham_Gia, Sanpham_GiaModel>();
        }

        public async Task<ActionResult> Update(Sanpham_GiaModel sanpham_gia)
        {
            //validate
            if (sanpham_gia == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "sanpham_gia không để null"
                };
				            
            var exist = await _sanpham_giaRepository.GetByIdAsync(sanpham_gia.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "sanpham_gia không tồn tại"
                };

            //update to storage
            var entity = sanpham_gia.CloneToModel<Sanpham_GiaModel, Sanpham_Gia>();
            var res = await _sanpham_giaRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

