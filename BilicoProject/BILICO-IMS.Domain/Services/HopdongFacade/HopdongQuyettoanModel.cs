﻿using System;
using System.Collections.Generic;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongQuyettoanModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int NguoiQuyettoanId { set; get; }
        public DateTime NgayQuyettoan { set; get; }
        public int NguoiDuyetQuyettoanId { set; get; }
        public string Ghichu { set; get; }
        public int? TamungId { set; get; }

        public List<ChiphiQuyettoanModel> Chiphis { set; get; }

        //Tong chi phi tam ung
        public decimal? TongTamung { set; get; }

        public decimal? TongChiPhi { set; get; }

        public decimal? Conlai { set; get; }
    }
}