﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class SanphamHopdongModel
    {
        public int Id { get; set; }
        public int HopdongId { get; set; }
        public int SanphamId { get; set; }
        public string MaSP { set; get; }
        public int Soluong { get; set; }
        public decimal Gia { get; set; }
        public decimal Thanhtien { get; set; }

        //not map
        public string TenSanpham { set; get; }
        public string AnhSanpham { get; set; }
    }
}