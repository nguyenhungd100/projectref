﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongVattuModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public string Vattu { set; get; }
        public DateTime Ngaygiao { set; get; }
        public DateTime? Ngaynhan { set; get; }
        public int NguoigiaoId { set; get; }
        public int? NguoinhanId { set; get; }
        public string Ghichu { set; get; }
    }
}