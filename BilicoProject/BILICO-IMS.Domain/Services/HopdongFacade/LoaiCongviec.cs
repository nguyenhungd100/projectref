﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    /// <summary>
    /// Loại báo cáo công việc
    /// </summary>
    public enum LoaiBaocaoCongviec
    {
        /// <summary>
        /// Thời gian đi xe
        /// </summary>
        Thoi_gian_di_xe = 1,

        /// <summary>
        /// Thời gian chờ vật tư
        /// </summary>
        Thoi_gian_cho_vat_tu = 2,

        /// <summary>
        /// Thời gian chuẩn bị vật tư
        /// </summary>
        Thoi_gian_chuan_bi_vat_tu = 3,

        /// <summary>
        /// Thời gian thi công
        /// </summary>
        Thoi_gian_thi_cong = 4,

        /// <summary>
        /// Đi tỉnh
        /// </summary>
        Di_tinh = 5,

        /// <summary>
        /// Khác
        /// </summary>
        Khac = 6
    }
}
