using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Data.Entity;
using BILICO.IMS.Domain.Services.KhachhangFacade.Implementation;

namespace BILICO.IMS.Domain.Services.HopdongFacade.Implementation
{
    public class HopdongService : IHopdongService
    {
        #region Private fields
        IEntityRepository<Hopdong, DomainContext> _hopdongRepository = new EntityRepository<Hopdong, DomainContext>();
        IEntityRepository<Hopdong_DanhgiaNoibo, DomainContext> _hopdongDanhgiaRepository = new EntityRepository<Hopdong_DanhgiaNoibo, DomainContext>();
        IEntityRepository<Hopdong_Dukienchi, DomainContext> _hopdongDukienchiRepository = new EntityRepository<Hopdong_Dukienchi, DomainContext>();
        IEntityRepository<Hopdong_Dukienhangcanthiet, DomainContext> _hopdongDukienhangcanthietRepository = new EntityRepository<Hopdong_Dukienhangcanthiet, DomainContext>();
        IEntityRepository<Hopdong_Dukienthu, DomainContext> _hopdongDukienthuRepository = new EntityRepository<Hopdong_Dukienthu, DomainContext>();
        IEntityRepository<Hopdong_Giaoviec, DomainContext> _hopdongGiaoviecRepository = new EntityRepository<Hopdong_Giaoviec, DomainContext>();
        IEntityRepository<Hopdong_Kehoach, DomainContext> _hopdongKehoachRepository = new EntityRepository<Hopdong_Kehoach, DomainContext>();
        IEntityRepository<Hopdong_Nhansu, DomainContext> _hopdongNhansuRepository = new EntityRepository<Hopdong_Nhansu, DomainContext>();
        IEntityRepository<Hopdong_Nhatkythicong, DomainContext> _hopdongNhatkythicongRepository = new EntityRepository<Hopdong_Nhatkythicong, DomainContext>();
        IEntityRepository<Hopdong_Phanhoi, DomainContext> _hopdongPhanhoiRepository = new EntityRepository<Hopdong_Phanhoi, DomainContext>();
        IEntityRepository<Hopdong_Quyettoan, DomainContext> _hopdongQuyettoanRepository = new EntityRepository<Hopdong_Quyettoan, DomainContext>();
        IEntityRepository<Hopdong_Quyettoan_Chiphi, DomainContext> _hopdongQuyettoanChiphiRepository = new EntityRepository<Hopdong_Quyettoan_Chiphi, DomainContext>();
        IEntityRepository<Hopdong_Suco, DomainContext> _hopdongSucoRepository = new EntityRepository<Hopdong_Suco, DomainContext>();
        IEntityRepository<Hopdong_TailieuDinhkem, DomainContext> _hopdongTailieuDinhkemRepository = new EntityRepository<Hopdong_TailieuDinhkem, DomainContext>();
        IEntityRepository<Hopdong_TailieuDinhkem_AnhCongtrinh, DomainContext> _hopdongTailieuDinhkemAnhCongtrinhRepository = new EntityRepository<Hopdong_TailieuDinhkem_AnhCongtrinh, DomainContext>();
        IEntityRepository<Hopdong_Tamung, DomainContext> _hopdongTamungRepository = new EntityRepository<Hopdong_Tamung, DomainContext>();
        IEntityRepository<Hopdong_Thanhtoan, DomainContext> _hopdongThanhtoanRepository = new EntityRepository<Hopdong_Thanhtoan, DomainContext>();
        IEntityRepository<Hopdong_Vattu, DomainContext> _hopdongVattuRepository = new EntityRepository<Hopdong_Vattu, DomainContext>();
        IEntityRepository<Hopdong_Sanpham, DomainContext> _hopdongSanphamRepository = new EntityRepository<Hopdong_Sanpham, DomainContext>();
        IEntityRepository<Hopdong_Dichvu, DomainContext> _hopdongDichvuRepository = new EntityRepository<Hopdong_Dichvu, DomainContext>();
        IEntityRepository<Sanpham, DomainContext> _sanphamRepository = new EntityRepository<Sanpham, DomainContext>();

        #endregion

        public async Task<ActionResult> AddDanhgiaNoibo(HopdongDanhgiaNoiboModel danhgia)
        {
            //validate
            if (danhgia == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Danhgia không để null"
                };

            if (danhgia.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (danhgia.KythuatId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn kỹ thuật"
                };

            if (danhgia.Tiendo <= 0 || danhgia.Chatluong <= 0 || danhgia.PhanhoiKH <= 0 || danhgia.Chiphi <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn điểm đánh giá"
                };

            if (String.IsNullOrEmpty(danhgia.Noidung))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung ý kiến đánh giá"
                };

            //add to storage
            danhgia.Ngaydanhgia = DateTime.Now;
            var entity = danhgia.CloneToModel<HopdongDanhgiaNoiboModel, Hopdong_DanhgiaNoibo>();
            var res = await _hopdongDanhgiaRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddDukienchi(HopdongDukienchiModel dukienchi)
        {
            //validate
            if (dukienchi == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Dukienchi không để null"
                };

            if (dukienchi.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (dukienchi.Sotien <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền phải lớn hơn 0"
                };

            if (String.IsNullOrEmpty(dukienchi.Lydochi))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có lý do chi"
                };

            //add to storage
            dukienchi.Ngaytao = DateTime.Now;
            var entity = dukienchi.CloneToModel<HopdongDukienchiModel, Hopdong_Dukienchi>();
            var res = await _hopdongDukienchiRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddDukienhangcanthiet(HopdongDukienhangcanthietModel dukienhang)
        {
            //validate
            if (dukienhang == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Dukienhang không để null"
                };


            if (dukienhang.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (String.IsNullOrEmpty(dukienhang.Chungloai))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có chủng loại"
                };

            if (String.IsNullOrEmpty(dukienhang.TenMathang))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tên mặt hàng là bắt buộc"
                };

            if (dukienhang.Tylechot <= 0 || dukienhang.Tylechot > 100)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tỷ lệ chốt trong khoảng 1 - 100%"
                };

            //add to storage
            dukienhang.Ngaytao = DateTime.Now;
            var entity = dukienhang.CloneToModel<HopdongDukienhangcanthietModel, Hopdong_Dukienhangcanthiet>();
            var res = await _hopdongDukienhangcanthietRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddDukienthu(HopdongDukienthuModel dukienthu)
        {
            //validate
            if (dukienthu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Dukienthu không để null"
                };

            if (dukienthu.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (dukienthu.Sotien <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền phải lớn hơn 0"
                };

            if (String.IsNullOrEmpty(dukienthu.Noidungthu))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung thu"
                };

            //add to storage
            dukienthu.Ngaytao = DateTime.Now;
            var entity = dukienthu.CloneToModel<HopdongDukienthuModel, Hopdong_Dukienthu>();
            var res = await _hopdongDukienthuRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddGiaoviec(HopdongGiaoviecModel giaoviec)
        {
            //validate
            if (giaoviec == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Giaoviec không để null"
                };

            if (giaoviec.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (giaoviec.KehoachId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hạng mục kế hoạch"
                };

            if (giaoviec.NguoithuchienId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người thực hiện"
                };

            if (String.IsNullOrEmpty(giaoviec.NoidungCongviec))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung công việc"
                };

            //add to storage
            var entity = giaoviec.CloneToModel<HopdongGiaoviecModel, Hopdong_Giaoviec>();
            var res = await _hopdongGiaoviecRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddKehoach(HopdongKehoachModel kehoach)
        {
            //validate
            if (kehoach == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Kehoach không để null"
                };

            if (kehoach.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (String.IsNullOrEmpty(kehoach.NoidungThuchien))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung thực hiện"
                };

            //add to storage
            var entity = kehoach.CloneToModel<HopdongKehoachModel, Hopdong_Kehoach>();
            var res = await _hopdongKehoachRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddNhansu(HopdongNhansuModel nhansu)
        {
            //validate
            if (nhansu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhansu không để null"
                };

            if (nhansu.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (nhansu.UserId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn kỹ thuật tham gia"
                };

            if (String.IsNullOrEmpty(nhansu.Vaitro))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Vai trò là bắt buộc"
                };

            //add to storage
            var entity = nhansu.CloneToModel<HopdongNhansuModel, Hopdong_Nhansu>();
            var res = await _hopdongNhansuRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddNhatkythicong(HopdongNhatkythicongModel nhatkythicong)
        {
            //validate
            if (nhatkythicong == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhatkythicong không để null"
                };

            if (nhatkythicong.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (nhatkythicong.KehoachId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hạng mục kế hoạch"
                };

            if (nhatkythicong.NguoibaocaoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn kỹ thuật báo cáo"
                };

            if (nhatkythicong.AnhDinhkem == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn ảnh đính kèm"
                };

            if (String.IsNullOrEmpty(nhatkythicong.Noidungbaocao))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Vai trò là bắt buộc"
                };

            //add to storage
            nhatkythicong.Ngaybaocao = DateTime.Now;
            var entity = nhatkythicong.CloneToModel<HopdongNhatkythicongModel, Hopdong_Nhatkythicong>();
            entity.ThoigianThuchien = entity.Ketthuc.Subtract(entity.Batdau).Hours;
            var res = await _hopdongNhatkythicongRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddPhanhoi(HopdongPhanhoiModel phanhoi)
        {
            //validate
            if (phanhoi == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhatkythicong không để null"
                };

            if (phanhoi.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (phanhoi.KythuatId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn kỹ thuật báo cáo"
                };

            if (String.IsNullOrEmpty(phanhoi.Nhanxet))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhận xét là bắt buộc"
                };

            //add to storage
            var entity = phanhoi.CloneToModel<HopdongPhanhoiModel, Hopdong_Phanhoi>();
            var res = await _hopdongPhanhoiRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddQuyettoan(HopdongQuyettoanModel quyettoan)
        {
            //validate
            if (quyettoan == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Quyettoan không để null"
                };

            if (quyettoan.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (quyettoan.NguoiQuyettoanId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người quyết toán"
                };

            if (quyettoan.NguoiDuyetQuyettoanId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người duyệt quyết toán"
                };

            if (quyettoan.Chiphis == null || quyettoan.Chiphis.Count <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có chi phí quyết toán"
                };

            if (quyettoan.Chiphis.Any(c => c.Sotien <= 0))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền chi phí không hợp lệ"
                };

            //check xem có tồn tại tạm ứng nào chưa quyết toán ko
            var tamung = await _hopdongTamungRepository.SingleAsync(c => c.HopdongId == quyettoan.HopdongId && c.NguoitamungId == quyettoan.NguoiQuyettoanId && c.Trangthai == false);
            if (tamung == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Kỹ thuật này đã quyết toán hết các lần tạm ứng"
                };

            //add to storage
            quyettoan.TamungId = tamung.Id;
            var entity = quyettoan.CloneToModel<HopdongQuyettoanModel, Hopdong_Quyettoan>();
            var res = await _hopdongQuyettoanRepository.InsertAsync(entity);
            if (res)
            {
                //them chi phi
                quyettoan.Chiphis.ForEach(c => { c.HopdongId = quyettoan.HopdongId; c.QuyettoanId = entity.Id; });
                var chiphis = quyettoan.Chiphis.CloneToListModels<ChiphiQuyettoanModel, Hopdong_Quyettoan_Chiphi>();
                await _hopdongQuyettoanChiphiRepository.InsertManyAsync(chiphis);

                //cap nhat tam ung
                tamung.Trangthai = true;
                await _hopdongTamungRepository.UpdateAsync(tamung);

                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddSuco(HopdongSucoModel suco)
        {
            //validate
            if (suco == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Quyettoan không để null"
                };

            if (suco.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (suco.LoaiSuco < 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn loại sự cố"
                };

            if (suco.AnhHientruong == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn ảnh hiện trường"
                };

            if (String.IsNullOrEmpty(suco.Ghichu))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa mô tả sự cố"
                };

            //add to storage
            var entity = suco.CloneToModel<HopdongSucoModel, Hopdong_Suco>();
            var res = await _hopdongSucoRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddTailieuDinhkem(HopdongTailieuDinhkemModel tailieu)
        {
            //validate
            if (tailieu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tailieu không để null"
                };

            if (tailieu.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (tailieu.KehoachId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hạng mục"
                };

            if (tailieu.NguoitaoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có người tạo"
                };

            if (tailieu.AnhCongtrinhs == null || tailieu.AnhCongtrinhs.Count <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn ảnh công trình"
                };


            if (tailieu.AnhCongtrinhs.Any(c => c.AnhCongtrinh == null))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Ảnh công trình không hợp lệ"
                };

            //add to storage
            tailieu.Ngaytao = DateTime.Now;
            var entity = tailieu.CloneToModel<HopdongTailieuDinhkemModel, Hopdong_TailieuDinhkem>();
            var res = await _hopdongTailieuDinhkemRepository.InsertAsync(entity);
            if (res)
            {
                tailieu.AnhCongtrinhs.ForEach(c => { c.HopdongId = tailieu.HopdongId; c.TailieuId = entity.Id; });
                var entities = tailieu.AnhCongtrinhs.CloneToListModels<HopdongAnhCongtrinhModel, Hopdong_TailieuDinhkem_AnhCongtrinh>();
                await _hopdongTailieuDinhkemAnhCongtrinhRepository.InsertManyAsync(entities);
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        private async Task<bool> AutoChangeStatusAsync(int hopdongId)
        {
            if (hopdongId > 0)
            {
                var hopdong = await _hopdongRepository.GetByIdAsync(hopdongId);

                if (hopdong != null && hopdong.Trangthai == OrderStatus.Pending)
                {
                    hopdong.Trangthai = OrderStatus.InProgress;
                    await _hopdongRepository.UpdateAsync(hopdong);
                }
            }
            return false;
        }

        public async Task<ActionResult> AddTamung(HopdongTamungModel tamung)
        {
            //validate
            if (tamung == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tamung không để null"
                };

            if (tamung.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (tamung.NguoitamungId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người tạm ứng"
                };

            if (tamung.NguoichoTamungId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người cho tạm ứng"
                };

            if (tamung.Sotien <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền tạm ứng không hợp lệ"
                };

            //check xem co tam ung nao da quyet toan chua
            var exist = await _hopdongTamungRepository.SingleAsync(c => c.Trangthai == false && c.NguoitamungId == tamung.NguoitamungId && c.HopdongId == tamung.HopdongId);
            if (exist != null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Kỹ thuật này đã tạm ứng và chưa quyết toán"
                };

            //add to storage
            tamung.Trangthai = false;
            var entity = tamung.CloneToModel<HopdongTamungModel, Hopdong_Tamung>();
            var res = await _hopdongTamungRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddThanhtoan(HopdongThanhtoanModel thanhtoan)
        {
            //validate
            if (thanhtoan == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Thanhtoan không để null"
                };

            if (thanhtoan.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (thanhtoan.Sotien <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền không hợp lệ"
                };

            if (String.IsNullOrEmpty(thanhtoan.NoidungThanhtoan))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung thanh toán"
                };

            //add to storage
            thanhtoan.Ngaytao = DateTime.Now;
            var entity = thanhtoan.CloneToModel<HopdongThanhtoanModel, Hopdong_Thanhtoan>();
            var res = await _hopdongThanhtoanRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddVattu(HopdongVattuModel vattu)
        {
            //validate
            if (vattu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Vattu không để null"
                };

            if (vattu.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (vattu.NguoigiaoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người giao"
                };

            if (vattu.NguoinhanId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người nhận"
                };

            if (String.IsNullOrEmpty(vattu.Vattu))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa nhập tên vật tư"
                };

            //add to storage
            var entity = vattu.CloneToModel<HopdongVattuModel, Hopdong_Vattu>();
            var res = await _hopdongVattuRepository.InsertAsync(entity);

            if (res)
            {
                await AutoChangeStatusAsync(entity.HopdongId);
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<HopdongBaocaoLoinhuanModel> BaocaoLoinhuanAsync(int hopdongId)
        {
            using (var db = new DomainContext())
            {
                var res = new HopdongBaocaoLoinhuanModel();

                res.Khoanthu = await db.Hopdong_Dichvus.Where(c => c.HopdongId == hopdongId).SumAsync(c => (decimal?)c.Thanhtien);
                res.ChiphiAnUong = await db.Hopdong_Quyettoan_Chiphis.Where(c => c.HopdongId == hopdongId && c.LoaiChiphi == LoaiChiphi.An_uong).SumAsync(c => (decimal?)c.Sotien);
                res.ChiphiDiLai = await db.Hopdong_Quyettoan_Chiphis.Where(c => c.HopdongId == hopdongId && c.LoaiChiphi == LoaiChiphi.Di_lai).SumAsync(c => (decimal?)c.Sotien);
                res.ChiphiKhac = await db.Hopdong_Quyettoan_Chiphis.Where(c => c.HopdongId == hopdongId && c.LoaiChiphi == LoaiChiphi.Khac).SumAsync(c => (decimal?)c.Sotien);
                res.ChiphiNgaycong = await db.Hopdong_Quyettoan_Chiphis.Where(c => c.HopdongId == hopdongId && c.LoaiChiphi == LoaiChiphi.Nhan_cong).SumAsync(c => (decimal?)c.Sotien);
                res.ChiphiNhaNghi = await db.Hopdong_Quyettoan_Chiphis.Where(c => c.HopdongId == hopdongId && c.LoaiChiphi == LoaiChiphi.Nha_nghi).SumAsync(c => (decimal?)c.Sotien);

                return res;
            }
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _hopdongRepository.DeleteManyAsync(c => Ids.Contains(c.HopdongId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<List<HopdongDanhgiaNoiboModel>> FilterDanhgiaNoibo(FilterDanhgiaNoiboModel filter)
        {
            var res = await _hopdongDanhgiaRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Noidung.Contains(filter.Search) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_DanhgiaNoibo, HopdongDanhgiaNoiboModel>();
        }

        public async Task<List<HopdongDukienchiModel>> FilterDukienchi(FilterHopdongBase filter)
        {
            var res = await _hopdongDukienchiRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Lydochi.Contains(filter.Search) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Dukienchi, HopdongDukienchiModel>();
        }

        public async Task<List<HopdongDukienhangcanthietModel>> FilterDukienhangcanthiet(FilterHopdongBase filter)
        {
            var res = await _hopdongDukienhangcanthietRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenMathang.Contains(filter.Search) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Dukienhangcanthiet, HopdongDukienhangcanthietModel>();
        }

        public async Task<List<HopdongDukienthuModel>> FilterDukienthu(FilterHopdongBase filter)
        {
            var res = await _hopdongDukienthuRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Noidungthu.Contains(filter.Search) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Dukienthu, HopdongDukienthuModel>();
        }

        public async Task<List<HopdongGiaoviecModel>> FilterGiaoviec(FilterHopdongBase filter)
        {
            var res = await _hopdongGiaoviecRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Ghichu.Contains(filter.Search) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Giaoviec, HopdongGiaoviecModel>();
        }

        public async Task<List<HopdongKehoachModel>> FilterKehoach(FilterHopdongBase filter)
        {
            var res = await _hopdongKehoachRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) 
            ? c.NoidungThuchien.Contains(filter.Search) : true) 
            && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            var models = res.CloneToListModels<Hopdong_Kehoach, HopdongKehoachModel>(); 
            using (var db = _hopdongKehoachRepository.GetDbContext())
            {
                var users = (from h in res
                            join u in db.Taikhoans
                            on h.NguoitaoId equals u.TaikhoanId
                            select u).ToList();

                models.ForEach(m =>
                {
                    var nguoitao = users.FirstOrDefault(u => u.TaikhoanId == m.NguoitaoId);
                    if (nguoitao != null)
                        m.Nguoitao = nguoitao.Hovaten;
                });
            }


            return models;



        }

        public async Task<List<HopdongNhansuModel>> FilterNhansu(FilterHopdongBase filter)
        {
            var res = await _hopdongNhansuRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Vaitro.Contains(filter.Search) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Nhansu, HopdongNhansuModel>();
        }

        public async Task<List<HopdongNhatkythicongModel>> FilterNhatkythicong(FilterNhatkythicongModel filter)
        {
            var res = await _hopdongNhatkythicongRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) 
            ? c.Noidungbaocao.Contains(filter.Search) : true)
            && (filter.KehoachId != null ? c.KehoachId == filter.KehoachId : true)
            && (filter.LoaiCongviec != 0 ? c.LoaiCongviec == filter.LoaiCongviec : true)
           
            && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Nhatkythicong, HopdongNhatkythicongModel>();
        }

        public async Task<List<HopdongPhanhoiModel>> FilterPhanhoi(FilterPhanhoiModel filter)
        {
            var res = await _hopdongPhanhoiRepository.FetchAsync(c =>
            (!String.IsNullOrEmpty(filter.Search) ? c.Nhanxet.Contains(filter.Search) : true) &&
            c.HopdongId == filter.HopdongId &&
            (filter.KythuatId != null ? c.KythuatId == filter.KythuatId : true), c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Phanhoi, HopdongPhanhoiModel>();
        }

        public async Task<List<HopdongQuyettoanModel>> FilterQuyettoan(FilterQuyettoanModel filter)
        {
            var res = await _hopdongQuyettoanRepository.FetchAsync(c =>
            (!String.IsNullOrEmpty(filter.Search) ? c.Ghichu.Contains(filter.Search) : true) &&
            c.HopdongId == filter.HopdongId &&
            (filter.NguoitamungId != null ? c.NguoiQuyettoanId == filter.NguoitamungId : true)
            , c => c.HopdongId, filter.Paging);

            var models = res.CloneToListModels<Hopdong_Quyettoan, HopdongQuyettoanModel>();

            models.ForEach(c =>
            {
                var chiphis = _hopdongQuyettoanChiphiRepository.Fetch(k => k.QuyettoanId == c.Id);
                c.Chiphis = chiphis.CloneToListModels<Hopdong_Quyettoan_Chiphi, ChiphiQuyettoanModel>();
                var tamung = _hopdongTamungRepository.GetById(c.TamungId);

                c.TongTamung = tamung != null ? tamung.Sotien : 0;
                c.TongChiPhi = chiphis.Sum(k => k.Sotien);
                c.Conlai = c.TongTamung - c.TongChiPhi;
            });

            return models;
        }

        public async Task<List<HopdongSucoModel>> FilterSuco(FilterSucoModel filter)
        {
            var res = await _hopdongSucoRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? (c.Ghichu.Contains(filter.Search) || c.PhuonganKhacphuc.Contains(filter.Search)) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Suco, HopdongSucoModel>();
        }

        public async Task<List<HopdongTailieuDinhkemModel>> FilterTailieuDinhkem(FilterTailieuDinhkemModel filter)
        {
            var res = await _hopdongTailieuDinhkemRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? true : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            var models = res.CloneToListModels<Hopdong_TailieuDinhkem, HopdongTailieuDinhkemModel>();

            models.ForEach(c =>
            {
                var anhcongtrinhs = _hopdongTailieuDinhkemAnhCongtrinhRepository.Fetch(k => k.TailieuId == c.Id);

                c.AnhCongtrinhs = anhcongtrinhs.CloneToListModels<Hopdong_TailieuDinhkem_AnhCongtrinh, HopdongAnhCongtrinhModel>();
            });

            return models;
        }

        public async Task<List<HopdongTamungModel>> FilterTamung(FilterTamungModel filter)
        {
            var res = await _hopdongTamungRepository.FetchAsync(c =>
            (!String.IsNullOrEmpty(filter.Search) ? c.Ghichu.Contains(filter.Search) : true) &&
            c.HopdongId == filter.HopdongId &&
            (filter.NguoitamungId != null ? c.NguoitamungId == filter.NguoitamungId : true)
            , c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Tamung, HopdongTamungModel>();
        }

        public async Task<List<HopdongThanhtoanModel>> FilterThanhtoan(FilterHopdongBase filter)
        {
            var res = await _hopdongThanhtoanRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.NoidungThanhtoan.Contains(filter.Search) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Thanhtoan, HopdongThanhtoanModel>();
        }

        public async Task<List<HopdongVattuModel>> FilterVattu(FilterHopdongBase filter)
        {
            var res = await _hopdongVattuRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Vattu.Contains(filter.Search) : true) && c.HopdongId == filter.HopdongId, c => c.HopdongId, filter.Paging);

            return res.CloneToListModels<Hopdong_Vattu, HopdongVattuModel>();
        }

        public async Task<HopdongModel> GetById(int hopdongId)
        {
            var entity = await _hopdongRepository.GetByIdAsync(hopdongId);
            return entity.CloneToModel<Hopdong, HopdongModel>();
        }

        public async Task<HopdongDetailModel> GetHopdongDetail(int id)
        {
            var res = await GetById(id);
            if (res == null) return null;

            var result = new HopdongDetailModel();

            result.Hopdong = res;

            var sanphams = await _hopdongSanphamRepository.FetchAsync(c => c.HopdongId == id);

            var dichvus = await _hopdongDichvuRepository.FetchAsync(c => c.HopdongId == id);

            result.Sanphams = sanphams.CloneToListModels<Hopdong_Sanpham, SanphamHopdongModel>();

            result.Sanphams.ForEach(c =>
            {
                var sp = _sanphamRepository.GetById(c.SanphamId);
                c.TenSanpham = sp != null ? sp.TenSanpham : "---";
                c.AnhSanpham = sp != null ? sp.AnhSanpham : "";
            });

            result.Dichvus = dichvus.CloneToListModels<Hopdong_Dichvu, DichvuHopdongModel>();

            return result;
        }

        public async Task<List<HopdongModel>> GetHopdongs(FilterModel filter)
        {
            //var res = await _hopdongRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Ghichu.Contains(filter.Search) : true), c => c.HopdongId, filter.Paging);

            //return res.CloneToListModels<Hopdong, HopdongModel>();

            if (filter == null || filter.Paging == null)
                return null;

            using (var db = new DomainContext())
            {
                var q = db.Hopdongs.AsQueryable();

                if (!String.IsNullOrEmpty(filter.Search))
                {
                    q = q.Where(c => c.Ghichu.Contains(filter.Search) || c.SoHD.Contains(filter.Search));
                }

                if (filter.Status != null)
                {
                    q = q.Where(c => c.Trangthai == filter.Status);
                }

                if (filter.CustomerId != null)
                {
                    q = q.Where(c => c.KhachhangId == filter.CustomerId);
                }

                if (filter.CreatorId != null)
                {
                    q = q.Where(c => c.Nguoitao == filter.CreatorId);
                }

                if (filter.LoaiHopdong != null)
                {
                    q = q.Where(c => c.Loai == filter.LoaiHopdong);
                }

                if (filter.CityId != null)
                {
                    q = (from u in q
                         join d in db.Khachhangs on u.KhachhangId equals d.KhachhangId
                         where d.ThanhphoId == filter.CityId
                         select u).Distinct();
                }

                if (filter.DistrictId != null)
                {
                    q = (from u in q
                         join d in db.Khachhangs on u.KhachhangId equals d.KhachhangId
                         where d.QuanhuyenId == filter.DistrictId
                         select u).Distinct();
                }

                if (filter.NhucaugiaId != null)
                {
                    q = (from u in q
                         join d in db.Khachhangs on u.KhachhangId equals d.KhachhangId
                         join n in db.Khachhang_Nhucaugias on d.KhachhangId equals n.KhachhangId
                         where n.NhucaugiaId == filter.NhucaugiaId
                         select u).Distinct();
                }

                if (filter.LoaiKhachhangId != null)
                {
                    q = (from u in q
                         join d in db.Khachhangs on u.KhachhangId equals d.KhachhangId
                         where d.LoaiKHId == filter.LoaiKhachhangId
                         select u).Distinct();
                }

                if (filter.NguonkhachId != null)
                {
                    q = (from u in q
                         join d in db.Khachhangs on u.KhachhangId equals d.KhachhangId
                         where d.NguonkhachId == filter.NguonkhachId
                         select u).Distinct();
                }

                if (filter.NhomsanphamId != null)
                {
                    q = (from u in q
                         join d in db.Hopdong_Sanphams on u.HopdongId equals d.HopdongId
                         join s in db.Sanphams on d.SanphamId equals s.SanphamId
                         where s.NhomsanphamId == filter.NhomsanphamId
                         select u).Distinct();
                }

                if (filter.SanphamQuantamId != null)
                {
                    q = (from u in q
                         join d in db.Hopdong_Sanphams on u.HopdongId equals d.HopdongId
                         join s in db.Sanphams on d.SanphamId equals s.SanphamId
                         where s.SanphamId == filter.SanphamQuantamId
                         select u).Distinct();
                }

                if (filter.Start != null && filter.End != null)
                {
                    q = q.Where(c =>
                    DbFunctions.DiffDays(filter.Start, c.Ngaytao) >= 0 &&
                    DbFunctions.DiffDays(c.Ngaytao, filter.End) >= 0);
                }

                filter.Paging.RowsCount = await q.CountAsync();

                var result = await q.OrderByDescending(c => c.Ngaytao).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToListAsync();

                var models = result.CloneToListModels<Hopdong, HopdongModel>();

                return models;
            }//using
        }

        public async Task<HopdongKehoachModel> GetKehoachByIdAsync(int id)
        {
            var res = await _hopdongKehoachRepository.GetByIdAsync(id);

            return res.CloneToModel<Hopdong_Kehoach, HopdongKehoachModel>();
        }

        public async Task<List<SanphamHopdongModel>> GetSanphamByHopdongIdAsync(int hopdongId)
        {
            using (var db = new DomainContext())
            {
                var q = from h in db.Hopdong_Sanphams
                        join s in db.Sanphams on h.SanphamId equals s.SanphamId
                        where h.HopdongId == hopdongId
                        select new SanphamHopdongModel()
                        {
                            Id = h.Id,
                            HopdongId = h.HopdongId,
                            SanphamId = h.SanphamId,
                            MaSP = h.MaSP,
                            Soluong = h.Soluong,
                            Thanhtien = h.Thanhtien,
                            Gia = h.Gia,
                            TenSanpham = s.TenSanpham,
                            AnhSanpham = s.AnhSanpham
                        };

                return await q.ToListAsync();
            }
        }

        public async Task<HopdongTamungModel> GetTamungByNguoiQuyettoanIdAsync(int hopdongId, int nguoiQuyettoanId)
        {
            var res = await _hopdongTamungRepository.SingleAsync(c => c.HopdongId == hopdongId && c.NguoitamungId == nguoiQuyettoanId && c.Trangthai == false);

            return res.CloneToModel<Hopdong_Tamung, HopdongTamungModel>();
        }

        public async Task<List<HopdongTamungModel>> GetTamungChamquyettoansAsync(int hopdongId)
        {
            var res = await _hopdongTamungRepository.FetchAsync(c => c.HopdongId == hopdongId && c.NgayDKQuyettoan < DateTime.Now, c => c.HopdongId, new PagingInfo(20, 1));

            return res.CloneToListModels<Hopdong_Tamung, HopdongTamungModel>();
        }

        public async Task<ActionResult> Move2TechAsync(int id)
        {
            var contract = await _hopdongRepository.GetByIdAsync(id);
            if (contract != null)
            {
                contract.Trangthai = OrderStatus.Pending;
                var res = await _hopdongRepository.UpdateAsync(contract);
                return new ActionResult() { Result = res };
            }
            return new ActionResult() { Result = false, Error = "Hợp đồng không tồn tại" };
        }

        public async Task<ActionResult> RemoveDanhgiaNoibo(int[] ids)
        {
            var res = await _hopdongDanhgiaRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveDukienchi(int[] ids)
        {
            var res = await _hopdongDukienchiRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveDukienhangcanthiet(int[] ids)
        {
            var res = await _hopdongDukienhangcanthietRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveDukienthu(int[] ids)
        {
            var res = await _hopdongDukienthuRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveGiaoviec(int[] ids)
        {
            var res = await _hopdongGiaoviecRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveKehoach(int[] ids)
        {
            var res = await _hopdongKehoachRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveNhansu(int[] ids)
        {
            var res = await _hopdongNhansuRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveNhatkythicong(int[] ids)
        {
            var res = await _hopdongNhatkythicongRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemovePhanhoi(int[] ids)
        {
            var res = await _hopdongPhanhoiRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveQuyettoan(int[] ids)
        {
            var res = await _hopdongQuyettoanRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveSuco(int[] ids)
        {
            var res = await _hopdongSucoRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveTailieuDinhkem(int[] ids)
        {
            var res = await _hopdongTailieuDinhkemRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveTamung(int[] ids)
        {
            var res = await _hopdongTamungRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveThanhtoan(int[] ids)
        {
            var res = await _hopdongThanhtoanRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> RemoveVattu(int[] ids)
        {
            var res = await _hopdongVattuRepository.DeleteManyAsync(c => ids.Contains(c.Id));

            return new ActionResult() { Result = res > 0 ? true : false };
        }

        public async Task<ActionResult> SaveHopdong(HopdongDetailModel hopdong)
        {
            if (hopdong == null || hopdong.Hopdong == null || String.IsNullOrEmpty(hopdong.Hopdong.SoHD))
            {
                return new ActionResult() { Result = false, Error = "Chưa nhập hợp đồng, số HĐ" };
            }

            if (hopdong.Hopdong.KhachhangId <= 0)
            {
                return new ActionResult() { Result = false, Error = "Chưa chọn khách hàng làm hợp đồng" };
            }

            if (hopdong.Sanphams == null || hopdong.Sanphams.Count == 0)
            {
                return new ActionResult() { Result = false, Error = "Chưa chọn sản phẩm" };
            }

            if (hopdong.Sanphams.Any(c => c.Gia <= 0))
            {
                return new ActionResult() { Result = false, Error = "Giá sản phẩm phải lớn hơn 0" };
            }

            if (hopdong.Sanphams.Any(c => c.Soluong <= 0))
            {
                return new ActionResult() { Result = false, Error = "Số lượng sản phẩm phải lớn hơn 0" };
            }

            if (hopdong.Dichvus != null && hopdong.Dichvus.Count >= 0 && hopdong.Dichvus.Any(c => String.IsNullOrEmpty(c.Dichvu)))
            {
                return new ActionResult() { Result = false, Error = "Tên dịch vụ là bắt buộc" };
            }

            if (hopdong.Dichvus != null && hopdong.Dichvus.Count >= 0 && hopdong.Dichvus.Any(c => c.Gia <= 0))
            {
                return new ActionResult() { Result = false, Error = "Giá dịch vụ phải lớn hơn 0" };
            }

            if (hopdong.Dichvus != null && hopdong.Dichvus.Count >= 0 && hopdong.Dichvus.Any(c => c.Soluong <= 0))
            {
                return new ActionResult() { Result = false, Error = "Số lượng dịch vụ phải lớn hơn 0" };
            }

            if (hopdong.Dichvus != null && hopdong.Dichvus.Count > 0)
            {
                hopdong.Hopdong.Loai = LoaiHopdong.HopdongDichvu;
            }
            else
            {
                hopdong.Hopdong.Loai = LoaiHopdong.Donhang;
            }

            if (hopdong.Hopdong.HopdongId <= 0)//them moi
            {
                hopdong.Hopdong.Trangthai = OrderStatus.New;

                //validate nguoi tao
                if (hopdong.Hopdong.Nguoitao <= 0)
                {
                    return new ActionResult() { Result = false, Error = "Chưa có người tạo" };
                }
                hopdong.Hopdong.Ngaytao = DateTime.Now;

                var entity = hopdong.Hopdong.CloneToModel<HopdongModel, Hopdong>();
                var res = await _hopdongRepository.InsertAsync(entity);
                if (res)
                {
                    var sanphams = hopdong.Sanphams.CloneToListModels<SanphamHopdongModel, Hopdong_Sanpham>();

                    sanphams.ForEach(c => c.HopdongId = entity.HopdongId);

                    await _hopdongSanphamRepository.InsertManyAsync(sanphams);

                    if (hopdong.Dichvus != null && hopdong.Dichvus.Count > 0)
                    {
                        var dichvus = hopdong.Dichvus.CloneToListModels<DichvuHopdongModel, Hopdong_Dichvu>();

                        dichvus.ForEach(c => c.HopdongId = entity.HopdongId);

                        await _hopdongDichvuRepository.InsertManyAsync(dichvus);
                    }

                    //change customer status
                    await (new KhachhangService()).Change2Conversion(entity.KhachhangId);
                }
            }
            else//cap nhat
            {
                //validate nguoi cap nhat
                if (hopdong.Hopdong.Nguoicapnhat == null || hopdong.Hopdong.Nguoicapnhat <= 0)
                {
                    return new ActionResult() { Result = false, Error = "Chưa có người cập nhật" };
                }
                hopdong.Hopdong.Ngaycapnhat = DateTime.Now;

                //keep old data
                var exist = await _hopdongRepository.GetByIdAsync(hopdong.Hopdong.HopdongId);
                hopdong.Hopdong.Ngaytao = exist.Ngaytao;
                hopdong.Hopdong.Nguoitao = exist.Nguoitao;

                var entity = hopdong.Hopdong.CloneToModel<HopdongModel, Hopdong>();
                var res = await _hopdongRepository.UpdateAsync(entity);
                if (res)
                {
                    var sanphams = hopdong.Sanphams.CloneToListModels<SanphamHopdongModel, Hopdong_Sanpham>();

                    sanphams.ForEach(c => c.HopdongId = entity.HopdongId);

                    //add new data
                    await _hopdongSanphamRepository.InsertManyAsync(sanphams.Where(c => c.Id == 0));

                    //remove old data
                    var oldIds = sanphams.Where(c => c.Id > 0).Select(c => c.Id);
                    var removes = await _hopdongSanphamRepository.DeleteManyAsync(c => c.HopdongId == hopdong.Hopdong.HopdongId && !oldIds.Contains(c.Id));

                    //Update old data
                    res = await _hopdongSanphamRepository.UpdateManyAsync(sanphams.Where(c => c.Id > 0));

                    if (hopdong.Dichvus != null && hopdong.Dichvus.Count > 0)
                    {
                        var dichvus = hopdong.Dichvus.CloneToListModels<DichvuHopdongModel, Hopdong_Dichvu>();

                        dichvus.ForEach(c => c.HopdongId = entity.HopdongId);

                        //add new data
                        await _hopdongDichvuRepository.InsertManyAsync(dichvus.Where(c => c.Id == 0));

                        //remove old data
                        oldIds = dichvus.Where(c => c.Id > 0).Select(c => c.Id);
                        removes = await _hopdongDichvuRepository.DeleteManyAsync(c => c.HopdongId == hopdong.Hopdong.HopdongId && !oldIds.Contains(c.Id));

                        //Update old data
                        res = await _hopdongDichvuRepository.UpdateManyAsync(dichvus.Where(c => c.Id > 0));
                    }

                    //change customer status
                    await (new KhachhangService()).Change2Conversion(entity.KhachhangId);
                }
            }//if

            return new ActionResult() { Result = true };
        }

        public async Task<List<SanphamHopdongModel>> SearchSanphamDatmuaAsync(string maSanpham)
        {
            using (var db = new DomainContext())
            {
                var q = from h in db.Hopdong_Sanphams
                        join s in db.Sanphams on h.SanphamId equals s.SanphamId
                        where h.MaSP.Contains(maSanpham)
                        select new SanphamHopdongModel()
                        {
                            Id = h.Id,
                            HopdongId = h.HopdongId,
                            SanphamId = h.SanphamId,
                            MaSP = h.MaSP,
                            Soluong = h.Soluong,
                            Thanhtien = h.Thanhtien,
                            Gia = h.Gia,
                            TenSanpham = s.TenSanpham
                        };

                return await q.Take(10).ToListAsync();
            }
        }

        public async Task<ActionResult> UpdateDanhgiaNoibo(HopdongDanhgiaNoiboModel danhgia)
        {
            //validate
            if (danhgia == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "danhgia không để null"
                };

            if (danhgia.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (danhgia.KythuatId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn kỹ thuật"
                };

            if (danhgia.Tiendo <= 0 || danhgia.Chatluong <= 0 || danhgia.PhanhoiKH <= 0 || danhgia.Chiphi <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn điểm đánh giá"
                };

            if (String.IsNullOrEmpty(danhgia.Noidung))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung ý kiến đánh giá"
                };

            var exist = await _hopdongDanhgiaRepository.GetByIdAsync(danhgia.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "danhgia không tồn tại"
                };

            //update to storage
            danhgia.Ngaydanhgia = DateTime.Now;
            var entity = danhgia.CloneToModel<HopdongDanhgiaNoiboModel, Hopdong_DanhgiaNoibo>();
            var res = await _hopdongDanhgiaRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateDukienchi(HopdongDukienchiModel dukienchi)
        {
            //validate
            if (dukienchi == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "dukienchi không để null"
                };

            if (dukienchi.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (dukienchi.Sotien <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền phải lớn hơn 0"
                };

            if (String.IsNullOrEmpty(dukienchi.Lydochi))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có lý do chi"
                };

            var exist = await _hopdongDukienchiRepository.GetByIdAsync(dukienchi.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "dukienchi không tồn tại"
                };

            //update to storage
            dukienchi.Ngaycapnhat = DateTime.Now;
            var entity = dukienchi.CloneToModel<HopdongDukienchiModel, Hopdong_Dukienchi>();
            var res = await _hopdongDukienchiRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateDukienhangcanthiet(HopdongDukienhangcanthietModel dukienhangcanthiet)
        {
            //validate
            if (dukienhangcanthiet == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "dukienhangcanthiet không để null"
                };

            if (dukienhangcanthiet.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (String.IsNullOrEmpty(dukienhangcanthiet.Chungloai))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có chủng loại"
                };

            if (String.IsNullOrEmpty(dukienhangcanthiet.TenMathang))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tên mặt hàng là bắt buộc"
                };

            if (dukienhangcanthiet.Tylechot <= 0 || dukienhangcanthiet.Tylechot > 100)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tỷ lệ chốt trong khoảng 1 - 100%"
                };


            var exist = await _hopdongDukienhangcanthietRepository.GetByIdAsync(dukienhangcanthiet.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "dukienhangcanthiet không tồn tại"
                };

            //update to storage
            dukienhangcanthiet.Ngaycapnhat = DateTime.Now;
            var entity = dukienhangcanthiet.CloneToModel<HopdongDukienhangcanthietModel, Hopdong_Dukienhangcanthiet>();
            var res = await _hopdongDukienhangcanthietRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateDukienthu(HopdongDukienthuModel dukienthu)
        {
            //validate
            if (dukienthu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "dukienthu không để null"
                };

            if (dukienthu.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (dukienthu.Sotien <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền phải lớn hơn 0"
                };

            if (String.IsNullOrEmpty(dukienthu.Noidungthu))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung thu"
                };

            var exist = await _hopdongDukienthuRepository.GetByIdAsync(dukienthu.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "dukienthu không tồn tại"
                };

            //update to storage
            dukienthu.Ngaycapnhat = DateTime.Now;
            var entity = dukienthu.CloneToModel<HopdongDukienthuModel, Hopdong_Dukienthu>();
            var res = await _hopdongDukienthuRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateGiaoviec(HopdongGiaoviecModel giaoviec)
        {
            //validate
            if (giaoviec == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "giaoviec không để null"
                };

            if (giaoviec.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (giaoviec.KehoachId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hạng mục kế hoạch"
                };

            if (giaoviec.NguoithuchienId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người thực hiện"
                };

            if (String.IsNullOrEmpty(giaoviec.NoidungCongviec))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung công việc"
                };

            var exist = await _hopdongGiaoviecRepository.GetByIdAsync(giaoviec.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "giaoviec không tồn tại"
                };

            //update to storage
            var entity = giaoviec.CloneToModel<HopdongGiaoviecModel, Hopdong_Giaoviec>();
            var res = await _hopdongGiaoviecRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateKehoach(HopdongKehoachModel kehoach)
        {
            //validate
            if (kehoach == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "kehoach không để null"
                };

            if (kehoach.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (String.IsNullOrEmpty(kehoach.NoidungThuchien))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung thực hiện"
                };

            var exist = await _hopdongKehoachRepository.GetByIdAsync(kehoach.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "kehoach không tồn tại"
                };

            //update to storage
            var entity = kehoach.CloneToModel<HopdongKehoachModel, Hopdong_Kehoach>();
            var res = await _hopdongKehoachRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateNhansu(HopdongNhansuModel nhansu)
        {
            //validate
            if (nhansu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhansu không để null"
                };

            if (nhansu.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (nhansu.UserId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn kỹ thuật tham gia"
                };

            if (String.IsNullOrEmpty(nhansu.Vaitro))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Vai trò là bắt buộc"
                };

            var exist = await _hopdongNhansuRepository.GetByIdAsync(nhansu.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhansu không tồn tại"
                };

            //update to storage
            var entity = nhansu.CloneToModel<HopdongNhansuModel, Hopdong_Nhansu>();
            var res = await _hopdongNhansuRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateNhatkythicong(HopdongNhatkythicongModel nhatkythicong)
        {
            //validate
            if (nhatkythicong == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nhatkythicong không để null"
                };

            if (nhatkythicong.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (nhatkythicong.KehoachId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hạng mục kế hoạch"
                };

            if (nhatkythicong.NguoibaocaoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn kỹ thuật báo cáo"
                };

            if (nhatkythicong.AnhDinhkem == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn ảnh đính kèm"
                };

            if (String.IsNullOrEmpty(nhatkythicong.Noidungbaocao))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Vai trò là bắt buộc"
                };

            var exist = await _hopdongNhatkythicongRepository.GetByIdAsync(nhatkythicong.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nhatkythicong không tồn tại"
                };

            //update to storage
            var entity = nhatkythicong.CloneToModel<HopdongNhatkythicongModel, Hopdong_Nhatkythicong>();
            entity.ThoigianThuchien = entity.Ketthuc.Subtract(entity.Batdau).Hours;
            var res = await _hopdongNhatkythicongRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdatePhanhoi(HopdongPhanhoiModel phanhoi)
        {
            //validate
            if (phanhoi == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "phanhoi không để null"
                };

            if (phanhoi.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (phanhoi.KythuatId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn kỹ thuật báo cáo"
                };

            if (String.IsNullOrEmpty(phanhoi.Nhanxet))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhận xét là bắt buộc"
                };

            var exist = await _hopdongPhanhoiRepository.GetByIdAsync(phanhoi.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "phanhoi không tồn tại"
                };

            //update to storage
            var entity = phanhoi.CloneToModel<HopdongPhanhoiModel, Hopdong_Phanhoi>();
            var res = await _hopdongPhanhoiRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateQuyettoan(HopdongQuyettoanModel quyettoan)
        {
            //validate
            if (quyettoan == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "quyettoan không để null"
                };

            if (quyettoan.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (quyettoan.NguoiQuyettoanId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người quyết toán"
                };

            if (quyettoan.NguoiDuyetQuyettoanId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người duyệt quyết toán"
                };

            if (quyettoan.Chiphis == null || quyettoan.Chiphis.Count <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có chi phí quyết toán"
                };

            if (quyettoan.Chiphis.Any(c => c.Sotien <= 0))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền chi phí không hợp lệ"
                };

            var exist = await _hopdongQuyettoanRepository.GetByIdAsync(quyettoan.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "quyettoan không tồn tại"
                };

            //update to storage
            quyettoan.TamungId = exist.TamungId;
            var entity = quyettoan.CloneToModel<HopdongQuyettoanModel, Hopdong_Quyettoan>();
            var res = await _hopdongQuyettoanRepository.UpdateAsync(entity);
            if (res)
            {
                quyettoan.Chiphis.ForEach(c => { c.HopdongId = quyettoan.HopdongId; c.QuyettoanId = entity.Id; });
                var chiphis = quyettoan.Chiphis.CloneToListModels<ChiphiQuyettoanModel, Hopdong_Quyettoan_Chiphi>();

                //add new data
                await _hopdongQuyettoanChiphiRepository.InsertManyAsync(chiphis.Where(c => c.Id == 0));

                //remove old data
                var oldIds = chiphis.Where(c => c.Id > 0).Select(c => c.Id);
                var removes = await _hopdongQuyettoanChiphiRepository.DeleteManyAsync(c => c.HopdongId == quyettoan.HopdongId && !oldIds.Contains(c.Id));

                //Update old data
                res = await _hopdongQuyettoanChiphiRepository.UpdateManyAsync(chiphis.Where(c => c.Id > 0));
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateSuco(HopdongSucoModel suco)
        {
            //validate
            if (suco == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "suco không để null"
                };

            if (suco.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (suco.LoaiSuco < 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn loại sự cố"
                };

            if (suco.AnhHientruong == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn ảnh hiện trường"
                };

            if (String.IsNullOrEmpty(suco.Ghichu))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa mô tả sự cố"
                };

            var exist = await _hopdongSucoRepository.GetByIdAsync(suco.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "suco không tồn tại"
                };

            //update to storage
            var entity = suco.CloneToModel<HopdongSucoModel, Hopdong_Suco>();
            var res = await _hopdongSucoRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateTailieuDinhkem(HopdongTailieuDinhkemModel tailieu)
        {
            //validate
            if (tailieu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "tailieu không để null"
                };

            if (tailieu.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (tailieu.KehoachId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hạng mục"
                };

            if (tailieu.NguoitaoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có người tạo"
                };

            //if (tailieu.AnhCongtrinhs == null || tailieu.AnhCongtrinhs.Count <= 0)
            //    return new ActionResult()
            //    {
            //        Result = false,
            //        Error = "Chưa chọn ảnh công trình"
            //    };

            //if (tailieu.AnhCongtrinhs.Any(c => c.AnhCongtrinh == null))
            //    return new ActionResult()
            //    {
            //        Result = false,
            //        Error = "Ảnh công trình không hợp lệ"
            //    };

            var exist = await _hopdongTailieuDinhkemRepository.GetByIdAsync(tailieu.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "tailieu không tồn tại"
                };

            //update to storage
            var entity = tailieu.CloneToModel<HopdongTailieuDinhkemModel, Hopdong_TailieuDinhkem>();
            var res = await _hopdongTailieuDinhkemRepository.UpdateAsync(entity);
            if (res)
            {
                tailieu.AnhCongtrinhs.ForEach(c => { c.HopdongId = tailieu.HopdongId; c.TailieuId = entity.Id; });
                var chiphis = tailieu.AnhCongtrinhs.CloneToListModels<HopdongAnhCongtrinhModel, Hopdong_TailieuDinhkem_AnhCongtrinh>();

                //add new data
                await _hopdongTailieuDinhkemAnhCongtrinhRepository.InsertManyAsync(chiphis.Where(c => c.Id == 0));

                //remove old data
                var oldIds = chiphis.Where(c => c.Id > 0).Select(c => c.Id);
                var removes = await _hopdongTailieuDinhkemAnhCongtrinhRepository.DeleteManyAsync(c => c.HopdongId == tailieu.HopdongId && !oldIds.Contains(c.Id));

                //Update old data
                res = await _hopdongTailieuDinhkemAnhCongtrinhRepository.UpdateManyAsync(chiphis.Where(c => c.Id > 0));
            }

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateTamung(HopdongTamungModel tamung)
        {
            //validate
            if (tamung == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "tamung không để null"
                };

            if (tamung.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (tamung.NguoitamungId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người tạm ứng"
                };

            if (tamung.NguoichoTamungId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người cho tạm ứng"
                };

            if (tamung.Sotien <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền tạm ứng không hợp lệ"
                };

            var exist = await _hopdongTamungRepository.GetByIdAsync(tamung.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "tamung không tồn tại"
                };

            //update to storage
            tamung.Trangthai = exist.Trangthai;
            var entity = tamung.CloneToModel<HopdongTamungModel, Hopdong_Tamung>();
            var res = await _hopdongTamungRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateThanhtoan(HopdongThanhtoanModel thanhtoan)
        {
            //validate
            if (thanhtoan == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "thanhtoan không để null"
                };

            if (thanhtoan.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (thanhtoan.Sotien <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Số tiền không hợp lệ"
                };

            if (String.IsNullOrEmpty(thanhtoan.NoidungThanhtoan))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung thanh toán"
                };

            var exist = await _hopdongThanhtoanRepository.GetByIdAsync(thanhtoan.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "thanhtoan không tồn tại"
                };

            //update to storage
            thanhtoan.Ngaycapnhat = DateTime.Now;
            thanhtoan.Ngaytao = exist.Ngaytao;
            var entity = thanhtoan.CloneToModel<HopdongThanhtoanModel, Hopdong_Thanhtoan>();
            var res = await _hopdongThanhtoanRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateVattu(HopdongVattuModel vattu)
        {
            //validate
            if (vattu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "vattu không để null"
                };

            if (vattu.HopdongId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn hợp đồng"
                };

            if (vattu.NguoigiaoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người giao"
                };

            if (vattu.NguoinhanId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn người nhận"
                };

            if (String.IsNullOrEmpty(vattu.Vattu))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa nhập tên vật tư"
                };

            var exist = await _hopdongVattuRepository.GetByIdAsync(vattu.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "vattu không tồn tại"
                };

            //update to storage
            var entity = vattu.CloneToModel<HopdongVattuModel, Hopdong_Vattu>();
            var res = await _hopdongVattuRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<TiendoHopdongModel> Xemtiendo(TiendoFilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from n in db.Hopdong_Nhatkythicongs
                        where n.HopdongId == filter.HopdongId
                        select n;

                if (filter.KehoachId != null)
                {
                    q = q.Where(c => c.KehoachId == filter.KehoachId);
                }

                if (filter.Start != null && filter.End != null)
                {
                    q = q.Where(c =>
                    DbFunctions.DiffDays(filter.Start, c.Ngaybaocao) >= 0 &&
                    DbFunctions.DiffDays(c.Ngaybaocao, filter.End) >= 0);
                }

                filter.Paging.RowsCount = await q.CountAsync();

                var result = await q.OrderByDescending(c => c.Ngaybaocao).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToListAsync();

                var models = result.CloneToListModels<Hopdong_Nhatkythicong, HopdongNhatkythicongModel>();

                var tiendos = models.Select(c => c.KehoachId).Distinct().ToList();

                var tiendo = new TiendoHopdongModel();

                tiendo.Giaidoans = new List<GiaidoanModel>();

                tiendos.ForEach(c =>
                {
                    var kh = db.Hopdong_Kehoachs.Find(c);
                    var giaidoan = new GiaidoanModel();
                    giaidoan.KehoachId = c;
                    giaidoan.Kehoach = kh != null ? kh.NoidungThuchien : "---";
                    giaidoan.NhatkyThicongs = models.Where(k => k.KehoachId == c).ToList();
                    tiendo.Giaidoans.Add(giaidoan);
                });

                return tiendo;
            }//using
        }

        public async Task<ActionResult> HoanthanhHopdongAsync(int hopdongId)
        {
            var hopdong = await _hopdongRepository.GetByIdAsync(hopdongId);
            if (hopdong != null)
            {
                hopdong.Trangthai = OrderStatus.Completed;
                var res = await _hopdongRepository.UpdateAsync(hopdong);
                return new ActionResult() { Result = res };
            }
            return new ActionResult() { Result = false };
        }
    }
}

