﻿using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class FilterSucoModel : FilterHopdongBase
    {
        public int? KythuatId { set; get; }

        public int? Loailoi { set; get; }
    }
}