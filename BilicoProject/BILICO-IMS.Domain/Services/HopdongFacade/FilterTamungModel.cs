﻿using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class FilterTamungModel : FilterHopdongBase
    {
        public int? NguoitamungId { set; get; }
    }
}