﻿using System.Collections.Generic;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongDetailModel
    {
        public HopdongModel Hopdong { set; get; }

        public List<SanphamHopdongModel> Sanphams { set; get; }

        public List<DichvuHopdongModel> Dichvus { set; get; }
    }
}