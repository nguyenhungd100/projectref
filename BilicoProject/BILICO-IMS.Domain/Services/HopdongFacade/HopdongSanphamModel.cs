﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongSanphamModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int SanphamId { set; get; }
        public string MaSP { set; get; }
        public int Soluong { set; get; }
        public decimal Thanhtien { set; get; }
        public decimal Gia { set; get; }

        //not map
        public string TenSanpham { set; get; }
    }
}