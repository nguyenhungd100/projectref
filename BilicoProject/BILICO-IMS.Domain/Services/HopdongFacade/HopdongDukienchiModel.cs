﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongDukienchiModel
    {
        public int Id { get; set; }
        public int HopdongId { get; set; }
        public string Lydochi { get; set; }
        public decimal Sotien { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
    }
}