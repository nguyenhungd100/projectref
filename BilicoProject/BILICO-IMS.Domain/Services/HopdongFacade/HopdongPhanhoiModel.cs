﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongPhanhoiModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public string Nhanxet { set; get; }
        public DateTime Ngaythicong { set; get; }
        public bool Ketqua { set; get; }
        public int KythuatId { set; get; }
    }
}