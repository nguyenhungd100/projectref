﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongGiaoviecModel
    {
        public int Id { set; get; }
        public int KehoachId { set; get; }
        public int HopdongId { set; get; }
        public string NoidungCongviec { set; get; }
        public DateTime Batdau { set; get; }
        public DateTime Ketthuc { set; get; }
        public int NguoithuchienId { set; get; }
        public string Ghichu { set; get; }
        public byte[] Dinhkem { set; get; }
    }
}