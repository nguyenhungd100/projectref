﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class ChiphiQuyettoanModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int QuyettoanId { set; get; }
        public LoaiChiphi LoaiChiphi { set; get; }
        public decimal Sotien { set; get; }
    }
}