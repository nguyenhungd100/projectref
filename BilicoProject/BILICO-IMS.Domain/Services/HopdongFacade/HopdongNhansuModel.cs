﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongNhansuModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int UserId { set; get; }
        public string Vaitro { set; get; }
        public decimal? ChiphiNgaycong { set; get; }
    }
}