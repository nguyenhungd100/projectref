﻿using System;
using System.Collections.Generic;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongTailieuDinhkemModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int KehoachId { set; get; }
        public DateTime Ngaytao { set; get; }
        public int NguoitaoId { set; get; }

        //not map
        public string FileKey { set; get; }
        public List<HopdongAnhCongtrinhModel> AnhCongtrinhs { set; get; }
    }
}