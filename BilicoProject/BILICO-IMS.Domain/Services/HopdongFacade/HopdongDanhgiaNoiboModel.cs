﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongDanhgiaNoiboModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int KythuatId { set; get; }
        public int Tiendo { set; get; }
        public int Chatluong { set; get; }
        public int Chiphi { set; get; }
        public int PhanhoiKH { set; get; }
        public string Noidung { set; get; }
        public DateTime Ngaydanhgia { set; get; }
    }
}