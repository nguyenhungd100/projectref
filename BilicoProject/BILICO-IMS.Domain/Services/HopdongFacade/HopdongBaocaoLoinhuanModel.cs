﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongBaocaoLoinhuanModel
    {
        public decimal? Khoanthu { set; get; }

        public decimal? ChiphiNgaycong { set; get; }

        public decimal? ChiphiAnUong { set; get; }

        public decimal? ChiphiNhaNghi { set; get; }

        public decimal? ChiphiDiLai { set; get; }

        public decimal? ChiphiKhac { set; get; }
    }
}