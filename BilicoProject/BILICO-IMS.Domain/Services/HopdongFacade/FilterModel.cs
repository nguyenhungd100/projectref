
using BILICO.IMS.Framework.Data;
using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class FilterModel
    {
        public string Search { set; get; }

        public int? CustomerId { set; get; }

        public OrderStatus? Status { set; get; }

        public int? CreatorId { set; get; }

        public int? CityId { set; get; }

        public int? DistrictId { set; get; }

        public int? NhucaugiaId { set; get; }

        public int? LoaiKhachhangId { set; get; }

        public LoaiHopdong? LoaiHopdong { set; get; }

        public int? NguonkhachId { set; get; }

        public int? NhomsanphamId { set; get; }

        public int? SanphamQuantamId { set; get; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }

        public PagingInfo Paging { set; get; }
    }
}


