﻿using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class FilterNhatkythicongModel : FilterHopdongBase
    {
        public int? KehoachId { set; get; }

        public LoaiBaocaoCongviec LoaiCongviec { set; get; }

        public PagingInfo Paging { set; get; }
    }
}