﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongAnhCongtrinhModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int TailieuId { set; get; }
        public string AnhCongtrinh { set; get; }
    }
}