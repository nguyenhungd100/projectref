﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class DichvuHopdongModel
    {
        public int Id { get; set; }
        public int HopdongId { get; set; }
        public string Dichvu { get; set; }
        public int Soluong { get; set; }
        public decimal Gia { get; set; }
        public decimal Thanhtien { get; set; }
    }
}