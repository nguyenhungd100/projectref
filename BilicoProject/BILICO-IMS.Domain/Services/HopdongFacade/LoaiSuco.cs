﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    /// <summary>
    /// Loại sự cố
    /// </summary>
    public enum LoaiSuco
    {
        /// <summary>
        /// Do khách hàng
        /// </summary>
        Do_khach_hang = 1,

        /// <summary>
        /// Do kỹ thuật
        /// </summary>
        Do_ky_thuat = 2,

        /// <summary>
        /// Do khách quan
        /// </summary>
        Khach_quan = 3
    }
}
