﻿using BILICO.IMS.Framework.Data;
using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class TiendoFilterModel
    {
        public int HopdongId { set; get; }

        public int? KehoachId { set; get; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }

        public PagingInfo Paging { set; get; }
    }
}