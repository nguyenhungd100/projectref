﻿using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class FilterQuyettoanModel : FilterHopdongBase
    {
        public int? NguoitamungId { set; get; }
    }
}