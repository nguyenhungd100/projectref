﻿using System.Collections.Generic;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class TiendoHopdongModel
    {
        public List<GiaidoanModel> Giaidoans { set; get; }
    }

    public class GiaidoanModel
    {
        public int KehoachId { set; get; }

        public string Kehoach { set; get; }

        public List<HopdongNhatkythicongModel> NhatkyThicongs { set; get; }
    }
}