﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public interface IHopdongService
    {
        /// <summary>
        /// Chuyển hợp đồng cho kỹ thuật
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        Task<ActionResult> Move2TechAsync(int id);

        Task<ActionResult> Delete(int[] Ids);

        /// <summary>
        /// Lấy danh sách hợp đồng
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        Task<HopdongBaocaoLoinhuanModel> BaocaoLoinhuanAsync(int hopdongId);

        /// <summary>
        /// Lấy danh sách hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongModel>> GetHopdongs(FilterModel filter);


        /// <summary>
        /// Lấy danh sách tiến độ của hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<TiendoHopdongModel> Xemtiendo(TiendoFilterModel filter);


        /// <summary>
        /// Lấy chi tiết kế hoạch của hợp đồng theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<HopdongKehoachModel> GetKehoachByIdAsync(int id);

        /// <summary>
        /// Lấy chi tiết hợp đồng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<HopdongDetailModel> GetHopdongDetail(int id);

        /// <summary>
        /// Hoàn thành hợp đồng
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        Task<ActionResult> HoanthanhHopdongAsync(int hopdongId);

        /// <summary>
        /// Ghi lại hợp đồng
        /// </summary>
        /// <param name="baogiaId"></param>
        /// <param name="sanphamId"></param>
        /// <param name="gia"></param>
        /// <param name="soluong"></param>
        /// <returns></returns>
        Task<ActionResult> SaveHopdong(HopdongDetailModel baogia);

        /// <summary>
        /// List d/s kế hoạch theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongKehoachModel>> FilterKehoach(FilterHopdongBase filter);

        /// <summary>
        /// Thêm kế hoạch mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddKehoach(HopdongKehoachModel kehoach);

        /// <summary>
        /// Cập nhật kế hoạch
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateKehoach(HopdongKehoachModel kehoach);

        /// <summary>
        /// Xóa d/s kế hoạch theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveKehoach(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongNhansuModel>> FilterNhansu(FilterHopdongBase filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddNhansu(HopdongNhansuModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateNhansu(HopdongNhansuModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveNhansu(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongGiaoviecModel>> FilterGiaoviec(FilterHopdongBase filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddGiaoviec(HopdongGiaoviecModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateGiaoviec(HopdongGiaoviecModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveGiaoviec(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongNhatkythicongModel>> FilterNhatkythicong(FilterNhatkythicongModel filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddNhatkythicong(HopdongNhatkythicongModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateNhatkythicong(HopdongNhatkythicongModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveNhatkythicong(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongThanhtoanModel>> FilterThanhtoan(FilterHopdongBase filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddThanhtoan(HopdongThanhtoanModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateThanhtoan(HopdongThanhtoanModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveThanhtoan(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongDukienthuModel>> FilterDukienthu(FilterHopdongBase filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddDukienthu(HopdongDukienthuModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateDukienthu(HopdongDukienthuModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveDukienthu(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongDukienchiModel>> FilterDukienchi(FilterHopdongBase filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddDukienchi(HopdongDukienchiModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateDukienchi(HopdongDukienchiModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveDukienchi(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongDukienhangcanthietModel>> FilterDukienhangcanthiet(FilterHopdongBase filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddDukienhangcanthiet(HopdongDukienhangcanthietModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateDukienhangcanthiet(HopdongDukienhangcanthietModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveDukienhangcanthiet(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongVattuModel>> FilterVattu(FilterHopdongBase filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddVattu(HopdongVattuModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateVattu(HopdongVattuModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveVattu(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongSucoModel>> FilterSuco(FilterSucoModel filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddSuco(HopdongSucoModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateSuco(HopdongSucoModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveSuco(int[] ids);

        /// <summary>
        /// List tạm ứng chậm quyết toán
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        Task<List<HopdongTamungModel>> GetTamungChamquyettoansAsync(int hopdongId);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <param name="nguoiQuyetoanId"></param>
        /// <returns></returns>
        Task<HopdongTamungModel> GetTamungByNguoiQuyettoanIdAsync(int hopdongId, int nguoiQuyetoanId);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongTamungModel>> FilterTamung(FilterTamungModel filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddTamung(HopdongTamungModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateTamung(HopdongTamungModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveTamung(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongQuyettoanModel>> FilterQuyettoan(FilterQuyettoanModel filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddQuyettoan(HopdongQuyettoanModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateQuyettoan(HopdongQuyettoanModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveQuyettoan(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongTailieuDinhkemModel>> FilterTailieuDinhkem(FilterTailieuDinhkemModel filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddTailieuDinhkem(HopdongTailieuDinhkemModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateTailieuDinhkem(HopdongTailieuDinhkemModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveTailieuDinhkem(int[] ids);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongPhanhoiModel>> FilterPhanhoi(FilterPhanhoiModel filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddPhanhoi(HopdongPhanhoiModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdatePhanhoi(HopdongPhanhoiModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemovePhanhoi(int[] ids);


        /// <summary>
        /// Lấy danh sách sản phẩm đặt mua của hợp đồng
        /// </summary>
        /// <param name="hopdongId"></param>
        /// <returns></returns>
        Task<List<SanphamHopdongModel>> GetSanphamByHopdongIdAsync(int hopdongId);


        /// <summary>
        /// Tìm kiếm sản phẩm đã đặt mua của hợp đồng
        /// </summary>
        /// <param name="maSanpham"></param>
        /// <returns></returns>
        Task<List<SanphamHopdongModel>> SearchSanphamDatmuaAsync(string maSanpham);

        /// <summary>
        /// List d/s nhân sự theo hợp đồng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<HopdongDanhgiaNoiboModel>> FilterDanhgiaNoibo(FilterDanhgiaNoiboModel filter);

        /// <summary>
        /// Thêm nhân sự mới
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> AddDanhgiaNoibo(HopdongDanhgiaNoiboModel kehoach);

        /// <summary>
        /// Cập nhật nhân sự
        /// </summary>
        /// <param name="kehoach"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateDanhgiaNoibo(HopdongDanhgiaNoiboModel kehoach);

        /// <summary>
        /// Xóa d/s nhân sự theo id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveDanhgiaNoibo(int[] ids);
    }
}


