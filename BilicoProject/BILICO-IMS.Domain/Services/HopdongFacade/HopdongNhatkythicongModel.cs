﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    /// <summary>
    /// Nhật ký thi công
    /// </summary>
    public class HopdongNhatkythicongModel
    {
        /// <summary>
        /// Mã nhật ký
        /// </summary>
        public int Id { set; get; }

        /// <summary>
        /// Hạng mục công trình
        /// </summary>
        public int KehoachId { set; get; }

        /// <summary>
        /// Mã hợp đồng
        /// </summary>
        public int HopdongId { set; get; }

        /// <summary>
        /// Nội dung báo cáo
        /// </summary>
        public string Noidungbaocao { set; get; }

        /// <summary>
        /// Loại công việc
        /// </summary>
        public LoaiBaocaoCongviec LoaiCongviec { set; get; }

        /// <summary>
        /// Ngày báo cáo
        /// </summary>
        public DateTime? Ngaybaocao { set; get; }

        /// <summary>
        /// Ngày bắt đầu
        /// </summary>
        public DateTime Batdau { set; get; }

        /// <summary>
        /// Ngày kết thúc
        /// </summary>
        public DateTime Ketthuc { set; get; }

        /// <summary>
        /// File đính kèm
        /// </summary>
        public byte[] AnhDinhkem { set; get; }

        /// <summary>
        /// Mã người báo cáo
        /// </summary>
        public int NguoibaocaoId { set; get; }

    }
}