﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    /// <summary>
    /// Loại hợp đồng
    /// </summary>
    public enum LoaiHopdong
    {
        /// <summary>
        /// Hợp đồng dịch vụ kèm bán sản phẩm
        /// </summary>
        HopdongDichvu = 1,

        /// <summary>
        /// Đơn hàng chỉ bán sản phẩm
        /// </summary>
        Donhang = 2
    }
}