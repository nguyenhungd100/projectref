﻿using BILICO.IMS.Framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class FilterHopdongBase
    {
        public string Search { set; get; }

        public int HopdongId { set; get; }

        public PagingInfo Paging { set; get; }
    }
}
