﻿namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    /// <summary>
    /// Trạng thái đơn hàng
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// Chờ xử lý
        /// </summary>
        Pending = 0,

        /// <summary>
        /// Đang xử lý
        /// </summary>
        InProgress = 1,

        /// <summary>
        /// Hoàn tất
        /// </summary>
        Completed = 2,

        /// <summary>
        /// Hợp đồng sale mới tạo
        /// </summary>
        New = 3
    }
}