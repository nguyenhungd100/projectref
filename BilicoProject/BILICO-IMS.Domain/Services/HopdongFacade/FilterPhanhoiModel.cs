﻿using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class FilterPhanhoiModel : FilterHopdongBase
    {
        public int? KythuatId { set; get; }
    }
}