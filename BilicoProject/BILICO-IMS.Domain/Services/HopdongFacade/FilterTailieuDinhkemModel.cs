﻿using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class FilterTailieuDinhkemModel : FilterHopdongBase
    {
        public int? NguoiguiId { set; get; }
    }
}