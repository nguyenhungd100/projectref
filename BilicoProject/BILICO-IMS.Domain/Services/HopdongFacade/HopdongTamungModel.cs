﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongTamungModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int NguoitamungId { set; get; }
        public int NguoichoTamungId { set; get; }
        public DateTime Ngaytamung { set; get; }
        public DateTime? NgayDKQuyettoan { set; get; }
        public decimal Sotien { set; get; }
        public string Ghichu { set; get; }
        public bool Trangthai { set; get; }
    }
}