
using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongModel
    {
        public Int32 HopdongId { set; get; }
        public Int32 KhachhangId { set; get; }
        public Nullable<Int32> BaogiaId { set; get; }
        public String SoHD { set; get; }
        public DateTime Ngaytao { set; get; }
        public Int32 Nguoitao { set; get; }
        public Decimal Trigia { set; get; }
        public Nullable<DateTime> Ngaycapnhat { set; get; }
        public Nullable<Int32> Nguoicapnhat { set; get; }
        public OrderStatus Trangthai { set; get; }
        public String Ghichu { set; get; }
        public LoaiHopdong Loai { set; get; }
        public System.DateTime? NgayTrienkhaiDukien { get; set; }
        public System.DateTime? NgayHoanthanhDukien { get; set; }
    }
}



