﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongDukienthuModel
    {
        public int Id { get; set; }
        public int HopdongId { get; set; }
        public string Noidungthu { get; set; }
        public decimal Sotien { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
    }
}