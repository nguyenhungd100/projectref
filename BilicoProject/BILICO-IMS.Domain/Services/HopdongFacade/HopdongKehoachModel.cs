﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongKehoachModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int? ParentId { set; get; }
        public string NoidungThuchien { set; get; }
        public DateTime Batdau { set; get; }
        public DateTime Ketthuc { set; get; }
        public string Ghichu { set; get; }
        public int? NguoitaoId { get; set; }
        public string Nguoitao { get; set; }
    }
}