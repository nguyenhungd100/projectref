﻿using System;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    public class HopdongSucoModel
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public LoaiSuco LoaiSuco { set; get; }
        public DateTime ThoidiemXayra { set; get; }
        public DateTime? ThoidiemKhacphuc { set; get; }
        public int? NguoithuchienId { set; get; }
        public string PhuonganKhacphuc { set; get; }
        public byte[] AnhHientruong { set; get; }
        public string Ghichu { set; get; }
    }
}