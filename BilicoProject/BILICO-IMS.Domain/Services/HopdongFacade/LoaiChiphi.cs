﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.HopdongFacade
{
    /// <summary>
    /// Loại chi phí
    /// </summary>
    public enum LoaiChiphi
    {
        /// <summary>
        /// Chi phí nhân công
        /// </summary>
        Nhan_cong,

        /// <summary>
        /// Chi phí đi lại
        /// </summary>
        Di_lai,

        /// <summary>
        /// Chi phí ăn uống
        /// </summary>
        An_uong,

        /// <summary>
        /// Chi phí nhà nghỉ
        /// </summary>
        Nha_nghi,

        /// <summary>
        /// Chi phí khác
        /// </summary>
        Khac
    }
}
