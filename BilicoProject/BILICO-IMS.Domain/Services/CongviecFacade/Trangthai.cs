﻿namespace BILICO.IMS.Domain.Services.CongviecFacade
{
    /// <summary>
    /// Trạng thái công việc
    /// </summary>
    public enum Trangthai
    {
        /// <summary>
        /// Chờ xử lý
        /// </summary>
        ChoXuly = 1,

        /// <summary>
        /// Đang xử lý
        /// </summary>
        DangXuly = 2,

        /// <summary>
        /// Hoàn thành
        /// </summary>
        Hoanthanh = 3
    }
}