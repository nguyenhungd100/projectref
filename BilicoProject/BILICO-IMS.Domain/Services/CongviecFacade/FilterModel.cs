
using BILICO.IMS.Framework.Data;
using System;

namespace BILICO.IMS.Domain.Services.CongviecFacade
{
    public class FilterModel
    {
        public string Search { set; get; }

        public Trangthai? Status { set; get; }

        public LoaiCongviec? Type { set; get; }

        public DateTime? Start { set; get; }

        public DateTime? End { set; get; }

        public PagingInfo Paging { set; get; }
    }
}


