﻿namespace BILICO.IMS.Domain.Services.CongviecFacade
{
    /// <summary>
    /// Loại công việc
    /// </summary>
    public enum LoaiCongviec
    {
        Cong_viec_duoc_giao = 1,

        Cong_viec_cua_toi = 2,
    }
}