
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.CongviecFacade
{
    public interface ICongviecService
    {
        Task<CongviecModel> GetById(int congviecId);

        Task<ActionResult> Add(CongviecModel congviec);

        Task<ActionResult> Update(CongviecModel congviec);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<CongviecModel>> List(int currentUserId, FilterModel filter);
    }
}


