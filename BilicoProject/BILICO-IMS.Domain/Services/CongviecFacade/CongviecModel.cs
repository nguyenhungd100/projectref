
using System;
namespace BILICO.IMS.Domain.Services.CongviecFacade
{
    public class CongviecModel
    {
        public Int32 Id { set; get; }
        public String TenCongviec { set; get; }
        public DateTime Ngaybatdau { set; get; }
        public DateTime Deadline { set; get; }
        public String MotaCongviec { set; get; }
        public String FileDinhkems { set; get; }
        public String HopdongLienquans { set; get; }
        public Int32 NguoithuchienId { set; get; }
        public Trangthai Trangthai { set; get; }
        public Int32 NguoitaoId { set; get; }
        public DateTime Ngaytao { set; get; }
    }
}



