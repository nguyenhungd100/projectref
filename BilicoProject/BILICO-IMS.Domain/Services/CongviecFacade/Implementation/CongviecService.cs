
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Data.Entity;

namespace BILICO.IMS.Domain.Services.CongviecFacade.Implementation
{
    public class CongviecService : ICongviecService
    {
        IEntityRepository<Congviec, DomainContext> _congviecRepository = new EntityRepository<Congviec, DomainContext>();

        public async Task<ActionResult> Add(CongviecModel congviec)
        {
            //validate
            if (congviec == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Congviec không để null"
                };

            if (congviec.NguoitaoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có người tạo"
                };

            if (congviec.NguoithuchienId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có người thực hiện"
                };

            if (String.IsNullOrEmpty(congviec.MotaCongviec))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung công việc"
                };

            //add to storage
            congviec.Ngaytao = DateTime.Now;
            var entity = congviec.CloneToModel<CongviecModel, Congviec>();
            var res = await _congviecRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _congviecRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<CongviecModel> GetById(int congviecId)
        {
            var entity = await _congviecRepository.GetByIdAsync(congviecId);
            return entity.CloneToModel<Congviec, CongviecModel>();
        }

        public async Task<List<CongviecModel>> List(int currentUserId, FilterModel filter)
        {
            //var res = await _congviecRepository.FetchAsync(c =>
            //(!String.IsNullOrEmpty(filter.Search) ? c.MotaCongviec.Contains(filter.Search) || c.TenCongviec.Contains(filter.Search) : true) &&
            //c.Trangthai == filter.Status &&
            //(filter.Start != null && filter.End != null ? (DbFunctions.DiffDays(filter.Start, c.Ngaybatdau) >= 0 &&
            //DbFunctions.DiffDays(c.Ngaybatdau, filter.End) >= 0) : true) &&
            //(filter.Type == null ?
            //(filter.Type == LoaiCongviec.Cong_viec_cua_toi ? (c.NguoitaoId == currentUserId && c.NguoithuchienId == currentUserId) : (c.NguoitaoId != currentUserId && c.NguoithuchienId == currentUserId)) :
            //(c.NguoitaoId == currentUserId || c.NguoithuchienId == currentUserId))
            //, c => c.Id, filter.Paging);

            using (var db = new DomainContext())
            {
                var q = db.Congviecs.AsQueryable();

                if (!String.IsNullOrEmpty(filter.Search))
                {
                    q = q.Where(c => c.MotaCongviec.Contains(filter.Search) || c.TenCongviec.Contains(filter.Search));
                }

                if (filter.Status != null)
                {
                    q = q.Where(c => c.Trangthai == filter.Status);
                }

                if (filter.Start != null && filter.End != null)
                {
                    q = q.Where(c => (DbFunctions.DiffDays(filter.Start, c.Ngaybatdau) >= 0 && DbFunctions.DiffDays(c.Ngaybatdau, filter.End) >= 0));
                }

                if (filter.Type != null)
                {
                    if (filter.Type == LoaiCongviec.Cong_viec_cua_toi)
                    {
                        q = q.Where(c => c.NguoitaoId == currentUserId && c.NguoithuchienId == currentUserId);
                    }
                    else
                    {
                        q = q.Where(c => c.NguoitaoId != currentUserId && c.NguoithuchienId == currentUserId);
                    }
                }
                else
                {
                    q = q.Where(c => c.NguoitaoId == currentUserId || c.NguoithuchienId == currentUserId);
                }

                filter.Paging.RowsCount = await q.CountAsync();

                var result = await q.OrderByDescending(c => c.Ngaytao).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToListAsync();

                var models = result.CloneToListModels<Congviec, CongviecModel>();

                return models;
            }
        }

        public async Task<ActionResult> Update(CongviecModel congviec)
        {
            //validate
            if (congviec == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "congviec không để null"
                };


            if (congviec.NguoithuchienId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có người thực hiện"
                };

            if (String.IsNullOrEmpty(congviec.MotaCongviec))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có nội dung công việc"
                };

            var exist = await _congviecRepository.GetByIdAsync(congviec.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "congviec không tồn tại"
                };

            //update to storage
            congviec.Ngaytao = exist.Ngaytao;
            congviec.NguoitaoId = exist.NguoitaoId;
            var entity = congviec.CloneToModel<CongviecModel, Congviec>();
            var res = await _congviecRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

