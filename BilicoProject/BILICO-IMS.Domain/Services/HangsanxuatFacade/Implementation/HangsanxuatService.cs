
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Linq.Expressions;

namespace BILICO.IMS.Domain.Services.HangsanxuatFacade.Implementation
{
    public class HangsanxuatService : IHangsanxuatService
    {
        IEntityRepository<Hangsanxuat, DomainContext> _hangsanxuatRepository = new EntityRepository<Hangsanxuat, DomainContext>();

        public async Task<ActionResult> Add(HangsanxuatModel hangsanxuat)
        {
            //validate
            if (hangsanxuat == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Hangsanxuat không để null"
                };

            //add to storage
            var entity = hangsanxuat.CloneToModel<HangsanxuatModel, Hangsanxuat>();
            var res = await _hangsanxuatRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _hangsanxuatRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<HangsanxuatModel> GetById(int hangsanxuatId)
        {
            var entity = await _hangsanxuatRepository.GetByIdAsync(hangsanxuatId);
            return entity.CloneToModel<Hangsanxuat, HangsanxuatModel>();
        }

        public async Task<List<HangsanxuatModel>> List(FilterModel filter)
        {
            var res = await _hangsanxuatRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenHangsanxuat.Contains(filter.Search) : true), c => c.Id, filter.Paging);

            return res.CloneToListModels<Hangsanxuat, HangsanxuatModel>();
        }

        public async Task<HangsanxuatModel> SingleAsync(Expression<Func<Hangsanxuat, bool>> exp)
        {
            var entity = await _hangsanxuatRepository.SingleAsync(exp);
            return entity.CloneToModel<Hangsanxuat, HangsanxuatModel>();
        }

        public HangsanxuatModel Single(Expression<Func<Hangsanxuat, bool>> exp)
        {
            var entity = _hangsanxuatRepository.Single(exp);
            return entity.CloneToModel<Hangsanxuat, HangsanxuatModel>();
        }

        public async Task<ActionResult> Update(HangsanxuatModel hangsanxuat)
        {
            //validate
            if (hangsanxuat == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "hangsanxuat không để null"
                };

            var exist = await _hangsanxuatRepository.GetByIdAsync(hangsanxuat.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "hangsanxuat không tồn tại"
                };

            //update to storage
            var entity = hangsanxuat.CloneToModel<HangsanxuatModel, Hangsanxuat>();
            var res = await _hangsanxuatRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

