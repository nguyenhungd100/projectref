
using BILICO.IMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.HangsanxuatFacade
{
    public interface IHangsanxuatService
    {
        Task<HangsanxuatModel> GetById(int hangsanxuatId);

        Task<ActionResult> Add(HangsanxuatModel hangsanxuat);

        Task<ActionResult> Update(HangsanxuatModel hangsanxuat);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<HangsanxuatModel>> List(FilterModel filter);

        Task<HangsanxuatModel> SingleAsync(Expression<Func<Hangsanxuat, bool>> exp);

        HangsanxuatModel Single(Expression<Func<Hangsanxuat, bool>> exp);
    }
}


