
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Data.Entity;
using BILICO.IMS.Domain.Services.CongviecFacade;
using BILICO.IMS.Domain.Services.CongviecFacade.Implementation;
using BILICO.IMS.Domain.Services.SanphamFacade;
using BILICO.IMS.Domain.Services.SanphamFacade.Implementation;

namespace BILICO.IMS.Domain.Services.KhachhangFacade.Implementation
{
    public class KhachhangService : IKhachhangService
    {
        IEntityRepository<Khachhang, DomainContext> _khachhangRepository = new EntityRepository<Khachhang, DomainContext>();
        IEntityRepository<Khachhang_Tuvan, DomainContext> _tuvanRepository = new EntityRepository<Khachhang_Tuvan, DomainContext>();
        IEntityRepository<Khachhang_SanphamQuantam, DomainContext> _sanphamQuantamRepository = new EntityRepository<Khachhang_SanphamQuantam, DomainContext>();
        IEntityRepository<Khachhang_Baogia, DomainContext> _baogiaRepository = new EntityRepository<Khachhang_Baogia, DomainContext>();
        IEntityRepository<Khachhang_Baogia_ChitietDichvu, DomainContext> _baogiaDichvuRepository = new EntityRepository<Khachhang_Baogia_ChitietDichvu, DomainContext>();
        IEntityRepository<Khachhang_Baogia_ChitietSanpham, DomainContext> _baogiaSanphamRepository = new EntityRepository<Khachhang_Baogia_ChitietSanpham, DomainContext>();
        IEntityRepository<Khachhang_Nhucaugia, DomainContext> _nhucaugiaRepository = new EntityRepository<Khachhang_Nhucaugia, DomainContext>();
        ICongviecService _congviecService = new CongviecService();
        ISanphamService _sanphamService = new SanphamService();

        public async Task<ActionResult> Add(KhachhangModel khachhang)
        {
            //validate
            if (khachhang == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Khachhang không để null"
                };

            if (khachhang.Nguoitao <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có người tạo"
                };

            if (String.IsNullOrEmpty(khachhang.Hoten))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Họ và tên là bắt buộc nhập"
                };

            if (String.IsNullOrEmpty(khachhang.Email) && String.IsNullOrEmpty(khachhang.Mobile))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Email hoặc Mobile là bắt buộc nhập"
                };

            var exist = await _khachhangRepository.SingleAsync(c => (!String.IsNullOrEmpty(khachhang.Email) ? c.Email == khachhang.Email : false) || (!String.IsNullOrEmpty(khachhang.Mobile) ? c.Mobile == khachhang.Mobile : false));
            if (exist != null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Email hoặc Mobile đã tồn tại"
                };

            //add to storage
            if (khachhang.SalerId != null)
            {
                khachhang.Trangthai = CustomerStatus.Move2Sale;
            }

            if (khachhang.SubSalerId != null)
            {
                khachhang.Trangthai = CustomerStatus.Move2SubSale;
            }

            khachhang.Ngaytao = DateTime.Now;
            var entity = khachhang.CloneToModel<KhachhangModel, Khachhang>();
            var res = await _khachhangRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> AddSanphamQuantam(int customerId, int[] SanphamIds)
        {
            var sanphams = SanphamIds.Select(c => new Khachhang_SanphamQuantam()
            {
                KhachhangId = customerId,
                SanphamId = c
            });

            var res = await _sanphamQuantamRepository.InsertManyAsync(sanphams);
            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> AddTuvan(TuvanModel tuvan)
        {
            //validate
            if (tuvan == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tuvan không để null"
                };

            if (tuvan.HanXuly != null && tuvan.HanXulyTime != null)
                tuvan.HanXuly = new DateTime(tuvan.HanXuly.Value.Year, tuvan.HanXuly.Value.Month, tuvan.HanXuly.Value.Day, tuvan.HanXulyTime.Value.Hour, tuvan.HanXulyTime.Value.Minute, 0);

            if (tuvan.Nguoitao <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có người tạo"
                };

            TinhtrangTuvan tinhtrang;
            if (!Enum.TryParse<TinhtrangTuvan>(tuvan.Tinhtrang.ToString(), out tinhtrang))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tình trạng không hợp lệ"
                };

            if (tuvan.KhachhangId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn khách hàng tư vấn"
                };

            if (tuvan.SanphamTuvanId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn sản phẩm tư vấn"
                };

            if (String.IsNullOrEmpty(tuvan.NoidungTuvan))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nội dung tư vấn là bắt buộc"
                };

            if (tuvan.PhanXuly != null && tuvan.PhanXuly == true)
            {
                if (tuvan.NguoiXulyId == null || String.IsNullOrEmpty(tuvan.NoidungGiaoviec) || tuvan.HanXuly == null)
                    return new ActionResult()
                    {
                        Result = false,
                        Error = "Chưa nhập nội dung phân công xử lý"
                    };
            }
            else
            {
                tuvan.NguoiXulyId = null;
                tuvan.NoidungGiaoviec = null;
                tuvan.HanXuly = null;
            }

            //add to storage
            tuvan.Ngaytao = DateTime.Now;
            var entity = tuvan.CloneToModel<TuvanModel, Khachhang_Tuvan>();
            var res = await _tuvanRepository.AddAsync(entity);

            //handle giao viec
            if (tuvan.PhanXuly != null && tuvan.PhanXuly == true)
            {
                await _congviecService.Add(new CongviecModel()
                {
                    Deadline = tuvan.HanXuly.Value,
                    MotaCongviec = tuvan.NoidungGiaoviec,
                    Ngaybatdau = DateTime.Now,
                    Ngaytao = DateTime.Now,
                    NguoitaoId = tuvan.Nguoitao,
                    NguoithuchienId = tuvan.NguoiXulyId.Value,
                    TenCongviec = tuvan.NoidungGiaoviec,
                    Trangthai = Trangthai.ChoXuly
                });
            }

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _khachhangRepository.DeleteManyAsync(c => Ids.Contains(c.KhachhangId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<ActionResult> DeleteTuvan(int[] tuvanIds)
        {
            var res = await _tuvanRepository.DeleteManyAsync(c => tuvanIds.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<BaogiaDetailModel> GetBaogiaDetail(int id)
        {
            var res = await _baogiaRepository.GetByIdAsync(id);
            if (res == null)
                return null;

            var result = new BaogiaDetailModel();
            result.Baogia = res.CloneToModel<Khachhang_Baogia, BaogiaModel>();

            var sanphams = await _baogiaSanphamRepository.FetchAsync(c => c.BaogiaId == id);
            result.Sanphams = sanphams.CloneToListModels<Khachhang_Baogia_ChitietSanpham, SanphamBaogiaModel>();

            foreach (var sanpham in result.Sanphams)
            {
                var sp = await _sanphamService.GetById(sanpham.SanphamId);
                sanpham.TenSanpham = sp != null ? sp.TenSanpham : "---";
            }

            var dichvus = await _baogiaDichvuRepository.FetchAsync(c => c.BaogiaId == id);
            result.Dichvus = dichvus.CloneToListModels<Khachhang_Baogia_ChitietDichvu, DichvuBaogiaModel>();

            return result;
        }

        public async Task<List<BaogiaModel>> GetBaogias(TuvanFilterModel filter)
        {
            var res = await _baogiaRepository.FetchAsync(c =>
            (!String.IsNullOrEmpty(filter.Search) ? c.NoidungBaogia.Contains(filter.Search) || c.PhanhoiKhachhang.Contains(filter.Search) : true) &&
            (filter.CustomerId > 0 ? c.KhachhangId == filter.CustomerId : true)
            , c => c.Ngaycapnhat, filter.Paging, SortDirection.Descending);

            return res.CloneToListModels<Khachhang_Baogia, BaogiaModel>();
        }

        public async Task<KhachhangModel> GetById(int khachhangId)
        {
            var entity = await _khachhangRepository.GetByIdAsync(khachhangId);
            return entity.CloneToModel<Khachhang, KhachhangModel>();
        }

        public async Task<List<SanphamQuantamModel>> GetSanphamQuantams(int customerId)
        {
            //var sanphams = await _sanphamQuantamRepository.FetchAsync(c => c.KhachhangId == customerId);
            //return sanphams.CloneToListModels<Khachhang_SanphamQuantam, SanphamQuantamModel>();
            using (var db = new DomainContext())
            {
                var q = from k in db.Khachhang_SanphamQuantams
                        join s in db.Sanphams on k.SanphamId equals s.SanphamId
                        where k.KhachhangId == customerId
                        select new SanphamQuantamModel()
                        {
                            Id = k.Id,
                            KhachhangId = k.KhachhangId,
                            SanphamId = k.SanphamId,
                            MaSanpham = s.MaSanpham,
                            TenSanpham = s.TenSanpham,
                            AnhSanpham = s.AnhSanpham
                        };

                return await q.ToListAsync();
            }
        }

        public async Task<List<KhachNhucaugiaModel>> GetNhucauGias(int customerId)
        {
            using (var db = new DomainContext())
            {
                var q = from k in db.Khachhang_Nhucaugias
                        join s in db.Nhucaugias on k.NhucaugiaId equals s.NhucauId
                        where k.KhachhangId == customerId
                        select new KhachNhucaugiaModel()
                        {
                            Id = k.Id,
                            NhucaugiaId = k.NhucaugiaId,
                            KhachhangId = k.KhachhangId,
                            TenNhucau = s.TenNhucau
                        };

                return await q.ToListAsync();
            }
        }

        public async Task<List<TuvanModel>> GetTuvans(TuvanFilterModel filter)
        {
            var tuvans = await _tuvanRepository.FetchAsync(c => c.KhachhangId == filter.CustomerId && (!String.IsNullOrEmpty(filter.Search) ? c.NoidungTuvan.Contains(filter.Search) : true), c => c.Ngaytao, filter.Paging);
            return tuvans.CloneToListModels<Khachhang_Tuvan, TuvanModel>();
        }

        public async Task<ActionResult> Import(List<KhachhangModel> customers)
        {
            foreach (var customer in customers)
            {
                var res = await Add(customer);
                if (res.Result == false)
                    return res;
            }

            return new ActionResult() { Result = true };
        }

        public async Task<List<KhachhangModel>> List(FilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from c in db.Khachhangs
                            //  where c.Trangthai == filter.Status
                        select c;

                switch (filter.Status)//request view
                {
                    case CustomerStatus.New:
                        {
                            q = q.Where(c => c.Nguoitao == filter.NguoitaoId && c.Trangthai == filter.Status && c.SalerId == null);
                        }
                        break;
                    case CustomerStatus.Converted:
                        {
                            q = q.Where(c => (c.Nguoitao == filter.NguoitaoId || c.SalerId == filter.KinhdoanhNhanId) && c.Trangthai == filter.Status);
                        }
                        break;
                    case CustomerStatus.Move2Sale:
                        {
                            q = q.Where(c => (c.Trangthai == CustomerStatus.Move2Sale || c.Trangthai == CustomerStatus.Received) && c.Nguoitao == filter.NguoitaoId);
                            if (filter.KinhdoanhNhanId != null)
                            {
                                q = q.Where(c => c.SalerId == filter.KinhdoanhNhanId && c.SubSalerId == null);
                            }
                            else
                            {
                                q = q.Where(c => c.SalerId != null);
                            }
                        }
                        break;
                    case CustomerStatus.Move2SubSale:
                        {
                            q = q.Where(c => (c.Trangthai == CustomerStatus.Move2SubSale));
                            if (filter.KinhdoanhNhanId != null)
                            {
                                q = q.Where(c => c.SalerId == filter.KinhdoanhNhanId && c.SubSalerId != null);
                            }

                            if (filter.SubSalerId != null)
                            {
                                q = q.Where(c => c.SubSalerId == filter.SubSalerId);
                            }
                        }
                        break;
                    case CustomerStatus.NotConversion:
                        {
                            q = q.Where(c => (c.Nguoitao == filter.NguoitaoId || c.SalerId == filter.KinhdoanhNhanId) && c.Trangthai == filter.Status);
                        }
                        break;
                    case CustomerStatus.Received:
                        {
                            q = q.Where(c => (c.Trangthai == CustomerStatus.Move2Sale || c.Trangthai == CustomerStatus.Received || c.Trangthai == CustomerStatus.Converted) && (c.SalerId == filter.KinhdoanhNhanId));
                        }
                        break;
                }

                if (!String.IsNullOrEmpty(filter.Search))
                {
                    q = q.Where(c => c.Diachi.Contains(filter.Search) || c.Ghichu.Contains(filter.Search) || c.Hoten.Contains(filter.Search) || c.Mobile.Contains(filter.Search));
                }

                if (filter.LoaikhachId != null)
                {
                    q = q.Where(c => c.LoaiKHId == filter.LoaikhachId);
                }

                if (filter.NguoixulyId != null)
                {
                    q = q.Where(c => c.Nguoicapnhat == filter.NguoixulyId);
                }

                if (filter.NguonkhachId != null)
                {
                    q = q.Where(c => c.NguonkhachId == filter.NguonkhachId);
                }

                if (filter.ThanhphoId != null)
                {
                    q = q.Where(c => c.ThanhphoId == filter.ThanhphoId);
                }

                if (filter.QuanhuyenId != null)
                {
                    q = q.Where(c => c.QuanhuyenId == filter.QuanhuyenId);
                }

                if (filter.LoaiKhachhang!=null)
                {
                    q = q.Where(c => c.LoaiKhachhang == filter.LoaiKhachhang);
                }

                if (filter.NhucauId != null)
                {
                    q = from k in q
                        join n in db.Khachhang_Nhucaugias on k.KhachhangId equals n.KhachhangId
                        where n.NhucaugiaId == filter.NhucauId
                        select k;
                }

             
                if (filter.SanphamQuantamId != null)
                {
                    q = from k in q
                        join n in db.Khachhang_SanphamQuantams on k.KhachhangId equals n.KhachhangId
                        where n.SanphamId == filter.SanphamQuantamId
                        select k;
                }

                if (filter.Start != null && filter.End != null)
                {
                    q = q.Where(c =>
                    DbFunctions.DiffDays(filter.Start, c.Ngaytao) >= 0 &&
                    DbFunctions.DiffDays(c.Ngaytao, filter.End) >= 0);
                }

                filter.Paging.RowsCount = await q.CountAsync();

                var entites = await q.OrderByDescending(c => c.Ngaytao).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToListAsync();

                var models = entites.CloneToListModels<Khachhang, KhachhangModel>();

                if (filter.Status == CustomerStatus.Received)
                {
                    //auto change to received
                    entites.ForEach(c =>
                    {
                        if (c.Trangthai == CustomerStatus.Move2Sale)
                        {
                            c.Trangthai = CustomerStatus.Received;
                            db.SaveChanges();
                        }
                    });
                }

                return models;
            }
        }

        public async Task<bool> Move2Sale(Move2SaleModel move2SaleModel)
        {
            var customers = await _khachhangRepository.FetchAsync(c => move2SaleModel.customerIds.Contains(c.KhachhangId));

            foreach (var customer in customers)
            {
                customer.SalerId = move2SaleModel.salerId;
                customer.Ngaygiao = DateTime.Now;
                customer.Trangthai = CustomerStatus.Move2Sale;
                customer.NhandebietIds = move2SaleModel.nhandebietIds;
            }

            return await _khachhangRepository.UpdateManyAsync(customers);
        }

        public async Task<bool> Move2SubSale(Move2SubSaleModel move2SubSaleModel)
        {
            var customers = await _khachhangRepository.FetchAsync(c => move2SubSaleModel.customerIds.Contains(c.KhachhangId));

            foreach (var customer in customers)
            {
                customer.SubSalerId = move2SubSaleModel.subSalerId;
                customer.Ngaygiao = DateTime.Now;
                customer.Trangthai = CustomerStatus.Move2SubSale;
                customer.NhandebietIds = move2SubSaleModel.nhandebietIds;
            }

            return await _khachhangRepository.UpdateManyAsync(customers);
        }

        public async Task<bool> Return2Marketing(int customerId)
        {
            var customer = await _khachhangRepository.GetByIdAsync(customerId);

            if (customer != null)
            {
                customer.Trangthai = CustomerStatus.NotConversion;

                return await _khachhangRepository.UpdateAsync(customer);
            }

            return false;
        }

        public async Task<bool> Change2Conversion(int customerId)
        {
            var customer = await _khachhangRepository.GetByIdAsync(customerId);

            if (customer != null)
            {
                customer.Trangthai = CustomerStatus.Converted;

                return await _khachhangRepository.UpdateAsync(customer);
            }

            return false;
        }

        public async Task<ActionResult> RemoveSanphamQuantam(int[] sanphamQuantamIds)
        {
            using (var db = _khachhangRepository.GetDbContext())
            {
                var items = db.Khachhang_SanphamQuantams.Where(c => sanphamQuantamIds.Contains(c.Id));

                foreach (var item in items)
                    db.Khachhang_SanphamQuantams.Remove(item);

                var res = await db.SaveChangesAsync();

                return new ActionResult()
                {
                    Result = res > 0 ? true : false
                };
            }
        }

        public async Task<ActionResult> SaveBaogia(BaogiaDetailModel baogia)
        {
            if (baogia == null || baogia.Baogia == null || String.IsNullOrEmpty(baogia.Baogia.NoidungBaogia))
            {
                return new ActionResult() { Result = false, Error = "Chưa nhập báo giá" };
            }

            if (baogia.Baogia.KhachhangId <= 0)
            {
                return new ActionResult() { Result = false, Error = "Chưa chọn khách hàng báo giá" };
            }

            if (baogia.Sanphams == null || baogia.Sanphams.Count == 0)
            {
                return new ActionResult() { Result = false, Error = "Chưa chọn sản phẩm" };
            }

            if (baogia.Sanphams.Any(c => c.Gia <= 0))
            {
                return new ActionResult() { Result = false, Error = "Giá sản phẩm phải lớn hơn 0" };
            }

            if (baogia.Sanphams.Any(c => c.Soluong <= 0))
            {
                return new ActionResult() { Result = false, Error = "Số lượng sản phẩm phải lớn hơn 0" };
            }

            if (baogia.Dichvus != null && baogia.Dichvus.Count >= 0 && baogia.Dichvus.Any(c => String.IsNullOrEmpty(c.Dichvu)))
            {
                return new ActionResult() { Result = false, Error = "Tên dịch vụ là bắt buộc" };
            }

            if (baogia.Dichvus != null && baogia.Dichvus.Count >= 0 && baogia.Dichvus.Any(c => c.Gia <= 0))
            {
                return new ActionResult() { Result = false, Error = "Giá dịch vụ phải lớn hơn 0" };
            }

            if (baogia.Dichvus != null && baogia.Dichvus.Count >= 0 && baogia.Dichvus.Any(c => c.Soluong <= 0))
            {
                return new ActionResult() { Result = false, Error = "Số lượng dịch vụ phải lớn hơn 0" };
            }

            if (baogia.Baogia.BaogiaId <= 0)//them moi
            {
                //validate nguoi tao
                if (baogia.Baogia.Nguoitao <= 0)
                {
                    return new ActionResult() { Result = false, Error = "Chưa có người tạo" };
                }
                baogia.Baogia.Ngaytao = DateTime.Now;

                var entity = baogia.Baogia.CloneToModel<BaogiaModel, Khachhang_Baogia>();

                var res = await _baogiaRepository.InsertAsync(entity);
                if (res)
                {
                    var sanphams = baogia.Sanphams.CloneToListModels<SanphamBaogiaModel, Khachhang_Baogia_ChitietSanpham>();

                    sanphams.ForEach(c => { c.BaogiaId = entity.BaogiaId; c.Thanhtien = c.Gia * c.Soluong; });

                    await _baogiaSanphamRepository.InsertManyAsync(sanphams);

                    if (baogia.Dichvus != null && baogia.Dichvus.Count > 0)
                    {
                        var dichvus = baogia.Dichvus.CloneToListModels<DichvuBaogiaModel, Khachhang_Baogia_ChitietDichvu>();

                        dichvus.ForEach(c => { c.BaogiaId = entity.BaogiaId; c.Thanhtien = c.Gia * c.Soluong; });

                        await _baogiaDichvuRepository.InsertManyAsync(dichvus);
                    }
                }
            }
            else//cap nhat
            {
                //validate nguoi cap nhat
                if (baogia.Baogia.Nguoicapnhat == null || baogia.Baogia.Nguoicapnhat <= 0)
                {
                    return new ActionResult() { Result = false, Error = "Chưa có người cập nhật" };
                }
                baogia.Baogia.Ngaycapnhat = DateTime.Now;

                //keep old data
                var exist = await _baogiaRepository.GetByIdAsync(baogia.Baogia.BaogiaId);
                baogia.Baogia.Ngaytao = exist.Ngaytao;
                baogia.Baogia.Nguoitao = exist.Nguoitao;

                var entity = baogia.Baogia.CloneToModel<BaogiaModel, Khachhang_Baogia>();

                var res = await _baogiaRepository.UpdateAsync(entity);
                if (res)
                {
                    var sanphams = baogia.Sanphams.CloneToListModels<SanphamBaogiaModel, Khachhang_Baogia_ChitietSanpham>();

                    sanphams.ForEach(c => { c.BaogiaId = entity.BaogiaId; c.Thanhtien = c.Gia * c.Soluong; });

                    //add new data
                    await _baogiaSanphamRepository.InsertManyAsync(sanphams.Where(c => c.Id == 0));

                    //remove old data
                    var oldIds = sanphams.Where(c => c.Id > 0).Select(c => c.Id);
                    var removes = await _baogiaSanphamRepository.DeleteManyAsync(c => c.BaogiaId == baogia.Baogia.BaogiaId && !oldIds.Contains(c.Id));

                    //Update old data
                    res = await _baogiaSanphamRepository.UpdateManyAsync(sanphams.Where(c => c.Id > 0));

                    if (baogia.Dichvus != null && baogia.Dichvus.Count > 0)
                    {
                        var dichvus = baogia.Dichvus.CloneToListModels<DichvuBaogiaModel, Khachhang_Baogia_ChitietDichvu>();

                        dichvus.ForEach(c => { c.BaogiaId = entity.BaogiaId; c.Thanhtien = c.Gia * c.Soluong; });

                        //add new data
                        await _baogiaDichvuRepository.InsertManyAsync(dichvus.Where(c => c.Id == 0));

                        //remove old data
                        oldIds = dichvus.Where(c => c.Id > 0).Select(c => c.Id);
                        removes = await _baogiaDichvuRepository.DeleteManyAsync(c => c.BaogiaId == baogia.Baogia.BaogiaId && !oldIds.Contains(c.Id));

                        //Update old data
                        res = await _baogiaDichvuRepository.UpdateManyAsync(dichvus.Where(c => c.Id > 0));
                    }
                }
            }//if

            return new ActionResult() { Result = true };
        }

        public async Task<ActionResult> SaveNhucau(int customerId, int[] NhucauIds)
        {
            if (customerId <= 0)
            {
                return new ActionResult() { Result = false, Error = "Chưa chọn khách hàng" };
            }

            if (NhucauIds == null || NhucauIds.Count() <= 0)
            {
                return new ActionResult() { Result = false, Error = "Chọn ít nhất 1 nhu cầu" };
            }

            if (NhucauIds.Any(c => c <= 0))
            {
                return new ActionResult() { Result = false, Error = "Nhu cầu không tồn tại" };
            }

            //remove old data
            var removes = await _nhucaugiaRepository.DeleteManyAsync(c => c.KhachhangId == customerId && !NhucauIds.Contains(c.NhucaugiaId));

            //add new if not exist
            NhucauIds.ToList().ForEach(k =>
            {
                var exist = _nhucaugiaRepository.Single(c => c.KhachhangId == customerId && c.NhucaugiaId == k);
                if (exist == null)
                {
                    _nhucaugiaRepository.Insert(new Khachhang_Nhucaugia()
                    {
                        KhachhangId = customerId,
                        NhucaugiaId = k
                    });
                }
            });

            return new ActionResult() { Result = true };
        }

        public async Task<ActionResult> Update(KhachhangModel khachhang)
        {
            //validate
            if (khachhang == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "khachhang không để null"
                };

            if (khachhang.Nguoicapnhat <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có người tạo"
                };

            if (String.IsNullOrEmpty(khachhang.Hoten))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Họ và tên là bắt buộc nhập"
                };

            if (String.IsNullOrEmpty(khachhang.Email) && String.IsNullOrEmpty(khachhang.Mobile))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Email hoặc Mobile là bắt buộc nhập"
                };

            var exist = await _khachhangRepository.GetByIdAsync(khachhang.KhachhangId);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "khachhang không tồn tại"
                };

            //update to storage
            if (khachhang.SalerId != null)
            {
                khachhang.Trangthai = CustomerStatus.Move2Sale;
            }

            if (khachhang.SubSalerId != null)
            {
                khachhang.Trangthai = CustomerStatus.Move2SubSale;
            }

            khachhang.Ngaycapnhat = DateTime.Now;
            var entity = khachhang.CloneToModel<KhachhangModel, Khachhang>();
            var res = await _khachhangRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> UpdateTuvan(TuvanModel tuvan)
        {
            //validate
            if (tuvan == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tuvan không để null"
                };

            if (tuvan.HanXuly != null && tuvan.HanXulyTime != null)
                tuvan.HanXuly = new DateTime(tuvan.HanXuly.Value.Year, tuvan.HanXuly.Value.Month, tuvan.HanXuly.Value.Day, tuvan.HanXulyTime.Value.Hour, tuvan.HanXulyTime.Value.Minute, 0);

            TinhtrangTuvan tinhtrang;
            if (!Enum.TryParse<TinhtrangTuvan>(tuvan.Tinhtrang.ToString(), out tinhtrang))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Tình trạng không hợp lệ"
                };

            if (tuvan.KhachhangId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn khách hàng tư vấn"
                };

            if (tuvan.SanphamTuvanId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn sản phẩm tư vấn"
                };

            if (String.IsNullOrEmpty(tuvan.NoidungTuvan))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nội dung tư vấn là bắt buộc"
                };

            if (tuvan.PhanXuly != null && tuvan.PhanXuly == true)
            {
                if (tuvan.NguoiXulyId == null || String.IsNullOrEmpty(tuvan.NoidungGiaoviec) || tuvan.HanXuly == null)
                    return new ActionResult()
                    {
                        Result = false,
                        Error = "Chưa chọn người xử lý, nội dung giao việc hoặc thời hạn hoàn thành"
                    };
            }
            else
            {
                tuvan.NguoiXulyId = null;
                tuvan.NoidungGiaoviec = null;
                tuvan.HanXuly = null;
            }

            //keep old data
            var exist = await _tuvanRepository.GetByIdAsync(tuvan.Id);
            tuvan.Ngaytao = exist.Ngaytao;
            tuvan.Nguoitao = exist.Nguoitao;

            //add to storage
            var entity = tuvan.CloneToModel<TuvanModel, Khachhang_Tuvan>();
            var res = await _tuvanRepository.UpdateAsync(entity);

            //handle giao viec
            if ((tuvan.PhanXuly != null && tuvan.PhanXuly == true) && (exist.PhanXuly == false || exist.PhanXuly == null))
            {
                await _congviecService.Add(new CongviecModel()
                {
                    Deadline = tuvan.HanXuly.Value,
                    MotaCongviec = tuvan.NoidungGiaoviec,
                    Ngaybatdau = DateTime.Now,
                    Ngaytao = DateTime.Now,
                    NguoitaoId = tuvan.Nguoitao,
                    NguoithuchienId = tuvan.NguoiXulyId.Value,
                    TenCongviec = tuvan.NoidungGiaoviec,
                    Trangthai = Trangthai.ChoXuly
                });
            }

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

