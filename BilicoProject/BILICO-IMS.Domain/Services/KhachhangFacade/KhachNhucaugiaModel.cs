﻿namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class KhachNhucaugiaModel
    {
        public int Id { get; set; }
        public int KhachhangId { get; set; }
        public int NhucaugiaId { get; set; }

        //not map
        public string TenNhucau { set; get; }
    }
}