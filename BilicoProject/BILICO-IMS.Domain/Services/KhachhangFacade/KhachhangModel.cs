
using System;

namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public enum CustomerType
    {
        Congty = 1,
        Canhan = 2
    }
    public class KhachhangModel
    {
        public Int32 KhachhangId { set; get; }

        public CustomerType? LoaiKhachhang { set; get; }
        public String TenCongty { set; get; }
        public String Xungho { set; get; }
        public String Hoten { set; get; }
        public Nullable<DateTime> Ngaysinh { set; get; }
        public Nullable<Boolean> Gioitinh { set; get; }
        public String Email { set; get; }
        public String Mobile { set; get; }
        public String TKFacebook { set; get; }
        public Nullable<Int32> ThanhphoId { set; get; }
        public Nullable<Int32> QuanhuyenId { set; get; }
        public Nullable<Int32> LoaiKHId { set; get; }
        public Nullable<Int32> NguonkhachId { set; get; }
        public DateTime Ngaytao { set; get; }
        public Int32 Nguoitao { set; get; }
        public Nullable<DateTime> Ngaycapnhat { set; get; }
        public Nullable<Int32> Nguoicapnhat { set; get; }
        public CustomerStatus Trangthai { set; get; }
        public Nullable<DateTime> Ngaygiao { set; get; }
        public Nullable<Int32> SalerId { set; get; }
        public String NhandebietIds { set; get; }
        public String DiachiMap { set; get; }
        public String Ghichu { set; get; }
        public String FileDinhkem { set; get; }
        public string Diachi { set; get; }
        public DateTime Ngayhoanthanh { set; get; }
        public int? SubSalerId { get; set; }
    }
}



