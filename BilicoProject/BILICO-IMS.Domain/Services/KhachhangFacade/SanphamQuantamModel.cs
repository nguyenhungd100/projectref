
using System;
namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class SanphamQuantamModel
    {
        public Int32 Id { set; get; }
        public Int32 SanphamId { set; get; }
        public Int64 KhachhangId { set; get; }
        public String MaSanpham { set; get; }
        public String TenSanpham { set; get; }
        public string AnhSanpham { get; set; }
    }
}



