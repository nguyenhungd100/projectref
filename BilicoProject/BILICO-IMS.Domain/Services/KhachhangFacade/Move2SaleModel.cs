﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class Move2SaleModel
    {
        public int salerId { set; get; }

        public int[] customerIds { set; get; }

        public String nhandebietIds { set; get; }

    }
}
