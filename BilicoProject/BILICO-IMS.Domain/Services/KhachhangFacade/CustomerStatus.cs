﻿namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public enum CustomerStatus
    {
        /// <summary>
        /// Khách hàng mới
        /// </summary>
        New = 0,

        /// <summary>
        /// Đã chuyển sale manager
        /// </summary>
        Move2Sale = 1,

        /// <summary>
        /// Đã chuyển đổi
        /// </summary>
        Converted = 2,

        /// <summary>
        /// Chưa chuyển đổi
        /// </summary>
        NotConversion = 3,

        /// <summary>
        /// Sale manager đã nhận
        /// </summary>
        Received = 4,

        /// <summary>
        /// Chuyển cho sale
        /// </summary>
        Move2SubSale = 5
    }
}