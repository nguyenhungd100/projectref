﻿namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class SanphamBaogiaModel
    {
        public int Id { get; set; }
        public int BaogiaId { get; set; }
        public int SanphamId { get; set; }
        public int Soluong { get; set; }
        public decimal Gia { get; set; }
        public decimal Thanhtien { get; set; }

        //not map
        public string TenSanpham { set; get; }
    }
}