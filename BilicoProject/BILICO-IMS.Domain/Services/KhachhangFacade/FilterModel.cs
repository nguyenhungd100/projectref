
using BILICO.IMS.Framework.Data;
using System;

namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class FilterModel
    {
        public string Search { set; get; }

        public CustomerStatus Status { set; get; }

        public DateTime? Start { set; get; }

        public DateTime? End { set; get; }

        public int? KinhdoanhNhanId { set; get; }

        public int? NguoitaoId { set; get; }

        public int? CskhId { set; get; }

        public int? ThanhphoId { set; get; }

        public int? QuanhuyenId { set; get; }

        public int? NhucauId { set; get; }

        public int? NguoixulyId { set; get; }

        public int? LoaikhachId { set; get; }

        public int? SanphamQuantamId { set; get; }

        public int? NguonkhachId { set; get; }

        public PagingInfo Paging { set; get; }

        public int? SubSalerId { get; set; }

        public CustomerType? LoaiKhachhang { set; get; }

        
    }
}


