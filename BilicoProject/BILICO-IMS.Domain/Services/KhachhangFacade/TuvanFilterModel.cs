﻿using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class TuvanFilterModel
    {
        public string Search { set; get; }

        public int CustomerId { set; get; }

        public PagingInfo Paging { set; get; }
    }
}