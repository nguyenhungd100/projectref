﻿using System.Collections.Generic;

namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class BaogiaDetailModel
    {
        public BaogiaModel Baogia { set; get; }

        public List<SanphamBaogiaModel> Sanphams { set; get; }

        public List<DichvuBaogiaModel> Dichvus { set; get; }
    }
}