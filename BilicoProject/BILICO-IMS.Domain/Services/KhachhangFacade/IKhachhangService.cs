﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public interface IKhachhangService
    {
        Task<KhachhangModel> GetById(int khachhangId);

        Task<ActionResult> Add(KhachhangModel khachhang);

        Task<ActionResult> Update(KhachhangModel khachhang);

        Task<ActionResult> Delete(int[] Ids);

        /// <summary>
        /// Lọc d/s khách hàng theo bộ lọc
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<KhachhangModel>> List(FilterModel filter);

        /// <summary>
        /// Chuyển danh sách khách hàng cho saler manager
        /// </summary>
        /// <param name="salerId"></param>
        /// <param name="customerIds"></param>
        /// <param name="nhandebietIds"></param>
        /// <returns></returns>
        Task<bool> Move2Sale(Move2SaleModel move2SaleModel);


        /// <summary>
        /// Chuyển danh sách khách hàng cho saler
        /// </summary>
        /// <param name="subSalerId"></param>
        /// <param name="customerIds"></param>
        /// <param name="nhandebietIds"></param>
        /// <returns></returns>
        Task<bool> Move2SubSale(Move2SubSaleModel move2SubSaleModel);


        /// <summary>
        /// Gửi khách hàng trả lại cho marketing chăm sóc tiếp
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<bool> Return2Marketing(int customerId);

        /// <summary>
        /// Nạp d/s khách hàng bởi creator
        /// </summary>
        /// <param name="customers"></param>
        /// <returns></returns>
        Task<ActionResult> Import(List<KhachhangModel> customers);

        /// <summary>
        /// Ghi lại nhu cầu của khách hàng
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="NhucauIds"></param>
        /// <returns></returns>
        Task<ActionResult> SaveNhucau(int customerId, int[] NhucauIds);

        /// <summary>
        /// Thêm sản phẩm vào d/s quan tâm
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="SanphamIds"></param>
        /// <returns></returns>
        Task<ActionResult> AddSanphamQuantam(int customerId, int[] SanphamIds);

        /// <summary>
        /// Bỏ sản phẩm quan tâm của khách hàng
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="SanphamIds"></param>
        /// <returns></returns>
        Task<ActionResult> RemoveSanphamQuantam(int[] sanphamQuantamIds);

        /// <summary>
        /// Lấy d/s sản phẩm quan tâm của khách hàng
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<List<SanphamQuantamModel>> GetSanphamQuantams(int customerId);

        /// <summary>
        /// Thêm tư vấn mới
        /// </summary>
        /// <param name="tuvan"></param>
        /// <returns></returns>
        Task<ActionResult> AddTuvan(TuvanModel tuvan);

        /// <summary>
        /// Cập nhật tư vấn
        /// </summary>
        /// <param name="tuvan"></param>
        /// <returns></returns>
        Task<ActionResult> UpdateTuvan(TuvanModel tuvan);

        /// <summary>
        /// Xóa tư vấn 
        /// </summary>
        /// <param name="tuvanIds"></param>
        /// <returns></returns>
        Task<ActionResult> DeleteTuvan(int[] tuvanIds);

        /// <summary>
        /// Lấy d/s tư vấn của khách hàng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<TuvanModel>> GetTuvans(TuvanFilterModel filter);

        /// <summary>
        /// Lấy d/s tư vấn của khách hàng
        /// </summary>
        /// <param name="khachhangId"></param>
        /// <returns></returns>
        Task<List<KhachNhucaugiaModel>> GetNhucauGias(int khachhangId);

        /// <summary>
        /// Lấy danh sách báo giá
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<BaogiaModel>> GetBaogias(TuvanFilterModel filter);

        /// <summary>
        /// Lấy chi tiết báo giá
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BaogiaDetailModel> GetBaogiaDetail(int id);

        /// <summary>
        /// Ghi lại báo giá sản phẩm
        /// </summary>
        /// <param name="baogiaId"></param>
        /// <param name="sanphamId"></param>
        /// <param name="gia"></param>
        /// <param name="soluong"></param>
        /// <returns></returns>
        Task<ActionResult> SaveBaogia(BaogiaDetailModel baogia);
    }
}


