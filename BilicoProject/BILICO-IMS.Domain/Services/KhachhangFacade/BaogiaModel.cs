﻿using System;

namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class BaogiaModel
    {
        public int BaogiaId { get; set; }
        public int KhachhangId { get; set; }
        public string NoidungBaogia { get; set; }
        public string PhanhoiKhachhang { get; set; }
        public int Nguoitao { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public Nullable<int> Nguoicapnhat { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
    }
}