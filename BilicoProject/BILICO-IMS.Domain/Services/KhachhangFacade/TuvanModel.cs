
using System;
namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class TuvanModel
    {
        public Int32 Id { set; get; }
        public Int32 KhachhangId { set; get; }
        public Nullable<Int32> SanphamTuvanId { set; get; }
        public String NoidungTuvan { set; get; }
        public DateTime Ngaytao { set; get; }
        public Int32 Nguoitao { set; get; }
        public TinhtrangTuvan Tinhtrang { set; get; }

        public bool? PhanXuly { get; set; }
        public int? NguoiXulyId { get; set; }
        public string NoidungGiaoviec { get; set; }
        public DateTime? HanXuly { get; set; }
        public DateTime? HanXulyTime { get; set; }
    }

    public enum TinhtrangTuvan
    {
        Hoanthanh = 1,

        Chuahoanthanh = 0
    }
}



