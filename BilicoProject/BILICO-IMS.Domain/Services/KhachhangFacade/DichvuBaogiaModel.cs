﻿namespace BILICO.IMS.Domain.Services.KhachhangFacade
{
    public class DichvuBaogiaModel
    {
        public int Id { get; set; }
        public int BaogiaId { get; set; }
        public string Dichvu { get; set; }
        public int Soluong { get; set; }
        public decimal Gia { get; set; }
        public decimal Thanhtien { get; set; }
    }
}