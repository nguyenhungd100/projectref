﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.ThongkeFacade
{
    public interface IThongkeService
    {
        /// <summary>
        /// Thống kê khách hàng theo các tiêu chí
        /// </summary>
        /// <param name="criteriaModel"></param>
        /// <returns></returns>
        ThongkeKhachhangModel ThongkeKhachhang(KhachhangCriteriaModel criteriaModel);

        /// <summary>
        /// Thống kê lợi nhuận
        /// </summary>
        /// <param name="criteriaModel"></param>
        /// <returns></returns>
        ThongkeLoinhuanModel ThongkeLoinhuan(LoinhuanCriteriaModel criteriaModel);

        /// <summary>
        /// Thống kê thi công
        /// </summary>
        /// <param name="criteriaModel"></param>
        /// <returns></returns>
        List<ThongkeThicongModel> ThongkeThicong(ThicongCriteriaModel criteriaModel);

        /// <summary>
        /// Thống kê chi phí
        /// </summary>
        /// <param name="criteriaModel"></param>
        /// <returns></returns>
        List<ThongkeChiphiModel> ThongkeChiphi(ChiphiCriteriaModel criteriaModel);

        /// <summary>
        /// Thống kê khu vực
        /// </summary>
        /// <param name="criteriaModel"></param>
        /// <returns></returns>
        List<ThongkeKhuvucModel> ThongkeKhuvuc(KhuvucCriteriaModel criteriaModel);

        /// <summary>
        /// Thống kê tiến độ
        /// </summary>
        /// <param name="criteriaModel"></param>
        /// <returns></returns>
        ThongkeTiendoModel ThongkeTiendo(TiendoCriteriaModel criteriaModel);

        /// <summary>
        /// Thống kê lợi nhuận chi tiết
        /// </summary>
        /// <param name="criteriaModel"></param>
        /// <returns></returns>
        List<ThongkeLoinhuanChitietModel> ThongkeLoinhuanChitiet(LoinhuanCriteriaModel criteriaModel);
    }
}
