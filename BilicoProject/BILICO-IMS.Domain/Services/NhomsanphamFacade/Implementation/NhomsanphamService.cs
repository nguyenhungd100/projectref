
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Linq.Expressions;

namespace BILICO.IMS.Domain.Services.NhomsanphamFacade.Implementation
{
    public class NhomsanphamService : INhomsanphamService
    {
        IEntityRepository<Nhomsanpham, DomainContext> _nhomsanphamRepository = new EntityRepository<Nhomsanpham, DomainContext>();

        public async Task<ActionResult> Add(NhomsanphamModel nhomsanpham)
        {
            //validate
            if (nhomsanpham == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhomsanpham không để null"
                };

            //add to storage
            var entity = nhomsanpham.CloneToModel<NhomsanphamModel, Nhomsanpham>();
            var res = await _nhomsanphamRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _nhomsanphamRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<NhomsanphamModel> GetById(int nhomsanphamId)
        {
            var entity = await _nhomsanphamRepository.GetByIdAsync(nhomsanphamId);
            return entity.CloneToModel<Nhomsanpham, NhomsanphamModel>();
        }

        public async Task<List<NhomsanphamModel>> List(FilterModel filter)
        {
            var res = await _nhomsanphamRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenNhomsanpham.Contains(filter.Search) : true), c => c.Id, filter.Paging);

            return res.CloneToListModels<Nhomsanpham, NhomsanphamModel>();
        }

        public async Task<NhomsanphamModel> SingleAsync(Expression<Func<Nhomsanpham, bool>> exp)
        {
            var entity = await _nhomsanphamRepository.SingleAsync(exp);
            return entity.CloneToModel<Nhomsanpham, NhomsanphamModel>();
        }

        public NhomsanphamModel Single(Expression<Func<Nhomsanpham, bool>> exp)
        {
            var entity = _nhomsanphamRepository.Single(exp);
            return entity.CloneToModel<Nhomsanpham, NhomsanphamModel>();
        }

        public async Task<ActionResult> Update(NhomsanphamModel nhomsanpham)
        {
            //validate
            if (nhomsanpham == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nhomsanpham không để null"
                };

            var exist = await _nhomsanphamRepository.GetByIdAsync(nhomsanpham.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nhomsanpham không tồn tại"
                };

            //update to storage
            var entity = nhomsanpham.CloneToModel<NhomsanphamModel, Nhomsanpham>();
            var res = await _nhomsanphamRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

