
using BILICO.IMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.NhomsanphamFacade
{
    public interface INhomsanphamService
    {
        Task<NhomsanphamModel> GetById(int nhomsanphamId);

        Task<ActionResult> Add(NhomsanphamModel nhomsanpham);

        Task<ActionResult> Update(NhomsanphamModel nhomsanpham);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<NhomsanphamModel>> List(FilterModel filter);

        Task<NhomsanphamModel> SingleAsync(Expression<Func<Nhomsanpham, bool>> exp);

        NhomsanphamModel Single(Expression<Func<Nhomsanpham, bool>> exp);
    }
}


