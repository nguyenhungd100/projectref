﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.NotificationFacade
{
    public interface INotificationService
    {
        Task<ActionResult> Add(NotificationModel notification);

        Task<ActionResult> Update(NotificationModel notification);

        Task<ActionResult> Delete(int userId, int[] Ids);

        Task<ActionResult> Preview(int userId, int[] Ids);

        Task<ActionResult> Receive(int userId, int[] Ids);

        Task<NotificationModel> GetById(int notificationId);

        Task<List<NotificationModel>> GetMyNotifications(int userId, FilterModel filter);
    }
}
