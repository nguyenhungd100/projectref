﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using BILICO.IMS.Framework.Utils;

namespace BILICO.IMS.Domain.Services.NotificationFacade.Implementation
{
    public class NotificationService : INotificationService
    {
        IEntityRepository<Notification, DomainContext> _notificationRepository = new EntityRepository<Notification, DomainContext>();

        public async Task<ActionResult> Add(NotificationModel notification)
        {
            //validate
            if (notification == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "notification không để null"
                };

            //add to storage
            var entity = notification.CloneToModel<NotificationModel, Notification>();
            var res = await _notificationRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int userId, int[] Ids)
        {
            var res = await _notificationRepository.FetchAsync(c => Ids.Contains(c.Id));

            foreach (var item in res)
            {
                if (String.IsNullOrEmpty(item.DeletedUserIds))
                {
                    item.DeletedUserIds = $";{userId};";
                }
                else
                {
                    if (!item.DeletedUserIds.Contains($";{userId};"))
                        item.DeletedUserIds += $"{userId};";
                }
            }

            var success = await _notificationRepository.UpdateManyAsync(res);

            return new ActionResult()
            {
                Result = success
            };
        }

        public async Task<ActionResult> Preview(int userId, int[] Ids)
        {
            var res = await _notificationRepository.FetchAsync(c => Ids.Contains(c.Id));

            foreach (var item in res)
            {
                if (String.IsNullOrEmpty(item.ViewedUserIds))
                {
                    item.ViewedUserIds = $";{userId};";
                }
                else
                {
                    if (!item.ViewedUserIds.Contains($";{userId};"))
                        item.ViewedUserIds += $"{userId};";
                }
            }

            var success = await _notificationRepository.UpdateManyAsync(res);

            return new ActionResult()
            {
                Result = success
            };
        }

        public async Task<ActionResult> Receive(int userId, int[] Ids)
        {
            var res = await _notificationRepository.FetchAsync(c => Ids.Contains(c.Id));

            foreach (var item in res)
            {
                if (String.IsNullOrEmpty(item.ReceivedUserIds))
                {
                    item.ReceivedUserIds = $";{userId};";
                }
                else
                {
                    if (!item.ReceivedUserIds.Contains($";{userId};"))
                        item.ReceivedUserIds += $"{userId};";
                }
            }

            var success = await _notificationRepository.UpdateManyAsync(res);

            return new ActionResult()
            {
                Result = success
            };
        }

        public async Task<NotificationModel> GetById(int notificationId)
        {
            var res = await _notificationRepository.GetByIdAsync(notificationId);
            return res.CloneToModel<Notification, NotificationModel>();
        }

        public async Task<List<NotificationModel>> GetMyNotifications(int userId, FilterModel filter)
        {
            var ids = $";{userId};";

            var entities = await _notificationRepository.FetchAsync(c =>
                (!String.IsNullOrEmpty(filter.Search) ? c.Title.Contains(filter.Search) || c.Message.Contains(filter.Search) : true) && (c.ReceiverIds.Contains(ids)) && (!String.IsNullOrEmpty(c.DeletedUserIds) ? !c.DeletedUserIds.Contains(ids) : true) && (c.SendDate <= DateTime.Now), c => c.CreateDate, filter.Paging);

            var result = entities.CloneToListModels<Notification, NotificationModel>();
            result.ForEach(c =>
            {
                if (!String.IsNullOrEmpty(c.ViewedUserIds) && c.ViewedUserIds.Contains(ids))
                {
                    c.IsViewed = true;
                }
                else
                {
                    c.IsViewed = false;
                }

                if (!String.IsNullOrEmpty(c.ReceivedUserIds) && c.ReceivedUserIds.Contains(ids))
                {
                    c.IsReceived = true;
                }
                else
                {
                    c.IsReceived = false;
                }
            });
            return result;
        }

        public async Task<ActionResult> Update(NotificationModel notification)
        {
            //validate
            if (notification == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Notify không để null"
                };


            var exist = await _notificationRepository.GetByIdAsync(notification.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Notify không tồn tại"
                };

            //update old password to storage and not change
            var entity = notification.CloneToModel<NotificationModel, Notification>();
            var res = await _notificationRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}
