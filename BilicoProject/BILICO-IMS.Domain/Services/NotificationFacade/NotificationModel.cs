﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.NotificationFacade
{
    public class NotificationModel
    {
        public int Id { set; get; }
        public int SenderId { set; get; }
        public string ReceiverIds { set; get; }
        public DateTime? CreateDate { set; get; }
        public DateTime? SendDate { set; get; }
        public string ReceivedUserIds { set; get; }
        public string ViewedUserIds { set; get; }
        public string DeletedUserIds { set; get; }
        public string Title { set; get; }
        public string Message { set; get; }
        public int ResourceType { set; get; }
        public int ObjectId { set; get; }

        //not for map
        public bool IsViewed
        {
            set; get;
        }

        public bool IsReceived
        {
            set; get;
        }

        public string SendDateStr
        {
            get
            {
                if (SendDate != null)
                    return SendDate.Value.ToString("dd/MM/yyyy hh:mm tt");

                return string.Empty;
            }
        }
    }
}
