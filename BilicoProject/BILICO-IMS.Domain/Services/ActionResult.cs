﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services
{
    public class ActionResult
    {
        public bool Result { set; get; }

        public string Error { set; get; }

        public object Data { set; get; }
    }
}
