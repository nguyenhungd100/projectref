
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.BaohanhFacade 		
{
	public interface IBaohanhService
	{
		Task<BaohanhModel> GetById(int baohanhId);

        Task<ActionResult> Add(BaohanhModel baohanh);

        Task<ActionResult> Update(BaohanhModel baohanh);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<BaohanhModel>> List(FilterModel filter);
	}
}


