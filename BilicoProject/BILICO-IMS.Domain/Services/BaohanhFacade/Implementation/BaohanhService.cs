
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Data.Entity;

namespace BILICO.IMS.Domain.Services.BaohanhFacade.Implementation
{
    public class BaohanhService : IBaohanhService
    {
        IEntityRepository<Baohanh, DomainContext> _baohanhRepository = new EntityRepository<Baohanh, DomainContext>();

        public async Task<ActionResult> Add(BaohanhModel baohanh)
        {
            //validate
            if (baohanh == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Baohanh không để null"
                };

            //add to storage
            baohanh.Ngaytao = DateTime.Now;
            var entity = baohanh.CloneToModel<BaohanhModel, Baohanh>();
            var res = await _baohanhRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _baohanhRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<BaohanhModel> GetById(int baohanhId)
        {
            var entity = await _baohanhRepository.GetByIdAsync(baohanhId);
            return entity.CloneToModel<Baohanh, BaohanhModel>();
        }

        public async Task<List<BaohanhModel>> List(FilterModel filter)
        {
            //var res = await _baohanhRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.GhichuHonghoc.Contains(filter.Search) : true), c => c.Id, filter.Paging);

            //return res.CloneToListModels<Baohanh, BaohanhModel>();
            using (var db = new DomainContext())
            {
                var q = (from b in db.Baohanhs
                         join s in db.Sanphams on b.SanphamId equals s.SanphamId
                         join h in db.Hopdongs on b.HopdongId equals h.HopdongId
                         join o in db.Hopdong_Sanphams on b.HopdongId equals o.HopdongId
                         where b.GhichuHonghoc.Contains(filter.Search) || s.TenSanpham.Contains(filter.Search) || h.SoHD.Contains(filter.Search) || o.MaSP.Contains(filter.Search)
                         select b).Distinct();

                filter.Paging.RowsCount = await q.CountAsync();

                var result = await q.OrderByDescending(c => c.Ngaytao).Skip(filter.Paging.StartRowIndex).Take(filter.Paging.PageSize).ToListAsync();

                var models = result.CloneToListModels<Baohanh, BaohanhModel>();

                return models;
            }
        }

        public async Task<ActionResult> Update(BaohanhModel baohanh)
        {
            //validate
            if (baohanh == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "baohanh không để null"
                };

            var exist = await _baohanhRepository.GetByIdAsync(baohanh.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "baohanh không tồn tại"
                };

            //update to storage
            var entity = baohanh.CloneToModel<BaohanhModel, Baohanh>();
            var res = await _baohanhRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

