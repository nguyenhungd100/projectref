
using System;
namespace BILICO.IMS.Domain.Services.BaohanhFacade 		
{
	public class BaohanhModel
	{
			public Int32 Id { set; get; }
			public Int32 SanphamId { set; get; }
			public Int32 HopdongId { set; get; }
			public String GhichuHonghoc { set; get; }
			public DateTime ThoigianDukienHoanthanh { set; get; }
			public Int32 KythuatXulyId { set; get; }
			public DateTime Ngaytao { set; get; }
			public Int32 NguoitaoId { set; get; }
	}
}



