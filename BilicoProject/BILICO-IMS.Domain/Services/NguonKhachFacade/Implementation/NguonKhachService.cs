
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;	
using BILICO.IMS.Domain.Data;	
using BILICO.IMS.Domain.Data.Entity;	
using BILICO.IMS.Framework.Data;	


namespace BILICO.IMS.Domain.Services.NguonKhachFacade.Implementation
{
    public class NguonKhachService : INguonKhachService
    {
        IEntityRepository<NguonKhach, DomainContext> _nguonkhachRepository = new EntityRepository<NguonKhach, DomainContext>();

        public async Task<ActionResult> Add(NguonKhachModel nguonkhach)
        {
            //validate
            if (nguonkhach == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "NguonKhach không để null"
                };

            //add to storage
            var entity = nguonkhach.CloneToModel<NguonKhachModel, NguonKhach>();
            var res = await _nguonkhachRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _nguonkhachRepository.DeleteManyAsync(c => Ids.Contains(c.NguonKhachId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<NguonKhachModel> GetById(int nguonkhachId)
        {
            var entity = await _nguonkhachRepository.GetByIdAsync(nguonkhachId);
            return entity.CloneToModel<NguonKhach, NguonKhachModel>();
        }

        public async Task<List<NguonKhachModel>> List(FilterModel filter)
        {
            var res = await _nguonkhachRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenNguon.Contains(filter.Search) : true), c => c.NguonKhachId, filter.Paging);

            return res.CloneToListModels<NguonKhach, NguonKhachModel>();
        }

        public async Task<ActionResult> Update(NguonKhachModel nguonkhach)
        {
            //validate
            if (nguonkhach == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nguonkhach không để null"
                };
				            
            var exist = await _nguonkhachRepository.GetByIdAsync(nguonkhach.NguonKhachId);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nguonkhach không tồn tại"
                };

            //update to storage
            var entity = nguonkhach.CloneToModel<NguonKhachModel, NguonKhach>();
            var res = await _nguonkhachRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

