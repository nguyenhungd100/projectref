
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.NguonKhachFacade 		
{
	public interface INguonKhachService
	{
		Task<NguonKhachModel> GetById(int nguonkhachId);

        Task<ActionResult> Add(NguonKhachModel nguonkhach);

        Task<ActionResult> Update(NguonKhachModel nguonkhach);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<NguonKhachModel>> List(FilterModel filter);
	}
}


