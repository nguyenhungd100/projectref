﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class KhachhangStatisticModel
    {
        public int Khachhang { set; get; }
        public int Namgioi { set; get; }
        public int Nugioi { set; get; }
        public int SNThangnay { set; get; }
        public int SNThangsau { set; get; }
        public int CoTKSocial { set; get; }
    }
}