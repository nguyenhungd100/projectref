﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class ProfitFilterModel : BaseFilterModel
    {
        public int? CityId { set; get; }

        public int? DistrictId { set; get; }
    }
}