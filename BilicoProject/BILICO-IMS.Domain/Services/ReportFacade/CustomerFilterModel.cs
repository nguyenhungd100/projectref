﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class CustomerFilterModel : BaseFilterModel
    {
        public int? CityId { set; get; }

        public int? DistrictId { set; get; }

        public int? NhucaugiaId { set; get; }

        public int? LoaiKhachhangId { set; get; }

        public int? SanphamQuantamId { set; get; }

        public int? NguonkhachId { set; get; }
    }
}