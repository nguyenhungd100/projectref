﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class RegionStatisticModel
    {
        public string Kythuat { set; get; }

        public int? SoLanDiTinh { set; get; }

        public int? SoLanOHaNoi { set; get; }

        public int? SoLanDiLanCanHN { set; get; }
    }
}