﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class CostStatisticModel
    {
        public string Kythuat { set; get; }

        public decimal? ChiphiAn { set; get; }

        public decimal? ChiphiNhanghi { set; get; }

        public decimal? ChiphiKhac { set; get; }

        public decimal? ChiphiDilai { set; get; }
    }
}