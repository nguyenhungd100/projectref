﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class ProfitDetailStatisticModel
    {
        public string SoHD { set; get; }

        public string Khachhang { set; get; }

        public decimal? DoanhthuDV { set; get; }



        public decimal? ChiphiNhanCong { set; get; }

        public decimal? ChiphiAn { set; get; }

        public decimal? ChiphiNhanghi { set; get; }

        public decimal? ChiphiKhac { set; get; }

        public decimal? ChiphiDilai { set; get; }

        public decimal? Loinhuan { get; set; }
    }
}