﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class ProfitStatisticModel
    {
        public int Hopdong { set; get; }

        public int Donhang { set; get; }

        public int Khachhang { set; get; }

        public decimal? Doanhso { set; get; }

        public decimal? Chiphi { set; get; }

        public decimal? Loinhuan { set; get; }
    }
}