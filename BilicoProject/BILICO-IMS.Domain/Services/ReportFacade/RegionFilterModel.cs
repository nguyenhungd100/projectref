﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class RegionFilterModel : BaseFilterModel
    {
        public int? KythuatId { set; get; }
    }
}