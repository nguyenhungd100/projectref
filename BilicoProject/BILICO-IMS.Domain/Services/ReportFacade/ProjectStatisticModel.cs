﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class ProjectStatisticModel
    {
        public int ChoThiCong { set; get; }

        public int DangThucHien { set; get; }

        public int ChoNghiemThu { set; get; }

        public int DaHoanThanh { set; get; }
    }
}