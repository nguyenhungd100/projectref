﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class ProgressFilterModel : BaseFilterModel
    {
        public int? CityId { set; get; }

        public int? DistrictId { set; get; }
    }
}