﻿namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class ConstructionStatisticModel
    {
        public string Kythuat { set; get; }

        public int? Thoigianlam { set; get; }

        public int? ThoigianDichuyen { set; get; }

        public int? ThoigianChoVT { set; get; }

        public int? ThoigianKhac { set; get; }
    }
}