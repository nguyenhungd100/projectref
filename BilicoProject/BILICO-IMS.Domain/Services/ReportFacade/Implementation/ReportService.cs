﻿using BILICO.IMS.Domain.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.ReportFacade.Implementation
{
    public class ReportService : IReportService
    {
        public async Task<KhachhangStatisticModel> ReportCustomer(CustomerFilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from c in db.Khachhangs
                        where
                        DbFunctions.DiffDays(filter.Start, c.Ngaytao) >= 0 &&
                        DbFunctions.DiffDays(c.Ngaytao, filter.End) >= 0
                        select c;

                if (filter.CityId != null)
                {
                    q = q.Where(c => c.ThanhphoId == filter.CityId);
                }

                if (filter.DistrictId != null)
                {
                    q = q.Where(c => c.QuanhuyenId == filter.DistrictId);
                }

                if (filter.LoaiKhachhangId != null)
                {
                    q = q.Where(c => c.LoaiKHId == filter.LoaiKhachhangId);
                }

                if (filter.NguonkhachId != null)
                {
                    q = q.Where(c => c.NguonkhachId == filter.NguonkhachId);
                }

                if (filter.NhucaugiaId != null)
                {
                    q = (from c in q
                         join n in db.Khachhang_Nhucaugias on c.KhachhangId equals n.KhachhangId
                         where n.NhucaugiaId == filter.NhucaugiaId
                         select c).Distinct();
                }

                if (filter.SanphamQuantamId != null)
                {
                    q = (from c in q
                         join n in db.Khachhang_SanphamQuantams on c.KhachhangId equals n.KhachhangId
                         where n.SanphamId == filter.SanphamQuantamId
                         select c).Distinct();
                }

                var result = new KhachhangStatisticModel();

                result.CoTKSocial = await q.Where(c => !String.IsNullOrEmpty(c.TKFacebook)).CountAsync();
                result.Khachhang = await q.CountAsync();
                result.Namgioi = await q.Where(c => c.Gioitinh == true).CountAsync();
                result.Nugioi = await q.Where(c => c.Gioitinh == false || c.Gioitinh == null).CountAsync();

                var start = FirstDayOfMonth(DateTime.Now);
                var end = LastDayOfMonth(DateTime.Now);

                result.SNThangnay = await q.Where(c =>
                         DbFunctions.DiffDays(start, c.Ngaysinh) >= 0 &&
                         DbFunctions.DiffDays(c.Ngaysinh, end) >= 0).CountAsync();

                start = FirstDayOfMonth(end.AddDays(10));
                end = LastDayOfMonth(end.AddDays(10));

                result.SNThangsau = await q.Where(c =>
                         DbFunctions.DiffDays(start, c.Ngaysinh) >= 0 &&
                         DbFunctions.DiffDays(c.Ngaysinh, end) >= 0).CountAsync();

                return result;
            }
        }

        private DateTime FirstDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }

        public DateTime LastDayOfMonth(DateTime dateTime)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public async Task<ProfitStatisticModel> ReportTotalProfitAsync(ProfitFilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from c in db.Hopdongs
                        where
                        DbFunctions.DiffDays(filter.Start, c.Ngaytao) >= 0 &&
                        DbFunctions.DiffDays(c.Ngaytao, filter.End) >= 0
                        select c;

                if (filter.CityId != null)
                {
                    q = (from h in q
                         join c in db.Khachhangs on h.KhachhangId equals c.KhachhangId
                         where c.ThanhphoId == filter.CityId
                         select h).Distinct();
                }

                if (filter.DistrictId != null)
                {
                    q = (from h in q
                         join c in db.Khachhangs on h.KhachhangId equals c.KhachhangId
                         where c.QuanhuyenId == filter.DistrictId
                         select h).Distinct();
                }

                var cp = from h in q
                         join c in db.Hopdong_Quyettoan_Chiphis on h.HopdongId equals c.HopdongId
                         select c;

                var result = new ProfitStatisticModel();

                result.Hopdong = await q.Where(c => c.Loai == HopdongFacade.LoaiHopdong.HopdongDichvu).CountAsync();
                result.Khachhang = await q.Select(c => c.KhachhangId).Distinct().CountAsync();
                result.Donhang = await q.Where(c => c.Loai == HopdongFacade.LoaiHopdong.Donhang).CountAsync();
                result.Doanhso = await q.SumAsync(c => (decimal?)c.Trigia);
                result.Chiphi = await cp.SumAsync(c => (decimal?)c.Sotien);
                result.Loinhuan = result.Doanhso - result.Chiphi;

                return result;
            }
        }

        public async Task<List<ConstructionStatisticModel>> ReportConstruction(ConstructionFilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from c in db.Hopdong_Nhatkythicongs
                        where
                        DbFunctions.DiffDays(filter.Start, c.Ngaybaocao) >= 0 &&
                        DbFunctions.DiffDays(c.Ngaybaocao, filter.End) >= 0
                        select c;

                if (filter.KythuatId != null)
                {
                    q = q.Where(c => c.NguoibaocaoId == filter.KythuatId);
                }

                var result = new List<ConstructionStatisticModel>();

                var ns = await (from n in q
                                join u in db.Taikhoans on n.NguoibaocaoId equals u.TaikhoanId
                                select u).Distinct().ToListAsync();

                ns.ForEach(c =>
                {
                    var kythuat = new ConstructionStatisticModel();
                    kythuat.Kythuat = c.Hovaten;
                    kythuat.Thoigianlam = q.Where(k => k.NguoibaocaoId == c.TaikhoanId && k.LoaiCongviec == HopdongFacade.LoaiBaocaoCongviec.Thoi_gian_thi_cong).Sum(k => (int?)k.ThoigianThuchien);
                    kythuat.ThoigianDichuyen = q.Where(k => k.NguoibaocaoId == c.TaikhoanId && k.LoaiCongviec == HopdongFacade.LoaiBaocaoCongviec.Thoi_gian_di_xe).Sum(k => (int?)k.ThoigianThuchien);
                    kythuat.ThoigianChoVT = q.Where(k => k.NguoibaocaoId == c.TaikhoanId && k.LoaiCongviec == HopdongFacade.LoaiBaocaoCongviec.Thoi_gian_cho_vat_tu).Sum(k => (int?)k.ThoigianThuchien);
                    kythuat.ThoigianKhac = q.Where(k => k.NguoibaocaoId == c.TaikhoanId && k.LoaiCongviec == HopdongFacade.LoaiBaocaoCongviec.Khac).Sum(k => (int?)k.ThoigianThuchien);

                    result.Add(kythuat);
                });

                return result;
            }
        }

        public async Task<List<CostStatisticModel>> ReportCost(CostFilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from h in db.Hopdong_Quyettoans
                        join c in db.Hopdong_Quyettoan_Chiphis on h.Id equals c.QuyettoanId
                        where
                        DbFunctions.DiffDays(filter.Start, h.NgayQuyettoan) >= 0 &&
                        DbFunctions.DiffDays(h.NgayQuyettoan, filter.End) >= 0 &&
                        (filter.KythuatId != null ? h.NguoiQuyettoanId == filter.KythuatId : true)
                        select new { CP = c, QT = h };

                var result = new List<CostStatisticModel>();

                var ns = await (from n in q
                                join u in db.Taikhoans on n.QT.NguoiQuyettoanId equals u.TaikhoanId
                                select u).Distinct().ToListAsync();

                ns.ForEach(c =>
                {
                    var kythuat = new CostStatisticModel();

                    kythuat.Kythuat = c.Hovaten;
                    kythuat.ChiphiAn = q.Where(k => k.QT.NguoiQuyettoanId == c.TaikhoanId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.An_uong).Sum(k => (int?)k.CP.Sotien);
                    kythuat.ChiphiDilai = q.Where(k => k.QT.NguoiQuyettoanId == c.TaikhoanId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.Di_lai).Sum(k => (int?)k.CP.Sotien);
                    kythuat.ChiphiNhanghi = q.Where(k => k.QT.NguoiQuyettoanId == c.TaikhoanId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.Nha_nghi).Sum(k => (int?)k.CP.Sotien);
                    kythuat.ChiphiKhac = q.Where(k => k.QT.NguoiQuyettoanId == c.TaikhoanId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.Khac).Sum(k => (int?)k.CP.Sotien);

                    result.Add(kythuat);
                });

                return result;
            }
        }

        public async Task<List<RegionStatisticModel>> ReportRegion(RegionFilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from c in db.Hopdong_Nhatkythicongs
                        where
                        DbFunctions.DiffDays(filter.Start, c.Ngaybaocao) >= 0 &&
                        DbFunctions.DiffDays(c.Ngaybaocao, filter.End) >= 0
                        select c;

                if (filter.KythuatId != null)
                {
                    q = q.Where(c => c.NguoibaocaoId == filter.KythuatId);
                }

                var result = new List<RegionStatisticModel>();

                var ns = await (from n in q
                                join u in db.Taikhoans on n.NguoibaocaoId equals u.TaikhoanId
                                select u).Distinct().ToListAsync();

                ns.ForEach(c =>
                {
                    var kythuat = new RegionStatisticModel();
                    kythuat.Kythuat = c.Hovaten;
                    kythuat.SoLanDiTinh = q.Where(k => k.NguoibaocaoId == c.TaikhoanId && k.LoaiCongviec == HopdongFacade.LoaiBaocaoCongviec.Di_tinh).Sum(k => (int?)k.ThoigianThuchien);
                    result.Add(kythuat);
                });

                return result;
            }
        }

        public async Task<ProjectStatisticModel> ReportProgress(ProgressFilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from c in db.Hopdongs
                        where
                        DbFunctions.DiffDays(filter.Start, c.Ngaytao) >= 0 &&
                        DbFunctions.DiffDays(c.Ngaytao, filter.End) >= 0
                        select c;

                if (filter.CityId != null)
                {
                    q = (from h in q
                         join k in db.Khachhangs on h.KhachhangId equals k.KhachhangId
                         where k.ThanhphoId == filter.CityId
                         select h).Distinct();
                }

                if (filter.DistrictId != null)
                {
                    q = (from h in q
                         join k in db.Khachhangs on h.KhachhangId equals k.KhachhangId
                         where k.QuanhuyenId == filter.DistrictId
                         select h).Distinct();
                }

                var result = new ProjectStatisticModel();

                result.ChoNghiemThu = await q.Where(c => c.Trangthai == HopdongFacade.OrderStatus.Completed).CountAsync();
                result.ChoThiCong = await q.Where(c => c.Trangthai == HopdongFacade.OrderStatus.Pending).CountAsync();
                result.DangThucHien = await q.Where(c => c.Trangthai == HopdongFacade.OrderStatus.InProgress).CountAsync();

                return result;
            }
        }

        public async Task<List<ProfitDetailStatisticModel>> ReportProfitDetail(ProfitFilterModel filter)
        {
            using (var db = new DomainContext())
            {
                var q = from h in db.Hopdongs
                        join k in db.Khachhangs on h.KhachhangId equals k.KhachhangId
                        where
                        DbFunctions.DiffDays(filter.Start, h.Ngaytao) >= 0 &&
                        DbFunctions.DiffDays(h.Ngaytao, filter.End) >= 0
                        select new { HD = h, KH = k };

                var qt = from h in db.Hopdong_Quyettoans
                         join c in db.Hopdong_Quyettoan_Chiphis on h.Id equals c.QuyettoanId
                         select new { CP = c, QT = h };

                var result = new List<ProfitDetailStatisticModel>();

                var hd = await q.ToListAsync();
                hd.ForEach(c =>
                {
                    var hopdong = new ProfitDetailStatisticModel();

                    hopdong.SoHD = c.HD.SoHD;

                    hopdong.Khachhang = c.KH.Hoten;

                    hopdong.DoanhthuDV = db.Hopdong_Dichvus.Sum(k => (decimal?)k.Thanhtien);

                    hopdong.ChiphiAn = qt.Where(k => k.QT.HopdongId == c.HD.HopdongId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.An_uong).Sum(k => (int?)k.CP.Sotien);

                    hopdong.ChiphiNhanCong = qt.Where(k => k.QT.HopdongId == c.HD.HopdongId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.Nhan_cong).Sum(k => (int?)k.CP.Sotien);

                    hopdong.ChiphiDilai = qt.Where(k => k.QT.HopdongId == c.HD.HopdongId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.Di_lai).Sum(k => (int?)k.CP.Sotien);

                    hopdong.ChiphiNhanghi = qt.Where(k => k.QT.HopdongId == c.HD.HopdongId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.Nha_nghi).Sum(k => (int?)k.CP.Sotien);

                    hopdong.ChiphiKhac = qt.Where(k => k.QT.HopdongId == c.HD.HopdongId && k.CP.LoaiChiphi == HopdongFacade.LoaiChiphi.Khac).Sum(k => (int?)k.CP.Sotien);

                    hopdong.Loinhuan = hopdong.DoanhthuDV - (hopdong.ChiphiAn + hopdong.ChiphiNhanCong + hopdong.ChiphiDilai + hopdong.ChiphiNhanghi + hopdong.ChiphiKhac);

                    result.Add(hopdong);
                });

                return result;
            }
        }
    }
}
