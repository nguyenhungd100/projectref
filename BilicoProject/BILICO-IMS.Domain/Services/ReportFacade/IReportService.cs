﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public interface IReportService
    {
        /// <summary>
        /// Thống kê khách hàng
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<KhachhangStatisticModel> ReportCustomer(CustomerFilterModel filter);

        /// <summary>
        /// Thống kê tổng hợp lợi nhuân
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<ProfitStatisticModel> ReportTotalProfitAsync(ProfitFilterModel filter);

        /// <summary>
        /// Báo cáo thi công
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<ConstructionStatisticModel>> ReportConstruction(ConstructionFilterModel filter);

        /// <summary>
        /// Báo cáo chi phí
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<CostStatisticModel>> ReportCost(CostFilterModel filter);

        /// <summary>
        /// Báo cáo khu vực
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<RegionStatisticModel>> ReportRegion(RegionFilterModel filter);

        /// <summary>
        /// Báo cáo tiến độ
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<ProjectStatisticModel> ReportProgress(ProgressFilterModel filter);

        /// <summary>
        /// Báo cáo tổng hợp lợi nhuận
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<ProfitDetailStatisticModel>> ReportProfitDetail(ProfitFilterModel filter);
    }
}
