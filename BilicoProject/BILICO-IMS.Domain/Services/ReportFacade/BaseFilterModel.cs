﻿using BILICO.IMS.Framework.Data;
using System;

namespace BILICO.IMS.Domain.Services.ReportFacade
{
    public class BaseFilterModel
    {
        public DateTime Start { set; get; }

        public DateTime End { set; get; }

        public PagingInfo Paging { set; get; }
    }
}