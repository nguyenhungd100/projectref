
using System;
namespace BILICO.IMS.Domain.Services.TaikhoanFacade
{
    public class TaikhoanModel
    {
        public Int32 TaikhoanId { set; get; }
        public string Avatar { get; set; }
        public String Email { set; get; }
        public String Matkhau { set; get; }
        public Int32 PhongbanId { set; get; }
        public String Hovaten { set; get; }
        public String Mobile { set; get; }
        public String Diachi { set; get; }
        public DateTime Ngaytao { set; get; }
        public Nullable<DateTime> Ngaycapnhat { set; get; }
        public bool Tinhtrang { set; get; }
        public Guid UserModifiedTimeStamp { set; get; }
        public string Roles { get; set; }
    }
}



