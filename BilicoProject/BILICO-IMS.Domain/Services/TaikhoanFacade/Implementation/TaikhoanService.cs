
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Linq.Expressions;

namespace BILICO.IMS.Domain.Services.TaikhoanFacade.Implementation
{
    public class TaikhoanService : ITaikhoanService
    {
        IEntityRepository<Taikhoan, DomainContext> _taikhoanRepository = new EntityRepository<Taikhoan, DomainContext>();

        public async Task<ActionResult> Add(TaikhoanModel taikhoan)
        {
            //validate
            if (taikhoan == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Taikhoan không để null"
                };

            if (taikhoan.PhongbanId == 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có phòng ban"
                };


            var exist = await _taikhoanRepository.SingleAsync(c => c.Email == taikhoan.Email);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Email đã tồn tại"
                };
            }

            //add to storage
            taikhoan.Matkhau = EncryptUtil.EncryptMD5(taikhoan.Matkhau);
            taikhoan.Ngaytao = DateTime.Now;
            taikhoan.UserModifiedTimeStamp = Guid.NewGuid();
            var entity = taikhoan.CloneToModel<TaikhoanModel, Taikhoan>();
            var res = await _taikhoanRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _taikhoanRepository.DeleteManyAsync(c => Ids.Contains(c.TaikhoanId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<TaikhoanModel> GetByIdAsync(int taikhoanId)
        {
            var entity = await _taikhoanRepository.GetByIdAsync(taikhoanId);
            entity.Matkhau = string.Empty;
            return entity.CloneToModel<Taikhoan, TaikhoanModel>();
        }

        /// <summary>
        /// Sử dụng cho cả module chứng thực
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<TaikhoanModel> SingleAsync(Expression<Func<Taikhoan, bool>> query)
        {
            var entity = await _taikhoanRepository.SingleAsync(query);
            return entity.CloneToModel<Taikhoan, TaikhoanModel>();
        }

        public TaikhoanModel GetById(int taikhoanId)
        {
            var entity = _taikhoanRepository.GetById(taikhoanId);
            entity.Matkhau = string.Empty;
            return entity.CloneToModel<Taikhoan, TaikhoanModel>();
        }

        public async Task<List<TaikhoanModel>> List(FilterModel filter)
        {
            var roleIds = $";{filter.RoleId};";

            var res = await _taikhoanRepository.FetchAsync(c =>
            (!String.IsNullOrEmpty(filter.Search) ? c.Diachi.Contains(filter.Search) || c.Email.Contains(filter.Search) || c.Hovaten.Contains(filter.Search) || c.Mobile.Contains(filter.Search) : true) &&
            (filter.PhongbanId != null ? c.PhongbanId == filter.PhongbanId : true) &&
            (filter.RoleId != null ? c.Roles.Contains(roleIds) : true)
            , c => c.TaikhoanId, filter.Paging);

            res.ForEach(c =>
            {
                c.Matkhau = string.Empty;
            });

            return res.CloneToListModels<Taikhoan, TaikhoanModel>();
        }

        public async Task<ActionResult> Update(TaikhoanModel taikhoan)
        {
            return await Update(taikhoan, true);
        }

        public async Task<ActionResult> Update(TaikhoanModel taikhoan, bool forceChangeTimestamp)
        {
            //validate
            if (taikhoan == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "taikhoan không để null"
                };

            if (taikhoan.PhongbanId == 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa có phòng ban"
                };

            var exist = await _taikhoanRepository.SingleAsync(c => c.Email == taikhoan.Email && c.TaikhoanId != taikhoan.TaikhoanId);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Email đã tồn tại"
                };
            }

            exist = await _taikhoanRepository.GetByIdAsync(taikhoan.TaikhoanId);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "taikhoan không tồn tại"
                };

            //update old password to storage and not change
            taikhoan.Matkhau = exist.Matkhau;
            taikhoan.Ngaycapnhat = DateTime.Now;
            if (forceChangeTimestamp == true)
                taikhoan.UserModifiedTimeStamp = Guid.NewGuid();

            var entity = taikhoan.CloneToModel<TaikhoanModel, Taikhoan>();
            var res = await _taikhoanRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }

        public async Task<ActionResult> ToggleLock(int staffId)
        {
            var res = await _taikhoanRepository.GetByIdAsync(staffId);
            if (res != null)
            {
                res.Tinhtrang = !res.Tinhtrang;
                await _taikhoanRepository.UpdateAsync(res);
                return new ActionResult() { Result = true };
            }
            return new ActionResult() { Result = false, Error = "User không tồn tại" };
        }

        public async Task<ActionResult> ChangePassword(int staffId, string oldPassword, string newPassword)
        {
            //validate
            var user = await _taikhoanRepository.GetByIdAsync(staffId);

            if (user == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "User không tồn tại"
                };

            if (user.Matkhau != EncryptUtil.EncryptMD5(oldPassword))
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Mật khẩu cũ không đúng"
                };
            }

            //update old password to storage and not change
            user.Matkhau = EncryptUtil.EncryptMD5(newPassword);
            user.Ngaycapnhat = DateTime.Now;
            user.UserModifiedTimeStamp = Guid.NewGuid();
            var res = await _taikhoanRepository.UpdateAsync(user);

            return new ActionResult()
            {
                Result = res
            };
        }
        public async Task<ActionResult> ResetPassword(int staffId, string newPassword)
        {
            if (String.IsNullOrEmpty(newPassword))
                return new ActionResult()
                {
                    Result = false,
                    Error = "Mật khẩu không hợp lệ"
                };

            //validate
            var staff = await _taikhoanRepository.GetByIdAsync(staffId);

            if (staff == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "User không tồn tại"
                };

            //update old password to storage and not change
            staff.Matkhau = EncryptUtil.EncryptMD5(newPassword);
            staff.Ngaycapnhat = DateTime.Now;
            staff.UserModifiedTimeStamp = Guid.NewGuid();
            var res = await _taikhoanRepository.UpdateAsync(staff);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

