
using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.TaikhoanFacade 		
{
	public class FilterModel
	{
		public string Search { set; get; }

        public int? PhongbanId { set; get; }

        public int? RoleId { set; get; }

        public PagingInfo Paging { set; get; }
	}
}


