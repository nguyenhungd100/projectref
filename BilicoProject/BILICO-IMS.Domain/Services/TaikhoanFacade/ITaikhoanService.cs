﻿
using BILICO.IMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.TaikhoanFacade 		
{
	public interface ITaikhoanService
	{
        /// <summary>
        /// Cập nhật tài khoản co thay đổi time stamp
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="forceChangeTimestamp"></param>
        /// <returns></returns>
        Task<ActionResult> Update(TaikhoanModel staff, bool forceChangeTimestamp);

        Task<TaikhoanModel> SingleAsync(Expression<Func<Taikhoan, bool>> query);

        Task<TaikhoanModel> GetByIdAsync(int taikhoanId);

        TaikhoanModel GetById(int taikhoanId);

        Task<ActionResult> Add(TaikhoanModel taikhoan);

        Task<ActionResult> Update(TaikhoanModel taikhoan);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<TaikhoanModel>> List(FilterModel filter);
        
        /// <summary>
        /// Reset mật khẩu theo user 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        Task<ActionResult> ResetPassword(int userId, string newPassword);

        /// <summary>
        /// Khóa hoặc mở khóa tài khoản
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        Task<ActionResult> ToggleLock(int staffId);

        /// <summary>
        /// Đổi mật khẩu user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        Task<ActionResult> ChangePassword(int staffId, string oldPassword, string newPassword);
    }
}


