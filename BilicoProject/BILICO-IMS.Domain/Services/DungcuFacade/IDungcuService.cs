
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.DungcuFacade 		
{
	public interface IDungcuService
	{
		Task<DungcuModel> GetById(int dungcuId);

        Task<ActionResult> Add(DungcuModel dungcu);

        Task<ActionResult> Update(DungcuModel dungcu);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<DungcuModel>> List(FilterModel filter);
	}
}


