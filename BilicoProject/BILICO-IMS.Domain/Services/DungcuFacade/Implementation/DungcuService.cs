
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;


namespace BILICO.IMS.Domain.Services.DungcuFacade.Implementation
{
    public class DungcuService : IDungcuService
    {
        IEntityRepository<Dungcu, DomainContext> _dungcuRepository = new EntityRepository<Dungcu, DomainContext>();

        public async Task<ActionResult> Add(DungcuModel dungcu)
        {
            //validate
            if (dungcu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Dungcu không để null"
                };

            //add to storage
            var entity = dungcu.CloneToModel<DungcuModel, Dungcu>();
            var res = await _dungcuRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _dungcuRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<DungcuModel> GetById(int dungcuId)
        {
            var entity = await _dungcuRepository.GetByIdAsync(dungcuId);
            return entity.CloneToModel<Dungcu, DungcuModel>();
        }

        public async Task<List<DungcuModel>> List(FilterModel filter)
        {
            var res = await _dungcuRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenDungcu.Contains(filter.Search) || c.Ghichu.Contains(filter.Search) : true), c => c.Id, filter.Paging);

            return res.CloneToListModels<Dungcu, DungcuModel>();
        }

        public async Task<ActionResult> Update(DungcuModel dungcu)
        {
            //validate
            if (dungcu == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "dungcu không để null"
                };

            var exist = await _dungcuRepository.GetByIdAsync(dungcu.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "dungcu không tồn tại"
                };

            //update to storage
            var entity = dungcu.CloneToModel<DungcuModel, Dungcu>();
            var res = await _dungcuRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

