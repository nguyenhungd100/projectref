
using System;
namespace BILICO.IMS.Domain.Services.DungcuFacade 		
{
	public class DungcuModel
	{
			public Int32 Id { set; get; }
			public String TenDungcu { set; get; }
			public DateTime Ngaygiao { set; get; }
			public DateTime Ngaynhan { set; get; }
			public Int32 NguoigiaoId { set; get; }
			public Int32 NguoinhanId { set; get; }
			public Decimal Dongia { set; get; }
			public Int32 Soluong { set; get; }
			public Int32 NguoimuaId { set; get; }
			public String Ghichu { set; get; }
	}
}



