
using System;
namespace BILICO.IMS.Domain.Services.TraodoiCongviecFacade
{
    public class TraodoiCongviecModel
    {
        public Int32 Id { set; get; }
        public Int32 CongviecId { set; get; }
        public String NoidungThaoluan { set; get; }
        public Int32 NguoitaoId { set; get; }
        public DateTime Ngaytao { set; get; }
        public Nullable<Int32> ParentId { set; get; }
    }
}



