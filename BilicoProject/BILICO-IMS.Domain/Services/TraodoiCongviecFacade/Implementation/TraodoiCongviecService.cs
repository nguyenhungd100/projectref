
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;


namespace BILICO.IMS.Domain.Services.TraodoiCongviecFacade.Implementation
{
    public class TraodoiCongviecService : ITraodoiCongviecService
    {
        IEntityRepository<TraodoiCongviec, DomainContext> _traodoicongviecRepository = new EntityRepository<TraodoiCongviec, DomainContext>();

        public async Task<ActionResult> Add(TraodoiCongviecModel traodoicongviec)
        {
            //validate
            if (traodoicongviec == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "TraodoiCongviec không để null"
                };

            //add to storage
            traodoicongviec.Ngaytao = DateTime.Now;
            var entity = traodoicongviec.CloneToModel<TraodoiCongviecModel, TraodoiCongviec>();
            var res = await _traodoicongviecRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _traodoicongviecRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<TraodoiCongviecModel> GetById(int traodoicongviecId)
        {
            var entity = await _traodoicongviecRepository.GetByIdAsync(traodoicongviecId);
            return entity.CloneToModel<TraodoiCongviec, TraodoiCongviecModel>();
        }

        public async Task<List<TraodoiCongviecModel>> List(FilterModel filter)
        {
            var res = await _traodoicongviecRepository.FetchAsync(c =>
            (!String.IsNullOrEmpty(filter.Search) ? c.NoidungThaoluan.Contains(filter.Search) : true) &&
            c.CongviecId == filter.CongviecId
            , c => c.Id, filter.Paging);

            return res.CloneToListModels<TraodoiCongviec, TraodoiCongviecModel>();
        }

        public async Task<ActionResult> Update(TraodoiCongviecModel traodoicongviec)
        {
            //validate
            if (traodoicongviec == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "traodoicongviec không để null"
                };

            var exist = await _traodoicongviecRepository.GetByIdAsync(traodoicongviec.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "traodoicongviec không tồn tại"
                };

            //update to storage
            var entity = traodoicongviec.CloneToModel<TraodoiCongviecModel, TraodoiCongviec>();
            var res = await _traodoicongviecRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

