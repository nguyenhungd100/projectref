
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.TraodoiCongviecFacade 		
{
	public interface ITraodoiCongviecService
	{
		Task<TraodoiCongviecModel> GetById(int traodoicongviecId);

        Task<ActionResult> Add(TraodoiCongviecModel traodoicongviec);

        Task<ActionResult> Update(TraodoiCongviecModel traodoicongviec);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<TraodoiCongviecModel>> List(FilterModel filter);
	}
}


