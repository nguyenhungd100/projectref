
using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.TraodoiCongviecFacade
{
    public class FilterModel
    {
        public string Search { set; get; }

        public int CongviecId { set; get; }

        public PagingInfo Paging { set; get; }
    }
}


