
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;
using System.Linq.Expressions;

namespace BILICO.IMS.Domain.Services.NhucaugiaFacade.Implementation
{
    public class NhucaugiaService : INhucaugiaService
    {
        IEntityRepository<Nhucaugia, DomainContext> _nhucaugiaRepository = new EntityRepository<Nhucaugia, DomainContext>();

        public async Task<ActionResult> Add(NhucaugiaModel nhucaugia)
        {
            //validate
            if (nhucaugia == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Nhucaugia không để null"
                };

            //add to storage
            var entity = nhucaugia.CloneToModel<NhucaugiaModel, Nhucaugia>();
            var res = await _nhucaugiaRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _nhucaugiaRepository.DeleteManyAsync(c => Ids.Contains(c.NhucauId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<NhucaugiaModel> GetById(int nhucaugiaId)
        {
            var entity = await _nhucaugiaRepository.GetByIdAsync(nhucaugiaId);
            return entity.CloneToModel<Nhucaugia, NhucaugiaModel>();
        }

        public async Task<List<NhucaugiaModel>> List(FilterModel filter)
        {
            var res = await _nhucaugiaRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenNhucau.Contains(filter.Search) : true), c => c.NhucauId, filter.Paging);

            return res.CloneToListModels<Nhucaugia, NhucaugiaModel>();
        }

        public async Task<NhucaugiaModel> SingleAsync(Expression<Func<Nhucaugia, bool>> exp)
        {
            var entity = await _nhucaugiaRepository.SingleAsync(exp);
            return entity.CloneToModel<Nhucaugia, NhucaugiaModel>();
        }

        public NhucaugiaModel Single(Expression<Func<Nhucaugia, bool>> exp)
        {
            var entity = _nhucaugiaRepository.Single(exp);
            return entity.CloneToModel<Nhucaugia, NhucaugiaModel>();
        }

        public async Task<ActionResult> Update(NhucaugiaModel nhucaugia)
        {
            //validate
            if (nhucaugia == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nhucaugia không để null"
                };

            var exist = await _nhucaugiaRepository.GetByIdAsync(nhucaugia.NhucauId);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "nhucaugia không tồn tại"
                };

            //update to storage
            var entity = nhucaugia.CloneToModel<NhucaugiaModel, Nhucaugia>();
            var res = await _nhucaugiaRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

