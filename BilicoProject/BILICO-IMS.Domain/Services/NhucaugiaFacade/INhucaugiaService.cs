
using BILICO.IMS.Domain.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.NhucaugiaFacade 		
{
	public interface INhucaugiaService
	{
		Task<NhucaugiaModel> GetById(int nhucaugiaId);

        Task<ActionResult> Add(NhucaugiaModel nhucaugia);

        Task<ActionResult> Update(NhucaugiaModel nhucaugia);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<NhucaugiaModel>> List(FilterModel filter);

        Task<NhucaugiaModel> SingleAsync(Expression<Func<Nhucaugia, bool>> exp);

        NhucaugiaModel Single(Expression<Func<Nhucaugia, bool>> exp);
    }
}


