
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;


namespace BILICO.IMS.Domain.Services.ThanhphoFacade.Implementation
{
    public class ThanhphoService : IThanhphoService
    {
        IEntityRepository<Thanhpho, DomainContext> _thanhphoRepository = new EntityRepository<Thanhpho, DomainContext>();

        public async Task<ActionResult> Add(ThanhphoModel thanhpho)
        {
            //validate
            if (thanhpho == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Thanhpho không để null"
                };

            var exist = await _thanhphoRepository.SingleAsync(c => c.Title == thanhpho.Title);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Title đã tồn tại"
                };
            }

            //add to storage
            thanhpho.AddedDate = DateTime.Now;
            var entity = thanhpho.CloneToModel<ThanhphoModel, Thanhpho>();
            var res = await _thanhphoRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _thanhphoRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<ThanhphoModel> GetById(int thanhphoId)
        {
            var entity = await _thanhphoRepository.GetByIdAsync(thanhphoId);
            return entity.CloneToModel<Thanhpho, ThanhphoModel>();
        }

        public async Task<List<ThanhphoModel>> List(FilterModel filter)
        {
            var res = await _thanhphoRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Title.Contains(filter.Search) : true), c => c.Id, filter.Paging);

            return res.CloneToListModels<Thanhpho, ThanhphoModel>();
        }

        public async Task<ActionResult> Update(ThanhphoModel thanhpho)
        {
            //validate
            if (thanhpho == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "thanhpho không để null"
                };

            var exist = await _thanhphoRepository.GetByIdAsync(thanhpho.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "thanhpho không tồn tại"
                };

            exist = await _thanhphoRepository.SingleAsync(c => c.Title == thanhpho.Title && c.Id != thanhpho.Id);
            if (exist != null)
            {
                return new ActionResult()
                {
                    Result = false,
                    Error = "Title đã tồn tại"
                };
            }

            //update to storage
            var entity = thanhpho.CloneToModel<ThanhphoModel, Thanhpho>();
            var res = await _thanhphoRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

