
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.ThanhphoFacade 		
{
	public interface IThanhphoService
	{
		Task<ThanhphoModel> GetById(int thanhphoId);

        Task<ActionResult> Add(ThanhphoModel thanhpho);

        Task<ActionResult> Update(ThanhphoModel thanhpho);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<ThanhphoModel>> List(FilterModel filter);
	}
}


