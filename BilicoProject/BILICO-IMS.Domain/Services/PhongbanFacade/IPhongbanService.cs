
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.PhongbanFacade 		
{
	public interface IPhongbanService
	{
		Task<PhongbanModel> GetById(int phongbanId);

        Task<ActionResult> Add(PhongbanModel phongban);

        Task<ActionResult> Update(PhongbanModel phongban);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<PhongbanModel>> List(FilterModel filter);
	}
}


