
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;	
using BILICO.IMS.Domain.Data;	
using BILICO.IMS.Domain.Data.Entity;	
using BILICO.IMS.Framework.Data;	


namespace BILICO.IMS.Domain.Services.PhongbanFacade.Implementation
{
    public class PhongbanService : IPhongbanService
    {
        IEntityRepository<Phongban, DomainContext> _phongbanRepository = new EntityRepository<Phongban, DomainContext>();

        public async Task<ActionResult> Add(PhongbanModel phongban)
        {
            //validate
            if (phongban == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Phongban không để null"
                };

            //add to storage
            var entity = phongban.CloneToModel<PhongbanModel, Phongban>();
            var res = await _phongbanRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _phongbanRepository.DeleteManyAsync(c => Ids.Contains(c.PhongbanId));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<PhongbanModel> GetById(int phongbanId)
        {
            var entity = await _phongbanRepository.GetByIdAsync(phongbanId);
            return entity.CloneToModel<Phongban, PhongbanModel>();
        }

        public async Task<List<PhongbanModel>> List(FilterModel filter)
        {
            var res = await _phongbanRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.TenPhongban.Contains(filter.Search) : true), c => c.PhongbanId, filter.Paging);

            return res.CloneToListModels<Phongban, PhongbanModel>();
        }

        public async Task<ActionResult> Update(PhongbanModel phongban)
        {
            //validate
            if (phongban == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "phongban không để null"
                };
				            
            var exist = await _phongbanRepository.GetByIdAsync(phongban.PhongbanId);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "phongban không tồn tại"
                };

            //update to storage
            var entity = phongban.CloneToModel<PhongbanModel, Phongban>();
            var res = await _phongbanRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

