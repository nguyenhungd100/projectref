
using BILICO.IMS.Framework.Data;

namespace BILICO.IMS.Domain.Services.QuanHuyenFacade 		
{
	public class FilterModel
	{
		public string Search { set; get; }

        public int? ThanhphoId { set; get; }

        public PagingInfo Paging { set; get; }
	}
}


