
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Services.QuanHuyenFacade 		
{
	public interface IQuanHuyenService
	{
		Task<QuanHuyenModel> GetById(int quanhuyenId);

        Task<ActionResult> Add(QuanHuyenModel quanhuyen);

        Task<ActionResult> Update(QuanHuyenModel quanhuyen);

        Task<ActionResult> Delete(int[] Ids);

        Task<List<QuanHuyenModel>> List(FilterModel filter);
	}
}


