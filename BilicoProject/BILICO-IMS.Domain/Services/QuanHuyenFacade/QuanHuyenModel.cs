
using System;

namespace BILICO.IMS.Domain.Services.QuanHuyenFacade 		
{
	public class QuanHuyenModel
	{
			public Int32 Id { set; get; }
			public Nullable<Int32> ThanhphoId { set; get; }
			public DateTime AddedDate { set; get; }
			public String Title { set; get; }
			public String Description { set; get; }
			public Nullable<Int32> DisplayOrder { set; get; }
			public Boolean IsActive { set; get; }
			public String PermanLink { set; get; }
	}
}



