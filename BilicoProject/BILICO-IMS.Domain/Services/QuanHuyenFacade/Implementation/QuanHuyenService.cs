
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BILICO.IMS.Framework.Utils;
using BILICO.IMS.Domain.Data;
using BILICO.IMS.Domain.Data.Entity;
using BILICO.IMS.Framework.Data;


namespace BILICO.IMS.Domain.Services.QuanHuyenFacade.Implementation
{
    public class QuanHuyenService : IQuanHuyenService
    {
        IEntityRepository<QuanHuyen, DomainContext> _quanhuyenRepository = new EntityRepository<QuanHuyen, DomainContext>();

        public async Task<ActionResult> Add(QuanHuyenModel quanhuyen)
        {
            //validate
            if (quanhuyen == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "QuanHuyen không để null"
                };

            if (quanhuyen.ThanhphoId == null || quanhuyen.ThanhphoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn thành phố"
                };

            //add to storage
            quanhuyen.AddedDate = DateTime.Now;
            var entity = quanhuyen.CloneToModel<QuanHuyenModel, QuanHuyen>();
            var res = await _quanhuyenRepository.AddAsync(entity);

            return new ActionResult()
            {
                Result = (res != null ? true : false)
            };
        }

        public async Task<ActionResult> Delete(int[] Ids)
        {
            var res = await _quanhuyenRepository.DeleteManyAsync(c => Ids.Contains(c.Id));

            return new ActionResult()
            {
                Result = (res > 0 ? true : false)
            };
        }

        public async Task<QuanHuyenModel> GetById(int quanhuyenId)
        {
            var entity = await _quanhuyenRepository.GetByIdAsync(quanhuyenId);
            return entity.CloneToModel<QuanHuyen, QuanHuyenModel>();
        }

        public async Task<List<QuanHuyenModel>> List(FilterModel filter)
        {
            var res = await _quanhuyenRepository.FetchAsync(c => (!String.IsNullOrEmpty(filter.Search) ? c.Title.Contains(filter.Search) : true) && (filter.ThanhphoId != null ? c.ThanhphoId == filter.ThanhphoId : true), c => c.Id, filter.Paging);

            return res.CloneToListModels<QuanHuyen, QuanHuyenModel>();
        }

        public async Task<ActionResult> Update(QuanHuyenModel quanhuyen)
        {
            //validate
            if (quanhuyen == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "quanhuyen không để null"
                };

            if (quanhuyen.ThanhphoId == null || quanhuyen.ThanhphoId <= 0)
                return new ActionResult()
                {
                    Result = false,
                    Error = "Chưa chọn thành phố"
                };

            var exist = await _quanhuyenRepository.GetByIdAsync(quanhuyen.Id);
            if (exist == null)
                return new ActionResult()
                {
                    Result = false,
                    Error = "quanhuyen không tồn tại"
                };

            //update to storage
            var entity = quanhuyen.CloneToModel<QuanHuyenModel, QuanHuyen>();
            var res = await _quanhuyenRepository.UpdateAsync(entity);

            return new ActionResult()
            {
                Result = res
            };
        }
    }
}

