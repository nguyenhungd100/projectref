
namespace BILICO.IMS.Domain.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using BILICO.IMS.Domain.Data.Entity;

    public partial class DomainContext : DbContext
    {
        public DomainContext()
            : base("name=DomainContext")
        {
            Database.SetInitializer<DomainContext>(null);
        }

        public virtual DbSet<Hopdong> Hopdongs { get; set; }
        public virtual DbSet<Hopdong_Dichvu> Hopdong_Dichvus { get; set; }
        public virtual DbSet<Hopdong_Dukienchi> Hopdong_Dukienchis { get; set; }
        public virtual DbSet<Hopdong_Dukienhangcanthiet> Hopdong_Dukienhangcanthiets { get; set; }
        public virtual DbSet<Hopdong_Dukienthu> Hopdong_Dukienthus { get; set; }
        public virtual DbSet<Hopdong_Sanpham> Hopdong_Sanphams { get; set; }
        public virtual DbSet<Hopdong_Thanhtoan> Hopdong_Thanhtoans { get; set; }
        public virtual DbSet<Khachhang_Nhucaugia> Khachhang_Nhucaugias { get; set; }
        public virtual DbSet<Khachhang> Khachhangs { get; set; }
        public virtual DbSet<Khachhang_Baogia> Khachhang_Baogias { get; set; }
        public virtual DbSet<Khachhang_Baogia_ChitietDichvu> Khachhang_Baogia_ChitietDichvus { get; set; }
        public virtual DbSet<Khachhang_Baogia_ChitietSanpham> Khachhang_Baogia_ChitietSanphams { get; set; }
        public virtual DbSet<Khachhang_SanphamQuantam> Khachhang_SanphamQuantams { get; set; }
        public virtual DbSet<Khachhang_Tuvan> Khachhang_Tuvans { get; set; }
        public virtual DbSet<LoaiKhachhang> LoaiKhachhangs { get; set; }
        public virtual DbSet<NguonKhach> NguonKhachs { get; set; }
        public virtual DbSet<Nhucaugia> Nhucaugias { get; set; }
        public virtual DbSet<Phongban> Phongbans { get; set; }
        public virtual DbSet<QuanHuyen> QuanHuyens { get; set; }
        public virtual DbSet<Sanpham> Sanphams { get; set; }
        public virtual DbSet<Taikhoan> Taikhoans { get; set; }
        public virtual DbSet<Thanhpho> Thanhphos { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Nhacungcap> Nhacungcaps { get; set; }
        public virtual DbSet<Hangsanxuat> Hangsanxuats { get; set; }
        public virtual DbSet<Nhomsanpham> Nhomsanphams { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Configuration> Configurations { get; set; }
        public virtual DbSet<Sanpham_Gia> Sanpham_Gias { get; set; }
        public virtual DbSet<Baohanh> Baohanhs { get; set; }
        public virtual DbSet<Congviec> Congviecs { get; set; }
        public virtual DbSet<TraodoiCongviec> TraodoiCongviecs { get; set; }
        public virtual DbSet<Dungcu> Dungcus { get; set; }
        public virtual DbSet<Hopdong_DanhgiaNoibo> Hopdong_DanhgiaNoibos { get; set; }
        public virtual DbSet<Hopdong_Giaoviec> Hopdong_Giaoviecs { get; set; }
        public virtual DbSet<Hopdong_Kehoach> Hopdong_Kehoachs { get; set; }
        public virtual DbSet<Hopdong_Nhansu> Hopdong_Nhansus { get; set; }
        public virtual DbSet<Hopdong_Nhatkythicong> Hopdong_Nhatkythicongs { get; set; }
        public virtual DbSet<Hopdong_Phanhoi> Hopdong_Phanhois { get; set; }
        public virtual DbSet<Hopdong_Quyettoan> Hopdong_Quyettoans { get; set; }
        public virtual DbSet<Hopdong_Quyettoan_Chiphi> Hopdong_Quyettoan_Chiphis { get; set; }
        public virtual DbSet<Hopdong_Tamung> Hopdong_Tamungs { get; set; }
        public virtual DbSet<Hopdong_Suco> Hopdong_Sucos { get; set; }
        public virtual DbSet<Hopdong_TailieuDinhkem> Hopdong_TailieuDinhkems { get; set; }
        public virtual DbSet<Hopdong_TailieuDinhkem_AnhCongtrinh> Hopdong_TailieuDinhkem_AnhCongtrinhs { get; set; }
        public virtual DbSet<Hopdong_Vattu> Hopdong_Vattus { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hopdong>().ToTable("Hopdong");
            modelBuilder.Entity<Hopdong_Dichvu>().ToTable("Hopdong_Dichvu");
            modelBuilder.Entity<Hopdong_Dukienchi>().ToTable("Hopdong_Dukienchi");
            modelBuilder.Entity<Hopdong_Dukienhangcanthiet>().ToTable("Hopdong_Dukienhangcanthiet");
            modelBuilder.Entity<Hopdong_Dukienthu>().ToTable("Hopdong_Dukienthu");
            modelBuilder.Entity<Hopdong_Sanpham>().ToTable("Hopdong_Sanpham");
            modelBuilder.Entity<Hopdong_Thanhtoan>().ToTable("Hopdong_Thanhtoan");
            modelBuilder.Entity<Khachhang_Nhucaugia>().ToTable("Khachhang_Nhucaugia");
            modelBuilder.Entity<Khachhang>().ToTable("Khachhang");
            modelBuilder.Entity<Khachhang_Baogia>().ToTable("Khachhang_Baogia");
            modelBuilder.Entity<Khachhang_Baogia_ChitietDichvu>().ToTable("Khachhang_Baogia_ChitietDichvu");
            modelBuilder.Entity<Khachhang_Baogia_ChitietSanpham>().ToTable("Khachhang_Baogia_ChitietSanpham");
            modelBuilder.Entity<Khachhang_SanphamQuantam>().ToTable("Khachhang_SanphamQuantam");
            modelBuilder.Entity<Khachhang_Tuvan>().ToTable("Khachhang_Tuvan");
            modelBuilder.Entity<LoaiKhachhang>().ToTable("LoaiKhachhang");
            modelBuilder.Entity<NguonKhach>().ToTable("NguonKhach");
            modelBuilder.Entity<Nhucaugia>().ToTable("Nhucaugia");
            modelBuilder.Entity<Phongban>().ToTable("Phongban");
            modelBuilder.Entity<QuanHuyen>().ToTable("QuanHuyen");
            modelBuilder.Entity<Sanpham>().ToTable("Sanpham");
            modelBuilder.Entity<Taikhoan>().ToTable("Taikhoan");
            modelBuilder.Entity<Thanhpho>().ToTable("Thanhpho");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<Nhacungcap>().ToTable("Nhacungcap");
            modelBuilder.Entity<Hangsanxuat>().ToTable("Hangsanxuat");
            modelBuilder.Entity<Nhomsanpham>().ToTable("Nhomsanpham");
            modelBuilder.Entity<Notification>().ToTable("Notification");
            modelBuilder.Entity<Configuration>().ToTable("Configuration");
            modelBuilder.Entity<Sanpham_Gia>().ToTable("Sanpham_Giaban");
            modelBuilder.Entity<Baohanh>().ToTable("Baohanh");
            modelBuilder.Entity<Congviec>().ToTable("Congviec");
            modelBuilder.Entity<TraodoiCongviec>().ToTable("TraodoiCongviec");
            modelBuilder.Entity<Dungcu>().ToTable("Dungcu");
            modelBuilder.Entity<Hopdong_DanhgiaNoibo>().ToTable("Hopdong_DanhgiaNoibo");
            modelBuilder.Entity<Hopdong_Giaoviec>().ToTable("Hopdong_Giaoviec");
            modelBuilder.Entity<Hopdong_Kehoach>().ToTable("Hopdong_Kehoach");
            modelBuilder.Entity<Hopdong_Nhansu>().ToTable("Hopdong_Nhansu");
            modelBuilder.Entity<Hopdong_Nhatkythicong>().ToTable("Hopdong_Nhatkythicong");
            modelBuilder.Entity<Hopdong_Phanhoi>().ToTable("Hopdong_Phanhoi");
            modelBuilder.Entity<Hopdong_Quyettoan>().ToTable("Hopdong_Quyettoan");
            modelBuilder.Entity<Hopdong_Quyettoan_Chiphi>().ToTable("Hopdong_Quyettoan_Chiphi");
            modelBuilder.Entity<Hopdong_Tamung>().ToTable("Hopdong_Tamung");
            modelBuilder.Entity<Hopdong_Suco>().ToTable("Hopdong_Suco");
            modelBuilder.Entity<Hopdong_TailieuDinhkem>().ToTable("Hopdong_TailieuDinhkem");
            modelBuilder.Entity<Hopdong_TailieuDinhkem_AnhCongtrinh>().ToTable("Hopdong_TailieuDinhkem_AnhCongtrinh");
            modelBuilder.Entity<Hopdong_Vattu>().ToTable("Hopdong_Vattu");
        }
    }
}
