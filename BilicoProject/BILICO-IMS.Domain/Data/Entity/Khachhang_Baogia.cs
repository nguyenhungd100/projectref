
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Khachhang_Baogia
    {
        [Key]
        public int BaogiaId { get; set; }
        public int KhachhangId { get; set; }
        public string NoidungBaogia { get; set; }
        public string PhanhoiKhachhang { get; set; }
        public int Nguoitao { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public Nullable<int> Nguoicapnhat { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
    }
}
