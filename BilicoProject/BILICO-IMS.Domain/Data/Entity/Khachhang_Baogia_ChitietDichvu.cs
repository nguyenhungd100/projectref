
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Khachhang_Baogia_ChitietDichvu
    {
        public int Id { get; set; }
        public int BaogiaId { get; set; }
        public string Dichvu { get; set; }
        public int Soluong { get; set; }
        public decimal Gia { get; set; }
        public decimal Thanhtien { get; set; }
    }
}
