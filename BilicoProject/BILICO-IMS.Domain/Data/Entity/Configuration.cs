﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Configuration
    {
        public int Id { set; get; }
        public string SmtpServer { set; get; }
        public int SmtpPort { set; get; }
        public string Username { set; get; }
        public string Password { set; get; }
        public bool UseSsl { set; get; }
        public string AuthenticationMethod { set; get; }
        public string EmailTest { set; get; }
    }
}
