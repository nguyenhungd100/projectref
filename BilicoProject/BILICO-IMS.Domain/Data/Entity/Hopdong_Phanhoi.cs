﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_Phanhoi
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public string Nhanxet { set; get; }
        public DateTime Ngaythicong { set; get; }
        public bool Ketqua { set; get; }
        public int KythuatId { set; get; }
    }
}
