﻿using BILICO.IMS.Domain.Services.HopdongFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_Quyettoan_Chiphi
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int QuyettoanId { set; get; }
        public LoaiChiphi LoaiChiphi { set; get; }
        public decimal Sotien { set; get; }
    }
}
