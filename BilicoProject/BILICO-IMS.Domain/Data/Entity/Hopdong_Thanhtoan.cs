
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hopdong_Thanhtoan
    {
        public int Id { get; set; }
        public int HopdongId { get; set; }
        public string NoidungThanhtoan { get; set; }
        public decimal Sotien { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
    }
}
