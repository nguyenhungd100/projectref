﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_Vattu
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public string Vattu { set; get; }
        public DateTime Ngaygiao { set; get; }
        public DateTime? Ngaynhan { set; get; }
        public int NguoigiaoId { set; get; }
        public int? NguoinhanId { set; get; }
        public string Ghichu { set; get; }
    }
}
