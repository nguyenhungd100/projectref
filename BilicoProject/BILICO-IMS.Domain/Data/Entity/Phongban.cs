
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Phongban
    {
        [Key]
        public int PhongbanId { get; set; }
        public string TenPhongban { get; set; }
        public int Thutu { get; set; }
    }
}
