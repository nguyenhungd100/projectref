
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Sanpham
    {
        [Key]
        public int SanphamId { set; get; }
        public string AnhSanpham { set; get; }
        public int? NhacungcapId { set; get; }
        public int? HangsanxuatId { set; get; }
        public int? NhomsanphamId { set; get; }
        public string MaSanpham { set; get; }
        public string TenSanpham { set; get; }
        public string Mota { set; get; }
        public decimal Giadauvao { set; get; }

        public DateTime Ngaytao { set; get; }
        public int NguoitaoId { set; get; }
        public DateTime? Ngaycapnhat { set; get; }
        public int? NguoiCapnhatId { set; get; }
    }
}
