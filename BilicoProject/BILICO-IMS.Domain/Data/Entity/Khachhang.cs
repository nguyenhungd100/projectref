
namespace BILICO.IMS.Domain.Data.Entity
{
    using BILICO.IMS.Domain.Services.KhachhangFacade;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Khachhang
    {
        [Key]
        public int KhachhangId { get; set; }

        public Nullable<CustomerType> LoaiKhachhang { set; get; }
        public String TenCongty { set; get; }
        public string Xungho { get; set; }
        public string Hoten { get; set; }
        public Nullable<System.DateTime> Ngaysinh { get; set; }
        public Nullable<bool> Gioitinh { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string TKFacebook { get; set; }
        public Nullable<int> ThanhphoId { get; set; }
        public Nullable<int> QuanhuyenId { get; set; }
        public Nullable<int> LoaiKHId { get; set; }
        public Nullable<int> NguonkhachId { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public int Nguoitao { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
        public Nullable<int> Nguoicapnhat { get; set; }
        public CustomerStatus Trangthai { get; set; }
        public Nullable<System.DateTime> Ngaygiao { get; set; }
        public Nullable<int> SalerId { get; set; }
        public string NhandebietIds { get; set; }
        public string DiachiMap { get; set; }
        public string Ghichu { get; set; }
        public string FileDinhkem { get; set; }
        public string Diachi { set; get; }
        public int? SubSalerId { get; set; }
    }
}
