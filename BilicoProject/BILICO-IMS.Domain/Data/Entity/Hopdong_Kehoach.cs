﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_Kehoach
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int? ParentId { set; get; }
        public string NoidungThuchien { set; get; }
        public DateTime Batdau { set; get; }
        public DateTime Ketthuc { set; get; }
        public string Ghichu { set; get; }
        public int? NguoitaoId { get; set; }

    }
}
