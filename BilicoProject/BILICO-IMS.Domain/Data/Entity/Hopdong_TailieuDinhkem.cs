﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_TailieuDinhkem
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int KehoachId { set; get; }
        public DateTime Ngaytao { set; get; }
        public int NguoitaoId { set; get; }
    }
}
