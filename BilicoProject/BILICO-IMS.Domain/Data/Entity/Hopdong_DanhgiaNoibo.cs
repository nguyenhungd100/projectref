﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_DanhgiaNoibo
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int KythuatId { set; get; }
        public int Tiendo { set; get; }
        public int Chatluong { set; get; }
        public int Chiphi { set; get; }
        public int PhanhoiKH { set; get; }
        public string Noidung { set; get; }
        public DateTime Ngaydanhgia { set; get; }
    }
}
