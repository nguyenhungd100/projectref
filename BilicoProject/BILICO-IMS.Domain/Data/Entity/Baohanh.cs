﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Baohanh
    {
        public int Id { set; get; }
        public int SanphamId { set; get; }
        public int HopdongId { set; get; }
        public string GhichuHonghoc { set; get; }
        public DateTime ThoigianDukienHoanthanh { set; get; }
        public int KythuatXulyId { set; get; }
        public DateTime Ngaytao { set; get; }
        public int NguoitaoId { set; get; }
    }
}
