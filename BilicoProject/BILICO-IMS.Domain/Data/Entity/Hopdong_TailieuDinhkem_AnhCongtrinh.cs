﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_TailieuDinhkem_AnhCongtrinh
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int TailieuId { set; get; }
        public string AnhCongtrinh { set; get; }
    }
}
