
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Nhomsanpham
    {
        [Key]
        public int Id { get; set; }
        public string TenNhomsanpham { get; set; }
        public Nullable<bool> Hoatdong { get; set; }
        public Nullable<int> Thutu { get; set; }
    }
}
