
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hopdong_Sanpham
    {
        public int Id { get; set; }
        public int HopdongId { get; set; }
        public int SanphamId { get; set; }
        public string MaSP { get; set; }
        public int Soluong { get; set; }
        public decimal Gia { get; set; }
        public decimal Thanhtien { get; set; }
    }
}
