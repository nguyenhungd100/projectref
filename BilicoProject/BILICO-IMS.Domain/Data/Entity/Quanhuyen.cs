
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class QuanHuyen
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> ThanhphoId { get; set; }
        public System.DateTime AddedDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public bool IsActive { get; set; }
        public string PermanLink { get; set; }
    }
}
