
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class LoaiKhachhang
    {
        [Key]
        public int LoaiKhachhangId { get; set; }
        public string TenLoai { get; set; }
        public Nullable<short> Thutu { get; set; }
    }
}
