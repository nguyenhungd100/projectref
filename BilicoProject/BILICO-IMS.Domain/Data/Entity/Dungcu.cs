﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Dungcu
    {
        public int Id { set; get; }
        public string TenDungcu { set; get; }
        public DateTime Ngaygiao { set; get; }
        public DateTime Ngaynhan { set; get; }
        public int NguoigiaoId { set; get; }
        public int NguoinhanId { set; get; }
        public decimal Dongia { set; get; }
        public int Soluong { set; get; }
        public int NguoimuaId { set; get; }
        public string Ghichu { set; get; }
    }
}
