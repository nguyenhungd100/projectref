
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Khachhang_Tuvan
    {
        [Key]
        public int Id { get; set; }
        public int KhachhangId { get; set; }
        public Nullable<int> SanphamTuvanId { get; set; }
        public string NoidungTuvan { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public int Nguoitao { get; set; }
        public int Tinhtrang { get; set; }

        public bool? PhanXuly { get; set; }
        public int? NguoiXulyId { get; set; }
        public string NoidungGiaoviec { get; set; }
        public DateTime? HanXuly { get; set; }
    }
}
