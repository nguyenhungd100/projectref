﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class TraodoiCongviec
    {
        public int Id { set; get; }
        public int CongviecId { set; get; }
        public string NoidungThaoluan { set; get; }
        public int NguoitaoId { set; get; }
        public DateTime Ngaytao { set; get; }
        public int? ParentId { set; get; }
    }
}
