﻿using BILICO.IMS.Domain.Services.HopdongFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_Nhatkythicong
    {
        public int Id { set; get; }
        public int KehoachId { set; get; }
        public int HopdongId { set; get; }
        public string Noidungbaocao { set; get; }
        public LoaiBaocaoCongviec LoaiCongviec { set; get; }
        public DateTime? Ngaybaocao { set; get; }
        public DateTime Batdau { set; get; }
        public DateTime Ketthuc { set; get; }
        public byte[] AnhDinhkem { set; get; }
        public int NguoibaocaoId { set; get; }
        public int? ThoigianThuchien { set; get; }
    }
}
