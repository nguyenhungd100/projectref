
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Nhucaugia
    {
        [Key]
        public int NhucauId { get; set; }
        public string TenNhucau { get; set; }
        public Nullable<bool> Hoatdong { get; set; }
    }
}
