
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Hangsanxuat
    {
        [Key]
        public int Id { get; set; }
        public string TenHangsanxuat { get; set; }
        public bool? Hoatdong { get; set; }
        public Nullable<int> Thutu { get; set; }
    }
}
