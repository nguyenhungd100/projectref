
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class NguonKhach
    {
        [Key]
        public int NguonKhachId { get; set; }
        public string TenNguon { get; set; }
        public Nullable<bool> Hoatdong { get; set; }
    }
}
