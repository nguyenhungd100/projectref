
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Khachhang_Nhucaugia
    {
        public int Id { get; set; }
        public int KhachhangId { get; set; }
        public int NhucaugiaId { get; set; }
    }
}
