
namespace BILICO.IMS.Domain.Data.Entity
{
    using BILICO.IMS.Domain.Services.HopdongFacade;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Hopdong
    {
        [Key]
        public int HopdongId { get; set; }
        public int KhachhangId { get; set; }
        public int? BaogiaId { get; set; }
        public string SoHD { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public int Nguoitao { get; set; }
        public decimal Trigia { get; set; }
        public System.DateTime? Ngaycapnhat { get; set; }
        public int? Nguoicapnhat { get; set; }
        public OrderStatus Trangthai { get; set; }
        public string Ghichu { get; set; }
        public LoaiHopdong Loai { get; set; }
        public System.DateTime? NgayTrienkhaiDukien { get; set; }
        public System.DateTime? NgayHoanthanhDukien { get; set; }
    }
}
