
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Sanpham_Gia
    {
        [Key]
        public int Id { get; set; }
        public int SanphamId { get; set; }
        public int NhucaugiaId { get; set; }
        public decimal Gia { set; get; }
    }
}
