﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_Giaoviec
    {
        public int Id { set; get; }
        public int KehoachId { set; get; }
        public int HopdongId { set; get; }
        public string NoidungCongviec { set; get; }
        public DateTime Batdau { set; get; }
        public DateTime Ketthuc { set; get; }
        public int NguoithuchienId { set; get; }
        public string Ghichu { set; get; }
        public byte[] Dinhkem { set; get; }
    }
}
