
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hopdong_Dukienhangcanthiet
    {
        public int Id { get; set; }
        public int HopdongId { get; set; }
        public string TenMathang { get; set; }
        public string Chungloai { get; set; }
        public System.DateTime Thoigiancan { get; set; }
        public int Tylechot { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
        public string Ghichu { set; get; }
    }
}
