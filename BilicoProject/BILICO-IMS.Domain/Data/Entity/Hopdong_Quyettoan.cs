﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_Quyettoan
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int NguoiQuyettoanId { set; get; }
        public DateTime NgayQuyettoan { set; get; }
        public int NguoiDuyetQuyettoanId { set; get; }
        public string Ghichu { set; get; }
        public int? TamungId { set; get; }
    }
}
