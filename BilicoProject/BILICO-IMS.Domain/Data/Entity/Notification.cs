﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Notification
    {
        public int Id { set; get; }
        public int SenderId { set; get; }
        public string ReceiverIds { set; get; }
        public DateTime? CreateDate { set; get; }
        public DateTime? SendDate { set; get; }
        public string ReceivedUserIds { set; get; }
        public string ViewedUserIds { set; get; }
        public string DeletedUserIds { set; get; }
        public string Title { set; get; }
        public string Message { set; get; }
        public int ResourceType { set; get; }
        public int ObjectId { set; get; }
    }
}
