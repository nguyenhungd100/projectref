
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Taikhoan
    {
        [Key]
        public int TaikhoanId { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public string Matkhau { get; set; }
        public int PhongbanId { get; set; }
        public string Hovaten { get; set; }
        public string Mobile { get; set; }
        public string Diachi { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
        public bool Tinhtrang { get; set; }
        public Guid UserModifiedTimeStamp { set; get; }
        public string Roles { get; set; }
    }
}
