﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Hopdong_Nhansu
    {
        public int Id { set; get; }
        public int HopdongId { set; get; }
        public int UserId { set; get; }
        public string Vaitro { set; get; }
        public decimal? ChiphiNgaycong { set; get; }
    }
}
