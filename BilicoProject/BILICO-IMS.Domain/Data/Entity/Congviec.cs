﻿using BILICO.IMS.Domain.Services.CongviecFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    public class Congviec
    {
        public int Id { set; get; }
        public string TenCongviec { set; get; }
        public DateTime Ngaybatdau { set; get; }
        public DateTime Deadline { set; get; }
        public string MotaCongviec { set; get; }
        public string FileDinhkems { set; get; }
        public string HopdongLienquans { set; get; }
        public int NguoithuchienId { set; get; }
        public Trangthai Trangthai { set; get; }
        public int NguoitaoId { set; get; }
        public DateTime Ngaytao { set; get; }
    }
}
