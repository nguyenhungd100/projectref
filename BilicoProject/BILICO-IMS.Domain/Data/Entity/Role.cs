﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BILICO.IMS.Domain.Data.Entity
{
    [Table("Roles")]
    public class Role
    {
        [Key]
        public int RoleId { set; get; }
        public string RoleName { set; get; }
        public string Description { set; get; }
        public string Title { set; get; }
        public string Permissions { set; get; }
    }
}
