
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Khachhang_Baogia_ChitietSanpham
    {
        [Key]
        public int Id { get; set; }
        public int BaogiaId { get; set; }
        public int SanphamId { get; set; }
        public int Soluong { get; set; }
        public decimal Gia { get; set; }
        public decimal Thanhtien { get; set; }
    }
}
