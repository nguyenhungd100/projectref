
namespace BILICO.IMS.Domain.Data.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Khachhang_SanphamQuantam
    {
        [Key]
        public int Id { get; set; }
        public int SanphamId { get; set; }
        public long KhachhangId { get; set; }
    }
}
